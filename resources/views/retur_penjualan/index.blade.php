@extends('layouts.admin')

@section('title')
    <title>EPOS | Retur Penjualan</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #tableReturPenjualan .btn {
            margin-bottom: 0;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Retur Penjualan {{ $transaksi_penjualan->kode_transaksi }}
                </h2>
                <a href="{{ url('transaksi-grosir/'.$transaksi_penjualan->id.'/retur/create') }}" class="btn btn-sm btn-success pull-right" style="margin-right: 0;">
                    <i class="fa fa-sign-in"></i> Tambah Retur
                </a>
                <a href="{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}" class="btn btn-sm btn-info pull-right">
                    <i class="fa fa-eye"></i> Detail Penjualan
                </a>
                <a href="{{ url('transaksi') }}" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-bordered table-hover table-striped" style="margin-bottom: 0px" id="tableReturPenjualan">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Retur</th>
                            <!-- <th>Kode Transaksi</th> -->
                            <th>Operator</th>
                            <th style="width: 100px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($retur_penjualan as $i => $retur)
                        <tr data-id="{{ $retur->id }}" kode="{{ $a = $i }}">
                            <td>{{ ++$i }}</td>
                            <td>{{ $retur->created_at->format('d-m-Y H:i:s') }}</td>
                            <td>{{ $retur->kode_retur }}</td>
                            {{-- <td>{{ $transaksi_penjualan->kode_transaksi }}</td> --}}
                            <td>{{ $retur->user->nama }}</td>
                            <td>
                                <a href="{{ url('transaksi/'.$transaksi_penjualan->id.'/retur/'.$retur->id) }}" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if($bayaran[$a] == 1)
                                    @if($pembayaran_data[$a]->cek_lunas != null || $pembayaran_data[$a]->bg_lunas != null)
                                        @if($pembayaran_data[$a]->cek_lunas == 0 && $pembayaran_data[$a]->nominal_cek > 0 )
                                            <a class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                                CEK
                                            </a>
                                        @endif
                                        @if($pembayaran_data[$a]->bg_lunas == 0 && $pembayaran_data[$a]->nominal_bg > 0)
                                            <a class="btn btn-xs btn-BG" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                                BG
                                            </a>
                                        @endif
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Penjualan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Penjualan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Penjualan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Penjualan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Retur Penjualan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Retur Penjualan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $(document).ready(function() {
            var url = "{{ url('retur-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $('#tableReturPenjualan').DataTable({
                'order': [[1, 'desc']]
            });
        });
    
    </script>
@endsection
