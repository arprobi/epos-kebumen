@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Pesanan Penjualan (Prioritas)</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnKembali {
            margin-bottom: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
        .has-error .btn {
            border-color: #a94442;
        }
        /* #infoTransaksi {
            position: fixed;
            bottom: 15px;
            left: 15px;
            z-index: 1000;
            background: rgba(237, 237, 237, 0.3);
            color: #fefefe;
            font-size: 1.2em;
            padding: 10px;
        } */
    </style>
@endsection

@section('content')
    <!-- <div id="infoTransaksi">
        TOTAL 0 ITEM
    </div> -->
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-12">
                        <h2 id="formSimpanTitle">Tambah Pesanan Penjualan (Prioritas)</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    {{-- <div class="col-md-3 pull-right">
                        <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div> --}}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-10 col-xs-10">
                        <label class="control-label">Pilih Pelanggan</label>
                        <select name="pelanggan_id" id="pilihPelanggan" class="select2_single form-control">
                            <option value="">Pilih Pelanggan</option>
                            @foreach ($pelanggans as $pelanggan)
                            <option value="{{ $pelanggan->id }}">[{{ $pelanggan->id }}] {{ $pelanggan->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2 col-xs-2" style="text-align: right;">
                        <label class="control-label" style="opacity: 0;">Aksi</label>
                        <button id="resetPelanggan" class="btn btn-default" style="margin-right: 0;"><i class="fa fa-trash"></i></button>
                    </div>
                    <div class="form-group col-sm-3 col-xs-3" style="display: none;">
                        <label class="control-label">Kode</label>
                        <input type="text" name="inputKodePelanggan" id="inputKodePelanggan" class="form-control">
                    </div>
                    <div class="form-group col-sm-7 col-xs-7" style="display: none;">
                        <label class="control-label">Nama</label>
                        <input type="text" name="inputNamaPelanggan" id="inputNamaPelanggan" class="form-control">
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelPelanggan" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Nama</th>
                                    <th style="text-align: left;">Alamat</th>
                                    <th style="text-align: left;">Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 hidden-xs">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Item</label>
                        <select name="item_id" id="pilihItem" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                            @foreach($items as $item)
                            <option value="{{$item->kode}}">[{{ $item->kode }}] {{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Item</th>
                                    <th style="text-align: left;">Jumlah (pcs)</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Belanja</h2>
                <div id="tipe_penjualan" class="label pull-right" style="font-size: 14px; font-weight: 400;" data-toggle="tooltip" data-placement="top" title="Status Penjualan">Eceran</div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <table class="table" id="tabelKeranjang">
                            <thead>
                                <tr>
                                    <th style="width: 20px;"></th>
                                    <th style="text-align: left;">Item</th>
                                    {{-- <th style="text-align: left; width: 100px;">Jumlah</th> --}}
                                    <th style="text-align: center; width: 300px;">Jumlah</th>
                                    {{-- <th style="text-align: left; width: 100px;">Satuan</th> --}}
                                    <th style="text-align: center; width: 125px;">Harga</th>
                                    <th style="text-align: center; width: 150px;">Total</th>
                                    <th style="text-align: center; width: 150px;">Nego Total (Rp)</th>
                                    <th style="text-align: left; width: 150px;">
                                        <span id="labelLaba" class="label label-success" style="color: white;">Laba</span>
                                        <span id="labelRugi" class="label label-danger" style="color: white;">Rugi</span>
                                    </th>
                                    <th style="text-align: left; width: 100px;">Bonus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <div class="row">
                            {{-- <div class="form-group col-sm-12 col-xs-12" id="pilihanPotonganContainer">
                                <label class="control-label">Pilihan Potongan</label>
                                <div id="pilihanPotonganButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnPotonganPersen" class="btn btn-default">Potongan Persen</button>
                                        <input type="hidden" id="checkPotonganPersen" value="false" />
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnPotonganTunai" class="btn btn-default">Potongan Tunai</button>
                                        <input type="hidden" id="checkPotonganTunai" value="false" />
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Bonus Penjualan</label>
                                <table class="table table-bordered table-hover table-stripped" id="tabelBonus">
                                    <thead>
                                        <tr>
                                            <th style="width: 20px;">No</th>
                                            <th>Bonus</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kredit</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDeposito" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Deposito</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_id">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Kartu</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    {{-- <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Jatuh Tempo</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputJatuhTempo" id="inputJatuhTempo" class="form-control">
                                        </div>
                                    </div> --}}
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div id="inputKreditContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Kredit</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKredit" id="inputNoKredit" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Kredit</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKredit" id="inputNominalKredit" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <div id="inputDepositoContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Deposito <span id="jumlahDeposito"></span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalDeposito" id="inputNominalDeposito" class="form-control angka">
                                </div>
                            </div>
                            <div id="JatuhTempoDiv" class="form-group col-sm-12 col-xs-12" style="margin-top: 20px; margin-bottom: 0;">
                                <div class="line"></div>
                                <label class="control-label">Jatuh Tempo</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="inputJatuhTempo" id="inputJatuhTempo" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                {{-- <label class="control-label">Metode Pembayaran</label> --}}
                                <div id="diambilAtauDikirmButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDiambil" class="btn btn-default"><i class="fa"></i> Diambil</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDikirim" class="btn btn-default"><i class="fa"></i> Dikirim</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputDikirimContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Karyawan</label>
                                        <select class="form-control select2_single" name="user_id">
                                            <option value="">Pilih Karyawan</option>
                                            @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                            @endforeach
                                            <option value="-">Pengirim Lain</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pengirim Lain</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputPengirimLain" class="form-control" disabled="" style="height: 38px;">
                                        </div>
                                        <p>Jika pengirim belum terdaftar, isi pengirim lain!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                {{-- <label class="control-label">Metode Pembayaran</label> --}}
                                <div id="alamatBiasaAtauLainButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnAlamatBiasa" class="btn btn-default"><i class="fa"></i> Alamat Biasa</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnAlamatLain" class="btn btn-default"><i class="fa"></i> Alamat Lain</button>
                                    </div>
                                </div>
                            </div>
                            <div id="alamatBiasaAtauLainContainer" class="form-group col-sm-12 col-xs-12">
                                {{-- <div class="line"></div> --}}
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <label class="control-label">Input Alamat</label>
                                        <textarea id="inputAlamat" class="form-control" name="inputAlamat" style="min-width: 100%; max-width: 100%;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div id="warningHutangContainer" class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                <p class="text-danger p1">Batas hutang Rp0,-</p>
                                <p class="text-danger p2">Batas hutang yang telah dipakai Rp0,-</p>
                                <p class="text-danger p3">Sisa batas hutang Rp0,-</p>
                                <p class="text-danger p4">Minimal jumlah bayar Rp0,-</p>
                            </div>
                            <div id="warningJatuhTempoContainer" class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                <p class="text-danger">Transaksi ini tidak boleh hutang, karena ada hutang yang melewati jatuh tempo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-6">
                                <label class="control-label">Sub Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />
                                    <input type="hidden" name="hiddenHargaTotal" id="hiddenHargaTotal" />
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-xs-6" id="negoSubTotal">
                                <label class="control-label">Nego Sub Total (Rp)</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><input type="checkbox" name="checkNegoTotal" id="checkNegoTotal" /></div>
                                    <input type="hidden" name="inputNegoTotalMin" id="inputNegoTotalMin" />
                                    <input type="text" name="inputNegoTotal" id="inputNegoTotal" class="form-control angka" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-xs-6">
                                <label class="control-label">
                                    <span id="labelLabaTotal" class="label label-success" style="color: white;">Laba Sub Total</span>
                                    <span id="labelRugiTotal" class="label label-danger" style="color: white;">Rugi Sub Total</span>
                                </label>
                                <div class="input-group">
                                    <div id="inputLabaRugiTotalContainer" class="input-group-addon">Rp</div>
                                    <input type="text" name="inputLabaRugiTotal" id="inputLabaRugiTotal" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-xs-6">
                                <label class="control-label">Nego & Potongan Penjualan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputPotonganPenjualan" id="inputPotonganPenjualan" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka" />
                                </div>
                            </div>
                            <div id="jumlahGrandContainer" class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Grand Total (+Ongkos Kirim)</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div id="jumlahBayarContainer" class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Pengurang Tagihan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div id="jumlahKembaliContainer" class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Tagihan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('transaksi-grosir') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="_method" value="put" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="pelanggan" value="" />
                                        <input type="hidden" name="kode_pelanggan" value="" />
                                        <input type="hidden" name="nama_pelanggan" value="" />
                                        <input type="hidden" name="level_pelanggan" value="" />
                                        <input type="hidden" name="lewat_jatuh_tempo" value="" />
                                        <input type="hidden" name="limit_piutang" value="" />
                                        <input type="hidden" name="batas_piutang" value="" />
                                        <input type="hidden" name="titipan" value="" />
                                        <input type="hidden" name="grosir" value="true" />
                                        {{-- <input type="hidden" name="potongan" value="" /> --}}

                                        <input type="hidden" name="harga_total" />
                                        <input type="hidden" name="harga_akhir" />
                                        <input type="hidden" name="nego_total" />
                                        <input type="hidden" name="check_nego_total" />
                                        <input type="hidden" name="nego_total_min" />
                                        <input type="hidden" name="nego_total_view" />
                                        <input type="hidden" name="potongan_penjualan" />
                                        <input type="hidden" name="ongkos_kirim" />
                                        <input type="hidden" name="jumlah_bayar" />
                                        <input type="hidden" name="kembali" />

                                        <input type="hidden" name="nominal_tunai" />

                                        <input type="hidden" name="no_transfer" />
                                        <input type="hidden" name="bank_transfer" />
                                        <input type="hidden" name="nominal_transfer" />

                                        <input type="hidden" name="no_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />

                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="nominal_cek" />

                                        <input type="hidden" name="jatuh_tempo" />
                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="nominal_bg" />

                                        <input type="hidden" name="nominal_titipan" />

                                        <input type="hidden" name="pengirim" />
                                        <input type="hidden" name="pengirim_lain" />

                                        <input type="hidden" name="alamat_lain" />

                                        <div id="append-section"></div>
                                        <div id="bonus-section"></div>
                                        <div class="clearfix">
                                        </div>
                                    </form>
                                </div>
                                <div class="form-group pull-left">
                                    <button type="submit" name="btnSimpanPO" id="btnSimpanPO" class="btn btn-success" disabled=""><i class="fa fa-save"></i> Simpan Pesanan</button>
                                    {{--<button type="submit" name="btnCetakPengambilan" id="btnCetakPengambilan" class="btn btn-success" disabled=""><i class="fa fa-print"></i> Cetak Pengambilan</button>--}}
                                    {{--<button type="submit" name="btnBayar" id="btnBayar" class="btn btn-success" disabled=""><i class="fa fa-money"></i> Bayar</button>--}}
                                </div>
                                <div class="form-group pull-left">
                                    <div type="div" id="infoTransaksi">
                                        TOTAL 0 ITEM
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('gagal') == 'ubah_stok')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Stok barang ada  yang kurang, cek kembali!',
                timer: 3000,
                type: 'warning'
            });
        </script>
    @endif
    <script type="text/javascript">
        var banks = [];
        var users = [];
        var items = [];
        var satuans = [];
        var kode_items = [];
        var pelanggans = [];
        var bonus_rules = [];
        var selected_items = [];
        var transaksi_penjualan = null;
        var custom_data_last_item = false;
        var relasi_transaksi_penjualan = null;
        var swapped = false;

        /*function isBtnSimpanPODisabled() {
            // console.log('isBtnSimpanPODisabled');
            var pelanggan = $('input[name="pelanggan"]').val();
            var kode_pelanggan = $('input[name="kode_pelanggan"]').val();
            var nama_pelanggan = $('input[name="nama_pelanggan"]').val();
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            // console.log(harga_total);

            if (pelanggan == '' && (kode_pelanggan == '' || nama_pelanggan == '')) return true;

            if (isNaN(harga_total) || harga_total <= 0) return true;

            return false;
        }*/

        function isSubmitButtonDisabled() {
            // console.log('isSubmitButtonDisabled');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var kode_pelanggan = $('input[name="kode_pelanggan"]').val();
            var nama_pelanggan = $('input[name="nama_pelanggan"]').val();
            var limit_piutang = parseFloat($('input[name="limit_piutang"]').val());
            var batas_piutang = parseFloat($('input[name="batas_piutang"]').val());
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var ongkos_kirim = parseFloat($('input[name="ongkos_kirim"]').val());
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
            var no_transfer = $('input[name="no_transfer"]').val();
            var bank_transfer = $('input[name="bank_transfer"]').val();
            var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
            var no_kartu = $('input[name="no_kartu"]').val();
            var bank_kartu = $('input[name="bank_kartu"]').val();
            var jenis_kartu = $('input[name="jenis_kartu"]').val();
            var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
            var no_cek = $('input[name="no_cek"]').val();
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
            var no_bg = $('input[name="no_bg"]').val();
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
            var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
            // console.log(nominal_titipan);

            if(ongkos_kirim > 0 && ongkos_kirim % 100 != 0) return true;

            if (isNaN(limit_piutang)) limit_piutang = 0;
            if (isNaN(batas_piutang)) batas_piutang = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;

            if (pelanggan_id != '') {
                var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
                // if (utang > 0 && utang > batas_piutang) {
                var lewat_jatuh_tempo = pelanggan.lewat_jatuh_tempo;
                if (utang > 0) {
                    $('#warningHutangContainer').show('fast', function() {
                        var jumlah_bayar_minimal = harga_total_plus_ongkos_kirim - batas_piutang;
                        if (jumlah_bayar_minimal < 0) jumlah_bayar_minimal = 0;
                        $(this).find('.p1').text('Batas hutang Rp'+limit_piutang.toLocaleString()+',-');
                        $(this).find('.p2').text('Batas hutang yang telah dipakai Rp'+(limit_piutang - batas_piutang).toLocaleString()+',-');
                        $(this).find('.p3').text('Sisa batas hutang Rp'+batas_piutang.toLocaleString()+',-');
                        $(this).find('.p4').text('Minimal jumlah bayar Rp'+(jumlah_bayar_minimal).toLocaleString()+',-');
                    });

                    if (lewat_jatuh_tempo > 0) {
                        $('#warningJatuhTempoContainer').show('fast');
                        return true;
                    } else if (utang > batas_piutang) {
                        $('<div id="warning"></div>JatuhTempoContainer').hide('fast');
                        return true;
                    } else {
                        $('#warningJatuhTempoContainer').hide('fast');
                    }
                } else {
                    $('#warningHutangContainer').hide('fast');
                    $('#warningJatuhTempoContainer').hide('fast');
                }
            } else {
                $('#warningHutangContainer').hide('fast');
                $('#warningJatuhTempoContainer').hide('fast');
            }

            // console.log('');
            // console.log('pelanggan');
            if (pelanggan_id == '' && (kode_pelanggan == '' || nama_pelanggan == '')) return true;

            // console.log('harga_total');
            if (isNaN(harga_total) || harga_total <= 0) return true;

            // console.log('harga_total');
            if ($('#inputHargaTotal').parents('.input-group').first().hasClass('has-error')) return true;

            // console.log('nominal_tunai', nominal_tunai);
            // if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

            // console.log('bank_transfer');
            if ($('#btnTransfer').hasClass('btn-warning') && (bank_transfer == '' || no_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

            // console.log('no_kartu');
            if ($('#btnKartu').hasClass('btn-info') && (no_kartu == '' || bank_kartu == '' || jenis_kartu == '' || isNaN(nominal_kartu) || nominal_kartu <= 0)) return true;

            // console.log('no_cek');
            if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

            // console.log('no_bg');
            if ($('#btnBG').hasClass('btn-BG') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

            // console.log('nominal_titipan');
            if ($('#btnDeposito').hasClass('btn-purple') && (nominal_titipan <= 0)) return true;

            // console.log('nominal_titipan');
            if ($('#btnDeposito').hasClass('btn-purple') && nominal_titipan > 0 && $('#inputDepositoContainer').find('.input-group').hasClass('has-error')) return true;

            // console.log('checkNego');
            // $('#checkNego').each(function(index, el) {
            //     var checked = $(el).prop('checked');
            //     var error = $(el).parents('.form-group').first().hasClass('has-error');
            //     if (checked && error) return true;
            // });

            // console.log('checkNegoTotal');
            if ($('#checkNegoTotal').prop('checked') && $('#inputNegoTotal').parents('.input-group').first().hasClass('has-error')) return true;

            // console.log('checkNegoTotal');
            if ($('#checkNegoTotal').prop('checked') && $('#checkNegoTotal').parents('.form-group').first().hasClass('has-error')) return true;

            var has_error = false;
            var temp_has_error = false;
            $('#tabelKeranjang tr').each(function(index, el) {
                temp_has_error = $(this).find('#inputJumlahItemContainer').hasClass('has-error');
                if (temp_has_error) has_error = true;

                temp_has_error = $(this).find('#inputLabaRugiContainer').hasClass('has-error');
                if (temp_has_error) has_error = true;

                temp_has_error = $(this).find('#inputNegoContainer').hasClass('has-error');
                if (temp_has_error) has_error = true;
            });

            if($('#inputLabaRugiTotalContainer').parent('div').hasClass('has-error')) return true;
            // console.log(has_error);
            if (has_error) return true;

            // console.log('jumlah_bayar');
            // if (jumlah_bayar > 0 && jumlah_bayar < harga_total + ongkos_kirim) return true;

            // console.log('jumlah_bayar');
            // if (jumlah_bayar > 0 && jumlah_bayar - nominal_tunai > harga_total + ongkos_kirim) return true;

            return false;
        }

        function isBtnSimpanPODisabled() {
            // console.log('isBtnSimpanPODisabled');
            return isSubmitButtonDisabled();
        }

        /*function cekEceranAtauGrosir() {
            var pelanggan_id = $('input[name="pelanggan"]').val();
            if (pelanggan_id == '') {
                var is_grosir = true;
                
                $('input[name="is_grosir[]"]').each(function(index, el) {
                    // jika ada yang salah maka is_grosir false
                    val = $(el).val();
                    if (val == 'false') is_grosir = false;
                });

                $('input[name="grosir"]').val(is_grosir);

                if (is_grosir) {
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            } else {
                // cek selisih jumlah * konversi grosir dan eceran
                var jumlah_grosir = 0;
                var jumlah_eceran = 0;

                $('input[name="item_kode[]"]').each(function(index, el) {
                    var item_kode = $(el).val();
                    var is_grosir = $('#is_grosir-'+item_kode).val() == 'true' ? true : false;
                    var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                    var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                    var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                    var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                    if (is_grosir) jumlah_grosir += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                    else jumlah_eceran += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                });

                if (jumlah_grosir > jumlah_eceran) {
                    $('input[name="grosir"]').val(true);
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('input[name="grosir"]').val(false);
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            }

            $('input[name="grosir"]').val(true);
            $('#tipe_penjualan').text('Grosir');
            $('#tipe_penjualan').removeClass('label-success');
            $('#tipe_penjualan').addClass('label-warning');
        }*/

        function cekEceranAtauGrosir() {
            // console.log('cekEceranAtauGrosir');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var level_pelanggan = $('input[name="level_pelanggan"]').val();

            if (pelanggan_id == '') {
                var is_grosir = true;
                var jumlah_item = 0;

                $('input[name="is_grosir[]"]').each(function(index, el) {
                    // jika ada yang salah maka is_grosir false
                    val = $(el).val();
                    if (val == 'false') is_grosir = false;
                    jumlah_item++;
                });

                $('input[name="grosir"]').val(is_grosir && jumlah_item > 0);

                $('#level_pelanggan').text('Eceran');
                $('#level_pelanggan').removeClass('label-warning');
                $('#level_pelanggan').addClass('label-success');

                if (is_grosir && jumlah_item > 0) {
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            } else {
                // cek selisih jumlah * konversi grosir dan eceran
                var jumlah_grosir = 0;
                var jumlah_eceran = 0;
                $('input[name="item_kode[]"]').each(function(index, el) {
                    var item_kode = $(el).val();
                    var is_grosir = $('#is_grosir-'+item_kode).val() == 'true' ? true : false;
                    var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                    var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                    var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                    var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                    if (is_grosir) jumlah_grosir += 1;
                    else jumlah_eceran += 1;
                });

                if (jumlah_grosir > jumlah_eceran) {
                    $('input[name="grosir"]').val(true);
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('input[name="grosir"]').val(false);
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            }
        }

        function updateHargaTotal() {
            var harga_total = 0;
            var harga_akhir = 0;
            var nego_total = 0;
            var nego_total_min = 0;

            $('.subtotal').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');

                var tmp_harga = parseFloat($('#subtotal-'+item_kode).val());
                if (isNaN(tmp_harga)) tmp_harga = 0;
                harga_total += tmp_harga;

                var tmp_nego = parseFloat($('#nego-'+item_kode).val());
                if (isNaN(tmp_nego)) tmp_nego = 0;
                nego_total += tmp_nego;

                if (tmp_nego > 0) {
                    harga_akhir += tmp_nego;
                } else {
                    harga_akhir += tmp_harga;
                }

                var tmp_nego_min = parseFloat($('#nego_min-'+item_kode).val());
                if (isNaN(tmp_nego_min)) tmp_nego_min = 0;
                nego_total_min += tmp_nego_min;
            });

            var nego_total_view = parseFloat($('input[name="nego_total_view"]').val());
            if (nego_total_view > 0 && nego_total_view < harga_akhir) harga_akhir = nego_total_view;

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="harga_akhir"]').val(harga_akhir);
            $('input[name="nego_total"]').val(nego_total);
            $('input[name="nego_total_min"]').val(nego_total_min);

            $('#inputNegoTotalMin').val(nego_total_min);
        }

        function updateBonusTransaksi() {
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var potongan_penjualan = parseFloat($('input[name="potongan_penjualan"]').val());
            var pelanggan_id = parseFloat($('input[name="pelanggan"]').val());

            var nominal = 0;
            var profit = 0;
            var jumlah = 0;
            var status = 1;

            nominal = harga_total - potongan_penjualan;

            var hpp = 0;
            $('input[name="hpp[]"]').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');
                var temp_hpp = parseFloat($('#hpp-'+item_kode).val());
                var temp_jumlah_1 = parseFloat($('#jumlah1-'+item_kode).val());
                var temp_jumlah_2 = parseFloat($('#jumlah2-'+item_kode).val());
                var temp_konversi_1 = parseFloat($('#konversi1-'+item_kode).val());
                var temp_konversi_2 = parseFloat($('#konversi2-'+item_kode).val());

                if (isNaN(temp_hpp)) temp_hpp = 0;
                if (isNaN(temp_jumlah_1)) temp_jumlah_1 = 0;
                if (isNaN(temp_jumlah_2)) temp_jumlah_2 = 0;
                if (isNaN(temp_konversi_1)) temp_konversi_1 = 0;
                if (isNaN(temp_konversi_2)) temp_konversi_2 = 0;

                var temp_jumlah = (temp_jumlah_1 * temp_konversi_1) + (temp_jumlah_2 * temp_konversi_2);

                temp_hpp *= temp_jumlah;
                hpp += temp_hpp;
            });
            profit = nominal - hpp;

            $('#tabelKeranjang tbody tr').each(function(index, el) {
                jumlah++;
            });

            if (pelanggan_id != '' ) {
                for (var i = 0; i < pelanggans.length; i++) {
                    var pelanggan = pelanggans[i];
                    if (pelanggan.id == pelanggan_id) {
                        if (pelanggan.level == 'grosir') {
                            status = 2;
                        }
                        break;
                    }
                }
            }
            // console.log(nominal, profit, jumlah, status);

            $('#tabelBonus tbody').find('tr').remove();
            $('#form-simpan').find('#bonus-section').find('input').remove();
            var valid_bonus_rules = [];
            for (var i = 0; i < bonus_rules.length; i++) {
                var is_bonus_available = false;
                var is_nominal_valid = false;
                var is_profit_valid = false;
                var is_jumlah_valid = false;
                var is_status_valid = false;
                var jumlah_bonus = 1;
                var bonus_rule = bonus_rules[i];

                if (parseFloat(bonus_rule.nominal) == 0) {
                    is_nominal_valid = true;
                } else {
                    if (parseFloat(nominal) >= parseFloat(bonus_rule.nominal)) {
                        is_nominal_valid = true;
                        if (parseFloat(bonus_rule.kelipatan) > 0) {
                            jumlah_bonus = parseInt(parseFloat(nominal) / parseFloat(bonus_rule.nominal));
                        }
                    }
                }

                if (parseFloat(bonus_rule.profit) == 0) {
                    is_profit_valid = true;
                } else {
                    if (parseFloat(profit) >= parseFloat(bonus_rule.profit)) {
                        is_profit_valid = true;
                    }
                }

                if (parseFloat(bonus_rule.jumlah) == 0) {
                    is_jumlah_valid = true;
                } else {
                    if (parseFloat(jumlah) >= parseFloat(bonus_rule.jumlah)) {
                        is_jumlah_valid = true;
                    }
                }

                if (parseFloat(bonus_rule.status) == 0) {
                    is_status_valid = true;
                } else if (parseFloat(bonus_rule.status) == 1 && parseFloat(status) == 1) {
                    is_status_valid = true;
                } else if (parseFloat(bonus_rule.status) == 2 && parseFloat(status) == 2) {
                    is_status_valid = true;
                }
                // console.log(is_nominal_valid, is_profit_valid, is_jumlah_valid, is_status_valid, jumlah_bonus, i);

                if (is_nominal_valid && is_profit_valid && is_jumlah_valid && is_status_valid) {
                    valid_bonus_rules.push({
                        bonus_rule: bonus_rule,
                        jumlah_bonus: jumlah_bonus
                    });
                }
            }
            // console.log(valid_bonus_rules);

            var bonus_transaksis = [];
            for (var i = 0; i < valid_bonus_rules.length; i++) {
                var bonus_rule = valid_bonus_rules[i].bonus_rule;
                var jumlah_bonus = valid_bonus_rules[i].jumlah_bonus;
                for (var j = 0; j < bonus_rule.relasi_bonuses.length; j++) {
                    if (parseFloat(jumlah_bonus) > 0) {
                        var bonus = bonus_rule.relasi_bonuses[j].bonus;
                        var bonus_id = bonus.id;
                        var bonus_jumlah = parseFloat(jumlah_bonus) * parseFloat(bonus_rule.relasi_bonuses[j].jumlah);

                        if (parseFloat(bonus.stoktotal) < bonus_jumlah) {
                            bonus_jumlah = parseFloat(bonus.stoktotal);
                        }

                        var ada_yang_sama = false;
                        for (var k = 0; k < bonus_transaksis.length; k++) {
                            var bonus_transaksi = bonus_transaksis[k];
                            if (bonus_transaksi.bonus_id == bonus_id) {
                                bonus_transaksi.bonus_jumlah += bonus_jumlah;
                                ada_yang_sama = true;
                                break;
                            }
                        }

                        if (!ada_yang_sama) {
                            bonus_transaksis.push({
                                bonus: bonus,
                                bonus_id: bonus_id,
                                bonus_jumlah: bonus_jumlah,
                            });
                        }
                    }
                }
            }

            var valid_bonus_rules_index = 0;
            // console.log(bonus_transaksis);
            for (var i = 0; i < bonus_transaksis.length; i++) {
                var bonus_transaksi = bonus_transaksis[i];
                var bonus = bonus_transaksi.bonus;
                var bonus_id = bonus_transaksi.bonus_id;
                var bonus_jumlah = bonus_transaksi.bonus_jumlah;
                var bonus_jumlah_satuan = '';
                var temp_bonus_jumlah = bonus_jumlah;
                for (var k = 0; k < bonus.satuan_pembelians.length; k++) {
                    if (temp_bonus_jumlah > 0) {
                        var satuan = bonus.satuan_pembelians[k];
                        if (temp_bonus_jumlah % satuan.konversi == 0) {
                            bonus_jumlah_satuan += parseInt(temp_bonus_jumlah / satuan.konversi);
                            bonus_jumlah_satuan += ' ';
                            bonus_jumlah_satuan += satuan.satuan.kode;
                        }
                    }
                }

                var tr = ''+
                    '<tr>'+
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+bonus.nama+'</td>'+
                        '<td>'+bonus_jumlah_satuan+'</td>'+
                    '</tr>';
                $('#tabelBonus tbody').append($(tr));

                $('#form-simpan').find('#bonus-section').append('<input type="hidden" name="bonus_id[]" id="bonus_id-'+valid_bonus_rules_index+'" value="'+bonus_id+'" />');
                $('#form-simpan').find('#bonus-section').append('<input type="hidden" name="bonus_jumlah[]" id="bonus_jumlah-'+valid_bonus_rules_index+'" value="'+bonus_jumlah+'" />');
                valid_bonus_rules_index++;
            }

            // console.log(valid_bonus_rules_index);
            if (valid_bonus_rules_index > 0) {
                $('#tabelBonus').parents('.form-group').show();
            } else {
                $('#tabelBonus').parents('.form-group').hide();
            }
        }

        function updateInfoTransaksi() {
            $('#infoTransaksi').text(`TOTAL ${selected_items.length} ITEM`);
        }

        function swapGrandTotal() {
            // console.log('swap');
            swapped = true;

            var textGrand = 'Grand Total (+Ongkos Kirim)';
            var textBayar1 = 'Pengurang Tagihan';
            var textBayar2 = 'Jumlah Bayar';
            var textKembali1 = 'Tagihan';
            var textKembali2 = 'Kembalian';

            var harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim').val();
            var jumlah_bayar = $('#inputJumlahBayar').val();
            var kembali = $('#inputTotalKembali').val();

            $('#jumlahGrandContainer').find('label').text(textBayar2);
            $('#jumlahBayarContainer').find('label').text(textGrand);
            $('#jumlahKembaliContainer').find('label').text(textKembali2);

            $('#jumlahGrandContainer').find('input').val(jumlah_bayar);
            $('#jumlahBayarContainer').find('input').val(harga_total_plus_ongkos_kirim);
        }

        function unswapGrandTotal() {
            // console.log('unswap');
            swapped = false;

            var textGrand = 'Grand Total (+Ongkos Kirim)';
            var textBayar1 = 'Pengurang Tagihan';
            var textBayar2 = 'Jumlah Bayar';
            var textKembali1 = 'Tagihan';
            var textKembali2 = 'Kembalian';

            var harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim').val();
            var jumlah_bayar = $('#inputJumlahBayar').val();
            var kembali = $('#inputTotalKembali').val();

            $('#jumlahGrandContainer').find('label').text(textGrand);
            $('#jumlahBayarContainer').find('label').text(textBayar1);
            $('#jumlahKembaliContainer').find('label').text(textKembali1);

            $('#jumlahGrandContainer').find('input').val(harga_total_plus_ongkos_kirim);
            $('#jumlahBayarContainer').find('input').val(jumlah_bayar);
        }

        function updateHargaOnKeyup() {
            if (swapped) {
                unswapGrandTotal();
            }

            updateHargaTotal();

            var $harga_total = $('#inputHargaTotal');
            var $laba_rugi_total = $('#inputLabaRugiTotal');
            var $potongan_penjualan = $('#inputPotonganPenjualan');
            var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $kembali = $('#inputTotalKembali');

            var level_pelanggan = $('input[name="level_pelanggan"]').val();
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var nominal_titipan = $('input[name="nominal_titipan"]').val();
            var harga_total = $('input[name="harga_total"]').val();
            if (harga_total === undefined) harga_total = '0';
            var harga_akhir = $('input[name="harga_akhir"]').val();
            if (harga_akhir === undefined) harga_akhir = '0';
            var nego_total = $('input[name="nego_total"]').val();
            if (nego_total === undefined) nego_total = '0';
            var nego_total_min = $('input[name="nego_total_min"]').val();
            if (nego_total_min === undefined) nego_total_min = '0';
            var nego_total_view = $('input[name="nego_total_view"]').val();
            if (nego_total_view === undefined) nego_total_view = '0';
            var ongkos_kirim = $('input[name="ongkos_kirim"]').val();
            if (ongkos_kirim === undefined) ongkos_kirim = '0';
            // console.log(harga_total);

            nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_transfer = parseFloat(nominal_transfer.replace(/\D/g, ''), 10);
            nominal_kartu = parseFloat(nominal_kartu);
            nominal_cek = parseFloat(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg = parseFloat(nominal_bg.replace(/\D/g, ''), 10);
            nominal_titipan = parseFloat(nominal_titipan.replace(/\D/g, ''), 10);
            harga_total = parseFloat(harga_total);
            harga_akhir = parseFloat(harga_akhir);
            nego_total = parseFloat(nego_total);
            nego_total_min = parseFloat(nego_total_min);
            nego_total_view = parseFloat(nego_total_view);
            ongkos_kirim = parseFloat(ongkos_kirim);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(harga_akhir)) harga_akhir = 0;
            if (isNaN(nego_total)) nego_total = 0;
            if (isNaN(nego_total_min)) nego_total_min = 0;
            if (isNaN(nego_total_view)) nego_total_view = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_kartu + nominal_cek + nominal_bg + nominal_titipan;
            var kembali = 0;
            var harga_total_plus_ongkos_kirim = 0;

            // Tambah potongan penjualan dengan diskon tiap item
            var potongan = 0;
            var potongan_penjualan = 0;
            $('input[name="diskon[]"]').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');

                var harga = 0;
                var harga1_eceran = parseFloat($('#harga1_eceran-'+item_kode).val());
                var harga1_grosir = parseFloat($('#harga1_grosir-'+item_kode).val());
                var harga2_eceran = parseFloat($('#harga2_eceran-'+item_kode).val());
                var harga2_grosir = parseFloat($('#harga2_grosir-'+item_kode).val());
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#harga_grosir-'+item_kode).val());
                var diskon = parseFloat($(el).val());
                // var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                // var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                var subtotal = parseFloat($('#subtotal-'+item_kode).val());
                var nego = parseFloat($('#nego-'+item_kode).val());

                if (isNaN(harga1_eceran)) harga1_eceran = 0;
                if (isNaN(harga1_grosir)) harga1_grosir = 0;
                if (isNaN(harga2_eceran)) harga2_eceran = 0;
                if (isNaN(harga2_grosir)) harga2_grosir = 0;
                if (isNaN(jumlah1)) jumlah1 = 0;
                if (isNaN(jumlah2)) jumlah2 = 0;
                if (isNaN(konversi1)) konversi1 = 0;
                if (isNaN(konversi2)) konversi2 = 0;
                if (isNaN(diskon)) diskon = 0;
                // if (isNaN(harga_eceran)) harga_eceran = 0;
                // if (isNaN(harga_grosir)) harga_grosir = 0;
                if (isNaN(subtotal)) subtotal = 0;
                if (isNaN(nego)) nego = 0;

                if (diskon > 0) {
                    // console.log('diskon');
                    var harga = 0;
                    if (level_pelanggan == 'grosir') {
                        harga = (harga1_grosir * jumlah1) + (harga2_grosir * jumlah2);
                    } else {
                        harga = (harga1_eceran * jumlah1) + (harga2_eceran * jumlah2);
                    }

                    // var jumlah = (jumlah1 * konversi1) + (jumlah2 * konversi2);

                    // harga = harga_eceran;
                    // if (level_pelanggan == 'grosir') harga = harga_grosir;

                    // var temp_potongan = diskon / 100 * harga * jumlah;
                    // temp_potongan = Math.ceil(temp_potongan / 100) * 100;

                    // var potongan_diskon = diskon / 100 * harga * jumlah;
                    var potongan_diskon = diskon / 100 * harga;
                    potongan_diskon = Math.ceil(potongan_diskon / 100) * 100;

                    var potongan_nego = subtotal - nego;
                    if (nego <= 0) {
                        potongan_nego = 0;
                    }
                    potongan_nego = Math.ceil(potongan_nego / 100) * 100;

                    if (potongan_diskon > potongan_nego) {
                        potongan += potongan_diskon;
                    } else {
                        potongan += potongan_nego;
                    }
                    // console.log(potongan_diskon, potongan_nego, potongan);
                } else {
                    if (nego > 0) {
                        var potongan_nego = subtotal - nego;
                        potongan_nego = Math.ceil(potongan_nego / 100) * 100;
                        potongan += potongan_nego;
                    }
                }
            });

            // if (nego_total_view > 0) {
            //     // potongan = harga_total - nego_total_view;
            //     potongan_penjualan = harga_total - nego_total_view;
            // } else {
            //     harga_akhir -= potongan;
            //     potongan_penjualan += (harga_total - harga_akhir);
            // }

            harga_akhir = harga_total - potongan;
            potongan_penjualan = potongan;
            if (nego_total_view > 0 && nego_total_view < harga_akhir) {
                harga_akhir = nego_total_view;
                potongan_penjualan = harga_total - nego_total_view;
            }

            var laba_rugi_total = harga_akhir - nego_total_min;

            if (harga_akhir <= 0) laba_rugi_total = 0;
            if (harga_akhir <= 0) potongan_penjualan = 0;

            harga_total_plus_ongkos_kirim = harga_akhir + ongkos_kirim;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            var swap = false;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(kembali)) kembali = 0;
            // if (kembali < 0) kembali = 0;
            var kembali_asal = kembali;
            if (kembali < 0) {
                kembali *= -1;
            } else {
                swap = true;
            }

            if (pelanggan != '') {
                if (level_pelanggan == 'eceran' && kembali_asal < 0 && isGrosir) {
                    $('#JatuhTempoDiv').show('fast');
                } else if (level_pelanggan == 'grosir' && kembali_asal < 0) {
                    $('#JatuhTempoDiv').show('fast');
                } else {
                    $('#JatuhTempoDiv').hide('fast');
                }
            } else {
                $('#JatuhTempoDiv').hide('fast');
            }

            // abang ijo harga_total
            // console.log(laba_rugi_total);
            if (laba_rugi_total >= 0) {
                $laba_rugi_total.parents('.input-group').removeClass('has-error');
                $laba_rugi_total.parents('.input-group').addClass('has-success');
            } else {
                $laba_rugi_total.parents('.input-group').removeClass('has-success');
                $laba_rugi_total.parents('.input-group').addClass('has-error');
                laba_rugi_total *= -1;
            }

            $harga_total.val(harga_total.toLocaleString());
            // $nego_total_view.val(nego_total_akhir.toLocaleString());
            $laba_rugi_total.val(laba_rugi_total.toLocaleString());
            $potongan_penjualan.val(potongan_penjualan.toLocaleString());
            $harga_total_plus_ongkos_kirim.val((harga_total_plus_ongkos_kirim).toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());
            $kembali.val(kembali.toLocaleString());

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="potongan_penjualan"]').val(potongan_penjualan);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="kembali"]').val(kembali);

            // cek boleh nego atau tidak
            if ($('input[name="pelanggan"]').val() == '') {
                $('input[name="checkNego"]').prop('disabled', true);
                $('input[name="checkNegoTotal"]').prop('disabled', true);
            } else {
                $('input[name="checkNego"]').prop('disabled', false);
                $('input[name="checkNegoTotal"]').prop('disabled', false);
                if ($('input[name="checkNegoTotal"]').prop('checked')) {
                    $('input[name="checkNego"]').prop('disabled', true);
                    $('input[name="inputNego"]').prop('disabled', true);
                } else {
                    $('input[name="checkNego"]').prop('disabled', false);
                    $('input[name="inputNego"]').prop('disabled', false);
                }
            }

            updateBonusTransaksi();
            updateInfoTransaksi();

            // $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
            $('#btnSimpanPO').prop('disabled', isSubmitButtonDisabled());

            if (swap) {
                swapGrandTotal();
            } else {
                unswapGrandTotal();
            }
        }

        /*function updateHargaKeEceran() {
            // console.log('updateHargaKeEceran');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var harga1 = parseFloat($('#harga1-'+item_kode).val());
                var harga2 = parseFloat($('#harga2-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                // var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                // var harga_per_pcs = Math.ceil(harga_eceran / 100) * 100;
                // var harga_view = harga_per_pcs * konversi;
                // var subtotal = harga_per_pcs * jumlah;

                harga1 = parseFloat(harga1);
                harga2 = parseFloat(harga2);

                if (isNaN(harga1)) harga1 = 0;
                if (isNaN(harga2)) harga2 = 0;

                var harga_eceran = 0;
                var subtotal = 0;
                if (konversi1 >= konversi2) {
                    harga_eceran = harga1;
                } else {
                    harga_eceran = harga2;
                }

                subtotal = harga1 * jumlah1 + harga2 * jumlah2;

                $(el).find('#inputHargaPerSatuan').val(harga_eceran.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                $('#harga_eceran-'+item_kode).val(harga_eceran);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }*/

        function updateHargaKeGrosir() {
            // console.log('updateHargaKeGrosir');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var harga1 = parseFloat($('#harga1-'+item_kode).val());
                var harga2 = parseFloat($('#harga2-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                // var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                // var harga_per_pcs = Math.ceil(harga_grosir / 100) * 100;
                // var harga_view = harga_per_pcs * konversi;
                // var subtotal = harga_per_pcs * jumlah;

                harga1 = parseFloat(harga1);
                harga2 = parseFloat(harga2);

                if (isNaN(harga1)) harga1 = 0;
                if (isNaN(harga2)) harga2 = 0;

                var harga_grosir = 0;
                var subtotal = 0;
                if (konversi1 >= konversi2) {
                    harga_grosir = harga1;
                } else {
                    harga_grosir = harga2;
                }

                subtotal = harga1 * jumlah1 + harga2 * jumlah2;

                $(el).find('#inputHargaPerSatuan').val(harga_grosir.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                $('#harga_grosir-'+item_kode).val(harga_grosir);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }

        function handlePelangganChange(pelanggan_id) {
            var id = pelanggan_id;
            if (id != '') {
                for (var i = 0; i < pelanggans.length; i++) {
                    if (pelanggans[i].id == id) {
                        pelanggan = pelanggans[i];
                        $('input[name="pelanggan"]').val(pelanggan.id);
                        $('input[name="kode_pelanggan"]').val(pelanggan.kode);
                        $('input[name="nama_pelanggan"]').val(pelanggan.nama);
                        $('input[name="level_pelanggan"]').val(pelanggan.level);
                        $('input[name="lewat_jatuh_tempo"]').val(pelanggan.lewat_jatuh_tempo);
                        $('input[name="limit_piutang"]').val(pelanggan.limit_jumlah_piutang);
                        $('input[name="batas_piutang"]').val(pelanggan.batas_piutang);
                        // $('#inputKodePelanggan').val(pelanggan.kode);
                        // $('#inputNamaPelanggan').val(pelanggan.nama);
                        // $('#inputNamaPelanggan').prop('readonly', true);

                        var nama = pelanggan.nama ? pelanggan.nama : '-';
                        var alamat = pelanggan.alamat ? pelanggan.alamat : '-';
                        var telepon = pelanggan.telepon ? pelanggan.telepon : '-';
                        var tr = ''+
                            '<tr>'+
                                '<td>'+nama+'</td>'+
                                '<td>'+alamat+'</td>'+
                                '<td>'+telepon+'</td>'+
                            '</tr>';

                        $('#tabelPelanggan tbody').empty();
                        $('#tabelPelanggan tbody').append(tr);

                        var lama_jatuh_tempo = pelanggan.jatuh_tempo;
                        var jatuh_tempo = getJatuhTempo(lama_jatuh_tempo);
                        // console.log(jatuh_tempo);
                        $('#inputJatuhTempo').val(ymd2dmy(jatuh_tempo));
                        $('input[name="jatuh_tempo"]').val(jatuh_tempo);

                        var titipan = pelanggan.titipan;
                        if (titipan == null || titipan == undefined) {
                            titipan = 0;
                        } else {
                            titipan = parseFloat(titipan.replace(/\D/g, ''), 10) / 1000;
                        }

                        if (isNaN(titipan)) titipan = 0;

                        if (titipan > 0) {
                            $('#btnDeposito').prop('disabled', false);
                            $('#jumlahDeposito').text('Rp'+titipan.toLocaleString()+',- | Nominal Deposito harus sama dengan Grand Total');
                        } else {
                            if ($('#btnDeposito').hasClass('btn-purple')) $('#btnDeposito').trigger('click');
                            $('#btnDeposito').prop('disabled', true);
                            $('#jumlahDeposito').text('');
                        }

                        var alamat_lain = $('input[name="alamat_lain"]').val();
                        if (alamat_lain == '') {
                            $('#btnAlamatBiasa').trigger('click');
                        } else {
                            $('#btnAlamatLain').trigger('click');
                        }

                        break;
                    }
                }

                if (pelanggan.level == 'eceran') updateHargaKeEceran();
                else updateHargaKeGrosir();
            } else {
                // $('#inputKodePelanggan').val('');
                // $('#inputNamaPelanggan').val('');
                // $('#inputNamaPelanggan').prop('readonly', false);

                pelanggan = null;
                $('#btnAlamatBiasa').trigger('click');
                $('input[name="pelanggan"]').val('');
                $('input[name="kode_pelanggan"]').val('');
                $('input[name="nama_pelanggan"]').val('');
                $('input[name="level_pelanggan"]').val('');
                $('input[name="lewat_jatuh_tempo"]').val(0);

                $('#tabelPelanggan tbody').empty();

                $('#inputJatuhTempo').val('');
                $('input[name="jatuh_tempo"]').val(0);

                if ($('#btnDeposito').hasClass('btn-purple')) $('#btnDeposito').trigger('click');
                $('#btnDeposito').prop('disabled', true);
                $('#jumlahDeposito').text('');
            }

            // (id !== '') ? $('#pilihanPotonganContainer').show('fast') : $('#pilihanPotonganContainer').hide('fast')

            var titipan = pelanggan == null || pelanggan.titipan == null ? '' : pelanggan.titipan;
            $('input[name="pelanggan"]').val(id);
            $('input[name="titipan"]').val(titipan);
            updateHargaOnKeyup();

            $('#inputJumlahItem1').trigger('keyup');
        }

        function handleItemChange(kode, custom_data) {
            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                selected_items.push(kode);
                $('#spinner').show();
                cariItem(kode, url, tr, custom_data);
            }
        }

        function cariItem(kode, url, tr, custom_data) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;
                var diskon = parseFloat(item.diskon);
                var bonus_id = item.bonus_id;
                var hpp = parseFloat(data.hpp.harga);
                var harga_eceran = parseFloat(data.harga.eceran);
                var harga_grosir = parseFloat(data.harga.grosir);
                var nego_min = Math.round(hpp * 110) / 100;
                var bonuses = data.relasi_bonus;
                var is_grosir = true;

                hpp = Math.round(hpp * 110) / 100;

                var has_error = false;
                if (harga_eceran <= 0 || harga_grosir <= 0) has_error = true;

                // var tp_item = null;
                var rtp_item = null;
                var v_jumlah = 1;
                var v_harga = harga_grosir;
                var v_subtotal = v_harga;
                var v_nego_min = nego_min;
                var v_nego = null;
                var v_bonuses = [];
                var text_bonus = 'Tidak ada';

                if (custom_data) {
                    var status = transaksi_penjualan.status;
                    if (status == 'po_grosir') {
                        v_harga = harga_grosir;
                        v_subtotal = v_harga;
                        is_grosir = true;
                    }

                    // for (var i = 0; i < items.length; i++) {
                    //     if (items[i].kode == kode) {
                    //         tp_item = items[i];
                    //         break;
                    //     }
                    // }

                    for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                        if (relasi_transaksi_penjualan[i].item_kode == kode) {
                            rtp_item = relasi_transaksi_penjualan[i];
                            v_bonuses = rtp_item.relasi_bonus_penjualan;
                            break;
                        }
                    }

                    text_bonus = '';
                    if (bonuses.length > 0) {
                        for (var i = 0; i < v_bonuses.length; i++) {
                            text_bonus += v_bonuses[i].jumlah+' '+v_bonuses[i].bonus.nama;
                            if (i != v_bonuses.length - 1) text_bonus += ', '
                        }
                    } else {
                        text_bonus += 'Tidak ada';
                    }

                    v_jumlah = rtp_item.jumlah;
                    v_harga = rtp_item.harga;
                    v_subtotal = rtp_item.subtotal;

                    if (rtp_item.nego != null) {
                        v_nego = rtp_item.nego;
                    }
                }

                var index_jumlah = 0;
                var v_jumlah1 = 1;
                var v_jumlah2 = 0;
                var v_satuan_id1 = 0;
                var v_satuan_id2 = 0;
                var v_satuan_kode1 = '';
                var v_satuan_kode2 = '';
                var v_konversi1 = 0;
                var v_konversi2 = 0;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var konversi = satuan.konversi;
                    if (v_jumlah / konversi >= 1) {
                        if (index_jumlah == 0) {
                            v_jumlah1 = Math.floor(v_jumlah / konversi);
                            v_satuan_id1 = satuan.satuan.id;
                            v_satuan_kode1 = satuan.satuan.kode;
                            v_konversi1 = konversi;
                        } else {
                            v_jumlah2 = Math.floor(v_jumlah / konversi);
                            v_satuan_id2 = satuan.satuan.id;
                            v_satuan_kode2 = satuan.satuan.kode;
                            v_konversi2 = konversi;
                        }
                        index_jumlah++;
                        v_jumlah = v_jumlah % konversi;
                    }
                }

                if (custom_data) v_nego_min *= rtp_item.jumlah;

                v_jumlah1 = parseFloat(v_jumlah1);
                v_jumlah2 = parseFloat(v_jumlah2);
                v_konversi1 = parseFloat(v_konversi1);
                v_konversi2 = parseFloat(v_konversi2);
                v_harga = parseFloat(v_harga);
                v_subtotal = parseFloat(v_subtotal);
                v_nego_min = parseFloat(v_nego_min);
                v_nego = v_nego != null ? parseFloat(v_nego) : 0;

                var htmlLabaRugi = '';
                var hasWhat = '';
                if (v_nego > 0) {
                    if (v_nego >= v_nego_min) {
                        htmlLabaRugi += '<input type="text" name="inputLabaRugi" id="inputLabaRugi" class="form-control input-sm angka inputLabaRugi" value="'+(v_nego - v_nego_min).toLocaleString()+'" readonly="readonly" />';
                        hasWhat = 'has-success';
                    } else {
                        htmlLabaRugi += '<input type="text" name="inputLabaRugi" id="inputLabaRugi" class="form-control input-sm angka inputLabaRugi" value="'+(v_nego_min - v_nego).toLocaleString()+'" readonly="readonly" />';
                        hasWhat = 'has-error';
                    }
                } else {
                    htmlLabaRugi += '<input type="text" name="inputLabaRugi" id="inputLabaRugi" class="form-control input-sm angka inputLabaRugi" readonly="readonly" />';
                }
                
                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < satuans.length; j++) {
                        if (satuans[j].id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: satuans[j].id,
                                kode: satuans[j].kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }

                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };
                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                // Ngurus view
                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="jumlah1[]" id="jumlah1-'+kode+'" value="'+(custom_data&&v_satuan_id1>0?v_jumlah1:1)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="jumlah2[]" id="jumlah2-'+kode+'" value="'+(custom_data&&v_satuan_id2>0?v_jumlah2:0)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="satuan1[]" id="satuan1-'+kode+'" value="'+(custom_data&&v_satuan_id1>0?v_satuan_id1:satuan_terkecil.id)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="satuan2[]" id="satuan2-'+kode+'" value="'+(custom_data&&v_satuan_id2>0?v_satuan_id2:satuan_terkecil.id)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi[]" id="konversi-' +kode+'" value="'+satuan_terkecil.konversi+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi1[]" id="konversi1-' +kode+'" value="'+(custom_data&&v_satuan_id1>0?v_konversi1:satuan_terkecil.konversi)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi2[]" id="konversi2-' +kode+'" value="'+(custom_data&&v_satuan_id2>0?v_konversi2:satuan_terkecil.konversi)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" item_kode="'+kode+'" name="diskon[]" id="diskon-' + kode + '" value="'+diskon+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="hpp[]" id="hpp-' + kode + '" value="'+hpp+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga[]" id="harga-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1[]" id="harga1-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1_eceran[]" id="harga1_eceran-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1_grosir[]" id="harga1_grosir-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2[]" id="harga2-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2_eceran[]" id="harga2_eceran-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2_grosir[]" id="harga2_grosir-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga_eceran[]" id="harga_eceran-' + kode + '" value="'+harga_eceran+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga_grosir[]" id="harga_grosir-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="subtotal[]" id="subtotal-'+kode+'" class="subtotal" value="'+(custom_data?v_subtotal:'')+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="nego[]" id="nego-'+kode+'" class="nego" value="'+(custom_data?v_nego:'')+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="nego_min[]" id="nego_min-'+kode+'" class="nego_min" value="'+nego_min+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="stoktotal[]" id="stoktotal-'+kode+'" class="stoktotal" value="'+stoktotal+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="is_grosir[]" id="is_grosir-'+kode+'" class="is_grosir" value="'+is_grosir+'" />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="vertical-align: top; padding-top: 15px">'+nama+'</td>'+
                            '<td id="inputJumlahItemContainer" class="'+(has_error?'has-error':'')+'">'+
                                '<div class="row">'+
                                    '<div class="col-md-6" style="padding-right: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem1" id="inputJumlahItem1" class="form-control input-sm" value="1" />'+
                                            '<div id="pilihSatuan1" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-6" style="padding-left: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem2" id="inputJumlahItem2" class="form-control input-sm" />'+
                                            '<div id="pilihSatuan2" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="padding-left: 0; padding-right: 0;">'+
                                '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + v_harga.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="padding-right: 0;">'+
                                '<div id="inputSubTotalContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                    '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka inputSubTotal" value="' + v_subtotal.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="padding-right: 0;">'+
                                '<div id="inputNegoContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            '<input type="checkbox" name="checkNego" id="checkNego" '+(custom_data&&v_nego>0?'checked="checked"':'')+' />'+
                                        '</div>'+
                                        '<input type="hidden" name="inputNegoMin" id="inputNegoMin" class="form-control input-sm" value="' + v_nego_min + '" />'+(custom_data&&v_nego>0?
                                        '<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" value="'+v_nego.toLocaleString()+'" />':'<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" readonly="readonly" />')+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="padding-right: 0;">'+
                                '<div id="inputLabaRugiContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group '+hasWhat+'" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        // '<input type="text" name="inputLabaRugi" id="inputLabaRugi" class="form-control input-sm angka inputLabaRugi" readonly="readonly" />'+
                                        htmlLabaRugi+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="vertical-align: middle;">'+
                                '<div id="bonusContainer">'+
                                    text_bonus+
                                '</div>'+
                            '</td>'+
                        '</tr>'
                    );
                }

                var tr = $('tr[data-id="'+kode+'"]');
                tr.find('#inputJumlahItem1').val(v_jumlah1);
                tr.find('#pilihSatuan1').find('.text').text(v_satuan_kode1+' ');
                if (v_satuan_id2 != 0) {
                    tr.find('#inputJumlahItem2').val(v_jumlah2);
                    tr.find('#pilihSatuan2').find('.text').text(v_satuan_kode2+' ');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr_stoktotal = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr_stoktotal);

                // Ngurus harga
                if (custom_data) {
                    $('#tabelKeranjang tbody tr:last-child').find('#inputJumlahItem1').trigger('keyup');

                    var harga_total = parseFloat($('input[name="harga_total"]').val());
                    harga_total = (isNaN(harga_total) || harga_total < 0) ? 0 : harga_total;
                    harga_total += v_nego ? v_nego : v_subtotal;
                    $('input[name="harga_total"]').val(harga_total);

                    var $nego_total_min = $('#inputNegoTotalMin');
                    var nego_total_min = parseFloat($nego_total_min.val());
                    nego_total_min = (isNaN(nego_total_min) || nego_total_min < 0) ? 0 : nego_total_min;
                    nego_total_min = nego_total_min + nego_min * rtp_item.jumlah;
                    $nego_total_min.val(nego_total_min);

                    if (custom_data_last_item) {
                        var nego_total = parseFloat(transaksi_penjualan.nego_total);
                        nego_total = (isNaN(nego_total) || nego_total < 0) ? 0 : nego_total;
                        // $('input[name="nego_total"]').val(nego_total);

                        // var nego_total_data = parseFloat($('input[name="nego_total"]'));
                        // nego_total_data = (isNaN(nego_total_data) || nego_total_data < 0) ? 0 : nego_total_data;
                        var nego_total_data = 0;
                        for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                            var relasi = relasi_transaksi_penjualan[i];
                            nego_total_data += parseFloat(relasi.nego);
                        }

                        if (nego_total < nego_total_data) {
                            $('#inputNegoTotal').val(nego_total.toLocaleString());
                            $('#checkNegoTotal').prop('checked', true);
                            $('#checkNegoTotal').trigger('change');
                        }
                    }
                } else {
                    var $harga_total = $('#inputHargaTotal');
                    var $nego_total_min = $('#inputNegoTotalMin');

                    var harga_total = parseFloat($harga_total.val());
                    var nego_total_min = parseFloat($nego_total_min.val());

                    harga_total = (isNaN(harga_total) || harga_total < 0) ? 0 : harga_total;
                    nego_total_min = (isNaN(nego_total_min) || nego_total_min < 0) ? 0 : nego_total_min;

                    $('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + harga_grosir);
                    $nego_total_min.val(nego_total_min + nego_min);
                }

                tr.find('#inputJumlahItem1').trigger('keyup');

                updateHargaOnKeyup();

                $('#spinner').hide();
            });
        }

        $(document).ready(function() {
            var url = "{{ url('po-penjualan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            banks = '{{ $banks }}';
            banks = banks.replace(/&quot;/g, '"');
            banks = JSON.parse(banks);

            users = '{{ $users }}';
            users = users.replace(/&quot;/g, '"');
            users = JSON.parse(users);

            items = '{{ json_encode($items) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);
            for (var i = 0; i < items.length; i++) {
                kode_items.push(items[i].kode);
            }

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            pelanggans = '{{ $pelanggans }}';
            pelanggans = pelanggans.replace(/&quot;/g, '"');
            pelanggans = JSON.parse(pelanggans);

            bonus_rules = '{{ $bonus_rules }}';
            bonus_rules = bonus_rules.replace(/&quot;/g, '"');
            bonus_rules = JSON.parse(bonus_rules);

            transaksi_penjualan = '{{ $transaksi_penjualan }}';
            transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
            transaksi_penjualan = JSON.parse(transaksi_penjualan);

            relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            $('input[name="pelanggan"]').val('');
            $('input[name="kode_pelanggan"]').val('');
            $('input[name="nama_pelanggan"]').val('');
            $('input[name="level_pelanggan"]').val('');
            $('input[name="lewat_jatuh_tempo"]').val(0);
            $('input[name="limit_piutang"]').val('');
            $('input[name="batas_piutang"]').val('');
            $('input[name="titipan"]').val('');
            $('input[name="grosir"]').val('false');

            $('input[name="harga_total"]').val('');
            $('input[name="harga_akhir"]').val('');
            $('input[name="nego_total"]').val('');
            $('input[name="nego_total_min"]').val('');
            $('input[name="nego_total_view"]').val('');
            $('input[name="potongan_penjualan"]').val('');
            $('input[name="ongkos_kirim"]').val('');
            $('input[name="jumlah_bayar"]').val('');
            $('input[name="kembali"]').val('');
            $('input[name="check_nego_total"]').val(0);

            $('input[name="nominal_tunai"]').val('');
            $('input[name="no_transfer"]').val('');
            $('input[name="bank_transfer"]').val('');
            $('input[name="nominal_transfer"]').val('');
            $('input[name="no_kartu"]').val('');
            $('input[name="bank_kartu"]').val('');
            $('input[name="jenis_kartu"]').val('');
            $('input[name="nominal_kartu"]').val('');
            $('input[name="no_cek"]').val('');
            $('input[name="nominal_cek"]').val('');
            $('input[name="no_bg"]').val('');
            $('input[name="nominal_bg"]').val('');
            $('input[name="nominal_titipan"]').val('');
            $('input[name="pengirim"]').val('');
            $('input[name="pengirim_lain"]').val('');

            $(".select2_single").select2({
                width: '100%'
            });

            // $('#pilihPelanggan').val(transaksi_penjualan.pelanggan_id).trigger('change');
            // $("#pilihPelanggan").select2('val', transaksi_penjualan.pelanggan_id+'');
            // handlePelangganChange(transaksi_penjualan.pelanggan_id);

            if (transaksi_penjualan.pelanggan_id != null) {
                // $('#pilihPelanggan').val(transaksi_penjualan.pelanggan_id).trigger('change');
                $("#pilihPelanggan").select2('val', transaksi_penjualan.pelanggan_id+'');
                handlePelangganChange(transaksi_penjualan.pelanggan_id);
                // console.log('ready');
            }

            if (transaksi_penjualan.pengirim == null) {
                $('#btnDiambil').trigger('click');
            } else {
                $('#btnDikirim').trigger('click');
                var user_id = '-';
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    if (transaksi_penjualan.pengirim == user.nama) {
                        user_id = user.id;
                        break;
                    }
                }
                if (user_id == '-') {
                    // pengirim lain
                    $('select[name="user_id"]').val('-').trigger('change');
                    $('select[name="user_id"]').select2('val', '-');
                    $('#inputPengirimLain').prop('disabled', false);

                    $('#inputPengirimLain').val(transaksi_penjualan.pengirim);
                    $('input[name="pengirim_lain"]').val(transaksi_penjualan.pengirim);
                } else {
                    // pengirim dari karyawan
                    $('#inputPengirimLain').prop('disabled', true);
                    $('input[name="pengirim"]').val(user_id);
                    $('input[name="pengirim_lain"]').val('');
                }
                $('select[name="user_id"]').select2('val', user_id+'');
                updateHargaOnKeyup();
            }

            if (transaksi_penjualan.alamat == null) {
                $('#btnAlamatBiasa').trigger('click');
            } else {
                $('#btnAlamatLain').trigger('click');
            }

            for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                var relasi = relasi_transaksi_penjualan[i];
                // rtp_nego_total += parseFloat(relasi.nego);

                if (i == relasi_transaksi_penjualan.length - 1) custom_data_last_item = true;

                handleItemChange(relasi.item_kode, true);
            }

            // // var v_harga_total = 0;
            // // var tp_harga_total = parseFloat(transaksi_penjualan.harga_total);
            // var tp_nego_total = parseFloat(transaksi_penjualan.nego_total);
            // var rtp_nego_total = 0;
            // for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
            //     var relasi = relasi_transaksi_penjualan[i];
            //     rtp_nego_total += parseFloat(relasi.nego);

            //     handleItemChange(relasi.item_kode, true);
            // }

            // if (tp_nego_total < rtp_nego_total) {
            //     $('#checkNegoTotal').prop('checked', true).trigger('change');
            //     $('#inputNegoTotal').val(tp_nego_total.toLocaleString());
            //     $('input[name="nego_total_view"]').val(tp_nego_total);
            //     updateHargaOnKeyup();
            // } else {
            //     $('#checkNegoTotal').prop('checked', true).trigger('change');
            // }

            $('input[name="kode_transaksi"]').val(transaksi_penjualan.kode_transaksi);
            // $('#kodeTransaksiTitle').text(transaksi_penjualan.kode_transaksi);

            // $('#pilihanPotonganContainer').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTunaiContainer').find('input').val('');
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputTransferBankContainer').find('select').val('').trigger('change');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('');
            $('#inputKartuContainer').find('select').val('').trigger('change');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputDepositoContainer').hide();
            $('#inputDepositoContainer').find('input').val('');

            var harga_total = transaksi_penjualan.harga_total;
            var nego_total = transaksi_penjualan.nego_total;
            var check_nego_total = transaksi_penjualan.check_nego_total;
            // var potongan_penjualan = transaksi_penjualan.potongan_penjualan;
            var ongkos_kirim = transaksi_penjualan.ongkos_kirim;
            var jumlah_bayar = transaksi_penjualan.jumlah_bayar;
            var nominal_tunai = transaksi_penjualan.nominal_tunai;
            var no_transfer = transaksi_penjualan.no_transfer;
            var bank_transfer = transaksi_penjualan.bank_transfer;
            var nominal_transfer = transaksi_penjualan.nominal_transfer;
            var no_kartu = transaksi_penjualan.no_kartu;
            var nominal_kartu = transaksi_penjualan.nominal_kartu;
            var no_cek = transaksi_penjualan.no_cek;
            var nominal_cek = transaksi_penjualan.nominal_cek;
            var no_bg = transaksi_penjualan.no_bg;
            var nominal_bg = transaksi_penjualan.nominal_bg;
            var jatuh_tempo = transaksi_penjualan.jatuh_tempo;
            var nominal_titipan = transaksi_penjualan.nominal_titipan;

            harga_total = harga_total ? parseFloat(transaksi_penjualan.harga_total) : 0;
            nego_total = nego_total ? parseFloat(transaksi_penjualan.nego_total) : 0;
            check_nego_total = check_nego_total ? parseFloat(transaksi_penjualan.check_nego_total) : 0;
            // potongan_penjualan = potongan_penjualan ? parseFloat(transaksi_penjualan.potongan_penjualan) : 0;
            ongkos_kirim = ongkos_kirim ? parseFloat(transaksi_penjualan.ongkos_kirim) : 0;
            jumlah_bayar = jumlah_bayar ? parseFloat(transaksi_penjualan.jumlah_bayar) : 0;
            nominal_tunai = nominal_tunai ? parseFloat(transaksi_penjualan.nominal_tunai) : 0;
            nominal_transfer = nominal_transfer ? parseFloat(transaksi_penjualan.nominal_transfer) : 0;
            nominal_kartu = nominal_kartu ? parseFloat(transaksi_penjualan.nominal_kartu) : 0;
            nominal_cek = nominal_cek ? parseFloat(transaksi_penjualan.nominal_cek) : 0;
            nominal_bg = nominal_bg ? parseFloat(transaksi_penjualan.nominal_bg) : 0;
            nominal_titipan = nominal_titipan ? parseFloat(transaksi_penjualan.nominal_titipan) : 0;

            // if (jumlah_bayar > harga_total) {
                nominal_tunai = jumlah_bayar - nominal_transfer - nominal_kartu - nominal_cek - nominal_bg - nominal_titipan;
            // }

            $('#btnTunai').trigger('click');
            // $('#inputNominalTunai').trigger('focus');
            if (nominal_tunai > 0) {
                // Buka Metode Pembayaran Tunai
                $('#inputNominalTunai').val(nominal_tunai.toLocaleString());
                // Set nominal_tunai
                $('input[name="nominal_tunai"]').val(nominal_tunai);
            }

            if (nominal_transfer > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnTransfer').trigger('click');
                $('#inputNoTransfer').val(no_transfer);
                $('#inputNominalTransfer').val(nominal_transfer.toLocaleString());
                $('select[name="bank_id"]').val(bank_transfer).trigger('change');
                // Set nominal_transfer
                $('input[name="no_transfer"]').val(no_transfer);
                $('input[name="nominal_transfer"]').val(nominal_transfer);
            }

            if (nominal_kartu > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnKartu').trigger('click');
                $('#inputNoKartu').val(no_kartu);
                $('#inputNominalKartu').val(nominal_kartu.toLocaleString());
                // Set nominal_kartu
                $('input[name="no_kartu"]').val(no_kartu);
                $('input[name="nominal_kartu"]').val(nominal_kartu);
            }

            if (nominal_cek > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnCek').trigger('click');
                $('#inputNoCek').val(no_cek);
                $('#inputNominalCek').val(nominal_cek.toLocaleString());
                // Set nominal_cek
                $('input[name="no_cek"]').val(no_cek);
                $('input[name="nominal_cek"]').val(nominal_cek);
            }

            if (nominal_bg > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnBG').trigger('click');
                $('#inputNoBG').val(no_bg);
                $('#inputNominalBG').val(nominal_bg.toLocaleString());
                $('#inputJatuhTempo').val(ymd2dmy(jatuh_tempo));
                // Set nominal_bg
                $('input[name="no_bg"]').val(no_bg);
                $('input[name="nominal_bg"]').val(nominal_bg);
                $('input[name="jatuh_tempo"]').val(jatuh_tempo);
            }

            if (nominal_titipan > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnDeposito').trigger('click');
                $('#inputNominalDeposito').val(nominal_titipan.toLocaleString());
                // Set nominal_titipan
                $('input[name="nominal_titipan"]').val(nominal_titipan);
            }

            // console.log('oi');
            if (jumlah_bayar > 0) {
                $('#inputJumlahBayar').val(jumlah_bayar.toLocaleString());
                $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            }

            if (nego_total > 0 && nego_total < harga_total) {
                $('#checkNegoTotal').prop('checked', true).trigger('change');
                $('#inputNegoTotal').val(nego_total.toLocaleString()).trigger('keyup');
                $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            }

            if (ongkos_kirim > 0) {
                $('#inputOngkosKirim').val(ongkos_kirim.toLocaleString());
                $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            }

            if (check_nego_total > 0) {
                $('#checkNegoTotal').prop('checked', true);
                $('#inputNegoTotal').prop('readonly', false);
                $('input[name="check_nego_total"]').val(1);
            } else {
                $('#checkNegoTotal').prop('checked', false);
                $('#inputNegoTotal').prop('readonly', true);
                $('#inputNegoTotal').val('');
                $('input[name="check_nego_total"]').val(0);
            }

            updateHargaOnKeyup();

            $('#spinner').hide();
        });

        // var keyupFromScanner = false;
        $(document).scannerDetection({
            avgTimeByChar: 40,
            onComplete: function(code, qty) {
                // console.log('Kode: ' + code, qty);
            },
            onError: function(error) {
                var kode = error;
                if (kode_items.includes(kode)) {
                    handleItemChange(kode, false);

                    $('#checkNegoTotal').prop('checked', false);
                    $('#checkNegoTotal').trigger('change');
                }
            }
        });

        $(document).keypress(function(e) {
            if (e.key == 'Enter') {
                $('#btnSimpanPO').trigger('click');
            }
        });

        $(document).on('change', 'select[name="pelanggan_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('#pilihPelanggan').val(id);
            handlePelangganChange(id);
        });

        $(document).on('click', '#resetPelanggan', function(event) {
            event.preventDefault();

            pelanggan = null;
            $('#btnAlamatBiasa').trigger('click');
            $('select[name="pelanggan_id"]').val('');
            $('select[name="pelanggan_id"]').trigger('change');
            $('input[name="kode_pelanggan"]').val('');
            $('input[name="kode_pelanggan"]').removeClass('has-error');
            $('input[name="nama_pelanggan"]').val('');
            $('#inputNamaPelanggan').prop('readonly', false);
            if ($('#btnDeposito').hasClass('btn-purple')) $('#btnDeposito').trigger('click');
            $('#btnDeposito').prop('disabled', true);
            $('#jumlahDeposito').text('');
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputKodePelanggan', function(event) {
            event.preventDefault();

            var ada_yang_sama = false;
            var kode_pelanggan = $(this).val();

            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }

            for (var i = 0; i < pelanggans.length; i++) {
                var pelanggan = pelanggans[i];
                if (kode_pelanggan == pelanggan.kode) {
                    ada_yang_sama = true;
                    break;
                }
            }

            if (ada_yang_sama) {
                $(this).parents('.form-group').addClass('has-error');
            } else {
                $(this).parents('.form-group').removeClass('has-error');
                $(this).val(kode_pelanggan);
                $('input[name="kode_pelanggan"]').val(kode_pelanggan);
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNamaPelanggan', function(event) {
            event.preventDefault();

            var nama_pelanggan = $(this).val();
            $('input[name="nama_pelanggan"]').val(nama_pelanggan);
            
            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var kode = $(this).val();
            handleItemChange(kode, false);

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');
        });

        /*var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah);
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var jumlah = $(this).val();
            temp_jumlah = jumlah;

            var kode = $(this).parents('tr').first().data('id');
            var satuan = $('#satuan-'+kode).val();
            var konversi = $('#konversi-'+kode).val();
            var stoktotal = $('#stoktotal-'+kode).val();
            
            var td = $(this).parents('td').first();

            if (jumlah == '') {
                jumlah = 0;
                td.addClass('has-error');
            } else {
                td.removeClass('has-error');
            }

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            satuan = parseFloat(satuan);
            if (isNaN(satuan) || satuan <= 0) satuan = 0;
            konversi = parseFloat(konversi);
            if (isNaN(konversi) || konversi <= 0) konversi = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir-vip') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.next().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#jumlah-'+kode).val(jumlah);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        var temp_jumlah_1 = 0;
        $(document).on('click', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_1 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah_1);
        });

        $(document).on('keyup', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_1 = $(this).val();

            var jumlah1 = parseFloat($(this).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah1-'+item_kode).val(jumlah1);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            // var harga = parseFloat(data.harga.grosir);
                            // var konversi = parseFloat(data.konversi);
                            // var harga_eceran = parseFloat(data.harga_eceran);
                            // var harga_grosir = parseFloat(data.harga_grosir);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = parseFloat(harga_darurat.grosir);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = parseFloat(data.harga.grosir);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            // cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            // if (is_grosir) updateHargaKeGrosir();
                            // else updateHargaKeEceran();

                            var harga1 = 0;
                            var harga2 = 0;

                            if (data.harga1 != null) {
                                harga1 = parseFloat(data.harga1.grosir);
                                if (isNaN(harga1)) harga1 = 0;
                            }
                            if (data.harga2 != null) {
                                harga2 = parseFloat(data.harga2.grosir);
                                if (isNaN(harga2)) harga2 = 0;
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            updateHargaKeGrosir();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    var jumlah_bonus = bonus[i].jumlah;

                                    if (bonus[i].bonus.stoktotal > 0) {
                                        if(bonus[i].jumlah >= bonus[i].bonus.stoktotal) jumlah_bonus = bonus[i].bonus.stoktotal;
                                        // text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        // if (i != bonus.length - 1) text_bonus += ', ';
                                        
                                        /* topik signature */
                                        var nama_bonus = bonus[i].bonus.nama;
                                        var satuan_item = [];
                                        for (var j = 0; j < bonus[i].bonus.satuan_pembelians.length; j++) {
                                            var satuan = {
                                                kode: bonus[i].bonus.satuan_pembelians[j].satuan.kode,
                                                konversi: bonus[i].bonus.satuan_pembelians[j].konversi
                                            }
                                            satuan_item.push(satuan);
                                        }

                                        var text_jumlah = '-';
                                        var temp_jumlah = jumlah_bonus;
                                        for (var k = 0; k < satuan_item.length; k++) {
                                            if (temp_jumlah > 0) {
                                                var satuan = satuan_item[k];
                                                var jumlah_stok = parseInt(temp_jumlah / satuan.konversi);
                                                if (jumlah_stok > 0) {
                                                    if (text_jumlah == '-') text_jumlah = '';
                                                    text_jumlah += jumlah_stok;
                                                    text_jumlah += ' ';
                                                    text_jumlah += satuan.kode;

                                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                                    if (k != satuan_item.length - 1 && temp_jumlah > 0) text_jumlah += ' ';
                                                }
                                            }
                                        }

                                        text_bonus += text_jumlah+' '+nama_bonus;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                        /* topik signature */
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * jumlahtotal / 100);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        var temp_jumlah_2 = 0;
        $(document).on('click', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_2 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah_2);
        });

        $(document).on('keyup', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_2 = $(this).val();

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($(this).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah2-'+item_kode).val(jumlah2);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            // var harga = parseFloat(data.harga.grosir);
                            // var konversi = parseFloat(data.konversi);
                            // var harga_eceran = parseFloat(data.harga_eceran);
                            // var harga_grosir = parseFloat(data.harga_grosir);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = parseFloat(harga_darurat.grosir);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = parseFloat(data.harga.grosir);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            // if (is_grosir) updateHargaKeGrosir();
                            // else updateHargaKeEceran();

                            var harga1 = 0;
                            var harga2 = 0;

                            if (data.harga1 != null) {
                                harga1 = parseFloat(data.harga1.grosir);
                                if (isNaN(harga1)) harga1 = 0;
                            }
                            if (data.harga2 != null) {
                                harga2 = parseFloat(data.harga2.grosir);
                                if (isNaN(harga2)) harga2 = 0;
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            updateHargaKeGrosir();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    var jumlah_bonus = bonus[i].jumlah;

                                    if (bonus[i].bonus.stoktotal > 0) {
                                        if(bonus[i].jumlah >= bonus[i].bonus.stoktotal) jumlah_bonus = bonus[i].bonus.stoktotal;
                                        // text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        // if (i != bonus.length - 1) text_bonus += ', ';
                                        
                                        /* topik signature */
                                        var nama_bonus = bonus[i].bonus.nama;
                                        var satuan_item = [];
                                        for (var j = 0; j < bonus[i].bonus.satuan_pembelians.length; j++) {
                                            var satuan = {
                                                kode: bonus[i].bonus.satuan_pembelians[j].satuan.kode,
                                                konversi: bonus[i].bonus.satuan_pembelians[j].konversi
                                            }
                                            satuan_item.push(satuan);
                                        }

                                        var text_jumlah = '-';
                                        var temp_jumlah = jumlah_bonus;
                                        for (var k = 0; k < satuan_item.length; k++) {
                                            if (temp_jumlah > 0) {
                                                var satuan = satuan_item[k];
                                                var jumlah_stok = parseInt(temp_jumlah / satuan.konversi);
                                                if (jumlah_stok > 0) {
                                                    if (text_jumlah == '-') text_jumlah = '';
                                                    text_jumlah += jumlah_stok;
                                                    text_jumlah += ' ';
                                                    text_jumlah += satuan.kode;

                                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                                    if (k != satuan_item.length - 1 && temp_jumlah > 0) text_jumlah += ' ';
                                                }
                                            }
                                        }

                                        text_bonus += text_jumlah+' '+nama_bonus;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                        /* topik signature */
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * jumlahtotal / 100);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        $(document).on('click', '#pilihSatuan1 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan1').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($(this).find('a').attr('konversi'));
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan1-'+item_kode).val(satuan1.id);
                $('#konversi1-'+item_kode).val(satuan1.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;
                        
                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            // var harga = parseFloat(data.harga.grosir);
                            // var konversi = parseFloat(data.konversi);
                            // var harga_eceran = parseFloat(data.harga_eceran);
                            // var harga_grosir = parseFloat(data.harga_grosir);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = parseFloat(harga_darurat.grosir);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = parseFloat(data.harga.grosir);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            // if (is_grosir) updateHargaKeGrosir();
                            // else updateHargaKeEceran();

                            var harga1 = 0;
                            var harga2 = 0;

                            if (data.harga1 != null) {
                                harga1 = parseFloat(data.harga1.grosir);
                                if (isNaN(harga1)) harga1 = 0;
                            }
                            if (data.harga2 != null) {
                                harga2 = parseFloat(data.harga2.grosir);
                                if (isNaN(harga2)) harga2 = 0;
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            updateHargaKeGrosir();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    var jumlah_bonus = bonus[i].jumlah;

                                    if (bonus[i].bonus.stoktotal > 0) {
                                        if(bonus[i].jumlah >= bonus[i].bonus.stoktotal) jumlah_bonus = bonus[i].bonus.stoktotal;
                                        // text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        // if (i != bonus.length - 1) text_bonus += ', ';
                                        
                                        /* topik signature */
                                        var nama_bonus = bonus[i].bonus.nama;
                                        var satuan_item = [];
                                        for (var j = 0; j < bonus[i].bonus.satuan_pembelians.length; j++) {
                                            var satuan = {
                                                kode: bonus[i].bonus.satuan_pembelians[j].satuan.kode,
                                                konversi: bonus[i].bonus.satuan_pembelians[j].konversi
                                            }
                                            satuan_item.push(satuan);
                                        }

                                        var text_jumlah = '-';
                                        var temp_jumlah = jumlah_bonus;
                                        for (var k = 0; k < satuan_item.length; k++) {
                                            if (temp_jumlah > 0) {
                                                var satuan = satuan_item[k];
                                                var jumlah_stok = parseInt(temp_jumlah / satuan.konversi);
                                                if (jumlah_stok > 0) {
                                                    if (text_jumlah == '-') text_jumlah = '';
                                                    text_jumlah += jumlah_stok;
                                                    text_jumlah += ' ';
                                                    text_jumlah += satuan.kode;

                                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                                    if (k != satuan_item.length - 1 && temp_jumlah > 0) text_jumlah += ' ';
                                                }
                                            }
                                        }

                                        text_bonus += text_jumlah+' '+nama_bonus;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                        /* topik signature */
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * jumlahtotal / 100);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan1.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            button.text(satuan1.kode+' ');
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        $(document).on('click', '#pilihSatuan2 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan2').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($(this).find('a').attr('konversi'));
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah2 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan2-'+item_kode).val(satuan2.id);
                $('#konversi2-'+item_kode).val(satuan2.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            // var harga = parseFloat(data.harga.grosir);
                            // var konversi = parseFloat(data.konversi);
                            // var harga_eceran = parseFloat(data.harga_eceran);
                            // var harga_grosir = parseFloat(data.harga_grosir);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = parseFloat(harga_darurat.grosir);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = parseFloat(data.harga.grosir);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
                            // if (is_grosir) updateHargaKeGrosir();
                            // else updateHargaKeEceran();

                            var harga1 = 0;
                            var harga2 = 0;

                            if (data.harga1 != null) {
                                harga1 = parseFloat(data.harga1.grosir);
                                if (isNaN(harga1)) harga1 = 0;
                            }
                            if (data.harga2 != null) {
                                harga2 = parseFloat(data.harga2.grosir);
                                if (isNaN(harga2)) harga2 = 0;
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            updateHargaKeGrosir();

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    var jumlah_bonus = bonus[i].jumlah;

                                    if (bonus[i].bonus.stoktotal > 0) {
                                        if(bonus[i].jumlah >= bonus[i].bonus.stoktotal) jumlah_bonus = bonus[i].bonus.stoktotal;
                                        // text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        // if (i != bonus.length - 1) text_bonus += ', ';
                                        
                                        /* topik signature */
                                        var nama_bonus = bonus[i].bonus.nama;
                                        var satuan_item = [];
                                        for (var j = 0; j < bonus[i].bonus.satuan_pembelians.length; j++) {
                                            var satuan = {
                                                kode: bonus[i].bonus.satuan_pembelians[j].satuan.kode,
                                                konversi: bonus[i].bonus.satuan_pembelians[j].konversi
                                            }
                                            satuan_item.push(satuan);
                                        }

                                        var text_jumlah = '-';
                                        var temp_jumlah = jumlah_bonus;
                                        for (var k = 0; k < satuan_item.length; k++) {
                                            if (temp_jumlah > 0) {
                                                var satuan = satuan_item[k];
                                                var jumlah_stok = parseInt(temp_jumlah / satuan.konversi);
                                                if (jumlah_stok > 0) {
                                                    if (text_jumlah == '-') text_jumlah = '';
                                                    text_jumlah += jumlah_stok;
                                                    text_jumlah += ' ';
                                                    text_jumlah += satuan.kode;

                                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                                    if (k != satuan_item.length - 1 && temp_jumlah > 0) text_jumlah += ' ';
                                                }
                                            }
                                        }

                                        text_bonus += text_jumlah+' '+nama_bonus;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                        /* topik signature */
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * jumlahtotal / 100);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan2.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            button.text(satuan2.kode+' ');
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        /*$(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();

            var satuan = $(this).val();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            var kode = $tr.data('id');
            var stoktotal = $('#stoktotal-'+kode).val();
            var jumlah = $('#jumlah-'+kode).val();
            // var jumlah = $(this).parent().prev().children('input').val();

            if (jumlah === '') jumlah = 0;

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir-vip') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var td = $(this).parents('td').first();

            td.prev().find('#inputJumlahItem').val(jumlah.toLocaleString());

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var konversi = data.konversi.konversi;
                    konversi = parseFloat(konversi);
                    if (isNaN(konversi) || konversi <= 0) konversi = 0;

                    var jumlahtotal = jumlah * konversi;
                    if (jumlahtotal <= stoktotal) {
                        td.removeClass('has-error');
                        td.prev().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#satuan-'+kode).val(satuan);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        $(document).on('change', '#checkNego', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            var tr = $(this).parents('tr').first();
            var kode = tr.data('id');
            var harga_total = 0;

            if (checked) {
                tr.find('#inputNego').prop('readonly', false).focus();

                var nego = tr.find('#inputNego').val();
                var nego_min = tr.find('#inputNegoMin').val();

                nego = parseFloat(nego.replace(/\D/g, ''), 10);
                if (isNaN(nego)) nego = 0;

                nego_min = parseFloat(nego_min);
                if (isNaN(nego_min)) nego_min = 0;

                var laba_rugi = nego - nego_min;
                if (isNaN(laba_rugi)) laba_rugi = 0;
                // console.log(nego, nego_min, laba_rugi);

                // Success
                if (nego >= nego_min) {
                    tr.find('#inputLabaRugiContainer').parent().removeClass('has-error');
                    tr.find('#inputLabaRugiContainer').parent().addClass('has-success');
                    tr.find('#inputLabaRugi').val(laba_rugi.toLocaleString());
                } else {
                    tr.find('#inputLabaRugiContainer').parent().removeClass('has-success');
                    tr.find('#inputLabaRugiContainer').parent().addClass('has-error');
                    tr.find('#inputLabaRugi').val(laba_rugi.toLocaleString());

                    tr.find('#inputNego').val('');
                }

                $('#nego-'+kode).val(nego);
                // updateHargaTotal();
                updateHargaOnKeyup();
            } else {
                tr.find('#inputLabaRugiContainer').parent().removeClass('has-success');
                tr.find('#inputLabaRugiContainer').parent().removeClass('has-error');
                tr.find('#inputLabaRugi').val('');

                tr.find('#inputNego').val('');
                tr.find('#inputNego').prop('readonly', true);

                $('#nego-'+kode).val('');
                
                $('#inputNegoContainer').removeClass('has-error')

                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNego', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();;
            var kode = tr.data('id');
            var nego = $(this).val();
            var nego_min = tr.find('#inputNegoMin').val();

            nego = parseFloat(nego.replace(/\D/g, ''), 10);
            if (isNaN(nego)) nego = 0;

            nego_min = parseFloat(nego_min);
            if (isNaN(nego_min)) nego_min = 0;

            var laba_rugi = nego - nego_min;
            if (isNaN(laba_rugi)) laba_rugi = 0;

            var is_nego_ratusan = nego % 100 == 0;
            if (is_nego_ratusan) {
                // Success
                if (nego >= nego_min) {
                    tr.find('#inputLabaRugiContainer').parent().removeClass('has-error');
                    tr.find('#inputLabaRugiContainer').parent().addClass('has-success');
                    tr.find('#inputLabaRugi').val(laba_rugi.toLocaleString());
                    // $('#nego-'+kode).val(nego);
                    updateHargaOnKeyup();
                } else {
                    tr.find('#inputLabaRugiContainer').parent().removeClass('has-success');
                    tr.find('#inputLabaRugiContainer').parent().addClass('has-error');
                    tr.find('#inputLabaRugi').val(laba_rugi.toLocaleString());
                    // $('#nego-'+kode).val('');
                    updateHargaOnKeyup();
                }

                $(this).parents('#inputNegoContainer').removeClass('has-error');

                $('#nego-'+kode).val(nego);
                // updateHargaTotal();
                updateHargaOnKeyup();
            } else {
                $(this).parents('#inputNegoContainer').addClass('has-error');
                if (laba_rugi < 0) {
                    // laba_rugi_total *= -1;
                     tr.find('#inputLabaRugiContainer').parent().removeClass('has-success');
                     tr.find('#inputLabaRugiContainer').parent().addClass('has-error');
                } else {
                     tr.find('#inputLabaRugiContainer').parent().removeClass('has-error');
                     tr.find('#inputLabaRugiContainer').parent().addClass('has-success');
                }
                tr.find('#inputLabaRugi').val(laba_rugi.toLocaleString());
                // $('#nego-'+kode).val('');

                $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
                updateHargaOnKeyup();
            }
        });

        $(document).on('click', '#btnPotonganPersen', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).next().val(true);

                $(this).parent().next().find('button').removeClass('btn-primary');
                $(this).parent().next().find('button').addClass('btn-default');
                $(this).parent().next().find('button').next().val(false);

                $.get(url, function(data) {
                    var persen = data.pelanggan.diskon_persen;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total = harga_total - (harga_total * (persen/100));

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('click', '#btnPotonganTunai', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;
            
            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).next().val(true);

                $(this).parent().prev().find('button').removeClass('btn-danger');
                $(this).parent().prev().find('button').addClass('btn-default');
                $(this).parent().prev().find('button').next().val(false);

                $.get(url, function(data) {
                    var potongan = data.pelanggan.potongan;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total -= potongan;

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('change', '#checkNegoTotal', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('input[name="check_nego_total"]').val(1);
                $('#inputNegoTotal').prop('readonly', false).focus();

                var nego_total = $('#inputNegoTotal').val();
                var nego_total_min = $('#inputNegoTotalMin').val();

                nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
                if (isNaN(nego_total)) nego_total = 0;

                nego_total_min = parseFloat(nego_total_min);
                if (isNaN(nego_total_min)) nego_total_min = 0;

                var laba_rugi_total = nego_total - nego_total_min;
                if (isNaN(laba_rugi_total)) laba_rugi_total = 0;

                // Success
                if (nego_total >= nego_total_min) {
                    // $('#labelLabaTotal').show();
                    // $('#labelRugiTotal').hide();
                    // $('#inputLabaRugiTotalContainer').parent().removeClass('has-error');
                    // $('#inputLabaRugiTotalContainer').parent().addClass('has-success');
                    // $('#inputLabaRugiTotal').val(laba_rugi_total.toLocaleString());

                    $('input[name="nego_total_view"]').val(nego_total);

                    // updateHargaOnKeyup();
                } else {
                    // $('#labelLabaTotal').hide();
                    // $('#labelRugiTotal').show();
                    // $('#inputLabaRugiTotalContainer').parent().removeClass('has-success');
                    // $('#inputLabaRugiTotalContainer').parent().addClass('has-error');
                    // $('#inputLabaRugiTotal').val(laba_rugi_total.toLocaleString());

                    $('input[name="nego_total_view"]').val('');

                    // updateHargaOnKeyup();
                }
                if($('#inputNegoContainer').hasClass('has-error')) $('#inputNegoContainer').removeClass('has-error')
                updateHargaOnKeyup();
            } else {
                $('input[name="check_nego_total"]').val(0);
                // $('#labelLabaTotal').hide();
                // $('#labelRugiTotal').hide();
                $(this).parents('.input-group').removeClass('has-error');
                $('#inputLabaRugiTotalContainer').parent().removeClass('has-success');
                $('#inputLabaRugiTotalContainer').parent().removeClass('has-error');
                $('#inputLabaRugiTotal').val('');

                $('#inputNegoTotal').val('');
                $('#inputNegoTotal').prop('readonly', true);

                // var harga_total = $('#inputHargaTotal').val();
                // harga_total = parseFloat(harga_total.replace(/\D/g, ''), 10);
                $('input[name="nego_total_view"]').val('');
                
                $('#negoSubTotal').removeClass('has-error')
                
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNegoTotal', function(event) {
            event.preventDefault();

            var nego_total = $('#inputNegoTotal').val();
            var nego_total_min = $('#inputNegoTotalMin').val();

            nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
            if (isNaN(nego_total)) nego_total = 0;

            nego_total_min = parseFloat(nego_total_min);
            if (isNaN(nego_total_min)) nego_total_min = 0;

            var laba_rugi_total = nego_total - nego_total_min;
            if (isNaN(laba_rugi_total)) laba_rugi_total = 0;

            var is_nego_ratusan = nego_total % 100 == 0;
            if (is_nego_ratusan) {
                // Success
                if (nego_total >= nego_total_min) {
                    // $('#labelLabaTotal').show();
                    // $('#labelRugiTotal').hide();
                    $('#inputLabaRugiTotalContainer').parent().removeClass('has-error');
                    $('#inputLabaRugiTotalContainer').parent().addClass('has-success');
                    $('#inputLabaRugiTotal').val(laba_rugi_total.toLocaleString());
                    // $('input[name="nego_total_view"]').val(nego_total);
                    updateHargaOnKeyup();
                } else {
                    // $('#labelLabaTotal').hide();
                    // $('#labelRugiTotal').show();
                    $('#inputLabaRugiTotalContainer').parent().removeClass('has-success');
                    $('#inputLabaRugiTotalContainer').parent().addClass('has-error');
                    $('#inputLabaRugiTotal').val(laba_rugi_total.toLocaleString());
                    // $('input[name="nego_total_view"]').val('');
                    updateHargaOnKeyup();
                }

                $(this).parents('.form-group').removeClass('has-error');

                $('input[name="nego_total_view"]').val(nego_total);
                updateHargaOnKeyup();
            } else {
                // $('#labelLabaTotal').hide();
                // $('#labelRugiTotal').show();
                $(this).parents('.form-group').addClass('has-error');
                if (laba_rugi_total < 0) {
                    // laba_rugi_total *= -1;
                    $('#inputLabaRugiTotalContainer').parent().removeClass('has-success');
                    $('#inputLabaRugiTotalContainer').parent().addClass('has-error');
                } else {
                    $('#inputLabaRugiTotalContainer').parent().removeClass('has-error');
                    $('#inputLabaRugiTotalContainer').parent().addClass('has-success');
                }
                $('#inputLabaRugiTotal').val(laba_rugi_total.toLocaleString());
                // $('input[name="nego_total_view"]').val('');

                $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
                updateHargaOnKeyup();
            }

            // var hpp_total = 0;
            // $('.hpp').each(function(index, el) {
            //  var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
            //  if (isNaN(tmp)) tmp = 0;
            //  hpp_total += tmp;
            // });
            // var nego_min = hpp_total + (hpp_total/10);

            // if (hpp_total > nego_min) {
            //  input.parents('.form-group').addClass('has-error');
            // } else {
            //  input.parents('.form-group').removeClass('has-error');

            //  $('#inputHargaTotal').val(nego_total.toLocaleString());
            //  $('input[name="harga_total"]').val(nego_total);

            //  var jumlah_bayar = parseInt($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            //  var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            //  var kembali      = jumlah_bayar - harga_total;
            //  if (kembali < 0) kembali = 0;

            //  $('#inputTotalKembali').val(kembali.toLocaleString());
            //  $('input[name="kembali"]').val(kembali);
            // }
        });

        $(document).on('keyup', '#inputOngkosKirim', function(event) {
            event.preventDefault();

            // var ongkos_kirim = $(this).val();
            // $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            // updateHargaOnKeyup();
            var ongkos_kirim = parseInt($(this).val().replace(/\D/g, ''), 10);
            if(ongkos_kirim % 100 == 0) {
                $(this).parents('.form-group').first().removeClass('has-error');
            } else {
                $(this).parents('.form-group').first().addClass('has-error');
            }
            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('input[name="no_transfer"]').val('');
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');

                    $(this).find('select[name="bank_transfer"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="no_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');

                    $(this).find('select[name="bank_kartu"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    // $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDeposito', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                $(this).find('i').show('fast');
                $('#inputDepositoContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputDepositoContainer').hide('fast', function() {
                    $('input[name="nominal_titipan"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDiambil', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirimContainer').hide('fast', function() {
                    $('select[name="user_id"]').select2('val', '');
                    $('select[name="user_id"]').val('').trigger('change');
                    $('#inputPengirimLain').val('');
                    $('#formSimpanContainer').find('input[name="pengirim"]').val('');
                    $('#formSimpanContainer').find('input[name="pengirim_lain"]').val('');
                    $('input[name="ongkos_kirim"]').val(0);

                    updateHargaOnKeyup();
                });
            }
            $('#inputOngkosKirim').val('');
            $('#inputOngkosKirim').prop('readonly', true);
            $('#btnDikirim').removeClass('btn-primary');
            $('#btnDikirim').addClass('btn-default');
            $('#btnDikirim').find('i').removeClass('fa-check');
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDikirim', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirimContainer').show('fast', function() {
                    updateHargaOnKeyup();
                });
            }
            $('#inputOngkosKirim').prop('readonly', false);
            $('#btnDiambil').removeClass('btn-success');
            $('#btnDiambil').addClass('btn-default');
            $('#btnDiambil').find('i').removeClass('fa-check');
        });

        $(document).on('keyup', '#inputPengirimLain', function(event) {
            event.preventDefault();

            var pengirim_lain = $(this).val();
            $('input[name="pengirim_lain"]').val(pengirim_lain);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnAlamatBiasa', function(event) {
            event.preventDefault();

            var alamat_biasa = '';
            if (pelanggan != null) alamat_biasa = pelanggan.alamat;
            $('#inputAlamat').prop('disabled', true);
            $('#inputAlamat').val(alamat_biasa);
            $('input[name="alamat_lain"]').val('');

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
            }
            $('#btnAlamatLain').removeClass('btn-primary');
            $('#btnAlamatLain').addClass('btn-default');
            $('#btnAlamatLain').find('i').removeClass('fa-check');
        });

        $(document).on('click', '#btnAlamatLain', function(event) {
            event.preventDefault();

            var alamat_lain = transaksi_penjualan.alamat;
            if (alamat_lain == null) alamat_lain = '';
            $('#inputAlamat').prop('disabled', false);
            $('#inputAlamat').val(alamat_lain).trigger('focus');
            $('input[name="alamat_lain"]').val(alamat_lain);

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
            }
            $('#btnAlamatBiasa').removeClass('btn-success');
            $('#btnAlamatBiasa').addClass('btn-default');
            $('#btnAlamatBiasa').find('i').removeClass('fa-check');
        });

        $(document).on('keyup', '#inputAlamat', function(event) {
            event.preventDefault();

            var alamat_lain = $(this).val();
            $('input[name="alamat_lain"]').val(alamat_lain);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // $(this).val(nominal_tunai.toLocaleString());
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_transfer"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            // $(this).val(nominal_transfer.toLocaleString());
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_transfer += '';
            //     nominal_transfer = nominal_transfer.slice(0, -1);
            //     nominal_transfer = parseFloat(nominal_transfer);
            //     if (isNaN(nominal_transfer)) nominal_transfer = 0;

            //     $(this).val(nominal_transfer.toLocaleString());
            //     $('input[name="nominal_transfer"]').val(nominal_transfer);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            // $(this).val(nominal_kartu.toLocaleString());
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_kartu += '';
            //     nominal_kartu = nominal_kartu.slice(0, -1);
            //     nominal_kartu = parseFloat(nominal_kartu);
            //     if (isNaN(nominal_kartu)) nominal_kartu = 0;

            //     $(this).val(nominal_kartu.toLocaleString());
            //     $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal_kartu);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            // $(this).val(nominal_cek.toLocaleString());
            $('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_cek += '';
            //     nominal_cek = nominal_cek.slice(0, -1);
            //     nominal_cek = parseFloat(nominal_cek);
            //     if (isNaN(nominal_cek)) nominal_cek = 0;

            //     $(this).val(nominal_cek.toLocaleString());
            //     $('input[name="nominal_cek"]').val(nominal_cek);
            //     updateHargaOnKeyup();
            // }
        });

        $(document).on('focus', '#inputJatuhTempo', function(event) {
            event.preventDefault();

            $('#inputJatuhTempo').daterangepicker({
                autoApply: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: true
            }, function(start) {
                var jatuh_tempo = (start.toISOString()).substring(0,10);
                $('#inputJatuhTempo').val(ymd2dmy(jatuh_tempo));
                $('input[name="jatuh_tempo"]').val(jatuh_tempo);
                updateHargaOnKeyup();
            });
        });

        $(document).on('keyup', '#inputNoBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            // $(this).val(nominal_bg.toLocaleString());
            $('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();

            // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            // if (isNaN(nominal_tunai)) nominal_tunai = 0;
            // if (isNaN(kembali)) kembali = 0;

            // if (nominal_tunai <= 0 && kembali > 0) {
            //     nominal_bg += '';
            //     nominal_bg = nominal_bg.slice(0, -1);
            //     nominal_bg = parseFloat(nominal_bg);
            //     if (isNaN(nominal_bg)) nominal_bg = 0;

            //     $(this).val(nominal_bg.toLocaleString());
            //     $('input[name="nominal_bg"]').val(nominal_bg);
            //     updateHargaOnKeyup();
            // }
        });

        /*$(document).on('keyup', '#inputNoKredit', function(event) {
            event.preventDefault();

            var no_kredit = $(this).val();
            $('input[name="no_kredit"]').val(no_kredit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKredit', function(event) {
            event.preventDefault();

            var nominal_kredit = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kredit)) nominal_kredit = 0;

            $(this).val(nominal_kredit.toLocaleString());
            $('input[name="nominal_kredit"]').val(nominal_kredit);
            updateHargaOnKeyup();

            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(kembali)) kembali = 0;

            if (nominal_tunai <= 0 && kembali > 0) {
                nominal_kredit += '';
                nominal_kredit = nominal_kredit.slice(0, -1);
                nominal_kredit = parseFloat(nominal_kredit);
                if (isNaN(nominal_kredit)) nominal_kredit = 0;

                $(this).val(nominal_kredit.toLocaleString());
                $('input[name="nominal_kredit"]').val(nominal_kredit);
                updateHargaOnKeyup();
            }
        });*/

        $(document).on('keyup', '#inputNominalDeposito', function(event) {
            event.preventDefault();

            var nominal_titipan = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_titipan_max = parseFloat($('input[name="titipan"]').val());

            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(nominal_titipan_max)) nominal_titipan_max = 0;

            if (nominal_titipan <= nominal_titipan_max) {
                $(this).parents('.input-group').first().removeClass('has-error');
                // $(this).val(nominal_titipan.toLocaleString());
                $('input[name="nominal_titipan"]').val(nominal_titipan);
                updateHargaOnKeyup();

                // var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
                // var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

                // if (isNaN(nominal_tunai)) nominal_tunai = 0;
                // if (isNaN(kembali)) kembali = 0;

                // if (nominal_tunai <= 0 && kembali > 0) {
                //     nominal_titipan += '';
                //     nominal_titipan = nominal_titipan.slice(0, -1);
                //     nominal_titipan = parseFloat(nominal_titipan);
                //     if (isNaN(nominal_titipan)) nominal_titipan = 0;

                //     $(this).val(nominal_titipan.toLocaleString());
                //     $('input[name="nominal_titipan"]').val(nominal_titipan);
                //     updateHargaOnKeyup();
                // }
            } else {
                $(this).parents('.input-group').first().addClass('has-error');
                updateHargaOnKeyup();
            }
        });

        $(document).on('change', 'select[name="user_id"]', function(event) {
            event.preventDefault();

            var user_id = $(this).val();
            if (user_id == '-') {
                $('#inputPengirimLain').prop('disabled', false);
                $('input[name="pengirim"]').val('');
            } else {
                $('#inputPengirimLain').prop('disabled', true);
                $('#inputPengirimLain').val('');
                $('input[name="pengirim"]').val(user_id);
                $('input[name="pengirim_lain"]').val('');
            }

            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnSimpanPO', function(event) {
            event.preventDefault();

            var action = "{{ url('transaksi-grosir-vip/simpan-po') }}" + '/' + transaksi_penjualan.id;
            $('#form-simpan').attr('action', action);
            $('#form-simpan').submit();
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            var index = selected_items.indexOf(kode);
            if (index >= -1) {
                selected_items.splice(index, 1);
            }

            var inputNegoContainer = tr.find('#inputNegoContainer');
            var checked = tr.find('#checkNego').prop('checked');
            var nego = parseFloat(tr.find('#inputNego').val().replace(/\D/g, ''), 10);;
            var nego_min = parseFloat(tr.find('#inputNegoMin').val());
            var subtotal = 0;
            if (checked && !inputNegoContainer.hasClass('has-error')) {
                subtotal = nego;
            } else {
                subtotal = parseFloat(tr.find('#inputSubTotal').val().replace(/\D/g, ''), 10);
            }

            var harga_total = parseFloat($('input[name="harga_total"]').val().replace(/\D/g, ''), 10);
            var nego_total_min = parseFloat($('#inputNegoTotalMin').val().replace(/\D/g, ''), 10);
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);

            harga_total -= subtotal;
            nego_total_min -= nego_min;
            harga_total_plus_ongkos_kirim -= subtotal;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            if (isNaN(harga_total) || harga_total == 0) harga_total = 0;
            if (isNaN(nego_total_min) || nego_total_min == 0) nego_total_min = '';
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('#inputNegoTotalMin').val(nego_total_min);
            $('#inputHargaTotalPlusOngkosKirim').val(harga_total_plus_ongkos_kirim.toLocaleString());
            $('#inputTotalKembali').val(kembali.toLocaleString());

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');

            $('input[name="harga_total"]').val(harga_total);
            // $('input[name="kembali"]').val(kembali);

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();

            updateHargaOnKeyup();
        });

    </script>
@endsection
