<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style media="print">
        table {
            margin: 10px;
        }
        table > tbody > tr:nth-child(odd) {
            background: #E96262;
        }
        table > tbody > tr:nth-child(even) {
            background: #5A56ED;
        }
        td {
            border: solid 1px;
        }

        div {
            margin: 10px;
            padding: 10px;
            background: #3DDA44;
            color: white;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <td>No</td>
                <td>Nama</td>
                <td>Alamat</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Mukhsin</td>
                <td>Banyumas</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Topik</td>
                <td>Kebumen</td>
            </tr>
        </tbody>
    </table>
    <div>
        Hello, world
    </div>
</body>
</html>