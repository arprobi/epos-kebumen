@extends('layouts.app')

@section('app.title')
    @yield('title')
@endsection

@section('app.style')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('css/prettify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

    <!-- <link href="{{ asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/scroller.bootstrap.min.css') }}" rel="stylesheet"> -->
    <style media="screen">
        table > thead > tr > th {
            text-align: center;
        }
        table > tbody > tr > td.tengah-hv{
            vertical-align: middle;
            text-align: center; 
        }
        table > tbody > tr > td.tengah-vh{
            vertical-align: middle;
            text-align: center; 
        }
        table > tbody > tr > td.tengah-v {
            vertical-align: middle;
        }
        table > tbody > tr > td.tengah-h {
            text-align: center; 
        }
        .form-group .input-group > .input-group-addon {
            border-radius: 0;
        }
        .line {
            height: 10px;
            width: 100%;
            border-top: 2px solid #E6E9ED;
        }
        .collapse-link {
            cursor: pointer;
        }
        .btn-purple {
            color: #ffffff;
            background-color: #611BBD;
            border-color: #130269;
        } 

        .btn-purple:hover,
        .btn-purple:focus,
        .btn-purple:active,
        .btn-purple.active,
        .open .dropdown-toggle.btn-purple {
            color: #ffffff;
            background-color: #49247A;
            border-color: #130269;
        } 

        .btn-purple:active,
        .btn-purple.active,
        .open .dropdown-toggle.btn-purple {
            background-image: none;
        }

        .btn-purple.disabled,
        .btn-purple[disabled],
        fieldset[disabled] .btn-purple,
        .btn-purple.disabled:hover,
        .btn-purple[disabled]:hover,
        fieldset[disabled] .btn-purple:hover,
        .btn-purple.disabled:focus,
        .btn-purple[disabled]:focus,
        fieldset[disabled] .btn-purple:focus,
        .btn-purple.disabled:active,
        .btn-purple[disabled]:active,
        fieldset[disabled] .btn-purple:active,
        .btn-purple.disabled.active,
        .btn-purple[disabled].active,
        fieldset[disabled] .btn-purple.active {
            background-color: #611BBD;
            border-color: #130269;
        }

        .btn-purple .badge {
            color: #611BBD;
            background-color: #ffffff;
        }

        .sembunyi {
            display: none;
        }

        ul.msg_list li:last-child {
            padding: 5px;
        }

        .link-mati {
            pointer-events: none;
            cursor: progress;
            color: #999 !important;
        }
    </style>
    @yield('style')
@endsection

@section('app.content')
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0; ">
            <a href="{{ url('/') }}" class="site_title">
                {{-- <i class="fa fa-paw"></i><span> EPOS - KM</span> --}}
                <div>
                    <img src="{{ asset('images/km.ico') }}" style="width:50px;" id="logo_perusahaan" alt="Avatar">
                    <span class="pull-right" style="padding-right: 25px;"> EPOS - KM</span>
                </div>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{URL::to('/foto_profil/'.Auth::user()->foto)}}" class="img-circle profile_img" style="width: 52px; height: 52px; vertical-align: middle" >
            </div>
            <div class="profile_info">
                <span>Sugeng Rawuh,</span>
                <h2>{{ Auth::user()->username }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    @if( Auth::user()->level_id == 1 ||  Auth::user()->level_id == 2 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Pemasok</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    {{-- <li><a href="{{url('bonus')}}">Bonus</a></li> --}}
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{ url('bank') }}"> Bank </a></li>
                                {{-- <li><a href="{{ url('kredit') }}"> Kredit </a></li> --}}
                                <li><a href="{{url('wilayah')}}">Wilayah</a></li>
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Pembelian<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian/create') }}">Tambah Transaksi Pembelian</a></li>
                                <li><a href="{{ url('po-pembelian') }}">Daftar Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian') }}">Daftar Transaksi Pembelian</a></li>
                                <li><a href="{{ url('retur-pembelian') }}">Daftar Retur Pembelian</a></li>
                                {{-- <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li> --}}
                                {{-- <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li> --}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                <li><a href="{{ url('transaksi-grosir-vip/create') }}">Tambah Pesanan Penjualan (Prioritas)</a></li>
                                <li><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan Penjualan</a></li>
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                                {{--<li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li>--}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Konsinyasi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-database"></i> Stok<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('stock-of-num') }}">Pengecekan Stok</a></li>
                                <li><a href="{{ url('penyesuaian_stok') }}">Penyesuaian Stok</a></li>
                                <li><a href="{{ url('penyesuaian_persediaan') }}">Penyesuaian Persediaan</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-archive"></i> Kas<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('kas')}}"></i> Kas</a></li>
                                <!-- <li><a href="{{url('cek')}}"></i> Cek</a></li>
                                <li><a href="{{url('bg')}}"></i> BG</a></li> -->
                                <li><a href="{{url('modal')}}"></i> Modal</a></li>
                                <li><a href="{{url('prive')}}"></i> Prive</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-credit-card"></i> Hutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('hutang') }}">Hutang Dagang </a></li>
                                <!-- <li><a href="{{ url('hutang-suplier') }}">Hutang Suplier</a></li> -->
                                <li><a href="{{ url('hutang_bank') }}"> Hutang Bank</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-newspaper-o"></i> Piutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('piutang-dagang') }}">Piutang Dagang</a></li>
                                <li><a href="{{ url('piutang_lain') }}">Piutang Lain-Lain</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-calculator"></i> Akuntansi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('harta_tetap') }}">Tanah dan Bangunan</a></li>
                                <li><a href="{{url('perlengkapan')}}">Perlengkapan</a></li>
                                <li><a href="{{url('peralatan')}}">Peralatan</a></li>
                                <li><a href="{{url('kendaraan')}}">Kendaraan</a></li>
                                <li><a href="{{ url('beban') }}">Pembayaran Beban Beban</a></li>
                                <li><a href="{{ url('gaji') }}">Pembayaran Gaji</a></li>
                                <li><a href="{{ url('pendapatan_lain') }}">Pendapatan Lain-lain</a></li>
                                <li><a href="{{ url('akun') }}">Akun</a></li>
                                <li><a href="{{ url('jurnal') }}">Jurnal Umum</a></li>
                                {{-- <li><a href="{{ url('jurnal_penyesuaian') }}">Jurnal Penyesuaian</a></li> --}}
                                <li><a href="{{ url('laba_rugi') }}">Laba Rugi</a></li>
                                <li><a href="{{ url('neraca') }}">Neraca</a></li>
                                <li><a href="{{ url('arus_kas') }}">Arus Kas</a></li>
                                <!-- <li><a href="{{ url('laba_ditahan') }}">Laba Ditahan</a></li>                           -->
                                {{-- <li><a href="{{ url('jurnal_penutup') }}">Jurnal Penutup</a></li>   --}} 
                                <li><a href="{{ url('bagi_hasil') }}">Penarikan Bagi Hasil</a></li> 
                            </ul>
                        </li>
                        <li><a><i class="fa fa-area-chart"></i> Grafik <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('grafik/pembelian') }}">Pembelian</a></li>
                                <li><a href="{{ url('grafik/penjualan') }}">Penjualan</a></li>
                                <li><a href="{{ url('grafik/beban') }}">Beban-beban</a></li>
                                <li><a href="{{ url('grafik/laba') }}">Laba Rugi Bersih</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('data_perusahaan')}}">Data Perusahaan</a></li>
                                <li><a href="{{ url('level') }}"> Tingkatan Pengguna</a></li>
                                <li><a href="{{ url('user') }}"> Pengguna </a></li>
                                <li><a href="{{url('catatan')}}">Catatan Nota</a></li>
                                <li><a href="{{url('money-limit')}}">Batas Laci Kasir Eceran</a></li>
                                <li><a href="{{url('laci')}}">Laci</a></li>
                                <li><a href="{{url('log_laci')}}">Riwayat Laci</a></li>
                                <li><a href="{{url('periode')}}">Periode Order</a></li>
                                {{-- <li><a href="{{url('setoran-kasir')}}">Setoran Kasir Eceran</a></li>
                                <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                                <li><a href="{{url('rule-bonus')}}">Bonus Penjualan</a></li>
                                <li><a href="{{url('rule_retur')}}">Batas Syarat Retur</a></li>
                                {{-- <li><a href="{{url('event')}}">Layout Invoice</a></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 3 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <!-- <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Suplier</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    <li><a href="{{url('bonus')}}">Bonus</a></li>
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                            </ul>
                        </li> -->
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                <li id="tambahPOEceran"><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan Penjualan</a></li>
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                                {{--<li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li>--}}
                            </ul>
                        </li>
                    
                        <li><a><i class="glyphicon glyphicon-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('setoran-kasir')}}">Laci</a></li>
                                {{-- <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                                {{-- <li><a href="{{url('laci')}}">Laci Grosia></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 4 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                               {{--  <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Suplier</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    <li><a href="{{url('bonus')}}">Bonus</a></li>
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li> --}}
                                <li><a href="{{url('pelanggan')}}">Pelanggan</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Penjualan<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{ url('transaksi/create') }}">Tambah Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi') }}">Daftar Transaksi Eceran</a></li> --}}
                                {{-- <li><a href="{{ url('transaksi-grosir/create') }}">Tambah Pesanan</a></li> --}}
                                <li><a href="{{ url('po-penjualan') }}">Daftar Pesanan Penjualan</a></li>
                                <li><a href="{{ url('transaksi-grosir') }}">Daftar Transaksi Penjualan</a></li>
                                <li><a href="{{ url('retur-penjualan') }}">Daftar Retur Penjualan</a></li>
                                <li><a href="{{ url('po-penjualan/create') }}">Tambah Pesanan Penjualan</a></li>
                            </ul>
                        </li>
                    
                        <li><a><i class="glyphicon glyphicon-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('laci')}}">Laci</a></li>
                                {{-- <li><a href="{{url('setoran-kasir')}}">Setoran Kasir Eceran</a></li> --}}
                                {{-- <li><a href="{{url('setoran-buka')}}">Setoran Buka</a></li> --}}
                            </ul>
                        </li>
                    @elseif( Auth::user()->level_id == 5 )
                        <li>
                            <a href="{{url('beranda')}}">
                                <i class="fa fa-home"></i> Beranda
                            </a>
                        </li>
                        <li><a><i class="fa fa-folder-open"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                {{-- <li><a href="{{url('item-konsinyasi')}}">Item Konsinyasi</a></li> --}}
                                <li><a>Item<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="{{url('suplier')}}">Pemasok</a></li>
                                    <li><a href="{{url('seller')}}">Penjual</a></li>
                                    <li><a href="{{url('jenisitem')}}">Jenis Item</a></li>
                                    <li><a href="{{url('satuan')}}">Satuan Item</a></li>
                                    {{-- <li><a href="{{url('bonus')}}">Bonus</a></li> --}}
                                    <li><a href="{{url('item')}}">Data Item</a></li>
                                  </ul>
                                </li>
                                {{-- <li><a href="{{url('pelanggan')}}">Pelanggan</a></li> --}}
                                <li><a href="{{url('wilayah')}}">Wilayah</a></li>
                                <!-- <li><a href="{{url('perlengkapan')}}">Perlengkapan</a></li>
                                <li><a href="{{url('peralatan')}}">Peralatan</a></li>
                                <li><a href="{{url('kendaraan')}}">Kendaraan</a></li> -->
                            </ul>
                        </li>
                        <li><a><i class="fa fa-exchange"></i> Transaksi Pembelian<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('transaksi-pembelian/create') }}">Tambah Transaksi Pembelian</a></li>
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li>
                                <li><a href="{{ url('transaksi-pembelian') }}">Daftar Transaksi Pembelian</a></li>
                                <li><a href="{{ url('retur-pembelian') }}">Daftar Retur Pembelian</a></li>
                                {{-- <li><a href="{{ url('konsinyasi-masuk/create') }}">Tambah Konsinyasi Masuk</a></li> --}}
                                {{-- <li><a href="{{ url('konsinyasi-masuk') }}">Daftar Konsinyasi Masuk</a></li>
                                
                                <li><a href="{{ url('konsinyasi-keluar') }}">Daftar Konsinyasi Keluar</a></li>
                                <li><a href="{{ url('po-pembelian/create') }}">Tambah Pesanan Pembelian</a></li> --}}
                                <li><a href="{{ url('po-pembelian') }}">Daftar Pesanan Pembelian</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-database"></i> Stok<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('stock-of-num') }}">Pengecekan Stok</a></li>
                                <li><a href="{{ url('penyesuaian_stok') }}">Penyesuaian Stok</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-credit-card"></i> Hutang<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('hutang') }}">Hutang Dagang </a></li>
                                <li><a href="{{ url('hutang_bank') }}"> Hutang Bank</a></li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-calculator"></i> Akuntansi<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{ url('beban') }}">Pembayaran Beban</a></li>
                            </ul>
                        </li>
                        <li><a><i class="glyphicon glyphicon-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('laci')}}">Laci</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <span aria-hidden="true" style="" class="invisible">a</span>

            {{-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
                <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a> --}}
        </div>
    <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right" style="width: 85%;">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{URL::to('/foto_profil/'.Auth::user()->foto)}}" alt="">{{ Auth::user()->username }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ url('profil') }}"> Profil</a></li>
                        {{-- <li><a href="javascript:;"> Bantuan</a></li> --}}
                        <li id="btnLogout">
                            <a href="{{url('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-green" id="sumAlert"></span>
                    </a>
                    <ul id="alertNotification" class="dropdown-menu list-unstyled msg_list" role="menu">
                    </ul>
                </li>
                @if (in_array(Auth::user()->id, [1, 2]))
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-exchange"></i>
                        <span class="badge bg-green" id="sumNotice"></span>
                    </a>
                    <ul id="noticeNotification" class="dropdown-menu list-unstyled msg_list" role="menu">
                    </ul>
                </li>
                @endif
                {{-- <li role="presentation" class="dropdown" id="duit">
                </li> --}}
                <div id="cash-box" class="nav toggle pull-right sembunyi" style="width: 600px; padding-right: 15px;">
                    <span id="cash" class="pull-right" style="margin-top: 4px;"></span>
                </div>
                <div id="duit-box" class="nav toggle pull-right sembunyi" style="width: 600px; padding-right: 15px;">
                    <span id="duit" class="pull-right" style="margin-top: 4px;"></span>
                </div>
            </ul>
            
        </nav>
    </div>
</div>

<!-- /top navigation -->
<div class="right_col" role="main">
    <div class="row">
        @yield('content')
    </div>
</div>

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/accounting.min.js') }}"></script>

    <!-- <script type="text/javascript" src="{{ asset('js/fastclick.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/icheck.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.scannerdetection.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script> -->

    <!-- <script type="text/javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.fixedHeader.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.keyTable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/responsive.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.scroller.min.js') }}"></script> -->

    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('js/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('js/prettify.js') }}"></script>
    <script type="text/javascript">
        var showSpinner = false;
    </script>

    @yield('script')
    <script type="text/javascript">
        // $('.right_col').css('min-height', $('.left_col').css('height'));
        var sumAlert = 0;

        $(document).ready(function() {
            if (!showSpinner) {
                setTimeout(function() {
                    $('#spinner').hide();
                }, 500);
            }

            $('.tanggal').each(function(index, el) {
                var text = $(el).text();
                if (text === undefined || text === '') {
                    $(el).text('-');
                    $(el).css('text-align', 'center');
                } else {
                    var tanggal = text.split('-');
                    var dd = tanggal[2];
                    var mm = tanggal[1];
                    var yyyy = tanggal[0];

                    tanggal = dd+'-'+mm+'-'+yyyy;
                    $(el).text(tanggal);
                }
            });

            $('.duit').each(function(index, el) {
                var duit = parseInt($(el).text());
                $(el).text(duit.toLocaleString());
            });

            $(".select2_single").select2({
                width: '100%',
                allowClear: true
            });

            // $('.select2_single').each(function(index, el) {
            //     $(el).val('').change();
            // });
        });

        /*$(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('logo_perusahaan/json') }}";

            $.get(url, function(data) {
                var src = '{{ URL::to("/images/") }}'+ '/' + data.logo.logo;
                $('#logo_perusahaan').attr('src', src);
            });
        });*/

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/stok/json') }}";

            $.get(url, function(data) {
                if (data.kurang.length > 0) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('item/alert') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Batas Minimal Stok</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa item yang kurang dari batas minimal! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/notice/json') }}";

            $.get(url, function(data) {
                // console.log(data);
                if (data.notices.length > 0) {
                    for (var i = 0; i < data.notices.length; i++) {
                        var notice = data.notices[i];
                        var url = '{{ url('transaksi-pembelian') }}' + '/' + notice.transaksi_pembelian_id + '/again';
                        $("#noticeNotification").append(''+
                            '<li>'+
                                '<a href="'+url+'">'+
                                    '<span>'+
                                        '<span><h5><strong>Notice Perubahan Harga Beli</h5></strong></span>'+
                                    '</span>'+
                                    '<span class="message" style="line-height: 1; margin-bottom: 5px;">'+
                                        'Kode Transaksi: ['+notice.transaksi_pembelian.kode_transaksi+']<br style="margin-bottom: 5px;">'+
                                        (notice.transaksi_pembelian.nota==null?'':'Kode Faktur: ['+notice.transaksi_pembelian.nota+']<br style="margin-bottom: 5px;">')+
                                        'Ada beberapa item yang harga jualnya harus disesuaikan!'+
                                    '</span>'+
                                '</a>'+
                            '</li>');
                        $('#sumNotice').text(data.notices.length);
                    }
                }
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/kadaluarsa/json') }}";
            $.get(url, function(data) {
                if (data.kadaluarsa.length > 0) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('item/kadaluarsa') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Item Kadaluarsa</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa item yang kadaluarsa, segera CEK! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_kasir/json') }}";
            $.get(url, function(data) {
                if (data.cash != null) {
                    $('#cash-box').removeClass('sembunyi');
                    var cash = data.cash;
                    var duit = cash.nominal;
                    duit = duit.toLocaleString(['ban', 'id']);
                    $('#cash').text('Saldo Kasir Rp'+duit);

                    var money_limit = data.money_limit;

                    var user = '{{ json_encode(Auth::user()) }}';
                    user = user.replace(/&quot;/g, '"');
                    user = JSON.parse(user);
                    if (cash['nominal'] >= money_limit.nominal * 0.75 && user.id == 3) {
                        swal({
                            title: 'Laci hampir penuh!',
                            text: 'Setorkan uang kepada kasir grosir untuk mengurangi!',
                            type: 'warning',
                            showCloseButton: true,
                            // showCancelButton: true,
                            confirmButtonColor: '#26B99A',
                            // cancelButtonColor: '#d9534f',
                            confirmButtonText: '<i class="fa fa-check"></i> OK',
                            // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                        }).then(function() {
                            // console.log('oi');
                        }, function(dismiss) {
                            // console.log(dismiss);
                        });
                    }
                } else {
                    $('#cash-box').addClass('sembunyi');
                }
            });
        });

        /*$(window).on('load', function(event) {
            var url = "{{ url('alert/cash_grosir/json') }}";
            $.get(url, function(data) {
                if(data.cash != null && Object.keys(data.cash).length > 0){
                    $("#duit").append('<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">'+
                        '<i class="fa fa-money"></i>'+
                        '<span class="badge bg-green" id="sumAlert"></span>'+
                    '</a>'+
                    '<ul id="duit_row" class="dropdown-menu list-unstyled msg_list" role="menu">');

                    for(var i=0; i < Object.keys(data.cash).length; i++){
                        $("#duit_row").append('<li>'+
                            '<a>'+
                                '<span>'+
                                    '<span><h5><strong> Rp' + data.cash[i].nominal.toLocaleString(['ban', 'id']) +'</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Saldo Kasir : '+ data.cash[i].nama +
                                '</span>'+
                            '</a>'+
                        '</li> </ul>');
                    }
                }
            });
        });*/

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/cash_grosir/json') }}";
            $.get(url, function(data) {
                if(data.length != 0){
                    // console.log(data.length);
                    var cash = '';

                    var a = 0;
                    var panjang = Object.keys(data.cash).length;
                    $('#duit-box').removeClass('sembunyi');
                    for(var i=0; i < Object.keys(data.cash).length; i++){
                        a += 1;
                        if(a == panjang){
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal;
                        }else{
                            cash += `Saldo `+data.cash[i].nama+` : Rp`+
                                    data.cash[i].nominal+' | ';
                        }
                    }
                    $('#duit').text(cash);
                }
            });
        });

        // $(window).on('load', function(event) {
        //     event.preventDefault();

        //     var url = "{{ url('sidebar/setoran-buka/json') }}";
        //     $.get(url, function(data) {
        //         // console.log(data);
        //         var setoran_buka = data.setoran_buka;
        //         var cash_drawer = data.cash_drawer;
        //         // console.log(setoran_buka);
        //         if (setoran_buka == null || cash_drawer.nominal <= 0 ) {
        //             $('#tambahPOEceran').addClass('link-mati');
        //             $('#tambahPOEceran').find('a').addClass('link-mati');
        //         }
        //     });
        // });

        $(document).on('keyup', 'input[type="text"]', function(event) {
            event.preventDefault();

            if ($(this).hasClass('angka')) {
                var angka = $(this).val();

                if (angka === undefined || angka === null || angka === '' || angka === ' ' || angka.length === 0) {
                    $(this).val(angka.slice(0, -1));
                    return;
                }

                if (isNaN(angka)) {
                    var angkaBaru = parseInt(angka.replace(/\D/g, ''), 10);
                    if (isNaN(angkaBaru)) {
                        if (angka.length > 0) {
                            $(this).val(angka.slice(0, -1));
                        } else {
                            $(this).val('');
                        }
                        
                    } else {
                        $(this).val(angkaBaru.toLocaleString(['ban', 'id']));
                        $(this).next().val(angkaBaru);
                    }
                } else {
                    angka = parseInt(angka.replace(/\D/g, ''), 10);
                    $(this).val(angka.toLocaleString(['ban', 'id']));
                    $(this).next().val(angka);
                }
            }
        });

        $(document).on('click', '#btnLogout', function(event) {
            event.preventDefault();

            var href = $(this).find('a').attr('href');
            var url = '{{ url("logout/json") }}';
            $.get(url, function(data) {
                var user = data.user;
                // console.log(user);
                if (user.level_id == 3 && nominal > 0) {
                    var cash_drawer = data.cash_drawer;
                    var laci = data.laci;
                    var nominal = parseFloat(cash_drawer.nominal);
                    var laci_grosir = parseFloat(laci.grosir);
                    swal({
                        title: 'Terjadi kesalahan!',
                        text: 'Masih terdapat uang di laci eceran!',
                        type: 'warning',
                        showCloseButton: true,
                        // showCancelButton: true,
                        confirmButtonColor: '#26B99A',
                        // cancelButtonColor: '#d9534f',
                        confirmButtonText: '<i class="fa fa-check"></i> OK',
                        // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                    }).then(function() {
                        // console.log('oi');
                    }, function(dismiss) {
                        // console.log(dismiss);
                    });
                // } else if (user.level_id == 4 && laci_grosir > 0 ) {
                //     swal({
                //         title: 'Terjadi kesalahan!',
                //         text: 'Masih terdapat uang di laci grosir!',
                //         type: 'warning',
                //         showCloseButton: true,
                //         // showCancelButton: true,
                //         confirmButtonColor: '#26B99A',
                //         // cancelButtonColor: '#d9534f',
                //         confirmButtonText: '<i class="fa fa-check"></i> OK',
                //         // cancelButtonText: '<i class="fa fa-close"></i> Batal'
                //     }).then(function() {
                //         // console.log('oi');
                //     }, function(dismiss) {
                //         // console.log(dismiss);
                //     });
                } else {
                    var ww = window.open(href, '_self');
                }
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/jatuh_tempo/pembelian/') }}";

            $.get(url, function(data) {
                if(data > 0 && data != undefined) {
                    $("#alertNotification").append('<li>'+
                        '<a href="{{ url('pembelian/jatuh_tempo') }}">'+
                            '<span>'+
                                '<span><h5><strong>Pemberitahuan Jatuh Tempo Pembelian</h5></strong></span>'+
                            '</span>'+
                            '<span class="message">'+
                                'Ada beberapa Transaksi Pembelian mendekati jatuh tempo, segera CEK! '+
                            '</span>'+
                        '</a>'+
                    '</li>');
                    sumAlert += 1;
                    $('#sumAlert').text(sumAlert);
                }
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('alert/laci_laci/') }}";
            $.get(url, function(data) {
                if(data.log.length > 0) {
                    if(data.user_level == 3){
                        console.log('aw');
                        $("#alertNotification").append('<li>'+
                            '<a href="{{ url('setoran-kasir/') }}">'+
                                '<span>'+
                                    '<span><h5><strong>Pemberitahuan Setoran Uang Laci</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Ada setoran laci yang belum diseujui, segera CEK! '+
                                '</span>'+
                            '</a>'+
                        '</li>');
                        sumAlert += 1;
                        $('#sumAlert').text(sumAlert);
                    }else{
                        console.log('bw');
                        $("#alertNotification").append('<li>'+
                            '<a href="{{ url('laci/') }}">'+
                                '<span>'+
                                    '<span><h5><strong>Pemberitahuan Setoran Uang Laci</h5></strong></span>'+
                                '</span>'+
                                '<span class="message">'+
                                    'Ada setoran laci yang belum diseujui, segera CEK! '+
                                '</span>'+
                            '</a>'+
                        '</li>');
                        sumAlert += 1;
                        $('#sumAlert').text(sumAlert);
                    }
                }
            });
        });

    </script>
@endsection
