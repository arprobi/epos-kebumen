@extends('layouts.admin')

@section('title')
    <title>EPOS | Pengguna</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah {
            margin: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #tableUser .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pengguna</h2>
                <a href="{{ url('user/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Pengguna">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableUser">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Nama Pengguna</th>
                            <th>Telepon</th>
                            <th>Tingkatan Pengguna</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user_on as $num => $user)
                        <tr id="{{$user->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $user->nama }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->telepon }}</td>
                            <td>{{ $user->level->nama }}</td>
                            <td class="tengah-h">
                                <!-- <a href="{{ url('user/show/'.$user->id) }}" class="btn btn-xs btn-info" id="btnDetail">
                                    <i class="fa fa-eye"></i> Detail
                                </a>
                                <a href="{{ url('user/edit/'.$user->id) }}" class="btn btn-xs btn-warning" id="btnUbah">
                                    <i class="fa fa-edit"></i> Ubah
                                </a>
                                <button class="btn btn-xs btn-danger" id="btnHapus">
                                    <i class="fa fa-trash"></i> Hapus
                                </button>
                                <button class="btn btn-xs btn-primary" id="btnReset">
                                    <i class="fa fa-edit"></i> Reset Sandi
                                </button> -->
                                <a href="{{ url('user/show/'.$user->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pengguna">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <!-- <a href="{{ url('user/edit/'.$user->id) }}" class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah">
                                    <i class="fa fa-edit"></i>
                                </a> -->
                                <button class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Pengguna">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <!-- <button class="btn btn-xs btn-primary" id="btnReset">
                                    <i class="fa fa-undo"></i>
                                </button> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
<!-- </div> -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Pengguna</h2>
                {{-- <a href="{{ url('user/create') }}" class="btn btn-sm btn-success pull-right">
                    <i class="fa fa-plus"></i> Tambah
                </a> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableUser1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Identitas Pengguna</th>
                            <th>Telepon</th>
                            <th>Tingkatan</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user_off as $num => $user)
                        <tr id="{{$user->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $user->nama }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->telepon }}</td>
                            <td>{{ $user->level->nama }}</td>
                            <td class="tangah-h">
                                <a href="{{ url('user/show/'.$user->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pengguna">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="btn btn-xs btn-success" id="btnReAktif" data-toggle="tooltip" data-placement="top" title="Aktifkan Pengguna">
                                    <i class="fa fa-check"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
<!-- </div> -->
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pengguna berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pengguna gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pengguna berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pengguna gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pengguna berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pengguna gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal_admin') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Admin tidak bisa dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pengguna berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pengguna gagal diaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pengguna kata sandi berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'reset')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pengguna kata sandi gagal direset!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableUser').DataTable();
        $('#tableUser1').DataTable();

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama_item + '\" akan dinonaktifkan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("user") }}' + '/' + id);
                $('#formHapusContainer').find('input[name="_method"]').val('delete');
                $('#formHapusContainer').find('form').submit();
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnReAktif', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama_item + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("user") }}' + '/re_aktif/' + id);
                $('#formHapusContainer').find('input[name="_method"]').val('post');
                {{-- href="{{ url('user/re_aktif/'.$user->id) }}" --}}
                    $('#formHapusContainer').find('form').submit();
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnReset', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();

            swal({
                title: 'Reset Kata Sandi?',
                text: 'Kata sandi \"' + nama_item + '\" akan di reset!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Reset!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    location.href = 'user/reset/' + id;
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

    </script>
@endsection
