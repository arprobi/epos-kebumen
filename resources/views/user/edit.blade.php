@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Pengguna</title>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Ubah Pengguna</h2>
				<a href="{{ URL::previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
            	</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
 				<form method="post" action="{{ url('user/'.$user->id) }}">
		        	<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="put">
					<div class="form-group">
						<label class="control-label">Nama Lengkap</label>
						<input class="form-control" type="text" name="nama" value="{{$user->nama}}" required="">
					</div>
					<div class="form-group" id="form_username">
						<label class="control-label">Nama Pengguna</label>
						<input class="form-control" type="text" name="username" value="{{$user->username}}" required="">
						<span id="username_error" style="color:red">Nama pengguna tidak boleh ada spasi dan tidak boleh lebih dari 10 karakter</span>
					</div>
					<div class="form-group">
						<label class="control-label">Email</label>
						<input class="form-control" type="text" name="email" value="{{$user->email}}" required="">
					</div>
					<!-- <div class="form-group">
						<label class="control-label">Tanggal Lahir</label>
						{{-- <input class="form-control" type="date" name="tanggal_lahir" value="{{$user->tanggal_lahir}}"> --}}
						<input type="text"  name="tanggal_" class="form-control has-feedback-left active" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" required="" value="{{ App\Util::date($user->tanggal_lahir) }}">
						<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
						<input class="form-control" type="hidden" name="tanggal_lahir" value="{{$user->tanggal_lahir}}">
					</div> -->
					<div class="form-group controls xdisplay_inputx has-feedback">
						<label class="control-label">Tanggal Lahir</label>
						<input type="text"  name="tanggal_" class="form-control has-feedback-left active tanggal-putih" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" required="" value="{{ App\Util::date($user->tanggal_lahir) }}" readonly="">
						<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
						<input class="form-control" type="hidden" name="tanggal_lahir" value="{{$user->tanggal_lahir}}">
					</div>
					<div class="form-group">
						<label class="control-label">Telepon</label>
						<input class="form-control" type="text" name="telepon" value="{{$user->telepon}}" required="">
					</div>
					<div class="form-group">
						<label class="control-label">Alamat</label>
						<textarea rows="4" class="form-control" name="alamat" required="">{{$user->alamat}}</textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Tingkatan Pengguna</label>
						<select class="form-control select2_single" id="level_id" name="level_id" required="">
							<option value="">Pilih Tingkatan Pengguna</option>
							@foreach($levels as $level)
							@if($user->level_id == $level->id)
							<option value="{{$level->id}}" selected>{{ $level->kode }} : {{ $level->nama }}</option>
							@else
							<option value="{{$level->id}}">{{ $level->kode }} : {{ $level->nama }}</option>
							@endif
							@endforeach
						</select>
					</div>
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-save"></i> Simpan
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			// console.log('oi');
			var url = "{{url('user')}}";
			var a = $('a[href="' + url + '"]');
			// console.log(a.text());
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
			$('.right_col').css('min-height', $('.left_col').css('height'));

			$(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

			$('#single_cal3').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				calender_style: "picker_2",
				format: 'DD-MM-YYYY',
				locale: {
					"applyLabel": "Pilih",
					"cancelLabel": "Batal",
					"fromLabel": "Awal",
					"toLabel": "Akhir",
					"customRangeLabel": "Custom",
					"weekLabel": "M",
					"daysOfWeek": [
						"Min",
						"Sen",
						"Sel",
						"Rab",
						"Kam",
						"Jum",
						"Sab"
					],
					"monthNames": [
						"Januari",
						"Februari",
						"Maret",
						"April",
						"Mei",
						"Juni",
						"Juli",
						"Agustus",
						"September",
						"Oktober",
						"November",
						"Desember"
					],
					"firstDay": 1
				},
			},function(start, end, label) {
				// console.log(start, end, label);
				var awal = (start.toISOString()).substring(0,10);
				$('input[name="tanggal_lahir"]').val(awal);
				// console.log(awal);
			});

            $('#username_error').hide();
		});

		$(document).on('keyup', 'input[name="username"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();

            if (text.indexOf(' ') != -1 || text.length > 10) {
                $('#form_username').addClass('has-error');
                $('#username_error').show();
            } else {
                $('#form_username').removeClass('has-error');
                $('#username_error').hide();
            }
            cek();
		});

		$(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
            	ini.parents('.form-group').first().addClass('has-error');
            	ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
            	ini.next('span').addClass('sembunyi');
            }

            cek();
		});

		function cek() {
			if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
			} else {
				$('#btnSimpan').prop('disabled', false);
			}
		}
	</script>
@endsection



