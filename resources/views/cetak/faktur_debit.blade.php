@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }

        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }

        .header_grosir{
            font-size: 10px;
            line-height: 1;
        }

        /*.invisible {
            visibility: visible;
        }*/

        .stempel p {
            font-size: 10px;
        }
    </style>
@endsection

@section('app.body')
    <img class="logo kmk" src="{{ URL::to('images/logo1.png') }}" style="margin-top: 1px; width: 175px; height: 90px; display: none;">
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>
    <script type="text/javascript" charset="utf-8" async defer>
        var data_perusahaan = null;
        var transaksi_penjualan = null;
        var potongan_penjualan = 0;
        var grand_total = 0;
        var jumlah_bayar = 0;
        var kembali = 0;
        var deposito = 0;
        var hargas = [];
        var catatans = [];
        var relasi_transaksi_penjualan = [];
        var relasi_bonus_rules_penjualan = [];
        var kasir_eceran = '';
        var kasir_grosir = '';
        var pkp = '';
        var npwp = '';

        var $kmk = $('.kmk').clone();
        $kmk.show();
        $('.kmk').remove();
        // console.log($kmk);

        $(document).ready(function() {
            data_perusahaan = '{{ json_encode($data_perusahaan) }}';
            data_perusahaan = data_perusahaan.replace(/&quot;/g, '"');
            data_perusahaan = JSON.parse(data_perusahaan);
            // console.log(data_perusahaan);

            if(data_perusahaan.npwp == null || data_perusahaan.npwp == undefined || data_perusahaan.npwp == '') {
                npwp = '-';
            } else {
                npwp = data_perusahaan.npwp;
            }

            if(data_perusahaan.pkp == null || data_perusahaan.pkp == undefined || data_perusahaan.pkp == '') {
                pkp = '-';
            } else {
                pkp = data_perusahaan.pkp;
            }

            transaksi_penjualan = '{{ $transaksi_penjualan }}';
            transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
            transaksi_penjualan = JSON.parse(transaksi_penjualan);
            // console.log(transaksi_penjualan);

            relasi_bonus_rules_penjualan = '{{ json_encode($relasi_bonus_rules_penjualan) }}';
            relasi_bonus_rules_penjualan = relasi_bonus_rules_penjualan.replace(/&quot;/g, '"');
            relasi_bonus_rules_penjualan = JSON.parse(relasi_bonus_rules_penjualan);

            kasir_eceran = '{{ json_encode($kasir_eceran) }}';
            kasir_eceran = kasir_eceran.replace(/&quot;/g, '"');
            kasir_eceran = JSON.parse(kasir_eceran);

            kasir_grosir = '{{ json_encode($kasir_grosir) }}';
            kasir_grosir = kasir_grosir.replace(/&quot;/g, '"');
            kasir_grosir = JSON.parse(kasir_grosir);

            hargas = '{{ $hargas }}';
            hargas = hargas.replace(/&quot;/g, '"');
            hargas = JSON.parse(hargas);

            catatans = '{{ $catatans }}';
            catatans = catatans.replace(/&quot;/g, '"');
            catatans = JSON.parse(catatans);

            potongan_penjualan = '{{ $potongan_penjualan }}';
            potongan_penjualan = potongan_penjualan.replace(/&quot;/g, '"');
            potongan_penjualan = JSON.parse(potongan_penjualan);

            grand_total = '{{ $grand_total }}';
            grand_total = grand_total.replace(/&quot;/g, '"');
            grand_total = JSON.parse(grand_total);

            jumlah_bayar = '{{ $jumlah_bayar }}';
            jumlah_bayar = jumlah_bayar.replace(/&quot;/g, '"');
            jumlah_bayar = JSON.parse(jumlah_bayar);

            kembali = '{{ $kembali }}';
            kembali = kembali.replace(/&quot;/g, '"');
            kembali = JSON.parse(kembali);

            deposito = transaksi_penjualan.nominal_titipan;

            // relasi_transaksi_penjualan = '{{ json_encode($relasi_transaksi_penjualan) }}';
            // relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            // relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            var temp_relasi_transaksi_penjualan = '{{ json_encode($relasi_transaksi_penjualan) }}';
            temp_relasi_transaksi_penjualan = temp_relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            temp_relasi_transaksi_penjualan = JSON.parse(temp_relasi_transaksi_penjualan);

            // untuk bonus transaksi
            for (var i = 0; i < relasi_bonus_rules_penjualan.length; i++) {
                var rbrp = relasi_bonus_rules_penjualan[i];
                var bonus = rbrp.bonus;
                var satuan_pembelians = [];
                for (var k = 0; k < bonus.satuan_pembelians.length; k++) {
                    var satuan = bonus.satuan_pembelians[k];
                    satuan_pembelians.push({
                        konversi: satuan.konversi,
                        satuan: {
                            id: satuan.satuan.id,
                            kode: satuan.satuan.kode
                        }
                    });
                }

                relasi_transaksi_penjualan.push({
                    absen: false,
                    item_kode: bonus.kode,
                    jumlah: rbrp.jumlah,
                    harga: 0,
                    subtotal: 0,
                    item: {
                        kode: bonus.kode,
                        kode_barang: bonus.kode,
                        nama: bonus.nama,
                        satuan_pembelians: satuan_pembelians
                    }
                });
            }

            for (var i = 0; i < temp_relasi_transaksi_penjualan.length; i++) {
                // relasi_transaksi_penjualan.push(temp_relasi_transaksi_penjualan[i]);

                var relasi = temp_relasi_transaksi_penjualan[i];
                // satuan pembelian untuk item biasa & bundle
                var satuan_pembelians = [];
                for (var j = 0; j < relasi.item.satuan_pembelians.length; j++) {
                    var satuan = relasi.item.satuan_pembelians[j];
                    satuan_pembelians.push({
                        konversi: satuan.konversi,
                        satuan: {
                            id: satuan.satuan.id,
                            kode: satuan.satuan.kode
                        }
                    });
                }

                // item biasa & bundle
                relasi_transaksi_penjualan.push({
                    absen: true,
                    item_kode: relasi.item_kode,
                    jumlah: relasi.jumlah,
                    harga: relasi.harga,
                    subtotal: relasi.subtotal,
                    item: {
                        kode: relasi.item.kode,
                        kode_barang: relasi.item.kode,
                        nama: relasi.item.nama,
                        satuan_pembelians: satuan_pembelians
                    }
                });

                if (relasi.item.konsinyasi == 2) {
                    for (var j = 0; j < relasi.relasi_bundle.length; j++) {
                        var rb = relasi.relasi_bundle[j];
                        var satuan_pembelians = [];
                        for (var k = 0; k < rb.itemxx.satuan_pembelians.length; k++) {
                            var satuan = rb.itemxx.satuan_pembelians[k];
                            satuan_pembelians.push({
                                konversi: satuan.konversi,
                                satuan: {
                                    id: satuan.satuan.id,
                                    kode: satuan.satuan.kode
                                }
                            });
                        }

                        // item child bundle
                        relasi_transaksi_penjualan.push({
                            absen: false,
                            item_kode: rb.itemxx.kode,
                            jumlah: rb.jumlah,
                            harga: 0,
                            subtotal: 0,
                            item: {
                                kode: rb.itemxx.kode,
                                kode_barang: rb.itemxx.kode,
                                nama: rb.itemxx.nama,
                                satuan_pembelians: satuan_pembelians
                            }
                        });
                    }
                }

                // untuk item bonus
                for (var j = 0; j < relasi.relasi_bonus_penjualan.length; j++) {
                    var rbp = relasi.relasi_bonus_penjualan[j];
                    var satuan_pembelians = [];
                    for (var k = 0; k < rbp.bonus.satuan_pembelians.length; k++) {
                        var satuan = rbp.bonus.satuan_pembelians[k];
                        satuan_pembelians.push({
                            konversi: satuan.konversi,
                            satuan: {
                                id: satuan.satuan.id,
                                kode: satuan.satuan.kode
                            }
                        });
                    }

                    // item bonus
                    relasi_transaksi_penjualan.push({
                        absen: false,
                        item_kode: rbp.bonus.kode,
                        jumlah: rbp.jumlah,
                        harga: 0,
                        subtotal: 0,
                        item: {
                            kode: rbp.bonus.kode,
                            kode_barang: rbp.bonus.kode,
                            nama: rbp.bonus.nama,
                            satuan_pembelians: satuan_pembelians
                        }
                    });
                }
            }

            // var oi = relasi_transaksi_penjualan.length;
            // for (var h = 0; h < 2; h++) {
            //     // for (var i = 0; i < oi; i++) {
            //         relasi_transaksi_penjualan.push(relasi_transaksi_penjualan[0]);
            //     // }
            // }

            var min_item = 10;
            var max_item = 20;
            var jumlah_item = relasi_transaksi_penjualan.length;
            // var jumlah_item = 30;
            var jumlah_halaman = Math.floor(jumlah_item / max_item) + 1;
            if (jumlah_item % max_item > min_item) {
                jumlah_halaman++;
            }

            // FAKTUR
            // if (data_perusahaan.tanggal_npwp == '0000-00-00') {
            //     var tanggal_npwp = '--/--/----';
            // } else {
            //     var tanggal_mpwp = ymd2dmyMiring(data_perusahaan.tanggal_npwp);
            // }

            //  if (data_perusahaan.tanggal_pkp == '0000-00-00') {
            //     var tanggal_pkp = '--/--/----';
            // } else {
            //     var tanggal_mpwp = ymd2dmyMiring(data_perusahaan.tanggal_pkp);
            // }

            var subtotal_total = 0;
            var nomor_item = 1;
            var subtotal_halaman_ = 0;
            for (var i = 0; i < jumlah_halaman; i++) {
                var table_head = ``+
                    `<table width="100%" class="table-head">
                        <tr>
                            <td class="kmkContainer" rowspan="6" align="left" style="width: 80mm;" valign="top" style="margin-right: 0">
                            </td>
                            <td rowspan="6" align="left" style="width: 100mm;" valign="bottom">
                                <p style="font-weight: bold; font-size: 12px; text-transform: uppercase; line-height:1;">Toko Eceran & Grosir</p>
                                <p style="font-weight: bold; font-size: 14px; text-transform: uppercase; line-height:1.2;">${data_perusahaan.nama}</p>
                                <p style="line-height:1;">${data_perusahaan.alamat}</p>
                                <p style="text-decoration:underline; line-height:1;margin-bottom:2px;">${data_perusahaan.email}</p>
                                <p style="line-height:1;">Telp. ${data_perusahaan.telepon} | WA ${data_perusahaan.wa_penjualan}</p>
                                <p style="line-height:1;">No. NPWP ${npwp} | Tgl. SK ${ymd2dmyMiring(data_perusahaan.tanggal_npwp)}</p>
                                <p style="line-height:1;">No. PKP ${pkp} | Tgl. SK ${ymd2dmyMiring(data_perusahaan.tanggal_pkp)}</p>
                            </td>
                            <td colspan="3">
                                <p style="font-weight: bold; font-size: 14px; margin-bottom: 2px; z-index: 10;">FAKTUR [`+(i+1)+`/`+jumlah_halaman+`]</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40mm;">
                                <p class="header_grosir"> Kode Transaksi </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td style="width: 60mm;">
                                <p class="header_grosir"> `+transaksi_penjualan.kode_transaksi+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Tanggal & Waktu </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+transaksi_penjualan.timestamp+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Operator (E) </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+pangkasNama(kasir_eceran)+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Pelanggan </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+(transaksi_penjualan.pelanggan==null?`-`:pangkasNama(transaksi_penjualan.pelanggan.nama))+` </p>
                            </td>
                        </tr>
                        <tr class="invisible"></tr>
                    </table>`;

                $('body').append($(table_head));

                var akhir_halaman = jumlah_item - (i * max_item);
                if (jumlah_item > (i + 1) * max_item) {
                    akhir_halaman = max_item;
                }

                var subtotal_halaman = 0;
                var tr = '';
                for (var j = 0; j < akhir_halaman; j++) {
                    var relasi = relasi_transaksi_penjualan[i * max_item + j];

                    if (j == 0) subtotal_halaman = 0;
                    subtotal_halaman += parseFloat(relasi.subtotal);
                    subtotal_total += parseFloat(relasi.subtotal);

                    var satuan_item = [];
                    for (var k = 0; k < relasi.item.satuan_pembelians.length; k++) {
                        var satuan = {
                            id: relasi.item.satuan_pembelians[k].satuan.id,
                            kode: relasi.item.satuan_pembelians[k].satuan.kode,
                            konversi: relasi.item.satuan_pembelians[k].konversi
                        }
                        satuan_item.push(satuan);
                    }

                    // bikin dua satuan
                    var jumlah1 = 0;
                    var jumlah2 = 0;
                    var jumlah1_text = '';
                    var jumlah2_text = '';
                    var satuan1 = null;
                    var satuan2 = null;
                    var temp_jumlah = relasi.jumlah;
                    for (var l = 0; l < satuan_item.length; l++) {
                        if (temp_jumlah > 0) {
                            var satuan = satuan_item[l];
                            var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                            if (jumlah_jual > 0) {
                                if (jumlah1_text == '') {
                                    jumlah1_text += jumlah_jual;
                                    jumlah1_text += ' ';
                                    jumlah1_text += satuan.kode;
                                    jumlah1 += jumlah_jual;
                                    satuan1 = satuan;
                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                } else {
                                    if (temp_jumlah % satuan.konversi > 0) {
                                        continue;
                                    } else {
                                        jumlah2_text += jumlah_jual;
                                        jumlah2_text += ' ';
                                        jumlah2_text += satuan.kode;
                                        jumlah2 += jumlah_jual;
                                        satuan2 = satuan;
                                        temp_jumlah = temp_jumlah % satuan.konversi;
                                    }
                                }
                            }
                        }
                    }

                    var harga_eceran = 0;
                    var harga_grosir = 0;
                    for (var l = 0; l < hargas.length; l++) {
                        // if (jumlah1 >= hargas[l].jumlah && relasi.item_kode == hargas[l].item_kode && parseFloat(hargas[l].eceran) > harga_eceran) {
                        if (jumlah1 >= hargas[l].jumlah && hargas[l].konversi == satuan1.konversi && relasi.item_kode == hargas[l].item_kode && parseFloat(hargas[l].eceran) > harga_eceran) {
                            harga_eceran = parseFloat(hargas[l].eceran);
                            harga_grosir = parseFloat(hargas[l].grosir);
                        }
                    }

                    var harga = 0;
                    if (transaksi_penjualan.pelanggan == null) {
                        harga = harga_eceran;
                        // if (transaksi_penjualan.status == 'gosir') {
                        //     harga = harga_grosir;
                        // }
                    } else if (transaksi_penjualan.pelanggan.level == 'eceran') {
                        harga = harga_eceran;
                        // if (transaksi_penjualan.status == 'gosir') {
                        //     harga = harga_grosir;
                        // }
                    } else {
                        harga = harga_grosir;
                    }

                    if (relasi.harga == 0) {
                        harga = 0;
                    }

                    var nomor = nomor_item;
                    if (!relasi.absen) nomor = '';
                    else nomor_item++;

                    tr += ''+
                        '<tr>'+
                            // '<td>'+ (i * max_item + j + 1) +'</td>'+
                            '<td>'+ nomor +'</td>'+
                            '<td>'+ relasi.item.kode_barang +'</td>'+
                            '<td>'+ relasi.item.nama +'</td>'+
                            '<td style="border-right: none !important">'+jumlah1_text+'</td>'+
                            '<td style="border-left: none !important">'+jumlah2_text+'</td>'+
                            '<td align="right">Rp'+ parseFloat(harga).toLocaleString() +',-</td>'+
                            '<td align="right">Rp'+ parseFloat(relasi.subtotal).toLocaleString() +',-</td>'+
                        '</tr>';
                }

                subtotal_halaman_ += subtotal_halaman;
                var nomor_subtotal = '1';
                if (i > 0) nomor_subtotal = '1 - ' + (i + 1);
                var tr_subtotal = ``+
                    `<tr>
                        <td colspan="3" style="border: none !important;"></td>
                        <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                        <td align="right">Rp`+subtotal_halaman_.toLocaleString()+`,-</td>
                    </tr>`;
                if (akhir_halaman <= 0) {
                    tr_subtotal = ``+
                        `<tr style="color: white;">
                            <th class="invisible" colspan="1" width="4%" style="text-align: center; border: none !important;">No</th>
                            <th class="invisible" colspan="1" width="16%" style="text-align: center; border: none !important;">Kode Item</th>
                            <th class="invisible" colspan="1" width="35%" style="text-align: center; border: none !important;">Item</th>
                            <th class="invisible" colspan="2" width="15%" style="text-align: center; border: none !important;">Jumlah</th>
                            <th class="invisible" colspan="1" width="14%" style="text-align: center; border: none !important;">Harga</th>
                            <th class="invisible" colspan="1" width="16%" style="text-align: center; border: none !important;">Total</th>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="6" style="text-transform: uppercase;">
                                <p style="font-weight: bold; padding-top: 5px; font-size:10px">TERBILANG :</p>
                                <p style="font-size:10px"># `+terbilang(grand_total)+` RUPIAH #</p>
                            </td>
                            <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                            <td align="right">Rp`+subtotal_total.toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Nego & Potongan Penjualan</td>
                            <td align="right">Rp`+parseFloat(potongan_penjualan).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Deposito Pelanggan</td>
                            <td align="right">Rp`+parseFloat(deposito).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Jumlah Bayar</td>
                            <td  align="right">Rp`+parseFloat(jumlah_bayar).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Grand Total (+Ongkos Kirim)</td>
                            <td  align="right">Rp`+parseFloat(grand_total).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Kembalian</td>
                            <td  align="right">Rp`+parseFloat(kembali).toLocaleString()+`,-</td>
                        </tr>`;
                } else if (akhir_halaman <= min_item) {
                    tr_subtotal = ``+
                        `<tr>
                            <td colspan="3" rowspan="6" style="text-transform: uppercase;">
                                <p style="font-weight: bold; padding-top: 5px; font-size:10px">TERBILANG :</p>
                                <p style="font-size:10px"># `+terbilang(grand_total)+` RUPIAH #</p>
                            </td>
                            <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                            <td  align="right">Rp`+subtotal_total.toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Nego & Potongan Penjualan</td>
                            <td align="right">Rp`+parseFloat(potongan_penjualan).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Deposito Pelanggan</td>
                            <td align="right">Rp`+parseFloat(deposito).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Jumlah Bayar</td>
                            <td  align="right">Rp`+parseFloat(jumlah_bayar).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Grand Total (+Ongkos Kirim)</td>
                            <td align="right">Rp`+parseFloat(grand_total).toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Kembalian</td>
                            <td  align="right">Rp`+parseFloat(kembali).toLocaleString()+`,-</td>
                        </tr>`;
                }

                var jumlah_tr_kosong = max_item - akhir_halaman + 2;
                // console.log(akhir_halaman);
                if (akhir_halaman > 10) jumlah_tr_kosong += 5;
                if (akhir_halaman > 13) jumlah_tr_kosong -= 1;
                var tr_kosong = '';
                for (var j = 0; j < jumlah_tr_kosong; j++) {
                    tr_kosong += ``+
                        `<tr style="color: white;">
                            <td colspan="1" class="invisible" style="border: none !important;">o</td>
                            <td colspan="1" class="invisible" style="border: none !important;">o</td>
                            <td colspan="1" class="invisible" style="border: none !important;">o</td>
                            <td colspan="2" class="invisible" style="border: none !important;">o</td>
                            <td colspan="1" class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                            <td colspan="1" class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                        </tr>`;
                }
                if (i == jumlah_halaman - 1) {
                    tr_kosong = '';
                }

                var thead = ``;
                if (akhir_halaman > 0) {
                    thead = ``+
                        `<tr>
                            <th colspan="1" width="4%" style="text-align: center;">No</th>
                            <th colspan="1" width="16%" style="text-align: center;">Kode Item</th>
                            <th colspan="1" width="35%" style="text-align: center;">Item</th>
                            <th colspan="2" width="15%" style="text-align: center;">Jumlah</th>
                            <th colspan="1" width="14%" style="text-align: center;">Harga</th>
                            <th colspan="1" width="16%" style="text-align: center;">Total</th>
                        </tr>`;
                }

                var margin_top = '';
                if (akhir_halaman > 0) margin_top = 'margin-top: 10px; ';
                var table_body = ``+
                    `<table width="100%" class="table table-bordered" style="`+margin_top+`border-top: none; border-bottom: none; border-right: none;">`+
                        `<tbody id="data">`+
                            thead+
                            tr+
                            tr_subtotal+
                            tr_kosong+
                        `</tbody>
                    </table>`;

                $('body').append($(table_body));

                if (i == jumlah_halaman - 1) {
                    var catatan_invoice = [];
                    for (var j = 0; j < catatans.length; j++) {
                        if (catatans[j].jenis_catatan == 'faktur') {
                            catatan_invoice.push(catatans[j]);
                        }
                    }
                    // console.log(catatan_invoice);

                    var max_char = 47;
                    var p1 = '';
                    var p2 = '';
                    var p3 = '';
                    var p1_text = '';
                    var p2_text = '';
                    var p3_text = '';

                    // console.log(catatan_invoice[0].length);
                    if (catatan_invoice[0].catatan.length <= max_char) {
                        p1_text = catatan_invoice[0].catatan;
                    } else {
                        var catatan = catatan_invoice[0].catatan.split(' ');
                        for (var j = 0; j < catatan.length; j++) {
                            if (p1_text.length + catatan[j].length < max_char) {
                                p1_text += catatan[j] + ' ';
                            } else if (p1_text.length + catatan[j].length == max_char) {
                                p1_text += catatan[j];
                            } else if (p2_text.length + catatan[j].length < max_char) {
                                p2_text += catatan[j] + ' ';
                            } else if (p2_text.length + catatan[j].length == max_char) {
                                p2_text += catatan[j];
                            } else if (p3_text.length + catatan[j].length < max_char) {
                                p3_text += catatan[j] + ' ';
                            } else if (p3_text.length + catatan[j].length == max_char) {
                                p3_text += catatan[j];
                            }
                        }
                    }
                    if (p1_text.length > 0) {
                        p1 = '<span>- </span>'+p1_text;
                    }
                    if (p2_text.length > 0) {
                        p2 = '<span class="invisible">- </span>'+p2_text;
                    }
                    if (p3_text.length > 0) {
                        p3 = '<span class="invisible">- </span>'+p3_text;
                    }
                    // console.log(p1_text, p1.length);
                    // console.log(p2_text, p2.length);
                    // console.log(p3_text, p3.length);

                    // console.log(catatan_invoice[1].length);
                    if (catatan_invoice[1].catatan.length <= max_char) {
                        if (p2_text == '') {
                            p2_text = catatan_invoice[1].catatan;
                        } else {
                            p3_text = catatan_invoice[1].catatan;
                        }
                    } else {
                        var catatan = catatan_invoice[1].catatan.split(' ');
                        if (p2_text == '') {
                            for (var j = 0; j < catatan.length; j++) {
                                if (p2_text.length + catatan[j].length < max_char) {
                                    p2_text += catatan[j] + ' ';
                                } else if (p2_text.length + catatan[j].length == max_char) {
                                    p2_text += catatan[j];
                                } else if (p3_text.length + catatan[j].length < max_char) {
                                    p3_text += catatan[j] + ' ';
                                } else if (p3_text.length + catatan[j].length == max_char) {
                                    p3_text += catatan[j];
                                }
                            }
                        } else {
                            for (var j = 0; j < catatan.length; j++) {
                                if (p3_text.length + catatan[j].length < max_char) {
                                    p3_text += catatan[j] + ' ';
                                } else if (p3_text.length + catatan[j].length == max_char) {
                                    p3_text += catatan[j];
                                }
                            }
                        }
                    }
                    if (p2_text.length > 0 && p2.length <= 0) {
                        p2 = '<span>- </span>'+p2_text;
                        if (p3_text.length > 0) {
                            p3 = '<span class="invisible">- </span>'+p3_text;
                        }
                    }
                    if (p3_text.length > 0 && p3.length <= 0) {
                        p3 = '<span>- </span>'+p3_text;
                    }
                    // console.log(p1_text, p1.length);
                    // console.log(p2_text, p2.length);
                    // console.log(p3_text, p3.length);

                    // console.log(catatan_invoice[2].length);
                    if (catatan_invoice[2].catatan.length <= max_char) {
                        if (p3_text == '') {
                            p3_text = catatan_invoice[2].catatan;
                        }
                    } else {
                        var catatan = catatan_invoice[2].catatan.split(' ');
                        if (p3_text == '') {
                            for (var j = 0; j < catatan.length; j++) {
                                if (p3_text.length + catatan[j].length < max_char) {
                                    p3_text += catatan[j] + ' ';
                                } else if (p3_text.length + catatan[j].length == max_char) {
                                    p3_text += catatan[j];
                                }
                            }
                        } else {
                            // for (var j = 0; j < catatan.length; j++) {
                            //     if (p3_text.length + catatan[j].length < max_char) {
                            //         p3_text += catatan[j] + ' ';
                            //     } else if (p3_text.length + catatan[j].length == max_char) {
                            //         p3_text += catatan[j];
                            //     }
                            // }
                        }
                    }
                    // console.log(p1_text, p1.length);
                    // console.log(p2_text, p2.length);
                    // console.log(p3_text, p3.length);
                    if (p3_text.length > 0 && p3.length <= 0) {
                        p3 = '<span>- </span>'+p3_text;
                    }

                    /*var table_foot = ``+
                        `<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
                            <tbody>
                                <tr>
                                    <td width="54%" style="padding-top: 5px; border: 1px solid">
                                            <p style="font-size: 10px;">CATATAN :</p>
                                            <p style="font-size: 10px;"><span>- </span>TESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;"><span class="invisible">- </span>TESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;">NOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;">KMK MANAGEMENT</p>
                                    </td>
                                    <td width="23%" align="center" class="invisible" style="border-top:0; text-decoration: overline;">
                                        <p style="margin-top: 65px; font-size: 11px;">Pembeli (Nama & Stampel)</p>
                                    </td>
                                    <td width="23%" align="right" style="border-top:0; text-decoration: overline;">
                                        <p style="margin-top: 65px; font-size: 11px;">Penjual (Nama & Stampel)</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>`;*/

                    var nama_pelanggan = '';
                    var nama_grosir = '';
                    if (transaksi_penjualan.pelanggan != null) {
                        nama_pelanggan = transaksi_penjualan.pelanggan.nama;
                    }
                    if (kasir_grosir != null) {
                        nama_grosir = kasir_grosir;
                    }

                    var table_foot = ``+
                        `<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
                            <tbody>
                                <tr>
                                    <td width="54%" style="padding-top: 5px; border: 1px solid;">
                                            <p style="font-size: 10px;  font-weight: bold">CATATAN :</p>
                                            <p style="font-size: 10px; text-transform: uppercase;">`+p1+`</p>
                                            <p style="font-size: 10px; text-transform: uppercase;">`+p2+`</p>
                                            <p style="font-size: 10px; text-transform: uppercase;">`+p3+`</p>
                                            <p class="invisible" style="font-size: 10px;">KMK MANAGEMENT</p>
                                            <p style="font-size: 10px;  font-weight: bold">KMK MANAGEMENT</p>
                                    </td>
                                    <td class="invisible stempel" width="23%" align="right" style="border-top:0;">
                                        <p class="invisible" style="width: 95%; text-align: center;">Pembeli</p>
                                        <p style="width: 95%; text-align: center; margin-top: 43px; border-bottom: solid 1px #000;">&nbsp;`+pangkasNama(nama_pelanggan)+`</p>
                                        <p style="width: 95%; text-align: center;">Pembeli</p>
                                    </td>
                                    <td class="stempel" width="23%" align="right" style="border-top:0;">
                                        <p class="invisible" style="width: 95%; text-align: center;">Penjual</p>
                                        <p style="width: 95%; text-align: center; margin-top: 43px; border-bottom: solid 1px #000;">&nbsp;`+pangkasNama(nama_grosir)+`</p>
                                        <p style="width: 95%; text-align: center;">Penjual (Nama & Stampel)</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>`;

                    $('body').append($(table_foot));

                    if (akhir_halaman < 0) akhir_halaman = 0;
                    // console.log(akhir_halaman);
                    var jumlah_tr_kosong = max_item - akhir_halaman + 2 - 15;
                    if (akhir_halaman <= 0) jumlah_tr_kosong += 2;
                    if (akhir_halaman >= 1 && akhir_halaman <= 2) jumlah_tr_kosong += 1;
                    // if (akhir_halaman > 9) jumlah_tr_kosong -= 2;
                    jumlah_tr_kosong += 5;
                    // console.log(jumlah_tr_kosong);
                    var tr_kosong = '';
                    if (jumlah_tr_kosong <= 0) {
                        tr_kosong += ``+
                            `<tr style="color: white;">
                                <td colspan="6" style="border: none !important;" class="invisible">..........</td>
                            </tr>`;
                    }
                    for (var j = 0; j < jumlah_tr_kosong; j++) {
                        tr_kosong += ``+
                            `<tr style="color: white;">
                                <td colspan="1" class="invisible" style="border: none !important;">o</td>
                                <td colspan="1" class="invisible" style="border: none !important;">o</td>
                                <td colspan="1" class="invisible" style="border: none !important;">o</td>
                                <td colspan="2" class="invisible" style="border: none !important;">o</td>
                                <td colspan="1" class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                                <td colspan="1" class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                            </tr>`;
                        if (j == jumlah_tr_kosong - 1) {
                            tr_kosong += ``+
                                `<tr style="color: white;">
                                    <td colspan="6" style="border: none !important;" class="invisible">..........</td>
                                </tr>`;
                        }
                    }

                    var margin_top = '';
                    if (akhir_halaman > 0) margin_top = 'margin-top: 10px; ';
                    var table_body = ``+
                        `<table width="100%" class="table table-bordered" style="`+margin_top+`border-top: none; border-bottom: none; border-right: none;">`+
                            `<tbody id="data">`+
                                tr_kosong+
                            `</tbody>
                        </table>`;

                    $('body').append($(table_body));
                }
            }

            // window.print();
            // var url = '{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}';
            // var ww = window.open(url, '_self');

            $('.kmkContainer').append($kmk);
            // window.print();
            // var url = '{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}';
            // var ww = window.open(url, '_self');
        });

    </script>
@endsection
