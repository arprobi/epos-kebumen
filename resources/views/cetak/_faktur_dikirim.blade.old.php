@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }

        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }

        .header_grosir{
            font-size: 10px;
            line-height: 1;
        }
    </style>
@endsection

@section('app.body')
@endsection

@section('app.script')
    <script type="text/javascript" charset="utf-8" async defer>
        var transaksi_penjualan = null;
        var potongan_penjualan = 0;
        var grand_total = 0;
        var deposito = 0;
        var hargas = [];
        var relasi_transaksi_penjualan = [];

        $(document).ready(function() {
            transaksi_penjualan = '{{ $transaksi_penjualan }}';
            transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
            transaksi_penjualan = JSON.parse(transaksi_penjualan);

            hargas = '{{ $hargas }}';
            hargas = hargas.replace(/&quot;/g, '"');
            hargas = JSON.parse(hargas);

            potongan_penjualan = '{{ $potongan_penjualan }}';
            potongan_penjualan = potongan_penjualan.replace(/&quot;/g, '"');
            potongan_penjualan = JSON.parse(potongan_penjualan);

            grand_total = '{{ $grand_total }}';
            grand_total = grand_total.replace(/&quot;/g, '"');
            grand_total = JSON.parse(grand_total);

            deposito = transaksi_penjualan.nominal_titipan;

            // relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            // relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            // relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            var temp_relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            temp_relasi_transaksi_penjualan = temp_relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            temp_relasi_transaksi_penjualan = JSON.parse(temp_relasi_transaksi_penjualan);

            for (var i = 0; i < temp_relasi_transaksi_penjualan.length; i++) {
                relasi_transaksi_penjualan.push(temp_relasi_transaksi_penjualan[i]);
                for (var j = 0; j < temp_relasi_transaksi_penjualan[i].relasi_bonus_penjualan.length; j++) {
                    var rbp = temp_relasi_transaksi_penjualan[i].relasi_bonus_penjualan[j];
                    relasi_transaksi_penjualan.push({
                        item_kode: rbp.bonus.kode,
                        jumlah: rbp.jumlah,
                        harga: 0,
                        subtotal: 0,
                        item: {
                            kode: rbp.bonus.kode,
                            nama: rbp.bonus.nama,
                            satuan_pembelians: [
                                {
                                    konversi: 1,
                                    satuan: {
                                        id: 0,
                                        kode: 'BUAH'
                                    }
                                }
                            ],
                        }
                    });
                }
            }

            var jumlah_item = relasi_transaksi_penjualan.length;
            // var jumlah_item = 30;
            var jumlah_halaman = Math.floor(jumlah_item / 25) + 1;
            if (jumlah_item % 25 > 15) {
                jumlah_halaman++;
            }

            var subtotal_total = 0;
            for (var i = 0; i < jumlah_halaman; i++) {
                var table_head = ``+
                    `<table width="100%" class="table-head">
                        <tr>
                            <td rowspan="5" align="left" style="width: 80mm;" valign="top" style="margin-right: 0">
                                <img width=173 height=63 src="{{ URL::to('images/logo1.png') }}" style="margin-top: 4px">
                            </td>
                            <td rowspan="5" align="left" style="width: 100mm;" valign="bottom">
                                <p style="font-weight: bold; font-size: 12px; text-transform: uppercase; line-height:1;">Toko Eceran & Grosir</p>
                                <p style="font-weight: bold; font-size: 14px; text-transform: uppercase; line-height:1.2;">KENCANA MULYA</p>
                                <p style="line-height:1;">Jl. Puring-Petanahan No. 3 Karangduwur</p>
                                <p style="text-decoration:underline; line-height:1;">kencanamulyakarangduwur@gmail.com</p>
                                <p style="line-height:1.5;">Telp. (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
                            </td>
                            <td colspan="3">
                                <p style="font-weight: bold; font-size: 14px; margin-bottom: 2px; z-index: 10;">FAKTUR [`+(i+1)+`/`+jumlah_halaman+`]</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40mm;">
                                <p class="header_grosir"> Kode Transaksi </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td style="width: 60mm;">
                                <p class="header_grosir"> `+transaksi_penjualan.kode_transaksi+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Tanggal & Waktu </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+transaksi_penjualan.timestamp+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Kasir </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+transaksi_penjualan.user.username+` </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="header_grosir"> Pembeli </p>
                            </td>
                            <td>
                                <p class="header_grosir"> : </p>
                            </td>
                            <td>
                                <p class="header_grosir"> `+(transaksi_penjualan.pelanggan==null?`-`:transaksi_penjualan.pelanggan.nama)+` </p>
                            </td>
                        </tr>
                    </table>`;

                $('body').append($(table_head));

                var akhir_halaman = jumlah_item - (i * 25);
                if (jumlah_item > (i + 1) * 25) {
                    akhir_halaman = 25;
                }

                var subtotal_halaman = 0;
                var tr = '';
                for (var j = 0; j < akhir_halaman; j++) {
                    var relasi = relasi_transaksi_penjualan[i * 25 + j];

                    if (j == 0) subtotal_halaman = 0;
                    subtotal_halaman += parseFloat(relasi.subtotal);
                    subtotal_total += parseFloat(relasi.subtotal);

                    var satuan_item = [];
                    for (var k = 0; k < relasi.item.satuan_pembelians.length; k++) {
                        var satuan = {
                            id: relasi.item.satuan_pembelians[k].satuan.id,
                            kode: relasi.item.satuan_pembelians[k].satuan.kode,
                            konversi: relasi.item.satuan_pembelians[k].konversi
                        }
                        satuan_item.push(satuan);
                    }

                    // bikin dua satuan
                    var jumlah1 = 0;
                    var jumlah2 = 0;
                    var jumlah1_text = '';
                    var jumlah2_text = '';
                    var satuan1 = null;
                    var satuan2 = null;
                    var temp_jumlah = relasi.jumlah;
                    for (var l = 0; l < satuan_item.length; l++) {
                        if (temp_jumlah > 0) {
                            var satuan = satuan_item[l];
                            var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                            if (jumlah_jual > 0) {
                                if (jumlah1_text == '') {
                                    jumlah1_text += jumlah_jual;
                                    jumlah1_text += ' ';
                                    jumlah1_text += satuan.kode;
                                    jumlah1 += jumlah_jual;
                                    satuan1 = satuan;
                                    temp_jumlah = temp_jumlah % satuan.konversi;
                                } else {
                                    if (temp_jumlah % satuan.konversi > 0) {
                                        continue;
                                    } else {
                                        jumlah2_text += jumlah_jual;
                                        jumlah2_text += ' ';
                                        jumlah2_text += satuan.kode;
                                        jumlah2 += jumlah_jual;
                                        satuan2 = satuan;
                                        temp_jumlah = temp_jumlah % satuan.konversi;
                                    }
                                }
                            }
                        }
                    }

                    var harga_eceran = 0;
                    var harga_grosir = 0;
                    for (var l = 0; l < hargas.length; l++) {
                        if (jumlah1 >= hargas[l].jumlah) {
                            harga_eceran = parseFloat(hargas[l].eceran);
                            harga_grosir = parseFloat(hargas[l].grosir);
                        }
                    }

                    var harga = 0;
                    if (transaksi_penjualan.pelanggan == null) {
                        harga = harga_eceran;
                        // if (transaksi_penjualan.status == 'gosir') {
                        //     harga = harga_grosir;
                        // }
                    } else if (transaksi_penjualan.pelanggan.level == 'eceran') {
                        harga = harga_eceran;
                        if (transaksi_penjualan.status == 'gosir') {
                            harga = harga_grosir;
                        }
                    } else {
                        harga = harga_grosir;
                    }

                    tr += ``+
                        `<tr>
                            <td>`+ (i * 25 + j + 1) +`</td>
                            <td>`+ relasi.item_kode +`</td>
                            <td>`+ relasi.item.nama +`</td>
                            <td style="border-right: none !important">`+jumlah1_text+`</td>
                            <td style="border-left: none !important">`+jumlah2_text+`</td>
                            <td align="right">Rp`+ parseFloat(harga).toLocaleString() +`,-</td>
                            <td align="right">Rp`+ parseFloat(relasi.subtotal).toLocaleString() +`,-</td>
                        </tr>`;
                }

                var nomor_subtotal = '1';
                if (i > 0) nomor_subtotal = '1 - ' + (i + 1);
                var tr_subtotal = ``+
                    `<tr>
                        <td colspan="3" style="border: none !important;"></td>
                        <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                        <td align="right">Rp`+subtotal_halaman.toLocaleString()+`,-</td>
                    </tr>`;
                if (akhir_halaman <= 0) {
                    tr_subtotal = ``+
                        `<tr style="color: white;">
                            <th class="invisible" width="4%" style="text-align: center; border: none !important;">No</th>
                            <th class="invisible" width="16%" style="text-align: center; border: none !important;">Kode Item</th>
                            <th class="invisible" width="35%" style="text-align: center; border: none !important;">Nama Item</th>
                            <th class="invisible" width="15%" style="text-align: center; border: none !important;">Jumlah</th>
                            <th class="invisible" width="14%" style="text-align: center; border: none !important;">Harga</th>
                            <th class="invisible" width="16%" style="text-align: center; border: none !important;">Total</th>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="4" style="text-transform: uppercase;">
                                <p style="font-weight: bold; padding-top: 5px; font-size:10px">TERBILANG :</p>
                                <p style="font-size:10px"># {{ App\Util::terbilang(51250500)}} RUPIAH #</p>
                            </td>
                            <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                            <td  align="right">Rp`+subtotal_total.toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-weight: bold">Potongan Harga</td>
                            <td  align="right">(Rp`+parseFloat(potongan_penjualan).toLocaleString()+`,-)</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-weight: bold">Deposito Pelanggan</td>
                            <td  align="right">(Rp`+parseFloat(deposito).toLocaleString()+`,-)</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-weight: bold">Grand Total</td>
                            <td  align="right">Rp`+parseFloat(grand_total).toLocaleString()+`,-</td>
                        </tr>`;
                } else if (akhir_halaman <= 15) {
                    tr_subtotal = ``+
                        `<tr>
                            <td colspan="3" rowspan="4" style="text-transform: uppercase;">
                                <p style="font-weight: bold; padding-top: 5px; font-size:10px">TERBILANG :</p>
                                <p style="font-size:10px"># {{ App\Util::terbilang(51250500)}} RUPIAH #</p>
                            </td>
                            <td colspan="3" style="font-weight: bold">Sub Total `+nomor_subtotal+`</td>
                            <td  align="right">Rp`+subtotal_total.toLocaleString()+`,-</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Potongan Harga</td>
                            <td align="right">(Rp`+parseFloat(potongan_penjualan).toLocaleString()+`,-)</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Deposito Pelanggan</td>
                            <td align="right">(Rp`+parseFloat(deposito).toLocaleString()+`,-)</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight: bold">Grand Total</td>
                            <td align="right">Rp`+parseFloat(grand_total).toLocaleString()+`,-</td>
                        </tr>`;
                }

                var jumlah_tr_kosong = 25 - akhir_halaman + 4;
                var tr_kosong = '';
                for (var j = 0; j < jumlah_tr_kosong; j++) {
                    tr_kosong += ``+
                        `<tr style="color: white;">
                            <td class="invisible" style="border: none !important;">o</td>
                            <td class="invisible" style="border: none !important;">o</td>
                            <td class="invisible" style="border: none !important;">o</td>
                            <td class="invisible" style="border: none !important;">o</td>
                            <td class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                            <td class="invisible" style="border: none !important;" align="right">Rp0,-</td>
                        </tr>`;
                }
                if (i == jumlah_halaman - 1) {
                    tr_kosong = '';
                }

                var thead = ``+
                    `<thead>
                    </thead>`;
                if (akhir_halaman > 0) {
                    thead = ``+
                        `<thead>
                            <tr>
                                <th colspan="1" width="4%" style="text-align: center;">No</th>
                                <th colspan="1" width="16%" style="text-align: center;">Kode Item</th>
                                <th colspan="1" width="35%" style="text-align: center;">Nama Item</th>
                                <th colspan="2" width="15%" style="text-align: center;">Jumlah</th>
                                <th colspan="1" width="14%" style="text-align: center;">Harga</th>
                                <th colspan="1" width="16%" style="text-align: center;">Total</th>
                            </tr>
                        </thead>`;
                }

                var margin_top = '';
                if (akhir_halaman > 0) margin_top = 'margin-top: 10px; ';
                var table_body = ``+
                    `<table width="100%" class="table table-bordered" style="`+margin_top+`border-top: none; border-bottom: none;">`+
                        thead+
                        `<tbody id="data">`+
                            tr+
                            tr_subtotal+
                            tr_kosong+
                        `</tbody>
                    </table>`;

                $('body').append($(table_body));

                if (i == jumlah_halaman - 1) {
                    var table_foot = ``+
                        `<table class="table" style="margin-top: 10px; margin-bottom:0" width="100%">
                            <tbody>
                                    <td width="54%" style="padding-top: 5px; border: 1px solid">
                                            <p style="font-size: 10px;">CATATAN :</p>
                                            <p style="font-size: 10px;">NOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;">NOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;">NOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTESNOTES</p>
                                            <p style="font-size: 10px;">KMK Management</p>
                                    </td>
                                    <td width="23%" align="center" class="invisible" style="border-top:0; text-decoration: overline;">
                                        <p style="margin-top: 65px; font-size: 11px;">Pembeli (Nama & Stampel)</p>
                                    </td>
                                    <td width="23%" align="right" style="border-top:0; text-decoration: overline;">
                                        <p style="margin-top: 65px; font-size: 11px;">Penjual (Nama & Stampel)</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>`;

                    $('body').append($(table_foot));
                }
            }

            window.print();
            var url = '{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}';
            var ww = window.open(url, '_self');
        });

    </script>

@endsection
