@extends('layouts.print')

@section('app.style')
    <style type="text/css">
        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        p {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
        }

        @page WordSection1
        {
            size:24.13cm 13.97cm;
            margin:0;
            margin-left: -20px;
            margin-right: 0;
            margin-bottom: 0;
            margin-top: -10px;
        }

        div.WordSection1
        { 
            page:WordSection1;
            font-family: "Arial";
        }
        
        .table {
            border-collapse: collapse !important;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 5px;
        }

        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 0.5px;
            padding-top: 0.5px;
            line-height: 12px;
            vertical-align: top;
            border-top: 1px solid #000;
            font-size: 10px;
            word-wrap: break-word;
        }

        .table-head > thead > tr > th,
        .table-head > tbody > tr > th,
        .table-head > tfoot > tr > th,
        .table-head > thead > tr > td,
        .table-head > tbody > tr > td,
        .table-head > tfoot > tr > td {
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
            padding-top: 1px;
            vertical-align: top;
            line-height: 14px;
            font-size: 12px;
        }
    </style>

    <style type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        @font-face {
            font-family: "RobotoCondensed-Light";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Light.ttf') }}");
        }
        @font-face {
            font-family: "RobotoCondensed-Regular";
            src: url("{{ asset('fonts/roboto/RobotoCondensed-Regular.ttf') }}");
        }

        @font-face {
            font-family: "ClarendonBT";
            src: url("{{ asset('fonts/ClarendonBT.ttf') }}");
        }

        @font-face {
            font-family: "Arial";
            src: url("{{ asset('fonts/Arial.ttf') }}");
        }

        @page {
            size: 8cm 100cm;
            margin: 0mm 0mm 0mm 0mm;
        }

        @media print {
            margin-top: 0;
        }

        .zero{
            margin: 0;
            padding: 0;
            font-size: 10px;
            line-height: 1.2;
        }
        #cetak {
            font-size: 10px;
        }

        #toko {
            font-family: 'ClarendonBT';
            font-size: 15.6px;
            font-weight: bold;
        }

        .text-center {
            text-align: center;
            margin: 0;
        }

        #content td {
            font-size: 9.5px;
            text-transform: uppercase;
        }
        #cetak, #content {
            font-family: 'Arial';
        }

        table tr td.ket{
            font-size: 10px;
            text-transform: uppercase;
        }
    </style>
@endsection

@section('app.body')
    <div id="cetak">
        <div id="header" style="margin-bottom: 10px;">
            {{-- <center>
                <img align="middle" src="{{ URL::to('images/logo.png') }}" class="profile_img" style="width: 150px; height: 54px;">
                <p id="toko" class="zero" style="font-size: 15.6px; font-family: ClarendonBT; font-weight: bold;">KENCANA MULYA</p>
                <p class="zero" style="font-size: 10px;">KARANGDUWUR - PETANAHAN</p>
                <p class="zero" style="margin-top: 2px; font-size: 10px;">JL. PURING-PETANAHAN NO. 3 KARANGDUWUR</p>
                <p class="zero" style="font-size: 10px;">kencanamulyakarangduwur@gmail.com</p>
                <p class="zero" style="margin-top: 2px; font-size: 10px;">(0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</p>
            </center> --}}
            <center>
                <img align="middle" src="{{ URL::to('images/logo.png') }}" class="profile_img" style="width: 150px; height: 54px;">
                <p id="toko" class="zero">{{$data_perusahaan->nama}}</p>
                <p class="zero">KARANGDUWUR - PETANAHAN</p>
                <p class="zero">{{$data_perusahaan->alamat}}</p>
                <p class="zero" style="text-decoration: underline;">{{$data_perusahaan->email}}</p>
                <p class="zero">Telp. {{$data_perusahaan->telepon}} | WA {{$data_perusahaan->wa_penjualan}}</p>
                <p class="zero">No. NPWP {{$data_perusahaan->npwp}} | Tgl. SK <span class="tanggal">{{$data_perusahaan->tanggal_npwp}}</span></p>
                <p class="zero">No. PKP <span class="invisible">M</span>{{$data_perusahaan->pkp}} | Tgl. SK <span class="tanggal">{{$data_perusahaan->tanggal_pkp}}</span></p>
            </center>
        </div>
        {{-- <hr style="margin-bottom: 0"> --}}
        <table style="width: 100%; border-top: 0.5px solid;">
            <tr>
                {{-- <td class="ket">0001/TRAJ/01/11/2017</td> --}}
                <td class="ket">{{ $transaksi_penjualan->kode_transaksi }}</td>
                {{-- <td class="ket" style="text-align: right;">Kasir : Sutinem</td> --}}
                <td class="ket" style="text-align: right;">Operator : {{ $kasir_eceran }}</td>
            </tr>
            <tr>
                {{-- <td class="ket">01/11/2017 09:30:11</td> --}}
                <td class="ket">{{ $transaksi_penjualan->timestamp }}</td>
                <td class="ket" style="text-align:right; ">KMK MANAGEMENT</td>
            </tr>
        </table>
        {{-- <hr style="margin: 0"> --}}
        <div id="content">
            <table style="width: 100%; border-top: 0.5px solid;">
                <tbody>
                   <!--  <tr>
                       <td width="5%">25</td>
                       <td width="50%">MMM MMMMM MMMMM</td>
                       <td width="20%" class="" style="text-align: right; padding-right: 5px;" item_kode="">100 KRTN</td>
                       <td width="20%" style="text-align: right; padding-right: 5px;">100 BIJI</td>
                       <td width="18%" class="" style="text-align: right;">10.000.000</td>
                   </tr> -->
                    @php ($nomor = 1)
                    @foreach ($relasi_transaksi_penjualan as $i => $relasi)
                    <tr>
                        @if ($relasi['absen'])
                        <td width="5%">{{ $nomor }}</td>
                        @else
                        <td width="5%"></td>
                        @endif
                        <td width="50%">{{ $relasi['item']['nama_pendek'] }}</td>
                        <td width="20%" class="jumlah" style="text-align: left; padding-right: 5px;" item_kode="{{ $relasi['item_kode'] }}">{{ $relasi['jumlah'] }}</td>
                        <td width="20%" style="text-align: left; padding-right: 5px;"></td>
                        {{-- <td width="10%" class="duit" style="text-align: right;">{{ ($relasi['harga']) }}</td> --}}
                        <td width="18%" class="duit" style="text-align: right;">{{ ($relasi['subtotal']) }}</td>
                    </tr>
                    @if ($relasi['absen'])
                        @php ($nomor++)
                    @endif
                    @endforeach
                    <tr>
                        <td style="text-align: right;" colspan="4">Sub Total :</td>
                        <td colspan="2" class="duit" style="text-align: right; border-top:0.5px solid;">{{ $harga_total }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">Potongan Penjualan :</td>
                        <td colspan="2" class="duit potongan_penjualan" style="text-align: right;">{{ $potongan_penjualan }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">Jumlah Bayar :</td>
                        <td colspan="2" class="duit" style="text-align: right;">{{ $jumlah_bayar }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">Grand Total :</td>
                        <td colspan="2" class="duit" style="text-align: right;">{{ $grand_total }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: right;" colspan="4">Kembalian :</td>
                        <td colspan="2" style="text-align: right; tex">
                        @if($kembali > 0)
                            {{ \App\Util::angka($kembali) }}
                        @else
                            {{-- {{ \App\Util::angka($kembali*-1) }} --}}
                            0
                        @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            {{-- <table style="width: 100%; border-bottom: 0.5px solid;">
                <tbody>
                    
                </tbody>
            </table> --}}
            <div style="margin-top: 20px; margin-bottom: 0; padding-bottom: 0;">
                @foreach ($catatans as $i => $catatan)
                    <P style="text-align: center; text-transform: uppercase;" class="zero">
                        {{ $catatan->catatan }}
                    </P>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('app.script')
    <script type="text/javascript" src="{{ asset('js/epos.js') }}"></script>
    <script type="text/javascript">

        var transaksi_penjualan = '{{ json_encode($transaksi_penjualan) }}';
        transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
        transaksi_penjualan = JSON.parse(transaksi_penjualan);

        var relasi_transaksi_penjualan = '{{ json_encode($relasi_transaksi_penjualan) }}';
        relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
        relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

        $('.duit').each(function(index, el) {
            var nominal = parseFloat($(el).text());
            if (isNaN(nominal)) nominal = 0;

            $(el).text(nominal.toLocaleString());
            // if ($(el).hasClass('potongan_penjualan')) {
            //     $(el).text(nominal.toLocaleString());
            // } else {
            //     $(el).text(nominal.toLocaleString());
            // }
        });

        $('.jumlah').each(function(index, el) {
            var item_kode = $(el).attr('item_kode');
            var jumlah = parseFloat($(el).text());
            if (isNaN(jumlah)) jumlah = 0;
            jumlah = parseInt(jumlah);

            var satuan_item = [];
            for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                var relasi = relasi_transaksi_penjualan[i];
                if (relasi.item_kode == item_kode) {
                    var satuans = relasi.item.satuan_pembelians;
                    for (var j = 0; j < satuans.length; j++) {
                        var satuan = {
                            id: satuans[j].satuan.id,
                            kode: satuans[j].satuan.kode,
                            konversi: satuans[j].konversi
                        }
                        satuan_item.push(satuan);
                    }
                }
            }

            var jumlah1 = '';
            var jumlah2 = '';
            var temp_jumlah = jumlah;
            for (var i = 0; i < satuan_item.length; i++) {
                if (temp_jumlah > 0) {
                    var satuan = satuan_item[i];
                    var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                    // if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                    //     jumlah2 += jumlah_jual;
                    //     jumlah2 += ' '; 
                    //     jumlah2 += satuan.kode;
                    //     temp_jumlah = temp_jumlah % satuan.konversi;
                    // }
                    if (jumlah_jual > 0) {
                        if (jumlah1 == '') {
                            jumlah1 += jumlah_jual;
                            jumlah1 += ' ';
                            jumlah1 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;
                        } else {
                            if (temp_jumlah % satuan.konversi > 0) {
                                continue;
                            } else {
                                jumlah2 += jumlah_jual;
                                jumlah2 += ' ';
                                jumlah2 += satuan.kode;
                                temp_jumlah = temp_jumlah % satuan.konversi;
                            }
                        }
                    }
                }
            }

            // $(el).text(jumlah2);
            if (jumlah2 == '') {
                $(el).text(jumlah1);
                $(el).next().text('');
            } else {
                $(el).text(jumlah1);
                $(el).next().text(jumlah2);
            }
        });

        $('.tanggal').each(function(index, el) {
            var tanggal = $(el).text();
            // if(tanggal == '0000-00-00'){
            //     tanggal = '--/--/----';
            //     $(el).text(tanggal);
            // }else{
                tanggal = ymd2dmyMiring(tanggal);
                $(el).text(tanggal.toLocaleString());
            // }
        });

        // window.print();
        // var url = '{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}';
        // var ww = window.open(url, '_self');

        $('img.profile_img').on('load', function() {
            // window.print();
            // var url = '{{ url('transaksi-grosir/'.$transaksi_penjualan->id) }}';
            // var ww = window.open(url, '_self');
        });

    </script>
@endsection
