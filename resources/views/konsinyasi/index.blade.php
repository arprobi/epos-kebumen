@extends('layouts.admin')

@section('title')
    <title>EPOS | Konsinyasi</title>
@endsection

@section('style')
    <style media="screen">
    	#btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Tambah Item</h2>
                <ul class="nav navbar-right panel_toolbox">
                	<div class="pull-right">
                		<li> 
                		<button data-target="#collapse-peralatan" data-toggle="collapse" class="sembunyi"> <i class="fa fa-chevron-down"> </i> </button> </li>
                	</div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            		<div class="collapse" id="collapse-peralatan">
		                <div class="row">
							<form method="POST" action="{{ url('item') }}">
								{!! csrf_field() !!}
								<input type="hidden" name="id">
								<input type="hidden" name="konsinyasi" value="0">
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="form-group">
											<label class="control-label">Kode Item</label>
											<input class="form-control" type="text" id="kode" name="kode">
										</div>
									</div>
									<div class="col-md-12 col-xs-12">
										<div class="form-group">
											<label class="control-label">Nama Item</label>
											<input class="form-control" type="text" id="nama" name="nama">
										</div>
									</div>
									<div class="form-group col-sm-12 col-xs-12">
										<label class="control-label">Nama Suplier</label>
										<select name="suplier_id" class="select2_single form-control">
											<option id="default">Pilih Suplier</option>
											@foreach($supliers as $suplier)
											<option value="{{ $suplier->id }}">{{$suplier->nama}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-12 col-xs-12">
										<div class="form-group">
											<label class="control-label">Jenis Item</label>
											<select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id">
												<option>Pilih jenis item</option>
												@foreach($jenis_items as $jenis_item)
												<option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-md-12 col-xs-12">
										<div class="col-md-12form-group">
											<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
												<i class="fa fa-save"></i> Simpan
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data Anu</h2>
				<a href="{{ url('konsinyasi/create') }}" class="btn btn-sm btn-success pull-right">
					<i class="fa fa-plus"></i> Tambah
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Item</th>
							{{-- <th>Satuan</th> --}}
							{{-- <th>Harga Dasar</th> --}}
							<th>Jumlah Stok</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($konsinyasis as $i => $konsinyasi)
						<tr id="{{ $konsinyasi->id }}">
							<td>{{ $i + 1 }}</td>
							<td>{{ $konsinyasi->nama }}</td>
							{{-- <td>{{ $konsinyasi->satuan->item_kode}}</td> --}}
							{{-- <td>{{ $konsinyasi->harga_dasar }}</td> --}}
							<td>{{ $konsinyasi->stoktotal }}</td>
							<td>
								<a href="{{ url('konsinyasi/'.$konsinyasi->id) }}" class="btn btn-xs btn-info" id="btnDetail">
									<i class="fa fa-eye"></i> Detail
								</a>
								<a href="{{ url('konsinyasi/edit/'.$konsinyasi->id) }}" class="btn btn-xs btn-warning" id="btnUbah">
									<i class="fa fa-edit"></i> Ubah
								</a>
								<button class="btn btn-xs btn-danger" id="btnHapus">
									<i class="fa fa-trash"></i> Hapus
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Konsinyasi berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Konsinyasi gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();

		$(document).on('click', '#btnHapus', function() {
			var $tr = $(this).parents('tr').first();
			var id = $tr.attr('id');
			var nama_item = $tr.find('td').first().next().text();
			$('input[name="id"]').val(id);

			swal({
				title: 'Hapus?',
				text: '\"' + nama_item + '\" akan dihapus!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#009688',
				cancelButtonColor: '#ff5252',
				confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
				cancelButtonText: '<i class="fa fa-close"></i> Batal'
			}, function(isConfirm) {
				if (isConfirm) {
					// Confirmed
					$('#formHapusContainer').find('form').attr('action', '{{ url("konsinyasi") }}' + '/' + id);
					$('#formHapusContainer').find('form').submit();
				} else {
					// Canceled
				}
			});
		});
	</script>
@endsection
