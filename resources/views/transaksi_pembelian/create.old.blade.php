@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Transaksi Pembelian</title>
@endsection

@section('style')
	<style media="screen">
		#btnKembali {
			margin-right: 0;
		}
		td > .input-group {
			margin-bottom: 0;
		}
		#tabelInfo span {
			font-size: 0.85em;
			margin-right: 5px;
			margin-top: 0;
			margin-bottom: 0;
		}
		#tabelKeranjang {
			width: 100%;
		}
		#tabelKeranjang td {
			border: none;
		}
		#formSimpanTitle,
		#kodeTransaksiTitle {
			width: 100%;
		}
		#metodePembayaranButtonGroup {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
					<div class="col-md-6">
						<h2 id="formSimpanTitle">Transaksi Pembelian</h2>
						<span id="kodeTransaksiTitle"></span>
					</div>
					<div class="col-md-6 pull-right">
						<a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Suplier</label>
						<select name="suplier_id" class="select2_single form-control">
							<option value="">Pilih Suplier</option>
							@foreach ($supliers as $suplier)
							<option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Sales</label>
						<select name="seller_id" class="select2_single form-control">
							<option value="">Pilih Sales</option>
						</select>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label class="control-label">Nama Item</label>
						<select name="item_id" class="select2_single form-control">
							<option value="">Pilih Item</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Informasi Item</h2>
				<a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
					<i class="fa fa-long-arrow-left"></i> Kembali
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabelInfo">
					<thead>
						<tr>
							<th style="text-align: left;">Item</th>
							<th style="text-align: left;">Stok</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang Pembelian</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="form-group col-sm-12 col-xs-12">
						<div class="row">
							<table class="table" id="tabelKeranjang">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Metode Pembayaran</label>
								<div class="input-group">
									<div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" id="btnKredit" class="btn btn-default"><i class="fa"></i> Kredit</button>
										</div>
									</div>
								</div>
							</div>
							<div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<label class="control-label">Nominal Tunai</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" id="inputNominalTunai" class="form-control angka">
								</div>
							</div>
							<div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
										<label class="control-label">Pilih Bank</label>
										<select class="form-control select2_single" name="bank_id">
											<option value="">Pilih Bank</option>
											@foreach ($banks as $bank)
											<option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nomor Transfer</label>
										<div class="input-group">
											<div class="input-group-addon">#</div>
											<input type="text" id="inputNoTransfer" class="form-control">
										</div>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Transfer</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalTransfrer" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
							<div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Pilih Cek</label>
										<select class="form-control select2_single" name="cek_id">
											<option value="">Pilih Cek</option>
											@foreach ($ceks as $cek)
											<option value="{{ $cek->id }}">{{ $cek->nomor }} [Rp {{ \App\Util::ewon($cek->nominal) }}]</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Cek</label>
										<div class="input-group" style="margin: 0;">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
										</div>
									</div>
									<div class="col-md-12" style="margin-bottom: 10px;">
										<p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
									</div>
								</div>
							</div>
							<div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Pilih BG</label>
										<select class="form-control select2_single" name="bg_id">
											<option value="">Pilih BG</option>
											@foreach ($bgs as $bg)
											<option value="{{ $bg->id }}">{{ $bg->nomor }} [Rp {{ \App\Util::ewon($bg->nominal) }}]</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal BG</label>
										<div class="input-group" style="margin: 0;">
											<div class="input-group-addon">Rp</div>
											<input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
										</div>
									</div>
									<div class="col-md-12" style="margin-bottom: 10px;">
										<p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
									</div>
								</div>
							</div>
							<div id="inputKreditContainer" class="form-group col-sm-12 col-xs-12">
								<div class="line"></div>
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Pilih Kredit</label>
										<select class="form-control select2_single" name="kredit_id">
											<option value="">Pilih Kredit</option>
											@foreach ($kredits as $kredit)
											<option value="{{ $kredit->id }}">{{ $kredit->nomor }} [{{ $kredit->validasi }}]</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6 col-xs-6">
										<label class="control-label">Nominal Kredit</label>
										<div class="input-group">
											<div class="input-group-addon">Rp</div>
											<input type="text" name="inputNominalKredit" id="inputNominalKredit" class="form-control angka">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="row">
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Harga Total + Ongkos Kirim</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control angka" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Jumlah Bayar</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" disabled="">
								</div>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label class="control-label">Kurang</label>
								<div class="input-group">
									<div class="input-group-addon">Rp</div>
									<input type="text" name="inputUtang" id="inputUtang" class="form-control angka" disabled="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div id="formSimpanContainer">
									<form id="form-simpan" action="{{ url('transaksi-pembelian') }}" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="kode_transaksi" value="" />
										<input type="hidden" name="suplier_id" />
										<input type="hidden" name="seller_id" />
										<input type="hidden" name="po" value="0" />

										<input type="hidden" name="harga_total" />
										<input type="hidden" name="jumlah_bayar" />
										<input type="hidden" name="utang" />

										<input type="hidden" name="ongkos_kirim" />
										
										<input type="hidden" name="nominal_tunai" />
										
										<input type="hidden" name="bank_id" />
										<input type="hidden" name="no_tansfer" />
										<input type="hidden" name="nominal_transfer" />
										
										<input type="hidden" name="no_cek" />
										<input type="hidden" name="nominal_cek" />
										
										<input type="hidden" name="no_bg" />
										<input type="hidden" name="nominal_bg" />
										
										<input type="hidden" name="no_kredit" />
										<input type="hidden" name="nominal_kredit" />

										<input type="hidden" name="nominal_titipan" />
										
										<div id="append-section"></div>
										<div class="clearfix">
											<div class="form-group pull-left">
												<button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
													<i class="fa fa-check"></i> OK
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		var selected_items = [];

		function isSubmitButtonDisabled() {
			var jumlah = 0;
			$('input[name="jumlah[]"]').each(function(index, el) {
				var val = parseInt($(el).val());
				jumlah += val;
			});
			// console.log('jumlah');
			if (isNaN(jumlah) || jumlah <= 0) return true;

			var subtotal = 0;
			$('input[name="subtotal[]"]').each(function(index, el) {
				var val = parseFloat($(el).val());
				subtotal += val;
			});
			// console.log('subtotal');
			if (isNaN(subtotal) || subtotal <= 0) return true;

			// console.log('harga_total');
			var harga_total = parseFloat($('input[name="harga_total"]').val());
			if (isNaN(harga_total) || harga_total <= 0) return true;

			var ongkos_kirim = parseFloat($('input[name="ongkos_kirim"]').val());
			var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
			var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
			var bank_id = $('input[name="bank_id"]').val();
			var no_transfer = $('input[name="no_transfer"]').val();
			var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
			var no_cek = $('input[name="no_cek"]').val();
			var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
			var no_bg = $('input[name="no_bg"]').val();
			var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
			var no_kredit = $('input[name="no_kredit"]').val();
			var nominal_kredit = parseFloat($('input[name="nominal_kredit"]').val());

			// console.log('btnTunai');
			if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

			// console.log('btnTransfer');
			if ($('#btnTransfer').hasClass('btn-warning') && (bank_id == '' || no_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

			// console.log('btnCek');
			if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

			// console.log('btnBG');
			if ($('#btnBG').hasClass('btn-primary') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

			// console.log('btnKredit');
			if ($('#btnKredit').hasClass('btn-info') && (no_kredit == '' || isNaN(nominal_kredit) || nominal_kredit <= 0)) return true;

			// console.log('jumlah_bayar');
			if (isNaN(jumlah_bayar) || jumlah_bayar <= 0) return true;
			if (jumlah_bayar > harga_total + ongkos_kirim) return true;

			// console.log('end');
			return false;
		}

		function updateHargaOnKeyup() {
			var $harga_total = $('#inputHargaTotal');
			var $ongkos_kirim = $('#inputOngkosKirim');
			var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
			var $jumlah_bayar = $('#inputJumlahBayar');
			var $utang = $('#inputUtang');

			var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
			var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();
			var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
			var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
			var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();

			nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
			nominal_transfer = parseInt(nominal_transfer.replace(/\D/g, ''), 10);
			nominal_cek = parseInt(nominal_cek.replace(/\D/g, ''), 10);
			nominal_bg = parseInt(nominal_bg.replace(/\D/g, ''), 10);
			ongkos_kirim = parseInt(ongkos_kirim.replace(/\D/g, ''), 10);

			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			if (isNaN(nominal_transfer)) nominal_transfer = 0;
			if (isNaN(nominal_cek)) nominal_cek = 0;
			if (isNaN(nominal_bg)) nominal_bg = 0;
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			
			var harga_total_plus_ongkos_kirim = harga_total;
			if (!isNaN(parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10)))
				harga_total_plus_ongkos_kirim += parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10);
			var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg;
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;

			if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
			if (isNaN(harga_total)) harga_total = 0;
			if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
			if (isNaN(utang)) utang = 0;
			if (utang < 0) utang = 0;

			if (ongkos_kirim == 0) $ongkos_kirim.val('');
			// else $ongkos_kirim.val(ongkos_kirim.toLocaleString(undefined, {minimumFractionDigits: 2}));
			else $ongkos_kirim.val(ongkos_kirim);
			$harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$harga_total.val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$jumlah_bayar.val(jumlah_bayar.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$utang.val(utang.toLocaleString(undefined, {minimumFractionDigits: 2}));

			$('input[name="ongkos_kirim"]').val(ongkos_kirim);
			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
			$('input[name="utang"]').val(utang);

			// console.log('Till here', isSubmitButtonDisabled());
			$('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
		}

		$(document).ready(function() {
			// var url = "{{ url('transaksi-pembelian/create') }}";
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$(".select2_single").select2();

			$('#inputTunaiContainer').hide();
			$('#inputTransferBankContainer').hide();
			$('#inputTransferBankContainer').find('input').val('');
			$('#inputCekContainer').hide();
			$('#inputCekContainer').find('input').val('');
			$('#inputBGContainer').hide();
			$('#inputBGContainer').find('input').val('');
			$('#inputKreditContainer').hide();
			$('#inputKreditContainer').find('input').val('');
		});

		// Buat ambil nomer transaksi terakhir
		$(window).on('load', function(event) {
			var url = "{{ url('transaksi-pembelian/last.json') }}";
			
			$.get(url, function(data) {
				var kode = 1;
				if (data.transaksi_pembelian !== null) {
					var kode_transaksi = data.transaksi_pembelian.kode_transaksi;
					kode = parseInt(kode_transaksi.split('/')[0]);
					kode++;
				}

				kode = int4digit(kode);
				var tanggal = printTanggalSekarang('dd/mm/yyyy');
				kode_transaksi = kode + '/TRAB/' + tanggal;

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiTitle').text(kode_transaksi);
			});
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-danger');
				$(this).find('i').show('fast');
				$('#inputTunaiContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-danger')) {
				$(this).removeClass('btn-danger');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputTunaiContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnTransfer', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-warning');
				$(this).find('i').show('fast');
				$('#inputTransferBankContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-warning')) {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputTransferBankContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_tansfer"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnCek', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
				$(this).find('i').show('fast');
				$('#inputCekContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-success')) {
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputCekContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnBG', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-primary');
				$(this).find('i').show('fast');
				$('#inputBGContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-primary')) {
				$(this).removeClass('btn-primary');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputBGContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_bg"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('click', '#btnKredit', function(event) {
			event.preventDefault();

			if ($(this).hasClass('btn-default')) {
				$(this).removeClass('btn-default');
				$(this).addClass('btn-info');
				$(this).find('i').show('fast');
				$('#inputKreditContainer').show('fast', function() {
					$(this).find('input').first().trigger('focus');
				});
			} else if ($(this).hasClass('btn-info')) {
				$(this).removeClass('btn-info');
				$(this).addClass('btn-default');
				$(this).find('i').hide('fast');
				$('#inputKreditContainer').hide('hide', function() {
					$('#formSimpanContainer').find('input[name="no_kredit"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_kredit"]').val('');
					$(this).find('input').val('');
					updateHargaOnKeyup();
				});
			}
		});

		$(document).on('keyup', '#inputNominalTunai', function(event) {
			event.preventDefault();
			var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_tunai)) nominal_tunai = 0;
			$('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="bank_id"]', function(event) {
			event.preventDefault();
			var bank_id = $(this).val();
			// console.log(bank_id);
			$('#formSimpanContainer').find('input[name="bank_id"]').val(bank_id);
		});

		$(document).on('keyup', '#inputNoTransfer', function(event) {
			event.preventDefault();
			var no_tansfer = $(this).val();
			$('#formSimpanContainer').find('input[name="no_tansfer"]').val(no_tansfer);
		});

		$(document).on('keyup', '#inputNominalTransfrer', function(event) {
			event.preventDefault();
			var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_transfer)) nominal_transfer = 0;
			$('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
			updateHargaOnKeyup();
		});

		/*$(document).on('keyup', '#inputNoCek', function(event) {
			event.preventDefault();
			var no_cek = $(this).val();
			$('#formSimpanContainer').find('input[name="no_cek"]').val(no_cek);
		});

		$(document).on('keyup', '#inputNominalCek', function(event) {
			event.preventDefault();
			var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_cek)) nominal_cek = 0;
			$('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputNoBG', function(event) {
			event.preventDefault();
			var no_bg = $(this).val();
			$('#formSimpanContainer').find('input[name="no_bg"]').val(no_bg);
		});

		$(document).on('keyup', '#inputNominalBG', function(event) {
			event.preventDefault();
			var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
			if (isNaN(nominal_bg)) nominal_bg = 0;
			$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
			updateHargaOnKeyup();
		});*/

		$(document).on('change', 'select[name="cek_id"]', function(event) {
			event.preventDefault();

			var utang = $('input[name="inputUtang"]').val();
			utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10)) / 100;

			var cek_id = $(this).val();
			if (cek_id != '') {
				var cek_text = $(this).select2('data')[0].text;
				var nominal_text = cek_text.split('Rp ')[1].split(']')[0];
				var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
				nominal = parseFloat(nominal);
				var nomor = cek_text.split(' [')[0];
				
				if (nominal <= utang) {
					// Success
					$('#inputCekContainer').find('p').hide();
					$('#inputNominalCek').val(nominal_text);
					$('#formSimpanContainer').find('input[name="no_cek"]').val(nomor);
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
					// Button
				} else {
					// Error
					$('#inputCekContainer').find('p').show();
					$('#inputNominalCek').val('');
					$('#formSimpanContainer').find('input[name="no_cek"]').val('');
					$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
					// Button
				}
			} else {
				$('#inputNominalCek').val('');
				$('#formSimpanContainer').find('input[name="no_cek"]').val('');
				$('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
				// Button
			}
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="bg_id"]', function(event) {
			event.preventDefault();
			
			var utang = $('input[name="inputUtang"]').val();
			utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10)) / 100;

			var bg_id = $(this).val();
			if (bg_id != '') {
				var bg_text = $(this).select2('data')[0].text;
				var nominal_text = bg_text.split('Rp ')[1].split(']')[0];
				var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
				nominal = parseFloat(nominal);
				var nomor = bg_text.split(' [')[0];

				if (nominal <= utang) {
					// Success
					$('#inputBGContainer').find('p').hide();
					$('#inputNominalBG').val(nominal_text);
					$('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
				} else {
					$('#inputBGContainer').find('p').show();
					$('#inputNominalBG').val('');
					$('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
					$('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
				}
			} else {
				$('#inputNominalBG').val('');
				$('#formSimpanContainer').find('input[name="no_bg"]').val('');
				$('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
				// Button
			}
			updateHargaOnKeyup();
		});

		$(document).on('change', 'select[name="kredit_id"]', function(event) {
			event.preventDefault();
			var no_kredit = $(this).val();
			// console.log(no_kredit);
			$('#formSimpanContainer').find('input[name="no_kredit"]').val(no_kredit);
		});

		$(document).on('keyup', '#inputOngkosKirim', function(event) {
			event.preventDefault();
			var ongkos_kirim = parseInt($(this).val().replace(/\D/g, ''), 10);
			console.log(ongkos_kirim);
			if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
			$('#formSimpanContainer').find('input[name="ongkos_kirim"]').val(ongkos_kirim);
			updateHargaOnKeyup();
		});

		/*$(document).on('keyup', '#inputKodeItem', function(event) {
			event.preventDefault();
			var kode_item = $(this).val();
			console.log(kode_item);
		});*/

		$(document).on('change', 'select[name="suplier_id"]', function(event) {
			event.preventDefault();
			
			var id = $(this).val();
			var url_items = "{{ url('transaksi-pembelian') }}" + '/' + id + '/items.json';
			var url_sellers = "{{ url('transaksi-pembelian') }}" + '/' + id + '/sellers.json';

			$('input[name="suplier_id"]').val(id);
			$('#tabelKeranjang > tbody').empty();
			$('#append-section').empty();
			$('#formSimpanContainer').find('input[name="harga_total"]').val('');
			$('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
			$('#formSimpanContainer').find('input[name="utang"]').val('');

			var $select_item = $('select[name="item_id"]');
			$select_item.empty();
			$select_item.append($('<option value="">Pilih Item</option>'));

			$.get(url_items, function(data) {
				var items = data.items;
				for (var i = 0; i < items.length; i++) {
					var item = items[i];
					$select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
				}
			});

			var $select_sellers = $('select[name="seller_id"]');
			$select_sellers.empty();
			$select_sellers.append($('<option value="">Pilih Sales</option>'));

			$('#spinner').show();
			$.get(url_sellers, function(data) {
				var sellers = data.sellers;
				for (var i = 0; i < sellers.length; i++) {
					var seller = sellers[i];
					$select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
				}
				$('#spinner').hide();
			});
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();
			
			var id = $('select[name="item_id"]').val();
			var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
			var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

			if (!selected_items.includes(parseInt(id))) {
				if (!isNaN(id)) {
					selected_items.push(parseInt(id));
				}

				$('#spinner').show();
				$.get(url, function(data) {
					var pcs = data.pcs;
					var item = data.item;
					var kode = item.kode;
					var nama = item.nama;
					var stok = item.stoktotal;
					var satuans = item.satuans;
					// console.log(satuans);
					var harga_lama = $('input[name="inputHargaTotal"]').val();
					var tunai_lama = $('input[name="inputJumlahBayar"]').val();
					var kembali_lama = $('input[name="inputUtang"]').val();

					var stoks = '';
					var temp = stok;
					if (satuans.length > 0) {
						for (var i = 0; i < satuans.length; i++) {
							var satuan = satuans[i];
							var hasil = parseInt(temp / satuan.konversi);
							if (hasil > 0) {
								temp %= satuan.konversi;
								stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
							}
						}
					} else {
						stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
					}

					$('#tabelInfo').find('tbody').empty();
					$('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

					if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;
					if (tunai_lama === '' || tunai_lama === undefined) tunai_lama = 0;
					if (kembali_lama === '' || kembali_lama === undefined) kembali_lama = 0;

					if (tr === undefined) {
						$('#tabelKeranjang').find('tr[id="totalharga"]').remove();
						$('#tabelKeranjang').find('tr[id="totalbayar"]').remove();
						$('#tabelKeranjang').find('tr[id="totalkembali"]').remove();

						var divs = '<div class="form-group">'+
										'<label class="control-label">Jumlah</label>';

						if (satuans.length > 0) {
							for (var i = 0; i < satuans.length; i++) {
								var satuan = satuans[i];
								divs += '<div class="input-group">' +
											'<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+satuan.konversi+'">' +
											'<div class="input-group-addon">'+satuan.satuan.kode+'</div>' +
										'</div>';
							}
							divs += '</div>';
						} else {
							divs += '<div class="input-group">' +
										'<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
										'<div class="input-group-addon">'+pcs.kode+'</div>' +
									'</div>' +
								'</div>';
						}

						var tr = '<tr data-id="' + id + '">'+
									'<input type="hidden" name="jumlah">'+
									'<td><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
									'<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3></td>'+
									'<td>'+
										divs+
										'<div class="form-group">'+
											'<label class="control-label">Kadaluarsa</label>'+
											'<div class="input-group">'+
												'<span class="input-group-addon">'+
													'<input type="checkbox" name="checkKadaluarsa" id="checkKadaluarsa">'+
												'</span>'+
												'<input type="text" name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsa" disabled="" value="">'+
											'</div>'+
										'</div>'+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">Sub Total</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">Harga</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
									'</td>'+
									'<td>'+
										'<div class="form-group">'+
											'<label class="control-label">HPP</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
										'<div class="form-group">'+
											'<label class="control-label">PPN</label>'+
											'<div class="input-group">'+
												'<div class="input-group-addon">Rp</div>' +
												'<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="">'+
											'</div>'+
										'</div>'+
									'</td>'+
								'</tr>';

						$('#tabelKeranjang').find('tbody').append($(tr));
					}

					$('.inputKadaluarsa').daterangepicker({
						singleDatePicker: true,
						calender_style: "picker_2",
						format: 'DD-MM-YYYY'
					}, function(start) {
						var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
							.find('input[name="inputKadaluarsa"]').val();

						$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
						$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
					});

					$('#spinner').hide();
				});
			}
		});

		$(document).on('change', 'select[name="seller_id"]', function(event) {
			event.preventDefault();
			var id = $(this).val();
			$('input[name="seller_id"]').val(id);
		});

		$(document).on('change', 'input[type="checkbox"]', function(event) {
			event.preventDefault();

			$(this).parent().next().val('');
			var checked = $(this).prop('checked');
			if (checked) {
				$(this).parent().next().prop('disabled', false);
			} else {
				$(this).parent().next().prop('disabled', true);
				id = $(this).parents('tr').first().data('id');
				$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
				$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="" />');
			}
		});

		$(document).on('keyup', 'input[name="inputJumlah"]', function(event) {
			event.preventDefault();

			var konversi = $(this).attr('konversi');

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $kadaluarsa = $tr.find('.inputKadaluarsa');
			var $jumlah = $tr.find('input[name="jumlah"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $subtotal = $tr.find('input[name="inputSubTotal"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
			var jumlah = 0;
			$(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
				var value = parseInt($(el).val().replace(/\D/g, ''), 10);
				var konversi = parseInt($(el).attr('konversi'));
				value *= konversi;
				if (isNaN(value)) value = 0;
				jumlah += value;
			});
			var kadaluarsa = $kadaluarsa.val();

			if (isNaN(subtotal)) subtotal = 0;
			if (isNaN(jumlah)) jumlah = 0;
			var harga = subtotal / jumlah;
			if (isNaN(harga) || harga === Infinity) harga = 0;
			var ppn = harga / 11;
			if (isNaN(ppn)) ppn = 0;
			var hpp = ppn * 10;
			if (isNaN(hpp)) hpp = 0;

			$jumlah.val(jumlah);
			$harga.val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$hpp.val(hpp.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$ppn.val(ppn.toLocaleString(undefined, {minimumFractionDigits: 2}));
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + hpp + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + id + '" value="' + ppn + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(undefined, {minimumFractionDigits: 2}));
			updateHargaOnKeyup();
		});

		$(document).on('keyup', '#inputSubTotal', function(event) {
			event.preventDefault();

			var $tr = $(this).parents('tr').first();
			var id = $tr.data('id');
			var $kadaluarsa = $tr.find('.inputKadaluarsa');
			var $jumlah = $tr.find('input[name="jumlah"]');
			var $harga = $tr.find('input[name="inputHarga"]');
			var $hpp = $tr.find('input[name="inputHPP"]');
			var $ppn = $tr.find('input[name="inputPPN"]');

			var subtotal = parseInt($(this).val().replace(/\D/g, ''), 10);
			var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
			var kadaluarsa = $kadaluarsa.val();

			if (isNaN(subtotal)) subtotal = 0;
			if (isNaN(jumlah)) jumlah = 0;
			var harga = subtotal / jumlah;
			if (isNaN(harga) || harga === Infinity) harga = 0;
			var ppn = harga / 11;
			if (isNaN(ppn)) ppn = 0;
			var hpp = ppn* 10;
			if (isNaN(hpp)) hpp = 0;

			$harga.val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$hpp.val(hpp.toLocaleString(undefined, {minimumFractionDigits: 2}));
			$ppn.val(ppn.toLocaleString(undefined, {minimumFractionDigits: 2}));
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="' + kadaluarsa + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="' + hpp + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="harga-' + id + '" value="' + ppn + '" />');
			$('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="' + subtotal + '" />');

			var totalharga = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				totalharga += subtotal;
			});
			
			$('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(undefined, {minimumFractionDigits: 2}));
			updateHargaOnKeyup();
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();

			var id = $(this).parents('tr').data('id');

			var index = selected_items.indexOf(parseInt(id));
			if (index >= -1) {
				selected_items.splice(index, 1);
			}

			$('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
			$('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();
			
			$('#form-simpan').find('input[id="item-' + id + '"]').remove();
			$('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
			$('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
			$('#form-simpan').find('input[id="harga-' + id + '"]').remove();
			$('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

			var harga_total = 0;
			$('input[name="inputSubTotal"]').each(function(index, el) {
				var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
				if (isNaN(subtotal)) subtotal = 0;
				harga_total += subtotal;
			});
			$('input[name="inputHargaTotal"]').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));

			var ongkos_kirim = parseInt($('input[name="inputOngkosKirim"]').val().replace(/\D/g, ''), 10);
			var harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
			$('input[name="inputHargaTotalPlusOngkosKirim"]').val(harga_total_plus_ongkos_kirim.toLocaleString(undefined, {minimumFractionDigits: 2}));

			var jumlah_bayar = parseInt($('input[name="inputJumlahBayar"]').val().replace(/\D/g, ''), 10);
			var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
			if (utang <= 0 || isNaN(utang)) utang = 0;
			$('input[name="inputUtang"]').val(utang.toLocaleString(undefined, {minimumFractionDigits: 2}));

			$('input[name="ongkos_kirim"]').val(ongkos_kirim);
			$('input[name="harga_total"]').val(harga_total);
			$('input[name="jumlah_bayar"]').val(jumlah_bayar);
			$('input[name="utang"]').val(utang);
		});

	</script>
@endsection
