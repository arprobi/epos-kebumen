@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnTambah {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Transaksi Pembelian</h2>
                <a href="{{ url('transaksi-pembelian/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Transaksi Pembelian">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
                    <thead>
                        <tr>
                            <th style="display: none;">No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Transaksi</th>
                            <th>Kode Faktur</th>
                            <th>Pemasok</th>
                            <th>Grand Total</th>
                            <th style="width: 160px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksi_pembelians as $i => $transaksi_pembelian)
                        <tr id="{{ $transaksi_pembelian->id }}">
                            <td class="tengah-vh" style="display: none;">{{ $i + 1 }}</td>
                            <td class="tengah-vh">{{ $transaksi_pembelian->created_at->format('d-m-Y H:i:s') }}</td>
                            <td class="tengah-vh">{{ $transaksi_pembelian->kode_transaksi }}</td>
                            <td class="tengah-vh">{{ $transaksi_pembelian->nota }}</td>
                            <td class="tengah-v">{{ $transaksi_pembelian->suplier->nama }}</td>
                            <td class="tengah-v text-right">{{ \App\Util::duit($transaksi_pembelian->harga_total) }}</td>
                            <td>
                                <!-- <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-info" id="btnDetail">
                                    <i class="fa fa-eye"></i> Detail Pembelian
                                </a>
                                <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/transaksi') }}" class="btn btn-xs btn-warning" id="btnUbah">
                                    <i class="fa fa-sign-out"></i> Lihat Retur
                                </a> -->

                                <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Transaksi Pembelian">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <!-- <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" class="btn btn-xs btn-warning" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Lihat Retur">
                                    <i class="fa fa-sign-out"></i>
                                </a> -->
                                <!-- <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/create') }}" class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Tambah Retur">
                                    <i class="fa fa-sign-out"></i>
                                </a> -->
                                @if(in_array(Auth::user()->level_id, [1, 2]))
                                    {{-- <a href="{{ url('transaksi-pembelian/edit/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-warning" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Ubah Transaksi Pembelian">
                                        <i class="fa fa-edit"></i>
                                    </a> --}}
                                    <a href="{{ url('transaksi-pembelian/create/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-Sesuai" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Penyesuaian Transaksi Pembelian">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                @endif
                                @if($transaksi_pembelian->sisa_utang > 0)
                                    <a  href="{{ url('hutang/show/'.$transaksi_pembelian->id) }}" class="btn btn-xs btn-primary" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Bayar Hutang Transaksi Pembelian">
                                        <i class="fa fa-money"></i>
                                    </a>
                                @endif
                                @if($transaksi_pembelian->aktif_cek == 1)
                                    <a class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                        CEK
                                    </a>
                                @endif
                                @if($transaksi_pembelian->aktif_bg == 1)
                                    <a class="btn btn-xs btn-BG" id="btnDetail" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                        BG
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == '404')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Halaman tidak ditemukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        $('#tableItemMasuk').DataTable({
            // 'order': [[1, 'desc'], [2, 'desc']]
        });

        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            var nama_suplier = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-pembelian") }}' + '/' + id);
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        });

    </script>
@endsection
