@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Transaksi Pembelian</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnKembali {
			margin-bottom: 0;
		}
		.no-border {
			border: none;
		}
	</style>
@endsection

@section('content')

<div class="col-md-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Detail Transaksi Pembelian</h2>
			<a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/transaksi') }}" class="btn btn-sm btn-warning pull-right" id="btnUbah">
				<i class="fa fa-sign-out"></i> Retur
			</a>
			<a href="{{ url('item/history-harga-beli/'.$item->kode) }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
            	<i class="fa fa-long-arrow-left"></i> Kembali
        	</a>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<section class="content invoice">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Detail Transaksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Kode Transaksi</td>
										<td>{{ $transaksi_pembelian->kode_transaksi }}</td>
									</tr>
									<tr>
										<td>Nama Suplier</td>
										<td>{{ $transaksi_pembelian->suplier->nama }}</td>
									</tr>
									<tr>
										<td>Nama Sales</td>
										<td>{{ $transaksi_pembelian->seller->nama }}</td>
									</tr>
									<tr>
										<td>Harga Total</td>
										<td>{{ number_format($transaksi_pembelian->harga_total) }}</td>
									</tr>
									<tr>
										<td>Jumlah Bayar</td>
										<td>{{ number_format($transaksi_pembelian->jumlah_bayar) }}</td>
									</tr>
								</tbody>
							</table>
							
							@if ($transaksi_pembelian->nominal_tunai)
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Tunai</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Nominal Tunai</td>
										<td>{{ number_format($transaksi_pembelian->nominal_tunai) }}</td>
									</tr>
								</tbody>
							</table>
							@endif
							
							@if ($transaksi_pembelian->nominal_debit)
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Debit</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Nomor Debit</td>
										<td>{{ $transaksi_pembelian->no_debit }}</td>
									</tr>
									<tr>
										<td>Nominal Debit</td>
										<td>{{ number_format($transaksi_pembelian->nominal_debit) }}</td>
									</tr>
								</tbody>
							</table>
							@endif
							
							@if ($transaksi_pembelian->nominal_cek)
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Cek</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Nomor Cek</td>
										<td>{{ $transaksi_pembelian->no_cek }}</td>
									</tr>
									<tr>
										<td>Nominal Cek</td>
										<td>{{ number_format($transaksi_pembelian->nominal_cek) }}</td>
									</tr>
								</tbody>
							</table>
							@endif
							
							@if ($transaksi_pembelian->nominal_bg)
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">BG</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Nomor BG</td>
										<td>{{ $transaksi_pembelian->no_bg }}</td>
									</tr>
									<tr>
										<td>Nominal BG</td>
										<td>{{ number_format($transaksi_pembelian->nominal_bg) }}</td>
									</tr>
								</tbody>
							</table>
							@endif
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Nama Item</th>
										<th>Jumlah</th>
										<th>Harga/pcs</th>
										<th>Sub Total</th>
									</tr>
								</thead>
								<tbody>
									@foreach($relasi_transaksi_pembelian as $num => $relasi)
										<tr>
											<td>{{ $relasi->item->nama }}</td>
											<td>{{ $relasi->jumlah }}</td>
											<td>{{ number_format($relasi->harga) }}</td>
											<td>{{ number_format($relasi->subtotal)}}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

@endsection


@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			// var harga_total = parseInt($('#harga_total').text().replace(/\D/g, ''), 10);
			// var subtotal = parseInt($('#subtotal').text().replace(/\D/g, ''), 10);
			// $('#harga_total').text(harga_total.toLocaleString());
			// $('#subtotal').text(subtotal.toLocaleString());
		});
	</script>
@endsection
