@extends('layouts.admin')

@section('title')
    <title>EPOS | Penyesuaian Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
        /* #infoTransaksi {
            position: fixed;
            bottom: 15px;
            left: 15px;
            z-index: 1000;
            background: rgba(237, 237, 237, 0.3);
            color: #fefefe;
            font-size: 1.2em;
            padding: 10px;
        } */
    </style>
@endsection

@section('content')
    <!-- <div id="infoTransaksi">
        TOTAL 0 ITEM
    </div> -->
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-12">
                        <h2 id="formSimpanTitle">Penyesuaian Transaksi Pembelian</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    {{-- <div class="col-md-6 pull-right">
                        <a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div> --}}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label" style="">Kode Faktur</label>
                        <!-- <div class="input-group">
                            <div class="input-group-addon">
                                #
                            </div> -->
                            <input type="text" id="nota" name="nota" class="form-control" style="height: 39px;" placeholder="Kode Faktur">
                        <!-- </div> -->
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Pemasok</label>
                        <select id="suplier_form" name="suplier_id" class="select2_single form-control">
                            <option value="">Pilih Pemasok</option>
                            @foreach ($supliers as $suplier)
                            <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Pilih Penjual</label>
                        <select id="seller_form" name="seller_id" class="select2_single form-control" id="seller">
                            <option value="">Pilih Penjual</option>
                        </select>
                    </div>
                   {{--  <div class="form-group col-sm-6 col-xs-6">
                        <label class="control-label" style="">Nama Sales</label>
                        <input type="text" name="nama_sales" class="form-control" style="height: 39px;">
                    </div> --}}
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select id="item_form" name="item_id" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                        </select>
                    </div>
                    {{-- <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Tanggal Jatuh Tempo</label>
                        <div class="input-group col-sm-12 col-xs-12">
                            <input name="pilihTanggal" id="pilihTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Tanggal" style="height: 38px;" readonly="">
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th width="70%" style="text-align: left;">Item</th>
                            <th width="30%" style="text-align: left;">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Pembelian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table" id="tabelKeranjang">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-7 col-xs-7">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div class="input-group">
                                    <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnPiutang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Piutang</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group" id="grupTunai">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_transfer">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfrer" id="inputNominalTransfrer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Dibayarkan</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Cek</label>
                                        <select class="form-control select2_single" name="cek_id">
                                            <option value="">Pilih Cek</option>
                                            <option value="cek_baru">Buat Cek Baru</option>
                                            @if ($cek_terpilih != null)
                                            <option value="{{ $cek_terpilih->id }}">{{ $cek_terpilih->nomor }} [{{ \App\Util::ewon($cek_terpilih->nominal) }}]</option>
                                            @endif
                                            @foreach ($ceks as $cek)
                                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [{{ \App\Util::ewon($cek->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                    </div> --}}
                                </div>
                                <div class="row" id="cek_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_cek">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih BG</label>
                                        <select class="form-control select2_single" name="bg_id">
                                            <option value="">Pilih BG</option>
                                            <option value="bg_baru">Buat BG Baru</option>
                                            @if ($bg_terpilih != null)
                                            <option value="{{ $bg_terpilih->id }}">{{ $bg_terpilih->nomor }} [{{ \App\Util::ewon($bg_terpilih->nominal) }}]</option>
                                            @endif
                                            @foreach ($bgs as $bg)
                                            <option value="{{ $bg->id }}">{{ $bg->nomor }} [{{ \App\Util::ewon($bg->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                    </div> --}}
                                </div>
                                <div class="row" id="bg_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_bg">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control" />
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            <div id="inputPiutangContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Piutang <span id="JumlahPiutang"></span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalPiutang" id="inputNominalPiutang" class="form-control angka">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Kembalian Uang</label>
                                <div class="input-group">
                                    <div id="cashbackButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunaiCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransferCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKartuCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnCekCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBGCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnPiutangCashback" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Piutang</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiCashbackContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group" id="grupTunai">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunaiCashback" id="inputNominalTunaiCashback" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankCashbackContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_transfer_cashback">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransferCashback" id="inputNoTransferCashback" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfrerCashback" id="inputNominalTransfrerCashback" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuCashbackContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu_cashback">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu_cashback">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartuCashback" id="inputNoKartuCashback" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Dibayarkan</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartuCashback" id="inputNominalKartuCashback" class="form-control angka" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-xs-5">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Sub Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Potongan Pembelian</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputPotonganPembelian" id="inputPotonganPembelian" class="form-control angka">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">PPN (Rp)</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <input name="checkPPNTotal" id="checkPPNTotal" type="checkbox">
                                    </div>
                                    <input type="text" name="inputPPNTotal" id="inputPPNTotal" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                {{-- <label class="control-label">Grand Total + Ongkos Kirim</label> --}}
                                <label class="control-label">Grand Total (+Ongkos Kirim)</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Pengurang Tagihan</label>
                                {{-- <label class="control-label">Jumlah Bayar</label> --}}
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                {{-- <label class="control-label">Kurang</label> --}}
                                <label class="control-label">Tagihan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputUtang" id="inputUtang" class="form-control angka" disabled="">
                                </div>
                            </div>
                            @if(Auth::user()->level_id == 5)
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Saldo Laci Gudang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="saldo" id="saldo" class="form-control angka" disabled="" value="{{ \App\Util::angka($laci->gudang) }}">
                                    </div>
                                </div>
                            @endif
                            @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                <div class="form-group col-sm-12 col-xs-12">
                                    <label class="control-label">Saldo Laci Owner</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="saldo" id="saldo" class="form-control angka" disabled="" value="{{ \App\Util::angka($laci->owner) }}">
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('transaksi-pembelian/again/'.$transaksi_pembelian->id) }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="kode_transaksi" value="" />

                                        <input type="hidden" name="nota" />
                                        <input type="hidden" name="suplier_id" />
                                        <input type="hidden" name="seller_id" />
                                        <input type="hidden" name="seller_name" />
                                        <input type="hidden" name="beli_po" value="0"/>
                                        <input type="hidden" name="po" value="0" />

                                        <input type="hidden" name="harga_total" />
                                        <input type="hidden" name="jumlah_bayar" />
                                        <input type="hidden" name="utang" />

                                        <input type="hidden" name="potongan_pembelian" />
                                        <input type="hidden" name="ppn_total" />
                                        <input type="hidden" name="ongkos_kirim" />
                                        
                                        <input type="hidden" name="nominal_tunai" />
                                        
                                        <input type="hidden" name="no_transfer" />
                                        <input type="hidden" name="bank_transfer" />
                                        <input type="hidden" name="nominal_transfer" />
                                        
                                        <input type="hidden" name="cek_id" />
                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="bank_cek" />
                                        <input type="hidden" name="nominal_cek" />
                                        
                                        <input type="hidden" name="bg_id" />
                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="bank_bg" />
                                        <input type="hidden" name="nominal_bg" />
                                        
                                        <input type="hidden" name="no_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />

                                        <input type="hidden" name="nominal_piutang" />
                                        <input type="hidden" name="jatuh_tempo" />
                                        
                                        <input type="hidden" name="nominal_tunai_cashback" />
                                        
                                        <input type="hidden" name="no_transfer_cashback" />
                                        <input type="hidden" name="bank_transfer_cashback" />
                                        <input type="hidden" name="nominal_transfer_cashback" />
                                        
                                        <input type="hidden" name="no_kartu_cashback" />
                                        <input type="hidden" name="bank_kartu_cashback" />
                                        <input type="hidden" name="jenis_kartu_cashback" />
                                        <input type="hidden" name="nominal_kartu_cashback" />
                                        
                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                            <div class="form-group pull-left">
                                                <button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
                                                    <i class="fa fa-check"></i> OK
                                                </button>
                                            </div>
                                            <div class="form-group pull-left">
                                                <div type="div" id="infoTransaksi">
                                                    TOTAL 0 ITEM
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var selected_items = [];
        var piutang = 0;
        var nominal_piutang = 0;
        var laci_gudang = {{ $laci->gudang }};
        var laci_owner = {{ $laci->owner }};
        var user_level = {{ Auth::user()->level_id }};

        // var on_ready_item_index = 0;
        var stoks = null;
        var transaksi_pembelian = null;
        var custom_data_last_item = false;
        var relasi_transaksi_pembelian = null;
        var cek_terpilih = null;
        var bg_terpilih = null;

        function isSubmitButtonDisabled() {
            var jumlah = 0;
            $('input[name="jumlah[]"]').each(function(index, el) {
                var val = parseInt($(el).val());
                jumlah += val;
            });
            // console.log('jumlah');
            var selisih = piutang - nominal_piutang;
            if (isNaN(jumlah) || jumlah <= 0) return true;
            if (isNaN(selisih) || selisih <0) return true;

            var subtotal = 0;
            $('input[name="subtotal[]"]').each(function(index, el) {
                var val = parseFloat($(el).val());
                subtotal += val;
            });
            // console.log('subtotal');
            // if (isNaN(subtotal) || subtotal <= 0) return true;

            // console.log('harga_total');
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            // if (isNaN(harga_total) || harga_total <= 0) return true;

            var ongkos_kirim = $('input[name="ongkos_kirim"]').val();
            var jumlah_bayar = $('input[name="jumlah_bayar"]').val();
            var potongan_pembelian = $('input[name="potongan_pembelian"]').val();
            var ppn_total = $('input[name="ppn_total"]').val();
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var no_transfer = $('input[name="no_transfer"]').val();
            var bank_transfer = $('input[name="bank_transfer"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var no_kartu = $('input[name="no_kartu"]').val();
            var bank_kartu = $('input[name="bank_kartu"]').val();
            var jenis_kartu = $('input[name="jenis_kartu"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var cek_id = $('input[name="cek_id"]').val();
            var no_cek = $('input[name="no_cek"]').val();
            var bank_cek = $('input[name="bank_cek"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var bg_id = $('input[name="bg_id"]').val();
            var no_bg = $('input[name="no_bg"]').val();
            var bank_bg = $('input[name="bank_bg"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var jatuh_tempo = $('#pilihTanggal').val();

            ongkos_kirim = parseFloat(ongkos_kirim);
            jumlah_bayar = parseFloat(jumlah_bayar);
            potongan_pembelian = parseFloat(potongan_pembelian);
            ppn_total = parseFloat(ppn_total);
            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_kartu = parseFloat(nominal_kartu);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);

            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            if (isNaN(ppn_total)) ppn_total = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var nota = $('#nota').val();
            if(nota == '' || nota == null || nota == undefined) return true;
            
            // console.log('btnTunai');
            if ($('#btnTunai').hasClass('btn-danger')) {
                if (nominal_tunai <= 0) return true;
                if (user_level == 5) {
                   if (nominal_tunai > laci_gudang) return true;
                } else if (user_level == 1 || user_level == 2) {
                    if (nominal_tunai > laci_owner) return true;
                }
            }

            // console.log('btnTransfer');
            if ($('#btnTransfer').hasClass('btn-warning')) {
                if (no_transfer == '') return true;
                if (bank_transfer == '') return true;
                if (nominal_transfer <= 0) return true;
            }

            // console.log('btnKartu');
            if ($('#btnKartu').hasClass('btn-info')) {
                if (no_kartu == '') return true;
                if (bank_kartu == '') return true;
                if (jenis_kartu == '') return true;
                if (nominal_kartu <= 0) return true;
            }

            // console.log('btnCek');
            if ($('#btnCek').hasClass('btn-success')) {
                if (no_cek == '') return true;
                if (nominal_cek <= 0) return true;
                if (cek_id == '' && bank_cek == '') return true;
            }

            // console.log('btnBG');
            if ($('#btnBG').hasClass('btn-BG')) {
                if (no_bg == '') return true;
                if (nominal_bg <= 0) return true;
                if (bg_id == '' && bank_bg == '') return true;
                // if (jatuh_tempo == '') return true;
            }

            // // console.log('btnTunai');
            // if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

            // // console.log('btnTransfer');
            // if ($('#btnTransfer').hasClass('btn-warning') && (bank_transfer == '' || no_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

            // // console.log('btnCek');
            // if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

            // // console.log('btnBG');
            // if ($('#btnBG').hasClass('btn-BG') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

            // // console.log('btnKredit');
            // if ($('#btnKredit').hasClass('btn-info') && (no_kredit == '' || isNaN(nominal_kredit) || nominal_kredit <= 0)) return true;

            // console.log('jumlah_bayar');
            // if (isNaN(jumlah_bayar) || jumlah_bayar <= 0) return true;
            var temp_ppn = 0;
            if ($('#checkPPN').prop('checked')) temp_ppn = ppn_total;
            var kelebihan_bayar = jumlah_bayar - (harga_total - potongan_pembelian + temp_ppn + ongkos_kirim);
            // console.log(kelebihan_bayar, jumlah_bayar, harga_total, temp_ppn, ongkos_kirim);
            // if (jumlah_bayar > harga_total + ongkos_kirim) return true;
            // console.log(kelebihan_bayar, jatuh_tempo);

            // * topik signature * //
            // if (kelebihan_bayar < 0 && jatuh_tempo == '') return true;
            if (kelebihan_bayar >= 100) return true;
            // * topik signature * //

            // console.log('end');
            return false;
        }

        function cek_laci_gudang(){
            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;

            if(user_level == 5 && nominal_tunai > laci_gudang ){
                $('#grupTunai').addClass('has-error');
            }else if(user_level == 1 && nominal_tunai > laci_owner ){
                $('#grupTunai').addClass('has-error');
            }else if(user_level == 2 && nominal_tunai > laci_owner ){
                $('#grupTunai').addClass('has-error');
            }else{
                $('#grupTunai').removeClass('has-error');
            }
        }

        function updateInfoTransaksi() {
            // 
            $('#infoTransaksi').text(`TOTAL ${selected_items.length} ITEM`);
        }

        function updateHargaOnKeyup() {
            var $harga_total = $('#inputHargaTotal');
            var $potongan_pembelian = $('#inputPotonganPembelian');
            var $ppn_total = $('#inputPPNTotal');
            var $ongkos_kirim = $('#inputOngkosKirim');
            var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $utang = $('#inputUtang');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();
            var nominal_kartu = $('#formSimpanContainer').find('input[name="nominal_kartu"]').val();
            var nominal_piutang = $('#formSimpanContainer').find('input[name="nominal_piutang"]').val();
            var potongan_pembelian = $('#formSimpanContainer').find('input[name="potongan_pembelian"]').val();
            var ppn_total = $('#formSimpanContainer').find('input[name="ppn_total"]').val();
            var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();
            var bank_cek = $('select[name="bank_cek"]').val();
            var bank_bg = $('select[name="bank_bg"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            //cashback
            var nominal_tunai_cashback = $('#formSimpanContainer').find('input[name="nominal_tunai_cashback"]').val();
            var nominal_transfer_cashback = $('#formSimpanContainer').find('input[name="nominal_transfer_cashback"]').val();
            var nominal_kartu_cashback = $('#formSimpanContainer').find('input[name="nominal_kartu_cashback"]').val();
            // if(bank_cek != undefined && bank_cek == ''){
            //     var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
            // }else{
            //     nominal_cek = '0';
            // }

            // if(bank_bg != undefined && bank_bg == ''){
            //     var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
            // }else{
            //     nominal_bg = '0';
            // }

            nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_transfer = parseFloat(nominal_transfer.replace(/\D/g, ''), 10);
            nominal_cek = parseFloat(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg = parseFloat(nominal_bg.replace(/\D/g, ''), 10);
            nominal_kartu = parseFloat(nominal_kartu.replace(/\D/g, ''), 10);
            nominal_piutang = parseFloat(nominal_piutang.replace(/\D/g, ''), 10);
            potongan_pembelian = parseFloat(potongan_pembelian.replace(/\D/g, ''), 10);
            ppn_total = parseFloat(ppn_total.replace(/\D/g, ''), 10);
            ongkos_kirim = parseFloat(ongkos_kirim.replace(/\D/g, ''), 10);
            //cashback
            nominal_tunai_cashback = parseFloat(nominal_tunai_cashback.replace(/\D/g, ''), 10);
            nominal_transfer_cashback = parseFloat(nominal_transfer_cashback.replace(/\D/g, ''), 10);
            nominal_kartu_cashback = parseFloat(nominal_kartu_cashback.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            if (isNaN(ppn_total)) ppn_total = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            //cashback
            if (isNaN(nominal_tunai_cashback)) nominal_tunai_cashback = 0;
            if (isNaN(nominal_transfer_cashback)) nominal_transfer_cashback = 0;
            if (isNaN(nominal_kartu_cashback)) nominal_kartu_cashback = 0;

            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });
            
            var harga_total_plus_ongkos_kirim = harga_total;
            if (!isNaN(parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10)))
                harga_total_plus_ongkos_kirim += parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10);
            if (!isNaN(parseInt($potongan_pembelian.val().replace(/\D/g, ''), 10)))
                harga_total_plus_ongkos_kirim -= parseInt($potongan_pembelian.val().replace(/\D/g, ''), 10);
            if (!isNaN(parseInt($ppn_total.val().replace(/\D/g, ''), 10)))
                harga_total_plus_ongkos_kirim += parseInt($ppn_total.val().replace(/\D/g, ''), 10);
            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kartu + nominal_piutang - nominal_kartu_cashback - nominal_transfer_cashback - nominal_tunai_cashback;
            var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;

            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(utang)) utang = 0;
            if (utang < 0) utang = 0;
            // console.log(harga_total_plus_ongkos_kirim);

            // if (ongkos_kirim == 0) $ongkos_kirim.val('');
            // else $ongkos_kirim.val(ongkos_kirim);
            if (potongan_pembelian == 0) $potongan_pembelian.val('');
            else $potongan_pembelian.val(potongan_pembelian.toLocaleString(['ban', 'id']));
            $harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString(['ban', 'id']));
            $harga_total.val(harga_total.toLocaleString(['ban', 'id']));
            $jumlah_bayar.val(jumlah_bayar.toLocaleString(['ban', 'id']));
            $utang.val(utang.toLocaleString(['ban', 'id']));

            $('input[name="potongan_pembelian"]').val(potongan_pembelian);
            // $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="utang"]').val(utang);

            cek_laci_gudang();
            updateInfoTransaksi();
            // console.log('Till here', isSubmitButtonDisabled());
            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
        }

        function cariItem(id, custom_data, index) {
            var on_ready_item_index = index;
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

            if (id == '') {
                $('#tabelInfo').find('tbody').empty();
            } else if (!selected_items.includes(parseInt(id))) {
                if (!isNaN(id)) {
                    selected_items.push(parseInt(id));
                }

                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(url, data);
                    var pcs = data.pcs;
                    var item = data.item;
                    var kode = item.kode;
                    var nama = item.nama;
                    var stok = item.stoktotal;
                    var satuans = item.satuan_pembelians;
                    // var harga_lama = $('input[name="inputHargaTotal"]').val();
                    // var tunai_lama = $('input[name="inputJumlahBayar"]').val();
                    // var kembali_lama = $('input[name="inputUtang"]').val();

                    var on_ready_item_subtotal = 0;

                    var jumlah_kadaluarsa = 0;
                    for (var i = 0; i < stoks.length; i++) {
                        var _stok = stoks[i];
                        if (_stok.item.id == id) {
                            jumlah_kadaluarsa++;
                        }
                    }

                    var text_stoks = '';
                    var temp = stok;
                    if (satuans.length > 0) {
                        for (var i = 0; i < satuans.length; i++) {
                            var satuan = satuans[i];
                            var hasil = parseInt(temp / satuan.konversi);
                            if (hasil > 0) {
                                temp %= satuan.konversi;
                                text_stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                            }
                        }
                    } else {
                        text_stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                    }

                    var on_ready_item_jumlah_view = [];
                    if (custom_data) {
                        var on_ready_item_jumlah = parseFloat(relasi_transaksi_pembelian[on_ready_item_index].jumlah);
                        on_ready_item_subtotal = parseFloat(relasi_transaksi_pembelian[on_ready_item_index].subtotal);

                        if (isNaN(on_ready_item_jumlah)) on_ready_item_jumlah = 0;
                        if (isNaN(on_ready_item_subtotal)) on_ready_item_subtotal = 0;
                        for (var i = 0; i < transaksi_pembelian.items[on_ready_item_index].satuan_pembelians.length; i++) {
                            var satuan = transaksi_pembelian.items[on_ready_item_index].satuan_pembelians[i];
                            if (on_ready_item_jumlah >= satuan.konversi) {
                                on_ready_item_jumlah_view.push({
                                    jumlah: parseInt(on_ready_item_jumlah / satuan.konversi),
                                    konversi: satuan.konversi
                                });
                                on_ready_item_jumlah = on_ready_item_jumlah % satuan.konversi;
                            }
                        }
                    }

                    $('#tabelInfo').find('tbody').empty();
                    $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+text_stoks+'</td></tr>');

                    // if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;
                    // if (tunai_lama === '' || tunai_lama === undefined) tunai_lama = 0;
                    // if (kembali_lama === '' || kembali_lama === undefined) kembali_lama = 0;

                    if (tr === undefined) {
                        $('#tabelKeranjang').find('tr[id="totalharga"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalbayar"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalkembali"]').remove();

                        var divs = '<div class="form-group">'+
                                        '<label class="control-label">Jumlah</label>';

                        if (satuans.length > 0) {
                            for (var i = 0; i < satuans.length; i++) {
                                var satuan = satuans[i];
                                var temp_jumlah = 0;
                                if (custom_data) {
                                    for (var j = 0; j < on_ready_item_jumlah_view.length; j++) {
                                        if (on_ready_item_jumlah_view[j].konversi == satuan.konversi) {
                                            temp_jumlah = on_ready_item_jumlah_view[j].jumlah;
                                        }
                                    }
                                }
                                divs += '<div class="input-group text-center">' +
                                            '<input type="text" id="inputJumlah" name="inputJumlah" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'" value="'+(temp_jumlah>0?temp_jumlah:'')+'">' +
                                            '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+satuan.satuan.kode+'</div>' +
                                        '</div>';
                            }
                            divs += '</div>';
                        } else {
                            divs += '<div class="input-group">' +
                                        '<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
                                        '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+pcs.kode+'</div>' +
                                    '</div>' +
                                '</div>';
                        }

                        var tr = '<tr data-id="' + id + '">'+
                                    '<input type="hidden" name="jumlah">'+
                                    '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                    '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3>'+
                                        `<div class="form-group" id="anak-kadaluarsa-` + id + `" jkal="0">
                                                <label class="control-label">Kadaluarsa</label>
                                                <i class="fa fa-plus" id="tambah_kadaluarsa" style="color: green; cursor: pointer; margin-left: 10px;"></i>
                                            </div>`+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        divs+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Total</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" required="" value="'+(on_ready_item_subtotal>0?on_ready_item_subtotal:'')+'">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Harga</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">HPP</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">PPN (Rp)</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">'+
                                                    '<input name="checkPPN" id="checkPPN" type="checkbox" class="invisible">'+
                                                '</div>'+
                                                '<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="" readonly="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                '</tr>';

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-0" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-0" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-0" value="" class="hpp-stok-' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-0" value="" class="jumlah-stok-'+id+'"/>');

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="ppn-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="ppn-' + id + '" value="" />');

                        $('#tabelKeranjang').find('tbody').append($(tr));
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#inputJumlah').trigger('keyup');
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#inputSubTotal').trigger('keyup');

                        if (custom_data) {
                            for (var i = 0; i < jumlah_kadaluarsa; i++) {
                                $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                            }
                        } else {
                            $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                        }
                    }

                    $('#spinner').hide();
                    updateHargaOnKeyup();
                    // $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                });
            }
        }

        $(document).ready(function() {
            var url = "{{ url('transaksi-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            stoks = '{{ $stoks }}';
            stoks = stoks.replace(/&quot;/g, '"');
            stoks = JSON.parse(stoks);

            transaksi_pembelian = '{{ $transaksi_pembelian }}';
            transaksi_pembelian = transaksi_pembelian.replace(/&quot;/g, '"');
            transaksi_pembelian = JSON.parse(transaksi_pembelian);

            relasi_transaksi_pembelian = '{{ $relasi_transaksi_pembelian }}';
            relasi_transaksi_pembelian = relasi_transaksi_pembelian.replace(/&quot;/g, '"');
            relasi_transaksi_pembelian = JSON.parse(relasi_transaksi_pembelian);

            bg_terpilih = '{{ json_encode($bg_terpilih) }}';
            bg_terpilih = bg_terpilih.replace(/&quot;/g, '"');
            bg_terpilih = JSON.parse(bg_terpilih);

            cek_terpilih = '{{ json_encode($cek_terpilih) }}';
            cek_terpilih = cek_terpilih.replace(/&quot;/g, '"');
            cek_terpilih = JSON.parse(cek_terpilih);

            // console.log(bg_terpilih, cek_terpilih);

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#pilihTanggal').val('');
            $('input[name="jatuh_tempo"]').val('');

            $('#inputTunaiContainer').hide();
            $('#inputPiutangContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputNominalTunai').val('');
            $('#inputNominalKartu').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputNoKartu').val('');
            $('#inputNominalPiutang').val('');
            $('input[name="nama_sales"]').prop('disabled', true);
            $('#nota').val('');

            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
            $('#formSimpanContainer').find('input[name="nota"]').val('');

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            });

            $('#inputHargaTotal').val('');
            $('#inputPotonganPembelian').val('').trigger('keyup');
            if ($('#checkPPNTotal').prop('checked')) {
                // $('#inputPPNTotal').val('').trigger('keyup');
                $('#checkPPNTotal').trigger('click');
            }
            $('#inputOngkosKirim').val('').trigger('keyup');
            $('#inputHargaTotalPlusOngkosKirim').val('');
            $('#inputJumlahBayar').val('');
            $('#inputUtang').val('');

            $('select[name="suplier_id"]').val(transaksi_pembelian.suplier_id).trigger('change');
            $('select[name="seller_id"]').val(transaksi_pembelian.seller_id).trigger('change');

            setTimeout(function() {
                for (var i = 0; i < relasi_transaksi_pembelian.length; i++) {
                    var relasi = relasi_transaksi_pembelian[i];
                    // rtp_nego_total += parseFloat(relasi.nego);
                    if (i == relasi_transaksi_pembelian.length - 1) custom_data_last_item = true;

                    cariItem(relasi.item_id, true, i);
                }

                $('#suplier_form').select2({disabled:true});
                $('#nota').prop('readonly', true);
                $('#seller_form').select2({disabled:true});
                $('#item_form').select2({disabled:true});
                // $('#pilihTanggal').prop('disabled', true);
                // $('#pilihTanggal').addClass('bg-disabled');
            }, 500);

            $('input[name="kode_transaksi"]').val(transaksi_pembelian.kode_transaksi);
            $('input[name="nota"]').val(transaksi_pembelian.nota);
            $('input[name="potongan_pembelian"]').val(transaksi_pembelian.potongan_pembelian);
            $('input[name="ppn_total"]').val(transaksi_pembelian.ppn_total);
            $('input[name="ongkos_kirim"]').val(transaksi_pembelian.ongkos_kirim);

            $('#inputTunaiContainer').hide();
            $('#inputTunaiContainer').find('input').val('').trigger('change');
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('').trigger('change');
            $('#inputTransferBankContainer').find('select').val('').trigger('change');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('').trigger('change');
            $('#inputKartuContainer').find('select').val('').trigger('change');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('').trigger('change');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('').trigger('change');
            $('#inputBGContainer').find('input').val('').trigger('change');

            $('#inputTunaiCashbackContainer').hide();
            $('#inputTunaiCashbackContainer').find('input').val('').trigger('change');
            $('#inputTransferBankCashbackContainer').hide();
            $('#inputTransferBankCashbackContainer').find('input').val('').trigger('change');
            $('#inputTransferBankCashbackContainer').find('select').val('').trigger('change');
            $('#inputKartuCashbackContainer').hide();
            $('#inputKartuCashbackContainer').find('input').val('').trigger('change');
            $('#inputKartuCashbackContainer').find('select').val('').trigger('change');

            $('#btnCekCashback').hide();
            $('#btnBGCashback').hide();
            $('#btnPiutangCashback').hide();

            var harga_total = parseFloat(transaksi_pembelian.harga_total);
            // var nego_total = parseFloat(transaksi_pembelian.nego_total);
            // var check_nego_total = parseFloat(transaksi_pembelian.check_nego_total);
            var potongan_pembelian = parseFloat(transaksi_pembelian.potongan_pembelian);
            var ongkos_kirim = parseFloat(transaksi_pembelian.ongkos_kirim);
            var ppn_total = parseFloat(transaksi_pembelian.ppn_total);
            var jumlah_bayar = parseFloat(transaksi_pembelian.jumlah_bayar);
            var nominal_tunai = parseFloat(transaksi_pembelian.nominal_tunai);
            var no_transfer = parseFloat(transaksi_pembelian.no_transfer);
            var bank_transfer = parseFloat(transaksi_pembelian.bank_transfer);
            var nominal_transfer = parseFloat(transaksi_pembelian.nominal_transfer);
            var no_kartu = parseFloat(transaksi_pembelian.no_kartu);
            var nominal_kartu = parseFloat(transaksi_pembelian.nominal_kartu);
            var bank_kartu = parseFloat(transaksi_pembelian.bank_kartu);
            var no_cek =transaksi_pembelian.no_cek;
            var nominal_cek = parseFloat(transaksi_pembelian.nominal_cek);
            var bank_cek = parseFloat(transaksi_pembelian.bank_cek);
            var aktif_cek = parseFloat(transaksi_pembelian.aktif_cek);
            var no_bg = transaksi_pembelian.no_bg;
            var nominal_bg = parseFloat(transaksi_pembelian.nominal_bg);
            var bank_bg = parseFloat(transaksi_pembelian.bank_bg);
            var aktif_bg = parseFloat(transaksi_pembelian.aktif_bg);
            var jatuh_tempo = transaksi_pembelian.jatuh_tempo;

            var jenis_kartu = transaksi_pembelian.jenis_kartu;

            if (isNaN(harga_total)) harga_total = 0;
            // if (isNaN(nego_total)) nego_total = 0;
            // if (isNaN(check_nego_total)) check_nego_total = 0;
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            if (isNaN(ppn_total)) ppn_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(no_transfer)) no_transfer = 0;
            if (isNaN(bank_transfer)) bank_transfer = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(no_kartu)) no_kartu = 0;
            // if (isNaN(jenis_kartu)) jenis_kartu = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(bank_kartu)) bank_kartu = 0;
            // if (isNaN(no_cek)) no_cek = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(bank_cek)) bank_cek = 0;
            if (isNaN(aktif_cek)) aktif_cek = 0;
            // if (isNaN(no_bg)) no_bg = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(bank_bg)) bank_bg = 0;
            if (isNaN(aktif_bg)) aktif_bg = 0;
            // if (isNaN(jatuh_tempo)) jatuh_tempo = 0;

            // nominal_tunai = jumlah_bayar - nominal_transfer - nominal_kartu - nominal_cek - nominal_bg;

            // if (jatuh_tempo != null) {
            //     $('#pilihTanggal').val(ymd2dmy(jatuh_tempo));
            //     $('input[name="jatuh_tempo"]').val(jatuh_tempo);
            // }

            if (nominal_tunai > 0) {
                // Buka Metode Pembayaran Tunai
                $('#btnTunai').trigger('click');
                $('#inputNominalTunai').val(nominal_tunai).trigger('keyup');
            }

            if (nominal_transfer > 0) {
                // Buka Metode Pembayaran Transfer
                $('#btnTransfer').trigger('click');
                $('#inputNoTransfer').val(no_transfer).trigger('keyup');
                $('#inputNominalTransfrer').val(nominal_transfer).trigger('keyup');
                $('select[name="bank_transfer"]').val(bank_transfer).trigger('change');
            }

            if (nominal_kartu > 0) {
                // Buka Metode Pembayaran Kartu
                $('#btnKartu').trigger('click');
                $('#inputNoKartu').val(no_kartu).trigger('keyup');
                $('#inputNominalKartu').val(nominal_kartu).trigger('keyup');
                $('select[name="bank_kartu"]').val(bank_kartu).trigger('change');
                $('select[name="jenis_kartu"]').val(jenis_kartu).trigger('change');
            }

            if (nominal_cek > 0) {
                // Buka Metode Pembayaran Cek
                $('#btnCek').trigger('click');
                // $('#inputNoCek').val(no_cek).trigger('keyup');
                // $('#inputNominalCek').val(nominal_cek).trigger('keyup');
                if(cek_terpilih != null){
                    $('select[name="cek_id"]').val(cek_terpilih.id).change().trigger('change');
                    $('select[name="cek_id"]').prop('disabled', true);
                }else{
                    $('select[name="cek_id"]').val('cek_baru').change();
                    $('select[name="bank_cek"]').val(bank_cek).change();
                    // $('select[name="bank_cek"]').val(bank_cek).change();
                    $('#inputNominalCek').val(nominal_cek).trigger('keyup');
                    $('#inputNomorCek').val(no_cek).trigger('keyup');
                    $('#inputNomorCek').prop('readonly', true);
                    // $('#inputNominalCek').val(nominal_cek).trigger('keyup');
                }
            }

            if (nominal_bg > 0) {
                // Buka Metode Pembayaran BG
                $('#btnBG').trigger('click');
                // $('#pilihTanggal').val(ymd2dmy(jatuh_tempo));
                // $('input[name="jatuh_tempo"]').val(jatuh_tempo);
                
                if(bg_terpilih != null){
                    $('select[name="bg_id"]').val(bg_terpilih.id).change().trigger('change');
                    $('select[name="bg_id"]').prop('disabled', true);
                }else{
                    $('select[name="bg_id"]').val('bg_baru').change();
                    $('select[name="bank_bg"]').val(bank_bg).change();
                    $('#inputNomorBG').val(no_bg).trigger('keyup');
                    $('#inputNominalBG').val(nominal_bg).trigger('keyup');
                    $('#inputNomorBG').prop('readonly', true);
                }
            }

            if (potongan_pembelian > 0) {
                $('#inputPotonganPembelian').val(potongan_pembelian).trigger('keyup');
            }

            if (ongkos_kirim > 0) {
                $('#inputOngkosKirim').val(ongkos_kirim).trigger('keyup');
            }

            if (ppn_total > 0) {
                $('#checkPPNTotal').trigger('click');
                $('#inputPPNTotal').val(ppn_total).trigger('keyup');
            }

            setTimeout(function() {
                if (ppn_total > 0) {
                    $('#checkPPNTotal').prop('checked', true).trigger('change');
                    $('#inputPPNTotal').val(ppn_total).trigger('keyup');
                }
            }, 5000);


            // setTimeout(function() {
            //     $('#suplier_form').select2({disabled:true});
            //     $('#nota').prop('readonly', true);
            //     $('#seller_form').select2({disabled:true});
            //     $('#item_form').select2({disabled:true});
            //     $('#pilihTanggal').prop('disabled', true);
            //     $('#pilihTanggal').addClass('bg-disabled');

            // }, 5000);

            // var token = $('input[name="_token"]').val();
            // $('#formSimpanContainer').find('input').val('');
            // $('input[name="_token"]').val(token);
        });

        // Buat ambil nomer transaksi terakhir
        /*$(window).on('load', function(event) {
            event.preventDefault();

            var url = "{{ url('transaksi-pembelian/last.json') }}";

            $.get(url, function(data) {
                var kode = 1;
                if (data.transaksi_pembelian !== null) {
                    var kode_transaksi = data.transaksi_pembelian.kode_transaksi;
                    kode = parseInt(kode_transaksi.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_transaksi = kode + '/TRAB/' + tanggal;

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });*/

        /*$(document).on('keyup', '#inputKodeItem', function(event) {
            event.preventDefault();

            var kode_item = $(this).val();
            console.log(kode_item);
        });*/

        $(document).on('change', 'select[name="suplier_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if(id != ''){
                var url_items = "{{ url('transaksi-pembelian') }}" + '/' + id + '/items.json';
                var url_sellers = "{{ url('transaksi-pembelian') }}" + '/' + id + '/sellers.json';
                var url_suplier = "{{ url('transaksi-pembelian') }}" + '/' + id + '/suplier_id.json';
                // console.log(url_items, url_suplier);
                $('input[name="suplier_id"]').val(id);
                $('#tabelKeranjang > tbody').empty();
                $('#append-section').empty();
                $('#formSimpanContainer').find('input[name="harga_total"]').val('');
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
                $('#formSimpanContainer').find('input[name="utang"]').val('');

                var $select_item = $('select[name="item_id"]');
                $select_item.empty();
                $select_item.append($('<option value="">Pilih Item</option>'));

                $.get(url_items, function(data) {
                    var items = data.items;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        $select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
                    }
                });

                var $select_sellers = $('select[name="seller_id"]');
                $select_sellers.empty();
                $select_sellers.append($('<option value="">Pilih Penjual</option>'));

                $('#spinner').show();
                selected_items = [];
                $.get(url_sellers, function(data) {
                    var sellers = data.sellers;
                    for (var i = 0; i < sellers.length; i++) {
                        var seller = sellers[i];
                        $select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
                    }
                    // $select_sellers.append($('<option value="lain">Sales Lain</option>'));
                    $('#spinner').hide();
                });
                $.get(url_suplier, function(data) {
                    // console.log(data);
                    var piutang = data.suplier.piutang;
                    if (piutang != null) piutang = parseFloat(piutang.replace(/\D/g, ''), 10) / 1000;
                    if (piutang == null) piutang = 0;
                    if (isNaN(piutang)) piutang = 0;

                    if(piutang == 0 ){
                        $('#btnPiutang').hide('slow/400/fast', function() {
                        });
                        $('#JumlahPiutang').text('');
                        $('#inputPiutangContainer').hide('hide', function() {
                            $('#formSimpanContainer').find('input[name="nominal_piutang"]').val('');
                            $('#btnPiutang').find('input').val('');
                            $('#JumlahPiutang').text('');
                            updateHargaOnKeyup();
                        });
                    }else{
                        $('#btnPiutang').show('slow/400/fast', function() {
                        });
                        $('#btnPiutang').removeClass('btn-dark');
                        $('#btnPiutang').addClass('btn-default');
                        $('#btnPiutang').find('i').hide('fast');
                        $('#inputPiutangContainer').hide('hide', function() {
                            $('#formSimpanContainer').find('input[name="nominal_piutang"]').val('');
                            $('#btnPiutang').find('input').val('');
                            $('#JumlahPiutang').text('Rp'+piutang.toLocaleString());
                            updateHargaOnKeyup();
                        });
                    }
                });
            } else {
                $('#btnPiutang').show('slow/400/fast', function() {
                });
                $('#btnPiutang').removeClass('btn-dark');
                $('#btnPiutang').addClass('btn-default');
                $('#btnPiutang').find('i').hide('fast');
                $('#inputPiutangContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_piutang"]').val('');
                    $('#btnPiutang').find('input').val('');
                    $('#JumlahPiutang').text('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('change', 'select[name="seller_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var nama = $("#seller option:selected").text();
            if (id == 'lain') {
                $('input[name="nama_sales"]').prop('disabled', false);
                $('input[name="seller_id"]').val('');
            } else {
                $('input[name="seller_id"]').val(id);
                $('input[name="nama_sales"]').val(nama);
            }
        });

        $(document).on('keyup', 'input[name="nama_sales"]', function(event) {
            event.preventDefault();

            var nama = $(this).val();
            $('input[name="seller_name"]').val(nama);
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var id = $('select[name="item_id"]').val();
            cariItem(id, false, -1);
        });

        $(document).on('focus', '.inputKadaluarsa', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // $(this).daterangepicker({
            //     singleDatePicker: true,
            //     calender_style: "picker_2",
            //     format: 'DD-MM-YYYY'
            // }, function(start) {
            //     var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
            //         .find('input[name="inputKadaluarsa"]').val();

            //     $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-0"]').val(kadaluarsa);
            // });

            $(this).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start) {
                var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
                    .find('input[name="inputKadaluarsa"]').val();

                $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-0"]').val(kadaluarsa);
            });

        });

        $(document).on('focus', '.inputKadaluarsaStok', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            var kol_kadal = $(this).parents('.kol-kadal');
            // console.log(jkal);
            // $(this).daterangepicker({
            //     singleDatePicker: true,
            //     calender_style: "picker_2",
            //     format: 'DD-MM-YYYY'
            // }, function(start) {
            //     var kadaluarsa = kol_kadal.find('input[name="inputKadaluarsa"]').val();
            //     $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-'+jkal+'"]').val(kadaluarsa);
            // });

            $(this).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start) {
                var kadaluarsa = kol_kadal.find('input[name="inputKadaluarsa"]').val();
                $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-'+jkal+'"]').val(kadaluarsa);
            });
        });

        $(document).on('change', '#checkKadaluarsa', function(event) {
            event.preventDefault();

            $(this).parent().next().val('');
            var checked = $(this).prop('checked');
            if (checked) {
                // $(this).parent().next().prop('readonly', false);
                $(this).parent().next().removeClass('bg-disabled');
            } else {
                // $(this).parent().next().prop('readonly', true);
                $(this).parent().next().addClass('bg-disabled');
                id = $(this).parents('tr').first().data('id');
                $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
                $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="" />');
            }
        });

        $(document).on('keyup', 'input[name="inputJumlah"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $subtotal = $tr.find('input[name="inputSubTotal"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
            var jumlah = 0;
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            }); 
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;

            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            // var hpp = parseInt(harga/1.1);
            // if (isNaN(hpp)) hpp = 0;
            // var ppn = harga-hpp;
            // if (isNaN(ppn)) ppn = 0;
            var ppn = 0;
            if(!$('#checkPPNTotal').prop('checked')){
                ppn = harga / 11;
            }
            var hpp = harga - ppn;

            if (isNaN(ppn)) ppn = 0;
            if (isNaN(hpp)) hpp = 0;

            var form_ppn = ppn;

            harga = parseFloat(harga.toFixed(2));
            hpp = parseFloat(hpp.toFixed(2));
            ppn = parseFloat(ppn.toFixed(2));

            $jumlah.val(jumlah);
            $harga.val(harga.toLocaleString(['ban', 'id']));
            $hpp.val(hpp.toLocaleString(['ban', 'id']));
            if (!$('#checkPPN').prop('checked')) {
                $ppn.val(ppn.toLocaleString(['ban', 'id']));
            }

            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);
            if (!$('#checkPPN').prop('checked')) {
                $('#form-simpan').find('input[id="ppn-' + id + '"]').val(form_ppn);
            }

            var skal = $('#kadaluarsa-'+id).attr('skal');
            if (skal == 0) {
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            } else {
                var total_hasil = 0;
                $('.jumlah-stok-'+id).each(function(index, el) {
                    var jumlah_stok = parseInt($(el).val());
                    if (isNaN(jumlah_stok)) jumlah_stok = 0;
                    total_hasil += jumlah_stok;
                })

                if (parseInt(total_hasil) != parseInt(jumlah)) {
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).addClass('has-error');
                    });
                } else {
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).removeClass('has-error');
                    });
                }
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });

            var hpp_stok = $('#form-simpan').find('input[id="harga-'+id+'"]').val();
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp_stok);
            });

            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(['ban', 'id']));
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputSubTotal', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;

            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            // var hpp = parseInt(harga/1.1);
            // if (isNaN(hpp)) hpp = 0;
            // var ppn = harga-hpp;
            // if (isNaN(ppn)) ppn = 0;
            // console.log(ppn, hpp);
            // var ppn = parseFloat((harga / 11).toFixed(2));
            var ppn = 0;
            if(!$('#checkPPNTotal').prop('checked')){
                ppn = harga / 11;
            }
            var hpp = harga - ppn;

            if (isNaN(ppn)) ppn = 0;
            if (isNaN(hpp)) hpp = 0;

            var form_ppn = ppn;

            harga = parseFloat(harga.toFixed(2));
            hpp = parseFloat(hpp.toFixed(2));
            ppn = parseFloat(ppn.toFixed(2));

            $harga.val(harga.toLocaleString(['ban', 'id']));
            $hpp.val(hpp.toLocaleString(['ban', 'id']));
            if (!$('#checkPPN').prop('checked')) {
                $ppn.val(ppn.toLocaleString(['ban', 'id']));
            }

            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);
            if (!$('#checkPPN').prop('checked')) {
                $('#form-simpan').find('input[id="ppn-' + id + '"]').val(form_ppn);
            }

            var skal = $('#kadaluarsa-'+id).attr('skal');
            if(skal == 0){
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });

            var hpp_stok = $('#form-simpan').find('input[id="harga-'+id+'"]').val();
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp_stok);
            });

            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(['ban', 'id']));
            updateHargaOnKeyup();
        });

        $(document).on('change', '#checkPPN', function(event) {
            event.preventDefault();

            // $(this).parents('.input-group').find('#inputPPN').val('');
            // var id = $(this).parents('tr').first().attr('data-id');
            var checked = $(this).prop('checked');
            if (checked) {
                var $tr = $(this).parents('tr').first();
                var id = $tr.data('id');
                var $kadaluarsa = $tr.find('.inputKadaluarsa');
                var $jumlah = $('#jumlah-'+id);
                var $subtotal = $('#subtotal-'+id);
                var $harga = $('input[name="inputHarga"]');
                var $hpp = $tr.find('input[name="inputHPP"]');
                // var $ppn = $tr.find('input[name="inputPPN"]');

                var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
                var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
                var kadaluarsa = $kadaluarsa.val();

                if (isNaN(subtotal)) subtotal = 0;
                if (isNaN(jumlah)) jumlah = 0;

                var harga = subtotal / jumlah;
                if (isNaN(harga) || harga === Infinity) harga = 0;
                // var hpp = parseInt(harga/1.1);
                // if (isNaN(hpp)) hpp = 0;
                // var ppn = harga-hpp;
                // if (isNaN(ppn)) ppn = 0;
                // console.log(ppn, hpp);
                // var ppn = parseFloat((harga / 11).toFixed(2));
                // var ppn = harga / 11;
                // var hpp = harga - ppn;
                var hpp = harga;

                // if (isNaN(ppn)) ppn = 0;
                if (isNaN(hpp)) hpp = 0;

                // var form_ppn = ppn;

                harga = parseFloat(harga.toFixed(2));
                hpp = parseFloat(hpp.toFixed(2));
                // ppn = parseFloat(ppn.toFixed(2));

                $harga.val(harga.toLocaleString(['ban', 'id']));
                $hpp.val(hpp.toLocaleString(['ban', 'id']));
                // $ppn.val(ppn.toLocaleString(['ban', 'id']));

                // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
                // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
                $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
                $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
                // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
                $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);
                // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(form_ppn);

                var skal = $('#kadaluarsa-'+id).attr('skal');
                if (skal == 0) {
                    $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                    $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
                }

                var totalharga = 0;
                $('input[name="inputSubTotal"]').each(function(index, el) {
                    var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                    if (isNaN(subtotal)) subtotal = 0;
                    totalharga += subtotal;
                });

                var hpp_stok = $('#form-simpan').find('input[id="harga-'+id+'"]').val()
                $('.hpp-stok-'+id).each(function(index, el) {
                    $(el).val(hpp_stok);
                })

                $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(['ban', 'id']));
                updateHargaOnKeyup();

                $('#ppn-'+id).val('');
                $(this).parents('.input-group').find('#inputPPN').val('');
                $(this).parents('.input-group').find('#inputPPN').prop('disabled', false);
                $(this).parents('.input-group').find('#inputPPN').trigger('focus');
            } else {
                $(this).parents('tr').first().find('#inputSubTotal').trigger('keyup');
                $(this).parents('.input-group').find('#inputPPN').prop('disabled', true);
            }
        });

        $(document).on('keyup', '#inputPPN', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().attr('data-id');
            var ppn = parseFloat($(this).val().replace(/\D/g, ''), 10);
            $('#ppn-'+id).val(ppn);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#tambah_kadaluarsa', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');

            var jumlah_kadaluarsa = 0;
            $('#anak-kadaluarsa-'+id+' .m-kadal').each(function(index, el) {
                jumlah_kadaluarsa++;
            });
            jumlah_kadaluarsa++;

            var jumlah_stok = 0;
            var stok_id = 0;
            var stok_jumlah_beli = 0;
            var stok_kadaluarsa = '';
            var temp_stoks = [];
            for (var i = 0; i < stoks.length; i++) {
                var _stok = stoks[i];
                if (_stok.item.id == id) {
                    temp_stoks.push(_stok);
                }
            }

            var temp_stok = null;
            for (var i = 0; i < temp_stoks.length; i++) {
                var _stok = temp_stoks[i];
                if (i == jumlah_kadaluarsa - 1) {
                    temp_stok = _stok;
                    stok_id = _stok.id;
                    stok_jumlah_beli = _stok.jumlah_beli;
                    if (_stok.kadaluarsa != '0000-00-00') {
                        stok_kadaluarsa = _stok.kadaluarsa;
                    }
                }
            }

            // var satuan = [[],[]];
            var satuans = [];
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                // console.log(index);
                var konversi = parseInt($(el).attr('konversi'));
                // var kode_satuan = $(el).next().find('.kode_satuan').text();
                var kode_satuan = $(el).next().text();
                // satuan[index]['konversi'] = konversi;
                // satuan[index]['kode'] = kode_satuan;
                satuans.push({'konversi' : konversi, 'kode' : kode_satuan});
            });
            // console.log(satuans, temp_stok);

            var satuan_pilihan = '';
            var temp_jumlah_beli = stok_jumlah_beli;
            for (var i = 0; i < satuans.length; i++) {
                var satuan = satuans[i];
                var temp_jumlah = 0;
                if (temp_jumlah_beli > 0 && parseInt(temp_jumlah_beli / satuan.konversi) > 0) {
                    temp_jumlah = parseInt(temp_jumlah_beli / satuan.konversi);
                    temp_jumlah_beli = parseInt(temp_jumlah_beli % satuan.konversi);
                }
                // console.log(temp_jumlah_beli, temp_jumlah);

                satuan_pilihan = satuan_pilihan+`<div class="row">
                            <div class="col-md-12">
                                <div class="input-group text-center">
                                    <input id="inputJumlahExp" name="inputJumlahExp" class="pull-right form-control input-sm angka" konversi="`+satuan.konversi+`" value="`+(temp_jumlah>0?temp_jumlah:'')+`" type="text">
                                    <div class="input-group-addon" style="width: 60px; text-align: right;">`+satuan.kode+`
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }

            // var jkal = parseInt($('#anak-kadaluarsa-'+id).attr('jkal'));
            // var skal = $('#kadaluarsa-'+id).attr('skal');
            // if (skal == 0) {
            //     $('#kadaluarsa-'+id).remove();
            // } else {
            //     jkal += 1;
            //     $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jkal+'" value="" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-'+jkal+'" value="' + id + '" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-'+jkal+'" value="" class="hpp-stok-' + id + '" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-'+jkal+'" value="" class="jumlah-stok-' + id + '"/>');
            // }
            // console.log(ymd2dmy(stok_kadaluarsa), stok_kadaluarsa);

            $('#form-simpan').find('#append-section').append('<input type="hidden" name="stok_id[]" id="stok-id-' + id + '-'+jumlah_kadaluarsa+'" value="'+stok_id+'" />');
            // $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jumlah_kadaluarsa+'" value="'+ymd2dmy(stok_kadaluarsa)+'" />');
            // *topik_signature* //
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jumlah_kadaluarsa+'" value="'+stok_kadaluarsa+'" />');
            // *topik_signature* //
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-'+jumlah_kadaluarsa+'" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="hpp-stok-' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="jumlah-stok-' + id + '"/>');

            $('#anak-kadaluarsa-'+id).append(`
                <div class="line"></div>
                    <div class="row kol-kadal m-kadal" kol-kadal="`+jumlah_kadaluarsa+`" stok_id="`+stok_id+`">
                        <div class="col-md-12">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Jumlah
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Kadaluarsa
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0;">
                                `+satuan_pilihan+`
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input name="checkKadaluarsa" id="checkKadaluarsa" type="checkbox">
                                    </span>
                                    <input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok tanggal-putih bg-disabled" readonly="" value="" type="text">
                                </div>
                            </div>
                        </div>
                    </div>`);

            $('tr[data-id="'+id+'"]').find('input[name="inputJumlahExp"]').trigger('keyup');
            if (stok_kadaluarsa != '') {
                $('tr[data-id="'+id+'"]').find('#checkKadaluarsa').prop('checked', true).trigger('change');
                $('tr[data-id="'+id+'"]').find('#inputKadaluarsa').val(ymd2dmy(stok_kadaluarsa));
            }
        });

        $(document).on('keyup', 'input[name="inputJumlahExp"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');
            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var jumlah = 0;
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            // console.log(jkal);
            var kol_kadal = $(this).parents('.kol-kadal').first().find('input[name="inputJumlahExp"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            });

            var hpp = $('#form-simpan').find('input[id="harga-'+id+'"]').val();
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp);
            })
            // console.log(jumlah);
            $('#form-simpan').find('input[id="jumlah-stok-' + id + '-'+jkal+'"]').val(jumlah);
            var jumlah_total = $('#form-simpan').find('input[id="jumlah-'+id+'"]').val();

            var total_hasil = 0;
            $('.jumlah-stok-'+id).each(function(index, el) {
                var jumlah_stok = parseInt($(el).val());
                if (isNaN(jumlah_stok)) jumlah_stok = 0;
                total_hasil += jumlah_stok;
            })

            if (parseInt(total_hasil) != parseInt(jumlah_total)) {
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).addClass('has-error');
                });
            } else {
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).removeClass('has-error');
                });
            }
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').data('id');

            var index = selected_items.indexOf(parseInt(id));
            if (index >= -1) {
                selected_items.splice(index, 1);
                updateHargaOnKeyup();
            }

            $('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
            $('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();

            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('input[id="harga-' + id + '"]').remove();
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();
            $('#form-simpan').find('input[id="ppn-' + id + '"]').remove();
            $('#form-simpan').find('input[id^="kadaluarsa-stok-' + id + '"]').remove();
            $('#form-simpan').find('input[id^="item-stok-' + id + '"]').remove();
            $('#form-simpan').find('input[id^="hpp-stok-' + id + '"]').remove();
            $('#form-simpan').find('input[id^="jumlah-stok-' + id + '"]').remove();

            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });
            $('input[name="inputHargaTotal"]').val(harga_total.toLocaleString(['ban', 'id']));

            var ongkos_kirim = parseInt($('input[name="inputOngkosKirim"]').val().replace(/\D/g, ''), 10);
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

            var harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
            $('input[name="inputHargaTotalPlusOngkosKirim"]').val(harga_total_plus_ongkos_kirim.toLocaleString(['ban', 'id']));

            var jumlah_bayar = parseInt($('input[name="inputJumlahBayar"]').val().replace(/\D/g, ''), 10);
            var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
            if (utang <= 0 || isNaN(utang)) utang = 0;
            $('input[name="inputUtang"]').val(utang.toLocaleString(['ban', 'id']));

            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="utang"]').val(utang);
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('select[name="bank_transfer"]').val('').change();
                    $('input[name="no_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="no_kartu"]').val('');
                    $('select[name="jenis_kartu"]').val('').change();
                    $('select[name="bank_kartu"]').val('').change();
                    
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                    $('input[name="bank_cek"]').val('').change();
                    $('select[name="cek_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                    $('select[name="bank_bg"]').val('').change();
                    $('select[name="bg_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnPiutang', function(event) {
            event.preventDefault();

            var suplier = $('select[name="suplier_id"]').val();
            if (suplier != '') {
                // console.log(suplier);
                var url = "{{ url('transaksi-pembelian') }}" + '/' + suplier + '/suplier.json';
                $.get(url, function(data) {
                    piutang += parseInt(data.suplier.piutang);
                    // console.log(piutang);
                    if ($('#btnPiutang').hasClass('btn-default')) {
                        $('#btnPiutang').removeClass('btn-default');
                        $('#btnPiutang').addClass('btn-dark');
                        $('#btnPiutang').find('i').show('fast');
                        $('#inputPiutangContainer').show('fast', function() {
                            $(this).find('input').first().trigger('focus');
                            // $('#btnPiutang').find('input').first().trigger('focus');
                        });
                    } else if ($('#btnPiutang').hasClass('btn-dark')) {
                        $('#btnPiutang').removeClass('btn-dark');
                        $('#btnPiutang').addClass('btn-default');
                        $('#btnPiutang').find('i').hide('fast');
                        $('#inputPiutangContainer').hide('fast', function() {
                            $('input[name="nominal_piutang"]').val('');
                            $('#btnPiutang').find('input').val('');
                            $('#inputNominalPiutang').val('');
                            updateHargaOnKeyup();
                        });
                    }
                });
            }
        });

        $(document).on('click', '#btnTunaiCashback', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiCashbackContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiCashbackContainer').hide('fast', function() {
                    $('input[name="nominal_tunai_cashback"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransferCashback', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankCashbackContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankCashbackContainer').hide('fast', function() {
                    $('select[name="bank_transfer_cashback"]').val('').change();
                    $('input[name="no_transfer_cashback"]').val('');
                    $('input[name="nominal_transfer_cashback"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartuCashback', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuCashbackContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuCashbackContainer').hide('fast', function() {
                    $('input[name="jenis_kartu_cashback"]').val('');
                    $('input[name="nominal_kartu_cashback"]').val('');
                    $('input[name="bank_kartu_cashback"]').val('');
                    $('input[name="no_kartu_cashback"]').val('');
                    $('select[name="jenis_kartu_cashback"]').val('').change();
                    $('select[name="bank_kartu_cashback"]').val('').change();
                    
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_transfer"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            // console.log(bank_transfer);
            $('#formSimpanContainer').find('input[name="bank_transfer"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('#formSimpanContainer').find('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="cek_id"]', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
            updateHargaOnKeyup();

            var utang = $('input[name="inputUtang"]').val();
            utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10));

            var cek_id = $(this).val();
            if(cek_id != 'cek_baru'){
                var cek_id = parseInt($(this).val());
            }

            if (Number.isInteger(cek_id)) {
                var cek_text = $(this).select2('data')[0].text;
                var nominal_text = cek_text.split('Rp')[1].split(']')[0];
                var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
                nominal = parseFloat(nominal);
                var nomor = cek_text.split(' [')[0];
                $('#cek_baru').addClass('sembunyi');
                $('#inputNominalCek').prop('disabled', true);
                $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
                $('select[name="bank_cek"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputCekContainer').find('p').hide();
                    $('#inputNominalCek').val(nominal_text);
                    $('#formSimpanContainer').find('input[name="no_cek"]').val(nomor);
                    $('input[name="cek_id"]').val(cek_id);
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
                    // Button
                } else {
                    // Error
                    console.log('cek kebanyakan');
                    $('#inputCekContainer').find('p').show();
                    $('#inputNominalCek').val('');
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    // Button
                }
            }else if(cek_id == 'cek_baru'){
                $('#inputCekContainer').find('p').hide();
                $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                $('#cek_baru').removeClass('sembunyi');
                $('#inputNominalCek').prop('disabled', false);
                $('#inputNominalCek').val('');
            } else {
                $('#inputCekContainer').find('p').hide();
                $('#cek_baru').addClass('sembunyi');
                $('#inputNominalCek').prop('disabled', true);
                $('select[name="bank_cek"]').val('').change();
                $('#inputNominalCek').val('');
                $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
                // Button
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('#formSimpanContainer').find('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_cek"]', function(event) {
            event.preventDefault();

            var bank_cek = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_cek"]').val(bank_cek);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bg_id"]', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
            updateHargaOnKeyup();

            var utang = $('input[name="inputUtang"]').val();
            utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10));

            var bg_id = $(this).val();
            if(bg_id != 'bg_baru'){
                var bg_id = parseInt($(this).val());
            }

            if (Number.isInteger(bg_id) && bg_id != '') {
                console.log(bg_id);
                var bg_text = $(this).select2('data')[0].text;
                var nominal_text = bg_text.split('Rp')[1].split(']')[0];
                var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
                nominal = parseFloat(nominal);
                var nomor = bg_text.split(' [')[0];
                $('#inputNominalBG').prop('disabled', true);
                $('#bg_baru').addClass('sembunyi');
                $('select[name="bank_bg"]').val('').change();
                $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
                if (nominal <= utang) {
                    // Success
                    $('#inputBGContainer').find('p').hide();
                    $('#inputNominalBG').val(nominal_text);
                    $('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
                    $('input[name="bg_id"]').val(bg_id);
                } else {
                    $('#inputBGContainer').find('p').show();
                    $('#inputNominalBG').val('');
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                }
            }else if(bg_id == 'bg_baru'){
                $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                $('#bg_baru').removeClass('sembunyi');
                $('#inputNominalBG').prop('disabled', false);
                $('#inputNominalBG').val('');
            } else {
                $('#bg_baru').addClass('sembunyi');
                $('#inputNominalBG').prop('disabled', true);
                $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
                $('#inputBGContainer').find('p').hide();
                $('select[name="bank_bg"]').val('').change();
                $('#inputNominalBG').val('');
                $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                // Button
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('#formSimpanContainer').find('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_bg"]', function(event) {
            event.preventDefault();

            var bank_bg = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_bg"]').val(bank_bg);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalPiutang', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal_piutang = 0;
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            nominal_piutang += nominal;
            if(nominal > piutang){
                $('#inputPiutangContainer').addClass('has-error');
                $('#submit').prop('disabled', isSubmitButtonDisabled());
            }else{
                $('#inputPiutangContainer').removeClass('has-error');
                $('#submit').prop('disabled', isSubmitButtonDisabled());
            }
            $('#formSimpanContainer').find('input[name="nominal_piutang"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTunaiCashback', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('#formSimpanContainer').find('input[name="nominal_tunai_cashback"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_transfer_cashback"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            // console.log(bank_transfer);
            $('#formSimpanContainer').find('input[name="bank_transfer_cashback"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransferCashback', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('#formSimpanContainer').find('input[name="no_transfer_cashback"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfrerCashback', function(event) {
            event.preventDefault();

            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('#formSimpanContainer').find('input[name="nominal_transfer_cashback"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_kartu_cashback"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_kartu_cashback"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu_cashback"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="jenis_kartu_cashback"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartuCashback', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="no_kartu_cashback"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartuCashback', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_kartu_cashback"]').val(nominal);
            updateHargaOnKeyup();
        });

        /*$(document).on('change', 'select[name="kredit_id"]', function(event) {
            event.preventDefault();

            var no_kredit = $(this).val();
            // console.log(no_kredit);
            $('#formSimpanContainer').find('input[name="no_kredit"]').val(no_kredit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKredit', function(event) {
            event.preventDefault();

            var nominal_kredit = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kredit)) nominal_kredit = 0;
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(nominal_kredit);
            updateHargaOnKeyup();
        });*/

        $(document).on('keyup', '#inputPotonganPembelian', function(event) {
            event.preventDefault();

            var potongan_pembelian = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            $('#formSimpanContainer').find('input[name="potongan_pembelian"]').val(potongan_pembelian);
            updateHargaOnKeyup();
        });

        $(document).on('change', '#checkPPNTotal', function(event) {
            event.preventDefault();

            // $(this).parents('.input-group').find('#inputPPN').val('');
            // var id = $(this).parents('tr').first().attr('data-id');
            var checked = $(this).prop('checked');
            if (checked) {
                $('#tabelKeranjang tr').each(function(index, el) {
                    if ($(el).find('#checkPPN').prop('checked')) {
                        $(el).find('#inputPPN').val(0);
                        $(el).find('#inputPPN').trigger('change');
                    } else {
                        $(el).find('#checkPPN').trigger('click');
                        $(el).find('#inputPPN').val(0);
                        $(el).find('#inputPPN').trigger('change');
                    }
                });
                $('#inputPPNTotal').attr('disabled', false);
                $('#inputPPNTotal').trigger('focus');
            } else {
                $('#tabelKeranjang tr').each(function(index, el) {
                    if ($(el).find('#checkPPN').prop('checked')) {
                        $(el).find('#checkPPN').trigger('click');
                    } else {
                        // $(el).find('#inputPPN').val(0);
                        // $(el).find('#inputPPN').trigger('change');
                    }
                });
                $('#inputPPNTotal').val('').trigger('keyup');
                $('#inputPPNTotal').attr('disabled', true);
            }
        });

        $(document).on('keyup', '#inputPPNTotal', function(event) {
            event.preventDefault();

            var ppn_total = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(ppn_total)) ppn_total = 0;
            $('input[name="ppn_total"]').val(ppn_total);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputOngkosKirim', function(event) {
            event.preventDefault();

            var ongkos_kirim = parseInt($(this).val().replace(/\D/g, ''), 10);
            // console.log(ongkos_kirim);
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val(ongkos_kirim);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#nota', function(event) {
            event.preventDefault();

            var nota = $(this).val();
            $('#formSimpanContainer').find('input[name="nota"]').val(nota);
            updateHargaOnKeyup();
        });

        $(document).on('focus', '#pilihTanggal', function(event) {
            event.preventDefault();

            $('#pilihTanggal').daterangepicker({
                    autoApply: true,
                    calender_style: "picker_2",
                    showDropdowns: true,
                    format: 'DD-MM-YYYY',
                    locale: {
                        "applyLabel": "Pilih",
                        "cancelLabel": "Batal",
                        "fromLabel": "Awal",
                        "toLabel": "Akhir",
                        "customRangeLabel": "Custom",
                        "weekLabel": "M",
                        "daysOfWeek": [
                            "Min",
                            "Sen",
                            "Sel",
                            "Rab",
                            "Kam",
                            "Jum",
                            "Sab"
                        ],
                        "monthNames": [
                            "Januari",
                            "Februari",
                            "Maret",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "Agustus",
                            "September",
                            "Oktober",
                            "November",
                            "Desember"
                        ],
                        "firstDay": 1
                    },
                    singleDatePicker: true
            }, function(start) {
                $('#pilihTahun').val('');
                $('#pilihBulan').val('');
                $('#pilihRentangTanggal').val('');
                var beban = $('#pilihBeban').val();

                var tanggal = (start.toISOString()).substring(0,10);
                // $('input[name="jatuh_tempo"]').val(tanggal);
                updateHargaOnKeyup();
            });
        });

    </script>
@endsection
