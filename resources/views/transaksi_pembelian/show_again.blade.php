@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambahRetur, #btnLihatRetur, #btnSesuaikanCek, #btnSesuaikanBG, #btnKembali {
            margin-bottom: 0;
        }
        .no-border {
            border: none;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Transaksi Pembelian</h2>
                <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur/create') }}" id="btnTambahRetur" data-toggle="tooltip" data-placement="top" title="Retur" class="btn btn-sm btn-success pull-right" style="margin-right: 0;">
                    <i class="fa fa-mail-reply"></i>
                </a>
                <!-- <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id.'/retur') }}" id="btnLihatRetur" data-toggle="tooltip" data-placement="top" title="Lihat Retur" class="btn btn-sm btn-warning pull-right">
                    <i class="fa fa-sign-in"></i>
                </a> -->
                @if($transaksi_pembelian->status_hutang > 0)
                    <a href="{{ url('hutang/show/'.$transaksi_pembelian->id) }}" data-toggle="tooltip" data-placement="top" title="Bayar Hutang" class="btn btn-sm btn-primary pull-right">
                        <i class="fa fa-money"></i>
                    </a>
                @endif
                @if(in_array(Auth::user()->level_id, [1, 2]))
                    <a  href="{{ url('transaksi-pembelian/edit/'.$transaksi_pembelian->id) }}" class="btn btn-sm btn-warning pull-right" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Ubah Transaksi Pembelian">
                        <i class="fa fa-edit"></i>
                    </a>
                @endif
                @if($transaksi_pembelian->aktif_bg == 1)
                    <a id="btnSesuaikanBG" data-toggle="tooltip" data-placement="top" title="Sesuaikan BG" class="btn btn-sm btn-BG pull-right">
                        <!-- <i class="fa fa-money"></i> -->BG
                    </a>
                @endif
                @if($transaksi_pembelian->aktif_cek == 1)
                    <a id="btnSesuaikanCek" data-toggle="tooltip" data-placement="top" title="Sesuaikan Cek" class="btn btn-sm btn-success pull-right">
                        <!-- <i class="fa fa-money"></i> -->CEK
                    </a>
                @endif
                <a href="{{ url('transaksi-pembelian') }}" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_pembelian->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Kode Faktur</th>
                                    <td style="width: 60%;">
                                        @if($transaksi_pembelian->nota != NULL)
                                        {{ $transaksi_pembelian->nota }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Pemasok</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Penjual</th>
                                    @if ($transaksi_pembelian->seller_id != null)
                                    <td>{{ $transaksi_pembelian->seller->nama }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Tanggal Pembelian</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_pembelian->utang != null && $transaksi_pembelian->utang > 0)
                                <tr>
                                    <th>Hutang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->utang) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_tunai != null && $transaksi_pembelian->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_transfer != null && $transaksi_pembelian->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_transfer) }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_transfers->nama_bank }} [{{ $transaksi_pembelian->bank_transfers->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_kartu != null && $transaksi_pembelian->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_kartu) }}</td>
                                </tr>
                                <tr>
                                    <th>Nomor Transaksi Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_kartu }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->jenis_kartu }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_kartus->nama_bank }} [{{ $transaksi_pembelian->bank_kartus->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_cek != null && $transaksi_pembelian->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">
                                    @if($transaksi_pembelian->aktif_cek == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_cek != null)
                                    <tr>
                                        <th>Bank Cek</th>
                                        <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_ceks->nama_bank }} [{{ $transaksi_pembelian->bank_ceks->no_rekening }}]</td>
                                    </tr>
                                @endif

                                @if ($transaksi_pembelian->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_bg != null && $transaksi_pembelian->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">@if($transaksi_pembelian->aktif_bg == 1)
                                        Menunggu Pencairan {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @else
                                        {{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}
                                    @endif
                                    </td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_bg != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank_bgs->nama_bank }} [{{ $transaksi_pembelian->bank_bgs->no_rekening }}]</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Harga Total</th>
                                    @if ($transaksi_pembelian->harga_total != null && $transaksi_pembelian->harga_total > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->ppn_total) }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Potongan Pembelian</th>
                                    @if ($transaksi_pembelian->potongan_pembelian != null && $transaksi_pembelian->potongan_pembelian > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->potongan_pembelian) }}</td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Ongkos Kirim</th>
                                    @if ($transaksi_pembelian->ongkos_kirim != null && $transaksi_pembelian->ongkos_kirim > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ongkos_kirim) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                @if ($transaksi_pembelian->ppn_total != null && $transaksi_pembelian->ppn_total > 0)
                                    <tr>
                                        <th>PPN Total</th>
                                            <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ppn_total) }}</td>
                                    </tr>
                                @endif
                                

                                <tr>
                                    <th>Jumlah Bayar Awal</th>
                                    @if ($transaksi_pembelian->jumlah_bayar != null && $transaksi_pembelian->jumlah_bayar > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->jumlah_bayar) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                    @endif
                                </tr>

                                <tr>
                                    <th>Jumlah Bayar Saat Ini</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->potongan_pembelian - $transaksi_pembelian->sisa_utang
                                     + $transaksi_pembelian->ongkos_kirim) }}</td>
                                </tr>

                                @if($transaksi_pembelian->sisa_utang>0)
                                    <tr>
                                        <th>Kekurangan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->sisa_utang) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>{{-- 
                                    <th class="text-right">HPP</th>
                                    <th class="text-right">PPN</th>
                                    <th class="text-right">Harga</th> --}}
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_pembelian as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item->kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="text-left jumlah">
                                        {{-- @foreach($hasil[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        {{-- @foreach($hasil[$a] as $x => $jumlah)
                                            {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                            @if ($x != count($hasil[$a]) - 1)
                                            <br>
                                            @endif
                                        @endforeach --}}
                                        {{ $relasi->jumlah }}
                                    </td>
                                    {{-- <td class="text-right">{{ \App\Util::duit($relasi->harga) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 10) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga * 1.1) }}</td> --}}
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <!-- <div class="x_title">
                            <h2>Stok</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Kadaluarsa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stoks as $i => $relasi)
                                {{-- <span style="display: none"> {{ $a = $i }} </span> --}}
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td>
                                        {{-- @foreach($hasil_stoks[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach --}}
                                        @foreach($hasil_stoks[$i] as $x => $jumlah)
                                            @if ($jumlah['jumlah'] != null)
                                                {{ $jumlah['jumlah'] }} {{ $jumlah['satuan'] }}
                                                @if ($x != count($hasil_stoks[$i]))
                                                <br>
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{ \App\Util::date($relasi->kadaluarsa) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Harga Item -->
    @foreach ($items as $i => $item)
    <div class="col-md-6" id="formSimpanHargaContainer" item_kode="{{ $item->kode }}">
        <div class="x_panel">
            <div class="x_title">
                {{-- <p id="formSimpanTitle" class="titleDetailItem">
                    <span><span>Harga Jual </span><span class="nama-item">{{ $item->nama }}</span></span>
                </p>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul> --}}
                <div class="col-md-8">
                        <p id="formSimpanTitle" class="titleDetailItem">
                            <span><span>Harga Jual </span><span class="nama-item">{{ $item->nama }}</span></span>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <a href="{{ url('item/show/'.$item->kode_barang) }}" class="pull-right btn btn-sm btn-info" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Detail Item">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{ url('item/history-harga-jual/'.$item->kode_barang) }}" class="pull-right btn btn-sm btn-pink" id="btnHargaJual" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Jual">
                            <i class="fa fa-history "></i>
                        </a>
                        <a href="{{ url('item/history-harga-beli/'.$item->kode_barang) }}" class="pull-right btn btn-sm btn-silver" id="btnHistory" data-toggle="tooltip" data-placement="top" title="Riwayat Harga Beli">
                            <i class="fa fa-history"></i>
                        </a>
                    </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="{{url('/harga')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="item_kode" value="{{$item->kode}}">
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label class="label-control">Harga Eceran</label>
                            <input type="text" name="eceran_" class="form-control angka" placeholder="Harga Eceran">
                            <input type="hidden" name="eceran">
                        </div>
                        <div class="form-group col-xs-6">
                            <label class="label-control">Harga Grosir</label>
                            <input type="text" name="grosir_" class="form-control angka" placeholder="Harga Grosir">
                            <input type="hidden" name="grosir">
                        </div>
                        <div class="form-group col-xs-6">
                            <select name="satuan_id" class="form-control" id="satuan_harga select2_single">
                                <option value="">Pilih satuan</option>
                                @foreach($item['relasi_satuans'] as $relasi_satuan)
                                <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->kode}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <button class="btn btn-success" id="btnSimpanHarga" type="submit" style="width: 100%; height: 34px;" disabled="">
                                <i class="fa fa-save"></i> <span>Tambah</span>
                            </button>
                        </div>
                    </div>
                </form>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
                <div class="line"></div>

                <table class="table table-bordered table-striped table-hover" id="tableHarga">
                    <thead>
                        <tr>
                            <th style="display: none;">#</th>
                            <th class="text-center">Satuan</th>
                            <th class="text-center">Harga Eceran</th>
                            <th class="text-center">Harga Grosir</th>
                            <th class="text-center" style="width: 15%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($item['hargas'] as $i => $harga)
                            <tr id="{{ $harga->id }}">
                                <td style="display: none;">{{ $i++ }}</td>
                                <td name="satuan_id" id="{{ $harga->satuan_id }}"> 1 {{ $harga->satuan->kode }}</td>
                                <td class="text-right">{{ \App\Util::ewon($harga->eceran) }}</td>
                                <td class="text-right">{{ \App\Util::ewon($harga->grosir) }}</td>
                                <td class="text-center">
                                    <button class="btn btn-xs btn-warning btnUbah" id="UbahHarga" title="Ubah Harga Jual"  data-toggle="tooltip" data-placement="top">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
@endsection

@section('script')
    @if (session('sukses') == 'cek')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran Cek Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'bg')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran BG Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tabel-item').DataTable();

        /*var items = [];
        var relasi_transaksi_pembelian = [];*/

        $(document).ready(function() {
            var url = "{{ url('transaksi-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('input[name="grosir_"]').prop('readonly', true);
            $('input[name="eceran_"]').prop('readonly', true);
            $('select[name="satuan_id"]').prop('disabled', true);

            items = '{{ json_encode($items) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            relasi_transaksi_pembelian = '{{ json_encode($relasi_transaksi_pembelian) }}';
            relasi_transaksi_pembelian = relasi_transaksi_pembelian.replace(/&quot;/g, '"');
            relasi_transaksi_pembelian = JSON.parse(relasi_transaksi_pembelian);

            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                var item = null;

                for (var i = 0; i < relasi_transaksi_pembelian.length; i++) {
                    if (relasi_transaksi_pembelian[i].item.kode == item_kode) {
                        item = relasi_transaksi_pembelian[i].item;
                        break;
                    }
                }

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
            });

            /*$('select[name="satuan_id"]').each(function(index, el) {
                var item_kode = $(el).parents('#formSimpanHargaContainer').attr('item_kode');
                var item = null;

                for (var i = 0; i < items.length; i++) {
                    if (items[i].kode == item_kode) {
                        item = items[i];
                        break;
                    }
                }

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    var option = ''+
                        '<option value="'+satuan.id+'">'+satuan.kode+
                        '</option>';
                    $(el).append($(option));
                }
            });*/
        });

        $(document).on('click', '#UbahHarga', function(event) {
            event.preventDefault();

            var $form = $(this).parents('#formSimpanHargaContainer').find('form');
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td[name="satuan_id"]').attr('id');
            var eceran = $tr.find('td').first().next().next().text().split('Rp');
            var grosir = $tr.find('td').first().next().next().next().text().split('Rp');
            // $('input[name="jumlah"]').val(jumlah);
            $form.find('#btnSimpanHarga').prop('disabled', false);
            $form.find('#satuan_harga').val(satuan_id);
            $form.find('input[name="eceran_"]').prop('readonly', false);
            $form.find('input[name="eceran_"]').val(eceran[1]);
            $form.find('input[name="eceran"]').val(parseInt(eceran[1].replace(/\D/g, ''), 10));

            $form.find('input[name="grosir_"]').prop('readonly', false);
            $form.find('input[name="grosir_"]').val(grosir[1]);
            $form.find('input[name="grosir"]').val(parseInt(grosir[1].replace(/\D/g, ''), 10));
            $form.find('#btnSimpanHarga span').text('Ubah');

            $form.attr('action', '{{ url("harga") }}' + '/' + id + '/update');
        });

    </script>
@endsection
