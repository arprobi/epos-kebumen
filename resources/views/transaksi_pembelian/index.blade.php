@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnTambah, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnTambah {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Transaksi Pembelian</h2>
                <a href="{{ url('transaksi-pembelian/create') }}" class="btn btn-sm btn-success pull-right" id="btnTambah" data-toggle="tooltip" data-placement="top" title="Tambah Transaksi Pembelian">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('transaksi-pembelian/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="created_at">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableItemMasuk" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="created_at">No</th>
                                <th class="sorting" field="created_at">Tanggal & Waktu</th>
                                <th class="sorting" field="kode_transaksi">Kode Pesanan</th>
                                <th class="sorting" field="nota">Kode Nota</th>
                                <th class="sorting" field="suplier_nama">Pemasok</th>
                                <th class="sorting" field="harga_total">Grand Total</th>
                                <th class="sorting" field="created_at" style="width: 200px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transaksi Pembelian berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Pembelian gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == '404')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Halaman tidak ditemukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('transaksi-pembelian/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var transaksi_pembelian = data[i];
                        var buttons = transaksi_pembelian.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Transaksi Pembelian">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }
                        if (buttons.penyesuaian != null) {
                            td_buttons += `
                                <a href="${buttons.penyesuaian.url}" class="btn btn-xs btn-Sesuai" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Penyesuaian Transaksi Pembelian">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            `;
                        }
                        if (buttons.bayar != null) {
                            td_buttons += `
                                <a href="${buttons.bayar.url}" class="btn btn-xs btn-primary" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Bayar Hutang Transaksi Pembelian">
                                    <i class="fa fa-money"></i>
                                </a>
                            `;
                        }
                        if (buttons.cek != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="CEK">
                                    CEK
                                </a>
                            `;
                        }
                        if (buttons.bg != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-BG" id="btnDetail" data-toggle="tooltip" data-placement="top" title="BG">
                                    BG
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${transaksi_pembelian.id}">
                                    <td>${nomor}</td>
                                    <td>${ymd2dmy(transaksi_pembelian.created_at.split(' ')[0])} ${transaksi_pembelian.created_at.split(' ')[1]}</td>
                                    <td>${transaksi_pembelian.kode_transaksi}</td>
                                    <td>${transaksi_pembelian.nota}</td>
                                    <td>${transaksi_pembelian.suplier_nama}</td>
                                    <td>${transaksi_pembelian.harga_total}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        /* $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama_item = $tr.find('td').first().next().text();
            var nama_suplier = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama_item + '\" dari \"' + nama_suplier + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }, function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#formHapusContainer').find('form').attr('action', '{{ url("transaksi-pembelian") }}' + '/' + id);
                    $('#formHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            });
        }); */

    </script>
@endsection
