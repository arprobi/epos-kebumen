@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-11">
                        <h2 id="formSimpanTitle">Ubah Transaksi Pembelian</h2>
                        {{-- <span id="kodeTransaksiTitle"></span> --}}
                    </div>
                    <div class="col-md-1">
                        <a href="{{ url('transaksi-pembelian/'.$transaksi_pembelian->id) }}" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali" class="btn btn-sm btn-default pull-right">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="form-simpan" action="{{ url('transaksi-pembelian/edit/'.$transaksi_pembelian->id) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <div class="form-group col-sm-12 col-xs-12">
                            <label class="control-label" style="">Kode Faktur</label>
                                <input type="text" id="nota" name="nota" class="form-control" style="height: 39px;" placeholder="Kode Faktur">
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            <label class="control-label">Pilih Pemasok</label>
                            <select id="suplier_form" name="suplier_id" class="select2_single form-control">
                                <option value="">Pilih Pemasok</option>
                                @foreach ($supliers as $suplier)
                                <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            <label class="control-label">Pilih Penjual</label>
                            <select id="seller_form" name="seller_id" class="select2_single form-control" id="seller">
                                <option value="">Pilih Penjual</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            <div class="form-group pull-left">
                                <button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
                                    <i class="fa fa-save"></i> Ubah
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var custom_data_last_item = false;
        var seller_id = '';
        var suplier_id = '';
        var seller_stat  = true;

        function isSubmitButtonDisabled() {
            var suplier = $('#suplier_form').val();
            var nota = $('input[name="nota"]').val();
            console.log(suplier);
            if (suplier == '') return true ;
            if (nota == '') return true;

            return false;
        }

        function cek() {
            $('#submit').prop('disabled', isSubmitButtonDisabled());
        }

        function cariItem(id, custom_data, index) {
            var on_ready_item_index = index;
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

            if (id == '') {
                $('#tabelInfo').find('tbody').empty();
            } else if (!selected_items.includes(parseInt(id))) {
                if (!isNaN(id)) {
                    selected_items.push(parseInt(id));
                }

                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(url, data);
                    var pcs = data.pcs;
                    var item = data.item;
                    var kode = item.kode;
                    var nama = item.nama;
                    var stok = item.stoktotal;
                    var satuans = item.satuan_pembelians;
                    // var harga_lama = $('input[name="inputHargaTotal"]').val();
                    // var tunai_lama = $('input[name="inputJumlahBayar"]').val();
                    // var kembali_lama = $('input[name="inputUtang"]').val();

                    var on_ready_item_subtotal = 0;

                    var jumlah_kadaluarsa = 0;
                    for (var i = 0; i < stoks.length; i++) {
                        var _stok = stoks[i];
                        if (_stok.item.id == id) {
                            jumlah_kadaluarsa++;
                        }
                    }

                    var text_stoks = '';
                    var temp = stok;
                    if (satuans.length > 0) {
                        for (var i = 0; i < satuans.length; i++) {
                            var satuan = satuans[i];
                            var hasil = parseInt(temp / satuan.konversi);
                            if (hasil > 0) {
                                temp %= satuan.konversi;
                                text_stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                            }
                        }
                    } else {
                        text_stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                    }

                    var on_ready_item_jumlah_view = [];
                    if (custom_data) {
                        var on_ready_item_jumlah = parseFloat(relasi_transaksi_pembelian[on_ready_item_index].jumlah);
                        on_ready_item_subtotal = parseFloat(relasi_transaksi_pembelian[on_ready_item_index].subtotal);

                        if (isNaN(on_ready_item_jumlah)) on_ready_item_jumlah = 0;
                        if (isNaN(on_ready_item_subtotal)) on_ready_item_subtotal = 0;
                        for (var i = 0; i < transaksi_pembelian.items[on_ready_item_index].satuan_pembelians.length; i++) {
                            var satuan = transaksi_pembelian.items[on_ready_item_index].satuan_pembelians[i];
                            if (on_ready_item_jumlah >= satuan.konversi) {
                                on_ready_item_jumlah_view.push({
                                    jumlah: parseInt(on_ready_item_jumlah / satuan.konversi),
                                    konversi: satuan.konversi
                                });
                                on_ready_item_jumlah = on_ready_item_jumlah % satuan.konversi;
                            }
                        }
                    }

                    $('#tabelInfo').find('tbody').empty();
                    $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+text_stoks+'</td></tr>');

                    // if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;
                    // if (tunai_lama === '' || tunai_lama === undefined) tunai_lama = 0;
                    // if (kembali_lama === '' || kembali_lama === undefined) kembali_lama = 0;

                    if (tr === undefined) {
                        $('#tabelKeranjang').find('tr[id="totalharga"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalbayar"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalkembali"]').remove();

                        var divs = '<div class="form-group">'+
                                        '<label class="control-label">Jumlah</label>';

                        if (satuans.length > 0) {
                            for (var i = 0; i < satuans.length; i++) {
                                var satuan = satuans[i];
                                var temp_jumlah = 0;
                                if (custom_data) {
                                    for (var j = 0; j < on_ready_item_jumlah_view.length; j++) {
                                        if (on_ready_item_jumlah_view[j].konversi == satuan.konversi) {
                                            temp_jumlah = on_ready_item_jumlah_view[j].jumlah;
                                        }
                                    }
                                }
                                divs += '<div class="input-group text-center">' +
                                            '<input type="text" id="inputJumlah" name="inputJumlah" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'" value="'+(temp_jumlah>0?temp_jumlah:'')+'">' +
                                            '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+satuan.satuan.kode+'</div>' +
                                        '</div>';
                            }
                            divs += '</div>';
                        } else {
                            divs += '<div class="input-group">' +
                                        '<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
                                        '<div class="input-group-addon kode_satuan" style="width: 60px; text-align: right;">'+pcs.kode+'</div>' +
                                    '</div>' +
                                '</div>';
                        }

                        var tr = '<tr data-id="' + id + '">'+
                                    '<input type="hidden" name="jumlah">'+
                                    '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                    '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3>'+
                                        `<div class="form-group" id="anak-kadaluarsa-` + id + `" jkal="0">
                                                <label class="control-label">Kadaluarsa</label>
                                                <i class="fa fa-plus" id="tambah_kadaluarsa" style="color: green; cursor: pointer; margin-left: 10px;"></i>
                                            </div>`+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        divs+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Total</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka" required="" value="'+(on_ready_item_subtotal>0?on_ready_item_subtotal:'')+'">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Harga</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">HPP</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">PPN (Rp)</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">'+
                                                    '<input name="checkPPN" id="checkPPN" type="checkbox" class="invisible">'+
                                                '</div>'+
                                                '<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="" readonly="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                '</tr>';

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-0" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-0" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-0" value="" class="hpp-stok-' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-0" value="" class="jumlah-stok-'+id+'"/>');

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="ppn-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="ppn-' + id + '" value="" />');

                        $('#tabelKeranjang').find('tbody').append($(tr));
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#inputJumlah').trigger('keyup');
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#inputSubTotal').trigger('keyup');

                        if (custom_data) {
                            for (var i = 0; i < jumlah_kadaluarsa; i++) {
                                $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                            }
                        } else {
                            $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                        }
                    }

                    $('#spinner').hide();
                    updateHargaOnKeyup();
                    // $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                });
            }
        }

        $(document).ready(function() {
            var url = "{{ url('transaksi-pembelian') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            transaksi_pembelian = '{{ $transaksi_pembelian }}';
            transaksi_pembelian = transaksi_pembelian.replace(/&quot;/g, '"');
            transaksi_pembelian = JSON.parse(transaksi_pembelian);

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            seller_id = transaksi_pembelian.seller_id;
            suplier_id = transaksi_pembelian.suplier_id;
            $('select[name="suplier_id"]').val(transaksi_pembelian.suplier_id).trigger('change');
            $('input[name="nota"]').val(transaksi_pembelian.nota);
            // $('select[name="seller_id"]').val(transaksi_pembelian.seller_id).trigger('change');
        });

        $(document).on('change', 'select[name="suplier_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if(id != ''){
                var url_items = "{{ url('transaksi-pembelian') }}" + '/' + id + '/items.json';
                var url_sellers = "{{ url('transaksi-pembelian') }}" + '/' + id + '/sellers.json';
                var url_suplier = "{{ url('transaksi-pembelian') }}" + '/' + id + '/suplier_id.json';
                
                $('input[name="suplier_id"]').val(id).change();
                $('#tabelKeranjang > tbody').empty();
                $('#append-section').empty();

                var $select_sellers = $('select[name="seller_id"]');
                $select_sellers.empty();
                $select_sellers.append($('<option value="">Pilih Penjual</option>'));

                $('#spinner').show();
                selected_items = [];
                $.get(url_sellers, function(data) {
                    var sellers = data.sellers;
                    for (var i = 0; i < sellers.length; i++) {
                        var seller = sellers[i];
                        $select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
                    }
                    if(seller_id > 0 && suplier_id == id) {
                        $('select[name="seller_id"]').val(seller_id).trigger('change');
                    }
                    $('#spinner').hide();
                    cek();
                });
            } else {
                var $select_sellers = $('select[name="seller_id"]');
                $select_sellers.empty();
                $select_sellers.append($('<option value="">Pilih Penjual</option>'));
            }

            cek();
        });

        $(document).on('change', 'select[name="seller_id"]', function(event) {
            event.preventDefault();

            cek();
        });

        $(document).on('keyup', 'input[name="nota"]', function(event) {
            event.preventDefault();

            cek();
        });

    </script>
@endsection
