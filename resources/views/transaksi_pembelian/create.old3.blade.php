@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Transaksi Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-6">
                        <h2 id="formSimpanTitle">Transaksi Pembelian</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    <div class="col-md-6 pull-right">
                        <a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Suplier</label>
                        <select name="suplier_id" class="select2_single form-control">
                            <option value="">Pilih Suplier</option>
                            @foreach ($supliers as $suplier)
                            <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6 col-xs-6">
                        <label class="control-label">Pilih Sales</label>
                        <select name="seller_id" class="select2_single form-control" id="seller">
                            <option value="">Pilih Sales</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6 col-xs-6">
                        <label class="control-label" style="">Nama Sales</label>
                        <input type="text" name="nama_sales" class="form-control" style="height: 39px;">
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('transaksi-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th style="text-align: left;">Item</th>
                            <th style="text-align: left;">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Pembelian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <div class="row">
                            <table class="table" id="tabelKeranjang">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-7 col-xs-7">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div class="input-group">
                                    <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnPiutang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Piutang</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_id">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalTransfrer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNoTraKartu" id="inputNoTraKartu" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Dibayarkan</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Cek</label>
                                        <select class="form-control select2_single" name="cek_id">
                                            <option value="">Pilih Cek</option>
                                            <option value="cek_baru">Buat Cek Baru</option>
                                            @foreach ($ceks as $cek)
                                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [{{ \App\Util::ewon($cek->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row sembunyi" id="cek_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_cek">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih BG</label>
                                        <select class="form-control select2_single" name="bg_id">
                                            <option value="">Pilih BG</option>
                                            <option value="bg_baru">Buat BG Baru</option>
                                            @foreach ($bgs as $bg)
                                            <option value="{{ $bg->id }}">{{ $bg->nomor }} [{{ \App\Util::ewon($bg->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row sembunyi" id="bg_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_bg">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputPiutangContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Piutang</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" id="inputNominalPiutang" class="form-control angka">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-xs-5">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Sub Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Potongan Pembelian</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputPotonganPembelian" id="inputPotonganPembelian" class="form-control angka">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Grand Total + Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Jumlah Bayar</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control angka" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Kurang</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputUtang" id="inputUtang" class="form-control angka" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('transaksi-pembelian') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="suplier_id" />
                                        <input type="hidden" name="seller_id" />
                                        <input type="hidden" name="seller_name" />
                                        <input type="hidden" name="beli_po" value="0"/>
                                        <input type="hidden" name="po" value="0" />

                                        <input type="hidden" name="harga_total" />
                                        <input type="hidden" name="jumlah_bayar" />
                                        <input type="hidden" name="utang" />

                                        <input type="hidden" name="potongan_pembelian" />
                                        <input type="hidden" name="ongkos_kirim" />
                                        
                                        <input type="hidden" name="nominal_tunai" />
                                        
                                        <input type="hidden" name="bank_id" />
                                        <input type="hidden" name="no_tansfer" />
                                        <input type="hidden" name="nominal_transfer" />
                                        
                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="bank_cek" />
                                        <input type="hidden" name="nominal_cek" />
                                        
                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="bank_bg" />
                                        <input type="hidden" name="nominal_bg" />
                                        
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="NoTraKartu" />

                                        <input type="hidden" name="nominal_piutang" />
                                        
                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                            <div class="form-group pull-left">
                                                <button type="submit" class="btn btn-success" id="submit" style="width: 100px;" disabled="">
                                                    <i class="fa fa-check"></i> OK
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var selected_items = [];
        var piutang = 0;
        var nominal_piutang = 0;

        function isSubmitButtonDisabled() {
            var jumlah = 0;
            $('input[name="jumlah[]"]').each(function(index, el) {
                var val = parseInt($(el).val());
                jumlah += val;
            });
            // console.log('jumlah');
            var selisih = piutang - nominal_piutang;
            if (isNaN(jumlah) || jumlah <= 0) return true;
            if (isNaN(selisih) || selisih <0) return true;
            
            var subtotal = 0;
            $('input[name="subtotal[]"]').each(function(index, el) {
                var val = parseFloat($(el).val());
                subtotal += val;
            });
            // console.log('subtotal');
            if (isNaN(subtotal) || subtotal <= 0) return true;

            // console.log('harga_total');
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            if (isNaN(harga_total) || harga_total <= 0) return true;

            var ongkos_kirim = parseFloat($('input[name="ongkos_kirim"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
            var bank_id = $('input[name="bank_id"]').val();
            var no_transfer = $('input[name="no_transfer"]').val();
            var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
            var no_cek = $('input[name="no_cek"]').val();
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
            var no_bg = $('input[name="no_bg"]').val();
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
            var no_kredit = $('input[name="no_kredit"]').val();
            var nominal_kredit = parseFloat($('input[name="nominal_kredit"]').val());

            // // console.log('btnTunai');
            // if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

            // // console.log('btnTransfer');
            // if ($('#btnTransfer').hasClass('btn-warning') && (bank_id == '' || no_transfer == '' || isNaN(nominal_transfer) || nominal_transfer <= 0)) return true;

            // // console.log('btnCek');
            // if ($('#btnCek').hasClass('btn-success') && (no_cek == '' || isNaN(nominal_cek) || nominal_cek <= 0)) return true;

            // // console.log('btnBG');
            // if ($('#btnBG').hasClass('btn-primary') && (no_bg == '' || isNaN(nominal_bg) || nominal_bg <= 0)) return true;

            // // console.log('btnKredit');
            // if ($('#btnKredit').hasClass('btn-info') && (no_kredit == '' || isNaN(nominal_kredit) || nominal_kredit <= 0)) return true;

            // // console.log('jumlah_bayar');
            // if (isNaN(jumlah_bayar) || jumlah_bayar <= 0) return true;
            if (jumlah_bayar > harga_total + ongkos_kirim) return true;

            // console.log('end');
            return false;
        }

        function updateHargaOnKeyup() {
            var $harga_total = $('#inputHargaTotal');
            var $potongan_pembelian = $('#inputPotonganPembelian');
            var $ongkos_kirim = $('#inputOngkosKirim');
            var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $utang = $('#inputUtang');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('#formSimpanContainer').find('input[name="nominal_transfer"]').val();
            var nominal_kartu = $('#formSimpanContainer').find('input[name="nominal_kartu"]').val();
            var nominal_piutang = $('#formSimpanContainer').find('input[name="nominal_piutang"]').val();
            var potongan_pembelian = $('#formSimpanContainer').find('input[name="potongan_pembelian"]').val();
            var ongkos_kirim = $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val();
            var bank_cek = $('select[name="bank_cek"]').val();
            var bank_bg = $('select[name="bank_bg"]').val();

            if(bank_cek != undefined && bank_cek == ''){
                var nominal_cek = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
            }else{
                nominal_cek = '0';
            }

            if(bank_bg != undefined && bank_bg == ''){
                var nominal_bg = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();
            }else{
                nominal_bg = '0';
            }

            nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_transfer = parseFloat(nominal_transfer.replace(/\D/g, ''), 10);
            nominal_cek = parseFloat(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg = parseFloat(nominal_bg.replace(/\D/g, ''), 10);
            nominal_kartu = parseFloat(nominal_kartu.replace(/\D/g, ''), 10);
            nominal_piutang = parseFloat(nominal_piutang.replace(/\D/g, ''), 10);
            potongan_pembelian = parseFloat(potongan_pembelian.replace(/\D/g, ''), 10);
            ongkos_kirim = parseFloat(ongkos_kirim.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });
            
            var harga_total_plus_ongkos_kirim = harga_total;
            if (!isNaN(parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10)))
                harga_total_plus_ongkos_kirim += parseInt($ongkos_kirim.val().replace(/\D/g, ''), 10);
            if (!isNaN(parseInt($potongan_pembelian.val().replace(/\D/g, ''), 10)))
                harga_total_plus_ongkos_kirim -= parseInt($potongan_pembelian.val().replace(/\D/g, ''), 10);
            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kartu + nominal_piutang;
            var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;

            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(utang)) utang = 0;
            if (utang < 0) utang = 0;

            if (ongkos_kirim == 0) $ongkos_kirim.val('');
            else $ongkos_kirim.val(ongkos_kirim);
            if (potongan_pembelian == 0) $potongan_pembelian.val('');
            else $potongan_pembelian.val(potongan_pembelian);
            $harga_total_plus_ongkos_kirim.val(harga_total_plus_ongkos_kirim.toLocaleString(['ban', 'id']));
            $harga_total.val(harga_total.toLocaleString(['ban', 'id']));
            $jumlah_bayar.val(jumlah_bayar.toLocaleString(['ban', 'id']));
            $utang.val(utang.toLocaleString(['ban', 'id']));

            $('input[name="potongan_pembelian"]').val(potongan_pembelian);
            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="utang"]').val(utang);

            // console.log('Till here', isSubmitButtonDisabled());
            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
        }

        $(document).ready(function() {
            // var url = "{{ url('transaksi-pembelian/create') }}";
            // var url = "{{ url('transaksi-pembelian') }}";
            // var a = $('a[href="' + url + '"]');
            // a.parent().addClass('current-page');
            // a.parent().parent().show();
            // a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#inputTunaiContainer').hide();
            $('#inputPiutangContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputNominalTunai').val('');
            $('#inputNominalKartu').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputNoTraKartu').val('');
            $('#inputNominalPiutang').val('');
            $('input[name="nama_sales"]').prop('disabled', true);
            
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            });             
        });

        // Buat ambil nomer transaksi terakhir
        /*$(window).on('load', function(event) {
            var url = "{{ url('transaksi-pembelian/last.json') }}";

            $.get(url, function(data) {
                var kode = 1;
                if (data.transaksi_pembelian !== null) {
                    var kode_transaksi = data.transaksi_pembelian.kode_transaksi;
                    kode = parseInt(kode_transaksi.split('/')[0]);
                    kode++;
                }

                kode = int4digit(kode);
                var tanggal = printTanggalSekarang('dd/mm/yyyy');
                kode_transaksi = kode + '/TRAB/' + tanggal;

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });*/

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('select[name="bank_id"]').val('').change();
                    $('input[name="no_tansfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="NoTraKartu"]').val('');
                    $('select[name="jenis_kartu"]').val('').change();
                    $('select[name="bank_kartu"]').val('').change();
                    
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                    $('input[name="bank_cek"]').val('').change();
                    $('select[name="cek_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                    $('select[name="bank_bg"]').val('').change();
                    $('select[name="bg_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnPiutang', function(event) {
            event.preventDefault();

            var suplier = $('select[name="suplier_id"]').val();
            if (suplier != '') {
                // console.log(suplier);
                var url = "{{ url('transaksi-pembelian') }}" + '/' + suplier + '/suplier.json';
                $.get(url, function(data) {
                    piutang += parseInt(data.suplier.piutang);
                    // console.log(piutang);
                    if ($('#btnPiutang').hasClass('btn-default')) {
                        $('#btnPiutang').removeClass('btn-default');
                        $('#btnPiutang').addClass('btn-dark');
                        $('#btnPiutang').find('i').show('fast');
                        $('#inputPiutangContainer').show('fast', function() {
                            $(this).find('input').first().trigger('focus');
                            // $('#btnPiutang').find('input').first().trigger('focus');
                        });
                    } else if ($('#btnPiutang').hasClass('btn-dark')) {
                        $('#btnPiutang').removeClass('btn-dark');
                        $('#btnPiutang').addClass('btn-default');
                        $('#btnPiutang').find('i').hide('fast');
                        $('#inputPiutangContainer').hide('fast', function() {
                            $('input[name="nominal_piutang"]').val('');
                            $('#btnPiutang').find('input').val('');
                            $('#inputNominalPiutang').val('');
                            updateHargaOnKeyup();
                        });
                    }
                });
            }
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var bank_id = $(this).val();
            // console.log(bank_id);
            $('#formSimpanContainer').find('input[name="bank_id"]').val(bank_id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_tansfer = $(this).val();
            $('#formSimpanContainer').find('input[name="no_tansfer"]').val(no_tansfer);
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="cek_id"]', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
            updateHargaOnKeyup();

            var utang = $('input[name="inputUtang"]').val();
            utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10));

            var cek_id = $(this).val();
            if(cek_id != 'cek_baru'){
                var cek_id = parseInt($(this).val());
            }

            if (Number.isInteger(cek_id)) {
                var cek_text = $(this).select2('data')[0].text;
                var nominal_text = cek_text.split('Rp')[1].split(']')[0];
                var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
                nominal = parseFloat(nominal);
                var nomor = cek_text.split(' [')[0];
                $('#cek_baru').addClass('sembunyi');
                $('#inputNominalCek').prop('disabled', true);
                $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
                $('select[name="bank_cek"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputCekContainer').find('p').hide();
                    $('#inputNominalCek').val(nominal_text);
                    $('#formSimpanContainer').find('input[name="no_cek"]').val(nomor);
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
                    // Button
                } else {
                    // Error
                    $('#inputCekContainer').find('p').show();
                    $('#inputNominalCek').val('');
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    // Button
                }
            }else if(cek_id == 'cek_baru'){
                $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                $('#cek_baru').removeClass('sembunyi');
                $('#inputNominalCek').prop('disabled', false);
                $('#inputNominalCek').val('');
            } else {
                $('#cek_baru').addClass('sembunyi');
                $('#inputNominalCek').prop('disabled', true);
                $('select[name="bank_cek"]').val('').change();
                $('#inputNominalCek').val('');
                $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
                // Button
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal);
            // updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('#formSimpanContainer').find('input[name="no_cek"]').val(no_cek);
        });

        $(document).on('change', 'select[name="bank_cek"]', function(event) {
            event.preventDefault();

            var bank_cek = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_cek"]').val(bank_cek);
        });

        $(document).on('change', 'select[name="bg_id"]', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
            updateHargaOnKeyup();

            var utang = $('input[name="inputUtang"]').val();
            utang = parseFloat(parseInt(utang.replace(/\D/g, ''), 10));

            var bg_id = $(this).val();
            if(bg_id != 'bg_baru'){
                var bg_id = parseInt($(this).val());
            }

            if (Number.isInteger(bg_id) && bg_id != '') {
                console.log(bg_id);
                var bg_text = $(this).select2('data')[0].text;
                var nominal_text = bg_text.split('Rp')[1].split(']')[0];
                var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
                nominal = parseFloat(nominal);
                var nomor = bg_text.split(' [')[0];
                $('#inputNominalBG').prop('disabled', true);
                $('#bg_baru').addClass('sembunyi');
                $('select[name="bank_bg"]').val('').change();
                $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
                if (nominal <= utang) {
                    // Success
                    $('#inputBGContainer').find('p').hide();
                    $('#inputNominalBG').val(nominal_text);
                    $('#formSimpanContainer').find('input[name="no_bg"]').val(nomor);
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
                } else {
                    $('#inputBGContainer').find('p').show();
                    $('#inputNominalBG').val('');
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                }
            }else if(bg_id == 'bg_baru'){
                $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                $('#bg_baru').removeClass('sembunyi');
                $('#inputNominalBG').prop('disabled', false);
                $('#inputNominalBG').val('');
            } else {
                $('#bg_baru').addClass('sembunyi');
                $('#inputNominalBG').prop('disabled', true);
                $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
                $('#inputBGContainer').find('p').hide();
                $('select[name="bank_bg"]').val('').change();
                $('#inputNominalBG').val('');
                $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                // Button
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal);
            // updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('#formSimpanContainer').find('input[name="no_bg"]').val(no_bg);
        });

        $(document).on('change', 'select[name="bank_bg"]', function(event) {
            event.preventDefault();

            var bank_bg = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_bg"]').val(bank_bg);
        });

        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="bank_kartu"]').val(bank_kartu);
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('#formSimpanContainer').find('input[name="jenis_kartu"]').val(jenis_kartu);
        });

        $(document).on('keyup', '#inputNoTraKartu', function(event) {
            event.preventDefault();

            var NoTraKartu = $(this).val();
            $('#formSimpanContainer').find('input[name="NoTraKartu"]').val(NoTraKartu);
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalPiutang', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal_piutang = 0;
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            nominal_piutang += nominal;
            if(nominal > piutang){
                $('#inputPiutangContainer').addClass('has-error');
                $('#submit').prop('disabled', isSubmitButtonDisabled());
            }else{
                $('#inputPiutangContainer').removeClass('has-error');
                $('#submit').prop('disabled', isSubmitButtonDisabled());
            }
            $('#formSimpanContainer').find('input[name="nominal_piutang"]').val(nominal);
            updateHargaOnKeyup();
        });

        /*$(document).on('change', 'select[name="kredit_id"]', function(event) {
            event.preventDefault();

            var no_kredit = $(this).val();
            // console.log(no_kredit);
            $('#formSimpanContainer').find('input[name="no_kredit"]').val(no_kredit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKredit', function(event) {
            event.preventDefault();

            var nominal_kredit = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kredit)) nominal_kredit = 0;
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(nominal_kredit);
            updateHargaOnKeyup();
        });*/

        $(document).on('keyup', '#inputPotonganPembelian', function(event) {
            event.preventDefault();

            var potongan_pembelian = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(potongan_pembelian)) potongan_pembelian = 0;
            $('#formSimpanContainer').find('input[name="potongan_pembelian"]').val(potongan_pembelian);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputOngkosKirim', function(event) {
            event.preventDefault();

            var ongkos_kirim = parseInt($(this).val().replace(/\D/g, ''), 10);
            // console.log(ongkos_kirim);
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;
            $('#formSimpanContainer').find('input[name="ongkos_kirim"]').val(ongkos_kirim);
            updateHargaOnKeyup();
        });

        /*$(document).on('keyup', '#inputKodeItem', function(event) {
            event.preventDefault();

            var kode_item = $(this).val();
            console.log(kode_item);
        });*/

        $(document).on('change', 'select[name="suplier_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if(id != ''){
                var url_items = "{{ url('transaksi-pembelian') }}" + '/' + id + '/items.json';
                var url_sellers = "{{ url('transaksi-pembelian') }}" + '/' + id + '/sellers.json';
                $('input[name="suplier_id"]').val(id);
                $('#tabelKeranjang > tbody').empty();
                $('#append-section').empty();
                $('#formSimpanContainer').find('input[name="harga_total"]').val('');
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
                $('#formSimpanContainer').find('input[name="utang"]').val('');

                var $select_item = $('select[name="item_id"]');
                $select_item.empty();
                $select_item.append($('<option value="">Pilih Item</option>'));

                $.get(url_items, function(data) {
                    var items = data.items;
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        $select_item.append($('<option value="' + item.id + '">' + item.kode + ' [' + item.nama + ']</option>'));
                    }
                });

                var $select_sellers = $('select[name="seller_id"]');
                $select_sellers.empty();
                $select_sellers.append($('<option value="">Pilih Sales</option>'));

                $('#spinner').show();
                selected_items = [];
                $.get(url_sellers, function(data) {
                    var sellers = data.sellers;
                    for (var i = 0; i < sellers.length; i++) {
                        var seller = sellers[i];
                        $select_sellers.append($('<option value="' + seller.id + '">' + seller.nama + '</option>'));
                    }
                    $select_sellers.append($('<option value="lain">Sales Lain</option>'));
                    $('#spinner').hide();
                });
                $('#btnPiutang').removeClass('btn-dark');
                $('#btnPiutang').addClass('btn-default');
                $('#btnPiutang').find('i').hide('fast');
                $('#inputPiutangContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_piutang"]').val('');
                    $('#btnPiutang').find('input').val('');
                    updateHargaOnKeyup();
                });
            }else{
                $('#btnPiutang').removeClass('btn-dark');
                $('#btnPiutang').addClass('btn-default');
                $('#btnPiutang').find('i').hide('fast');
                $('#inputPiutangContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_piutang"]').val('');
                    $('#btnPiutang').find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var id = $('select[name="item_id"]').val();
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="' + id + '"]').data('id');
            var url = "{{ url('transaksi-pembelian') }}" + '/items/' + id + '/json';

            if (id == '') {
                $('#tabelInfo').find('tbody').empty();
            } else if (!selected_items.includes(parseInt(id))) {
                if (!isNaN(id)) {
                    selected_items.push(parseInt(id));
                }

                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    var pcs = data.pcs;
                    var item = data.item;
                    var kode = item.kode;
                    var nama = item.nama;
                    var stok = item.stoktotal;
                    var satuans = item.satuan_pembelians;
                    var harga_lama = $('input[name="inputHargaTotal"]').val();
                    var tunai_lama = $('input[name="inputJumlahBayar"]').val();
                    var kembali_lama = $('input[name="inputUtang"]').val();

                    var stoks = '';
                    var temp = stok;
                    if (satuans.length > 0) {
                        for (var i = 0; i < satuans.length; i++) {
                            var satuan = satuans[i];
                            var hasil = parseInt(temp / satuan.konversi);
                            if (hasil > 0) {
                                temp %= satuan.konversi;
                                stoks += '<span class="label label-info">'+hasil+' '+satuan.satuan.kode+'</span>';
                            }
                        }
                    } else {
                        stoks += '<span class="label label-info">'+stok+' '+pcs.kode+'</span>';
                    }

                    $('#tabelInfo').find('tbody').empty();
                    $('#tabelInfo').find('tbody').append('<tr><td>'+nama+'</td><td>'+stoks+'</td></tr>');

                    if (harga_lama === '' || harga_lama === undefined) harga_lama = 0;
                    if (tunai_lama === '' || tunai_lama === undefined) tunai_lama = 0;
                    if (kembali_lama === '' || kembali_lama === undefined) kembali_lama = 0;

                    if (tr === undefined) {
                        $('#tabelKeranjang').find('tr[id="totalharga"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalbayar"]').remove();
                        $('#tabelKeranjang').find('tr[id="totalkembali"]').remove();

                        var divs = '<div class="form-group">'+
                                        '<label class="control-label">Jumlah</label>';

                        if (satuans.length > 0) {
                            for (var i = 0; i < satuans.length; i++) {
                                var satuan = satuans[i];
                                divs += '<div class="input-group text-center">' +
                                            '<input type="text" id="inputJumlah" name="inputJumlah" class="pull-right form-control input-sm angka" konversi="'+satuan.konversi+'"">' +
                                            '<div class="input-group-addon" style="width: 60px; text-align: right;"><i class="kode_satuan">'+satuan.satuan.kode+'</i></div>' +
                                        '</div>';
                            }
                            divs += '</div>';
                        } else {
                            divs += '<div class="input-group">' +
                                        '<input type="text" id="inputJumlah" name="inputJumlah" class="form-control input-sm angka" konversi="'+1+'">' +
                                        '<div class="input-group-addon" style="width: 60px; text-align: right;"><i class="kode_satuan">'+pcs.kode+'</i></div>' +
                                    '</div>' +
                                '</div>';
                        }

                        var tr = '<tr data-id="' + id + '">'+
                                    '<input type="hidden" name="jumlah">'+
                                    '<td style="width: 50px;"><h3><i class="fa fa-times" id="remove" style="color: #c9302c; cursor: pointer; margin-left: 10px;"></i></h3></td>'+
                                    '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3>'+
                                        `<div class="form-group" id="anak-kadaluarsa-` + id + `" jkal="0">
                                                <label class="control-label">Kadaluarsa</label>
                                                <i class="fa fa-plus" id="tambah_kadaluarsa" style="color: green; cursor: pointer; margin-left: 10px;"></i>
                                            </div>`+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        divs+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Total</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Harga</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHarga" id="inputHarga" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                    '<td style="width: 200px;">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">HPP</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputHPP" id="inputHPP" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">PPN</label>'+
                                            '<div class="input-group">'+
                                                '<div class="input-group-addon">Rp</div>' +
                                                '<input type="text" name="inputPPN" id="inputPPN" class="form-control input-sm angka" disabled="">'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                '</tr>';
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-0" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-0" value="' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-0" value="" class="hpp-stok-' + id + '" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-0" value="" class="jumlah-stok-'+id+'"/>');

                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + id + '" value="" />');
                        // $('#form-simpan').find('#append-section').append('<input type="hidden" name="ppn[]" id="ppn-' + id + '" value="" />');
                        $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-' + id + '" value="" />');

                        $('#tabelKeranjang').find('tbody').append($(tr));
                        $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                    }
                    $('#spinner').hide();
                    // $('#tabelKeranjang tr[data-id="'+id+'"]').find('#tambah_kadaluarsa').trigger('click');
                });
            }
        });

        $(document).on('focus', '.inputKadaluarsa', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            $(this).daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY'
            }, function(start) {
                var kadaluarsa = $('#tabelKeranjang').find('tr[data-id="' + id + '"]')
                    .find('input[name="inputKadaluarsa"]').val();
                
                $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-0"]').val(kadaluarsa);
            });
        });

        $(document).on('focus', '.inputKadaluarsaStok', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            var kol_kadal = $(this).parents('.kol-kadal');
            // console.log(jkal);
            $(this).daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY'
            }, function(start) {
                var kadaluarsa = kol_kadal.find('input[name="inputKadaluarsa"]').val();
                $('#form-simpan').find('input[id="kadaluarsa-stok-' + id + '-'+jkal+'"]').val(kadaluarsa);
            });
        });

        $(document).on('change', 'select[name="seller_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var nama = $("#seller option:selected").text();
            if (id == 'lain') {
                $('input[name="nama_sales"]').prop('disabled', false);
                $('input[name="seller_id"]').val('');
            } else {
                $('input[name="seller_id"]').val(id);
                $('input[name="nama_sales"]').val(nama);
            }
        });

        $(document).on('change', 'input[type="checkbox"]', function(event) {
            event.preventDefault();

            $(this).parent().next().val('');
            var checked = $(this).prop('checked');
            if (checked) {
                $(this).parent().next().prop('disabled', false);
            } else {
                $(this).parent().next().prop('disabled', true);
                id = $(this).parents('tr').first().data('id');
                $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
                $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-' + id + '" value="" />');    
            }
        });

        $(document).on('keyup', 'input[name="inputJumlah"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $subtotal = $tr.find('input[name="inputSubTotal"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($subtotal.val().replace(/\D/g, ''), 10);
            var jumlah = 0;
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            }); 
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;
            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            // var hpp = parseInt(harga/1.1);
            // if (isNaN(hpp)) hpp = 0;
            // var ppn = harga-hpp;
            // if (isNaN(ppn)) ppn = 0;
            var ppn = parseFloat((harga / 11).toFixed(2));          
            if (isNaN(ppn)) ppn = 0;
            var hpp = harga - ppn;
            if (isNaN(hpp)) hpp = 0;

            $jumlah.val(jumlah);
            $harga.val(harga.toLocaleString(['ban', 'id']));
            $hpp.val(hpp.toLocaleString(['ban', 'id']));
            $ppn.val(ppn.toLocaleString(['ban', 'id']));
            
            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);
            
            var skal = $('#kadaluarsa-'+id).attr('skal');
            if(skal == 0){
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            }else{
                var total_hasil = 0;
                $('.jumlah-stok-'+id).each(function(index, el) {
                    var jumlah_stok = parseInt($(el).val());
                    if (isNaN(jumlah_stok)) jumlah_stok = 0;
                    total_hasil += jumlah_stok;
                })

                if(parseInt(total_hasil) != parseInt(jumlah)){
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).addClass('has-error');
                    });
                }else{
                    $tr.find('.kol-kadal').each(function(index, el) {
                        $(el).removeClass('has-error');
                    });
                }   
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });
            
            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(['ban', 'id']));
            updateHargaOnKeyup();
        });

        $(document).on('keyup', 'input[name="nama_sales"]', function(event) {
            event.preventDefault();

            var nama = $(this).val();
            $('input[name="seller_name"]').val(nama);
        });

        $(document).on('keyup', '#inputSubTotal', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $kadaluarsa = $tr.find('.inputKadaluarsa');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var $harga = $tr.find('input[name="inputHarga"]');
            var $hpp = $tr.find('input[name="inputHPP"]');
            var $ppn = $tr.find('input[name="inputPPN"]');

            var subtotal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
            var kadaluarsa = $kadaluarsa.val();

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(jumlah)) jumlah = 0;
            var harga = subtotal / jumlah;
            if (isNaN(harga) || harga === Infinity) harga = 0;
            // var hpp = parseInt(harga/1.1);
            // if (isNaN(hpp)) hpp = 0;
            // var ppn = harga-hpp;
            // if (isNaN(ppn)) ppn = 0;
            // console.log(ppn, hpp);
            var ppn = parseFloat((harga / 11).toFixed(2));
            if (isNaN(ppn)) ppn = 0;
            var hpp = harga - ppn;
            if (isNaN(hpp)) hpp = 0;

            $harga.val(harga.toLocaleString(['ban', 'id']));
            $hpp.val(hpp.toLocaleString(['ban', 'id']));
            $ppn.val(ppn.toLocaleString(['ban', 'id']));
            
            // $('#form-simpan').find('input[id="item-' + id + '"]').val(id);
            // $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').val(jumlah);
            $('#form-simpan').find('input[id="harga-' + id + '"]').val(hpp);
            // $('#form-simpan').find('input[id="ppn-' + id + '"]').val(ppn);
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').val(subtotal);

            var skal = $('#kadaluarsa-'+id).attr('skal');
            if(skal == 0){
                $('#form-simpan').find('input[id="hpp-stok-' + id + '-0"]').val(hpp);
                $('#form-simpan').find('input[id="jumlah-stok-' + id + '-0"]').val(jumlah);
            }

            var totalharga = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                totalharga += subtotal;
            });
            
            var hpp_stok = $('#form-simpan').find('input[id="harga-'+id+'"]').val()
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp_stok);                
            })

            $('input[name="inputHargaTotal"]').val(totalharga.toLocaleString(['ban', 'id']));
            updateHargaOnKeyup();
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').data('id');

            var index = selected_items.indexOf(parseInt(id));
            if (index >= -1) {
                selected_items.splice(index, 1);
            }

            $('select[name="item_id"]').children('option[id="default"]').first().attr('selected', 'selected');
            $('#tabelKeranjang').find('tr[data-id="'+id+'"]').remove();
            
            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('input[id="harga-' + id + '"]').remove();
            $('#form-simpan').find('input[id="subtotal-' + id + '"]').remove();

            var harga_total = 0;
            $('input[name="inputSubTotal"]').each(function(index, el) {
                var subtotal = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(subtotal)) subtotal = 0;
                harga_total += subtotal;
            });
            $('input[name="inputHargaTotal"]').val(harga_total.toLocaleString(['ban', 'id']));

            var ongkos_kirim = parseInt($('input[name="inputOngkosKirim"]').val().replace(/\D/g, ''), 10);
            var harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
            $('input[name="inputHargaTotalPlusOngkosKirim"]').val(harga_total_plus_ongkos_kirim.toLocaleString(['ban', 'id']));

            var jumlah_bayar = parseInt($('input[name="inputJumlahBayar"]').val().replace(/\D/g, ''), 10);
            var utang = harga_total_plus_ongkos_kirim - jumlah_bayar;
            if (utang <= 0 || isNaN(utang)) utang = 0;
            $('input[name="inputUtang"]').val(utang.toLocaleString(['ban', 'id']));

            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            $('input[name="harga_total"]').val(harga_total);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="utang"]').val(utang);
        });

        $(document).on('click', '#tambah_kadaluarsa', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // var satuan = [[],[]];
            var satuan = [];
            $(this).parents('tr').first().find('input[name="inputJumlah"]').each(function(index, el) {
                // console.log(index);
                var konversi = parseInt($(el).attr('konversi'));
                var kode_satuan = $(el).next().find('.kode_satuan').text();
                // satuan[index]['konversi'] = konversi;
                // satuan[index]['kode'] = kode_satuan;
                satuan.push({'konversi' : konversi, 'kode' : kode_satuan});
            });
            var satuan_pilihan = '';
            for (var i = 0; i < satuan.length; i++) {
                satuan_pilihan = satuan_pilihan+`<div class="row">
                            <div class="col-md-12">
                                <div class="input-group text-center">
                                    <input id="inputJumlahExp" name="inputJumlahExp" class="pull-right form-control input-sm angka" konversi="`+satuan[i]['konversi']+`"  type="text">
                                    <div class="input-group-addon" style="width: 60px; text-align: right;">`+satuan[i]['kode']+`
                                    </div>
                                </div>
                            </div>
                        </div>`;
            }

            // var jkal = parseInt($('#anak-kadaluarsa-'+id).attr('jkal'));
            // var skal = $('#kadaluarsa-'+id).attr('skal');
            // if (skal == 0) {
            //     $('#kadaluarsa-'+id).remove();
            // } else {
            //     jkal += 1;
            //     $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jkal+'" value="" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-'+jkal+'" value="' + id + '" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-'+jkal+'" value="" class="hpp-stok-' + id + '" />');
            //         $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-'+jkal+'" value="" class="jumlah-stok-' + id + '"/>');
            // }

            var jumlah_kadaluarsa = 0;
            $('#anak-kadaluarsa-'+id+' .m-kadal').each(function(index, el) {
                jumlah_kadaluarsa++;
            });
            jumlah_kadaluarsa++;

            $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa_stok[]" id="kadaluarsa-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_stok[]" id="item-stok-' + id + '-'+jumlah_kadaluarsa+'" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="hpp_stok[]" id="hpp-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="hpp-stok-' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah_stok[]" id="jumlah-stok-' + id + '-'+jumlah_kadaluarsa+'" value="" class="jumlah-stok-' + id + '"/>');

            $('#anak-kadaluarsa-'+id).append(`
                <div class="line"></div>
                    <div class="row kol-kadal m-kadal" kol-kadal="`+jumlah_kadaluarsa+`">
                        <div class="col-md-12">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Jumlah
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">
                                            Kadaluarsa
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 0;">
                                `+satuan_pilihan+`
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input name="checkKadaluarsa" id="checkKadaluarsa" type="checkbox">
                                    </span>
                                    <input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok" disabled="" value="" type="text">
                                </div>
                            </div>
                        </div>
                    </div>`);
        });

        $(document).on('keyup', 'input[name="inputJumlahExp"]', function(event) {
            event.preventDefault();

            var konversi = $(this).attr('konversi');
            var $tr = $(this).parents('tr').first();
            var id = $tr.data('id');
            var $jumlah = $tr.find('input[name="jumlah"]');
            var jumlah = 0;
            var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            // console.log(jkal);
            var kol_kadal = $(this).parents('.kol-kadal').first().find('input[name="inputJumlahExp"]').each(function(index, el) {
                var value = parseInt($(el).val().replace(/\D/g, ''), 10);
                var konversi = parseInt($(el).attr('konversi'));
                value *= konversi;
                if (isNaN(value)) value = 0;
                jumlah += value;
            }); 
            
            var hpp = $('#form-simpan').find('input[id="harga-'+id+'"]').val()
            $('.hpp-stok-'+id).each(function(index, el) {
                $(el).val(hpp);             
            })
            // console.log(jumlah);
            $('#form-simpan').find('input[id="jumlah-stok-' + id + '-'+jkal+'"]').val(jumlah);
            var jumlah_total = $('#form-simpan').find('input[id="jumlah-'+id+'"]').val();           

            var total_hasil = 0;
            $('.jumlah-stok-'+id).each(function(index, el) {
                var jumlah_stok = parseInt($(el).val());
                if (isNaN(jumlah_stok)) jumlah_stok = 0;
                total_hasil += jumlah_stok;
            })

            if(parseInt(total_hasil) != parseInt(jumlah_total)){
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).addClass('has-error');
                });
            }else{
                $tr.find('.kol-kadal').each(function(index, el) {
                    $(el).removeClass('has-error');
                });
            }           
        });

    </script>
@endsection
