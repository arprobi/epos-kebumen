@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Pendapatan Lain-Lain</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-5" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Pendapatan Lain-Lain</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right">	
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('pendapatan_lain') }}" class="form-horizontal">
                        <div class="col-md-12 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control input_cek angka" type="text" name="nominal_" required="">
                                <input class="form-control input_cek" type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control input_cek" type="text" name="keterangan" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Kas Tujuan</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank" id="bank" class="form-control select2_single">
                                    <option value="" id="default">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                    @endforeach
                                </select>
                            </div>
                                <input type="hidden" name="kas" />
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7 col-xs-7" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Daftar Pendapatan Lain-Lain</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('pendapatan_lain/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="referensi">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tableJenisItem" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="referensi">No</th>
                                <th class="sorting" field="referensi">Referensi</th>
                                <th class="sorting" field="kredit">Nominal</th>
                                <th class="sorting" field="keterangan">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('pendapatan_lain/mdt1') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="4" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var pendapatan_lain = data[i];

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${pendapatan_lain.id}">
                                    <td>${nomor}</td>
                                    <td>${pendapatan_lain.referensi}</td>
                                    <td class="text-right">${pendapatan_lain.kredit}</td>
                                    <td>${pendapatan_lain.keterangan}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');

            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            btnSimpan();
        });

        $(window).on('load', function(event) {

            var url = "{{ url('pendapatan_lain/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			

            $.get(url, function(data) {
                if (data.pendapatan === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/PLL/' + tanggal;
                } else {
                    var referensi = data.pendapatan.referensi.substr(0,20);
                    // var referensi = data.pendapatan.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/PLL/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/PLL/' + tanggal_transaksi;				}
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#jenis_beban', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
