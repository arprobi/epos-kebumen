@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah PO Penjualan</title>
@endsection

@section('style')
	<style type="text/css">
		#title,
		#kodeTransaksiPenjualanTitle {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-5 col-xs-12">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2 id="title">PO Penjualan</h2>
						<span id="kodeTransaksiPenjualanTitle"></span>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label class="control-label">Pelanggan</label>
								<select name="pelanggan" class="select2_single form-control">
									<option id="default-pelanggan">Pilih Pelanggan</option>
									@foreach ($pelanggans as $pelanggan)
									<option value="{{ $pelanggan->id }}">{{ $pelanggan->nama }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label class="control-label">Nama Item</label>
								<select name="item_id" class="select2_single form-control">
									<option id="default-item">Pilih Item</option>
									@foreach ($items as $item)
									<option value="{{ $item->kode }}">{{ $item->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Informasi Item</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table" id="tabel-info">
							<thead>
								<tr>
									<th>Nama Item</th>
									<th>Stok</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Keranjang PO Penjualan</h2>
				<a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table" id="tabel-keranjang">
					<thead>
						<tr>
							<th></th>
							<th>Nama Item</th>
							<th>Jumlah</th>
							<th>Satuan</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<form id="form-simpan" method="post" action="{{ url('po-penjualan') }}">
					{{ csrf_field() }}
					<input type="hidden" name="kode_transaksi" />
					<input type="hidden" name="pelanggan_id" />
					<input type="hidden" name="harga_total" />

					<div id="append-section"></div>

					<button type="submit" class="btn btn-success">
						<i class="fa fa-check"></i> OK
					</button>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
	var select_satuan = '<select name="satuan" class="form-control input-sm">'+
							'@foreach($satuans as $satuan)'+
							'<option value="{{ $satuan->id }}">{{ $satuan->kode }}</option>'+
							'@endforeach'+
						'</select>';

		$(document).ready(function() {
			var url = "{{ url('po-penjualan/create') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$(".select2_single").select2();
		});

		$(window).on('load', function(event) {
			var url = "{{ url('transaksi/last/json') }}";
			var tanggal = printTanggalSekarang('dd/mm/yyyy');			

			$.get(url, function(data) {
				if (data.transaksi_penjualan === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/TRAJ/' + tanggal;
				} else {
					var kode_transaksi = data.transaksi_penjualan.kode_transaksi;
					var dd_transaksi = kode_transaksi.split('/')[2];
					var mm_transaksi = kode_transaksi.split('/')[3];
					var yyyy_transaksi = kode_transaksi.split('/')[4];
					var tanggal_transaksi = dd_transaksi + '/' + mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/TRAJ/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/TRAJ/' + tanggal_transaksi;
					}
				}

				$('input[name="kode_transaksi"]').val(kode_transaksi);
				$('#kodeTransaksiPenjualanTitle').text(kode_transaksi);
			});
		});

		$(document).on('change', 'select[name="pelanggan"]', function(event) {
			event.preventDefault();

			var id = $(this).val();
			
			$('input[name="pelanggan_id"]').val(id);
		});

		$(document).on('change', 'select[name="item_id"]', function(event) {
			event.preventDefault();

			var kode = $(this).val();
			var url  = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
			var tr   = $('#tabel-keranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

			$.get(url, function(data) {
				var nama      = data.item.nama;
				var stoktotal = data.stok.stok;

				if (tr === undefined) {
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="0" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="1" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="" />');
					$('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal[]" id="subtotal-'+kode+'" value="" />');

					$('#tabel-keranjang').find('tbody').append(
						'<tr data-id="'+kode+'">'+
							'<td>'+
								'<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
							'</td>'+
							'<td style="padding-top: 13px;">'+nama+'</td>'+
							'<td>'+
								'<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm" />'+
							'</td>'+
							'<td>'+select_satuan+'</td>'+
						'<tr>');
				}

				$('#tabel-info').find('tbody').children('tr').first().children('td').first().text(nama);
				$('#tabel-info').find('tbody').children('tr').first().children('td').last().text(stoktotal)
			});
		});

		$(document).on('keyup', '#inputJumlahItem', function(event) {
			event.preventDefault();
			
			var jumlah = $(this).val();
			if (jumlah === '') jumlah = 0;

			var kode   = $(this).parents('tr').data('id');
			var satuan = $('#satuan-'+kode).val();
			var tr     = $('#tabel-keranjang').find('tr[data-id="'+kode+'"]');
			var url    = "{{ url('transaksi') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;

			$('#jumlah-'+kode).val(jumlah);

			$.get(url, function(data) {
				if (data.harga === null) {
					$('#harga-'+kode).val(0);
					$('#subtotal-'+kode).val(0);	
				} else {
					var harga 		= data.harga.harga;
					var subtotal	= parseInt(harga) * parseInt(jumlah);
					var harga_total = 0;

					$('#harga-'+kode).val(harga);
					$('#subtotal-'+kode).val(subtotal);

					$('.subtotal').each(function(index, el) {
						var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
						if (isNaN(tmp)) tmp = 0;
						harga_total += tmp;
					});

					$('input[name="harga_total"]').val(harga_total);
				}
			});
		});

		$(document).on('change', 'select[name="satuan"]', function(event) {
			event.preventDefault();
			
			var kode   = $(this).parents('tr').data('id');
			var satuan = $(this).val();

			$('#satuan-'+kode).val(satuan);
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			
			var kode = $(this).parents('tr').data('id');
			var tr   = $('#tabel-keranjang').find('tr[data-id="'+kode+'"]');

			tr.remove();
			$('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
		});
	</script>
@endsection
