@extends('layouts.blank')

@section('style')
    <style type="text/css">
        body {
            background-color: #f7f7f7;
        }
        .submit {
            float: left;
        }
    </style>
@endsection

@section('content')
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form method="post" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Nama Pengguna" name="username" value="{{old('username')}}" required="" autofocus="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Kata Sandi" name="password" value="{{old('password')}}" required="" />
                    </div>
                    <div>
                        <button class="btn btn-default submit">Log in</button>
                    </div>

                    <div class="clearfix"></div>
                </form>
            </section>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses'))
        <script type="text/javascript">
            // swal({
            //     title: 'Mantap!',
            //     text: 'Anda berhasil login!',
            //     timer: 3000,
            //     type: 'success'
            // }, function(isConfirm) {
            //     window.location.replace("{{url('beranda')}}");
            // });

            // swal({
            //     title: 'Mantap!',
            //     text: 'Anda berhasil login!',
            //     timer: 3000,
            //     type: 'success',
            //     closeOnConfirm: true,
            //     preConfirm: () => {
            //         return new Promise((resolve) => {
            //             setTimeout(() => {
            //             window.location.replace("{{url('beranda')}}");
            //           }, 2000)
            //         })
            //       },
            // }).then(function() {
            //     window.location.replace("{{url('beranda')}}");
            // });
            swal({
                title: 'Mantap!',
                text: 'Anda berhasil login!',
                timer: 3000,
                type: 'success',
                closeOnConfirm: true,
                // onOpen: () => {
                //     console.log('open');
                // },
                onClose: () => {
                    // console.log('close');
                    window.location.replace("{{url('beranda')}}");
                }
            });
        </script>
    @elseif (session('gagal') == 'not matched')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Nama Pengguna dan Kata Sandi tidak cocok!',
                type: 'error'
            });
            $('input[name="username"]').val("{{session('username')}}");
            $('input[name="password"]').val("{{session('password')}}");
        </script>
    @elseif (session('gagal') == 'not_found')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Nama Pengguna tidak ditemukan!',
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == 'not found')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Nama Pengguna \"{{session('username')}}\" tidak ditemukan!',
                type: 'error'
            });
            $('input[name="username"]').val("{{session('username')}}");
            $('input[name="password"]').val("{{session('password')}}");
        </script>
    @endif

    <script type="text/javascript">
        setTimeout(function() {
            $('#spinner').hide();
        }, 500);
    </script>

@endsection
