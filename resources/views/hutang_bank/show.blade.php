@extends('layouts.admin')

@section('title')
    @if($hutang_bank->sisa_hutang > 0)
    	<title>EPOS | Bayar Hutang Bank</title>
    @else
    	<title>EPOS | Data Riwayat Pembayaran Hutang</title>
    @endif
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		@if($hutang_bank->sisa_hutang > 0)
			<div class="col-md-4">
				<div class="x_panel">
					<div class="x_title">
						<h2 id="formSimpanTitle">Bayar Hutang Bank</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" id="formSimpanContainer">
				        <form method="post" action="{{ url('bayar_hutang_bank') }}" class="form-horizontal">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
							<div class="form-group">
								<label class="control-label">Kode Transaksi</label>
								<input class="form-control" type="text" name="kode_transaksi" readonly>
							</div>
							<div class="form-group">
								<label class="control-label">Nama</label>
								<input class="form-control" type="text" name="nama" disabled value="{{ $hutang_bank->bank->nama_bank }}">
							</div>
							<div class="form-group">
								<label class="control-label">Jumlah Bayar Angsuran Berjalan</label>
								<input class="form-control angka" type="text" name="nominal_" required="">
								<input class="form-control" type="hidden" name="nominal">
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label class="control-label">Bunga</label>
									</div>
								</div>
							<div class="row">
								<div class="col-md-5">
									<div class="input-group" style="margin-bottom: 0;">
										<div class="input-group-addon"><input type="checkbox" name="cek_bunga" id="cek_bunga" /> % </div>
										<input class="form-control" type="text" id="bunga" disabled="">
									</div>
								</div>
								<div class="col-md-7">
									<input class="form-control text-right" type="text" name="bunga_" id="nilai_bunga">
									<input class="form-control" type="hidden" name="bunga">
								</div>
							</div>
							<div class="form-group" style="padding-top: 15px">
								<label class="control-label">Jumlah Bayar</label>
								<input class="form-control angka text-right" type="text" name="total_">
								<input class="form-control" type="hidden" name="total">
							</div>
							</div>
							<div class="form-group">
								<label class="control-label">Dari Kas</label>
								<div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
									<div class="btn-group" role="group">
										<button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="btnBank" class="btn btn-default">Bank</button>
									</div>
								</div>
							</div>
							<div class="form-group" id="BankContainer">
								<div class="form-group">
									<label class="control-label">Pilih Rekening Bank</label>
									<select name="bank" id="bank" class="select2_single form-control">
										<option value="" id="default">Pilih Bank</option>
										@foreach($banks as $bank)
										<option value="{{ $bank->id }}">{{ $bank->nama_bank}} - {{$bank->no_rekening}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group" id="inputNoTransferContainer">
								<label class="control-label">Nomor Transaksi</label>
								<input class="form-control" id="inputNoTransfer" type="text" name="no_transaksi">
							</div>
								<input type="hidden" name="kas" />
								<input type="hidden" name="hutang_bank_id" value="{{ $hutang_bank->id }}" />
							<div class="form-group" style="margin-bottom: 0;">
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> <span>Bayar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		@endif
		<!-- kolom kanan -->
		<div class="col-md-8">
			<div class="x_panel">
				<div class="x_title">
					<h2>Data Riwayat Pembayaran Hutang {{ $hutang_bank->bank->nama_bank }}</h2>
					<a href="{{url('hutang_bank')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					@if($hutang_bank->sisa_hutang > 0)	
						<p class="pull-right"><b>Sisa Hutang </b>: {{ \App\Util::duit($hutang_bank->sisa_hutang) }}</p>
					@endif
					<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Pembayaran</th>
								<th>Pembayaran Via</th>
								<th>Nomor Transaksi</th>
								<th>Jumlah Dibayar</th>	
							</tr>
						</thead>
						<tbody>
							@foreach($bayar as $num => $log)
							<tr id="{{$log->id}}">
								<td>{{ $num+1 }}</td>
								<td>{{ $log->kode_transaksi }}</td>
								<td>{{ $log->created_at }}</td>
								<td style="text-transform: capitalize;">
									@if($log->metode_pembayaran == 'debit')
										Bank
									@elseif($log->metode_pembayaran == 'tunai')
										Tunai
									@else{
										{{ $log->metode_pembayaran}}
									@endif
								</td>
								<td>{{ $log->no_transaksi }}</td>
								<td align="right">{{ \App\Util::duit($log->nominal) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil dibayar!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Hutang gagal dibayar!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();
		$('.select2_single').select2({
			allowClear: true
		});
		$('input[name="total_"]').prop('disabled', true);
		$('input[name="bunga_"]').prop('disabled', true);

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
			$('#bunga').prop('disabled', true);
			$('input[name="nominal_"]').val('');
			$('input[name="nominal"]').val('');
			$('input[name="bunga"]').val('');
			$('input[name="total_"]').val('');
			$('input[name="total"]').val('');
			$('#nilai_bunga').val('');

		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
			btnSimpan();
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
			btnSimpan();
		});

		$(window).on('load', function(event) {
			var url = "{{ url('bayar_hutang_bank/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			
			console.log(tanggal);
			$.get(url, function(data) {
				if (data.bayar_hutang_bank === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/BHL/' + tanggal;
				} else {
					var kode_transaksi = data.bayar_hutang_bank.kode_transaksi;
					var mm_transaksi = kode_transaksi.split('/')[2];
					var yyyy_transaksi = kode_transaksi.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
					// console.log(kode_transaksi.split('/')[3]);
					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/BHL/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/BHL/' + tanggal_transaksi;
					}
				}
				
				$('input[name="kode_transaksi"]').val(kode_transaksi);
			});
		});

		$(document).on('keyup', '#bunga', function(event) {
			event.preventDefault();
			var bunga = $(this).val();
			bunga = parseFloat(bunga.replace(',', '.'));
			var nominal = parseFloat($('input[name="nominal"]').val());
			var bunga_val = nominal * bunga / 100;
			bunga_val = parseFloat(bunga_val.toFixed(2));
			var bunga_view = bunga_val.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
			var total_val = bunga_val + nominal;
			var total_view = total_val.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

			console.log(bunga_val,  nominal, total_val, total_view);
			if(!isNaN(nominal) && !isNaN(bunga)){
				$('input[name="bunga"]').val(bunga_val);
				$('input[name="bunga_"]').val(bunga_view);
				$('input[name="total"]').val(total_val);
				$('input[name="total_"]').val(total_view);
			}
		});

		$(document).on('keyup', 'input[name="nominal_"]', function(event) {
			event.preventDefault();
			var nominal = parseFloat($(this).val().replace(/\D/g, ''), 10);
			var status_bunga = $('#bunga').val();
			console.log(status_bunga);
			var total_val = nominal;
			var total_view = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
			if(status_bunga.length > 0){
				var bunga = parseFloat(status_bunga.replace(',', '.'));	
				var bunga_val = nominal * bunga / 100;
				bunga_val = parseFloat(bunga_val.toFixed(2));
				var bunga_view = bunga_val.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});
				var total_val = bunga_val + nominal;
				var total_view = total_val.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});

				$('input[name="bunga"]').val(bunga_val);
				$('input[name="bunga_"]').val(bunga_view);
			}

			$('input[name="total"]').val(total_val);
			$('input[name="total_"]').val(total_view);
		});

		$(document).on('change', '#cek_bunga', function(event) {
			event.preventDefault();

			var checked = $(this).prop('checked');
			if (checked) {
				$('#bunga').prop('disabled', false);
			}else{
				$('input[name="bunga_"]').prop('disabled', true);
				$('#bunga').val('');
				$('#bunga').prop('disabled', true);
				$('#nilai_bunga').val('0');
				$('input[name="bunga"]').val('');
				var nominal = parseFloat($('input[name="nominal_"]').val().replace(/\D/g, ''), 10);
				var total_val = nominal;
				var total_view = nominal.toLocaleString(['ban', 'id'], {minimumFractionDigits:2});	

				if(!isNaN(nominal)){
					$('input[name="total"]').val(total_val);
					$('input[name="total_"]').val(total_view);
				}
			}
		});

		$(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                console.log(bank);
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            }else{
                $('#btnSimpan').prop('disabled', false);
            }
        }
	</script>
@endsection