@extends('layouts.admin')

@section('title')
    <title>EPOS | Setoran Kasir</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="col-md-8 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Data Setoran Kasir Hari Ini</h2>
            <strong><p class="pull-right">Uang di Laci Saat ini : {{ \App\Util::ewon($laci->grosir) }}</p></strong>
            <div class="clearfix"></div>
        </div>
        <div id="formApprove" style="display: none;">
            <form method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="_method" value="post">
            </form>
        </div>
        <div class="x_content">
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranKasir">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kasir</th>
                        <th>Waktu</th>
                        <th>Nominal</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($setors as $i => $setor)
                    <tr id="{{ $setor->id }}">
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $setor->user->nama }}</td>
                        <td>{{ $setor->created_at->format('H:i') }}</td>
                        <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                        <td style="text-align: center">
                            @if($setor->status == 0)
                                <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                    Setujui
                                </button>
                            @else
                                <span class="label label-info">Sudah disetujui</span>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'setuju')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Kasir berhasil disetujui!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Kasir gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Kasir berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Kasir gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Kasir berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Kasir gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableSetoranKasir').DataTable();

        $(document).on('click', '#approve', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // console.log(id);
            $('#formApprove').find('form').attr('action', '{{ url("setoran_kasir") }}' + '/' + id);

            var nama = $tr.find('td').first().next().text();
            var jumlah = $tr.find('td').first().next().next().next().text();
            // $('input[name="id"]').val(id);

            swal({
                title: 'Setujui?',
                text: '\"Setoran dari ' + nama + ' sebesar ' + jumlah + '?\"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Setujui!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formApprove').find('form').attr('action', '{{ url("setoran-kasir") }}' + '/' + id);
                $('#formApprove').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });
    </script>
@endsection
