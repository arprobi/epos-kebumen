@extends('layouts.admin')

@section('title')
    <title>EPOS | Laci Kasir</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="col-md-4 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 id="formSimpanTitle">Transfer Laci ke Pengguna Lain</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content" id="formSimpanContainer">
            <form method="post" action="{{ url('setoran_kasir') }}" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="control-label">Nominal</label>
                    <input class="form-control angka" type="text" name="nominal_show" id="nominal" required="">
                    <input type="hidden" name="nominal">
                </div>
                <div class="form-group">
                    <label class="control-label">Penerima</label>
                    <select name="penerima" id="penerima" class="form-control select2_single" required="">
                        <option value="">Pilih Penerima</option>
                        @foreach($all_receiver as $i => $user)
                            <option value="{{$user->id}}">{{$user->nama}} [{{$user->level->nama}}]</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" style="margin-bottom: 0;">
                    <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                        <i class="fa fa-save"></i> <span>Tambah</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- kolom kanan -->
<div class="col-md-8 col-xs-12">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data Setoran Masuk Kasir {{ $cash_drawer->user->nama }}</h2>
                    <p class="pull-right">Uang di Laci : {{ \App\Util::ewon($cash_drawer->nominal) }}</p>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranKasir" var="{{ $a = 0 }}">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pengirim</th>
                                <th>Waktu</th>
                                <th>Nominal</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($log_laci_in as $i => $setoran)
                                @if($setoran->pengirim != null)
                                    <tr id="{{ $setoran->id }}">
                                        <td>{{ $a + 1 }}</td>
                                        <td>{{ $setoran->sender->nama }}</td>
                                        <td>{{ $setoran->created_at->format('d-m-Y H:i') }}</td>
                                        <td class="text-right">{{ \App\Util::ewon($setoran->nominal) }}</td>
                                        <td style="text-align: center">
                                            @if($setoran->approve == 0)
                                                <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                                    Setujui
                                                </button>
                                            @else
                                                <span class="label label-info">Sudah disetujui</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data Setoran Keluar Kasir {{ $cash_drawer->user->nama }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranKasir1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Penerima</th>
                                <th>Waktu</th>
                                <th>Nominal</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($log_laci_out as $i => $setoran)
                                <tr id="{{ $setoran->id }}">
                                    <td>{{ $a + 1 }}</td>
                                    <td>{{ $setoran->receiver->nama }}</td>
                                    <td>{{ $setoran->created_at->format('d-m-Y H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setoran->nominal) }}</td>
                                    <td style="text-align: center">
                                        @if($setoran->approve == 0)
                                            <span class="label label-danger">Belum diterima</span>
                                        @else
                                            <span class="label label-info">Sudah diterima</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="formApprove" style="display: none;">
    <form method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="_method" value="post">
    </form>
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Kasir berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Kasir gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Kasir berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Kasir gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'setuju')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran masuk dari Grosir berhasil disetujui!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'limit')
        <script type="text/javascript">
            console.log('cek');
            swal({
                title: 'Waduh!',
                text: 'Anda terkena Money Limit, segera setorkan uang anda ke kasir Grosir!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableSetoranKasir').DataTable();
        $('#tableSetoranKasir1').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2();
        });

        $(document).on('keyup', '#nominal', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $cash_drawer->nominal }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpan').prop('disabled', false);
            } else {
                $('#btnSimpan').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('click', '#approve', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // console.log(id);
            $('#formApprove').find('form').attr('action', '{{ url("approve_eceran") }}' + '/' + id);

            var nama = $tr.find('td').first().next().text();
            var jumlah = $tr.find('td').first().next().next().next().text();
            // $('input[name="id"]').val(id);

            swal({
                title: 'Setujui?',
                text: '\"Setoran dari ' + nama + ' sebesar ' + jumlah + '?\"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Setujui!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formApprove').find('form').attr('action', '{{ url("approve_eceran") }}' + '/' + id);
                $('#formApprove').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });
    </script>
@endsection
