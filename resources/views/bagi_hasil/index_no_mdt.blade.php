@extends('layouts.admin')

@section('title')
    <title>EPOS | Penarikan Bagi Hasil</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        table>thead>tr>th {
            text-align: center;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Penarikan Bagi Hasil</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                    <form method="post" action="{{ url('bagi_hasil') }}" class="form-horizontal">
                        <div class="col-md-12 col-xs-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Kode Transaksi</label>
                                <input class="form-control" type="text" name="kode_transaksi" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Persen</label>
                            </div>
                            <div class="input-group" style="margin-bottom: 0;">
                                <div class="input-group-addon"> % <span class="invisible">$</span> </div>
                                <input class="form-control" type="text" id="persen">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nominal (Rp)</label>
                            </div>
                            <div class="input-group" style="margin-bottom: 0;" id="divNominal">
                                <div class="input-group-addon"><input type="checkbox" name="cek_nominal" id="cek_nominal" /></div>
                                <input class="form-control angka" type="text" id="val_nominal_" disabled="">
                                <input class="form-control" type="hidden" id="val_nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <input class="form-control" type="text" name="keterangan">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Sumber Kas</label>
                                <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank" class="form-control select2_single" id="bank_select">
                                    <option value="" id="default">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option nominal="{{ $bank->nominal }}" value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}} - {{ \App\Util::duit0($bank->nominal) }}</option>
                                    @endforeach
                                </select>
                            </div>
                                <input type="hidden" name="kas" />
                                <input type="hidden" name="nominal" />
                            @if($akun->kredit < 0)
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-sm btn-success pull-right hidden" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tarik</span>
                                    </button>
                                    <span class="pull-right" style="color:red">Anda sedang mengalami kerugian, tidak bisa tarik bagi hasil.</span>
                                </div>
                            @else
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tarik</span>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 id="formSimpanTitle">Log Penarikan Bagi Hasil</h2><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="pull-left">
                                    Saldo Laba Ditahan: 
                                    @if($akun->kredit < 0)
                                        ({{ \App\Util::Duit($akun->kredit * -1) }})
                                    @else
                                        {{ \App\Util::Duit($akun->kredit) }}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                    <thead>
                        <tr>
                            <th>Referensi</th>
                            <th>Kode Akun</th>
                            <th>Nama Akun</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tariks as $nums => $tarik)
                            @if($tarik->debet != 0 && $tarik->debet != NULL)
                            <tr>
                                <td>{{ $tarik->referensi }}</td>
                                <td>{{ $tarik->kode_akun }}</td>
                                <td>{{ $tarik->akun->nama }}</td>
                                <td class="text-right">{{ \App\Util::dk($tarik->debet) }}</td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-11">
                        <h2>Nominal Kas</h2>
                    </div>
                    <div class="col-md-1">
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableKas">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kas</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Kas Tunai</td>
                            <td class="text-right">{{ \App\Util::duit2($akun_tunai->debet) }}</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Kas Bank</td>
                            <td class="text-right">{{ \App\Util::duit2($akun_bank->debet) }}</td>
                        </tr>
                        {{-- @foreach($banks as $i => $bank)
                            <tr>
                                <td>{{$i+2}}</td>
                                <td>{{$bank->nama_bank}}</td>
                                <td class="text-right">{{ \App\Util::duit2($bank->nominal) }}</td>
                            </tr>
                        @endforeach --}}
                    </tbody>
                </table>
            </div>
        </div> 
    </div>
<!-- </div> -->


@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();
        $('#tableKas').DataTable();
        $('#tablePenarikan').DataTable();
        var nominal_banks = [];
        
        $(document).ready(function() {
            nominal_banks = '{{ json_encode($nominal_bank) }}';
            nominal_banks = nominal_banks.replace(/&quot;/g, '"');
            nominal_banks = JSON.parse(nominal_banks);

            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            $('#btnSimpan').prop('disabled', true);

            $(".select2_single").select2({
                allowClear: true
            });
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            // btnSimpan();
            nominalKeyup();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            // btnSimpan();
            nominalKeyup();
        });

        $(window).on('load', function(event) {

            var url = "{{ url('bagi_hasil/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			

            $.get(url, function(data) {
                if (data.bagi_hasil === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/PBH/' + tanggal;
                } else {
                    var referensi = data.bagi_hasil.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/PBH/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/PBH/' + tanggal_transaksi;				}
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });

        $(document).on('keyup', '#val_nominal_', function(event) {
            event.preventDefault();
            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $akun->kredit }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;
            $('input[name="nominal"]').val(nominal);

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpan').prop('disabled', false);
            } else {
                $('#btnSimpan').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
            // btnSimpan();
            nominalKeyup();
        });

        $(document).on('keyup', '#persen', function(event) {
            var val = $(this).val();
            var valBaru = parseFloat(val.replace(',', '.'));
            
            var laba = {{ $akun->kredit }};
            var bagi_hasil = laba * valBaru / 100;
            
            bagi_hasil = Math.ceil(bagi_hasil / 100) * 100;
            if(isNaN(bagi_hasil)) bagi_hasil = 0;
            
            $('#val_nominal_').val(bagi_hasil.toLocaleString());
            $('#val_nominal_').trigger('keyup')
            $('input[name="nominal"]').val(bagi_hasil);
            
            nominalKeyup();
        });

        function nominalKeyup() {
            var hasil = $('input[name="nominal"]').val();
            var tunai =  {{ $akun_tunai->debet }};
            var bank =  {{ $akun_bank->debet }};
            var kas = $('input[name="kas"]').val();
            if(isNaN(hasil)) hasil = 0;

            var bank_selected = $('#bank_select').val();
            var is_bank = true;
            
            if(bank_selected == null || bank_selected == '' || bank_selected == 0 || bank_selected == undefined) is_bank = false;

            if(hasil > 0) {
                if(kas == 'tunai'){
                    if(hasil > tunai){
                        $('#divNominal').addClass('has-error');
                        $('#btnSimpan').prop('disabled', true);
                    }else{
                        $('#divNominal').removeClass('has-error');
                        $('#btnSimpan').prop('disabled', false);
                    }
                }
                else if(kas == 'bank'){
                    if(is_bank){
                        // var id_bank = $('select[name="bank').val();
                        var nominal_bank = parseFloat(nominal_banks[bank_selected]);
                        if(hasil > nominal_bank){
                            $('#divNominal').addClass('has-error');
                            $('#btnSimpan').prop('disabled', true);
                        }else{
                            $('#divNominal').removeClass('has-error');
                            $('#btnSimpan').prop('disabled', false);
                        }
                    } else {
                        $('#btnSimpan').prop('disabled', true);
                    }
                }
            } else {
                $('#btnSimpan').prop('disabled', true);
            }
        };

        $(document).on('change', '#cek_nominal', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('#val_nominal_').prop('disabled', false);
                $('#persen').prop('disabled', true);
                $('#persen').val('');
            }else{
                $('#val_nominal_').prop('disabled', true);
                $('#val_nominal_').val('');
                $('#persen').prop('disabled', false);
            }
            nominalKeyup();

        });

        $(document).on('change', '#bank_select', function(event) {
            event.preventDefault();
            // btnSimpan();
            nominalKeyup();
        });

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            // btnSimpan();
            nominalKeyup();
        });

        function btnSimpan() {
            var nominal = $('input[name="nominal"]').val();

            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection
