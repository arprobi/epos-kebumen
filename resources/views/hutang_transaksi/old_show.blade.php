@extends('layouts.admin')

@section('title')
    <title>EPOS | Bayar Hutang</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnBayar {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .input-group-addon {
            min-width: 40px;
        }
    </style>
@endsection

@section('content')
    @if($hutang->sisa_utang > 0)
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Bayar Hutang</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <div id="formSimpanContainer">
                    <form method="post" action="{{ url('hutang') }}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="id_transaksi" value="{{ $hutang->id }}">
                        <input type="hidden" name="utang" value="{{ $hutang->utang }}">
                        <input type="hidden" name="metode_pembayaran" value="tunai">
                        <div class="form-group">
                            <label class="control-label">Kode Transaksi</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-exchange"></i></div>
                                <input class="form-control" type="text" name="kode_transaksi" value="{{ $hutang->kode_transaksi }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nama Suplier</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input class="form-control" type="text" name="nama_suplier" value="{{ $hutang->suplier->nama }}" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Metode Pembayaran</label>
                            <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnTunai" class="btn btn-default">
                                        <i class="fa fa-check" style="display: none;"></i> Tunai
                                    </button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnTransfer" class="btn btn-default">
                                        <i class="fa fa-check" style="display: none;"></i> Transfer
                                    </button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnKartu" class="btn btn-default">
                                        <i class="fa fa-check" style="display: none;"></i> Kartu
                                    </button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnCek" class="btn btn-default">
                                        <i class="fa fa-check" style="display: none;"></i> Cek
                                    </button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" id="btnBG" class="btn btn-default">
                                        <i class="fa fa-check" style="display: none;"></i> BG
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="inputTunaiContainer">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Nominal Tunai</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalTunai" name="inputNominalTunai" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputTransferContainer">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_transfer">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nomor Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorTransfer" name="inputNomorTransfer" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nominal Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalTransfer" name="inputNominalTransfer" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputKartuContainer">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_kartu">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Pilih Jenis Kartu</label>
                                    <select class="form-control select2_single" name="jenis_kartu">
                                        <option value="">Pilih Kartu</option>
                                        <option value="debet">Kartu Debit</option>
                                        <option value="kredit">Kartu Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nomor Transaksi</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorKartu" name="inputNomorKartu" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nominal Dibayarkan</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalKartu" name="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputCekContainer">
                            <div class="row">
                                <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    <label class="control-label">Pilih Cek</label>
                                    <select class="form-control select2_single" name="cek_id">
                                        <option value="">Pilih Cek</option>
                                        <option value="cek_baru">Buat Cek Baru</option>
                                        @foreach ($ceks as $cek)
                                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [Rp {{ \App\Util::ewon($cek->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalCek" name="inputNominalCek" class="form-control angka" style="height: 38px;" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi sisa hutang.</p>
                                </div>
                            </div>
                            <div id="cek_baru" class="row" style="display: none;">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_cek">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nomor Cek</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorCek" name="inputNomorCek" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputBGContainer">
                            <div class="row">
                                <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    <label class="control-label">Pilih BG</label>
                                    <select class="form-control select2_single" name="bg_id">
                                        <option value="">Pilih BG</option>
                                        <option value="bg_baru">Buat BG Baru</option>
                                        @foreach ($bgs as $bg)
                                            <option value="{{ $bg->id }}">{{ $bg->nomor }} [Rp {{ \App\Util::ewon($bg->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalBG" name="inputNominalBG" class="form-control angka" style="height: 38px;" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi sisa hutang.</p>
                                </div>
                            </div>
                            <div id="bg_baru" class="row" style="display: none;">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_bg">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nomor BG</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorBG" name="inputNomorBG" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="jumlah_bayar">
                        <input type="hidden" name="nominal_tunai">
                        <input type="hidden" name="no_transfer">
                        <input type="hidden" name="bank_transfer">
                        <input type="hidden" name="nominal_transfer">
                        <input type="hidden" name="no_kartu">
                        <input type="hidden" name="bank_kartu">
                        <input type="hidden" name="jenis_kartu">
                        <input type="hidden" name="nominal_kartu">
                        <input type="hidden" name="no_cek">
                        <input type="hidden" name="bank_cek">
                        <input type="hidden" name="nominal_cek">
                        <input type="hidden" name="no_bg">
                        <input type="hidden" name="bank_bg">
                        <input type="hidden" name="nominal_bg">
                        {{-- <div class="line"></div>
                        <div class="form-group">
                            <label class="control-label">Jumlah Bayar</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                <input class="form-control angka" type="text" name="bayar" style="height: 38px;">
                                <input type="hidden" name="jumlah_bayar">
                            </div>
                        </div> --}}
                        <div class="form-group" style="margin-bottom: 0; padding-top: 10px;">
                            <button type="submit" id="buttonSubmit" class="btn btn-sm btn-success" disabled="">
                                <i class="fa fa-money"></i> Bayar
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Log Hutang</h2>
                <a href="{{ url('hutang') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="pull-left">{{ $hutang->kode_transaksi }}</p>
                @if($hutang->sisa_utang > 0)
                    <p class="pull-right"><b>Sisa Hutang </b>: {{ \App\Util::ewon($hutang->sisa_utang) }}</p>
                @endif
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pembayaran</th>
                            <th>Jumlah Dibayar</th>
                            <th>Pembayaran Via</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($hutang->hutangs as $i => $log)
                        <tr id="{{ $log->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $log->created_at }}</td>
                            <td class="text-right">{{ \App\Util::ewon($log->jumlah_bayar) }}</td>
                            <td class="text-center">
                                @if($log->nominal_tunai > 0)
                                    <span class="label label-info">Tunai</span>
                                @elseif($log->nominal_transfer > 0)
                                    <span class="label label-danger">Transfer</span>
                                @elseif($log->nominal_cek > 0)
                                    <span class="label label-warning">CEK</span>
                                @elseif($log->nominal_bg > 0)
                                    <span class="label label-success">BG</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- </div> --}}
@endsection

@section('script')
    @if (session('sukses') == 'bayar')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Hutang berhasil dibayar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'bayar')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Hutang gagal dibayar!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableItem').DataTable();

        function updateHargaOnKeyup() {
            console.log('updateHargaOnKeyup');
        }

        $(document).ready(function() {
            var url = "{{ url('hutang') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2();

            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-danger');
            $('#inputTransferContainer').hide();
            $('#inputTransferContainer').find('select').val('');
            $('#inputTransferContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('select').val('');
            $('#inputKartuContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('select').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('select').val('');
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('tunai');
            $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
            $('#formSimpanContainer').find('input[name="bayar"]').val('');
            $('#buttonSubmit').prop('disabled', true);

            $(this).removeClass('btn-default');
            $(this).addClass('btn-danger');

            $('#btnTransfer').removeClass('btn-warning');
            $('#btnTransfer').addClass('btn-default');
            $('#btnKartu').removeClass('btn-info');
            $('#btnKartu').addClass('btn-default');
            $('#btnCek').removeClass('btn-success');
            $('#btnCek').addClass('btn-default');
            $('#btnBG').removeClass('btn-primary');
            $('#btnBG').addClass('btn-default');

            $('#inputTunaiContainer').show('fast');
            $('#inputTransferContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_transfer"]').val('');
                $('input[name="bank_transfer"]').val('');
                $('input[name="nominal_transfer"]').val('');
            });
            $('#inputKartuContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_kartu"]').val('');
                $('input[name="bank_kartu"]').val('');
                $('input[name="jenis_kartu"]').val('');
                $('input[name="nominal_kartu"]').val('');
            });
            $('#inputCekContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            });
            $('#inputBGContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            });

            // $('input[name="bayar"]').prop('disabled', false);
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('transfer');
            $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
            $('#formSimpanContainer').find('input[name="bayar"]').val('');
            $('#buttonSubmit').prop('disabled', true);

            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');

            $('#btnTunai').removeClass('btn-danger');
            $('#btnTunai').addClass('btn-default');
            $('#btnKartu').removeClass('btn-info');
            $('#btnKartu').addClass('btn-default');
            $('#btnCek').removeClass('btn-success');
            $('#btnCek').addClass('btn-default');
            $('#btnBG').removeClass('btn-primary');
            $('#btnBG').addClass('btn-default');

            $('#inputTunaiContainer').hide('fast', function() {
                $(this).find('input').val('');
                $('input[name="nominal_tunai"]').val('');
            });
            $('#inputTransferContainer').show('fast');
            $('#inputKartuContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_kartu"]').val('');
                $('input[name="bank_kartu"]').val('');
                $('input[name="jenis_kartu"]').val('');
                $('input[name="nominal_kartu"]').val('');
            });
            $('#inputCekContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            });
            $('#inputBGContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            });

            // $('input[name="bayar"]').prop('disabled', false);
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('kartu');
            $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
            $('#formSimpanContainer').find('input[name="bayar"]').val('');
            $('#buttonSubmit').prop('disabled', true);

            $(this).removeClass('btn-default');
            $(this).addClass('btn-info');

            $('#btnTunai').removeClass('btn-danger');
            $('#btnTunai').addClass('btn-default');
            $('#btnTransfer').removeClass('btn-warning');
            $('#btnTransfer').addClass('btn-default');
            $('#btnCek').removeClass('btn-success');
            $('#btnCek').addClass('btn-default');
            $('#btnBG').removeClass('btn-primary');
            $('#btnBG').addClass('btn-default');

            $('#inputTunaiContainer').hide('fast', function() {
                $(this).find('input').val('');
                $('input[name="nominal_tunai"]').val('');
            });
            $('#inputTransferContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_transfer"]').val('');
                $('input[name="bank_transfer"]').val('');
                $('input[name="nominal_transfer"]').val('');
            });
            $('#inputKartuContainer').show('fast');
            $('#inputCekContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            });
            $('#inputBGContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            });

            // $('input[name="bayar"]').prop('disabled', true);
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('cek');
            $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
            $('#formSimpanContainer').find('input[name="bayar"]').val('');
            $('#buttonSubmit').prop('disabled', true);

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnTunai').removeClass('btn-danger');
            $('#btnTunai').addClass('btn-default');
            $('#btnTransfer').removeClass('btn-warning');
            $('#btnTransfer').addClass('btn-default');
            $('#btnKartu').removeClass('btn-info');
            $('#btnKartu').addClass('btn-default');
            $('#btnBG').removeClass('btn-primary');
            $('#btnBG').addClass('btn-default');

            $('#inputTunaiContainer').hide('fast', function() {
                $(this).find('input').val('');
                $('input[name="nominal_tunai"]').val('');
            });
            $('#inputTransferContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_transfer"]').val('');
                $('input[name="bank_transfer"]').val('');
                $('input[name="nominal_transfer"]').val('');
            });
            $('#inputKartuContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_kartu"]').val('');
                $('input[name="bank_kartu"]').val('');
                $('input[name="jenis_kartu"]').val('');
                $('input[name="nominal_kartu"]').val('');
            });
            $('#inputCekContainer').show('fast');
            $('#inputCekContainer').find('p').hide();
            $('#inputBGContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            });

            // $('input[name="bayar"]').prop('disabled', true);
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            $('#formSimpanContainer').find('input[name="metode_pembayaran"]').val('bg');
            $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
            $('#formSimpanContainer').find('input[name="bayar"]').val('');
            $('#buttonSubmit').prop('disabled', true);

            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');

            $('#btnTunai').removeClass('btn-danger');
            $('#btnTunai').addClass('btn-default');
            $('#btnTransfer').removeClass('btn-warning');
            $('#btnTransfer').addClass('btn-default');
            $('#btnKartu').removeClass('btn-info');
            $('#btnKartu').addClass('btn-default');
            $('#btnCek').removeClass('btn-success');
            $('#btnCek').addClass('btn-default');

            $('#inputTunaiContainer').hide('fast', function() {
                $(this).find('input').val('');
                $('input[name="nominal_tunai"]').val('');
            });
            $('#inputTransferContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_transfer"]').val('');
                $('input[name="bank_transfer"]').val('');
                $('input[name="nominal_transfer"]').val('');
            });
            $('#inputKartuContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $('input[name="no_kartu"]').val('');
                $('input[name="bank_kartu"]').val('');
                $('input[name="jenis_kartu"]').val('');
                $('input[name="nominal_kartu"]').val('');
            });
            $('#inputCekContainer').hide('fast', function() {
                $(this).find('select').val('').trigger('change');
                $(this).find('input').val('');
                $(this).find('p').hide();
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            });
            $('#inputBGContainer').show('fast');
            $('#inputCekContainer').find('p').hide();

            // $('input[name="bayar"]').prop('disabled', true);
        });

        // INPUT TUNAI
        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        // INPUT TRANSFER
        $(document).on('change', 'select[name="bank_transfer"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            $('input[name="bank_transfer"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        // INPUT KARTU
        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorKartu', function(event) {
            event.preventDefault();

            var nomor_kartu = $(this).val();
            $('input[name="no_kartu"]').val(nomor_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT CEK
        $(document).on('change', 'select[name="cek_id"]', function(event) {
            event.preventDefault();

            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;

            var utang = harga_total - (jumlah_bayar - nominal_cek);

            var cek_id = $(this).val();
            if (cek_id != 'cek_baru') {
                cek_id = parseInt(cek_id);
            }

            if (Number.isInteger(cek_id)) {
                var cek_text = $(this).select2('data')[0].text;
                var nominal_text = cek_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = cek_text.split(' [')[0];
                $('#cek_baru').hide('fast');
                $('#inputNominalCek').prop('disabled', true);
                $('input[name="bank_cek"]').val('');
                $('select[name="bank_cek"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputCekContainer').find('p').hide();
                    $('#inputNominalCek').val(nominal_text);
                    $('input[name="no_cek"]').val(nomor);
                    $('input[name="cek_id"]').val(cek_id);
                    $('input[name="nominal_cek"]').val(nominal);
                } else {
                    // Error
                    $('#inputCekContainer').find('p').show();
                    $('#inputNominalCek').val('');
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                }

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');
            } else if(cek_id == 'cek_baru') {
                $('#cek_baru').show('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', false);

                $('input[name="no_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            } else {
                $('#cek_baru').hide('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', true);

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');

                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();

            var nomor_cek = $(this).val();
            $('input[name="no_cek"]').val(nomor_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            $('input[name="nominal_cek"]').val(nominal_cek);
            // total_uang();
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_cek"]', function(event) {
            event.preventDefault();

            var bank_cek = $(this).val();
            $('input[name="bank_cek"]').val(bank_cek);
            updateHargaOnKeyup();
        });

        // INPUT BG
        $(document).on('change', 'select[name="bg_id"]', function(event) {
            event.preventDefault();

            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var utang = harga_total - (jumlah_bayar - nominal_bg);

            var bg_id = $(this).val();
            if (bg_id != 'bg_baru') {
                bg_id = parseInt(bg_id);
            }

            if (Number.isInteger(bg_id)) {
                var bg_text = $(this).select2('data')[0].text;
                var nominal_text = bg_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = bg_text.split(' [')[0];
                $('#bg_baru').hide('fast');
                $('#inputNominalBG').prop('disabled', true);
                $('input[name="bank_bg"]').val('');
                $('select[name="bank_bg"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputBGContainer').find('p').hide();
                    $('#inputNominalBG').val(nominal_text);
                    $('input[name="no_bg"]').val(nomor);
                    $('input[name="bg_id"]').val(bg_id);
                    $('input[name="nominal_bg"]').val(nominal);
                } else {
                    // Error
                    $('#inputBGContainer').find('p').show();
                    $('#inputNominalBG').val('');
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                }

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');
            } else if(bg_id == 'bg_baru') {
                $('#bg_baru').show('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', false);

                $('input[name="no_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            } else {
                $('#bg_baru').hide('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', true);

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');

                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();

            var nomor_bg = $(this).val();
            $('input[name="no_bg"]').val(nomor_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            $('input[name="nominal_bg"]').val(nominal_bg);
            // total_uang();
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_bg"]', function(event) {
            event.preventDefault();

            var bank_bg = $(this).val();
            $('input[name="bank_bg"]').val(bank_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', 'input[name="bayar"]', function(event) {
            event.preventDefault();

            // Nunggu js di admin selesai
            setTimeout(function() {
                var nominal = $('input[name="bayar"]').val();
                if (!isNaN(nominal) && nominal.length > 0) {
                    // Angka
                    $('#buttonSubmit').prop('disabled', false);
                } else {
                    // isNaN
                    $('#buttonSubmit').prop('disabled', true);
                }
            }, 100);
        });

        /*$(document).on('change', '#inputCekContainer select', function(event) {
            event.preventDefault();

            var sisa_utang = $('#formSimpanContainer').find('input[name="utang"]').val();
            sisa_utang = parseFloat(sisa_utang);

            var cek_text = $(this).select2('data')[0].text;
            var nominal_text = cek_text.split('Rp ')[1].split(']')[0];
            var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
            nominal = parseFloat(nominal);

            if (nominal <= sisa_utang) {
                // Success
                $('#inputCekContainer').find('p').hide();
                $('#formSimpanContainer').find('input[name="bayar"]').val(nominal_text);
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val(nominal);
                $('#buttonSubmit').prop('disabled', false);
            } else {
                // Error
                $('#inputCekContainer').find('p').show();
                $('#formSimpanContainer').find('input[name="bayar"]').val('');
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
                $('#buttonSubmit').prop('disabled', true);
            }
        });

        $(document).on('change', '#inputBGContainer select', function(event) {
            event.preventDefault();

            var sisa_utang = $('#formSimpanContainer').find('input[name="utang"]').val();
            sisa_utang = parseFloat(sisa_utang);

            var bg_text = $(this).select2('data')[0].text;
            var nominal_text = bg_text.split('Rp ')[1].split(']')[0];
            var nominal = parseInt(nominal_text.replace(/\D/g, ''), 10);
            nominal = parseFloat(nominal);

            if (nominal <= sisa_utang) {
                // Success
                $('#inputBGContainer').find('p').hide();
                $('#formSimpanContainer').find('input[name="bayar"]').val(nominal_text);
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val(nominal);
                $('#buttonSubmit').prop('disabled', false);
            } else {
                $('#inputBGContainer').find('p').show();
                $('#formSimpanContainer').find('input[name="bayar"]').val('');
                $('#formSimpanContainer').find('input[name="jumlah_bayar"]').val('');
                $('#buttonSubmit').prop('disabled', true);
            }
        });*/

    </script>
@endsection
