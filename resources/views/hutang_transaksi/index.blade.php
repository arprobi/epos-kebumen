@extends('layouts.admin')

@section('title')
    <title>EPOS | Hutang Dagang</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnBayar, #btnLogPembayaran {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Hutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('hutang/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Pemasok</th>
                                <th>Jumlah Hutang</th>
                                <th>Sisa Hutang</th>
                                <th style="width: 130px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Hutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('hutang/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Transaksi</th>
                                <th>Pemasok</th>
                                <th>Jumlah Hutang</th>
                                <th style="width: 25px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'lunas')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Hutang berhasil lunas!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('hutang/mdt1') }}";
        var mdt2 = "{{ url('hutang/mdt2') }}";

        function refreshMDTData(data, base_url) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                $mdtContainer.find('tbody').empty();
                var tr = `<tr>
                            <td colspan="6" class="tengah-h">Data tidak tersedia di tabel</td>
                        </tr>`;

                $mdtContainer.find('tbody').append($(tr));

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var hutang = data[i];
                        var buttons = hutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.bayar != null) {
                            td_buttons += `
                                <a href="${buttons.bayar.url}" class="btn btn-xs btn-primary" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Bayar">
                                    <i class="fa fa-money"></i>
                                </a>
                            `;
                        }
                        if (buttons.cek != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="CEK" disabled="">
                                    CEK
                                </a>
                            `;
                        }
                        if (buttons.bg != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-BG" id="btnDetail" data-toggle="tooltip" data-placement="top" title="BG" disabled="">
                                    BG
                                </a>
                            `;
                        }

                        var tr = `<tr id="${hutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${hutang.kode_transaksi}</td>
                                    <td>${hutang.mdt_suplier.nama}</td>
                                    <td>${hutang.utang}</td>
                                    <td>${hutang.sisa_utang}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var hutang = data[i];
                        var buttons = hutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.log != null) {
                            td_buttons += `
                                <a href="${buttons.log.url}" class="btn btn-xs btn-primary" id="btnLogPembayaran" data-toggle="tooltip" data-placement="top" title="Log Pembayaran Hutang">
                                    <i class="fa fa-history"></i>
                                </a>
                            `;
                        }

                        var tr = `<tr id="${hutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${hutang.kode_transaksi}</td>
                                    <td>${hutang.mdt_suplier.nama}</td>
                                    <td>${hutang.utang}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

    </script>

@endsection
