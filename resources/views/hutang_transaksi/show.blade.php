@extends('layouts.admin')

@section('title')
    @if($transaksi_pembelian->sisa_utang > 0)
        <title>EPOS | Bayar Hutang Dagang</title>
    @else
        <title>EPOS | Data Riwayat Hutang Dagang</title>
    @endif
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnSesuaikanCek, #btnSesuaikanBG, #btnBayar, #btnKembali {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .input-group-addon {
            min-width: 40px;
        }


    </style>
@endsection

@section('content')
    @if($transaksi_pembelian->sisa_utang > 0)
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Bayar Hutang Dagang</h2>
                    @if(Auth::user()->level_id == 5)
                        <p class="pull-right">Saldo Tunai Gudang: {{ \App\Util::duit0($laci->gudang) }}</p>
                    @else
                        <p class="pull-right">Saldo Tunai Owner: {{ \App\Util::duit0($laci->owner) }}</p>
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <div id="formSimpanContainer">
                    <form method="post" action="{{ url('hutang') }}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="id_transaksi" value="{{ $transaksi_pembelian->id }}">
                        <input type="hidden" name="utang" value="{{ $transaksi_pembelian->utang }}">
                        <input type="hidden" name="sisa_utang" value="{{ $transaksi_pembelian->sisa_utang }}">
                        {{-- <input type="hidden" name="metode_pembayaran" value="tunai"> --}}
                        <div class="form-group">
                            <label class="control-label">Kode Transaksi</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-exchange"></i></div>
                                <input class="form-control" type="text" name="kode_transaksi" value="{{ $transaksi_pembelian->kode_transaksi }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Pemasok</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input class="form-control" type="text" name="nama_suplier" value="{{ $transaksi_pembelian->suplier->nama }}" disabled="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div class="input-group">
                                    <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_transfer">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfrer" id="inputNominalTransfrer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Dibayarkan</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Cek</label>
                                        <select class="form-control select2_single" name="cek_id">
                                            <option value="">Pilih Cek</option>
                                            <option value="cek_baru">Buat Cek Baru</option>
                                            @foreach ($ceks as $cek)
                                            <option value="{{ $cek->id }}">{{ $cek->nomor }} [{{ \App\Util::ewon($cek->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalCek" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row" id="cek_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_cek">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control" />
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih BG</label>
                                        <select class="form-control select2_single" name="bg_id">
                                            <option value="">Pilih BG</option>
                                            <option value="bg_baru">Buat BG Baru</option>
                                            @foreach ($bgs as $bg)
                                            <option value="{{ $bg->id }}">{{ $bg->nomor }} [{{ \App\Util::ewon($bg->nominal) }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" id="inputNominalBG" class="form-control angka" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                        <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                    </div>
                                </div>
                                <div class="row" id="bg_baru">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_bg">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="grupJB">
                            <label class="control-label">Jumlah Bayar</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input class="form-control" type="text" id="inputJumlahBayar" disabled>
                            </div>
                        </div>
                        <input type="hidden" name="jumlah_bayar">
                        <input type="hidden" name="nominal_tunai">

                        <input type="hidden" name="no_transfer" />
                        <input type="hidden" name="bank_transfer" />
                        <input type="hidden" name="nominal_transfer" />

                        <input type="hidden" name="no_kartu" />
                        <input type="hidden" name="bank_kartu" />
                        <input type="hidden" name="jenis_kartu" />
                        <input type="hidden" name="nominal_kartu" />

                        <input type="hidden" name="cek_id" />
                        <input type="hidden" name="no_cek" />
                        <input type="hidden" name="bank_cek" />
                        <input type="hidden" name="nominal_cek" />

                        <input type="hidden" name="bg_id" />
                        <input type="hidden" name="no_bg" />
                        <input type="hidden" name="bank_bg" />
                        <input type="hidden" name="nominal_bg" />

                        {{-- <div class="line"></div>
                        <div class="form-group">
                            <label class="control-label">Jumlah Bayar</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                <input class="form-control angka" type="text" name="bayar" style="height: 38px;">
                                <input type="hidden" name="jumlah_bayar">
                            </div>
                        </div> --}}
                        <div class="form-group" style="margin-bottom: 0; padding-top: 10px;">
                            <button type="submit" id="buttonSubmit" class="btn btn-success" style="width: 100px;" disabled="">
                                <i class="fa fa-money" style="margin-right: 5px;"></i> Bayar
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Riwayat Hutang Dagang</h2>
                <a href="{{ url('hutang') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="pull-left">{{ $transaksi_pembelian->kode_transaksi }}</p>
                @if($transaksi_pembelian->sisa_utang > 0)
                    <p class="pull-right"><b>Sisa Hutang </b>: {{ \App\Util::ewon($transaksi_pembelian->sisa_utang) }}</p>
                @endif
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pembayaran</th>
                            <th>Jumlah Dibayar</th>
                            <th>Pembayaran Via</th>
                            <th>Operator</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($hutangs as $i => $log)
                        <tr id="{{ $log['id'] }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $log['created_at'] }}</td>
                            <td class="text-right">{{ \App\Util::ewon($log['jumlah']) }}</td>
                            <td class="text-center">
                                @if($log['metode'] == 'tunai')
                                    <span class="label label-danger">Tunai</span>
                                @elseif($log['metode'] == 'transfer')
                                    <span class="label label-warning">Transfer</span>
                                @elseif($log['metode'] == 'kartu')
                                    <span class="label label-info">Kartu</span>
                                @elseif($log['metode'] == 'cek')
                                    @if($log['lunas'] == 0)
                                        <a href="{{ url('hutang/'.$log['id'].'/cairkan/cek') }}" id="btnSesuaikanCek" class="btn btn-sm btn-success" title="Sesuaikan Cek" data="{{ $log['id'] }}">
                                            Sesuaikan Cek
                                        </a>
                                    @else
                                        <span class="label label-success">Cek</span>
                                    @endif
                                @elseif($log['metode'] == 'hutang')
                                    <span class="label label-warning">Bayar Hutang</span>
                                @elseif($log['metode'] == 'bg')
                                    @if($log['lunas'] == 0)
                                        <a href="{{ url('hutang/'.$log['id'].'/cairkan/bg') }}" id="btnSesuaikanBG" class="btn btn-sm btn-BG" title="Sesuaikan BG" data="{{ $log['id'] }}">
                                            Sesuaikan BG
                                        </a>
                                    @else
                                        <span class="label btn-BG">BG</span>
                                    @endif
                                @endif
                            </td>
                            <td>{{ $log['user'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- </div> --}}
@endsection

@section('script')
    @if (session('sukses') == 'bayar')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Hutang berhasil dibayar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'bayar')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Hutang gagal dibayar!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'cek')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran Cek Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'bg')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Penyesuaian Pembayaran BG Berhasil Disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableItem').DataTable();
        var laci_gudang = {{ $laci->gudang }};
        var laci_owner = {{ $laci->owner }};
        var user_level = {{ Auth::user()->level_id }};

        function isSubmitButtonDisabled(){
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var sisa_utang = parseFloat($('input[name="sisa_utang"]').val());
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var no_transfer = $('input[name="no_transfer"]').val();
            var bank_transfer = $('input[name="bank_transfer"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var no_kartu = $('input[name="no_kartu"]').val();
            var bank_kartu = $('input[name="bank_kartu"]').val();
            var jenis_kartu = $('input[name="jenis_kartu"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var cek_id = $('input[name="cek_id"]').val();
            var no_cek = $('input[name="no_cek"]').val();
            var bank_cek = $('input[name="bank_cek"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var bg_id = $('input[name="bg_id"]').val();
            var no_bg = $('input[name="no_bg"]').val();
            var bank_bg = $('input[name="bank_bg"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();

            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_kartu = parseFloat(nominal_kartu);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            if ($('#btnTunai').hasClass('btn-danger')) {
                if (nominal_tunai <= 0) return true;
                if (user_level == 5){
                    if (nominal_tunai > laci_gudang){
                        $('#inputTunaiContainer').addClass('has-error');
                        return true;
                    }else{
                        $('#inputTunaiContainer').removeClass('has-error');
                    }
                }else{
                    if (nominal_tunai > laci_owner){
                        $('#inputTunaiContainer').addClass('has-error');
                        return true;
                    }else{
                        $('#inputTunaiContainer').removeClass('has-error');
                    }
                }
            }

            if ($('#btnTransfer').hasClass('btn-warning')) {
                if (no_transfer == '') return true;
                if (bank_transfer == '') return true;
                if (nominal_transfer <= 0) return true;
            }

            if ($('#btnKartu').hasClass('btn-info')) {
                if (no_kartu == '') return true;
                if (bank_kartu == '') return true;
                if (jenis_kartu == '') return true;
                if (nominal_kartu <= 0) return true;
            }

            if ($('#btnCek').hasClass('btn-success')) {
                if (no_cek == '') return true;
                if (nominal_cek <= 0) return true;
                if (cek_id == '' && bank_cek == '') return true;
            }

            if ($('#btnBG').hasClass('btn-BG')) {
                if (no_bg == '') return true;
                if (nominal_bg <= 0) return true;
                if (bg_id == '' && bank_bg == '') return true;
            }
            
            if (jumlah_bayar <= 0) return true;

            var is_genep = sisa_utang % 100;
            if(is_genep != 0) {
                if (sisa_utang - jumlah_bayar <= -100) return true;
            }


            return false;
        }

        function updateHargaOnKeyup() {
            // console.log('updateHargaOnKeyup');
            var $jumlah_bayar = $('#inputJumlahBayar');
            // var $utang = {{ $transaksi_pembelian->sisa_utang }};

            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var nominal_kartu = $('input[name="nominal_kartu"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            nominal_tunai = parseFloat(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_transfer = parseFloat(nominal_transfer.replace(/\D/g, ''), 10);
            nominal_kartu = parseFloat(nominal_kartu.replace(/\D/g, ''), 10);
            nominal_cek = parseFloat(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg = parseFloat(nominal_bg.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_kartu + nominal_cek + nominal_bg;
            // console.log('x'+jumlah_bayar);
            var sisa_utang = parseFloat($('input[name="sisa_utang"]').val());
            var is_genep = sisa_utang % 100;
            sisa_utang -= jumlah_bayar;

            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(sisa_utang)) sisa_utang = 0;

            if(is_genep != 0) {
                if (sisa_utang < -100) {
                    sisa_utang = 0;
                    $('#grupJB').addClass('has-error');
                } else {
                    $('#grupJB').removeClass('has-error');
                }
            } else {
                if (sisa_utang < 0) {
                    $('#grupJB').addClass('has-error');
                } else {
                    $('#grupJB').removeClass('has-error');
                }
            }

            // $jumlah_bayar.val(jumlah_bayar.toLocaleString(undefined, {minimumFractionDigits: 2}));
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());
            // $utang.val(utang.toLocaleString(['ban', 'id']));

            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            // $('input[name="utang"]').val(utang);
            // console.log(nominal_cek, nominal_bg);

            // console.log('Till here', isSubmitButtonDisabled());
            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
            console.log(sisa_utang);
        }

        $(document).ready(function() {
            var url = "{{ url('hutang') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2();

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 

            $('#inputTunaiContainer').hide();
            $('#inputPiutangContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputNominalTunai').val('');
            $('#inputNominalKartu').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputNoKartu').val('');

            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
            $('#inputJumlahBayar').val('');

            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('select[name="bank_transfer"]').val('').change();
                    $('input[name="no_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="no_kartu"]').val('');
                    $('select[name="jenis_kartu"]').val('').change();
                    $('select[name="bank_kartu"]').val('').change();
                    
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                    $('input[name="bank_cek"]').val('').change();
                    $('select[name="cek_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                    $('select[name="bank_bg"]').val('').change();
                    $('select[name="bg_id"]').val('').change();
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        // INPUT TUNAI
        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if(nominal_tunai % 100 == 0) {
                $('#inputTunaiContainer').removeClass('has-error');
                $('input[name="nominal_tunai"]').val(nominal_tunai);
                updateHargaOnKeyup();
            } else {
                $('#inputTunaiContainer').addClass('has-error');
            }
        });

        // INPUT TRANSFER
        $(document).on('change', 'select[name="bank_transfer"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            // console.log(bank_transfer);
            $('input[name="bank_transfer"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();
        });

        // INPUT KARTU
        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartu', function(event) {
            event.preventDefault();

            var no_kartu = $(this).val();
            $('input[name="no_kartu"]').val(no_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('input[name="nominal_kartu"]').val(nominal);
            updateHargaOnKeyup();
        });

        // INPUT CEK
        $(document).on('change', 'select[name="cek_id"]', function(event) {
            event.preventDefault();

            $('input[name="nominal_cek"]').val('');
            updateHargaOnKeyup();

            // var utang = {{ $transaksi_pembelian->sisa_utang }};
            var sisa_utang = parseFloat($('input[name="sisa_utang"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());

            if (isNaN(sisa_utang)) sisa_utang = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

            sisa_utang -= jumlah_bayar;

            var cek_id = $(this).val();
            if (cek_id != 'cek_baru') {
                var cek_id = parseInt($(this).val());
            }

            if (Number.isInteger(cek_id)) {
                var cek_text = $(this).select2('data')[0].text;
                var nominal_text = cek_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = cek_text.split(' [')[0];
                // $('#cek_baru').addClass('sembunyi');
                $('#cek_baru').hide('fast');
                $('#inputNominalCek').prop('disabled', true);
                $('input[name="bank_cek"]').val('');
                $('select[name="bank_cek"]').val('').change();
                if (nominal <= sisa_utang) {
                    // Success
                    $('#inputCekContainer').find('p').hide();
                    $('#inputNominalCek').val(nominal_text);
                    $('input[name="cek_id"]').val(cek_id);
                    $('input[name="no_cek"]').val(nomor);
                    $('input[name="nominal_cek"]').val(nominal);
                } else {
                    // Error
                    $('#inputCekContainer').find('p').show();
                    $('#inputNominalCek').val('');
                    $('input[name="cek_id"]').val('');
                    $('input[name="no_cek"]').val('');
                    $('input[name="bank_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                }

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');
            } else if (cek_id == 'cek_baru') {
                // $('#cek_baru').removeClass('sembunyi');
                $('#cek_baru').show('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', false);

                $('input[name="cek_id"]').val('');
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            } else {
                // $('#cek_baru').addClass('sembunyi');
                $('#cek_baru').hide('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', true);

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');

                $('input[name="cek_id"]').val('');
                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('input[name="nominal_cek"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_cek"]', function(event) {
            event.preventDefault();

            var bank_cek = $(this).val();
            $('input[name="bank_cek"]').val(bank_cek);
            updateHargaOnKeyup();
        });

        // INPUT BG
        $(document).on('change', 'select[name="bg_id"]', function(event) {
            event.preventDefault();

            $('input[name="nominal_bg"]').val('');
            updateHargaOnKeyup();

            // var utang = {{ $transaksi_pembelian->sisa_utang }};
            var sisa_utang = parseFloat($('input[name="sisa_utang"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());

            if (isNaN(sisa_utang)) sisa_utang = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

            sisa_utang -= jumlah_bayar;

            var bg_id = $(this).val();
            if (bg_id != 'bg_baru') {
                var bg_id = parseInt($(this).val());
            }

            if (Number.isInteger(bg_id)) {
                var bg_text = $(this).select2('data')[0].text;
                var nominal_text = bg_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = bg_text.split(' [')[0];
                // $('#bg_baru').addClass('sembunyi');
                $('#bg_baru').hide('fast');
                $('#inputNominalBG').prop('disabled', true);
                $('input[name="bank_bg"]').val('');
                $('select[name="bank_bg"]').val('').change();
                if (nominal <= sisa_utang) {
                    // Success
                    $('#inputBGContainer').find('p').hide();
                    $('#inputNominalBG').val(nominal_text);
                    $('input[name="bg_id"]').val(bg_id);
                    $('input[name="no_bg"]').val(nomor);
                    $('input[name="nominal_bg"]').val(nominal);
                } else {
                    // Error
                    $('#inputBGContainer').find('p').show();
                    $('#inputNominalBG').val('');
                    $('input[name="bg_id"]').val('');
                    $('input[name="no_bg"]').val('');
                    $('input[name="bank_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                }

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');
            } else if (bg_id == 'bg_baru') {
                // $('#bg_baru').removeClass('sembunyi');
                $('#bg_baru').show('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', false);

                $('input[name="bg_id"]').val('');
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            } else {
                // $('#bg_baru').addClass('sembunyi');
                $('#bg_baru').hide('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', true);

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');

                $('input[name="bg_id"]').val('');
                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal = $(this).val();
            nominal = parseInt(nominal.replace(/\D/g, ''), 10);
            $('input[name="nominal_bg"]').val(nominal);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_bg"]', function(event) {
            event.preventDefault();

            var bank_bg = $(this).val();
            $('input[name="bank_bg"]').val(bank_bg);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnSesuaikanCek', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('hutang/') }}' + '/' + id + '/cairkan/cek';
            console.log(url);
            swal({
                title: 'Sesuaikan Cek?',
                text: 'Cek untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#btnSesuaikanBG', function(event) {
            event.preventDefault();

            var id = $(this).attr('data');
            var url = '{{ url('hutang/') }}' + '/' + id + '/cairkan/bg';
            swal({
                title: 'Sesuaikan BG?',
                text: 'BG untuk transaksi ini akan disesuaikan!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Sesuaikan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    window.open(url, '_self');
                } else {
                    // Canceled
                }
            }).catch(function(isConfirm) {
                //canceled
            });
        });
    </script>
@endsection
