@extends('layouts.admin')

@section('title')
    <title>EPOS | Piutang Dagang</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnTransfer, #btnTanggal, #btnDetail {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Piutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">    
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('piutang-dagang/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Transaksi</th>
                                <th>Penggalan</th>
                                <th>Nominal Piutang</th>
                                <th>Sisa Piutang</th>
                                <th style="width: 80px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Riwayat Piutang Dagang</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('piutang-dagang/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Transaksi</th>
                                <th>Penggalan</th>
                                <th>Nominal Piutang</th>
                                <th style="width: 25px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->
@endsection

@section('script')
    @if (session('sukses') == 'transfer')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transfer kerugian Piutang Berhasil!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif
    
    <script type="text/javascript">

        var mdt1 = "{{ url('piutang-dagang/mdt1') }}";
        var mdt2 = "{{ url('piutang-dagang/mdt2') }}";

        function refreshMDTData(data, base_url) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="6" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="5" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var piutang = data[i];
                        var buttons = piutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        // if (buttons.detail != null) {
                        //     td_buttons += `
                        //         <a href="${buttons.detail.url}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Piutang Dagang">
                        //             <i class="fa fa-eye"></i>
                        //         </a>
                        //     `;
                        // }
                        if (buttons.bayar != null) {
                            td_buttons += `
                                <a href="${buttons.bayar.url}" class="btn btn-xs btn-primary" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Bayar Piutang Dagang">
                                    <i class="fa fa-money"></i>
                                </a>
                            `;
                        }
                        if (buttons.transfer != null) {
                            td_buttons += `
                                <a href="${buttons.transfer.url}" class="btn btn-xs btn-success" id="btnTanggal" data-toggle="tooltip" data-placement="top" title="Transfer ke Piutang tak Tertagih">
                                    <i class="fa fa-sign-in"></i>
                                </a>
                            `;
                        }

                        var tr = `<tr id="${piutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${piutang.kode_transaksi}</td>
                                    <td>${piutang.pelanggan.nama}</td>
                                    <td class="text-right">${piutang.nominal}</td>
                                    <td class="text-right">${piutang.sisa}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var piutang = data[i];
                        var buttons = piutang.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.riwayat != null) {
                            td_buttons += `
                                <a href="${buttons.riwayat.url}" class="btn btn-xs btn-info" id="btnLogPembayaran" data-toggle="tooltip" data-placement="top" title="Riwayat Pembayaran Piutang Dagang">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                        }

                        var tr = `<tr id="${piutang.id}">
                                    <td>${no_terakhir + i + 1}</td>
                                    <td>${piutang.kode_transaksi}</td>
                                    <td>${piutang.pelanggan.nama}</td>
                                    <td class="text-right">${piutang.nominal}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            var url = "{{ url('piutang-dagang') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
        });

    </script>
@endsection
