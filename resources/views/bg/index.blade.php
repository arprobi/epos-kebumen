@extends('layouts.admin')

@section('title')
	<title>EPOS | BG</title>
@endsection

@section('style')
	<style media="screen">
		#btnSimpan {
			margin-top: 10px;
		}
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		textarea {
			min-width: 100%;
			max-width: 48px;
			min-height: 100%;
			max-height: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Data BG</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBG">
					<thead>
						<tr>
							<th>No</th>
							<th>Nomor</th>
							<th>Nominal</th>
						</tr>
					</thead>
					<tbody>
						@foreach($bgs as $i => $bg)
						<tr id="{{ $bg->id }}">
							<td>{{ $i+1 }}</td>
							<td>{{ $bg->nomor }}</td>
							<td class="text-right">{{ \App\Util::duit($bg->nominal) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2_single").select2();
		});
		$('#tableBG').DataTable();
	</script>
@endsection
