@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Penyesuaian Stok</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnKembali {
            margin-right: 0;
        }
        #btnKembali {
            margin-bottom: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
        .has-error .btn {
            border-color: #a94442;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 hidden-xs">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tambah Penyesuaian Stok</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-6 col-xs-6">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                            @foreach($items as $item)
                            <option value="{{$item->kode}}">[{{ $item->kode }}] {{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <div class="line"></div>
                        <h3 id="item_title" class="text-center"></h3>
                        <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">No</th>
                                    <th style="text-align: left;">Kode Transaksi</th>
                                    <th style="text-align: left;">Jumlah (Saat Ini)</th>
                                    <th style="text-align: left;">Kondisi</th>
                                    <th style="text-align: left;">Kadaluarsa</th>
                                    <th style="text-align: center;">Jumlah (Rusak)</th>
                                    <th style="text-align: left;">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="formSimpanContainer">
                    <form id="form-simpan" action="{{ url('penyesuaian_stok') }}" method="post">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div id="append-section"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="row sembunyi" id="submit_button">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" name="submit" id="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Stok berhasil disesuaikan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'PO gagal disimpan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        var satuans = [];
        var pelanggans = [];
        var pelanggan = null;
        var selected_items = [];
        var satuan_item = [];
        var level_user = 0;
        /*function isBtnSimpanPODisabled() {
            // console.log('isBtnSimpanPODisabled');
            var harga_total = parseFloat($('input[name="harga_total"]').val());

            if (isNaN(harga_total) || harga_total <= 0) return true;

            return false;
        }*/

        function cariItem(kode, url) {
             $.get(url, function(data) {
                $('#tabelInfo > tbody').empty();
                // $('#tabelKeranjang > tbody').empty();
                $('#append-section').empty();
                $('#item_title').text(data.item.nama);
                satuan_item = [];
                item_satuan = [];
                var item = data.item;
                var stok = data.stok_pakai;
                //simpan satuan id, konversi, kode
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                for (var i = item.satuan_pembelians.length; i > 0 ; i--) {
                    var a = i - 1;
                    var satuan = {
                        id: item.satuan_pembelians[a].satuan.id,
                        kode: item.satuan_pembelians[a].satuan.kode,
                        konversi: item.satuan_pembelians[a].konversi
                    }
                    item_satuan.push(satuan);
                }

                 var satuan_item_resort = satuan_item;

                var ul_satuan = '<ul class="dropdown-menu">';

                for (var i = 0; i < item_satuan.length; i++) {
                    var satuan = item_satuan[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                }

                ul_satuan += '</ul>';

                for(var x=0; x < stok.length; x++){
                    var jumlah = '';
                    var sisa_jumlah = parseInt(stok[x]['jumlah']);
                    var r = -1;
                    for(var b=0; b < satuan_item.length; b++){
                        var hitung = parseInt(sisa_jumlah / parseInt(satuan_item_resort[b].konversi));
                        if(hitung>0){
                            r += 1;
                            // jumlah.push({'jumlah' : hitung, 'kode' : satuan_item_resort[b].kode});
                            jumlah += hitung+' '+satuan_item_resort[b].kode+' ';
                        }
                        sisa_jumlah = sisa_jumlah % parseInt(satuan_item_resort[b].konversi);
                    }

                    if(stok[x]['rusak'] == 1){
                        var kondisi = 'Rusak';
                    }else{
                        var kondisi = 'Baik';
                    }
                    
                    if(stok[x]['kadaluarsa'] == null || stok[x]['kadaluarsa'] == 0){
                        var kadaluarsa = '-';
                    }else{
                        var kadaluarsa = stok[x]['kadaluarsa'].split('-');
                        kadaluarsa = kadaluarsa[2]+'-'+kadaluarsa[1]+'-'+kadaluarsa[0];
                    }

                    if(stok[x]['rusak'] == 0){
                        var nex_td = `<td class="td_input" width="17%">
                                            <div class="row">
                                                <div class="col-md-12" style="padding-right: 0;">
                                                    <div class="input-group" style="margin-bottom: 0;">
                                                        <span class="input-group-addon">
                                                            <input name="checkInputJumlah" id="checkInputJumlah" type="checkbox">
                                                        </span>
                                                        <input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm" disabled="" />
                                                        <div id="pilihSatuan" class="input-group-btn">
                                                            <button konversi="`+item_satuan[0].konversi+`" type="button" id="button_satuan" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; width: 60px;" disabled="" ><span class="text" style="line-height: inherit;">`+item_satuan[0].kode+`</span><span class="caret"></span></button>
                                                            `+ul_satuan+`
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="keterangan" id="keterangan" class="form-control input-sm keterangan" disabled=""/>
                                        </td>`;
                    }else{
                        var nex_td = `<td class="text-center">-</td><td class="text-center">-</td>`;
                    }

                    var a = x+1;
                    // $('#tabelInfo').find('tbody').append(`
                    //     <tr data-id="`+stok[x]['id']+`">
                    //         <td>`+a+`</td>
                    //         <td trapem_id="`+stok[x]['transaksi_pembelian_id']+`">`+stok[x]['kode_transaksi']+`</td>
                    //         <td id="jumlah" jumlah="`+stok[x]['jumlah']+`">`+jumlah+`</td>
                    //         <td>`+kondisi+`</td>
                    //         <td>`+kadaluarsa+`</td>
                    //         `+nex_td+`
                    //     </tr>
                    // `);
                    if (level_user == 1 || level_user == 2) {
                        var input_kadaluarsa = `<input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok tanggal-putih" readonly="" value="`+kadaluarsa+`" type="text">`;
                    } else {
                        var input_kadaluarsa = `<input name="inputKadaluarsa" id="inputKadaluarsa" class="form-control input-sm inputKadaluarsaStok tanggal-putih bg-disabled" readonly="" value="`+kadaluarsa+`" type="text" disabled="">`;
                    }
                    $('#tabelInfo').find('tbody').append(`
                        <tr data-id="`+stok[x]['id']+`">
                            <td>`+a+`</td>
                            <td trapem_id="`+stok[x]['transaksi_pembelian_id']+`">`+stok[x]['kode_transaksi']+`</td>
                            <td id="jumlah" jumlah="`+stok[x]['jumlah']+`">`+jumlah+`</td>
                            <td>`+kondisi+`</td>
                            <td>`+input_kadaluarsa+`</td>
                            `+nex_td+`
                        </tr>
                    `);

                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="stok_id[]" id="stok-'+stok[x]['id']+'" value="'+stok[x]['id']+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+stok[x]['id']+'" value="0" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="keterangan[]" id="keterangan-'+stok[x]['id']+'" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="kadaluarsa[]" id="kadaluarsa-'+stok[x]['id']+'" value="'+kadaluarsa+'" />');
                }

                $('#submit_button').removeClass('sembunyi');
             });
        }

        $(document).ready(function() {
            // var url = "{{ url('po-penjualan') }}";
            // var a = $('a[href="' + url + '"]');
            // a.parent().addClass('current-page');
            // a.parent().parent().show();
            // a.parent().parent().parent().addClass('active');

            $(".select2_single").select2({
                width: '100%'
                // allowClear: true
            });

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 

            level_user = '{{ Auth::user()->level_id }}';

            $('#inputHargaTotal').val(0);
            // $('#inputNegoTotal').val(0);
            $('#inputHargaTotalPlusOngkosKirim').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var kode = $(this).val();
            var url = "{{ url('stok') }}"+'/'+kode+'/item/json';

            if(!selected_items.includes(kode)) {
                selected_items.push(kode);
                // $('#spinner').show();
                cariItem(kode, url);
                // console.log(url);
            }
        });

        $(document).on('change', 'input[type="checkbox"]', function(event) {
            event.preventDefault();

            $(this).parent().next().val('');
            var tr = $(this).parents('tr').first();
            var id = tr.data('id');
            // console.log(id);

            var checked = $(this).prop('checked');
            if (checked) {
                tr.find('#button_satuan').prop('disabled', false);
                tr.find('#inputJumlahItem').prop('disabled', false);
                tr.find('#keterangan').prop('disabled', false);
                // $('#form-simpan').find('#append-section').append('<input type="hidden" name="stok_id[]" id="stok-'+id+'" value="'+id+'" />');
                // $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+id+'" value="" />');
                // $('#form-simpan').find('#append-section').append('<input type="hidden" name="keterangan[]" id="keterangan-'+id+'" value="" />');
            } else {
                tr.find('#inputJumlahItem').prop('disabled', true);
                tr.find('#button_satuan').prop('disabled', true);
                tr.find('#keterangan').prop('disabled', true);
                id = $(this).parents('tr').first().data('id');
                // $('#form-simpan').find('#append-section').find('input[id*=-'+id+']').remove();
            }
        });

        $(document).on('click', '#pilihSatuan li', function(event) {
            event.preventDefault();

            var kode = $(this).text();
            var tr = $(this).parents('tr').first();
            var id = tr.data('id');
            var button = $(this).parents('#pilihSatuan').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');
            var jumlah_stok = parseInt(tr.find('#jumlah').attr('jumlah'));

            var jumlah = parseInt($('#jumlah-'+id).val());
            var konversi = parseInt($(this).find('a').attr('konversi'));
            var total_jumlah = jumlah * konversi;

            if (isNaN(total_jumlah) || total_jumlah < 0) total_jumlah = 0;
            if (isNaN(konversi) || konversi < 0) konversi = 0;
            // console.log(total_jumlah, jumlah_stok, konversi);

            if(total_jumlah <= jumlah_stok){
                $('#jumlah-'+id).val(total_jumlah);
                button.text(kode);
                tr.find('button').attr('konversi', konversi);
                tr.find('.td_input').removeClass('has-error');
            }else{
                tr.find('.td_input').addClass('has-error');
            }
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var jumlah = parseInt($(this).val().replace(/\D/g, ''), 10);
            var tr = $(this).parents('tr').first();
            var id = tr.data('id');
            var konversi = tr.find('button').attr('konversi');

            var total_jumlah = parseInt(jumlah) * parseInt(konversi);

            var jumlah_stok = parseInt(tr.find('#jumlah').attr('jumlah'));

            if (isNaN(total_jumlah) || total_jumlah < 0) total_jumlah = 0;
            // console.log(total_jumlah, jumlah_stok, konversi);
            if(total_jumlah <= jumlah_stok){
                $('#jumlah-'+id).val(total_jumlah);
                tr.find('.td_input').removeClass('has-error');
            }else{
                tr.find('.td_input').addClass('has-error');
            }
        });

        $(document).on('keyup', '#keterangan', function(event) {
            event.preventDefault();

            var keterangan = $(this).val();
            var id = $(this).parents('tr').first().data('id');
            $('#keterangan-'+id).val(keterangan);
        });

        $(document).on('focus', '.inputKadaluarsaStok', function(event){
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            // var jkal = $(this).parents('.kol-kadal').attr('kol-kadal');
            var kol_kadal = $(this);
            $(this).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start) {
                var kadaluarsa = kol_kadal.val();
                console.log(kadaluarsa);
                $('#form-simpan').find('input[id="kadaluarsa-' + id + '"]').val(kadaluarsa);
            });
        });

    </script>
@endsection
