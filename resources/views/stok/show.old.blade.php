@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Transaksi Grosir {{ $transaksi_grosir->kode_transaksi }}</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnCairkan {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Transaksi Grosir</h2>
                <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/cairkan-kredit') }}" id="btnCairkan" class="btn btn-sm btn-success pull-right" {{ $transaksi_grosir->nominal_kredit > 0 && $piutang_dagang->sisa > 0 ? '' : 'disabled' }}>
                    <i class="fa fa-money"></i> Cairkan Kredit
                </a>
                <a href="{{ url('transaksi/'.$transaksi_grosir->id.'/retur/create') }}" class="btn btn-sm btn-warning pull-right">
                    <i class="fa fa-sign-in"></i> Retur
                </a>
                <a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right">
                    <i class="fa fa-long-arrow-left"></i> Kembali
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_grosir->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->kode_transaksi }}</td>
                                </tr>
                                @if ($transaksi_grosir->pelanggan_id != null)
                                <tr>
                                    <th>Nama Pelanggan</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->nama }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Tanggal PO</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th>Status Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->status }}</td>
                                </tr>
                                <tr>
                                    <th>Kasir</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->user->nama }}</td>
                                </tr>
                                @if ($transaksi_grosir->pengirim != null)
                                <tr>
                                    <th>Pengirim</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pengirim }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_grosir->nominal_tunai != null && $transaksi_grosir->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_tunai + ($transaksi_grosir->jumlah_bayar - $transaksi_grosir->harga_total)) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_transfer != null && $transaksi_grosir->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_id != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->bank->nama_bank }} [{{ $transaksi_grosir->bank->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_cek != null && $transaksi_grosir->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_bg != null && $transaksi_grosir->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_bg) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_kredit != null)
                                <tr>
                                    <th>No Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_grosir->no_kredit }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_kredit != null && $transaksi_grosir->nominal_kredit > 0)
                                <tr>
                                    <th>Nominal Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_kredit) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_titipan != null && $transaksi_grosir->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_titipan) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_grosir->harga_total != null && $transaksi_grosir->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nego_total != null && $transaksi_grosir->nego_total > 0)
                                <tr>
                                    <th>Nego Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nego_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->ongkos_kirim != null && $transaksi_grosir->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->ongkos_kirim) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->jumlah_bayar != null && $transaksi_grosir->jumlah_bayar > 0)
                                <tr>
                                    <th>Kembali</th>
                                    {{-- <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar - $transaksi_grosir->harga_total) }}</td> --}}
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar - ($transaksi_grosir->nego_total?$transaksi_grosir->nego_total:$transaksi_grosir->harga_total) - $transaksi_grosir->ongkos_kirim) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Subtotal</th>
                                    <th class="text-left">Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_grosir as $i => $relasi)
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah">{{ $relasi->jumlah }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->subtotal / $relasi->jumlah) }}</td>
                                    <td class="text-right {{ $relasi->nego != null ? 'text-success' : '' }}">{{ \App\Util::duit0($relasi->nego != null ? $relasi->nego : $relasi->subtotal) }}</td>
                                    @if ($relasi->bonuses != null)
                                    <td>
                                        @foreach ($relasi->bonuses as $j => $rb)
                                            {{ $rb['jumlah'] }} {{ $rb['bonus']->nama }}
                                            @if ($j != count($relasi->bonuses) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_grosir->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th style="border-top: none;">Kode Transaksi</th>
                                    <td style="border-top: none;">{{ $transaksi_grosir->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Total Harga</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->harga_total) }}</td>
                                </tr>
                                <tr>
                                    <th>Total Bayar</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->jumlah_bayar) }}</td>
                                </tr>
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->nominal_tunai) }}</td>
                                </tr>
                                <tr>
                                    <th>No. Debit</th>
                                    <td>
                                        @if (!empty($transaksi_grosir->no_debit))
                                        {{ $transaksi_grosir->no_debit }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nominal Debit</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->nominal_debit) }}</td>
                                </tr>
                                <tr>
                                    <th>No. Cek</th>
                                    <td>
                                        @if (!empty($transaksi_grosir->no_cek))
                                        {{ $transaksi_grosir->no_cek }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->nominal_cek) }}</td>
                                </tr>
                                <tr>
                                    <th>No. BG</th>
                                    <td>
                                        @if (!empty($transaksi_grosir->no_bg))
                                        {{ $transaksi_grosir->no_bg }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nominal BG</th>
                                    <td>Rp {{ \App\Util::duit0($transaksi_grosir->nominal_bg) }}</td>
                                </tr>
                                <tr>
                                    <th>Kasir</th>
                                    <td>{{ $transaksi_grosir->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Item</th>
                                    <th>Jumlah (pcs)</th>
                                    <th>Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_grosir as $i => $relasi)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td>{{ $relasi->jumlah }}</td>
                                    <td>{{ $relasi->jumlah_bonus }} pcs {{ $relasi->item->bonus->nama }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'PO Penjualan berhasil dibayar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'kredit')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Kredit berhasil dicairkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tabel-item').DataTable();

        var satuans = null;
        var relasi_transaksi_grosir = null;

        $(document).ready(function() {
            var url = "{{ url('transaksi-grosir') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            
            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            relasi_transaksi_grosir = '{{ $relasi_transaksi_grosir }}';
            relasi_transaksi_grosir = relasi_transaksi_grosir.replace(/&quot;/g, '"');
            relasi_transaksi_grosir = JSON.parse(relasi_transaksi_grosir);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var v_jumlah = [];
                var v_satuan = [];
                var text_satuan = '';

                for (var i = 0; i < relasi_transaksi_grosir.length; i++) {
                    if (relasi_transaksi_grosir[i].id == id) {
                        relasi = relasi_transaksi_grosir[i];
                        break;
                    }
                }

                var item = relasi.item;
                var jumlah = relasi.jumlah;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var konversi = satuan.konversi;
                    if (jumlah / konversi >= 1) {
                        text_satuan += Math.floor(jumlah / konversi);
                        text_satuan += ' ' + satuan.satuan.kode;
                        if (i != item.satuan_pembelians.length - 1) text_satuan += '<br>';
                        jumlah = jumlah % konversi;
                    }
                }

                $(el).find('#jumlah').html(text_satuan);
            });
        });

    </script>
@endsection
