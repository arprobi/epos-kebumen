@extends('layouts.admin')

@section('title')
    <title>EPOS | Item Masuk</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Transaksi Eceran</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
				<div class="col-sm-4 col-xs-12">
			        <form method="post" action="{{ url('transaksi') }}" class="form-horizontal">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group col-sm-8 col-xs-12">
							<label class="control-label">Item</label>
							<select name="item_id" class="select2_single form-control">
								<option>Pilih Item</option>
								@foreach($items as $item)
								<option value="{{$item->id}}">{{$item->nama}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-12">
							<label class="control-label">Jumlah</label>
							<input type="text" name="jumlah" class="form-control">
						</div>
						<div class="form-group col-sm-8" style="margin-bottom: 0;">
							<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Tambah</span>
							</button>
							<button class="btn btn-sm btn-default" id="btnReset" type="button">
								<i class="fa fa-refresh"></i> Reset
							</button>
						</div>
					</form>
				</div>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Suplier berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Suplier gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Suplier berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Suplier gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Suplier berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Suplier gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script>
      $(document).ready(function() {
        $(".select2_single").select2({
          allowClear: true
        });
      });
    </script>
@endsection

