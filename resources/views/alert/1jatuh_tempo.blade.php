@extends('layouts.admin')

@section('title')
    <title>EPOS | Jatuh Tempo Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnBayar {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Hutang Dagang melewati Jatuh Tempo</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Transaksi</th>
                            <th>Pemasok</th>
                            <th>Jumlah Hutang</th>
                            <th>Sisa Hutang</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list as $num => $hutang)
                        <tr id="{{$hutang->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>
                                <a href="{{ url('transaksi-pembelian/'.$hutang->id) }}">{{ $hutang->kode_transaksi}}</a>
                            </td>
                            <td>{{ $hutang->suplier->nama }}</td>
                            <td class="text-right">{{ \App\Util::ewon($hutang->utang) }}</td>
                            <td class="text-right">{{ \App\Util::ewon($hutang->sisa_utang) }}</td>
                            <td td class="text-center">
                                <a href="{{ url('hutang/show/'.$hutang->id) }}" class="btn btn-xs btn-primary" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Bayar Hutang">
                                    <i class="fa fa-money"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'lunas')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Hutang berhasil lunas!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableItem').DataTable();
        $('#tableRiwayat').DataTable();
    </script>

@endsection
