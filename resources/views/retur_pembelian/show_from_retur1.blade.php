@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Retur Transaksi Pembelian</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnKembali {
			margin-bottom: 0;
		}
		.no-border {
			border: none;
		}
        .dataTables_filter {
            width: 100%;
        }
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="row">
                    <div class="col-md-10">
                        <h2 id="formSimpanTitle">Detail Retur Pembelian</h2>
                        <span id="kodeReturTitle"></span>
                    </div>
                    <div class="col-md-2 pull-right">
                        <a href="{{ url('retur-pembelian') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $retur_pembelian->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <tr>
                                    <th>Kode Retur</th>
                                    <td style="width: 60%;">{{ $retur_pembelian->kode_retur }}</td>
                                </tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->kode_transaksi }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Suplier</th>
                                    <td style="width: 60%;">{{ $transaksi_pembelian->suplier->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $retur_pembelian->user->nama }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                            	@if ($transaksi_pembelian->utang != null && $transaksi_pembelian->utang > 0)
                                <tr>
                                    <th>Hutang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->utang) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_tunai != null && $transaksi_pembelian->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_transfer != null)
                                <tr>
                                    <th>No Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_transfer != null && $transaksi_pembelian->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_id != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank->nama_bank }} [{{ $transaksi_pembelian->bank->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_cek != null)
                                <tr>
                                    <th>No Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_cek != null && $transaksi_pembelian->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->no_bg != null)
                                <tr>
                                    <th>No BG</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_bg != null && $transaksi_pembelian->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_bg) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->bank_id != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->bank->nama_bank }} [{{ $transaksi_pembelian->bank->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->kredit_id != null)
                                <tr>
                                    <th>No Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ $transaksi_pembelian->kredit->nomor }} [{{ $transaksi_pembelian->kredit->validasi }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_pembelian->nominal_kredit != null && $transaksi_pembelian->nominal_kredit > 0)
                                <tr>
                                    <th>Nominal Kredit</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->nominal_kredit) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Harga Total</th>
                                	@if ($transaksi_pembelian->harga_total != null && $transaksi_pembelian->harga_total > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total) }}</td>
                                    @else
                                    <td>-</td>
                                	@endif
                                </tr>

                                <tr>
                                    <th>Ongkos Kirim</th>
                                	@if ($transaksi_pembelian->ongkos_kirim != null && $transaksi_pembelian->ongkos_kirim > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->ongkos_kirim) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                	@endif
                                </tr>

                                <tr>
                                    <th>Jumlah Bayar</th>
                                	@if ($transaksi_pembelian->jumlah_bayar != null && $transaksi_pembelian->jumlah_bayar > 0)
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->jumlah_bayar) }}</td>
                                    @else
                                    <td class="text-right" style="width: 60%;">-</td>
                                	@endif
                                </tr>

                                <tr>
                                    <th>Kurang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit($transaksi_pembelian->harga_total - $transaksi_pembelian->jumlah_bayar) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-left">HPP</th>
                                    <th class="text-left">PPN</th>
                                    <th class="text-left">Harga</th>
                                    <th class="text-left">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_pembelian as $i => $relasi)
                                <span style="display: none"> {{ $a = $i }} </span>
                                <tr id="{{ $relasi->id }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td class="text-center">
                                        @foreach($hasil[$a] as $x => $jumlah)
                                            <span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 11 * 10) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga / 11) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->harga) }}</td>
                                    <td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection


@section('script')
	<script type="text/javascript">
        $('#tabel-item').DataTable();
        
		$(document).ready(function() {
			var url = "{{ url('transaksi-pembelian') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
