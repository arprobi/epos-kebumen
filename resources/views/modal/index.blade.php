@extends('layouts.admin')

@section('title')
	<title>EPOS | Modal</title>
@endsection

@section('style')
	<style media="screen">
		#btnSimpan {
			margin-top: 10px;
		}
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		textarea {
			min-width: 100%;
			max-width: 48px;
			min-height: 100%;
			max-height: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Tambah Modal</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
				<form method="post" action="{{ url('modal') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="_method" value="post">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Nominal</label>
								<div class="input-group" style="margin-bottom: 0;">
									<div class="input-group-addon">Rp</div>
									<input class="form-control angka" type="text" required="">
									<input class="form-control" type="hidden" name="nominal">
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Kas Tujuan</label>
								<select class="form-control select2_single" id="kas_id" name="kas_id" required="">
									<option value="">Pilih Kas Tujuan</option>
									<option value="tunai">Tunai</option>
									@foreach($banks as $bank)
										<option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{$bank->no_rekening}}]</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Keterangan</label>
								<textarea required="" name="keterangan" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group" style="margin-bottom: 0;">
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> <span>Tambah</span>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- kolom kanan -->
	<div class="col-md-8 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h4 class="pull-right">Modal Saat ini : {{ \App\Util::ewon($akun->kredit) }}</h4>
				<h2>Data Modal</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableModal">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Nominal</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						@foreach($modals as $i => $modal)
						<tr id="{{ $modal->id }}">
							<td>{{ $i+1 }}</td>
							<td class="text-right">{{ $modal->created_at->format('Y-m-d') }}</td>
							<td class="text-right">{{ \App\Util::duit($modal->nominal) }}</td>
							<td class="CapitalFirst">{{ $modal->keterangan }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Modal berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Modal gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Modal berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Modal gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Modal berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Modal gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2_single").select2();
		});
		$('#tableModal').DataTable();
	</script>
@endsection
