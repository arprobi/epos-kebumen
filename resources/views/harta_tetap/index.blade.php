
@extends('layouts.admin')

@section('title')
    <title>EPOS | Tanah dan Bangunan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')


<!-- <div class="row"> -->
    <div class="col-md-6 col-xs-6">
        @if(count($harta_tetap) > 0)
            <div class="row">
                <div class="col-md-12 col-xs-12" id="">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 id="">Tambah Nilai Tanah dan Bangunan</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <div class="pull-right">
                                    <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                                </div>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" id="formSimpanContainer">
                            <form method="post" action="{{ url('harta_tetap') }}" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Kode Transaksi</label>
                                    <input class="form-control" type="text" name="kode_transaksi" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Pilih Akun</label>
                                    <select class="form-control select2_single" name="akun" required="">
                                        <option value="">Pilih Tanah atau Bangunan</option>
                                        @foreach($harta_tetap as $isi)
                                            <option value="{{ $isi->kode }}">{{ $isi->kode }} - {{ $isi->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label">Dengan Kas</label>
                                    <select class="form-control select2_single" name="sumber_kas" required="">
                                        <option value="">Pilih Kas</option>
                                        @foreach($kases as $kas)
                                            <option value="{{ $kas->kode }}">{{ $kas->kode }} - {{ $kas->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group  col-md-12 col-md-offset-1 pull-right" id="BankContainer1">
                                    <label class="control-label">Pilih Rekening Bank</label>
                                    <select name="bank_tujuan" class="select2_single form-control">
                                        <option value="">Pilih Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nominal</label>
                                    <input class="form-control angka" type="text" name="nominal_">
                                    <input class="form-control" type="hidden" name="nominal">
                                </div>
                                <div class="form-group bangunan">
                                    <label class="control-label">Nilai Residu</label>
                                    <input class="form-control angka" type="text" name="residu_">
                                    <input class="form-control" type="hidden" name="residu">
                                </div>
                                <div class="form-group bangunan">
                                    <label class="control-label">Umur Ekonomis</label>
                                    <input class="form-control" type="text" name="umur">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control" type="text" name="keterangan">
                                </div>
                                <div class="form-group controls xdisplay_inputx has-feedback">
                                    <label class="control-label">Tanggal</label>
                                    <input type="text"  name="tanggal_" class="form-control has-feedback-left active tanggal-putih" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" readonly="">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <input class="form-control" type="hidden" name="tanggal">
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-success" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12 col-xs-12" id="">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 id="">Tambah Nilai Tanah dan Bangunan</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <div class="pull-right">
                                    <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                                </div>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" id="formSimpanContainer">
                            <form method="post" action="{{ url('tambah_aset') }}" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Kode Transaksi</label>
                                    <input class="form-control" type="text" name="kode_transaksi" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Pilih Akun</label>
                                    <select class="form-control select2_single" name="akun" required="">
                                        <option value="">Pilih Tanah atau Bangunan</option>
                                        @foreach($tambah_aset as $isi)
                                            <option value="{{ $isi->kode }}">{{ $isi->kode }} - {{ $isi->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                                <div class="form-group">
                                    <label class="control-label">Dengan Kas</label>
                                    <select class="form-control select2_single" name="sumber_kas" required="Tidak Boleh Kosong!!">
                                        <option value="">Pilih Kas</option>
                                        @foreach($kases as $kas)
                                            <option value="{{ $kas->kode }}">{{ $kas->kode }} - {{ $kas->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group  col-md-12 col-md-offset-1 pull-right" id="BankContainer1">
                                    <label class="control-label">Pilih Rekening Bank</label>
                                    <select name="bank_tujuan" class="select2_single form-control">
                                        <option value="">Pilih Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nominal</label>
                                    <input class="form-control angka" type="text" name="nominal_" required="Tidak Boleh Kosong!!">
                                    <input class="form-control" type="hidden" name="nominal">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control" type="text" name="keterangan" required="Tidak Boleh Kosong!!">
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-success" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="col-md-6 col-xs-6" id="formSimpanContainer">
        <!-- <div class="row"> -->
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Nilai Akun Tanah</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableJenisItem">
                    <thead>
                        <tr>
                            <th>Kode Akun</th>
                            <th>Nama Akun</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $tanah->kode }}</td>
                            <td>{{ $tanah->nama }}</td>
                            <td class="text-right">{{ \App\Util::ewon($tanah->debet) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Nilai Akun Bangunan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBangunan">
                    <thead>
                        <tr>
                            <th>Umur Ekonomis</th>
                            <th>Nominal</th>
                            <th>Nilai Saat Ini</th>
                            <th>Nilai Residu</th>
                        </tr>
                    </thead>
                    <tbody>
                            @if($bangunan->harga != 0)
                            <tr>
                                <td>{{ $bangunan->umur }} tahun</td>
                                <td class="text-right">{{ \App\Util::ewon($bangunan->harga) }}</td>
                                <td class="text-right">{{ \App\Util::ewon($bangunan->nominal) }}</td>
                                <td class="text-right">{{ \App\Util::ewon($bangunan->residu) }}</td>
                            </tr>
                            @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- </div> -->
    </div>
<!-- </div> -->
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Tanah/Bangunan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Tanah/Bangunan berhasil diperbaharui!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableJenisItem').DataTable();
        $('#tableBangunan').DataTable();
        $('#tableCEK').DataTable();
        $('#tableBG').DataTable();
        $('#tableCEK_').DataTable();
        $('#tableBG_').DataTable();

        $(document).ready(function() {
            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#BankContainer1').hide();
            $('#BankContainer1').find('input').val('');
            $('#CekContainer').hide();
            $('#CekContainer').find('input').val('');
            $('#CekContainer1').hide();
            $('#CekContainer1').find('input').val('');
            $('#BGContainer').hide();
            $('#BGContainer').find('input').val('');
            $('#BGContainer1').hide();
            $('#BGContainer1').find('input').val('');
            $('.bangunan').hide();

            $('#single_cal3').daterangepicker({
                  singleDatePicker: true,
                  calender_style: "picker_3"
            }, function(start, end, label) {
                  var date = start.toISOString().substr(0,10);
                  $('input[name="tanggal"]').val(date);
            });

            $('#single_cal3').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                  calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, function(start, end, label) {
                  var date = start.toISOString().substr(0,10);
                  $('input[name="tanggal"]').val(date);
            });
        });

        $(document).on('change', 'select[name="akun"]', function(event) {
            var id = $(this).val();
            if(id=={{\App\Akun::Bangunan}}){
                $('.bangunan').show('fast', function() {
                });
            }else{
                $('.bangunan').hide();
            }
        });

        $(document).on('change', 'select[name="sumber_kas"]', function(event) {
            var id = $(this).val();
            if(id=='111310'){
                $('#BankContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#CekContainer1').hide();
                $('#BGContainer1').hide();
            }else if(id=='111320'){
                $('#CekContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer1').hide();
                $('#BGContainer1').hide();
            }else if(id=='111330'){
                $('#BGContainer1').show('fast', function() {
                    $(this).find('input').trigger('focus');
                });
                $('#BankContainer1').hide();
                $('#CekContainer1').hide();
            }else{
                $('#BankContainer1').hide();
                $('#CekContainer1').hide();
                $('#BGContainer1').hide();
            }
        });

        $(window).on('load', function(event) {

            var url = "{{ url('harta_tetap/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			

            $.get(url, function(data) {
                if (data.harta_tetap === null) {
                    var kode = int4digit(1);
                    var referensi = kode + '/TDB/' + tanggal;
                } else {
                    var referensi = data.harta_tetap.referensi;
                    var mm_transaksi = referensi.split('/')[2];
                    var yyyy_transaksi = referensi.split('/')[3];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        referensi = kode + '/TDB/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
                        referensi = kode + '/TDB/' + tanggal_transaksi;				}
                }
                $('input[name="kode_transaksi"]').val(referensi);
            });
        });
    </script>
@endsection
    