@extends('layouts.admin')

@section('title')
@if($piutang_lain->sisa > 0)
    <title>EPOS | Detail Pembayaran Piutang Lain-Lain</title>
@else
	<title>EPOS | Data Riwayat Pembayaran Piutang Lain-Lain</title>
@endif
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		@if($piutang_lain->sisa > 0)
			<div class="col-md-4">
				<div class="x_panel">
				<div class="x_title">
					<h2 id="formSimpanTitle">Pembayaran Piutang</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content" id="formSimpanContainer">
			        <form method="post" action="{{ url('bayar_piutang_lain') }}" class="form-horizontal">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label">Kode Transaksi</label>
							<input class="form-control" type="text" name="kode_transaksi" readonly>
						</div>
						<div class="form-group">
							<label class="control-label">Nama</label>
							<input class="form-control" type="text" name="nama" disabled value="{{ $piutang_lain->nama }}">
						</div>
						<div class="form-group">
							<label class="control-label">Jumlah Dibayar</label>
							<input class="form-control angka" type="text" name="nominal_" required="">
							<input class="form-control" type="hidden" name="nominal">
						</div>
						<div class="form-group">
							<label class="control-label">Masuk ke Kas</label>
							<div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
								<div class="btn-group" role="group">
									<button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
								</div>
								<div class="btn-group" role="group">
									<button type="button" id="btnBank" class="btn btn-default">Bank</button>
								</div>
						{{-- 		<div class="btn-group" role="group">
									<button type="button" id="btnGaji" class="btn btn-default">Potong Gaji</button>
								</div> --}}
							</div>
						</div>
						<div class="form-group" id="BankContainer">
							<div class="form-group">
								<label class="control-label">Pilih Rekening Bank</label>
								<select name="bank" id="bank" class="select2_single form-control">
									<option value="" id="default">Pilih Bank</option>
									@foreach($banks as $bank)
									<option value="{{ $bank->id }}">{{ $bank->nama_bank}} - {{$bank->no_rekening}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group" id="inputNoTransferContainer">
								<label class="control-label">Nomor Transfer</label>
								<input class="form-control" id="inputNoTransfer" type="text" name="no_transfer">
							</div>
						</div>
							<input type="hidden" name="kas" />
							<input type="hidden" name="karyawan" value="{{ $piutang_lain->karyawan }}" />
							<input type="hidden" name="nama" value="{{ $piutang_lain->nama }}" />
							<input type="hidden" name="keterangan" value="{{ $piutang_lain->keterangan }}" />
							<input type="hidden" name="piutang_lain_id" value="{{ $piutang_lain->id }}" />
						<div class="form-group" style="margin-bottom: 0;">
							<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Bayar</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		@endif
		</div>
		<div class="col-md-8">
			<div class="x_panel">
				<div class="x_title">
					@if($piutang_lain->sisa > 0)
						<h2>Data Pembayaran Piutang a/n {{ $piutang_lain->nama }}</h2>
					@else
						<h2>Data Riwayat Pembayaran Piutang a/n {{ $piutang_lain->nama }}</h2>
					@endif
					<a href="{{url('piutang_lain')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					@if($piutang_lain->karyawan==1)
						<p class="pull-left"><b>Karyawan</b></p>
					@else
						<p class="pull-left"><b>None Karyawan</b></p>
					@endif
					<p class="pull-right"><b>Sisa Piutang </b>: {{ \App\Util::duit0($piutang_lain->sisa) }}</p>
					<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Pembayaran</th>
								<th>Pembayaran Via</th>
								<th>Nomor Transfer</th>
								<th>Jumlah Dibayar</th>
							</tr>
						</thead>
						<tbody>
							@foreach($bayar as $num => $log)
							<tr id="{{$log->id}}">
								<td>{{ $num+1 }}</td>
								<td>{{ $log->kode_transaksi }}</td>
								<td>{{ $log->created_at}}</td>
								@if ($log->metode_pembayaran=='tunai')
									<td>Tunai</td>
									<td align="center">-</td>
								@elseif ($log->metode_pembayaran=='gaji')
									<td>Gaji</td>
									<td align="center">-</td>
								@else
									<td>Bank</td>
									<td>{{ $log->no_transfer }}</td>
								@endif
								<td align="right">{{ \App\Util::duit0($log->nominal) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil dibayar!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Hutang gagal dibayar!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');
			$('#btnGaji').removeClass('btn-success');
			$('#btnGaji').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
			$('#formSimpanContainer').find('input[name="no_transfer"]').hide();
			btnSimpan();
		});

		$(document).on('click', '#btnGaji', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('gaji');
			$('#formSimpanContainer').find('input[name="no_transfer"]').hide();
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');
			$('#btnGaji').removeClass('btn-success');
			$('#btnGaji').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
			$('#formSimpanContainer').find('input[name="no_transfer"]').show();
			$('#formSimpanContainer > form').append('<input type="hidden" name="no_transfer" />');
			btnSimpan();
		});

		$(document).on('keyup', '#inputNoTransfer', function(event) {
			event.preventDefault();
			var no_transfer = $(this).val();
			$('#formSimpanContainer').find('input[name="no_transfer"]').val(no_transfer);
		});

		$(window).on('load', function(event) {
			var url = "{{ url('bayar_piutang_lain/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			

			$.get(url, function(data) {
				if (data.bayar_piutang_lain === null) {
					var kode = int4digit(1);
					var kode_transaksi = kode + '/BHL/' + tanggal;
				} else {
					var kode_transaksi = data.bayar_piutang_lain.kode_transaksi;
					var mm_transaksi = kode_transaksi.split('/')[2];
					var yyyy_transaksi = kode_transaksi.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
					// console.log(kode_transaksi.split('/')[3]);
					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						kode_transaksi = kode + '/BHL/' + tanggal;
					} else {
						var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
						kode_transaksi = kode + '/BHL/' + tanggal_transaksi;
					}
				}
				// console.log(kode_transaksi);
				$('input[name="kode_transaksi"]').val(kode_transaksi);
			});
		});

		$(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                console.log(bank);
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            }else{
                $('#btnSimpan').prop('disabled', false);
            }
        }
	</script>
@endsection