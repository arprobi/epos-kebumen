@extends('layouts.admin')

@section('title')
    <title>EPOS | Input Jurnal Penyesuaian</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Tambah Jurnal Penyesuaian</h2>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row">
    				<form method="post" action="{{ url('jurnal_penyesuaian') }}" class="form-horizontal" id="formJurnal">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="_method" value="post">
						<div class="row">
							<div class="form-group col-md-6 col-xs-12">
								<label class="control-label">Kode Transaksi</label>
								<input class="form-control" type="text" name="kode_transaksi" readonly>
							</div>
							<div class="form-group col-md-6 col-xs-12">
								<label class="control-label">Keterangan</label>
								<input class="form-control" type="text" name="keterangan">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="control-label">Isi Jurnal</label>
									<button class="btn btn-sm btn-success pull-right" id="tambah">
										<i class="fa fa-plus"></i> <span>Tambah</span>
									</button>
									<table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;">
										<thead>
											<tr>
												<th></th>
												<th width="20%">Akun</th>
												<th width="20%">Inputan Debit</th>
												<th width="20%">Inputan Kredit</th>
												<th width="20%">Debit</th>
												<th width="20%">Kredit</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												
											</tr>
										</tbody>
									</table>
								</div>
								<div class="form-group" style="margin-bottom: 0;">
									<button class="btn btn-sm btn-success pull-right" id="btnSimpan" type="submit">
										<i class="fa fa-save"></i> <span>Simpan</span>
									</button>
									<span class="pull-right" id="notif" style="padding-right: 20px; padding-top: 5px"></span>
								</div>
							</div>
						</div>
					</form>
    			</div>
      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();
		var akuns = [];

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$(".select2_single").select2({
				allowClear: true
			});

			akuns = '{{ $akuns }}';
			akuns = akuns.replace(/&quot;/g, '"');
			akuns = JSON.parse(akuns);
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
		});

		$(window).on('load', function(event) {
			var url = "{{ url('jurnal_penyesuaian/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			

			$.get(url, function(data) {
				if (data.jurnal === null) {
					var kode = int4digit(1);
					var referensi = kode + '/JLP/' + tanggal;
				} else {
					var referensi = data.jurnal.referensi;
					var mm_transaksi = referensi.split('/')[2];
					var yyyy_transaksi = referensi.split('/')[3];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;
					if (tanggal != tanggal_transaksi) {
						var kode = int4digit(1);
						referensi = kode + '/JLP/' + tanggal;
					} else {
						var kode = int4digit(parseInt(referensi.split('/')[0]) + 1);
						referensi = kode + '/JLP/' + tanggal_transaksi;				}
				}
				$('input[name="kode_transaksi"]').val(referensi);
			});
		});

		$(document).on('click', '#tambah', function(event) {
			event.preventDefault();
			var select_akun = '<select name="akun[]" id="pilihAkun" class="select2_single form-control">'+
								'<option id="default">Pilih Akun</option>';
			for(var i=0; i<akuns.length; i++){
				select_akun += '<option value="'+ akuns[i].kode +'">['+ akuns[i].kode +'] '+ akuns[i].nama +'</option>';
			}
			select_akun += '</select>';

			$('#tabelKeranjang').find('tbody').append(
				'<tr>'+
					'<td>'+
						'<i class="fa fa-times" title="Hapus Item" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
					'</td>'+
					'<td>'+
						select_akun+
					'</td>'+
					'<td class="text-right">'+
						'<input class="form-control debit" id="debit" type="text" name="debit_[]" value="0,000">'+
						'<input class="form-control" type="hidden" name="debit[]" value="0.000">'+
					'</td>'+
					'<td class="text-right">'+
						'<input class="form-control kredit" id="kredit" type="text" name="kredit_[]" value="0,000">'+
						'<input class="form-control" type="hidden" name="kredit[]" value="0.000">'+
					'</td>'+
					'<td class="text-right" style="vertical-align: middle;" id="debit_t">'+ '0,000' + '</td>'+
					'<td class="text-right" style="vertical-align: middle;" id="kredit_t">'+ '0,000' + '</td>'+
				'</tr>'
			);
		});

		$(document).on('change', 'select[name="akun[]"]', function(event) {
		    // console.log($(this).val());
		    if($(this).val() === '{{ \App\Akun::KasBank }}'){
		    	$(this).parent().append(
		    		`<select name="bank" id="pilihAkun" class="select2_single form-control">
						<option id="default">Pilih Bank</option>
						@foreach ($banks as $bank)
						<option value="{{ $bank->id }}">[{{ $bank->nama_bank }}] {{ $bank->no_rekening }}</option>
						@endforeach
					</select>`
		    	);
		    }
		});

		$(document).on('click', '#remove', function(event) {
			event.preventDefault();
			var tr = $(this).parents('tr');
			tr.remove();
			$('#formJurnal').find('#append-section').find('input[debit]').remove();
			akuns = '{{ $akuns }}';
			akuns = akuns.replace(/&quot;/g, '"');
			akuns = JSON.parse(akuns);
		});

		function cekDK() {
			var debit_total = 0;
			var kredit_total = 0;
			$('input[name="debit[]"]').each(function(index, el){
				var debit = $(el).val();
				debit_total += parseInt(debit);
			});

			$('input[name="kredit[]"]').each(function(index, el){
				var kredit = $(el).val();
				kredit_total += parseInt(kredit);
			});

			if(debit_total === kredit_total){
				$('#btnSimpan').removeAttr('disabled');
				$("#notif").text("");
			}else{
				$('#btnSimpan').attr('disabled','disabled');
				$("#notif").text("Debit Kredit Tidak Seimbang");
				$("#notif").css('color', 'red');
			}
		}

		$(document).on('keyup', '.debit', function(event) {
			// // event.preventDefault();
			// var debit = parseFloat($(this).val().replace(/\D/g, ''), 10)/1000;
			var nominal_t = $(this).val().split(',');
			var nominal_0 = parseFloat(nominal_t[0].replace(/\D/g, ''), 10);
			var nominal = parseFloat(nominal_0 +'.' + nominal_t[1]);
			$(this).next().val(nominal);
			
			var nilais = $(this).val().split(',');
			var nilai = 0;
			if(nilais.length > 1){
				nilai = $(this).val();
				if(nilais[0].length < 1){
					nilais[0] = 0;
					$(this).val(nilais[0] + ',' + nilais[1]);
				}
				else if(nilais[1].length > 3){
					nilai = $(this).val().slice(0,-1);
					$(this).val(nilai);
				
				}else if(nilais[1].length < 3){
					nilai = parseFloat(nilai.replace(',', '.'));
					nilai = nilai.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
					nilai = nilai.replace('.', '');
					$(this).val(nilai);
				}
			}else{
				nilai = $(this).val();
				var bel_koma = nilai.substr(nilai.length -3);
				var depan_koma = nilai.substr(0, nilai.length -3);

				$(this).val(depan_koma + ',' + bel_koma);

			}
			var nilai_h = parseFloat(nilai.replace(',', '.'));
			var nilai_v = nilai_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
			console.log(nilai_v);
			$(this).parents('tr').first().find('#debit_t').text('Rp'+nilai_v);
			
			cekDK();
		});

		$(document).on('keyup', '.kredit', function(event) {
			// // event.preventDefault();
			// var debit = parseFloat($(this).val().replace(/\D/g, ''), 10)/1000;
			var nominal_t = $(this).val().split(',');
			var nominal_0 = parseFloat(nominal_t[0].replace(/\D/g, ''), 10);
			var nominal = parseFloat(nominal_0 +'.' + nominal_t[1]);
			$(this).next().val(nominal);
			
			var nilais = $(this).val().split(',');
			var nilai = 0;
			if(nilais.length > 1){
				nilai = $(this).val();
				if(nilais[0].length < 1){
					nilais[0] = 0;
					$(this).val(nilais[0] + ',' + nilais[1]);
				}
				else if(nilais[1].length > 3){
					nilai = $(this).val().slice(0,-1);
					$(this).val(nilai);
				
				}else if(nilais[1].length < 3){
					nilai = parseFloat(nilai.replace(',', '.'));
					nilai = nilai.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
					nilai = nilai.replace('.', '');
					$(this).val(nilai);
				}
			}else{
				nilai = $(this).val();
				var bel_koma = nilai.substr(nilai.length -3);
				var depan_koma = nilai.substr(0, nilai.length -3);

				$(this).val(depan_koma + ',' + bel_koma);

			}
			var nilai_h = parseFloat(nilai.replace(',', '.'));
			var nilai_v = nilai_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
			console.log(nilai_v);
			$(this).parents('tr').first().find('#kredit_t').text('Rp'+nilai_v);
			
			cekDK();
		});
	</script>
@endsection


