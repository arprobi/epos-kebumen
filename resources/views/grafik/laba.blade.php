@extends('layouts.admin')

@section('title')
    <title>EPOS | Grafik Beban</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnHistory, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .form-control {
            height: 34px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <label class="control-label">
                    Pilih Tahun
                </label>
                <div class="form-group">
                    <select name="pilihTahun" id="pilihTahun" class="form-control select2_single">
                        <option value="">Pilih Tahun</option>
                        @for ($i = intval(date('Y')); $i >= 2017; $i--)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <label class="control-label">
                    Pilih Bulan
                </label>
                <div class="form-group">
                    <select name="pilihBulan" id="pilihBulan" class="form-control select2_single">
                        <option value="">Pilih Bulan</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <label class="control-label">
                    Pilih Rentang Tanggal
                </label>
                <div class="form-group">
                    <input name="pilihRentangTanggal" id="pilihRentangTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Rentang Tanggal" readonly="">
                </div>
            </div>
            <div class="col-md-3">
                <label class="control-label">
                    Pilih Tanggal
                </label>
                <div class="form-group">
                    <input name="pilihTanggal" id="pilihTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Tanggal" readonly="">
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Grafik Laba Rugi</h2>
                <div class="clearfix"></div>
            </div>
            <div id="grafikContainer" class="x_content">
                <canvas id="lineChart"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        Chart.defaults.global.legend = {
            enabled: false
        };

        $(document).ready(function() {
            $('#pilihTahun').val('');
            $('#pilihBulan').val('');
            // $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');

            $(".select2_single").select2({
            });
        });

        $(document).on('change', '#pilihTahun', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');

            var tahun = $(this).val();
            if (tahun != '') {
                var url = '{{ url("grafik/laba/tahun") }}' + '/' + tahun;
                $('#spinner').show();
                $.get(url, function(data) {
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            }

        });

        $(document).on('change', '#pilihBulan', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').val('');
            $('#pilihTanggal').val('');

            var tahun = $('#pilihTahun').val();
            var bulan = $(this).val();
            if (tahun != '' && bulan != '') {
                $('#spinner').show();
                var url = '{{ url("grafik/laba/tahun") }}' + '/' + tahun + '/bulan/' + bulan;
                $.get(url, function(data) {
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            }
        });

        $(document).on('focus', '#pilihTanggal', function(event) {
            event.preventDefault();

            $('#pilihTanggal').daterangepicker({
                autoApply: true,
                calender_style: "picker_2",
                showDropdowns: true,
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: true
            }, function(start) {
                $('#pilihTahun').val('');
                $('#pilihBulan').val('');
                $('#pilihRentangTanggal').val('');

                var tanggal = (start.toISOString()).substring(0,10);
                var url = '{{ url("grafik/laba/tanggal") }}' + '/' + tanggal;
                console.log(url);
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            });
        });

        $(document).on('focus', '#pilihRentangTanggal', function(event) {
            event.preventDefault();

            $('#pilihRentangTanggal').daterangepicker({
                autoApply: true,
                calender_style: "picker_2",
                showDropdowns: true,
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: false
            }, function(start, end) {
                $('#pilihTahun').val('');
                $('#pilihBulan').val('');
                $('#pilihTanggal').val('');

                // var tanggal = (start.toISOString()).substring(0,10);
                var awal = (start.toISOString()).substring(0,10);
                var akhir = (end.toISOString()).substring(0,10);

                var url = '{{ url("grafik/laba/rentang") }}' + '/' + awal + '/' + akhir;
                // console.log(url);
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });

                    $('#spinner').hide();
                });

            });
        });

    </script>
@endsection
