@extends('layouts.admin')

@section('title')
    <title>EPOS | Grafik Wilayah</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnHistory, #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .form-control {
            height: 34px;
        }
        #pilihRentangTanggal,
        #pilihTanggal {
            height: 38px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="control-label">
                    Pilih Provinsi
                </label>
                <div class="form-group">
                    <select name="pilihProvinsi" id="pilihProvinsi" class="form-control select2_single">
                        <option value="">Pilih Provinsi</option>
                        <option value="semua">Semua Provinsi</option>
                        @foreach ($provinsis as $i => $provinsi)
                            <option value="{{ $provinsi->id }}">{{ $provinsi->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label class="control-label">
                    Pilih Kabupaten
                </label>
                <div class="form-group">
                    <select name="pilihKabupaten" id="pilihKabupaten" class="form-control select2_single">
                        <option value="">Pilih Kabupaten</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label class="control-label">
                    Pilih Kecamatan
                </label>
                <div class="form-group">
                    <select name="pilihKecamatan" id="pilihKecamatan" class="form-control select2_single">
                        <option value="">Pilih Kecamatan</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Grafik Wilayah</h2>
                <div class="clearfix"></div>
            </div>
            <div id="grafikContainer" class="x_content">
                <canvas id="lineChart"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        Chart.defaults.global.legend = {
            enabled: false
        };

        $(document).ready(function() {
            $(".select2_single").select2({
            });

            $('#pilihProvinsi').val('');
            $('#pilihKabupaten').val('');
            $('#pilihKecamatan').val('');
        });

        $(document).on('change', '#pilihProvinsi', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if (id != 'semua') {
                var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';

                var $select = $('#pilihKabupaten');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

                var $select_kec = $('#pilihKecamatan');
                $select_kec.empty();
                $select_kec.append($('<option value="">Pilih Kecamatan</option>'));

                $.get(url, function(data) {
                    var kabupatens = data.kabupaten;
                    for (var i = 0; i < kabupatens.length; i++) {
                        var kabupaten = kabupatens[i];
                        $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                    }
                });

                if (id != '') {
                    var url = '{{ url("grafik/wilayah/provinsi") }}' + '/' + id;
                    $('#spinner').show();
                    $.get(url, function(data) {
                        // console.log(data);
                        $('#grafikContainer').find('#lineChart').remove();
                        var ctx = $('<canvas id="lineChart"></canvas>');
                        $('#grafikContainer').append(ctx);
                        var lineChart = new Chart(ctx, {
                            type: 'line',
                                data: {
                                labels: data.labels,
                                datasets: [{
                                    label: "",
                                    backgroundColor: "rgba(3, 88, 106, 0.3)",
                                    borderColor: "rgba(3, 88, 106, 0.70)",
                                    pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                    pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                    pointHoverBackgroundColor: "#fff",
                                    pointHoverBorderColor: "rgba(151,187,205,1)",
                                    pointBorderWidth: 1,
                                    data: data.datas
                                }]
                            },
                        });
                        $('#spinner').hide();
                    });
                }
            } else if (id = 'semua'){
                var url = '{{ url("grafik/wilayah/provinsi") }}';
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            }
        });

        $(document).on('change', '#pilihKabupaten', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kecamatan.json';

            var $select = $('#pilihKecamatan');
            $select.empty();
            $select.append($('<option value="">Pilih Kecamatan</option>'));

            $.get(url, function(data) {
                var kecamatans = data.kecamatan;
                for (var i = 0; i < kecamatans.length; i++) {
                    var kecamatan = kecamatans[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
                }
            });

            if (id != '') {
                var url = '{{ url("grafik/wilayah/kabupaten") }}' + '/' + id;
                $('#spinner').show();
                $.get(url, function(data) {
                    // console.log(data);
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            }
        });

        $(document).on('change', '#pilihKecamatan', function(event) {
            event.preventDefault();

            var id = $(this).val();
            console.log(id);
            if (id != '') {
                var url = '{{ url("grafik/wilayah/kecamatan") }}' + '/' + id;
                $('#spinner').show();
                $.get(url, function(data) {
                    console.log(data);
                    $('#grafikContainer').find('#lineChart').remove();
                    var ctx = $('<canvas id="lineChart"></canvas>');
                    $('#grafikContainer').append(ctx);
                    var lineChart = new Chart(ctx, {
                        type: 'line',
                            data: {
                            labels: data.labels,
                            datasets: [{
                                label: "",
                                backgroundColor: "rgba(3, 88, 106, 0.3)",
                                borderColor: "rgba(3, 88, 106, 0.70)",
                                pointBorderColor: "rgba(3, 88, 106, 0.70)",
                                pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(151,187,205,1)",
                                pointBorderWidth: 1,
                                data: data.datas
                            }]
                        },
                    });
                    $('#spinner').hide();
                });
            }
        });

    </script>
@endsection
