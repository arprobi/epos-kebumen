@extends('layouts.admin')

@section('title')
    <title>EPOS | Wilayah</title>
@endsection

@section('style')
    <style media="screen">
        .btnSimpan, .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Desa</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanDesaContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/desa')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-2">
                                     <select name="provinsi" class="form-control select2_single" id="prov_des" style="height: 39px;" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kabupaten" class="form-control select2_single" id="kab_des" style="height: 39px;" required="">
                                        <option value="">Pilih Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kecamatan" class="form-control select2_single" id="kec_des" style="height: 39px;" required="">
                                        <option value="">Pilih Kecamatan</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <input type="text" name="desa" class="form-control" placeholder="Desa" style="height: 39px;" required="">
                                </div>
                                <div class="form-group col-xs-3 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>

                            <div id="desaHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableDesa">
                                    <thead>
                                        <tr>
                                            <!-- <th width="5%">No</th>
                                            <th width="19%">Provinsi</th>
                                            <th width="19%">Kabupaten</th>
                                            <th width="19%">Kecamatan</th>
                                            <th width="19%">Desa</th>
                                            <th width="19%">Aksi</th> -->
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten</th>
                                            <th>Kecamatan</th>
                                            <th>Desa</th>
                                            <th>Operator</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($desas as $nums => $desa)
                                        <tr id="{{ $desa->id }}">
                                            <td> {{ $nums+1 }} </td>
                                            <td data-provin="{{ $desa->kecamatan->kabupaten->provinsi->id }}"> {{ $desa->kecamatan->kabupaten->provinsi->nama }} </td>
                                            <td data-paten="{{ $desa->kecamatan->kabupaten->id }}"> {{ $desa->kecamatan->kabupaten->nama }} </td>
                                            <td data-camat="{{ $desa->kecamatan->id }}"> {{ $desa->kecamatan->nama }} </td>
                                            <td> {{ $desa->nama }} </td>
                                            @if ($desa->user_id != null)
                                            <td> {{ $desa->user->nama }} </td>
                                            @else
                                            <td> - </td>
                                            @endif
                                            <td class="tengah-h"> 
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_desa" data-toggle="tooltip" data-placement="top" title="Ubah Desa">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_desa" data-toggle="tooltip" data-placement="top" title="Hapus Desa">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 25px">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kecamatan</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanKecamatanContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/kecamatan')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-2">
                                     <select name="provinsi" class="form-control select2_single" id="prov_kab" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                     <select name="kabupaten" class="form-control select2_single" id="kabs" required="">
                                        <option value="">Pilih Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" style="height: 39px;" id="camat_" required="">
                                </div>
                                <div class="form-group col-xs-2 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="kecamatanHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableKecamatan">
                                    <thead>
                                        <tr>
                                            <!-- <th width="10%">No</th>
                                            <th width="20%">Provinsi</th>
                                            <th width="20%">Kabupaten</th>
                                            <th width="20%">Kecamatan</th>
                                            <th width="30%">Aksi</th> -->
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten</th>
                                            <th>Kecamatan</th>
                                            <th>Operator</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($kecamatans as $nums => $kecamatan)
                                        <tr id="{{ $kecamatan->id }}">
                                            <td> {{ $nums+1 }} </td>
                                            <td data-prova="{{ $kecamatan->kabupaten->provinsi->id }}"> {{ $kecamatan->kabupaten->provinsi->nama }} </td>
                                            <td data-kab="{{ $kecamatan->kabupaten->id }}"> {{ $kecamatan->kabupaten->nama }} </td>
                                            <td> {{ $kecamatan->nama }} </td>
                                            @if ($kecamatan->user_id != null)
                                            <td> {{ $kecamatan->user->nama }} </td>
                                            @else
                                            <td> - </td>
                                            @endif
                                            <td class="tengah-h">  
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_kecamatan" data-toggle="tooltip" data-placement="top" title="Ubah Kecamatan">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_kecamatan" data-toggle="tooltip" data-placement="top" title="Hapus Kecamatan">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 25px">
            <div class="col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kabupaten</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanKabupatenContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/kabupaten')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group col-xs-4">
                                     <select name="provinsi" class="form-control select2_single" required="">
                                        <option value="">Pilih Provinsi</option>
                                        @foreach($provinsis as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-4">
                                    <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" style="height: 39px;" required="">
                                </div>
                                <div class="form-group col-xs-4">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="kabupatenHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>
                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableKabupaten">
                                    <thead>
                                        <tr>
                                            <!-- <th width="5%">No</th>
                                            <th width="30%">Provinsi</th>
                                            <th width="30%">kabupaten</th>
                                            <th width="35%">Aksi</th> -->
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten</th>
                                            <th>Operator</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($kabupatens as $nums => $kabupaten)
                                        <tr id="{{ $kabupaten->id }}">

                                            <td> {{ $nums+1 }} </td>
                                            <td data-prov="{{ $kabupaten->provinsi->id }}"> {{ $kabupaten->provinsi->nama }} </td>
                                            <td> {{ $kabupaten->nama }} </td>
                                            @if ($kabupaten->user_id != null)
                                            <td> {{ $kabupaten->user->nama }} </td>
                                            @else
                                            <td> - </td>
                                            @endif
                                            <td>
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_kabupaten" data-toggle="tooltip" data-placement="top" title="Ubah Kabupaten">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_kabupaten" data-toggle="tooltip" data-placement="top" title="Hapus Kabupaten">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Provinsi</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <div class="pull-right">
                                <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" id="SimpanProvinsiContainer">
                            <form class="form-horizontal" action="{{url('/wilayah/provinsi')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="col-xs-4">
                                    <input type="text" name="provinsi" class="form-control" placeholder="Provinsi" style="height: 39px;" required="">
                                </div>
                                <div class="col-xs-4 pull-right">
                                    <button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </form>
                            <div id="provinsiHapusContainer" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>

                            <div class="col-xs-12 x-content" style="margin-top: 20px">
                                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableProvinsi">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Operator</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($provinsis as $nums => $provinsi)
                                        <tr id="{{ $provinsi->id }}">
                                            <td> {{ $nums+1 }} </td>
                                            <td> {{ $provinsi->nama }} </td>
                                            @if ($provinsi->user_id != null)
                                            <td> {{ $provinsi->user->nama }} </td>
                                            @else
                                            <td> - </td>
                                            @endif
                                            <td>  
                                            <button class="btn btn-xs btn-warning btnUbah" id="ubah_provinsi" data-toggle="tooltip" data-placement="top" title="Ubah Provinsi">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_provinsi" data-toggle="tooltip" data-placement="top" title="Hapus Provinsi">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableProvinsi').DataTable();
        $('#tableKabupaten').DataTable();
        $('#tableKecamatan').DataTable();
        $('#tableDesa').DataTable();

        $(document).ready(function() {
            $(".select2_single").select2();
        });

        var provinsi_id = '';
        var kabupaten_id = '';
        var kabupaten_x = '';
        var kecamatan_id = '';
        var kecamatan_x = '';
        var sata =  '';

        $(document).on('click', '#ubah_provinsi', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi = $tr.find('td').first().next().text();

            $('#SimpanProvinsiContainer').find('input[name="provinsi"]').val(provinsi);
            $('#SimpanProvinsiContainer').find('form').attr('action', '{{ url("wilayah/provinsi") }}' + '/' + id);
            $('#SimpanProvinsiContainer').find('input[name="_method"]').val('put');
            $('#SimpanProvinsiContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_provinsi', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#provinsiHapusContainer').find('form').attr('action', '{{ url("wilayah/provinsi") }}' + '/' + id);
                    $('#provinsiHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('click', '#ubah_kabupaten', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi_id = $tr.find('td').first().next().data('prov');
            var kabupaten = $tr.find('td').first().next().next().text();

            $('#SimpanKabupatenContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            $('#SimpanKabupatenContainer').find('input[name="kabupaten"]').val(kabupaten);
            $('#SimpanKabupatenContainer').find('form').attr('action', '{{ url("wilayah/kabupaten") }}' + '/' + id);
            $('#SimpanKabupatenContainer').find('input[name="_method"]').val('put');
            $('#SimpanKabupatenContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_kabupaten', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    // Confirmed
                    $('#kabupatenHapusContainer').find('form').attr('action', '{{ url("wilayah/kabupaten") }}' + '/' + id);
                    $('#kabupatenHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('click', '#ubah_kecamatan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var provinsi_id = $tr.find('td').first().next().data('prova');
            kabupaten_id = $tr.find('td').first().next().next().data('kab');
            var kecamatan = $tr.find('td').first().next().next().next().text();
            // $('#SimpanKabupatenContainer').find('select[name="provinsi"]')
            $('#SimpanKecamatanContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            var url = "{{ url('wilayah') }}" + '/' + provinsi_id + '/kabupaten.json';

            var $select = $('#kabs');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kabupaten_x = data.kabupaten;
                }
            });

            for (var i = 0; i < kabupaten_x.length; i++) {
                var kabupaten = kabupaten_x[i];
                if(kabupaten.id == kabupaten_id){
                    $select.append($('<option value="' + kabupaten.id + '" selected>' + kabupaten.nama + '</option>'));
                }else{
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            }

            $('#SimpanKecamatanContainer').find('input[name="kecamatan"]').val(kecamatan);
            $('#SimpanKecamatanContainer').find('form').attr('action', '{{ url("wilayah/kecamatan") }}' + '/' + id);
            $('#SimpanKecamatanContainer').find('input[name="_method"]').val('put');
            $('#SimpanKecamatanContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('change', '#prov_kab', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';

            var $select = $('#kabs');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            });

            $('#SimpanKecamatanContainer').find('input[name=kecamatan]').val('');
        });

        $(document).on('click', '#hapus_kecamatan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                // console.log(isConfirm);
                if (isConfirm) {
                    // Confirmed
                    $('#kecamatanHapusContainer').find('form').attr('action', '{{ url("wilayah/kecamatan") }}' + '/' + id);
                    $('#kecamatanHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

        $(document).on('change', '#prov_des', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var $select = $('#kab_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));
            var $select_kec = $('#kec_des');
                $select_kec.empty();
                $select_kec.append($('<option value="">Pilih kecamatan</option>'));

            $('#SimpanDesaContainer').find('input[name="desa"]').val('');
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';
            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                // console.log(kabupatens);
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    $select.append($('<option value="' + kabupaten.id + '" >' + kabupaten.nama + '</option>'));
                }
            });
        });

        $(document).on('change', '#kab_des', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var $select = $('#kec_des');
                $select.empty();
                $select.append($('<option value="">Pilih kecamatan</option>'));
            $('#SimpanDesaContainer').find('input[name="desa"]').val('');
            var url = "{{ url('wilayah') }}" + '/' + id + '/kecamatan.json';
            $.get(url, function(data) {
                var kecamatans = data.kecamatan;
                // console.log(kecamatans);
                for (var i = 0; i < kecamatans.length; i++) {
                    var kecamatan = kecamatans[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
                }
            });
        });

        $(document).on('click', '#ubah_desa', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            provinsi_id = $tr.find('td').first().next().data('provin');
            kabupaten_id = $tr.find('td').first().next().next().data('paten');
            kecamatan_id = $tr.find('td').first().next().next().next().data('camat');
            var nama = $tr.find('td').first().next().next().next().next().text();

            // console.log(id, provinsi_id, kabupaten_id, kecamatan_id, nama);

            $('#SimpanDesaContainer').find('select[name="provinsi"]').val(provinsi_id).trigger("change");
            var url = "{{ url('wilayah') }}" + '/' + provinsi_id + '/kabupaten.json';

            var $select = $('#kab_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kabupaten</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kabupaten_x = data.kabupaten;
                }
            });

            for (var i = 0; i < kabupaten_x.length; i++) {
                var kabupaten = kabupaten_x[i];
                if(kabupaten.id == kabupaten_id){
                    $select.append($('<option value="' + kabupaten.id + '" selected>' + kabupaten.nama + '</option>'));
                }else{
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
            }

            var url = "{{ url('wilayah') }}" + '/' + kabupaten_id + '/kecamatan.json';

            var $select = $('#kec_des');
                $select.empty();
                $select.append($('<option value="">Pilih Kecamatan</option>'));

            $.ajax({
                url: url,
                type: 'get',
                async: false,
                success:function(data) {
                    kecamatan_x = data.kecamatan;
                }
            });

            for (var i = 0; i < kecamatan_x.length; i++) {
                var kecamatan = kecamatan_x[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
            }

            $("select[name='kecamatan']").val(kecamatan_id).trigger("change");
            // console.log(nama);
            $('#SimpanDesaContainer').find('input[name="desa"]').val(nama);
            $('#SimpanDesaContainer').find('form').attr('action', '{{ url("wilayah/desa") }}' + '/' + id);
            $('#SimpanDesaContainer').find('input[name="_method"]').val('put');
            $('#SimpanDesaContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapus_desa', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                // console.log(isConfirm);
                if (isConfirm) {
                    // Confirmed
                    $('#desaHapusContainer').find('form').attr('action', '{{ url("wilayah/desa") }}' + '/' + id);
                    $('#desaHapusContainer').find('form').submit();
                } else {
                    // Canceled
                }
            }).catch(function(error) {
                console.log(error);
            });
        });

    </script>
@endsection
