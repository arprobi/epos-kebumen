@extends('layouts.admin')

@section('title')
    <title>EPOS | Edit Konsinyasi Masuk</title>
@endsection

@section('style')
    <style media="screen">
    	.kiri, .kanan {
    		border-bottom: 1px solid #E6E9ED;
    		padding-bottom: 5px;
    	}
    	.kiri, .bawah {
    		padding-left: 0;
    	}
    	.kanan {
    		padding-right: 0;
    	}
    	.bawah {
    		margin-top: 5px;
    	}
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Ubah Konsinyasi Masuk</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="POST" action="{{ url('konsinyasi-masuk') }}/{{ $konsinyasi_masuk->id }}">
		        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		        	<input type="hidden" name="_method" value="put">

					<div class="col-md-6 col-xs-12 kiri">
						<div class="form-group">
							<label class="control-label">Item</label>
							<select class="form-control" id="itemId" name="item_id" value="{{ old('item_id') }}">
								<option value="">Pilih Item</option>
								@foreach ($items as $item)
									@if ($item->id == $konsinyasi_masuk->item->id)
										<option value="{{ $item->id }}" selected="">{{ $item->kode }} : {{ $item->nama }}</option>
									@else
										<option value="{{ $item->id }}">{{ $item->kode }} : {{ $item->nama }}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label class="control-label">Kode Item</label>
							<input class="form-control" type="text" id="kodeItem" name="kode_item" readonly="" value="{{ old('kode_item') }}">
						</div>
						<div class="form-group">
							<label class="control-label">Nama Item</label>
							<input class="form-control" type="text" id="namaItem" name="nama_item" readonly="" value="{{ old('nama_item') }}">
						</div>
					</div>
					<div class="col-md-6 col-xs-12 kanan">
						<div class="form-group">
							<label class="control-label">Suplier</label>
							<select class="form-control" id="suplierId" name="suplier_id" value="{{ old('suplier_id') }}">
								<option value="">Pilih Suplier</option>
								@foreach ($supliers as $suplier)
									@if ($suplier->id == $konsinyasi_masuk->suplier->id)
										<option value="{{ $suplier->id }}" selected="">{{ $suplier->nama }}, {{ $suplier->alamat }}</option>
									@else
										<option value="{{ $suplier->id }}">{{ $suplier->nama }}, {{ $suplier->alamat }}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label class="control-label">Nama</label>
							<input class="form-control" type="text" id="namaSuplier" name="nama_suplier" readonly="" value="{{ old('nama_suplier') }}">
						</div>
						<div class="form-group">
							<label class="control-label">Alamat</label>
							<input class="form-control" type="text" id="alamatSuplier" name="alamat_suplier" readonly="" value="{{ old('alamat_suplier') }}">
						</div>
					</div>
					<div class="col-md-6 col-xs-12 bawah">
						<div class="form-group">
							<label class="control-label">Jumlah</label>
							<input class="form-control" type="text" id="jumlah" name="jumlah" value="{{ $konsinyasi_masuk->jumlah }}">
						</div>
						<div class="form-group">
							<label class="control-label">Harga Dasar</label>
							<input class="form-control" type="text" id="harga_dasar" name="harga_dasar" value="{{ $konsinyasi_masuk->harga_dasar }}">
						</div>

						<a href="{{ url('konsinyasi-masuk') }}" class="btn btn-sm btn-default" id="btnKembali" type="button">
							<i class="fa fa-long-arrow-left"></i> Kembali
						</a>
						<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
							<i class="fa fa-save"></i> Simpan
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#itemId').trigger('change');
			$('#suplierId').trigger('change');

		    var url = "{{ url('konsinyasi-masuk') }}";
		    var a = $('a[href="' + url + '"]');
		    a.parent().addClass('current-page');
		    a.parent().parent().show();
		    a.parent().parent().parent().addClass('active');
		});

		$(document).on('change', '#itemId', function() {
			var id = $(this).val();
			if (id == '') {
				$('#kodeItem').val('');
				$('#namaItem').val('');
			} else {
				var url = "{{ url('konsinyasi_masuk/show') }}" + '/' + id + '/json';
				$.get(url, function(data) {
					var item = JSON.parse(data);
					var kode = item.kode;
					var nama = item.nama;
					$('#kodeItem').val(kode);
					$('#namaItem').val(nama);
				});
			}
		});

		$(document).on('change', '#suplierId', function() {
			var id = $(this).val();
			if (id == '') {
				$('#namaSuplier').val('');
				$('#alamatSuplier').val('');
			} else {
				var url = "{{ url('suplier') }}" + '/' + id + '/json';
				$.get(url, function(data) {
					var suplier = JSON.parse(data);
					var nama = suplier.nama;
					var alamat = suplier.alamat;
					$('#namaSuplier').val(nama);
					$('#alamatSuplier').val(alamat);
				});
			}
		});
	</script>
@endsection
