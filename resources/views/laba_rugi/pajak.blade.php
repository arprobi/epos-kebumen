@extends('layouts.admin')

@section('title')
	<title>EPOS | Laba Rugi</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
		.animated .putih {
			color: white;
		}
		
		table > thead > tr > th {
			text-align: center;
		}

		table > tbody > tr > td.tengah-hv{
			vertical-align: middle;
			text-align: center;	
			background: white;
		}

		table > tbody > tr > td.atasan{
			font-weight: bold;
			/*text-decoration: underline;*/
		}

		table > tbody > tr > td.bawahan{
			border-bottom: 1px solid;
		}		

		table > tbody > tr > td.jumlah{
			font-weight: bold;
			border-top: 1px solid;
		}

		table > tbody > tr > td.money{
			text-align: right;
		}		
	</style>
@endsection

@section('content')
<div class="col-md-8 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<div class="row">
				<div class="col-md-8">
					@if($status==1)
						<h2 id="header_tanggal">Laporan Laba Rugi Bulan {{ \App\Util::tanggal($newAwal->format('d m Y')) }}</h2>
					@elseif($status==2)
						<h2 id="header_tanggal">Laporan Laba Rugi Bulan {{ \App\Util::tanggal($akhiri->format('d m Y')) }}</h2>
					@else
						<h2 id="header_tanggal">Laporan Laba Rugi Bulan {{ \App\Util::tanggal($newAwal->format('d m Y')) }} s/d {{ \App\Util::tanggal($newAkhir->format('d m Y')) }} </h2>
					@endif
				</div>
					
				<div class="col-md-4 pull-right">
					<a href="{{ url()->previous() }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		

		<div class="x_content">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<table class="" style="margin-bottom: 0;" width="100%">
						<tbody>
							<tr>
								<td class="atasan" width="60%">Pendapatan Penjualan</td>
								<td class="money" width="20%"></td>
								<td class="money" width="20%"></td>
							</tr>
							<tr>
								<td>Penjualan</td>
								<td class="money"></td>
								<td class="money">{{ $data['penjualan'] }}</td>
							</tr>
							<tr>
								<td>Retur Pembelian</td>
								<td class="money"></td>
								<td class="money">{{ $data['retur_pembelian'] }}</td>
							</tr>
							<tr>
								<td>Potongan Pembelian</td>
								<td class="money"></td>
								<td class="money">{{ $data['pot_pembelian'] }}</td>
							</tr>
							<tr>
								<td>Dikurangi: Retur Penjualan</td>
								<td class="money"></td>
								<td class="money">({{ $data['retur_penjualan'] }})</td>
							</tr>
							<tr>
								<td><i style="color:white">Dikurangi:</i> Potongan Penjualan</td>
								<td class="money"></td>
								<td class="money">({{ $data['pot_penjualan'] }})</td>
							</tr>
							<tr>
								<td><b>Pendapatan Penjualan Bersih<b></td>
								<td class="money"></td>
								<td class="money jumlah">{{ $data['penjualan_bersih'] }}</td>
							</tr>
							<tr>
								<td><i style="color:white"><br></i></td>
								<td></td>
								<td></td>
							</tr>

							<tr>
								<td class="atasan">Laba Kotor</td>
							</tr>
							<tr>
								<td>Harga Pokok Penjualan</td>
								<td class="money"></td>
								<td class="money">({{ $data['HPP'] }})</td>
							</tr>
							<tr>
								<td>Beban Ongkir</td>
								<td class="money"></td>
								<td class="money">({{ $data['beban_ongkir'] }})</td>
							</tr>					
							<tr>
								<td><b>Laba Kotor</b></td>
								<td class="money"></td>
								<td class="money jumlah">{{ $data['laba_kotor'] }}</td>
							</tr>
							<tr>
								<td><i style="color:white"><br></i></td>
								<td></td>
								<td></td>
							</tr>

							<tr>
								<td class="atasan">Beban Beban</td>
							</tr>
							<tr>
								<td>Beban Iklan</td>
								<td class="money">({{ $data['beban_iklan'] }})</td>
							</tr>
							<tr>
								<td>Beban Sewa</td>
								<td class="money">({{ $data['beban_sewa'] }})</td>
							</tr>
							<tr>
								<td>Beban Perlengkapan</td>
								<td class="money">({{ $data['beban_perlengkapan'] }})</td>
							</tr>
							<tr>
								<td>Beban Perawatan dan Perbaikan</td>
								<td class="money">({{ $data['beban_rawat'] }})</td>
							</tr>
							<tr>
								<td>Beban Kerugian Aset</td>
								<td class="money">({{ $data['beban_rugi_aset'] }})</td>
							</tr>
							<tr>
								<td>Beban Kerugian Piutang</td>
								<td class="money">({{ $data['rugi_piutang'] }})</td>
							</tr>
							<tr>
								<td>Beban Depresiasi Peralatan</td>
								<td class="money">({{ $data['beban_dep_alat'] }})</td>
							</tr>
							<tr>
								<td>Beban Depresiasi Kendaraan</td>
								<td class="money">({{ $data['beban_dep_kendaraan'] }})</td>
							</tr>
							<tr>
								<td>Beban Depresiasi Bangunan</td>
								<td class="money">({{ $data['dep_bangunan'] }})</td>
							</tr>
							<tr>
								<td>Beban Asuransi</td>
								<td class="money">({{ $data['beban_asuransi'] }})</td>
							</tr>
							<tr>
								<td>Beban Gaji</td>
								<td class="money">({{ $data['beban_gaji'] }})</td>
							</tr>
							<tr>
								<td>Beban Administrasi Bank</td>
								<td class="money">({{ $data['beban_adm_bank'] }})</td>
							</tr>
							<tr>
								<td>Beban Bunga</td>
								<td class="money">({{ $data['bunga'] }})</td>
							</tr>
							<tr>
								<td>Beban Denda</td>
								<td class="money">({{ $data['beban_denda'] }})</td>
							</tr>
							<tr>
								<td>Beban Utilitas</td>
								<td class="money">({{ $data['beban_utilitas'] }})</td>
							</tr>
							<tr>
								<td>Beban Kerugian Persediaan</td>
								<td class="money">({{ $data['rugi_persediaan'] }})</td>
							</tr>
							<tr>
								<td>Beban Transportasi</td>
								<td class="money">({{ $data['transportasi'] }})</td>
							</tr>
							<tr>
								<td>Beban Garansi</td>
								<td class="money">({{ $data['garansi'] }})</td>
							</tr>
							<tr>
								<td>Beban Pajak</td>
								<td class="money">({{ $data['pajak'] }})</td>
							</tr>
							<tr>
								<td>Beban Lain-Lain</td>
								<td class="money bawahan">({{ $data['beban_lain'] }})</td>
							</tr>
							<tr>
								<td>&emsp;&emsp; <b>Total Beban</b></td>
								<td class="money">({{ $data['total_beban'] }})</td>
							</tr>
							<tr>
								<td><b>Laba dikurangi Beban</b></td>
								<td class="money"></td>
								<td class="money jumlah">{{ $data['laba_beban'] }}</td>
							</tr>

							<tr>
								<td><i style="color:white"><br></i></td>
								<td></td>
								<td></td>
							</tr>

							<tr>
								<td class="atasan">Pendapatan Lain-Lain dan Keuntungan Lain-Lain</td>
							</tr>
							<tr>
								<td>Pendapatan Lain-Lain</td>
								<td class="money"></td>
								<td class="money">{{ $data['pendapatan_lain'] }}</td>
							</tr>
							<tr>
								<td>Keuntungan Persediaan</td>
								<td class="money"></td>
								<td class="money">{{ $data['untung_persediaan'] }}</td>
							</tr>


							<tr>
								<td><b>Laba Sebelum Pajak</b></td>
								<td class="money"></td>
								<td class="money jumlah" id="belum_pajak">{{ $data['laba_sebelum_pajak'] }}</td>
							</tr>

							<tr>
								<td><i style="color:white"><br></i></td>
								<td></td>
								<td></td>
							</tr>

							<tr>
								<td><b>Pajak Penghasilan</b></td>
								<td>
									<input class="form-control" type="text" name="pajak" style="height: 25px"  id="pajak" value="0,000">
								</td>
								<td class="money"><span id="pajak_v">0,000</span></td>
							</tr>
							<tr>
								<td><i style="color:white"><br></i></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td><b>Laba Bersih</b></td>
								<td class="money"></td>
								<td class="money">
									<span id="laba_bersih">-</span>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
					<div class="line"></div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<form method="post" action="{{ url('jurnal_penutup') }}" class="form-horizontal">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
							<input type="hidden" name="awal" value="{{ $awal }}">
							<input type="hidden" name="akhir" value="{{ $akhir }}">
							<input type="hidden" name="newAkhir" value="{{ $newAkhir }}">
							<input type="hidden" name="pajak_set">
							<input type="hidden" name="laba_bersih">
							<div class="form-group col-sm-12 col-xs-12">
								<button class="btn btn-sm btn-success pull-right" id="penutup" type="submit">
									<span style="color:white">Buat Jurnal Penutup</span>
								</button>
							</div>
						</form>
					</div>	
				</div>
			</div>
		</div>

	</div>
</div>
@endsection


@section('script')
  <script type="text/javascript">
     $(document).ready(function() {
     	var url = "{{ url('laba_rugi') }}";
		var a = $('a[href="' + url + '"]');
		a.parent().addClass('current-page');
		a.parent().parent().show();
		a.parent().parent().parent().addClass('active');

		// $('#penutup').prop('disabled', true);
		cek();

        $('#rentang_tanggal').daterangepicker(null, function(start, end, label) {
			var mulai = (start.toISOString()).substring(0,10);
			console.log(mulai);
			var akhir = (end.toISOString()).substring(0,10);
			console.log(akhir);
			$('#formPilihan').find('input[name="awal"]').val(mulai);
			$('#formPilihan').find('input[name="akhir"]').val(akhir);
		});
      });  


     function cek(){
     	var laba_bersih = $('#laba_bersih').text();
		if(laba_bersih == '-'){
			$('#penutup').prop('disabled', true);
		}else{
			var laba_bersih = parseFloat(laba_bersih.replace(',', '.'));
			if(laba_bersih == 0){
				$('#penutup').prop('disabled', true);
			}else{
				$('#penutup').prop('disabled', false);
			}
		}
    }

    $(document).on('keyup', '#pajak', function(event) {
		event.preventDefault();
		var pajaks = $(this).val().split(',');
		var pajak = 0;
		if(pajaks.length > 1){
			pajak = $(this).val();
			if(pajaks[0].length < 1){
				pajaks[0] = 0;
				$(this).val(pajaks[0] + ',' + pajaks[1]);
			}
			else if(pajaks[1].length > 3){
				pajak = $(this).val().slice(0,-1);
				$(this).val(pajak);
				// console.log(pajak.toLocaleString(['ban', 'id']));
			}else if(pajaks[1].length < 3){
				pajak = parseFloat(pajak.replace(',', '.'));
				pajak = pajak.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
				pajak = pajak.replace('.', '');
				$(this).val(pajak);
				// console.log('tengah');
			}
		}else{
			pajak = $(this).val();
			var bel_koma = pajak.substr(pajak.length -3);
			var depan_koma = pajak.substr(0, pajak.length -3);

			$(this).val(depan_koma + ',' + bel_koma);
		}
		var pajak_h = parseFloat(pajak.replace(',', '.'));
		var pajak_v = pajak_h.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
		$('#pajak_v').text(pajak_v);

		var belum_pajak_t = $('#belum_pajak').text().split(',');
		var belum_pajak_0 = parseFloat(belum_pajak_t[0].replace(/\D/g, ''), 10);
		var belum_pajak = parseFloat(belum_pajak_0 +'.' + belum_pajak_t[1]);

		// // console.log(belum_pajak_t, belum_pajak);
		// console.log(belum_pajak_t, belum_pajak, pajak_h);

		var laba =  (belum_pajak - pajak_h);
		var laba_v = laba.toLocaleString(['ban', 'id'], {minimumFractionDigits:3});
		$('#laba_bersih').text(laba_v);
		
		$('input[name="laba_bersih"]').val(laba_v);
		$('input[name="pajak_set"]').val(pajak_v);

		cek();
	});  

  </script>
@endsection