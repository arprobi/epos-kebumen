@extends('layouts.admin')

@section('title')
    <title>EPOS | Pemasok</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah,
        #btnHapus,
        #btnAktif {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-11">
                        <h2 id="formSimpanTitle">Tambah Pemasok</h2>
                    </div>
                    <div class="col-md-1">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('suplier') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Nama Pemasok</label>
                                <input class="form-control" type="text" name="nama" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Telepon</label>
                                <input class="form-control" type="text" name="telepon">
                                <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="email" name="email">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Alamat</label>
                                <textarea rows="4" class="form-control" name="alamat" ></textarea>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-success" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                                <button class="btn btn-default" id="btnReset" type="button">
                                    <i class="fa fa-refresh"></i> Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Pemasok</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="mdtContainer">
                            <input type="hidden" name="base_url" value="{{ url('suplier/mdt1') }}">
                            <input type="hidden" name="data_per_halaman" value="">
                            <input type="hidden" name="search_query" value="">
                            <input type="hidden" name="data_total" value="">
                            <input type="hidden" name="halaman_sekarang" value="">
                            <input type="hidden" name="field" value="nama">
                            <input type="hidden" name="order" value="asc">

                            <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                            <table id="tableSuplier" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th class="sorting" field="nama">No</th>
                                        <th class="sorting" field="nama">Nama Pemasok</th>
                                        <th class="sorting" field="telepon">Telepon</th>
                                        <th class="sorting" field="email">Email</th>
                                        <th class="sorting" field="alamat">Alamat</th>
                                        <th class="sorting" field="piutang">Piutang</th>
                                        <th class="sorting" field="nama" style="width: 80px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                <div class="col-md-6" id="paginationText"></div>
                                <div class="col-md-6" style="text-align: right;">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination" style="margin: 0;">
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Riwayat Pemasok</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="mdtContainer">
                            <input type="hidden" name="base_url" value="{{ url('suplier/mdt2') }}">
                            <input type="hidden" name="data_per_halaman" value="">
                            <input type="hidden" name="search_query" value="">
                            <input type="hidden" name="data_total" value="">
                            <input type="hidden" name="halaman_sekarang" value="">
                            <input type="hidden" name="field" value="nama">
                            <input type="hidden" name="order" value="asc">

                            <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                            <table id="tableSuplier1" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th class="sorting" field="nama">No</th>
                                        <th class="sorting" field="nama">Nama Pemasok</th>
                                        <th class="sorting" field="telepon">Telepon</th>
                                        <th class="sorting" field="email">Email</th>
                                        <th class="sorting" field="alamat">Alamat</th>
                                        <th class="sorting" field="nama" style="width: 40px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <div id="mdtFooter" class="row" style="margin-top: 10px;">
                                <div class="col-md-6" id="paginationText"></div>
                                <div class="col-md-6" style="text-align: right;">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination" style="margin: 0;">
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pemasok berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pemasok gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pemasok berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pemasok gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'aktif')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pemasok berhasil diaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pemasok berhasil dinonaktifkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pemasok gagal dinonaktifkan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('suplier/mdt1') }}";
        var mdt2 = "{{ url('suplier/mdt2') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="7" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="6" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var suplier = data[i];
                        var buttons = suplier.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.ubah != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-warning" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Pemasok">
                                    <i class="fa fa-edit"></i>
                                </a>
                            `;
                        }
                        if (buttons.nonaktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Nonaktifkan Pemasok">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${suplier.id}">
                                    <td>${nomor}</td>
                                    <td>${suplier.nama}</td>
                                    <td>${suplier.telepon}</td>
                                    <td>${suplier.email}</td>
                                    <td>${suplier.alamat}</td>
                                    <td class="text-right">${suplier.piutang}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var suplier = data[i];
                        var buttons = suplier.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.aktifkan != null) {
                            td_buttons += `
                                <a class="btn btn-xs btn-success" id="btnAktif" data-toggle="tooltip" data-placement="top" title="Aktifkan Pemasok">
                                    <i class="fa fa-check"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${suplier.id}">
                                    <td>${nomor}</td>
                                    <td>${suplier.nama}</td>
                                    <td>${suplier.telepon}</td>
                                    <td>${suplier.email}</td>
                                    <td>${suplier.alamat}</td>
                                    <td>${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            $('#btnReset').trigger('click');
        });

        $(document).on('click', '#btnReset', function() {
            $('input[name="nama"]').val('');
            $('input[name="telepon"]').val('');
            $('input[name="email"]').val('');
            $('textarea[name="alamat"]').val('');

            $('#formSimpanContainer').find('form').attr('action', '{{ url("suplier") }}');
            $('#formSimpanContainer').find('input[name="_method"]').val('post');
            $('#btnSimpan').find('span').text('Tambah');

            $('#formSimpanTitle').text('Tambah Pemasok');
        });

        $(document).on('click', '#btnUbah', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            var telepon = $tr.find('td').first().next().next().text();
            var email = $tr.find('td').first().next().next().next().text();
            var alamat = $tr.find('td').first().next().next().next().next().text();

            $('input[name="nama"]').val(nama);
            $('input[name="telepon"]').val(telepon);
            $('input[name="email"]').val(email);
            $('textarea[name="alamat"]').val(alamat);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("suplier") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan').find('span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Pemasok');
        });
        
        $(document).on('click', '#btnHapus', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Nonaktifkan?',
                text: '\"' + nama + '\" akan dinonaktifkan!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Nonaktifkan!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                $('#formHapusContainer').find('form').attr('action', '{{ url("suplier") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('click', '#btnAktif', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var nama = $tr.find('td').first().next().text();
            var id = $tr.attr('id');

            var url = '{{ url('suplier/aktif/') }}' + '/' + id;

            swal({
                title: 'Aktifkan?',
                text: '\"' + nama + '\" akan diaktifkan kembali!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Aktifkan',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                window.open(url, '_self');
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }

    </script>
@endsection
