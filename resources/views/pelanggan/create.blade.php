@extends('layouts.admin')

@section('title')
    <title>EPOS | Pelanggan</title>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tambah Pelanggan</h2>
                <a href="{{ url('pelanggan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <p class="pull-right text-danger" style="padding-right: 15px; padding-top: 5px">
                    <strong>* Wajib Diisi</strong>
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('pelanggan/store') }}" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    @if ($po_penjualan != null)
                    <input type="hidden" name="po_penjualan_id" value="{{ $po_penjualan->id }}">
                    @endif
                    <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        {{-- <div class="form-group">
                            <label class="control-label">Kode Pelanggan *</label>
                            <input class="form-control" type="text" name="kode">
                        </div> --}}
                        <div class="form-group">
                            <label class="control-label text-danger">Jenis Identitas KTP / SIM *</label>
                            <select name="status_identitas" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Identitas</option>
                                <option value="1">KTP</option>
                                <option value="2">SIM</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Nomor Identitias KTP / SIM *</label>
                            <input class="form-control" type="text" name="ktp" required="">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Nama Pelanggan*</label>
                            <input class="form-control" type="text" name="nama" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Nama Toko *</label>
                            <input class="form-control" type="text" name="toko" required="">
                            <p class="">Jika pelanggan lupa/tidak memiliki nama toko, isilah dengan nama pelanggan.</p>
                            <p id="TokoAlert" class="text-danger">Nama Toko tidak boleh lebih dari 21 karakter!</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Alamat *</label>
                            <textarea style="margin: 0px 0.5px 0px 0px;height: 104px;width: 505px;" class="form-control" name="alamat" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Provinsi *</label>
                            <select name="provinsi_id" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Provinsi</option>
                                @foreach($provinsis as $provinsi)
                                    <option value="{{$provinsi->id}}">{{$provinsi->nama}}</option>
                                @endforeach
                                {{-- <option id="lain_prov">Lain-lain</option> --}}
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Kabupaten *</label>
                            <select name="kabupaten_id" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Kabupaten</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Kecamatan *</label>
                            <select name="kecamatan_id" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label text-danger">Desa *</label>
                            <select name="desa_id" class="select2_single form-control" required="">
                                <option id="default" value="">Pilih Desa</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label text-danger">Telepon *</label>
                            <input class="form-control" type="text" name="telepon" required="">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Fax</label>
                            <input class="form-control" type="text" name="fax">
                            <span style="color:red" class="sembunyi">Tidak boleh isi selain nomor!</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="text" name="email">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Rekening</label>
                            <input class="form-control" type="text" name="no_rekening">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rekening Atas Nama</label>
                            <input class="form-control" type="text" name="atas_nama">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Bank</label>
                            <input class="form-control" type="text" name="bank">
                        </div>
                        @if ($po_penjualan == null && (Auth::user()->level_id == 1 || Auth::user()->level_id == 2))
                            <div class="form-group">
                                <label class="control-label">Limit Jumlah Piutang (Rp)</label>
                                <input class="form-control angka" type="text" name="limit_jumlah_piutang_">
                                <input class="form-control hide" type="text" name="limit_jumlah_piutang">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Waktu Jatuh Tempo (Hari)</label>
                                <input class="form-control" id="" type="text" name="jatuh_tempo" value="7">
                            </div>
                        @endif
                            {{-- <div class="form-group">
                                <label class="control-label text-danger">Grup Pelanggan</label>
                                <select class="form-control" id="level" name="level">
                                    <option value="">Pilih Grup Pelanggan</option>
                                    @foreach ($enumLevel as $val)
                                        <option value="{{ $val }}">
                                            @if($val == 'eceran')
                                                Eceran
                                            @else
                                                Grosir
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <!-- <div class="form-group">
                                <label class="control-label">Diskon dengan Persen</label>
                                <input class="form-control" type="text" name="diskon_persen">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jumlah Potongan</label>
                                <input class="form-control angka" type="text" name="potongan_">
                                <input class="form-control hide" type="text" name="potongan">
                            </div> -->
                        </div>
                    </div>

                    <div class="form-group pull-right" style="margin-bottom: 0;">
                        <button class="btn btn-default" id="btnReset" type="button">
                            <i class="fa fa-refresh"></i> Reset
                        </button>
                        <button class="btn btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('gagal') == 'telepon')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Nomor telepon sudah terdaftar!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ktp')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Nomor KTP sudah terdaftar!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        function isRequiredFieldFilled() {
            // console.log('isRequiredFieldFilled');
            var ktp = $('input[name="ktp"]').val();
            var nama = $('input[name="nama"]').val();
            var toko = $('input[name="toko"]').val();
            var alamat = $('textarea[name="alamat"]').val();
            var telepon = $('input[name="telepon"]').val();
            var level = $('select[name="level"]').val();
            if (ktp == '' || nama == '' || toko == '' || alamat == '' || telepon == '' || level == '') return false;
            return true;
        }

        $(document).ready(function() {
            var url = "{{ url('pelanggan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));
            
            $(".select2_single").select2({
                allowClear: true
            });
            
            $('#TokoAlert').hide();
            $('#btnSimpan').prop('disabled', true);
        });

        $(document).on('click', '#btnReset', function() {
            event.preventDefault();

            $('input[name="nama"]').val('');
            $('textarea[name="alamat"]').val('');
            $('select[name="provinsi_id"]').val('');
            $('select[name="kabupaten_id"]').val('');
            $('select[name="kecamatan_id"]').val('');
            $('select[name="desa_id"]').val('');
            $('input[name="telepon"]').val('');     
            $('input[name="fax"]').val('');
            $('input[name="email"]').val('');
            $('input[name="no_rekening"]').val('');
            $('input[name="atas_nama"]').val('');
            $('input[name="bank"]').val('');
            $('input[name="level"]').val('');
            $('input[name="diskon_persen"]').val('');
            $('input[name="potongan"]').val('');
            $('input[name="limit_jumlah_piutang"]').val('');
            $('select[name="level"]').val('');
        });

        $(document).on('keyup', 'input[name="ktp"]', function(event) {
            event.preventDefault();

            var ktp = $(this).val();
            var url = "{{ url('pelanggan') }}"+'/'+ ktp +'/ktpJson';
            var ini = $(this);
            // console.log(url);
             $.get(url, function(data) {
                // console.log(data);
                if (isNaN(ktp)) {
                    ini.parents('.form-group').first().addClass('has-error');
                    ini.next('span').removeClass('sembunyi');
                } else {
                    ini.next('span').addClass('sembunyi');
                    if (data.ktp != null && Object.keys(data.ktp).length > 0) {
                        $('input[name="ktp"]').parents('.form-group').addClass('has-error');
                    } else {
                        $('input[name="ktp"]').parents('.form-group').removeClass('has-error');
                    }
                }

                cek();
            });
        });

        $(document).on('keyup', 'input[name="nama"]', function(event) {
            event.preventDefault();

            var nama = $(this).val();
            if (nama == '') {
                $('input[name="nama"]').parents('.form-group').addClass('has-error');
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('input[name="nama"]').parents('.form-group').removeClass('has-error');
                if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
            }
        });

        $(document).on('keyup', 'input[name="toko"]', function(event) {
            event.preventDefault();

            var toko = $(this).val();
            if (toko == '') {
                $('input[name="toko"]').parents('.form-group').addClass('has-error');
                $('#btnSimpan').prop('disabled', true);
            } else {
                if (toko.length > 21) {
                    $('#TokoAlert').show();
                    $('#btnSimpan').prop('disabled', true);
                } else {
                    $('#TokoAlert').hide();
                    $('input[name="toko"]').parents('.form-group').removeClass('has-error');
                    if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
                }
            }
        });

        $(document).on('keyup', 'textarea[name="alamat"]', function(event) {
            event.preventDefault();

            var alamat = $(this).val();
            if (alamat == '') {
                $('textarea[name="alamat"]').parents('.form-group').addClass('has-error');
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('textarea[name="alamat"]').parents('.form-group').removeClass('has-error');
                if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
            }
        });

        $(document).on('keyup', 'input[name="telepon"]', function(event) {
            event.preventDefault();

            var telepon = $(this).val();
            var url = "{{ url('pelanggan') }}"+'/'+ telepon +'/teleponJson';
            var ini = $(this);

            // console.log(url);
             $.get(url, function(data) {
                // console.log(data);
                if (isNaN(telepon)) {
                    ini.parents('.form-group').first().addClass('has-error');
                    ini.next('span').removeClass('sembunyi');
                } else {
                    ini.next('span').addClass('sembunyi');
                    if (data.telepon != null && Object.keys(data.telepon).length > 0) {
                        $('input[name="telepon"]').parents('.form-group').addClass('has-error');
                    } else {
                        $('input[name="telepon"]').parents('.form-group').removeClass('has-error');
                    }
                }

                cek();
            });
        });

        $(document).on('change', 'select[name="provinsi_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kabupaten.json';

            var $select = $('select[name="kabupaten_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Kabupaten</option>'));

            var $select_kec = $('select[name="kecamatan_id"]');
            $select_kec.empty();
            $select_kec.append($('<option value="">Pilih Kecamatan</option>'));

            var $select_des = $('select[name="desa_id"]');
            $select_des.empty();
            $select_des.append($('<option value="">Pilih Desa</option>'));

            $.get(url, function(data) {
                var kabupatens = data.kabupaten;
                for (var i = 0; i < kabupatens.length; i++) {
                    var kabupaten = kabupatens[i];
                    $select.append($('<option value="' + kabupaten.id + '">' + kabupaten.nama + '</option>'));
                }
                if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
            });
        });

        $(document).on('change', 'select[name="kabupaten_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/kecamatan.json';

            var $select = $('select[name="kecamatan_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Kecamatan</option>'));

            var $select_des = $('select[name="desa_id"]');
            $select_des.empty();
            $select_des.append($('<option value="">Pilih Desa</option>'));

            $.get(url, function(data) {
                var kecamatans = data.kecamatan;
                for (var i = 0; i < kecamatans.length; i++) {
                    var kecamatan = kecamatans[i];
                    $select.append($('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>'));
                }
                if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
            });
        });

        $(document).on('change', 'select[name="kecamatan_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            var url = "{{ url('wilayah') }}" + '/' + id + '/desa.json';

            var $select = $('select[name="desa_id"]');
            $select.empty();
            $select.append($('<option value="">Pilih Desa</option>'));

            $.get(url, function(data) {
                var desas = data.desa;
                for (var i = 0; i < desas.length; i++) {
                    var desa = desas[i];
                    $select.append($('<option value="' + desa.id + '">' + desa.nama + '</option>'));
                }
                if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
            });
        });

        $(document).on('change', 'select[name="desa_id"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        // $(document).on('keyup', 'input[name="fax"]', function(event) {
        //     event.preventDefault();

        //     if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        // });

        $(document).on('keyup', 'input[name="email"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('keyup', 'input[name="no_rekening"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('keyup', 'input[name="atas_nama"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('keyup', 'input[name="bank"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('change', 'select[name="level"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('keyup', 'input[name="limit_jumlah_piutang_"]', function(event) {
            event.preventDefault();

            if (isRequiredFieldFilled()) $('#btnSimpan').prop('disabled', false);
        });

        $(document).on('keyup', 'input[name="fax"]', function(event) {
            event.preventDefault();
            
            var text = $(this).val();
            var ini = $(this);

            if (isNaN(text)) {
                ini.parents('.form-group').first().addClass('has-error');
                ini.next('span').removeClass('sembunyi');
            } else {
                ini.parents('.form-group').first().removeClass('has-error');
                ini.next('span').addClass('sembunyi');
            }

            cek();
        });

        function cek() {
            if ($('.form-group').hasClass('has-error')) {
                $('#btnSimpan').prop('disabled', true);
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
    </script>
@endsection


