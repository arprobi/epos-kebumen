@extends('layouts.admin')

@section('title')
    <title>EPOS | Riwayat Harga Beli</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus, #btnKembali {
            margin-bottom: 0;
        }
        #btnKembali {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .link {
            text-decoration: underline;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Riwayat Harga Beli <b>{{ $nama_item }}</b></h2>
                <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover tableItem" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Transaksi</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Pemasok</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($harga_beli as $num => $harga)
                        <tr id="{{ $harga->id }}">
                            <td>{{ $num + 1 }}</td>
                            <td>{{ $harga->created_at->format('d-m-Y H:i:s') }}</td>
                            <td class="link"><a href="{{ url('transaksi-pembelian/'.$harga->transaksi_pembelian->id) }}">{{ $harga->transaksi_pembelian->kode_transaksi }}</a></td>
                            <td class="stok">{{ $harga->jumlah }}</td>
                            <td style="text-align: right;">
                                @foreach ($satuans as $i => $satuan)
                                <!-- <div class="row">
                                    <div class="form-group col-sm-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" readonly="readonly" value="Rp{{ \App\Util::dk($harga->harga * $satuan->konversi) }}" />
                                            <div class="input-group-addon">per {{ $satuan->satuan->kode }}</div>
                                        </div>
                                    </div>
                                </div> -->
                                {{ \App\Util::duit0($harga->harga * $satuan->konversi * 1.1) }}/{{$satuan->satuan->nama }}
                                <br>
                                @endforeach
                            </td>
                            <td>{{ $harga->transaksi_pembelian->suplier->nama }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- <div class="col-md-12 col-xs-12">
        @foreach ($supliers as $i => $suplier)
        <div class="x_panel">
            <div class="x_title">
                <h2>Riwayat Harga Beli <b>{{ $suplier->item->nama }}</b> dari <b>{{ $suplier->nama }}</b></h2>
                <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover tableItem" style="margin-bottom: 0;" id="tableItem">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Tanggal</th>
                            <th>Kode Transaksi</th>
                            <th>Harga Beli</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($suplier->relasis as $num => $relasi)
                        <tr id="{{ $relasi->id }}">
                            <td>{{ $num + 1 }}</td>
                            <td>{{ $relasi->created_at->format('d-m-Y') }}</td>
                            <!-- <td class="link"><a href="{{ url('transaksi-pembelian/'.$relasi->transaksi_pembelian->id.'/hhb/'.$relasi->item->id) }}">{{ $relasi->transaksi_pembelian->kode_transaksi }}</a></td> -->
                            <td class="link"><a href="{{ url('transaksi-pembelian/'.$relasi->transaksi_pembelian->id) }}">{{ $relasi->transaksi_pembelian->kode_transaksi }}</a></td>
                            <td style="text-align: right;">
                                @foreach ($satuans as $i => $satuan)
                                <!-- <div class="row">
                                    <div class="form-group col-sm-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" readonly="readonly" value="Rp{{ \App\Util::dk($relasi->harga * $satuan->konversi) }}" />
                                            <div class="input-group-addon">per {{ $satuan->satuan->kode }}</div>
                                        </div>
                                    </div>
                                </div> -->
                                {{ \App\Util::duit0($relasi->harga * $satuan->konversi * 1.1) }}/{{$satuan->satuan->kode }}
                                <br>
                                @endforeach
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div> --}}
@endsection

@section('script')
    <script type="text/javascript">
        var item = [];

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            item = '{{ json_encode($item) }}';
            item = item.replace(/&quot;/g, '"');
            item = JSON.parse(item);

            $('.stok').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('id');
                var jumlah = parseFloat($(el).text());

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var text_stoktotal = '-';
                var temp_stoktotal = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                $(el).text(text_stoktotal);
                if (jumlah > 0 && text_stoktotal == '-') {
                    $(el).text(jumlah);
                }
            });

            $('.tableItem').DataTable();
        });

    </script>
@endsection
