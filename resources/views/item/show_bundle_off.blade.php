@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Item {{ $item_detail->nama }}</title>
@endsection

@section('style')
    <style media="screen">
        .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
        td span {
            line-height: 1.42857143;
        }
    </style>
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <!-- Detail Item -->
        <div class="col-md-6" id="formSimpanContainer">
            <div class="x_panel">
                <div class="x_title">
                    <!-- <h2 id="formSimpanTitle">Detail Item</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul> -->
                    <div class="row">
                        <div class="col-md-11">
                            <p id="formSimpanTitle" class="titleDetailItem">
                                <span><span>Detail Item </span><span class="nama-item">{{ $item_detail->nama }}</span></span>
                            </p>
                        </div>
                        <div class="col-md-1">
                            <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                                <div class="pull-right">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Kode Barang</th>
                                        <td id="item_kode">{{ $item_detail->kode_barang }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Kode Item</th>
                                        <td id="item_kode">
                                            @foreach ($item_kodes as $i => $item_kode )
                                            <span class="label label-info">{{ $item_kode->kode }}</span> 
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Nama Item</th>
                                        <td>{{ $item_detail->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:50%">Nama Pendek</th>
                                        <td>{{ $item_detail->nama_pendek }}</td>
                                    </tr>
                                    <!-- <tr>
                                        <th style="width:50%">Kode Item</th>
                                        <td id="item_kode">{{ $item_kode_super }}</td>
                                    </tr> -->
                                    <tr>
                                        <th style="width:50%">Pemasok</th>
                                        <td>
                                        {{-- @foreach ($items as $i => $item ) --}}
                                            <span class="label label-info suplier_bundle">{{ $item_detail->suplier->nama }}</span> 
                                        {{-- @endforeach --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="50%">Jenis Item</th>
                                        <td>{{ $item_detail->jenis_item->nama}}</td>
                                    </tr>
                                    <tr>
                                        <th width="50%">Kategori Item</th>
                                        @if ($item_detail->konsinyasi == 0) <td> Item Biasa </td>
                                        @elseif ($item_detail->konsinyasi == 3) <td> Item Bonus </td>
                                        @elseif ($item_detail->konsinyasi == 1) <td> Item Konsinyasi </td>
                                        @elseif ($item_detail->konsinyasi == 2) <td> Item Paket </td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th width="50%">Status Retur</th>
                                        @if ($item_detail->retur == 1)
                                            <td>Boleh Retur</td>
                                        @else
                                            <td>Tidak Boleh Retur</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th width="50%">Satuan Item</th>
                                        <td>
                                            @foreach($relasi_satuans as $relasi_satuan)
                                                1 {{$relasi_satuan->satuan->nama}}
                                            @endforeach
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <th>Stok</th>
                                        <!-- <td>{{ $stok->stok }} PCS</td> -->
                                        <td>
                                            @for ($i = 0; $i < count($stok_total); $i++)
                                                <span class="label label-info stok">{{ $stok_total[$i]['jumlah'] }} {{ $stok_total[$i]['satuan'] }}</span>
                                            @endfor
                                        </td>
                                    </tr> --}}
                                    @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                        <tr>
                                            <th width="50%">Keuntungan yang Diharapkan (E)</th>
                                            <td >{{ \App\Util::angka_koma($item_detail->profit_eceran) }}% | {{ \App\Util::duit0($item_detail->plus_grosir) }} (Grosir)</td>
                                        </tr>
                                        <tr>
                                            <th width="50%">Keuntungan yang Diharapkan (G)</th>
                                            <td id="profit" class="profit_grosir">{{ $item_detail->profit }}</td>
                                        </tr>
                                        <tr>
                                            <th width="50%">Rentang Batas Perubahan Harga</th>
                                            <td>{{ App\Util::duit0($item_detail->rentang) }}</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Satuan Item -->
        {{-- <div class="col-md-6" id="formSimpanContainer">
            <div class="x_panel">
                <div class="x_title">
                    <h2 id="formSimpanTitle">Satuan Item</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <a href="{{ url('item-bundle/edit/'.$item_kode_super) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Item">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="formSimpanSatuan">
                                <form class="form-horizontal" action="{{url('/relasi_satuan')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="post">
                                    <input type="hidden" name="item_kode" value="{{$item_kode_super}}">
                                    <div class="row">
                                        <div class="form-group col-xs-5">
                                            <select name="satuanID" class="form-control select2_single" id="satuan_relasi" disabled="">
                                                <option>Pilih Satuan</option>
                                                @foreach($satuans as $satuan)
                                                <option value="{{$satuan->id}}">{{$satuan->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <input type="text" name="konversi" class="form-control" placeholder="Konversi" style="height: 38px" disabled="">
                                        </div>
                                        <div class="form-group col-xs-3">
                                            <button class="btn btn-sm btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit" style="margin-right: 0; width: 100%; height: 34px;" disabled="">
                                                <i class="fa fa-save"></i> <span>Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div id="formHapusSatuan" style="display: none;">
                                    <form method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="_method" value="delete">
                                    </form>
                                </div>
                                <table class="table table-bordered table-striped table-hover" id="">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Jenis Satuan</th>
                                            <th class="text-left">Konversi</th>
                                            <!-- <th class="text-left" style="width: 31%;">Aksi</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <tr id="{{$relasi_satuan->id}}">
                                            <td data-satu="{{$relasi_satuan->satuan_id}}">{{$relasi_satuan->satuan->nama}}</td>
                                            <td class="duit">{{$relasi_satuan->konversi}}</td>
                                            <!-- <td class="text-center">
                                                <button class="btn btn-xs btn-warning btnUbah" id="ubah_relasi_satuan" title="Ubah">
                                                    <i class="fa fa-edit"></i> Ubah
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapus_relasi_satuan" title="Hapus">
                                                    <i class="fa fa-trash"></i> Hapus
                                                </button>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Item</h2>
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <a href="{{ url('item-bundle/edit/'.$item_kode_super) }}" class="btn btn-sm btn-warning pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Ubah Item">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
                        <i class="fa fa-long-arrow-left"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="formHapusBundle" style="display: none;">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="delete">
                            {{-- <input type="hidden" name="id"> --}}
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="formBundleContainer">
                            <form action="{{url('/item/bundle/create')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="item_kode" value="{{$item_kode_super}}">
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <input type="text" name="jumlah" class="form-control select2_tinggi" placeholder="Jumlah" id="jumlah_bundle" required="">
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <select name="konversi" class="form-control pull-right select2_single" id="satuan_bundle" required="">
                                            <option value="">Pilih Satuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <select name="item_list" class="form-control select2_single" id="item_list" required="">
                                            <option value="">Pilih Item</option>
                                            @foreach($item_list as $item_bundles)
                                            <option value="{{$item_bundles->kode}}">{{$item_bundles->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <button class="btn btn-success select2_tinggi" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Tambah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                 <table class="table table-bordered table-striped table-hover" id="BundleTable">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th class="text-center">Item</th>
                                            <th width="5%" class="text-center">Jumlah</th>
                                            <th class="text-center" style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($relasi_bundles as $i => $relasi_bundle)
                                        <tr id="{{ $relasi_bundle->id }}" var="{{ $a = $i + 1 }}" item_kode="{{ $relasi_bundle->item_child }}">
                                            <td>{{ $a }}</td>
                                            <td kode="{{ $relasi_bundle->item_child }}">{{ $relasi_bundle->itemxx->nama }}</td>
                                            <td class="jumlah" jumlah="" konversi="">{{ $relasi_bundle->jumlah }}</td>
                                            {{-- <td konversi="{{ $jumlah_bundles[$i]['konversi'] }}" jumlah="{{ $relasi_bundle->jumlah }}">
                                                {{ $jumlah_bundles[$i]['jumlah'] }} {{ $jumlah_bundles[$i]['satuan'] }}
                                            </td> --}}
                                            <td>
                                                <button class="btn btn-xs btn-warning btnUbah" id="UbahBundle" data-toggle="tooltip" data-placement="top" title="Ubah Item">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapusBundle" data-toggle="tooltip" data-placement="top" title="Hapus Item">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Daftar Item -->
    {{-- <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Item <b>{{ $item_detail->nama }}</b></h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="formHapusBundle" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="delete">
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-6" id="formBundleContainer">
                        <form action="{{url('/item/bundle/create')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="item_kode" value="{{$item_kode_super}}">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <select name="item_list" class="form-control select2_single" id="item_list" required="">
                                        <option value="">Pilih Item</option>
                                        @foreach($item_list as $item_bundles)
                                        <option value="{{$item_bundles->kode}}">{{$item_bundles->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-6">
                                    <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" id="jumlah_bundle" required="">
                                </div>
                                <div class="form-group col-xs-6 pull-right">
                                    <select name="konversi" class="form-control pull-right" id="satuan_bundle" required="">
                                        <option value="">Pilih Satuan</option>
                                    </select>
                                </div>

                                <div class="form-group col-xs-6">
                                    <button class="btn btn-success" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                             <table class="table table-bordered table-striped table-hover" id="BundleTable">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th class="text-center">Item</th>
                                        <th width="5%" class="text-center">Jumlah</th>
                                        <th class="text-center" style="width: 50px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($relasi_bundles as $i => $relasi_bundle)
                                    <tr id="{{ $relasi_bundle->id }}" var="{{ $a = $i + 1 }}" item_kode="{{ $relasi_bundle->item_child }}">
                                        <td>{{ $a }}</td>
                                        <td kode="{{ $relasi_bundle->item_child }}">{{ $relasi_bundle->itemxx->nama }}</td>
                                        <td class="jumlah" jumlah="" konversi="">{{ $relasi_bundle->jumlah }}</td>
                                        <td>
                                            <button class="btn btn-xs btn-warning btnUbah" id="UbahBundle" data-toggle="tooltip" data-placement="top" title="Ubah Satuan">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger btnHapus" id="hapusBundle" data-toggle="tooltip" data-placement="top" title="Hapus Satuan">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row">
        <!-- Stok Item -->
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Harga Beli</h2>
                    <!-- <h2>Harga Beli {{ $item->nama }}</h2> -->
                    <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                        <div class="pull-right">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                         <table class="table table-bordered" id="StokTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th class="text-center">Item</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Sub Total</th>
                                    {{-- <th class="text-center">Tanggal Masuk</th>
                                    <th class="text-center">Pemasok</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Harga Dasar</th>
                                    <th class="text-center">Tanggal Kadaluarsa</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stok_childs as $i => $stok_child)
                                    @foreach ($stok_child as $j => $stok)
                                        <tr>
                                            @if ($j == 0)
                                            <td rowspan="{{ count($stok_child) }}">{{ $i + 1 }}</td>
                                            @endif
                                            <td id="item_kode" item_kode="{{ $stok['kode'] }}">{{ $stok['nama'] }}</td>
                                            <td id="harga-{{$i+1}}" class="text-right harga harga-{{$i+1}}">{{ $stok['harga'] }}/{{ $stok['jumlah'] }}</td>
                                            @if ($j == 0)
                                            <td rowspan="{{ count($stok_child) }}" id="subtotal-{{$i+1}}" class="text-right subtotal">-</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endforeach
                                {{-- @foreach ($stoks as $i => $stok)
                                <tr id="{{ $stok->id }}" var="{{ $a = $i + 1 }}">
                                    <td class="sembunyi">{{ $a }}</td>
                                    <td>{{ $stok->created_at->format("d-m-Y") }}</td>
                                    <td>{{ $stok->item->suplier->nama }}</td>
                                    <td style="text-align:center;"><!-- {{$stok->jumlah}} -->
                                        @for ($j = 0; $j < count($stok_pcs[$i]); $j++)
                                        <span class="stok">{{ $stok_pcs[$i][$j]['jumlah'] }} {{ $stok_pcs[$i][$j]['satuan'] }}</span><br>
                                        @endfor
                                    </td>
                                    <td style="text-align: right;"> 
                                        @for ($j = 0; $j < count($harga_satuan[$i]); $j++)
                                            {{ \App\Util::duit0($harga_satuan[$i][$j]['harga'] * 1.1) }} /{{$harga_satuan[$i][$j]['satuan'] }}
                                            <br>
                                        @endfor
                                    </td>
                                    <td class="tanggal">{{ $stok->kadaluarsa }}</td>
                                </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
            <!-- Harga Item -->
            <div class="col-md-6" id="formSimpanHargaContainer">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Harga Jual</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <a class="btn btn-sm btn-default pull-right" id="btnResetHarga" data-toggle="tooltip" data-placement="top" title="Reset Harga Jual">
                            <i class="fa fa-undo"></i>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{url('/harga')}}" method="post" id="formHargaJual">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="item_kode" value="{{$item_kode_super}}">
                            <div class="row">
                                <div class="form-group col-xs-6" id="formEceran">
                                    <label class="label-control">Harga Eceran</label>
                                    <input type="text" name="eceran_" class="form-control angka" placeholder="Harga Eceran" required="">
                                    <input type="hidden" name="eceran" required="">
                                </div>
                                <div class="form-group col-xs-6" id="formGrosiran">
                                    <label class="label-control">Harga Grosir</label>
                                    <input type="text" name="grosir_" class="form-control angka" placeholder="Harga Grosir" required="">
                                    <input type="hidden" name="grosir" required="">
                                </div>
                                <div class="form-group col-xs-6">
                                    <select name="satuan_ids" class="form-control" id="satuan_harga" multiple="" required="">
                                        <option value="">Satuan</option>
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama}}</option>
                                        @endforeach
                                    </select>
                                    <select name="satuan_id" class="form-control hidden" id="satuan_hargas" required="" readonly="">
                                        {{-- <option value="">Satuan</option> --}}
                                        @foreach($relasi_satuans as $relasi_satuan)
                                        <option value="{{ $relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-6">
                                    <button class="btn btn-success" id="btnSimpanHarga" type="submit" style="width: 100%; height: 34px;" disabled="">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div id="formHapusContainer" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                            </form>
                        </div>
                        <div class="line"></div>

                        <table class="table table-bordered table-striped table-hover" id="tableHarga">
                            <thead>
                                <tr>
                                    <th style="display: none;">#</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Harga Eceran</th>
                                    <th class="text-center">Harga Grosir</th>
                                    <th class="text-center" style="width: 25px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($hargas as $i => $harga)
                                    <tr id="{{ $harga->id }}">
                                        <td style="display: none;">{{ $i++ }}</td>
                                        <td name="satuan_id" id="{{ $harga->satuan_id }}"> 1 {{ $harga->satuan->nama }}</td>
                                        <td class="text-right">{{ \App\Util::ewon($harga->eceran) }}</td>
                                        <td class="text-right">{{ \App\Util::ewon($harga->grosir) }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-warning btnUbah" id="UbahHarga" data-toggle="tooltip" data-placement="top" title="Ubah Harga Jual">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
        {{-- <div class="row">
            <!-- Batas Minimal Item -->
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Batas Minimal Stok</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form action="{{url('/item/limit-stok')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <!-- <input type="hidden" name="item_kode" value="{{$item_kode_super}}"> -->
                                <input type="hidden" name="kode_barang" value="{{$kode_barang_super}}">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" required="">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <select name="satuan_id_limit" class="form-control select2_single" id="satuan_bonus" required="">
                                            <option value="">Pilih Satuan</option>
                                            @foreach($relasi_satuans as $relasi_satuan)
                                            <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Batas Minimal Stok</h3>
                                        @if($limit_stok == 0 || $limit_stok == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div class="count text-center">{{ $limit_stok['jumlah'] }} {{ $limit_stok['satuan'] }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Batas Grosir Item -->
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Batas Minimal Grosir</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form action="{{url('/item/limit-grosir')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <!-- <input type="hidden" name="item_kode" value="{{$item_kode_super}}"> -->
                                <input type="hidden" name="kode_barang" value="{{$kode_barang_super}}">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" required="">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <select name="satuan_id_limit" class="form-control select2_single" id="satuan_bonus" required="">
                                            <option value="">Pilih Satuan</option>
                                            @foreach($relasi_satuans as $relasi_satuan)
                                            <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Batas Minimal Grosir</h3>
                                        @if($limit_grosir == 0 || $limit_grosir == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div class="count text-center">{{ $limit_grosir['jumlah'] }} {{ $limit_grosir['satuan'] }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        
        <div class="row">
            <!-- Diskon Item -->
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Diskon</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formSimpanContainer">
                            <form class="form-horizontal" action="{{url('/item/diskon')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <!-- <input type="hidden" name="item_kode" value="{{$item_kode_super}}"> -->
                                <input type="hidden" name="kode_barang" value="{{$kode_barang_super}}">
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <div class="input-group">
                                            <input type="text" name="persen" class="form-control" placeholder="Diskon Total" required="">
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <button class="btn btn-success" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Ubah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div class="row">
                                <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="tile-stats" style="background-color: #5cb85c; color: white;  padding: 20px; margin-top: 0px;">
                                        <h3 class="text-center" style="color: white">Diskon</h3>
                                        @if($item_detail->diskon == 0 || $item_detail->diskon == NULL)
                                            <div class="count text-center">-</div>
                                        @else
                                            <div id="diskon" class="diskon count text-center">{{ $item_detail->diskon }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div id="formHapusContainer" style="display: none;">
                                    <form method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="hidden" name="_method" value="post">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Bonus Item -->
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Bonus</h2>
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="formBonusContainer">
                            <form action="{{url('/item/bonus/create')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="item_kode" value="{{$item_kode_super}}">
                                <input type="hidden" name="bundle" value="1">
                                <div class="row">
                                    <div class="form-group col-xs-6">
                                        <input type="text" name="syarat" class="form-control select2_tinggi" placeholder="Jumlah Minimal Pembelian" required="">
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <select name="satuan_id" class="form-control select2_single" id="satuan_bonus" required="">
                                            <option value="">Pilih Satuan</option>
                                            @foreach($relasi_satuans as $relasi_satuan)
                                            <option value="{{$relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <select name="bonus" class="form-control select2_single" id="bonus" required="">
                                            <option value="">Pilih Bonus</option>
                                            @foreach($bonuses as $bonus)
                                            <option value="{{$bonus->id}}">{{$bonus->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <button class="btn btn-success" id="btnSimpan" type="submit" style="width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Tambah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <table class="table table-bordered table-striped table-hover" id="tableBonus">
                                <thead>
                                    <tr>
                                        <th class="text-left">Item Bonus</th>
                                        <th class="text-left">Syarat</th>
                                        <th class="text-left" style="width: 50px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($relasi_bonuses as $x => $relasi_bonus)
                                        <tr id="{{ $relasi_bonus->id }}">
                                            <td id="{{ $relasi_bonus->bonus_id }}">{{ $relasi_bonus->bonus->nama }}</td>
                                            <td id="{{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }}" class="{{ $satuan_bonuses[$x]['satuan']->id }}">
                                                {{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }} {{ $satuan_bonuses[$x]['satuan']->nama }}
                                            </td>
                                            <td class="text-center">
                                                <button class="btn btn-xs btn-warning btnUbah" id="UbahRB"data-toggle="tooltip" data-placement="top"  title="Ubah Bonus">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-xs btn-danger btnHapus" id="hapusRB"data-toggle="tooltip" data-placement="top"  title="Hapus Bonus">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="formHapusBonus" style="display: none;">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="_method" value="post">
                            <input type="hidden" name="bundle" value="1">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item Paket berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item Paket gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item Paket berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item Paket gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_diskon')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Diskon berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_stok')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Stok Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_grosir')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Grosir Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'tambah_bundle')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item paket berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah_bundle')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item ini sudah dimasukkan sebelumnya!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah_bundle')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item paket berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah_bundle')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item ini sudah dimasukkan sebelumnya!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'delete_bundle')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Item paket berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'delete_bundle')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Item paket gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'reset_harga')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Harga jual berhasil direset!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'delete_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableSatuan').DataTable();
        $('#tableHarga').DataTable();
        $('#tableBonus').DataTable();
        // $('#StokTable').DataTable();
        $('#BundleTable').DataTable();
        var items = [];

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            items = '{{ json_encode($item_list) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            // $('#formHargaJual').find('input').prop('disabled', true);
            // $('#formHargaJual').find('input').val('');
            // $('#formHargaJual').find('#satuan_ids').prop('disabled', true);
            $('#formHargaJual').find('button').prop('disabled', true);

            $(".select2_single").select2({
            });

            $('input[name="grosir_"]').prop('disabled', true);
            $('input[name="eceran_"]').prop('disabled', true);
            $('#satuan_harga').prop('disabled', true);

            $('.stok').each(function(index, el) {
                var text = $(el).text();
                text = text.split(' ');
                var stok = parseFloat(text[0]);
                if (stok <= 0) {
                    $(el).hide();
                    $(el).next().hide();
                }
            });

            $("#satuan_harga").select2({
                maximumSelectionLength: 1
            });

            $("#satuan_harga").val('').change();

            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);
                // console.log(item_kode);

                var satuan_item = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }
                // console.log(satuan_item);

                var item_jumlah = '';
                var item_konversi = '';
                // var jumlah1 = '';
                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                            item_jumlah = jumlah_jual;
                            item_konversi = satuan.konversi;

                            jumlah2 += jumlah_jual;
                            jumlah2 += ' ';
                            jumlah2 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;
                        }
                    }
                }
                // console.log(jumlah2);

                $(el).attr('jumlah', item_jumlah);
                $(el).attr('konversi', item_konversi);

                $(el).text(jumlah2);
                if (jumlah2 == '') $(el).text(jumlah);
            });

            $('.harga').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().find('#item_kode').attr('item_kode');
                var text = $(el).text();
                var harga = parseFloat(text.split('/')[0]);
                var jumlah = parseFloat(text.split('/')[1]);
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);
                // console.log(item_kode);

                var satuan_item = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }
                // console.log(satuan_item);

                // var jumlah1 = '';
                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                            jumlah2 += jumlah_jual;
                            jumlah2 += ' ';
                            jumlah2 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;
                        }
                    }
                }
                // console.log(jumlah2);

                harga = Math.round(harga);
                $(el).attr('harga', harga);

                harga = 'Rp' + harga.toLocaleString();

                $(el).text(harga + ' [' + jumlah2 + ']');
                if (jumlah2 == '') $(el).text(harga + ' [' + jumlah + ']');
            });

            $('.subtotal').each(function(index, el) {
                var id = $(el).attr('id');
                var nomor = id.split('-')[1];
                var subtotal = 0;
                $('.harga-'+nomor).each(function(index, el2) {
                    var harga = parseFloat($(el2).attr('harga'));
                    if (isNaN(harga)) harga = 0;
                    subtotal += harga;
                });
                // console.log(subtotal);
                $(el).text('Rp' + subtotal.toLocaleString());
            });

            var profit_eceran = parseFloat($('.profit_eceran').text());
            var profit_grosir = parseFloat($('.profit_grosir').text());
            var diskon = parseFloat($('.diskon').text());

            $('.profit_eceran').text(profit_eceran.toLocaleString() + '%');
            $('.profit_grosir').text(profit_grosir.toLocaleString() + '%');
            $('.diskon').text(diskon.toLocaleString() + '%');

            $('input').each(function (index, el) { 
              // console.log($(el).attr('name')); 
                $(el).prop('disabled', 'true');
            });

            $('select').each(function (index, el) { 
                $(el).prop('disabled', 'true');
            });

            $('button').each(function (index, el) { 
                // if ($(el).attr('id') == 'btnKembali'){
                $(el).prop('disabled', 'true');
                // }
            });
        });

        $(document).on('click', '#UbahHarga', function(event) {
            event.preventDefault();

            var $form = $('#formSimpanHargaContainer').find('form');
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var ecer = $tr.find('td').first().next().next().text().split('Rp');
            var grosir = $tr.find('td').first().next().next().next().text().split('Rp');
            // $('input[name="jumlah"]').val(jumlah);
            $('#satuan_harga').val(satuan_id);
            $('#satuan_hargas').val(satuan_id).change();
            $('input[name="eceran_"]').prop('disabled', false);
            $('input[name="eceran_"]').val(ecer[1]);
            $('input[name="eceran"]').val(parseInt(ecer[1].replace(/\D/g, ''), 10));

            $('input[name="grosir_"]').prop('disabled', false);
            $('input[name="grosir_"]').val(grosir[1]);
            $('input[name="grosir"]').val(parseInt(grosir[1].replace(/\D/g, ''), 10));

            $form.attr('action', '{{ url("harga") }}' + '/' + id + '/update');
            $('#btnSimpanHarga span').text('Ubah');

            $(this).parents('.x_content').find('#btnSimpanHarga').prop('disabled', false);
        });

        $(document).on('click', '#ubah_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $(this).parents('tr').attr('id');
            var satu = $tr.find('td').first().data('satu');
            var konversi = $tr.find('td').first().next().text();
            
            $('#satuan_relasi').val(satu).trigger("change")
            // $('select[name="satuanID"]').val(satu);
            $('input[name="konversi"]').val(konversi);

            $('#formSimpanSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
            $('#formSimpanSatuan').find('input[name="_method"]').val('put');
            $('#btnSimpanSatuan span').text('Ubah');
        });

        $(document).on('click', '#UbahRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().attr('id');
            var syarat = $tr.find('td').first().next().attr('id');
            var satuan = $tr.find('td').first().next().attr('class');

            $('#bonus').val(bonus).trigger('change');
            $('#satuan_bonus').val(satuan).trigger('change');

            $('input[name="syarat"]').val(syarat);
            
            $('#formBonusContainer').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id  + '/update');
            $('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapusRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().text();
            var kode = $('#item_kode').text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: 'Bonus \ "' + bonus + '\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusBonus').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id + '/' + kode +'/delete');
                $('#formHapusBonus').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_harga', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id + '/delete');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $tr.find('td').first().text();
            var konversi = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + satuan_id + ' ' + konversi +'\" akan dihapus!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
                $('#formHapusSatuan').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('change', '#item_list', function(event) {
            event.preventDefault();
            var kode = $(this).val();
            var satuans = [];
            var satuan_list = '<option value="">Pilih Satuan</option>';

            for(var i=0; i<items.length; i++) {
                var item = items[i];
                if(item.kode == kode){
                    satuans = item.satuan_pembelians;
                }
            }

            for(var i=0; i<satuans.length; i++){
                satuan_list += `<option value="`+satuans[i].konversi+`">`+satuans[i].satuan.kode+`</option>`;
            }

            $('#satuan_bundle').empty();
            $('#satuan_bundle').append(satuan_list);
        });

        $(document).on('click', '#UbahBundle', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // var item_kode = $tr.find('td').first().next().attr('kode');
            // var jumlah = $tr.find('td').first().next().next().attr('jumlah');
            // var konversi = $tr.find('td').first().next().next().attr('konversi');
            var item_kode = $tr.attr('item_kode');
            var jumlah = $tr.find('.jumlah').attr('jumlah');
            var konversi = $tr.find('.jumlah').attr('konversi');
            // var hasil = jumlah / konversi;
            var hasil = jumlah;
            // console.log(item_kode, jumlah, konversi, hasil);

            $('#item_list').val(item_kode).trigger("change");
            $('#item_list').prop('disabled', true);
            $('#jumlah_bundle').val(hasil);

            var satuans = [];
            var satuan_list = '<option value="">Pilih Satuan</option>';

            for(var i=0; i<items.length; i++) {
                var item = items[i];
                if(item.kode == item_kode){
                    satuans = item.satuan_pembelians;
                }
            }

            for(var i=0; i<satuans.length; i++){
                if(satuans[i].konversi == konversi){
                    satuan_list += `<option selected value="`+satuans[i].konversi+`">`+satuans[i].satuan.kode+`</option>`;
                }else{
                    satuan_list += `<option value="`+satuans[i].konversi+`">`+satuans[i].satuan.kode+`</option>`;
                }
            }

            $('#satuan_bundle').empty();
            $('#satuan_bundle').append(satuan_list);

            $('#formBundleContainer').find('form').attr('action', '{{ url("item/bundle") }}' + '/' + id  + '/update');
            // $('#formBundleContainer').find('form').attr('method', 'put');
            $('#formBundleContainer').find('input[name="_method"]').val('put');
            $('#formBundleContainer').find('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapusBundle', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var item = $tr.find('td').first().next().text();
            var item_kode = '{{ $item_kode_super }}';
            // $('#formHapusBundle').find('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + item +'\" akan dihapus dari paket Paket!',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusBundle').find('form').attr('action', '{{ url("relasi_bundle") }}' + '/' + id + '/' + item_kode);
                $('#formHapusBundle').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('keyup', 'input[name="eceran_"]', function(event) {
            event.preventDefault();
            // cekHarga();
            // var ecer = $('input[name="eceran"]').val();
            var ecer = parseInt($(this).val().replace(/\D/g, ''), 10);

            var is_ratusan = ecer % 100 == 0;
            if (is_ratusan) {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', false);
                $('#formEceran').removeClass('has-error');
            } else {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
                $('#formEceran').addClass('has-error');
            }
            cekHarga();
        });

        $(document).on('keyup', 'input[name="grosir_"]', function(event) {
            event.preventDefault();
            // var grosir = $('input[name="grosir"]').val();
            var grosir = parseInt($(this).val().replace(/\D/g, ''), 10);
            var is_ratusan = grosir % 100 == 0;
            if (is_ratusan) {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', false);
                $('#formGrosiran').removeClass('has-error');
            } else {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
                $('#formGrosiran').addClass('has-error');
            }
            cekHarga();
        });

        function cekHarga() {
            if ($('#formGrosiran').hasClass('has-error') || $('#formEceran').hasClass('has-error')) {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', true);
            } else {
                $('#formSimpanHargaContainer').find('#btnSimpanHarga').prop('disabled', false);
            }
        }

        $(document).on('click', '#btnResetHarga', function(event) {
            event.preventDefault();
            var kode = '{{ json_encode($item_kodes[0]->kode) }}';
            kode = kode.replace(/&quot;/g, '"');
            kode = JSON.parse(kode);

            var id = kode;
            var url = '{{ url('item/reset/harga-bundle/') }}' + '/' + id;

            swal({
                title: 'Reset Harga Jual?',
                text: 'Harga jual akan direset!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Reset!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(isConfirm) {
                //confirmed
                // $('#formHapusContainer').find('form').attr('action', '{{ url("user") }}' + '/' + id);
                window.open(url, '_self');
            }).catch(function(isConfirm) {
                //canceled
            });
        });
    </script>
@endsection
