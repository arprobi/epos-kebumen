@extends('layouts.admin')

@section('title')
    <title>EPOS | Detail Riwayat Item {{ $item->nama }}</title>
@endsection

@section('style')
    <style media="screen">
        .btnUbah, .btnHapus {
            margin-bottom: 0;
        }
        .btnSimpan {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .dataTables_filter input {
            max-width: 150px;
        }
        #btnUbah, #btnKembali {
            margin-bottom: 0;
        }
        td span {
            line-height: 1.42857143;
        }
    </style>
@endsection

@section('content')
    <!-- Detail Item -->
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Detail Item <b>{{ $item->nama }}</b></h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th style="width:50%">Kode Barang</th>
                                    <td id="item_kode">{{ $item->kode_barang }}</td>
                                </tr>
                                <tr>
                                    <th style="width:50%">Kode Item</th>
                                    <td id="item_kode">
                                        @foreach ($item_kodes as $i => $item_kode )
                                        <span class="label label-info">{{ $item_kode->kode }}</span> 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width:50%">Nama Pendek</th>
                                    <td>{{ $item->nama_pendek }}</td>
                                </tr>
                                <tr>
                                    <th style="width:50%">Nama Item</th>
                                    <td>{{ $item->nama }}</td>
                                </tr>
                                <tr>
                                    <th style="width:50%">Pemasok</th>
                                    <td>
                                        {{-- @foreach ($items as $i => $item )
                                        <span class="label label-info">{{ $item->suplier->nama }}</span> 
                                        @endforeach --}}
                                        @foreach ($supliers as $i => $suplier )
                                        <span class="label label-info">{{ $suplier->nama }}</span> 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Jenis Item</th>
                                    <td>{{ $item->jenis_item->nama}}</td>
                                </tr>
                                <tr>
                                    <th>Stok</th>
                                    <!-- <td>{{ $stok->stok }} PCS</td> -->
                                    <td>
                                        @for ($i = 0; $i < count($stok_total); $i++)
                                            <span class="label label-info stok">{{ $stok_total[$i]['jumlah'] }} {{ $stok_total[$i]['satuan'] }}</span>
                                        @endfor
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Profit Margin</th>
                                    <td id="profit">{{ $item->profit }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="formHapusItemKode" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="delete">
                                <input type="hidden" name="hapus_item_kode" value="">
                            </form>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="BarcodeTable">
                            <thead>
                                <tr>
                                    <th class="sembunyi"></th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center" style="width: 20%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item_kodes as $i => $item_kode)
                                <tr id="{{ $item_kode->kode }}" var="{{ $a = $i + 1 }}">
                                    <td class="sembunyi">{{ $a }}</td>
                                    <td>{{ $item_kode->kode }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-danger btnHapus" id="hapus_item_kode" title="Hapus Barcode" disabled="">
                                            <i class="fa fa-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Stok Item -->
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Stok</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                {{-- <a href="{{ url('item/edit/'.$item->kode) }}" class="btn btn-sm btn-primary pull-right" id="btnUbah">
                    <i class="fa fa-edit"></i> Ubah
                </a> --}}
                <a href="{{ url('item') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {{-- <div class="table-responsive"> --}}
                    <table class="table table-bordered table-striped table-hover" id="StokTable">
                        <thead>
                            <tr>
                                <th class="sembunyi"></th>
                                <th class="text-center">Tanggal Masuk</th>
                                <th class="text-center">Pemasok</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Tanggal Kadaluarsa</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stoks as $i => $stok)
                            <tr id="{{ $stok->id }}" var="{{ $a = $i + 1 }}">
                                <td class="sembunyi">{{ $a }}</td>
                                <td>{{ $stok->created_at->format("d-m-Y") }}</td>
                                <td>{{ $stok->item->suplier->nama }}</td>
                                <td style="text-align:center;"><!-- {{$stok->jumlah}} -->
                                    @for ($j = 0; $j < count($stok_pcs[$i]); $j++)
                                    <span class="stok">{{ $stok_pcs[$i][$j]['jumlah'] }} {{ $stok_pcs[$i][$j]['satuan'] }}</span><br>
                                    @endfor
                                </td>
                                <td style="text-align: right;"> 
                                    @for ($j = 0; $j < count($harga_satuan[$i]); $j++)
                                        {{ \App\Util::duit0($harga_satuan[$i][$j]['harga'] * 1.1) }} /{{$harga_satuan[$i][$j]['satuan'] }}
                                        <br>
                                    @endfor
                                </td>
                                <td class="tanggal">{{ $stok->kadaluarsa }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                {{-- </div> --}}
            </div>
        </div>
    </div>

    <div class="row"></div>

    <!-- Satuan Item -->
    <div class="col-md-6" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Satuan Item</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="formSimpanSatuan">
                            <form class="form-horizontal" action="{{url('/relasi_satuan')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                                <div class="row">
                                    <div class="form-group col-xs-5">
                                        <select name="satuanID" class="form-control select2_single" id="satuan_relasi" required="" disabled="">
                                            <option>Pilih satuan</option>
                                            @foreach ($satuans as $satuan)
                                            <option value="{{ $satuan->id }}">{{ $satuan->kode }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <input type="text" name="konversi" class="form-control" placeholder="Konversi" required="" disabled="" style="height: 38px">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <button class="btn btn-sm btn-success btnSimpan pull-right" id="btnSimpanSatuan" type="submit" disabled="" style="margin-right: 0; width: 100%; height: 34px;">
                                            <i class="fa fa-save"></i> <span>Tambah</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div id="formHapusSatuan" style="display: none;">
                                <form method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </div>
                            <table class="table table-bordered table-striped table-hover" id="tableSatuan">
                                <thead>
                                    <tr>
                                        <th class="text-left">Jenis Satuan</th>
                                        <th class="text-left">Konversi</th>
                                        <th class="text-left" style="width: 31%;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($relasi_satuans as $relasi_satuan)
                                    <tr id="{{ $relasi_satuan->id }}">
                                        <td data-satu="{{ $relasi_satuan->satuan_id }}">{{ $relasi_satuan->satuan->kode }}</td>
                                        <td class="duit">{{ $relasi_satuan->konversi }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-warning btnUbah" id="ubah_relasi_satuan" data-toggle="tooltip" data-placement="top" title="Ubah Satuan" disabled="">
                                                <i class="fa fa-edit"></i>\
                                            </button>
                                            <button class="btn btn-xs btn-danger btnHapus" id="hapus_relasi_satuan" data-toggle="tooltip" data-placement="top" title="Hapus Satuan" disabled="">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Harga Item -->
    <div class="col-md-6" id="formSimpanHargaContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2>Harga Jual</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form action="{{url('/harga')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label class="label-control">Harga Eceran</label>
                            <input type="text" name="eceran_" class="form-control angka" placeholder="Harga Eceran">
                            <input type="hidden" name="eceran" required="">
                        </div>
                        <div class="form-group col-xs-6">
                            <label class="label-control">Harga Grosir</label>
                            <input type="text" name="grosir_" class="form-control angka" placeholder="Harga Grosir">
                            <input type="hidden" name="grosir" required="">
                        </div>
                        <div class="form-group col-xs-6">
                            <select name="satuan_id" class="form-control" id="satuan_harga" required="">
                                <option>Pilih satuan</option>
                                @foreach($relasi_satuans as $relasi_satuan)
                                <option value="{{ $relasi_satuan->satuan->id}}">{{$relasi_satuan->satuan->kode }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <button class="btn btn-success" id="btnSimpanHarga" type="submit" style="width: 100%; height: 34px;" disabled="">
                                <i class="fa fa-save"></i> <span>Tambah</span>
                            </button>
                        </div>
                    </div>
                </form>
                <div id="formHapusContainer" style="display: none;">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
                <div class="line"></div>

                <table class="table table-bordered table-striped table-hover" id="tableHarga">
                    <thead>
                        <tr>
                            <th style="display: none;">#</th>
                            <th class="text-center">Satuan</th>
                            <th class="text-center">Harga Eceran</th>
                            <th class="text-center">Harga Grosir</th>
                            <th class="text-center" style="width: 15%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($hargas as $i => $harga)
                            <tr id="{{ $harga->id }}">
                                <td style="display: none;">{{ $i++ }}</td>
                                <td name="satuan_id" id="{{ $harga->satuan_id }}"> 1 {{ $harga->satuan->kode }}</td>
                                <td class="text-right">{{ \App\Util::ewon($harga->eceran) }}</td>
                                <td class="text-right">{{ \App\Util::ewon($harga->grosir) }}</td>
                                <td class="text-center">
                                    <button class="btn btn-xs btn-warning btnUbah" id="UbahHarga" data-toggle="tooltip" data-placement="top" title="Ubah Harga Jual" disabled="">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row"></div>

    <!-- Bonus Item -->
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Bonus</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="formBonusContainer">
                    <form action="{{ url('/item/bonus/create') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="item_kode" value="{{ $item->kode }}">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <input type="text" name="syarat" class="form-control" placeholder="Jumlah Minimal" required="" disabled="">
                            </div>
                            <div class="form-group col-xs-6">
                                <select name="satuan_id" class="form-control" id="satuan_bonus" required="" disabled="">
                                    <option>Pilih Satuan</option>
                                    @foreach ($relasi_satuans as $relasi_satuan)
                                    <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->kode }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <select name="bonus" class="form-control" id="bonus" required="" disabled="">
                                    <option>Pilih Bonus</option>
                                    @foreach ($bonuses as $bonus)
                                    <option value="{{ $bonus->id }}">{{ $bonus->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-6">
                                <button class="btn btn-success" id="btnSimpan" type="submit" disabled="" style="width: 100%; height: 34px;">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped table-hover" id="tableBonus">
                        <thead>
                            <tr>
                                <th class="text-left">Item Bonus</th>
                                <th class="text-left">Syarat</th>
                                <th class="text-left" style="width: 31%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($relasi_bonuses as $x => $relasi_bonus)
                                <tr id="{{ $relasi_bonus->id }}">
                                    <td id="{{ $relasi_bonus->bonus_id }}">{{ $relasi_bonus->bonus->nama }}</td>
                                    <td id="{{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }}" class="{{ $satuan_bonuses[$x]['satuan']->id }}">
                                        {{ $relasi_bonus->syarat / $satuan_bonuses[$x]['konversi'] }} {{ $satuan_bonuses[$x]['satuan']->kode }}
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-warning btnUbah" id="UbahRB" data-toggle="tooltip" data-placement="top" title="Ubah Bonus" disabled="">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-xs btn-danger btnHapus" id="hapusRB" data-toggle="tooltip" data-placement="top" title="Hapus Bonus" disabled="">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div id="formHapusBonus" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                </form>
            </div>
        </div>
    </div>

    <!-- Diskon Item -->
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Diskon</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="formSimpanContainer">
                    <form class="form-horizontal" action="{{url('/item/diskon')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <div class="input-group">
                                    <input type="text" name="persen" class="form-control" placeholder="Diskon" required="" disabled="">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="form-group col-xs-6">
                                <button class="btn btn-success" id="btnSimpan" type="submit" disabled="" style="width: 100%; height: 34px;">
                                    <i class="fa fa-save"></i> <span>Ubah</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tile-stats" style="background-color: #5cb85c; color: white;  padding: 20px; margin-top: 0px;">
                                <h3 class="text-center" style="color: white">Diskon</h3>
                                @if($item->diskon == 0 || $item->diskon == NULL)
                                    <div class="count text-center">-</div>
                                @else
                                    <div id="diskon" class="count text-center">{{ $item->diskon }}</div>
                                @endif
                            </div>
                        </div>
                        <div id="formHapusContainer" style="display: none;">
                            <form method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row"></div>

    <!-- Batas Minimal Item -->
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Batas Minimal Stok</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="formSimpanContainer">
                    <form action="{{ url('/item/limit-stok') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" required="" disabled="">
                            </div>
                            <div class="form-group col-md-5">
                                <select name="satuan_id_limit" class="form-control" id="satuan_bonus" required="" disabled="">
                                    <option>Pilih satuan</option>
                                    @foreach ($relasi_satuans as $relasi_satuan)
                                    <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->kode }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <button class="btn btn-success pull-right" id="btnSimpan" disabled="" type="submit" style="width: 100%; height: 34px;">
                                    <i class="fa fa-save"></i> <span>Ubah</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                <h3 class="text-center" style="color: white">Batas Minimal Stok</h3>
                                @if($limit_stok == 0 || $limit_stok == NULL)
                                    <div class="count text-center">-</div>
                                @else
                                    <div class="count text-center">{{ $limit_stok['jumlah'] }} {{ $limit_stok['satuan'] }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Batas Grosir Item -->
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Batas Minimal Grosir</h2>
                <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                    <div class="pull-right">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="formSimpanContainer">
                    <form action="{{ url('/item/limit-grosir') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="post">
                        <input type="hidden" name="kode_barang" value="{{ $item->kode_barang }}">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" required="" disabled="">
                            </div>
                            <div class="form-group col-md-5">
                                <select name="satuan_id_limit" class="form-control" id="satuan_bonus" required="" disabled="">
                                    <option>Pilih satuan</option>
                                    @foreach ($relasi_satuans as $relasi_satuan)
                                    <option value="{{ $relasi_satuan->satuan->id }}">{{ $relasi_satuan->satuan->kode }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <button class="btn btn-success pull-right" id="btnSimpan" type="submit" disabled="" style="width: 100%; height: 34px;">
                                    <i class="fa fa-save"></i> <span>Ubah</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tile-stats" style="background-color: #5cb85c; color: white; padding: 20px; margin-top: 0px;">
                                <h3 class="text-center" style="color: white">Batas Minimal Grosir</h3>
                                @if($limit_grosir == 0 || $limit_grosir == NULL)
                                    <div class="count text-center">-</div>
                                @else
                                    <div class="count text-center">{{ $limit_grosir['jumlah'] }} {{ $limit_grosir['satuan'] }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('harga_gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Duplikat Data!\n harga gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Data berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Data gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'tambah_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_diskon')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Diskon berhasil di ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_stok')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Stok Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'update_limit_grosir')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Grosir Minimal berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'delete_bonus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @endif

    <script type="text/javascript"> 
        $('#tableHarga').DataTable();
        $('#tableBonus').DataTable();
        $('#StokTable').DataTable();
        $('#BarcodeTable').DataTable();

        $(document).ready(function() {
            var url = "{{ url('item') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parents('ul').show();
            a.parents('li').addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
            });

            $('input[name="grosir_"]').prop('readonly', true);
            $('input[name="eceran_"]').prop('readonly', true);
            $('#satuan_harga').prop('disabled', true);

            $('.stok').each(function(index, el) {
                var text = $(el).text();
                text = text.split(' ');
                var stok = parseFloat(text[0]);
                if (stok <= 0) {
                    $(el).hide();
                    $(el).next().hide();
                }
            });

            var profit = parseFloat($('#profit').text());
            var diskon = parseFloat($('#diskon').text());

            $('#profit').text(profit.toLocaleString() + '%');
            $('#diskon').text(diskon.toLocaleString() + '%');
        });

        /*$(document).on('click', '#UbahHarga', function(event) {
            event.preventDefault();

            var $form = $('#formSimpanHargaContainer').find('form');
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // var jumlah = $tr.find('td').first().text();
            var satuan_id = $(this).parents('tr').first().find('td[name="satuan_id"]').attr('id');
            var ecer = $tr.find('td').first().next().next().text().split('Rp');
            var grosir = $tr.find('td').first().next().next().next().text().split('Rp');
            // $('input[name="jumlah"]').val(jumlah);
            $('#satuan_harga').val(satuan_id);
            $('input[name="eceran_"]').prop('readonly', false);
            $('input[name="eceran_"]').val(ecer[1]);
            $('input[name="eceran"]').val(parseInt(ecer[1].replace(/\D/g, ''), 10));

            $('input[name="grosir_"]').prop('readonly', false);
            $('input[name="grosir_"]').val(grosir[1]);
            $('input[name="grosir"]').val(parseInt(grosir[1].replace(/\D/g, ''), 10));

            $form.attr('action', '{{ url("harga") }}' + '/' + id + '/update');
            $('#btnSimpanHarga span').text('Ubah');

            $(this).parents('.x_content').find('#btnSimpanHarga').prop('disabled', false);
        });

        $(document).on('click', '#ubah_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $(this).parents('tr').attr('id');
            var satu = $tr.find('td').first().data('satu');
            var konversi = $tr.find('td').first().next().text();
            
            $('#satuan_relasi').val(satu).trigger("change")
            // $('select[name="satuanID"]').val(satu);
            $('input[name="konversi"]').val(konversi);

            $('#formSimpanSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
            $('#formSimpanSatuan').find('input[name="_method"]').val('put');
            $('#btnSimpanSatuan span').text('Ubah');
        });

        $(document).on('click', '#UbahRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().attr('id');
            var syarat = $tr.find('td').first().next().attr('id');
            var satuan = $tr.find('td').first().next().attr('class');

            $('#bonus').val(bonus).trigger('change');
            $('#satuan_bonus').val(satuan).trigger('change');

            $('input[name="syarat"]').val(syarat);
            
            $('#formBonusContainer').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id  + '/update');
            $('#btnSimpan span').text('Ubah');
        });

        $(document).on('click', '#hapusRB', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var bonus = $tr.find('td').first().text();
            var kode = $('#item_kode').text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: 'Bonus \ "' + bonus + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusBonus').find('form').attr('action', '{{ url("item/bonus") }}' + '/' + id + '/' + kode +'/delete');
                $('#formHapusBonus').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_harga', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var jumlah = $tr.find('td').first().text();
            var satuan_id = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + jumlah + ' ' + satuan_id +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("harga") }}' + '/' + id + '/delete');
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_relasi_satuan', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var satuan_id = $tr.find('td').first().text();
            var konversi = $tr.find('td').first().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + satuan_id + ' ' + konversi +'\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusSatuan').find('form').attr('action', '{{ url("relasi_satuan") }}' + '/' + id);
                $('#formHapusSatuan').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });

        $(document).on('click', '#hapus_item_kode', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var item_kode = $tr.attr('id');
            $('#formHapusItemKode input[name="item_kode"]').val(item_kode);

            swal({
                title: 'Hapus?',
                text: '\"' + item_kode + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            })  .then(function() {
                //confirmed
                $('#formHapusItemKode').find('form').attr('action', '{{ url("item") }}' + '/' + item_kode + '/kode');
                $('#formHapusItemKode').find('form').submit();
            }, function(isConfirm) {
                //canceled
            });
        });*/

    </script>
@endsection
