@extends('layouts.admin')

@section('title')
    <title>EPOS | Neraca</title>
@endsection

@section('style')
    <style media="screen">
    	#btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        table>thead>tr>th {
        	text-align: center;
        }
        .panel_toolbox li {
        	cursor: pointer;
        }

        .menjorok {
        	padding-left: 35px;
        }
        .garis{
        	border-style: solid;
        	border-width: 2px;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
        	<div class="x_title">
				<h2 id="formSimpanTitle">Neraca Saat Ini</h2>
					<ul class="nav navbar-right panel_toolbox">
						<div class="pull-right">	
			            	<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			            </div>
			        </ul>
				<div class="clearfix"></div>
	  		</div>
      		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
    				<div class="x_title col-md-12">
    					<form method="post" action="{{ url('neraca/tampil') }}" class="form-horizontal">
	    					<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="_method" value="post">
	    					<div class="row" style="padding-bottom: 20px">
	    						<div class="col-md-2"><h4><strong>Pilih Data Saldo</strong></h4></div>
	    						<div class="col-md-3">
	    							<select name="data[]" multiple id="data" class="form-control">
	    								<option value="">Pilih Data</option>
	    								@foreach($saldo as $x)
	    								<option value="{{ $x->date_strip }}">{{ App\Util::tanggal($x->date_space) }}</option>
								        @endforeach
	    							</select>
	    						</div>
	    						<div class="col-md-2">
	    							<button class="btn btn-sm btn-success" id="btnUbah" type="submit">
										<span style="color:white">Tampil</span>
									</button>
	    						</div>
	    					</div>
						</form>
    				</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-9" style="padding-bottom: 20px"><strong>AKTIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-9">Kas Tunai</div>
							<div class="col-md-3 text-right">{{ $data['KasTunai'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Kas Kecil</div>
							<div class="col-md-3 text-right">{{ $data['KasKecil'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Kas Bank</div>
							<div class="col-md-3 text-right">{{ $data['KasBank'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Kas Cek</div>
							<div class="col-md-3 text-right">{{ $data['KasCek'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Kas BG</div>
							<div class="col-md-3 text-right">{{ $data['KasBG'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Persediaan</div>
							<div class="col-md-3 text-right">{{ $data['Persediaan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Perlengkapan</div>
							<div class="col-md-3 text-right">{{ $data['Perlengkapan'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Piutang Dagang</div>
							<div class="col-md-3 text-right">{{ $data['PiutangDagang'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Piutang Karyawan</div>
							<div class="col-md-3 text-right">{{ $data['PiutangKaryawan'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Piutang Lain</div>
							<div class="col-md-3 text-right">{{ $data['PiutangLain'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Piutang Tak Tertagih</div>
							<div class="col-md-3 text-right">{{ $data['PiutangTakTertagih'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Asuransi Bayar Dimuka</div>
							<div class="col-md-3 text-right">{{ $data['AsuransiDimuka'] }}</div>
						</div><div class="row">
							<div class="col-md-9">Sewa Bayar Dimuka</div>
							<div class="col-md-3 text-right">{{ $data['SewaDimuka'] }}</div>
						</div>
						</div><div class="row">
							<div class="col-md-9">PPN Masukan</div>
							<div class="col-md-3 text-right">{{ $data['PPNMasukan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9 menjorok"><strong>Aset Lancar</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['AsetLancar'] }}</strong></div>
						</div>
						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-9">Tanah</div>
							<div class="col-md-3 text-right">{{ $data['Tanah'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Bangunan</div>
							<div class="col-md-3 text-right">{{ $data['Bangunan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Peralatan</div>
							<div class="col-md-3 text-right">{{ $data['Peralatan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Kendaraan</div>
							<div class="col-md-3 text-right">{{ $data['Kendaraan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Akumulasi Depresiasi Bangunan</div>
							<div class="col-md-3 text-right">({{ $data['ADBangunan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-9">Akumulasi Depresiasi Peralatan</div>
							<div class="col-md-3 text-right">({{ $data['ADPeralatan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-9">Akumulasi Depresiasi Peralatan</div>
							<div class="col-md-3 text-right">({{ $data['ADKendaraan'] }})</div>
						</div>
						<div class="row">
							<div class="col-md-9 menjorok"><strong>Aset Tetap</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['AsetTetap'] }}</strong></div>
						</div>
					</div>

					<div class="col-md-6 garis" style="margin-bottom: -99999px; padding-bottom: 99999px;">
						<div class="row">
							<div class="col-md-9" style="padding-bottom: 20px"><strong>PASIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-9"><strong>KEWAJIBAN</strong></div>
						</div>
						<div class="row">
							<div class="col-md-9">Kartu Kredit</div>
							<div class="col-md-3 text-right">{{ $data['KartuKredit'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Hutang Dagang</div>
							<div class="col-md-3 text-right">{{ $data['HutangDagang'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Hutang Pajak</div>
							<div class="col-md-3 text-right">{{ $data['HutangPajak'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Taksiran Hutang Garansi</div>
							<div class="col-md-3 text-right">{{ $data['TaksiranUtangGaransi'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Hutang Konsinyasi</div>
							<div class="col-md-3 text-right">{{ $data['HutangKonsinyasi'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Pendapatan Dibayar Dimuka</div>
							<div class="col-md-3 text-right">{{ $data['PendapatanDimuka'] }}</div>
						</div>

						<div class="row">
							<div class="col-md-9 menjorok"><strong>Hutang Jangka Pendek</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['HutangJangkaPendek'] }}</strong></div>
						</div>

						<div class="row">
							<div class="col-md-9">HutangBank</div>
							<div class="col-md-3 text-right">{{ $data['HutangBank'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Hutang PPN</div>
							<div class="col-md-3 text-right">{{ $data['HutangPPN'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">PPN Keluaran</div>
							<div class="col-md-3 text-right">{{ $data['PPNKeluaran'] }}</div>
						</div>
						
						<div class="row">
							<div class="col-md-9 menjorok"><strong>Hutang Jangka Panjang</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['HutangJangkaPanjang'] }}</strong></div>
						</div>
						<div class="row">
							<div class="col-md-9"><strong>Total Kewajiban</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['Kewajiban'] }}</strong></div>
						</div>

						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-9"><strong>EQUITAS</strong></div>
						</div>
						<div class="row">
							<div class="col-md-9">Modal</strong></div>
							<div class="col-md-3 text-right">{{ $data['Modal'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Laba di Tahan</div>
							<div class="col-md-3 text-right">{{ $data['LabaTahan'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">Prive</div>
							<div class="col-md-3 text-right">{{ $data['Prive'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9">BagiHasil</div>
							<div class="col-md-3 text-right">{{ $data['BagiHasil'] }}</div>
						</div>
						<div class="row">
							<div class="col-md-9"><strong>Total Equitas</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['Equitas'] }}</strong></div>
						</div>
					</div>
    			</div>

    			<div class="row">
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 20px">
							<div class="col-md-9"><strong>Total Aset</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['Aset'] }}</strong></div>
						</div>
    				</div>
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 20px">
							<div class="col-md-9"><strong>Total Kewajiban + Equitas</strong></div>
							<div class="col-md-3 text-right"><strong>{{ $data['TotalKE'] }}</strong></div>
						</div>
    				</div>
    			</div>

      		</div>
    	</div>
  	</div>
</div>	


@endsection

@section('script')

	@if (session('sukses') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil ditambah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'tambah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal ditambah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal diubah!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@elseif (session('sukses') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Data berhasil dihapus!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'hapus')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Data gagal dihapus!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableJenisItem').DataTable();

		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');

			$("#data").select2({
				maximumSelectionLength: 2
			});
		});
	</script>
@endsection
