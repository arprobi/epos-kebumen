@extends('layouts.print')

@section('app.style')
	<style>
		.garis{
        	border-style: solid;
        	border-width: 2px;
        }

		.kanan{
        	text-align: right;
        }

        .x_content{
        	font-size: 12px;
        	line-height: 120%;
        }

        .text-center{
        	text-align: center
        }

        .text-right{
        	text-align: right
        }

        .gemuk {
        	font-weight: bold;
        }
		@font-face
			{font-family:"Cambria Math";
			panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
			{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
		@font-face
			{font-family:"Clarendon Blk BT";
			panose-1:2 4 9 5 5 5 5 2 2 4;}
		 /* Style Definitions */
		 p.MsoNormal, li.MsoNormal, div.MsoNormal
			{margin-top:0cm;
			margin-right:0cm;
			margin-bottom:8.0pt;
			margin-left:0cm;
			line-height:107%;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin:0cm;
			margin-bottom:.0001pt;
			font-size:11.0pt;
			font-family:"Calibri","sans-serif";}
		a:link, span.MsoHyperlink
			{color:#0563C1;
			text-decoration:underline;}
		a:visited, span.MsoHyperlinkFollowed
			{color:#954F72;
			text-decoration:underline;}
		span.FooterChar
			{mso-style-name:"Footer Char";
			mso-style-link:Footer;}
		.MsoChpDefault
			{font-family:"Calibri","sans-serif";}
		.MsoPapDefault
			{margin-bottom:8.0pt;
			line-height:107%;}
		@page WordSection1
			{size:841.9pt 595.3pt;
			margin:1.0cm 1.0cm 1.0cm 1.0cm;}
		div.WordSection1
			{page:WordSection1;}
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

	</style>
@endsection

@section('app.body')
	<div class=WordSection1>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><img width=185 height=100 src="{{ URL::to('images/image001.png') }}" align=left hspace=12><b><span style='font-size:14.0pt;line-height:107%; font-family:"Clarendon Blk BT","serif"'>KENCANA MULYA</span></b></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='font-size:14.0pt;line-height:107%;font-family: "Clarendon Blk BT","serif"'>KARANGDUWUR-PETANAHAN</span></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Jl. Puring-Petanahan No. 3 Karangduwur, Kec. Petanahan, Kab. Kebumen</span></p>
		<p class=MsoNormal align=center style='margin-bottom:3px; text-align:center'><a href="mailto:kencanamulyakarangduwur@gmail.com"><span style='line-height:107%;font-family:"Arial","sans-serif"'>kencanamulyakarangduwur@gmail.com</span></a></p>
		<p class=MsoNormal align=center style='margin-bottom:0cm; margin-bottom:.0001pt; text-align:center'><span style='line-height:107%;font-family:"Arial","sans-serif"'>Telp./Fax (0287) 66 55 492 | WA 0878 6 33 000 11 (XL)</span></p>
		
		<p class=MsoFooter>

		<table cellpadding=0 cellspacing=0 align=left>
			 <tr>
				  <td width=0 height=5></td>
			 </tr>
			 <tr>
				  <td></td>
				  <td><img width=1049 height=5 src="{{ URL::to('images/image0021.png') }}"></td>
			 </tr>
		</table>

		<center style="padding-top: 40px; padding-bottom: 20px; font-size: 12px">
			<STRONG>KENCANA MULYA </STRONG><br/>
			<STRONG>LAPORAN POSISI KEUANGAN </STRONG><br/>
		</center>

		<div class="x_content">
    			<div class="row" style="overflow: hidden;">
					<div class="col-md-6 garis">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>AKTIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							@foreach($tanggal as  $i => $x)
									<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px"><strong>{{ $x }}</strong></div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6"><strong>ASET LANCAR</strong></div>
						</div>

						<div class="row">
							<div class="col-md-6">Kas Tunai</div>
							@foreach($akuns['KasTunai'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Kecil</div>
							@foreach($akuns['KasKecil'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Bank</div>
							@foreach($akuns['KasBank'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas Cek</div>
							@foreach($akuns['KasCek'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kas BG</div>
							@foreach($akuns['KasBG'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Persediaan</div>
							@foreach($akuns['Persediaan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Perlengkapan</div>
							@foreach($akuns['Perlengkapan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Dagang</div>
							@foreach($akuns['PiutangDagang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Karyawan</div>
							@foreach($akuns['PiutangKaryawan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Lain</div>
							@foreach($akuns['PiutangLain'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Piutang Tak Tertagih</div>
							@foreach($akuns['PiutangTakTertagih'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Asuransi Bayar Dimuka</div>
							@foreach($akuns['AsuransiDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Sewa Bayar Dimuka</div>
							@foreach($akuns['SewaDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">PPN Masukan</div>
							@foreach($akuns['PPNMasukan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Total Aset Lancar</strong></div>
							@foreach($akuns['AsetLancar'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
						
						<div class="row">
							<div class="col-md-6" style="padding-top: 20px"><strong>ASET TETAP</strong></div>
						</div>
						<div class="row" ">
							<div class="col-md-6">Tanah</div>
							@foreach($akuns['Tanah'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Bangunan</div>
							@foreach($akuns['Bangunan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Peralatan</div>
							@foreach($akuns['Peralatan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Kendaraan</div>
							@foreach($akuns['Kendaraan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Bangunan</div>
							@foreach($akuns['ADBangunan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Peralatan</div>
							@foreach($akuns['ADPeralatan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Akumulasi Depresiasi Kendaraan</div>
							@foreach($akuns['ADKendaraan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Total Aset Tetap</strong></div>
							@foreach($akuns['AsetTetap'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
					</div>

					<div class="col-md-6 garis">
						<div class="row">
							<div class="col-md-12 text-center" style="padding-bottom: 20px"><strong>PASIVA</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6"></div>
							@foreach($tanggal as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px;"><strong>{{ $x }}</span></strong></div>
							@endforeach
							{{-- @foreach($tanggal as  $i => $x)
									<div class="col-md-3 text-right pull-right data" style="padding-bottom: 10px"><strong>{{ $x }}</strong></div>
							@endforeach --}}
						</div>
						<div class="row">
							<div class="col-md-6"><strong>KEWAJIBAN</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6">Kartu Kredit</div>
							@foreach($akuns['KartuKredit'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang Dagang</div>
							@foreach($akuns['HutangDagang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang Pajak</div>
							@foreach($akuns['HutangPajak'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						{{-- <div class="row">
							<div class="col-md-6">Taksiran Hutang Garansi</div>
							@foreach($akuns['TaksiranUtangGaransi'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div> --}}
						<div class="row">
							<div class="col-md-6">Hutang Konsinyasi</div>
							@foreach($akuns['HutangKonsinyasi'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Hutang PPN</div>
							@foreach($akuns['HutangPPN'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div><div class="row">
							<div class="col-md-6">PPN Keluaran</div>
							@foreach($akuns['PPNKeluaran'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Deposito Pelanggan</div>
							@foreach($akuns['PendapatanDimuka'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6 menjorok"><strong>Hutang Jangka Pendek</strong></div>
							@foreach($akuns['HutangJangkaPendek'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>

						<div class="row">
							<div class="col-md-6">Hutang Bank</div>
							@foreach($akuns['HutangBank'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						
						<div class="row">
							<div class="col-md-6 menjorok"><strong>Hutang Jangka Panjang</strong></div>
							@foreach($akuns['HutangJangkaPanjang'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6"><strong>Total Kewajiban</strong></div>
							@foreach($akuns['Kewajiban'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
						</div>

						
						<div class="row" style="padding-top: 20px">
							<div class="col-md-6"><strong>MODAL</strong></div>
						</div>
						<div class="row">
							<div class="col-md-6">Modal</strong></div>
							@foreach($akuns['Modal'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Laba di Tahan</div>
							@foreach($akuns['LabaTahan'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data"> {{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">Prive</div>
							@foreach($akuns['Prive'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6">BagiHasil</div>
							@foreach($akuns['BagiHasil'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-md-6"><strong>Total Modal</strong></div>
							@foreach($akuns['Equitas'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data" style="text-decoration: overline; font-weight: bold">{{ $x }}</div>
							@endforeach
			    			<br>
			    			<br>
			    			<br>
			    			<br>
			    			<br>
			    			<br>
			    			<br>
			    			<br>
						</div>
					</div>
    			</div>

    			<div class="row">
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 10px; padding-bottom: 10px">
							<div class="col-md-6"><strong>Total Aset</strong></div>
							@foreach($akuns['Aset'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
    				</div>
    				<div class="col-md-6 garis">
    					<div class="row" style="padding-top: 10px; padding-bottom: 10px">
							<div class="col-md-6"><strong>Total Kewajiban + Modal</strong></div>
							@foreach($akuns['TotalKE'] as  $i => $x)
								<div class="col-md-3 text-right pull-right data">{{ $x }}</div>
							@endforeach
						</div>
    				</div>
    			</div>

      		</div>
	</div>
@endsection

@section('app.script')
    <script type="text/javascript">
    	$(document).ready(function() {
			$('.data').each(function(index, el) {
                var duit = $(el).text();
                if(duit.includes('(')){
                	$(el).text(duit);
                }else{
                	var a = '<span class="invisible">)</span>';
                	$(el).text(duit);
                	$(el).append(a);
                }
            });

            window.print();
		});
    </script>
@endsection