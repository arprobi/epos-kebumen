@extends('layouts.admin')

@section('title')
	<title>EPOS | Batas Periode Pesanan</title>
@endsection

@section('style')
	<style media="screen">
		.btnSimpan {
			margin: 0;
		}
		.lead {
			margin-bottom: 10px;
		}
	</style>
@endsection

@section('content')
<!-- <div class="row"> -->
	<div class="col-md-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Batas Periode Pesanan</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row" id="formSimpanContainer">
					<form class="form-horizontal" action="{{url('/periode')}}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="post">
						<div class="form-group col-xs-5">
							<label class="control-label">Batas Bulan Penurunan Kelas</label>
							<input type="text" name="bulan_down" class="number form-control" placeholder="Batas Bulan Penurunan Kelas" required="">
						</div>
						<div class="form-group col-xs-7">
							<label class="control-label">Batas Jumlah Transaksi Penurunan Kelas</label>
							<input type="text" name="jumlah_down" class="number form-control odd_number" placeholder="Batas Jumlah Transaksi Penurunan Kelas" required="">
						</div>
						<div class="form-group col-xs-5">
							<label class="control-label">Batas Bulan Peningkatan Kelas</label>
							<input type="text" name="bulan_up" class="number form-control" placeholder="Batas Bulan Peningkatan Kelas" required="">
						</div>
						<div class="form-group col-xs-7">
							<label class="control-label">Batas Jumlah Transaksi Peningkatan Kelas</label>
							<input type="text" name="jumlah_up" class="number form-control odd_number" placeholder="Batas Jumlah Transaksi Peningkatan Kelas" required="">
						</div>
						<div class="form-group col-xs-12">
							<button class="btn btn-success pull-right btnSimpan" id="btnSimpan" type="submit">
								<i class="fa fa-save"></i> <span>Ubah</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Batas Periode Pesanan Saat Ini</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-xs-12">
						<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBank">
							<thead>
								<tr>
									<th>Batas</th>
									<th>Bulan</th>
									<th>Jumlah Transaksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Penurunan Kelas</td>
									<td>
										@if($periode->bulan_down != 0)
											{{ $periode->bulan_down }} Bulan
										@else
											- 
										@endif
									</td>
									<td>
										@if($periode->jumlah_down != 0)
											{{ $periode->jumlah_down }} Transaksi
										@else
											- 
										@endif
									</td>
								</tr>
								<tr>
									<td>Peningkatan Kelas</td>
									<td>
										@if($periode->bulan_up != 0)
											{{ $periode->bulan_up }} Bulan
										@else
											- 
										@endif
									</td>
									<td>
										@if($periode->jumlah_up != 0)
											{{ $periode->jumlah_up }} Transaksi
										@else
											- 
										@endif
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h4>Operator : {{ $periode->user->nama }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- </div> -->
@endsection

@section('script')
	@if (session('sukses') == 'ubah')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Batas Periode Pesanan berhasil diubah!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@endif

	<script type="text/javascript">
		$(document).on('keyup', '.odd_number', function() {
			var num = $(this).val();

			if(num % 2 == 0) {
				$(this).parents('.form-group').addClass('has-error');
				$('#btnSimpan').prop('disabled', true);
			}else{
				$(this).parents('.form-group').removeClass('has-error');
				$('#btnSimpan').prop('disabled', false);
			}
		});

		$(document).on('keyup', '.number', function() {
			var num = $(this).val();

			if(isNaN(num)){
				$(this).val('');
			}
		});
	</script>

@endsection

