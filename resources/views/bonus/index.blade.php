@extends('layouts.admin')

@section('title')
    <title>EPOS | Bonus</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        #btnSimpan,
        #btnReset {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Bonus Item</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="formSimpanContainer">
                <form method="post" action="{{ url('bonus') }}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="control-label">Kode Bonus</label>
                        <input class="form-control" type="text" name="kode">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nama Bonus</label>
                        <input class="form-control" type="text" name="nama">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nama Pendek (15 Karakter)</label>
                        <input class="form-control" type="text" name="nama_pendek">
                    </div>
                    {{-- <div class="form-group">
                        <label class="control-label">Jumlah Bonus (stok)</label>
                        <input class="form-control" type="text" name="stok">
                    </div> --}}
                    <div class="form-group" style="margin-top: 20px;">
                        <button class="btn btn-success" id="btnSimpan" type="submit">
                            <i class="fa fa-save"></i> <span>Tambah</span>
                        </button>
                        <button class="btn btn-default" id="btnReset" type="button">
                            <i class="fa fa-refresh"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
            <div id="formHapusContainer" style="display: none;">
                <form method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="delete">
                </form>
            </div>
        </div>
    </div>
    <!-- kolom kanan -->
    <div class="col-md-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Bonus Item</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableBonus">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Bonus</th>
                            <th>Nama Bonus</th>
                            <th>Jumlah Stok</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bonuses as $num => $bonus)
                        <tr id="{{ $bonus->id }}">
                            <td>{{ $num + 1 }}</td>
                            <td>{{ $bonus->kode }}</td>
                            <td>{{ $bonus->nama }}</td>
                            <td>{{ $bonus->stoktotal }}</td>
                            <td>
                                <button class="btn btn-xs btn-primary" id="btnUbah">
                                    <i class="fa fa-edit"></i> Ubah
                                </button>
                                <button class="btn btn-xs btn-danger" id="btnHapus">
                                    <i class="fa fa-trash"></i> Hapus
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Bonus gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Bonus gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Bonus berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Bonus gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableBonus').DataTable();

        $(document).on('click', '#btnReset', function(event) {
            event.preventDefault();

            $('input[name="kode"]').val('');
            $('input[name="nama"]').val('');
            $('input[name="stok"]').val('');

            $('#formSimpanContainer').find('form').attr('action', '{{ url("bonus") }}');
            $('#formSimpanContainer').find('input[name="_method"]').val('post');
            $('#btnSimpan span').text('Tambah');

            $('#formSimpanTitle').text('Tambah Bonus Item');
        });

        $(document).on('click', '#btnUbah', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var kode = $tr.find('td').first().next().text();
            var nama = $tr.find('td').first().next().next().text();
            var stok = $tr.find('td').first().next().next().next().text();


            $('input[name="kode"]').val(kode);
            $('input[name="nama"]').val(nama);
            $('input[name="stok"]').val(stok);

            $('#formSimpanContainer').find('form').attr('action', '{{ url("bonus") }}' + '/' + id);
            $('#formSimpanContainer').find('input[name="_method"]').val('put');
            $('#btnSimpan span').text('Ubah');

            $('#formSimpanTitle').text('Ubah Bonus Item');
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + nama + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function(){
                //confirmed
                $('#formHapusContainer').find('form').attr('action', '{{ url("bonus") }}' + '/' + id);
                $('#formHapusContainer').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

    </script>
@endsection
