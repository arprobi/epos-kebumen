@extends('layouts.admin')

@section('title')
    <title>EPOS | Perlengkapan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .panel_toolbox li {
            cursor: pointer;
        }

        button[data-target="#collapse-perlengkapan"] {
            background-color: transparent;
            border: none;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Perlengkapan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div>
                    <div class="row">
                        <form method="post" action="{{ url('perlengkapan') }}" class="form-horizontal">
                            <div class="col-md-6 col-xs-12">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Kode Barang</label>
                                    <input class="form-control" type="text" name="kode" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nomor Struk</label>
                                    <input class="form-control input_cek" type="text" name="no_transaksi">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Item</label>
                                    <input class="form-control input_cek" type="text" name="nama" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control input_cek" type="text" name="keterangan">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control input_cek" type="text" name="jumlah" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Harga Satuan</label>
                                    <input class="form-control angka" type="text" name="harga_" required="">
                                    <input class="form-control input_cek" type="hidden" name="harga">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Sumber Kas</label>
                                    <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="BankContainer">
                                    <label class="control-label">Pilih Rekening Bank</label>
                                    <select name="bank" id="bank" class="select2_single form-control">
                                        <option value="" id="default">Pilih Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="kas" />
                                <input type="hidden" name="umur" value="0" />
                                <input type="hidden" name="status" value="1" />
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- kolom bawah -->
<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Perlengkapan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePerlengkapan">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Nomor Struk</th>
                            <th>Item</th>
                            <th>Harga</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                            <th style="width: 25px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($perlengkapans as $num => $perlengkapan)
                        <tr id="{{$perlengkapan->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $perlengkapan->kode }}</td>
                            <td>{{ $perlengkapan->no_transaksi }}</td>
                            <td>{{ $perlengkapan->nama }}</td>
                            <td align="right">{{ \App\Util::ewon($perlengkapan->harga) }}</td>
                            @if($perlengkapan->keterangan!==NULL)
                                <td>{{ $perlengkapan->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                            <td>{{ $perlengkapan->created_at->format('d-m-Y') }}</td>
                            <td class="text-center">
                                <button class="btn btn-xs btn-danger" id="btnOff" data-toggle="tooltip" data-placement="top" title="Rusak / Habis">
                                    <i class="fa fa-trash"></i>
                                </button> 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->
    <div id="formRusakContainer" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="put">
        </form>
    </div>

<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Perlengkapan Lama</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tablePerlengkapanOff">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Nomor Struk</th>
                            <th>Item</th>
                            <th>Harga</th>
                            <th>Keterangan</th>
                            <th>Tanggal pindah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($perlengkapan_offs as $num => $perlengkapan_off)
                        <tr id="{{$perlengkapan_off->id}}">
                            <td>{{ $num+1 }}</td>
                            <td>{{ $perlengkapan_off->kode }}</td>
                            <td>{{ $perlengkapan_off->no_transaksi }}</td>
                            <td>{{ $perlengkapan_off->nama }}</td>
                            <td align="right">{{ \App\Util::ewon($perlengkapan_off->harga) }}</td>
                            @if($perlengkapan_off->keterangan!==NULL)
                                <td>{{ $perlengkapan_off->keterangan }}</td>
                            @else
                                <td align="center"> - </td>
                            @endif
                            <td class="text-center">{{ $perlengkapan_off->updated_at->format('d-m-Y') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- </div> -->
@endsection

@section('script')

    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Perlengkapan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Perlengkapan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Perlengkapan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Perlengkapan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Perlengkapan berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Perlengkapan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tablePerlengkapan').DataTable();
        $('#tablePerlengkapanOff').DataTable();

        $(document).ready(function() {
            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(document).on('click', 'button[data-target="#collapse-perlengkapan"]', function(event) {
            event.preventDefault();
            
            if ($(this).hasClass('tampil')) {
                $(this).removeClass('tampil');
                $(this).addClass('sembunyit');
                $(this).find('i').removeClass('fa-chevron-up');
                $(this).find('i').addClass('fa-chevron-down');
            } else if ($(this).hasClass('sembunyit')) {
                $(this).removeClass('sembunyit');
                $(this).addClass('tampil');
                $(this).find('i').removeClass('fa-chevron-down');
                $(this).find('i').addClass('fa-chevron-up');
            }
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }

        $(window).on('load', function(event) {

			var url = "{{ url('perlengkapan/last/json') }}";
			var tanggal = printBulanSekarang('mm/yyyy');			
			console.log('kode');
			$.get(url, function(data) {
				if (data.perkap === null) {
					var kode = int6digit(1);
					var kode = kode + '/KMK/PR/' + tanggal;
					console.log('anu');
				} else {
					var kode = data.perkap.kode;
					var mm_transaksi = kode.split('/')[3];
					var yyyy_transaksi = kode.split('/')[4];
					var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

					if (tanggal != tanggal_transaksi) {
						var kode = int6digit(parseInt(kode.split('/')[0]) + 1);
						kode = kode + '/KMK/PR/' + tanggal;
					} else {
						var kode = int6digit(parseInt(kode.split('/')[0]) + 1);
						kode = kode + '/KMK/PR/' + tanggal_transaksi;			
					}
					// console.log('ini');
				}
				// console.log(kode);
				$('input[name="kode"]').val(kode);
			});
		});

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('click', '#btnOff', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var kode = $tr.find('td').first().next().text();
            var nama = $tr.find('td').first().next().next().text();
            $('input[name="id"]').val(id);

            swal({
                title: 'Hapus?',
                text: '\"' + kode +' ' + nama  + '\" akan dihapus!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
                
                }).then(function() {
                    // Confirmed
                    $('#formRusakContainer').find('form').attr('action', '{{ url("perlengkapan/off") }}' + '/' + id);
                    $('#formRusakContainer').find('form').submit();
                }, function(isConfirm) {
                    //canceled
                    console.log('gagal');
                });
            });
    </script>
@endsection
