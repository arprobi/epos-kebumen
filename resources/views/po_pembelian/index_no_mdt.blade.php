@extends('layouts.admin')

@section('title')
    <title>EPOS | Daftar Pesanan Pembelian</title>
@endsection

@section('style')
    <style media="screen">
        #btnDetail, #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnUbah {
            margin: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Pesanan Pembelian</h2>
                <a href="{{ url('po-pembelian/create') }}" class="btn btn-sm btn-success pull-right" id="btnUbah" data-toggle="tooltip" data-placement="top" title="Tambah Pesanan Pembelian">
                    <i class="fa fa-plus"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItemMasuk">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal & Waktu</th>
                            <th>Kode Pesanan</th>
                            <th>Pemasok</th>
                            <th style="width: 50px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($po_pembelians as $i => $po_pembelian)
                        <tr id="{{ $po_pembelian->id }}">
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $po_pembelian->created_at->format('d-m-Y H:i:s') }}</td>
                            <td>{{ $po_pembelian->kode_transaksi }}</td>
                            <td>{{ $po_pembelian->suplier->nama }}</td>
                            <td class="text-left">
                                <a href="{{ url('po-pembelian/'.$po_pembelian->id) }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Pesanan Pembelian">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="btn btn-xs btn-danger" id="btnHapus" data-toggle="tooltip" data-placement="top" title="Hapus Pesanan Pembelian">
                                    <i class="fa fa-trash"></i>
                                </a>
                                {{-- </a><a href="{{ url('po-pembelian/'.$po_pembelian->id.'/beli') }}" class="btn btn-xs btn-success" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Beli Pesanan">
                                    <i class="fa fa-money"></i>
                                </a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Pembelian berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Pembelian gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Pembelian berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Pembelian gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Pesanan Pembelian berhasil dihapus!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pesanan Pembelian gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('gagal') == '404')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Halaman tidak ditemukan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableItemMasuk').DataTable({
            // 'order': [[1, 'desc'], [2, 'desc']]
        });

        $(document).on('click', '#btnHapus', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').attr('id');

            swal({
                title: 'Hapus Pesanan?',
                text: 'Pesanan Pembelian akan dihapus!',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#26B99A',
                cancelButtonColor: '#d9534f',
                confirmButtonText: '<i class="fa fa-check"></i> Hapus!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                var url = '{{ url("po-pembelian/") }}' + '/' + id + '/hapus';
                var ww = window.open(url, '_self');
            }, function(dismiss) {
                // console.log(dismiss);
            });
        });

    </script>
@endsection
