@extends('layouts.admin')

@section('title')
    <title>EPOS | Peralatan</title>
@endsection

@section('style')
    <style media="screen">
        #btnUbah, #btnHapus {
            margin-bottom: 0;
        }
        #btnJual {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .panel_toolbox li {
            cursor: pointer;
        }
        button[data-target="#collapse-peralatan"] {
            background-color: transparent;
            border: none;
        }
        #tablePiutangBank .btn {
            margin-botton: 0;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2 id="formSimpanTitle">Tambah Peralatan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div>
                    <div class="row">
                        <form method="post" action="{{ url('peralatan') }}" class="form-horizontal">
                            <div class="col-md-6 col-xs-12">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="_method" value="post">
                                <div class="form-group">
                                    <label class="control-label">Kode Barang</label>
                                    <input class="form-control input_cek" type="text" name="kode" readonly>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nomor Struk</label>
                                    <input class="form-control input_cek" type="text" name="no_transaksi">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Item</label>
                                    <input class="form-control input_cek" type="text" name="nama" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Umur Ekonomis</label>
                                    <input class="form-control input_cek" type="text" name="umur" required="">
                                </div>
                                <div class="form-group controls xdisplay_inputx has-feedback" style="padding-left: 0">
                                    <label class="control-label">Tanggal Pembelian</label>
                                    <input type="text"  name="tanggal_" class="form-control has-feedback-left active tanggal-putih" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px" readonly="">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <input class="form-control" type="hidden" name="tanggal">
                                </div>
                                <!-- <div class="form-group controls xdisplay_inputx has-feedback col-md-6" style="padding-right: 0">
                                    <label class="control-label">Tanggal Pakai</label>
                                    <input type="text"  name="tanggal_a_" class="form-control has-feedback-left active" id="single_cal3_" placeholder="" aria-describedby="inputSuccess2Status" style="padding-left: 60px">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <input class="form-control" type="hidden" name="tanggal_a">
                                </div> -->
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Jumlah</label>
                                    <input class="form-control input_cek" type="text" name="jumlah" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Harga Perolehan per Satuan</label>
                                    <input class="form-control angka" type="text" name="harga_" required="">
                                    <input class="form-control input_cek" type="hidden" name="harga">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nilai Residu</label>
                                    <input class="form-control angka" type="text" name="residu_">
                                    <input class="form-control input_cek" type="hidden" name="residu">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Keterangan</label>
                                    <input class="form-control input_cek" type="text" name="keterangan">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Sumber Kas</label>
                                    <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnBank" class="btn btn-default">Bank</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="BankContainer">
                                    <label class="control-label">Pilih Rekening Bank</label>
                                    <select name="bank" id="bank" class="select2_single form-control">
                                        <option value="" id="default">Pilih Bank</option>
                                        @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="kas" />
                                <div class="form-group" style="margin-bottom: 0;">
                                    <button class="btn btn-success pull-right" id="btnSimpan" type="submit" style="margin: 0;">
                                        <i class="fa fa-save"></i> <span>Tambah</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Daftar Peralatan</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <div class="pull-right">
                        <li> <a class="collapse-link"> <i class="fa fa-chevron-up"> </i> </a> </li>
                    </div>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('peralatan/mdt1') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tablePeralatan" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="kode">Kode Barang</th>
                                <th class="sorting" field="nama">Item</th>
                                <th class="sorting" field="umur">Umur Ekonomis</th>
                                <th class="sorting" field="harga">Harga</th>
                                <th class="sorting" field="nominal">Nilai Saat Ini</th>
                                <th class="sorting" field="residu">Nilai Residu</th>
                                <th class="sorting" field="created_at">Tanggal Perolehan</th>
                                <th class="sorting" field="keterangan">Keterangan</th>
                                <th class="sorting" field="no_transaksi">Nomor Struk</th>
                                <th class="sorting" field="kode" style="width: 120px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Riwayat Peralatan</h2>
                <ul class="nav navbar-right panel_toolbox" style="padding-left: 100px">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="mdtContainer">
                    <input type="hidden" name="base_url" value="{{ url('peralatan/mdt2') }}">
                    <input type="hidden" name="data_per_halaman" value="">
                    <input type="hidden" name="search_query" value="">
                    <input type="hidden" name="data_total" value="">
                    <input type="hidden" name="halaman_sekarang" value="">
                    <input type="hidden" name="field" value="kode">
                    <input type="hidden" name="order" value="asc">

                    <div id="mdtHeader" class="row" style="margin-bottom: 10px;"></div>

                    <table id="tabelOff" class="table table-striped table-bordered table-hover" style="margin-bottom: 0;">
                        <thead>
                            <tr>
                                <th class="sorting" field="kode">No</th>
                                <th class="sorting" field="kode">Kode Barang</th>
                                <th class="sorting" field="nama">Item</th>
                                <th class="sorting" field="umur">Umur Ekonomis</th>
                                <th class="sorting" field="harga">Harga</th>
                                <th class="sorting" field="nominal">Nilai Saat Ini</th>
                                <th class="sorting" field="residu">Nilai Residu</th>
                                <th class="sorting" field="created_at">Tanggal Kerusakan</th>
                                <th class="sorting" field="keterangan">Keterangan</th>
                                <th class="sorting" field="no_transaksi">Nomor Struk</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="mdtFooter" class="row" style="margin-top: 10px;">
                        <div class="col-md-6" id="paginationText"></div>
                        <div class="col-md-6" style="text-align: right;">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0;">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row"></div>
<!-- </div> -->

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" id="formModal">
            <form method="post" action="{{ url('peralatan/rusak') }}" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="modal_info"> <h4 class="modal-title">Keterangan <i id="modal_nama"></i> Mengalami Rusak/Hilang</h4> </span>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" id="modal_id">
                    <div class="form-group" id="label_modal">
                        <label class="control-label">Keterangan</label>
                        <input class="form-control" type="text" name="keterangan" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-default">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal_jual" role="dialog">
    <div class="modal-dialog modal-md" id="formSimpanContainerJual">
        <div class="modal-content" id="formModal">
            <form method="post" action="{{ url('peralatan/jual') }}" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="modal_info"> <h4 class="modal-title">Penjualan <i id="modal_jual_nama"></i></h4> </span>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" id="modal_jual">
                    <div class="form-group">
                        <label class="control-label">Nominal Penjualan</label>
                        <input class="form-control input_cek_jual angka" type="text" name="nominal_" required="">
                        <input class="form-control input_cek_jual" type="hidden" name="nominal">
                    </div>
                    <div class="form-group" id="label_modal">
                        <label class="control-label">Keterangan</label>
                        <input class="form-control input_cek_jual" type="text" name="keterangan" required="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kas Tujuan</label>
                        <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                            <div class="btn-group" role="group">
                                <button type="button" id="btnTunaiJual" class="btn btn-default">Tunai</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" id="btnBankJual" class="btn btn-default">Bank</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="BankContainerJual">
                        <label class="control-label">Pilih Rekening Bank</label>
                        <select name="bank" id="bank_jual" class="form-control select2_single">
                            <option value="" id="default">Pilih Bank</option>
                            @foreach($banks as $bank)
                            <option value="{{ $bank->id }}">{{$bank->nama_bank}} - {{$bank->no_rekening}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="kas" />
                    <button type="submit" id="btnSimpanJual" name="submit" class="btn btn-default">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Peralatan berhasil ditambah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Peralatan gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Peralatan berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Peralatan gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Peralatan berhasil dipindahkan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'jual')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Peralatan berhasil dijual!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Peralatan gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        var mdt1 = "{{ url('peralatan/mdt1') }}";
        var mdt2 = "{{ url('peralatan/mdt2') }}";

        function refreshMDTData(data, base_url, inverse, data_total) {
            // console.log('refreshMDTData', data);

            var $mdtContainer = null;
            $('.mdtContainer').each(function(index, el) {
                if ($(el).find('input[name="base_url"]').val() == base_url) {
                    $mdtContainer = $(el);
                }
            });

            if (data.length <= 0) {
                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="11" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    var tr = `<tr>
                                <td colspan="10" class="tengah-h">Data tidak tersedia di tabel</td>
                            </tr>`;

                    $mdtContainer.find('tbody').append($(tr));

                }

            } else {
                var data_per_halaman = parseInt($mdtContainer.find('input[name="data_per_halaman"]').val());
                var halaman_sekarang = parseInt($mdtContainer.find('input[name="halaman_sekarang"]').val());
                var no_terakhir = (halaman_sekarang - 1) * data_per_halaman;

                if (base_url == mdt1) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var peralatan = data[i];
                        var buttons = peralatan.buttons;
                        // console.log(buttons);
                        var td_buttons = '';
                        if (buttons.detail != null) {
                            td_buttons += `
                                <a href="${buttons.detail.url}" class="btn btn-xs btn-pelihara" id="btnBayar" data-toggle="tooltip" data-placement="top" title="Pemeliharaan">
                                    <i class="fa fa-chain"></i>
                                </a>
                            `;
                        }
                        if (buttons.hapus != null) {
                            td_buttons += `
                                <a href="#myModal" data-target="#myModal" data-toggle="modal" class="btn btn-xs btn-danger" id="btnRusak" title="Alat Rusak/Hilang">
                                    <i class="fa fa-trash"></i>
                                </a>
                            `;
                        }
                        if (buttons.jual != null) {
                            td_buttons += `
                                <a href="#myModal_jual" data-target="#myModal_jual" data-toggle="modal" class="btn btn-xs btn-primary" id="btnJual" title="Jual Alat">
                                    <i class="fa fa-money"></i>
                                </a>
                            `;
                        }

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${peralatan.id}">
                                    <td>${nomor}</td>
                                    <td>${peralatan.kode}</td>
                                    <td>${peralatan.nama}</td>
                                    <td>${peralatan.umur} Tahun</td>
                                    <td class="text-right">${peralatan.harga}</td>
                                    <td class="text-right">${peralatan.nominal}</td>
                                    <td class="text-right">${peralatan.residu}</td>
                                    <td>${ymd2dmy(peralatan.created_at.split(' ')[0])}</td>
                                    <td>${peralatan.keterangan}</td>
                                    <td>${peralatan.no_transaksi}</td>
                                    <td class="text-center">${td_buttons}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                } else if (base_url == mdt2) {
                    $mdtContainer.find('tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        var peralatan = data[i];

                        var nomor = no_terakhir + i + 1;
                        if (inverse) {
                            nomor = data_total - nomor + 1;
                        }

                        var tr = `<tr id="${peralatan.id}">
                                    <td>${nomor}</td>
                                    <td>${peralatan.kode}</td>
                                    <td>${peralatan.nama}</td>
                                    <td>${peralatan.umur} Tahun</td>
                                    <td class="text-right">${peralatan.harga}</td>
                                    <td class="text-right">${peralatan.nominal}</td>
                                    <td class="text-right">${peralatan.residu}</td>
                                    <td>${ymd2dmy(peralatan.created_at.split(' ')[0])}</td>
                                    <td>${peralatan.keterangan}</td>
                                    <td>${peralatan.no_transaksi}</td>
                                </tr>`;

                        $mdtContainer.find('tbody').append($(tr));
                    }

                }
            }
        }

        $(document).ready(function() {
            /* $('#single_cal3').daterangepicker({
              	singleDatePicker: true,
              	calender_style: "picker_3"
            }, function(start, end, label) {
            	// console.log(start.toISOString());
              	var date = start.toISOString().substr(0,10);
              	$('input[name="tanggal"]').val(date);
            }); */

            $('[data-toggle="modal"][title]').tooltip();

            $('#single_cal3').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                  calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
            }, 
            function(start, end, label) {
                var date = start.toISOString().substr(0,10);
                  $('input[name="tanggal"]').val(date);
            });

            /* $('#single_cal3_').daterangepicker({
              	singleDatePicker: true,
              	calender_style: "picker_3"
            }, function(start, end, label) {
            	console.log(start.toISOString());
              	var date = start.toISOString().substr(0,10);
              	$('input[name="tanggal_a"]').val(date);
            }); */

            $('#BankContainerJual').hide();
            $('#BankContainerJual').find('input').val('');
            $('#btnTunaiJual').removeClass('btn-default');
            $('#btnTunaiJual').addClass('btn-success');
            $('#formSimpanContainerJual').find('input[name="kas"]').val('tunai');

            $('#BankContainer').hide();
            $('#BankContainer').find('input').val('');
            $('#btnTunai').removeClass('btn-default');
            $('#btnTunai').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBank').removeClass('btn-success');
            $('#btnBank').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('tunai');
            btnSimpan();
        });

        $(document).on('click', '#btnBank', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunai').removeClass('btn-success');
            $('#btnTunai').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="kas"]').val('bank');
            btnSimpan();
        });

        $(document).on('click', '#btnTunaiJual', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnBankJual').removeClass('btn-success');
            $('#btnBankJual').addClass('btn-default');

            $('#BankContainerJual').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainerJual').find('input[name="kas"]').val('tunai');
            btnSimpanJual();
        });

        $(document).on('click', '#btnBankJual', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnTunaiJual').removeClass('btn-success');
            $('#btnTunaiJual').addClass('btn-default');

            $('#BankContainerJual').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainerJual').find('input[name="kas"]').val('bank');
            btnSimpanJual();
        });

        $(window).on('load', function(event) {

            var url = "{{ url('peralatan/last/json') }}";
            var tanggal = printBulanSekarang('mm/yyyy');			
            $.get(url, function(data) {
                if (data.perkap === null) {
                    var kode = int4digit(1);
                    var kode = kode + '/KMK/PL/' + tanggal;
                } else {
                    var kode = data.perkap.kode;
                    var mm_transaksi = kode.split('/')[3];
                    var yyyy_transaksi = kode.split('/')[4];
                    var tanggal_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode = kode + '/KMK/PL/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode.split('/')[0]) + 1);
                        kode = kode + '/KMK/PL/' + tanggal_transaksi;				}
                }
                // console.log(kode);
                $('input[name="kode"]').val(kode);
            });
        });

        $(document).on('click', 'button[data-target="#collapse-peralatan"]', function(event) {
            event.preventDefault();
            
            if ($(this).hasClass('tampil')) {
                $(this).removeClass('tampil');
                $(this).addClass('sembunyit');
                $(this).find('i').removeClass('fa-chevron-up');
                $(this).find('i').addClass('fa-chevron-down');
            } else if ($(this).hasClass('sembunyit')) {
                $(this).removeClass('sembunyit');
                $(this).addClass('tampil');
                $(this).find('i').removeClass('fa-chevron-down');
                $(this).find('i').addClass('fa-chevron-up');
            }
        });

        $(document).on('click', '#btnRusak', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();
            
            $('#modal_id').val(id);
            $('#modal_nama').text(nama);
        });

        $(document).on('click', '#btnJual', function() {
            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            var nama = $tr.find('td').first().next().next().text();	
            
            $('#modal_jual').val(id);
            $('#modal_jual_nama').text(nama);
        });

        $(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }

        $(document).on('keyup', '.input_cek_jual', function(event) {
            event.preventDefault();

            btnSimpanJual();
        });

        $(document).on('change', '#bank_jual', function(event) {
            event.preventDefault();

            btnSimpanJual();
        });

        function btnSimpanJual() {
            if ($('#btnBankJual').hasClass('btn-success')){
                var bank = $('#bank_jual').val();
                console.log(bank+'cek');
                if (bank != ''){
                    $('#btnSimpanJual').prop('disabled', false);
                } else {
                    $('#btnSimpanJual').prop('disabled', true);
                }
            } else {
                $('#btnSimpanJual').prop('disabled', false);
            }
        }

    </script>
@endsection
