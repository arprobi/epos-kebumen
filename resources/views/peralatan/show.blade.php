@extends('layouts.admin')

@section('title')
    <title>EPOS | Perawatan Alat</title>
@endsection

@section('style')
    <style media="screen">
    	#btnBayar {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="col-md-4">
			<div class="x_panel">
			<div class="x_title">
				<h2 id="formSimpanTitle">Simpan Biaya Perawatan Alat</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
		        <form method="post" action="{{ url('rawat_alat') }}" class="form-horizontal">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="post">
					<div class="form-group">
						<label class="control-label">Kode Barang</label>
						<input class="form-control" type="text" name="kode" value="{{ $alat->kode }}" readonly>
					</div>
					<div class="form-group">
						<label class="control-label">Keterangan</label>
						<input class="form-control input_cek" type="text" name="keterangan" required="">
					</div>
					<div class="form-group">
						<label class="control-label">Jumlah Biaya</label>
						<input class="form-control input_cek angka" type="text" name="nominal_" required="">
						<input class="form-control input_cek" type="hidden" name="nominal">
					</div>
					<div class="form-group">
						<label class="control-label">Dari Kas</label>
						<div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
							<div class="btn-group" role="group">
								<button type="button" id="btnTunai" class="btn btn-default">Tunai</button>
							</div>
							<div class="btn-group" role="group">
								<button type="button" id="btnBank" class="btn btn-default">Bank</button>
							</div>
						</div>
					</div>
					<div class="form-group" id="BankContainer">
						<div class="form-group">
							<label class="control-label">Pilih Rekening Bank</label>
							<select name="bank" id="bank" class="select2_single form-control">
								<option value="" id="default">Pilih Bank</option>
								@foreach($banks as $bank)
								<option value="{{ $bank->id }}">{{ $bank->nama_bank}} - {{$bank->no_rekening}}</option>
								@endforeach
							</select>
						</div>
					</div>
						<input type="hidden" name="kas" />
						<input type="hidden" name="id" value="{{ $alat->id }}" />
					<div class="form-group" style="margin-bottom: 0;">
						<button class="btn btn-sm btn-success pull-left" id="btnSimpan" type="submit">
							<i class="fa fa-save"></i> <span>Simpan</span>
						</button>
					</div>
				</form>
			</div>
		</div>
		</div>
		<!-- kolom kanan -->
		<div class="col-md-8">
			<div class="x_panel">
				<div class="x_title">
					<h2>Riwayat Perawatan Alat - {{ $alat->nama }}</h2>
					<a href="{{url('peralatan')}}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                		<i class="fa fa-long-arrow-left"></i>
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableItem">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal Perawatan</th>
								<th>Keterangan</th>
								<th>Jumlah Biaya</th>	
							</tr>
						</thead>
						<tbody>
							@foreach($history as $num => $log)
							<tr id="{{$log->id}}">
								<td>{{ $num+1 }}</td>
								<td>{{ ($log->created_at)->format('d-m-Y') }}</td>
								<td>{{ $log->keterangan }}</td>
								<td align="right">{{ \App\Util::ewon($log->debet) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	@if (session('sukses') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Mantap!',
				text: 'Hutang berhasil dibayar!',
				timer: 3000,
				type: 'success'
			});
		</script>
	@elseif (session('gagal') == 'bayar')
		<script type="text/javascript">
			swal({
				title: 'Waduh!',
				text: 'Hutang gagal dibayar!',
				timer: 3000,
				type: 'error'
			});
		</script>
	@endif

	<script type="text/javascript">
		$('#tableItem').DataTable();
		$('.select2_single').select2({
			allowClear: true
		});
		$(document).ready(function() {
			$('#BankContainer').hide();
			$('#BankContainer').find('input').val('');
			$('#btnTunai').removeClass('btn-default');
			$('#btnTunai').addClass('btn-success');
			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
		});

		$(document).on('click', '#btnTunai', function(event) {
			event.preventDefault();
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');

			$('#btnBank').removeClass('btn-success');
			$('#btnBank').addClass('btn-default');

			$('#BankContainer').hide('fast', function() {
				$(this).find('input').val('');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('tunai');
			btnSimpan();
		});

		$(document).on('click', '#btnBank', function(event) {
			event.preventDefault();
			
			$(this).removeClass('btn-default');
			$(this).addClass('btn-success');
			
			$('#btnTunai').removeClass('btn-success');
			$('#btnTunai').addClass('btn-default');

			$('#BankContainer').show('fast', function() {
				$(this).find('input').trigger('focus');
			});

			$('#formSimpanContainer').find('input[name="kas"]').val('bank');
			btnSimpan();
		});

		$(document).on('keyup', '.input_cek', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        $(document).on('change', '#bank', function(event) {
            event.preventDefault();

            btnSimpan();
        });

        function btnSimpan() {
            if ($('#btnBank').hasClass('btn-success')){
                var bank = $('#bank').val();
                console.log(bank+'cek');
                if (bank != ''){
                    $('#btnSimpan').prop('disabled', false);
                } else {
                    $('#btnSimpan').prop('disabled', true);
                }
            } else {
                $('#btnSimpan').prop('disabled', false);
            }
        }
	</script>
@endsection