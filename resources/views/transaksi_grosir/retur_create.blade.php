@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Retur Penjualan</title>
@endsection

@section('style')
    <style media="screen">
        #btnRetur, #btnDetail, #btnKembali {
            margin-bottom: 0;
        }
        #btnRetur {
            /*margin-right: 0;*/
        }
        .full-width {
            width: 100%;
        }
        .feedback {
            background-color : #31B0D5;
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
            border-color: #46b8da;
        }
        #mybutton {
            position: fixed;
            bottom: -4px;
            right: 10px;
        }
        #tabelKeranjang td {
            border: none;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title" style="height: auto;">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="full-width">Detail Transaksi Penjualan</h2>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav navbar-right panel_toolbox" style="margin-left: -50px">
                            <div class="pull-right">
                                {{-- <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li> --}}
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            </div>
                        </ul>
                        <!-- <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur') }}" id="btnRetur" class="btn btn-sm btn-warning pull-right">
                            <i class="fa fa-sign-in"></i> Lihat Retur
                        </a> -->
                        {{-- <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id) }}" id="btnDetail" class="btn btn-sm btn-info pull-right">
                            <i class="fa fa-eye"></i> Detail Penjualan
                        </a> --}}
                        <a href="{{ url('transaksi-grosir/'.$transaksi_grosir->id) }}" id="btnKembali" class="btn btn-sm btn-default pull-right" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                            <i class="fa fa-long-arrow-left"></i>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            {{-- <div class="x_content" style="display: none;"> --}}
            <div class="x_content">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="x_title">
                            <h2>{{ $transaksi_grosir->created_at->format('d-m-Y H:i:s') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>Kode Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->kode_transaksi }}</td>
                                </tr>
                                @if ($transaksi_grosir->pelanggan_id != null)
                                <tr>
                                    <th>Pelanggan</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->nama }}</td>
                                </tr>
                                <tr>
                                    <th>Toko</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->toko }}</td>
                                </tr>
                                <tr>
                                    <th>Telepon</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->telepon }}</td>
                                </tr>
                                @if ($transaksi_grosir->alamat != null)
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->alamat }}</td>
                                </tr>
                                @else
                                <tr>
                                    <th>Alamat</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pelanggan->alamat }}</td>
                                </tr>
                                @endif
                                @endif
                                <tr>
                                    <th>Tanggal Transaksi</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->created_at->format('d-m-Y') }}</td>
                                </tr>
                                @if($transaksi_grosir->jatuh_tempo != NULL && $transaksi_grosir->jumlah_bayar < $transaksi_grosir->harga_total - $transaksi_grosir->potongan_penjualan)
                                    <tr>
                                        <th>Jatuh Tempo</th>
                                        <td style="width: 60%;">{{ \App\Util::date($transaksi_grosir->jatuh_tempo) }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Status Transaksi</th>
                                    <td style="width: 60%; text-transform: capitalize;">{{ $transaksi_grosir->status }}</td>
                                </tr>
                                <tr>
                                    <th>Operator</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->user->nama }}</td>
                                </tr>
                                @if ($transaksi_grosir->pengirim != null)
                                <tr>
                                    <th>Pengirim</th>
                                    <td style="width: 60%;">{{ $transaksi_grosir->pengirim }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        {{-- <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_penjualan->nominal_tunai != null && $transaksi_penjualan->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->banktransfer->nama_bank }} [{{ $transaksi_penjualan->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_transfer != null && $transaksi_penjualan->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->jenis_kartu }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->bankkartu->nama_bank }} [{{ $transaksi_penjualan->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_kartu != null && $transaksi_penjualan->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->bankcek->nama_bank }} [{{ $transaksi_penjualan->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_cek != null && $transaksi_penjualan->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_penjualan->bankbg->nama_bank }} [{{ $transaksi_penjualan->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_bg != null && $transaksi_penjualan->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_bg) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nominal_titipan != null && $transaksi_penjualan->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Deposito</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nominal_titipan) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->jumlah_bayar != null && $transaksi_penjualan->jumlah_bayar > 0)
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->jumlah_bayar) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                @if ($transaksi_penjualan->harga_total != null && $transaksi_penjualan->harga_total > 0)
                                <tr>
                                    <th>Harga Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->harga_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->nego_total != null && $transaksi_penjualan->nego_total > 0 && $is_nego)
                                <tr>
                                    <th>Nego Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->nego_total) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->potongan_penjualan > 0 && $transaksi_penjualan->potongan_penjualan != NULL)
                                <tr>
                                    <th>Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->potongan_penjualan) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->ongkos_kirim != null && $transaksi_penjualan->ongkos_kirim > 0)
                                <tr>
                                    <th>Ongkos Kirim</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_penjualan->ongkos_kirim) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_penjualan->jumlah_bayar != null && $transaksi_penjualan->jumlah_bayar > 0)
                                <tr>
                                    @if ($kembali > 0)
                                    <th>Kembali</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali) }}</td>
                                    @elseif($kembali < 0)
                                    <th>Kurang</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali*-1) }}</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table> --}}
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Rincian</th>
                            </thead>
                            <tbody>
                                @if ($transaksi_grosir->harga_total != null && $transaksi_grosir->harga_total > 0)
                                <tr>
                                    <th>Sub Total</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total) }}</td>
                                </tr>
                                @endif

                                <tr>
                                    <th>Nego & Potongan Penjualan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->potongan_penjualan) }}</td>
                                </tr>

                                <tr>
                                    <th>Deposito Pelanggan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_titipan) }}</td>
                                </tr>

                                @if ($kembali >= 0)
                                    <tr>
                                        <th>Jumlah Bayar</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total + $transaksi_grosir->ongkos_kirim - $transaksi_grosir->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kembalian</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali) }}</td>
                                    </tr>
                                @elseif($kembali < 0)
                                    <tr>
                                        <th>Grand Total (+ Ongkos Kirim)</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->harga_total + $transaksi_grosir->ongkos_kirim - $transaksi_grosir->potongan_penjualan) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pengurang Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tagihan</th>
                                        <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($kembali*-1) }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th class="text-left" colspan="2">Pembayaran</th>
                            </thead>
                            <tbody>
                                @if ($transaksi_grosir->nominal_tunai != null && $transaksi_grosir->nominal_tunai > 0)
                                <tr>
                                    <th>Nominal Tunai</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_tunai) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_transfer != null)
                                <tr>
                                    <th>Nomor Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->no_transfer }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_transfer != null)
                                <tr>
                                    <th>Bank Transfer</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->banktransfer->nama_bank }} [{{ $transaksi_grosir->banktransfer->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_transfer != null && $transaksi_grosir->nominal_transfer > 0)
                                <tr>
                                    <th>Nominal Transfer</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_transfer) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_kartu != null)
                                <tr>
                                    <th>Nomor Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->no_kartu }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->jenis_kartu != null)
                                <tr>
                                    <th>Jenis Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->jenis_kartu }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_kartu != null)
                                <tr>
                                    <th>Bank Kartu</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->bankkartu->nama_bank }} [{{ $transaksi_grosir->bankkartu->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_kartu != null && $transaksi_grosir->nominal_kartu > 0)
                                <tr>
                                    <th>Nominal Kartu</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_kartu) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_cek != null)
                                <tr>
                                    <th>Nomor Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->no_cek }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_cek != null)
                                <tr>
                                    <th>Bank Cek</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->bankcek->nama_bank }} [{{ $transaksi_grosir->bankcek->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_cek != null && $transaksi_grosir->nominal_cek > 0)
                                <tr>
                                    <th>Nominal Cek</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_cek) }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->no_bg != null)
                                <tr>
                                    <th>Nomor BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->no_bg }}</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->bank_bg != null)
                                <tr>
                                    <th>Bank BG</th>
                                    <td class="text-left" style="width: 60%;">{{ $transaksi_grosir->bankbg->nama_bank }} [{{ $transaksi_grosir->bankbg->no_rekening }}]</td>
                                </tr>
                                @endif

                                @if ($transaksi_grosir->nominal_bg != null && $transaksi_grosir->nominal_bg > 0)
                                <tr>
                                    <th>Nominal BG</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_bg) }}</td>
                                </tr>
                                @endif

                                {{-- @if ($transaksi_grosir->nominal_titipan != null && $transaksi_grosir->nominal_titipan > 0)
                                <tr>
                                    <th>Nominal Titipan</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->nominal_titipan) }}</td>
                                </tr>
                                @endif --}}

                                {{-- @if ($transaksi_grosir->jumlah_bayar != null && $transaksi_grosir->jumlah_bayar > 0) --}}
                                <tr>
                                    <th>Jumlah Bayar</th>
                                    <td class="text-right" style="width: 60%;">{{ \App\Util::duit0($transaksi_grosir->jumlah_bayar - $transaksi_grosir->nominal_titipan) }}</td>
                                </tr>
                                {{-- @endif --}}
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="x_title">
                            <h2>Item</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Item</th>
                                    <th class="text-left">Jumlah</th>
                                    <th class="text-right">Harga</th>
                                    <th class="text-right">Total</th>
                                    <th class="text-right">Nego</th>
                                    <th class="text-left">Item Bonus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_transaksi_penjualan as $i => $relasi)
                                <tr id="{{ $relasi->id }}" item_kode="{{ $relasi->item_kode }}">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $relasi->item->nama }}</td>
                                    <td id="jumlah" class="jumlah">{{ $relasi->jumlah }}</td>
                                    <td id="harga" class="text-right">{{ \App\Util::duit0($relasi->subtotal / $relasi->jumlah) }}</td>
                                    {{-- <td class="text-right {{ $relasi->nego != null ? 'text-success' : '' }}">{{ \App\Util::duit0($relasi->nego != null ? $relasi->nego : $relasi->subtotal) }}</td> --}}
                                    <td class="text-right">{{ \App\Util::duit0($relasi->subtotal) }}</td>
                                    <td class="text-right">{{ \App\Util::duit0($relasi->nego) }}</td>
                                    @if ($relasi->bonuses != null)
                                    <td>
                                        @foreach ($relasi->bonuses as $j => $rb)
                                            {{ $rb['jumlah'] }} {{ $rb['bonus']->nama }}
                                            @if ($j != count($relasi->bonuses) - 1)
                                            <br>
                                            @endif
                                        @endforeach
                                    </td>
                                    @else
                                    <td>-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @if (count($relasi_bonus_rules_penjualan) > 0)
                        <div class="x_title">
                            <h2>Bonus Penjualan</h2>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="tabel-item-bundle">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Nama Paket</th>
                                    <th class="text-left">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($relasi_bonus_rules_penjualan as $i => $rbrp)
                                <tr id="{{ $rbrp->id }}" item_kode="{{ $rbrp->bonus->kode }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $rbrp->bonus->nama }}</td>
                                    <td class="jumlah">{{ $rbrp->jumlah }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="full-width">Tambah Retur Penjualan</h2>
                <h2 id="kodeReturTitle" class="full-width" style="font-size: 12px; margin:0"></h2>
                {{-- <span ></span> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option id="default" value="">Pilih Item</option>
                            @foreach($item_list as $item)
                            <option value="{{ $item['item_kode'] }}|{{ $item['item_parent'] }}">{{ $item['item_kode'] }} [{{ $item['nama'] }}]</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
            </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th class="text-left">Item</th>
                            <th class="text-left">Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Retur</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {{-- <table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;"> --}}
                        <table class="table" id="tabelKeranjang">
                            {{-- <thead>
                                <tr>
                                    <th style="width: 30px;"></th>
                                    <th class="text-left">Item</th>
                                    <th style="width: 100px;">Jumlah</th>
                                    <th style="width: 100px;">Satuan</th>
                                    <th style="width: 125px;">Harga</th>
                                    <th style="width: 125px;">Total</th>
                                </tr>
                            </thead> --}}
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group col-md-8 col-sm-8 col-xs-8" id="pilihTindakan">
                        <div class="row">
                            <label class="control-label">Tindakan</label>
                            <div class="input-group">
                                <div id="tindakanButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnUang" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Uang</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnSama" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Sama</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnLain" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Ganti Barang Lain</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="metodePembayaranButtonGroup" >
                            <label class="control-label">Metode Pembayaran</label>
                            <div class="input-group">
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDeposito" class="btn btn-default" {{ $transaksi_grosir->pelanggan_id == null ? 'disabled' : '' }}><i class="fa fa-check" style="display: none;" ></i> Deposito</button>
                                    </div>
                                    @if($transaksi_grosir->nego_total > $transaksi_grosir->jumlah_bayar)
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnPiutang" class="btn btn-default" {{ $transaksi_grosir->pelanggan_id == null ? 'disabled' : '' }}><i class="fa fa-check" style="display: none;"></i> 
                                            <span style="margin-left: -7px">Bayar Hutang</span>
                                        </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div id="inputTunaiContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Tunai</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" id="inputNominalTunai" class="form-control angka">
                            </div>
                        </div>
                        <div id="inputTransferBankContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_transfer">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNoTransfer" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Transfer</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalTransfer" class="form-control angka">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputKartuContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_kartu">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Jenis Kartu</label>
                                    <select class="form-control select2_single" name="jenis_kartu">
                                        <option value="">Pilih Kartu</option>
                                        <option value="debet">Kartu Debit</option>
                                        <option value="kredit">Kartu Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Transaksi</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorKartu" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Dibayarkan</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputCekContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Cek</label>
                                    <select class="form-control select2_single" name="cek_id">
                                        <option value="">Pilih Cek</option>
                                        <option value="cek_baru">Buat Cek Baru</option>
                                        @foreach ($ceks as $cek)
                                        <option value="{{ $cek->id }}">{{ $cek->nomor }} [{{ \App\Util::ewon($cek->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Cek</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalCek" class="form-control angka" style="height: 38px;" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan Cek karena nominal Cek melebihi kekurangan.</p>
                                </div>
                            </div>
                            <div id="cek_baru" class="row" style="display: none;">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_cek">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor Cek</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorCek" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputBGContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih BG</label>
                                    <select class="form-control select2_single" name="bg_id">
                                        <option value="">Pilih BG</option>
                                        <option value="bg_baru">Buat BG Baru</option>
                                        @foreach ($bgs as $bg)
                                        <option value="{{ $bg->id }}">{{ $bg->nomor }} [{{ \App\Util::ewon($bg->nominal) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal BG</label>
                                    <div class="input-group" style="margin: 0;">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" id="inputNominalBG" class="form-control angka" style="height: 38px;" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <p style="color: #f44e42; display: none; margin: 0; margin-top: 5px;">Tidak bisa membayar menggunakan BG karena nominal BG melebihi kekurangan.</p>
                                </div>
                            </div>
                            <div id="bg_baru" class="row" style="display: none;">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Bank</label>
                                    <select class="form-control select2_single" name="bank_bg">
                                        <option value="">Pilih Bank</option>
                                        @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nomor BG</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">#</div>
                                        <input type="text" id="inputNomorBG" class="form-control" style="height: 38px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inputDepositoContainer" class="form-group row">
                            <div class="line"></div>
                            <label class="control-label">Nominal Deposito<span>{{ $transaksi_grosir->pelanggan_id == null ? '' : App\Util::duit0($transaksi_grosir->pelanggan->titipan) }}</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" id="inputNominalDeposito" class="form-control angka">
                            </div>
                        </div>
                        <div id="inputPiutangContainer" class="form-group row">
                            <div class="line"></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Pilih Transaksi</label>
                                    <select class="form-control select2_single" name="piutang_id">
                                        <option value="">Pilih Transaksi</option>
                                        @foreach ($piutangs as $piutang)
                                        <option value="{{ $piutang->id }}">{{ $piutang->kode_transaksi }} [{{ \App\Util::duit0($piutang->sisa) }}]</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <label class="control-label">Nominal Bayar Hutang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp</div>
                                        <input type="text" name="inputNominalPiutang" id="inputNominalPiutang" class="form-control angka" style="height: 38px;" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-1"></div>
                    <div class="form-group col-md-3" id="inputTotali">
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Sub Total</label>
                                {{-- <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" /> --}}
                                <div class="input-group" style="margin-bottom: 0;">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <div class="row" id="uangKeluar" style="padding-top: 10px">
                            <div class="col-sm-12">
                                <label class="control-label">Jumlah Uang Pengganti</label>
                                <div class="input-group" style="margin-bottom: 0;">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="JumlahUangPengganti" id="JumlahUangPengganti" class="form-control text-right uang" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Jumlah Bayar Barang Lain</label>
                                <div class="input-group" style="margin-bottom: 0;">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" />
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="row form-group" id="keranjangLain" style="padding-top: 20px">
                <div class="form-group" style="padding: 10px">
                    <div class="line"></div>
                    <h2>Keranjang Retur Barang Lain</h2>
                    <div class="line"></div>
                    <div class="row">
                        <div class="form-group col-md-4 col-xs-4">
                            <label class="control-label">Nama Item Barang Retur</label>
                            <select name="item_retur" class="select2_single form-control">
                                <option value="">Pilih Item</option>
                                @foreach($item_list2 as $item)
                                    <option value="{{ $item->kode }}">{{ $item->kode }} [{{$item->nama}}]</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table" id="tabelKeranjangRetur" style="border-bottom: 1px solid #dfdfdf;">
                                    <thead>
                                        <tr>
                                            <th style="width: 25px;"></th>
                                            <th>Item</th>
                                            <th style="width: 100px;">Jumlah</th>
                                            <th style="width: 100px;">Satuan</th>
                                            <th style="width: 200px;">Harga</th>
                                            <th style="width: 200px;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-9 col-xs-9">
                                <div class="row">
                                    <div class="form-group col-sm-12 col-xs-12">
                                        <label class="control-label">Metode Pembayaran</label>
                                        <div id="metodePembayaranButtonGroupIn" class="btn-group btn-group-justified" role="group">
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnTunaiIn" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnTransferIn" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnKartuIn" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnCekIn" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnBGIn" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnDepositoIn" class="btn btn-default" {{ $transaksi_grosir->pelanggan_id == null ? 'disabled' : '' }}><i class="fa fa-check" style="display: none;"></i> Deposito</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputTunaiContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <label class="control-label">Nominal Tunai</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTunaiIn" id="inputNominalTunaiIn" class="form-control angka">
                                        </div>
                                        <div class="col-md-12 sembunyi" id="eror_tunai" style="margin-bottom: 10px;">
                                            <p style="color: #f44e42; margin: 0; margin-top: 5px;">Nominal Melebihi Batas Kapasitas Laci Operator. <a href="{{ url('setoran-kasir') }}">Klik Disini</a>, untuk Setor Ke Operator Grosir</p>
                                        </div>
                                    </div>
                                    <div id="inputTransferBankContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                                <label class="control-label">Pilih Bank</label>
                                                <select class="form-control select2_single" name="bank_transfer_in">
                                                    <option value="">Pilih Bank</option>
                                                    @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Nomor Transfer</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">#</div>
                                                    <input type="text" name="inputNoTransferIn" id="inputNoTransferIn" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Nominal Transfer</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">Rp</div>
                                                    <input type="text" name="inputNominalTransferIn" id="inputNominalTransferIn" class="form-control angka">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputKartuContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Pilih Bank</label>
                                                <select class="form-control select2_single" name="bank_kartu_in">
                                                    <option value="">Pilih Bank</option>
                                                    @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Pilih Jenis Kartu</label>
                                                <select class="form-control select2_single" name="jenis_kartu_in">
                                                    <option value="">Pilih Kartu</option>
                                                    <option value="debet">Kartu Debit</option>
                                                    <option value="kredit">Kartu Kredit</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Nomor Transaksi</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">#</div>
                                                    <input type="text" name="inputNoKartuIn" id="inputNoKartuIn" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Nominal Kartu</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">Rp</div>
                                                    <input type="text" name="inputNominalKartuIn" id="inputNominalKartuIn" class="form-control angka">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputCekContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <div class="row">
                                            <div class="col-xs-6 col-md-6">
                                                <label class="control-label">Nomor Cek</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">#</div>
                                                    <input type="text" name="inputNoCekIn" id="inputNoCekIn" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <label class="control-label">Nominal Cek</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">Rp</div>
                                                    <input type="text" name="inputNominalCekIn" id="inputNominalCekIn" class="form-control angka" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputBGContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <div class="row">
                                            <div class="col-xs-6 col-md-6">
                                                <label class="control-label">Nomor BG</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">#</div>
                                                    <input type="text" name="inputNoBGIn" id="inputNoBGIn" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <label class="control-label">Nominal BG</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">Rp</div>
                                                    <input type="text" name="inputNominalBGIn" id="inputNominalBGIn" class="form-control angka" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputDepositoContainerIn" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <label class="control-label">Nominal Deposito <span>{{ $transaksi_grosir->pelanggan_id == null ? '' : App\Util::duit0($transaksi_grosir->pelanggan->titipan) }}</span></label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalDepositoIn" id="inputNominalDepositoIn" class="form-control angka">
                                        </div>
                                    </div>
                                    {{-- <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                        <div id="diambilAtauDikirmButtonGroup" class="btn-group btn-group-justified" role="group">
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnDiambil" class="btn btn-default"><i class="fa"></i> Diambil</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" id="btnDikirim" class="btn btn-default"><i class="fa"></i> Dikirim</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inputDikirmContainer" class="form-group col-sm-12 col-xs-12">
                                        <div class="line"></div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                                <label class="control-label">Pilih Karyawan</label>
                                                <select class="form-control select2_single" name="user_id">
                                                    <option value="">Pilih Karyawan</option>
                                                    @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                                    @endforeach
                                                    <option value="-">Pengirim Lain</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <label class="control-label">Pengirim Lain</label>
                                                <div class="input-group" style="margin: 0;">
                                                    <div class="input-group-addon">#</div>
                                                    <input type="text" id="inputPengirimLain" class="form-control" disabled="" style="height: 38px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="LainGrup">
                                <div class="row">
                                    <div class="form-group col-md-12" id="">
                                        <label class="control-label">Sub Total</label>
                                        <div class="input-group" style="margin-bottom: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="NilaiBarangRetur" id="NilaiBarangRetur" class="form-control text-right" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" id="">
                                        <label class="control-label">Jumlah Bayar</label>
                                        <div class="input-group" style="margin-bottom: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="JumlahBayarMasuk" id="JumlahBayarMasuk" class="form-control text-right" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" id="">
                                        <label class="control-label">Kekurangan</label>
                                        <div class="input-group" style="margin-bottom: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="JumlahBayarMasukKurang" id="JumlahBayarMasukKurang" class="form-control text-right" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" id="">
                                        <label class="control-label">Kembalian</label>
                                        <div class="input-group" style="margin-bottom: 0;">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="JumlahBayarMasukKembali" id="JumlahBayarMasukKembali" class="form-control text-right" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="formSimpanContainer">
                        <form id="form-simpan" action="{{ url('transaksi-grosir/'.$transaksi_grosir->id.'/retur') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="kode_retur" value="" />
                            <input type="hidden" name="transaksi_penjualan_id" value="{{ $transaksi_grosir->id }}" />
                            <input type="hidden" name="harga_total" value="0" />
                            <input type="hidden" name="jumlah_bayar" value="0" />
                            <input type="hidden" name="kurang" value="0" />
                            <input type="hidden" name="status" value="sama" />

                            <input type="hidden" name="total_uang" />
                            <input type="hidden" name="nominal_tunai" class="uang_retur" />

                            <input type="hidden" name="no_transfer" />
                            <input type="hidden" name="bank_transfer" />
                            <input type="hidden" name="nominal_transfer" class="uang_retur" />

                            <input type="hidden" name="no_kartu" />
                            <input type="hidden" name="bank_kartu" />
                            <input type="hidden" name="jenis_kartu" />
                            <input type="hidden" name="nominal_kartu" class="uang_retur" />

                            <input type="hidden" name="cek_id" />
                            <input type="hidden" name="no_cek" />
                            <input type="hidden" name="bank_cek" />
                            <input type="hidden" name="nominal_cek" class="uang_retur" />

                            <input type="hidden" name="bg_id" />
                            <input type="hidden" name="no_bg" />
                            <input type="hidden" name="bank_bg" />
                            <input type="hidden" name="nominal_bg" class="uang_retur" />

                            <input type="hidden" name="nominal_titipan" class="uang_retur" />

                            <input type="hidden" name="piutang_id" />
                            <input type="hidden" name="piutang_max" />
                            <input type="hidden" name="nominal_piutang" class="uang_retur" />
                            <input type="hidden" name="kembali" />

                            <input type="hidden" name="hutang[]" />

                            <div id="append-section"></div>
                            <div id="append-section-in-metode">
                                <input type="hidden" name="jumlah_bayar_in" />
                                <input type="hidden" name="nominal_tunai_in" />
                                <input type="hidden" name="no_transfer_in" />
                                <input type="hidden" name="bank_transfer_in" />
                                <input type="hidden" name="nominal_transfer_in" />
                                <input type="hidden" name="no_kartu_in" />
                                <input type="hidden" name="bank_kartu_in" />
                                <input type="hidden" name="jenis_kartu_in" />
                                <input type="hidden" name="nominal_kartu_in" />
                                <input type="hidden" name="no_cek_in" />
                                <input type="hidden" name="nominal_cek_in" />
                                <input type="hidden" name="no_bg_in" />
                                <input type="hidden" name="nominal_bg_in" />
                                <input type="hidden" name="nominal_titipan_in" />
                            </div>
                            <div id="append-section-in"></div>
                        </form>
                        <div class="clearfix">
                            <div class="col-md-3 pull-right">
                                {{-- <button type="submit" class="btn btn-success" id="submit" disabled style="width: 100px;"> --}}
                                <button type="submit" class="btn btn-success" id="submit" disabled style="width: 100%;">
                                    <i class="fa fa-check"></i> OK
                                </button>
                            </div>
                        </div>
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var items = [];
        var item_all = [];
        var selected_items = [];
        var selected_retur = [];
        // var user_level = '{{ Auth::user()->level_id }}';

        var satuans = null;
        var transaksi_penjualan = null;
        var relasi_transaksi_penjualan = null;
        var cash_drawer = 0;
        var is_kasir = 0;
        var user_level = 0;
        var setoran_buka = 0;
        var money_limit = 0;
        // $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());

        function isBtnSimpanDisabled() {
            // console.log('isBtnSimpanDisabled');
            if ($('#btnUang').hasClass('btn-success')) {
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
                var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
                var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
                var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
                var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
                var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
                var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
                var nominal_piutang = parseFloat($('input[name="nominal_piutang"]').val());
                // console.log(nominal_tunai, nominal_transfer, nominal_cek, nominal_bg, nominal_kartu, nominal_titipan, nominal_piutang);
                var no_transfer = $('input[name="no_transfer"]').val();
                var bank_transfer = $('input[name="bank_transfer"]').val();
                var no_kartu = $('input[name="no_kartu"]').val();
                var bank_kartu = $('input[name="bank_kartu"]').val();
                var jenis_kartu = $('input[name="jenis_kartu"]').val();
                var no_cek = $('input[name="no_cek"]').val();
                var bank_cek = $('input[name="bank_cek"]').val();
                var no_bg = $('input[name="no_bg"]').val();
                var bank_bg = $('input[name="bank_bg"]').val();
                var piutang_id = $('input[name="piutang_id"]').val();

                if (isNaN(harga_total)) harga_total = 0;
                if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
                if (isNaN(nominal_tunai)) nominal_tunai = 0;
                if (isNaN(nominal_transfer)) nominal_transfer = 0;
                if (isNaN(nominal_kartu)) nominal_kartu = 0;
                if (isNaN(nominal_cek)) nominal_cek = 0;
                if (isNaN(nominal_bg)) nominal_bg = 0;
                if (isNaN(nominal_titipan)) nominal_titipan = 0;
                if (isNaN(nominal_piutang)) nominal_piutang = 0;
                // console.log(nominal_tunai, nominal_titipan);

                if ($('#btnTunai').hasClass('btn-danger') && nominal_tunai <= 0) return true;

                if ($('#btnTransfer').hasClass('btn-warning') && (nominal_transfer <= 0 || no_transfer == '' || bank_transfer == '')) return true;

                if ($('#btnKartu').hasClass('btn-info') && (nominal_kartu <= 0 || no_kartu == '' || bank_kartu == '' || jenis_kartu == '')) return true;

                // if ($('#btnCek').hasClass('btn-success') && (nominal_cek <= 0 || no_cek == '' || bank_cek == '')) return true;

                if ($('#btnCek').hasClass('btn-success')) {
                    var cek_id = $('select[name="cek_id"]').val();
                    if (cek_id == '') {
                        // disabled
                        return true;
                    } else if (cek_id == 'cek_baru' && (nominal_cek <= 0 || no_cek == '' || bank_cek == '')) {
                        // enabled
                        return true;
                    } else if (!isNaN(parseInt(cek_id)) && (nominal_cek <= 0 || no_cek == '')) {
                        return true;
                    } else {
                        // return false;
                    }
                }

                // if ($('#btnBG').hasClass('btn-BG') && (nominal_bg <= 0 || no_bg == '' || bank_bg == '')) return true;

                if ($('#btnBG').hasClass('btn-BG')) {
                    var bg_id = $('select[name="bg_id"]').val();
                    if (bg_id == '') {
                        // disabled
                        return true;
                    } else if (bg_id == 'bg_baru' && (nominal_bg <= 0 || no_bg == '' || bank_bg == '')) {
                        // enabled
                        return true;
                    } else if (!isNaN(parseInt(bg_id)) && (nominal_bg <= 0 || no_bg == '')) {
                        return true;
                    } else {
                        // return false;
                    }
                }

                if ($('#btnDeposito').hasClass('btn-purple') && (nominal_titipan <= 0)) return true;

                if ($('#btnPiutang').hasClass('btn-primary') && (nominal_piutang <= 0 || piutang_id == '')) return true;

                if (nominal_tunai > 0 || nominal_transfer > 0 || nominal_cek > 0 || nominal_bg > 0 || nominal_kartu > 0 || nominal_titipan > 0 || nominal_piutang > 0) {
                    // console.log('oi', jumlah_bayar, harga_total);
                    if (jumlah_bayar != harga_total) return true;
                    return false;
                } else {
                    return true;
                }
            }

            if ($('#btnSama').hasClass('btn-success')) {
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                if (harga_total <= 0) return true;
            }

            if ($('#btnLain').hasClass('btn-success')) {
                // masuk
                var harga_total = parseFloat($('input[name="harga_total"]').val());
                // keluar
                var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
                // masuk
                var jumlah_bayar_in = parseFloat($('input[name="jumlah_bayar_in"]').val());
                var nominal_tunai_in = parseFloat($('input[name="nominal_tunai_in"]').val());

                // console.log(harga_total, jumlah_bayar, jumlah_bayar_in);
                // boleh
                if(nominal_tunai_in > 0 && harga_total + jumlah_bayar_in > jumlah_bayar) return false;
                if (harga_total + jumlah_bayar_in == jumlah_bayar) return false;
                // tidak boleh
                else return true;
                // if (jumlah_bayar != harga_total) return true;
                // return false;
            }

            var has_error = false;
            var temp_has_error = false;
            $('#tabelKeranjang tr').each(function(index, el) {
                temp_has_error = $(this).find('#inputJumlahItem').hasClass('has-error');
                if (temp_has_error) has_error = true;
            });
            // console.log(has_error);
            if (has_error) return true;

            return false;
        }

        function updateHargaOnKeyup() {
            // console.log('updateHargaOnKeyup');
            // var harga_total = parseFloat($('input[name="harga_total"]').val());
            var status_retur = $('input[name="status"]').val();
            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val());
            var nominal_transfer = parseFloat($('input[name="nominal_transfer"]').val());
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());
            var nominal_kartu = parseFloat($('input[name="nominal_kartu"]').val());
            var nominal_titipan = parseFloat($('input[name="nominal_titipan"]').val());
            var nominal_piutang = parseFloat($('input[name="nominal_piutang"]').val());
            var nominal_tunai_in = parseFloat($('input[name="nominal_tunai_in"]').val());
            var nominal_transfer_in = parseFloat($('input[name="nominal_transfer_in"]').val());
            var nominal_cek_in = parseFloat($('input[name="nominal_cek_in"]').val());
            var nominal_bg_in = parseFloat($('input[name="nominal_bg_in"]').val());
            var nominal_kartu_in = parseFloat($('input[name="nominal_kartu_in"]').val());
            var nominal_titipan_in = parseFloat($('input[name="nominal_titipan_in"]').val());

            // if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if (isNaN(nominal_tunai_in)) nominal_tunai_in = 0;
            if (isNaN(nominal_transfer_in)) nominal_transfer_in = 0;
            if (isNaN(nominal_cek_in)) nominal_cek_in = 0;
            if (isNaN(nominal_bg_in)) nominal_bg_in = 0;
            if (isNaN(nominal_kartu_in)) nominal_kartu_in = 0;
            if (isNaN(nominal_titipan_in)) nominal_titipan_in = 0;

            // console.log(selisih);
            var jumlah_bayar = 0;
            var jumlah_bayar_in = 0;
            if (status_retur == 'uang') {
                // console.log(nominal_tunai, nominal_titipan);
                jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kartu + nominal_titipan + nominal_piutang;
                $('#JumlahUangPengganti').val(jumlah_bayar.toLocaleString());
            } else if (status_retur == 'lain') {
                
                // ngitung semua subtotal di append-section-in
                // var subtotal_in = 0;
                $('input[name="subtotal_in[]"]').each(function(index, el) {
                    var temp = $(el).val();
                    if (isNaN(temp)) temp = 0;
                    jumlah_bayar += parseFloat(temp);
                });
                $('#NilaiBarangRetur').val(jumlah_bayar.toLocaleString());
                $('input[name="jumlah_bayar"]').val(jumlah_bayar);

                var barang_retur = parseFloat($('input[name="jumlah_bayar"]').val());
                var harga_total = parseFloat($('input[name="harga_total"]').val());

                if (isNaN(barang_retur)) barang_retur = 0;
                if (isNaN(harga_total)) harga_total = 0;

                var selisih = barang_retur - harga_total;

                jumlah_bayar_in += nominal_tunai_in + nominal_transfer_in + nominal_cek_in + nominal_bg_in + nominal_kartu_in + nominal_titipan_in;
                $('#JumlahBayarMasuk').val(jumlah_bayar_in.toLocaleString());
                
                var hitung = selisih - jumlah_bayar_in;
                // console.log(selisih);
                if(hitung < 0){
                    hitung = hitung * -1;
                    $('#JumlahBayarMasukKembali').val(hitung.toLocaleString());
                    $('#JumlahBayarMasukKurang').val(0);
                    $('input[name="kembali"]').val(hitung);
                }else if(hitung > 0){
                    $('#JumlahBayarMasukKembali').val(0);
                    $('#JumlahBayarMasukKurang').val(hitung.toLocaleString());
                    $('input[name="kembali"]').val(0);
                }else{
                    $('#JumlahBayarMasukKembali').val(0);
                    $('#JumlahBayarMasukKurang').val(0);
                    $('input[name="kembali"]').val(0);
                }
            }

            // $('input[name="nominal_tunai"]').val(nominal_tunai);
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            $('input[name="nominal_cek"]').val(nominal_cek);
            $('input[name="nominal_bg"]').val(nominal_bg);
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            $('input[name="nominal_titipan"]').val(nominal_titipan);
            $('input[name="nominal_titipan"]').val(nominal_titipan);
            $('input[name="nominal_piutang"]').val(nominal_piutang);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="jumlah_bayar_in"]').val(jumlah_bayar_in);

            $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isBtnSimpanDisabled());
        }

        function cek_laci(id) {
            if (user_level == 3) {
                var url = "{{ url('transaksi-grosir/retur/user') }}"+'/'+id;

                $.get(url, function(data) {
                    if(data.bukaan == null){
                        setoran_buka = 0;
                    }else{
                        setoran_buka = data.bukaan.nominal;
                    }

                    money_limit = data.limit.nominal;
                    cash_drawer = data.cash_drawer.nominal;
                });
            }
        }

        $(document).ready(function() {
            var url = "{{ url('transaksi-grosir') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('.select2_single').each(function(index, el) {
                $(el).val('').change();
            }); 

            $('#submit').hide();
            $('#inputTotali').hide();
            $('#pilihTindakan').hide();

            $('input[name="harga_total"]').val(0);
            $('#keranjangLain').hide();
            $('#metodePembayaranButtonGroup').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('');
            $('#inputDepositoContainer').hide();
            $('#inputPiutangContainer').hide();
            $('#uangKeluar').hide();
            $('#uangKeluar').val('');

            $('#inputNominalTunai').val('');
            $('#inputNominalTransfer').val('');
            $('#inputNominalCek').val('');
            $('#inputNominalBG').val('');
            $('#inputNominalKartu').val('');
            $('#inputNominalDeposito').val('');
            $('#inputNominalPiutang').val('');
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(0);
            $('#formSimpanContainer').find('input[name="nominal_piutang"]').val(0);

            $('#btnSama').removeClass('btn-default');
            $('#btnSama').addClass('btn-success');
            // $('#btnSama').find('i').show('fast');

            // $('.panel_toolbox').trigger('click');

            items = '{{ json_encode($item_list2) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);

            item_all = '{{ json_encode($item_all_) }}';
            item_all = item_all.replace(/&quot;/g, '"');
            item_all = JSON.parse(item_all);

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            transaksi_penjualan = '{{ $transaksi_grosir }}';
            transaksi_penjualan = transaksi_penjualan.replace(/&quot;/g, '"');
            transaksi_penjualan = JSON.parse(transaksi_penjualan);

            relasi_transaksi_penjualan = '{{ $relasi_transaksi_penjualan }}';
            relasi_transaksi_penjualan = relasi_transaksi_penjualan.replace(/&quot;/g, '"');
            relasi_transaksi_penjualan = JSON.parse(relasi_transaksi_penjualan);

            $('#tabel-item tbody > tr').each(function(index, el) {
                var id = parseInt($(el).attr('id'));
                var relasi = null;
                var konversi = null;
                var v_jumlah = [];
                var v_satuan = [];
                var text_satuan = '';

                for (var i = 0; i < relasi_transaksi_penjualan.length; i++) {
                    if (relasi_transaksi_penjualan[i].id == id) {
                        relasi = relasi_transaksi_penjualan[i];
                        break;
                    }
                }

                var item = relasi.item;
                var jumlah = relasi.jumlah;
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = item.satuan_pembelians[i];
                    var temp_konversi = satuan.konversi;
                    if (jumlah / temp_konversi >= 1) {
                        if (konversi == null) konversi = temp_konversi;
                        text_satuan += Math.floor(jumlah / temp_konversi);
                        text_satuan += ' ' + satuan.satuan.kode;
                        // console.log(text_satuan);
                        if (i != item.satuan_pembelians.length - 1) text_satuan += '<br>';
                        jumlah = jumlah % temp_konversi;
                    }
                }

                $('#tabel-item').DataTable();

                // console.log(text_satuan);
                // $(el).find('#jumlah').html(text_satuan);

                var harga = $(el).find('#harga').text();
                harga = harga.split('p')[1];
                harga = parseFloat(harga.replace(/\D/g, ''), 10);
                harga *= konversi;
                harga = Math.ceil(harga / 100) * 100;
                $(el).find('#harga').text('Rp'+harga.toLocaleString());
            });

            $('.jumlah').each(function(index, el) {
                var item_kode = $(el).parents('tr').first().attr('item_kode');
                var jumlah = parseFloat($(el).text());
                if (isNaN(jumlah)) jumlah = 0;
                jumlah = parseInt(jumlah);
                // console.log(item_kode);

                var satuan_item = [];
                // console.log(items.length);
                for (var i = 0; i < item_all.length; i++) {
                    var item = item_all[i];
                    // console.log(item.kode);
                    if (item.kode == item_kode) {
                        var satuans = item.satuan_pembelians;
                        // console.log(item.kode);
                        for (var j = 0; j < satuans.length; j++) {
                            var satuan = {
                                id: satuans[j].satuan.id,
                                kode: satuans[j].satuan.kode,
                                konversi: satuans[j].konversi
                            }
                            satuan_item.push(satuan);
                        }
                    }
                }
                // console.log(satuan_item);

                // var jumlah1 = '';
                var jumlah2 = '';
                var temp_jumlah = jumlah;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_jumlah > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_jual = parseInt(temp_jumlah / satuan.konversi);
                        // if (jumlah_jual > 0 && temp_jumlah % satuan.konversi == 0) {
                        //     jumlah2 += jumlah_jual;
                        //     jumlah2 += ' ';
                        //     jumlah2 += satuan.kode;
                        //     temp_jumlah = temp_jumlah % satuan.konversi;
                        // }
                        if (jumlah_jual > 0) {
                            jumlah2 += jumlah_jual;
                            jumlah2 += ' ';
                            jumlah2 += satuan.kode;
                            temp_jumlah = temp_jumlah % satuan.konversi;

                            if (i != satuan_item.length - 1 && temp_jumlah > 0) jumlah2 += ' ';
                        }
                    }
                }
                // console.log(jumlah2);

                $(el).text(jumlah2);
                if (jumlah2 == '') $(el).text(jumlah);
            });

            var id = {{ $user->id }};
            user_level = {{ $user->level_id }};
            cek_laci(id);
        });

        // Buat ambil nomer transaksi terakhir
        $(window).on('load', function(event) {
            // var url = "{{ url('transaksi-pembelian/retur/last.json') }}";
            
            // $.get(url, function(data) {
            //     var kode = 1;
            //     if (data.retur_pembelian !== null) {
            //         var kode_retur = data.retur_pembelian.kode_retur;
            //         kode = parseInt(kode_retur.split('/')[0]);
            //         kode++;
            //     }

            //     kode = int4digit(kode);
            //     var tanggal = printTanggalSekarang('dd/mm/yyyy');
            //     kode_retur = kode + '/TRABR/' + tanggal;
            //     $('input[name="kode_retur"]').val(kode_retur);
            //     $('#kodeReturTitle').text(kode_retur);
            // });
        });

        function cariItem(kode, item_parent, url, tr) {
            $.get(url, function(data) {
                // console.log(url, data);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;

                var transaksi_penjualan = data.transaksi_penjualan;
                var relasi_transaksi_penjualan = data.relasi_transaksi_penjualan;
                var history_jual_bundle = data.history_jual_bundle;
                var tp_nego_total = parseFloat(transaksi_penjualan.nego_total);
                var tp_potongan_penjualan = parseFloat(transaksi_penjualan.potongan_penjualan);
                var tp_harga_total = parseFloat(transaksi_penjualan.harga_total);
                var selisih = tp_harga_total - tp_potongan_penjualan;
                // if (parseFloat(relasi_transaksi_penjualan.nego) > 0) {
                //     tp_nego_total += parseFloat(relasi_transaksi_penjualan.nego);
                // } else {
                //     tp_nego_total += parseFloat(relasi_transaksi_penjualan.subtotal);
                // }
                // console.log(tp_nego_total, tp_harga_total);

                var v_harga = 0;
                if (item_parent == '-') {
                    if (tp_potongan_penjualan > 0) {
                        // nego
                        // console.log('nego');
                        v_harga = selisih / tp_harga_total * parseFloat(relasi_transaksi_penjualan.subtotal) / parseFloat(relasi_transaksi_penjualan.jumlah);
                        // console.log(v_harga);
                    } else {
                        // tidak nego
                        // console.log('tidak nego');
                        v_harga = parseFloat(relasi_transaksi_penjualan.subtotal) / parseFloat(relasi_transaksi_penjualan.jumlah);
                        // console.log(v_harga);
                    }
                } else {
                    // harga jual retur untuk item bundle sesuai dengan harga beli
                    v_harga = parseFloat(history_jual_bundle.harga) * 1.1;
                }
                // console.log(v_harga);
                v_harga = Math.floor(v_harga / 100) * 100;
                // console.log(v_harga);
                $('#submit').show();
                $('#inputTotali').show();
                $('#pilihTindakan').show();

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    var satuan = {
                        id: item.satuan_pembelians[i].id,
                        satuan_id: item.satuan_pembelians[i].satuan.id,
                        kode: item.satuan_pembelians[i].satuan.kode,
                        konversi: item.satuan_pembelians[i].konversi
                    }
                    satuan_item.push(satuan);
                }

                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };
                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                var divs = '';
                var div_rusak = '';
                var input_jumlah_rusak = '';
                var checkbox_jumlah_rusak = '';
                var input_relasi_stok_penjualan = '';
                var relasi_stok_penjualan = [];
                if (item_parent == '') {
                    relasi_stok_penjualan = relasi_transaksi_penjualan.relasi_stok_penjualan;
                } else {
                    for (var i = 0; i < relasi_transaksi_penjualan.relasi_stok_penjualan.length; i++) {
                        var rsp = relasi_transaksi_penjualan.relasi_stok_penjualan[i];
                        if (rsp.stok.item_kode == kode) {
                            relasi_stok_penjualan.push(rsp);
                        }
                    }
                }

                for (var i = 0; i < relasi_stok_penjualan.length; i++) {
                    var rsp = relasi_stok_penjualan[i];
                    // console.log(rsp);
                    if (rsp.stok.retur <= 0) {
                        var kadaluarsa = ymd2dmy(rsp.stok.kadaluarsa);
                        if (kadaluarsa == null) kadaluarsa = '00-00-0000';
                        var jumlah = rsp.jumlah;
                        // console.log(kadaluarsa);

                        divs += ''+
                            '<div class="row">'+
                                '<div class="form-group col-md-12 input-grup" jumlah="'+jumlah+'">'+
                                    '<div id="rsp_id" rsp-id="'+rsp.id+'" class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            kadaluarsa+
                                        '</div>'+
                                        '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="'+(i==0?1:'')+'" />'+
                                        '<div id="pilihSatuan" class="input-group-btn">'+
                                            '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                            ul_satuan+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                        div_rusak += ''+
                            '<div class="row">'+
                                '<div class="form-group col-md-12">'+
                                    '<div id="rsp_id" rsp-id="'+rsp.id+'" class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            '<input type="checkbox" name="checkRusak" id="checkRusak" />'+
                                        '</div>'+
                                        '<input type="text" name="inputRusak" id="inputRusak" class="form-control input-sm" value="Rusak" readonly />'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                        input_jumlah_rusak += '<input type="hidden" name="jumlah_rusak['+kode+'][]" id="jumlah_rusak-'+kode+'-'+rsp.id+'" value="'+(i==0?1:0)+'" />';
                        checkbox_jumlah_rusak += '<input type="hidden" name="is_rusak['+kode+'][]" id="is_rusak-'+kode+'-'+rsp.id+'" value="0" />';
                        input_relasi_stok_penjualan += '<input type="hidden" name="relasi_stok_penjualan['+kode+'][]" id="relasi_stok_penjualan-'+kode+'-'+rsp.id+'" value="'+rsp.id+'" />';
                    }
                }

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="satuan_id[]" id="satuan-' +kode+'" value="'+ satuan_item[satuan_item.length-1].satuan_id +'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="'+parseFloat(v_harga) +'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="subtotal[]" id="subtotal-'+kode+'" value="'+parseFloat(v_harga)+'" class="subtotal" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="stoktotal[]" id="stoktotal-'+kode+'" value="'+stoktotal+'" class="stoktotal" />');
                    $('#form-simpan').find('#append-section').append(input_jumlah_rusak);
                    $('#form-simpan').find('#append-section').append(checkbox_jumlah_rusak);
                    $('#form-simpan').find('#append-section').append(input_relasi_stok_penjualan);

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td style="width: 50px;">'+
                                '<h3><i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: #c9302c; padding-top: 8px;"></h3></i>'+
                            '</td>'+
                            '<td><h3><small>'+kode+'</small></h3><h3>'+nama+'</h3></td>'+
                            '<td style="width: 250px;">'+
                                '<label class="control-label">Jumlah</label>'+
                                divs+
                            '</td>'+
                            '<td style="width: 200px;">'+
                                '<label class="control-label">Status Item</label>'+
                                div_rusak+
                            '</td>'+
                            '<td style="width: 200px;">'+
                                '<div class="row">'+
                                    '<div class="form-group col-md-12">'+
                                        '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                            '<label id="satuanHarga" class="control-label">Harga</label>'+
                                            '<div class="input-group" style="margin-bottom: 0;">'+
                                                '<div class="input-group-addon">'+
                                                    'Rp'+
                                                '</div>'+
                                                '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(v_harga).toLocaleString() + '" readonly="readonly" />'+
                                                '<input type="hidden" name="HargaPerSatuan" id="HargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(v_harga)+ '"/>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="row">'+
                                    '<div class="form-group col-md-12">'+
                                        '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                            '<label class="control-label">Total</label>'+
                                            '<div class="input-group" style="margin-bottom: 0;">'+
                                                '<div class="input-group-addon">'+
                                                    'Rp'+
                                                '</div>'+
                                                '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control text-right input-sm angka" value="' + parseFloat(v_harga).toLocaleString() + '" readonly="readonly" />'+
                                                '<input type="hidden" name="SubTotal" id="SubTotal" class="form-control input-sm" value="' + parseFloat(v_harga)+ '"/>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                        '</tr>');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                // console.log(satuan_item);
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr);

                var harga_total = $('input[name="harga_total"]').val();
                var total = parseFloat(harga_total) + parseFloat(v_harga);

                $('#inputHargaTotal').val((total).toLocaleString());
                $('#formSimpanContainer').find('input[name="harga_total"]').val(parseFloat(total));
                $('#tabelKeranjang').find('#inputJumlahItem').trigger('keyup');
                updateHargaOnKeyup();
            });
        }

        function cariItemRetur(kode, url, tr) {
            $.get(url, function(data) {
                console.log(url, data);
                var item = data.item;
                var nama = data.item.nama;
                var stoktotal = parseFloat(data.item.stoktotal);
                var harga = data.harga;
                // console.log(harga);
                var v_harga = 0;

                if (transaksi_penjualan.status == 'eceran') {
                    v_harga = parseFloat(harga.eceran);
                } else if (transaksi_penjualan.status == 'grosir') {
                    v_harga = parseFloat(harga.grosir);
                }
                // v_harga = v_harga - (v_harga * item.diskon / 100);
                // v_harga = Math.ceil(v_harga / 100) * 100;

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < item.satuans.length; j++) {
                        if (item.satuans[j].satuan_id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: item.satuan_pembelians[i].satuan.id,
                                kode: item.satuan_pembelians[i].satuan.kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }

                var select_satuans = "";
                // satuan_item.sort(function(a, b) {
                //     return a.konversi-b.konversi
                // });

                for (var i = 0; i < satuan_item.length; i++) {
                    // select_satuans[i] = satuan_item[i];
                    if (i == satuan_item.length - 1) {
                        select_satuans += `<option value="`+satuan_item[i].id+'-'+satuan_item[i].konversi+`" selected>`+ satuan_item[i].kode +`</option>`;
                    } else {
                        select_satuans += `<option value="`+satuan_item[i].id+'-'+satuan_item[i].konversi+`">`+ satuan_item[i].kode +`</option>`;
                    }
                }
                // #satuan-'+kode
                var pilihan_satuan = 
                    `<select name="pilihSatuanIn" id="pilihSatuanIn" class="form-control input-sm">`
                    +select_satuans+`</select>`;

                if (tr === undefined) {
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="item_kode_in[]" id="itemIn-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="jumlah_in[]" id="jumlahIn-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="satuan_id_in[]" id="satuanIn-' +kode+'" value="'+ satuan_item[satuan_item.length - 1].id +'" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="konversi_in[]" id="konversiIn-' +kode+'" value="'+ satuan_item[satuan_item.length - 1].konversi +'" />');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="harga_in[]" id="hargaIn-' + kode + '" value="'+v_harga+'"/>');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="subtotal_in[]" id="subtotalIn-'+kode+'" value="'+v_harga+'" class="subtotalIn"/>');
                    $('#form-simpan').find('#append-section-in').append('<input type="hidden" name="stoktotal_in[]" id="stoktotalIn-'+kode+'" value="'+stoktotal+'"/>');

                    $('#tabelKeranjangRetur').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang retur" id="removeIn" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="padding-top: 13px">'+nama+'</td>'+
                            '<td id="inputJumlahItemInTD">'+
                                '<input type="text" name="inputJumlahItemIn" id="inputJumlahItemIn" class="form-control input-sm angka" value="1" />'+
                            '</td>'+
                            '<td id="pilihSatuanInTD">'+ pilihan_satuan +'</td>'+
                            '<td>'+
                                '<div class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        'Rp'+
                                    '</div>'+
                                    '<input type="text" name="inputHargaPerSatuanIn" id="inputHargaPerSatuanIn" class="form-control text-right input-sm" value="' + parseFloat(v_harga).toLocaleString() + '" readonly />'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="input-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group-addon">'+
                                        'Rp'+
                                    '</div>'+
                                    '<input type="text" name="inputSubTotalIn" id="inputSubTotalIn" class="form-control text-right input-sm SubTotalIn" value="' + parseFloat(v_harga).toLocaleString() + '" readonly />'+
                                '</div>'+
                            '</td>'+
                        '</tr>');

                    var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
                    jumlah_bayar += v_harga;
                    $('#NilaiBarangRetur').val(jumlah_bayar.toLocaleString());
                    $('input[name="jumlah_bayar"]').val(jumlah_bayar);

                    updateHargaOnKeyup();
                }
            });
        }

        function deleteMe( arr, me ) {
           var i = arr.length;
           while( i-- ) if (arr[i] == me ) arr.splice(i,1);
        }

        function removeSelectedItem(item) {
            var index = selected_items.indexOf(item);
            if (index > -1) {
                selected_items.splice(index, 1);
            }
        }

        function removeSelectedRetur(item) {
            var index = selected_retur.indexOf(item);
            if (index > -1) {
                selected_retur.splice(index, 1);
            }
        }

        function uangClose() {
            // $('#inputTunaiContainer').hide();
            // $('#inputTransferBankContainer').hide();
            // $('#inputTransferBankContainer').find('input').val('');
            // $('#inputCekContainer').hide();
            // $('#inputCekContainer').find('input').val('');
            // $('#inputBGContainer').hide();
            // $('#inputBGContainer').find('input').val('');
            // $('#inputKartuContainer').hide();
            // $('#inputKartuContainer').find('input').val('');
            // $('#inputDepositoContainer').hide();
            // $('#inputPiutangContainer').hide();

            // $('#btnTunai').removeClass('btn-danger');
            // $('#btnTunai').addClass('btn-default');
            // $('#btnTunai').find('i').hide('fast');
            if ($('#btnTunai').hasClass('btn-danger')) $('#btnTunai').trigger('click');

            // $('#btnTransfer').removeClass('btn-warning');
            // $('#btnTransfer').addClass('btn-default');
            // $('#btnTransfer').find('i').hide('fast');
            if ($('#btnTransfer').hasClass('btn-warning')) $('#btnTransfer').trigger('click');

            // $('#btnKartu').removeClass('btn-info');
            // $('#btnKartu').addClass('btn-default');
            // $('#btnKartu').find('i').hide('fast');
            if ($('#btnKartu').hasClass('btn-info')) $('#btnKartu').trigger('click');

            // $('#btnCek').removeClass('btn-success');
            // $('#btnCek').addClass('btn-default');
            // $('#btnCek').find('i').hide('fast');
            if ($('#btnCek').hasClass('btn-success')) $('#btnCek').trigger('click');

            // $('#btnBG').removeClass('btn-BG');
            // $('#btnBG').addClass('btn-default');
            // $('#btnBG').find('i').hide('fast');
            if ($('#btnBG').hasClass('btn-BG')) $('#btnBG').trigger('click');

            // $('#btnDeposito').removeClass('btn-purple');
            // $('#btnDeposito').addClass('btn-default');
            // $('#btnDeposito').find('i').hide('fast');
            if ($('#btnDeposito').hasClass('btn-purple')) $('#btnDeposito').trigger('click');

            // $('#btnPiutang').removeClass('btn-primary');
            // $('#btnPiutang').addClass('btn-default');
            // $('#btnPiutang').find('i').hide('fast');
            if ($('#btnPiutang').hasClass('btn-primary')) $('#btnPiutang').trigger('click');
            $('#uangKeluar').hide();
            $('#uangKeluar').val('');
            // $('#inputNominalTunai').val('');
            // $('#inputNominalTransfer').val('');
            // $('#inputNominalCek').val('');
            // $('#inputNominalBG').val('');
            // $('#inputNominalKartu').val('');
            // $('#inputNominalDeposito').val('');
            // $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_transfer"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_transfer"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="jenis_kartu"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_kartu"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_cek"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_cek"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_cek"]').val(0);
            // $('#formSimpanContainer').find('input[name="no_bg"]').val('');
            // $('#formSimpanContainer').find('input[name="bank_bg"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_bg"]').val(0);
            // $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(0);
            // $('#formSimpanContainer').find('input[name="piutang_id"]').val('');
            // $('#formSimpanContainer').find('input[name="nominal_piutang"]').val(0);
        }

        function lainClose() {
            selected_retur = [];
            $('select[name="item_retur"]').val('').trigger('change');
            $('#tabelKeranjangRetur').find('tbody tr').remove();
            $('#form-simpan').find('#append-section-in').find('input').remove();

            $('#inputTunaiContainerIn').hide();
            // $('#inputTransferBankContainerIn').find('select').val('').trigger('change');
            // $('#inputTransferBankContainerIn').find('input').val('');
            $('#inputTransferBankContainerIn').hide();
            // $('#inputKartuContainerIn').find('select').val('').trigger('change');
            // $('#inputKartuContainerIn').find('input').val('');
            $('#inputKartuContainerIn').hide();
            // $('#inputCekContainerIn').find('input').val('');
            $('#inputCekContainerIn').hide();
            // $('#inputBGContainerIn').find('input').val('');
            $('#inputBGContainerIn').hide();
            // $('#inputDepositoContainerIn').find('input').val('');
            $('#inputDepositoContainerIn').hide();

            // $('#btnTunaiIn').removeClass('btn-danger');
            // $('#btnTunaiIn').addClass('btn-default');
            // $('#btnTunaiIn').find('i').hide('fast');
            if ($('#btnTunaiIn').hasClass('btn-danger')) $('#btnTunaiIn').trigger('click');

            // $('#btnTransferIn').removeClass('btn-warning');
            // $('#btnTransferIn').addClass('btn-default');
            // $('#btnTransferIn').find('i').hide('fast');
            if ($('#btnTransferIn').hasClass('btn-warning')) $('#btnTransferIn').trigger('click');

            // $('#btnKartuIn').removeClass('btn-info');
            // $('#btnKartuIn').addClass('btn-default');
            // $('#btnKartuIn').find('i').hide('fast');
            if ($('#btnKartuIn').hasClass('btn-info')) $('#btnKartuIn').trigger('click');

            // $('#btnCekIn').removeClass('btn-success');
            // $('#btnCekIn').addClass('btn-default');
            // $('#btnCekIn').find('i').hide('fast');
            if ($('#btnCekIn').hasClass('btn-success')) $('#btnCekIn').trigger('click');

            // $('#btnBGIn').removeClass('btn-BG');
            // $('#btnBGIn').addClass('btn-default');
            // $('#btnBGIn').find('i').hide('fast');
            if ($('#btnBGIn').hasClass('btn-BG')) $('#btnBGIn').trigger('click');

            // $('#btnDepositoIn').removeClass('btn-purple');
            // $('#btnDepositoIn').addClass('btn-default');
            // $('#btnDepositoIn').find('i').hide('fast');
            if ($('#btnDepositoIn').hasClass('btn-purple')) $('#btnDepositoIn').trigger('click');
            // $('#uangKeluar').hide();
            // $('#uangKeluar').val('');
            // $('#inputNominalTunaiIn').val('');
            // $('#inputNominalTransferIn').val('');
            // $('#inputNominalCekIn').val('');
            // $('#inputNominalBGIn').val('');
            // $('#inputNominalKartuIn').val('');
            // $('#inputNominalDepositoIn').val('');

            // $('input[name="nominal_tunai_in"]').val(0);
            // $('input[name="no_transfer_in"]').val('');
            // $('input[name="bank_transfer_in"]').val('');
            // $('input[name="nominal_transfer_in"]').val(0);
            // $('input[name="no_kartu_in"]').val('');
            // $('input[name="bank_kartu_in"]').val('');
            // $('input[name="jenis_kartu_in"]').val('');
            // $('input[name="nominal_kartu_in"]').val(0);
            // $('input[name="no_cek_in"]').val('');
            // $('input[name="nominal_cek_in"]').val(0);
            // $('input[name="no_bg_in"]').val('');
            // $('input[name="nominal_bg_in"]').val(0);
            // $('input[name="nominal_titipan_in"]').val(0);
        }

        function total_uang() {
            var total = 0;
            $('.total_uang').each(function(index, el) {
                var tmp = parseFloat($(el).val());
                if (isNaN(tmp)) tmp = 0;
                total_retur += parseFloat(tmp);
            });
            $('#formSimpanContainer').find('input[name="total_uang"]').val(total);
        }

        function nol_jumah() {
            $('input[name="inputJumlahItem"]').each(function(index, el) {
                $(this).parents('td').first().removeClass('has-error');
                $(el).val('');
            });
        }

        /*function cek_lain(button, hitung) {
            var harga_total = $('input[name="harga_total"').val();
            var total_retur = 0;
            $('input[name="subtotal_in[]"').each(function(index, el) {
                var tmp = parseFloat($(el).val());
                if (isNaN(tmp)) tmp = 0;
                total_retur += parseFloat(tmp);
            });

            if (harga_total != total_retur) {
                if (button == 0) {
                    $('.inputSubTotalIn').each(function(index, el) {
                        $(this).parents('td').first().addClass('has-error');
                    });
                } else {
                    return true;
                }
            } else {
                if (button == 0) {
                    $('.inputSubTotalIn').each(function(index, el) {
                        $(this).parents('td').first().removeClass('has-error');
                    });
                } else {
                    return false;
                }
            }
        }*/

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();
            var transaksi = {{ $transaksi_grosir->id }};
            var value = $(this).val();
            // console.log(value);
            var kode = value.split('|')[0];
            var item_parent = value.split('|')[1];
            if (item_parent == '') item_parent = '-';
            var url = "{{ url('transaksi-grosir') }}"+'/'+transaksi+'/retur/'+kode+'/'+item_parent+'/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');
            // console.log(url);

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                if (kode) {
                    // kode_ = 'item-'+kode;
                    selected_items.push(kode);
                    cariItem(kode, item_parent, url, tr);
                    $('#submit').prop('disabled', false);
                }
            }

            // if (!selected_items.includes(kode)) {
            //     if (kode) {
            //         // kode_ = 'item-'+kode;
            //         selected_items.push(kode);
            //         cariItem(kode, url, tr);
            //     }
            // }
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var $tr = $(this);
            var item_kode = $(this).parents('tr').first().data('id');
            var rsp_id = parseFloat($(this).parents('#rsp_id').attr('rsp-id'));
            // console.log(rsp_id);
            var jumlah = 0;
            var td_jumlah = $(this).parents('td').first();
            $(this).parents('td').first().find('input[name="inputJumlahItem"]').each(function(index, el) {
                var temp_jumlah = parseFloat($(el).val());
                if (isNaN(temp_jumlah)) temp_jumlah = 0;

                var temp_konversi = 0;
                var temp_kode_satuan = $(el).parents('#rsp_id').find('.text').text();
                temp_kode_satuan = temp_kode_satuan.split(' ')[0];
                $(el).parents('#rsp_id').find('ul li').each(function(index2, el2) {
                    var kode = $(el2).find('a').attr('kode');
                    if (kode == temp_kode_satuan) {
                        temp_konversi = parseFloat($(el2).find('a').attr('konversi'));
                    }
                });

                temp_jumlah *= temp_konversi;
                jumlah += temp_jumlah;

                if ($(el).parents('#rsp_id').attr('rsp-id') == rsp_id)
                    $('#jumlah_rusak-'+item_kode+'-'+rsp_id).val(temp_jumlah);
            });
            // console.log(jumlah);
            if (isNaN(jumlah)) jumlah = 0;

            //ngitung total masuk jika barang sama, stok e ada ato egak
            var stoktotal = $('#stoktotal-'+item_kode).val();
            // console.log(stoktotal);

            var jumlah_item = document.querySelectorAll('input[id^="jumlah_rusak-'+item_kode+'"]').length;
            // console.log(jumlah_item);
            var jumlahtotal_rusak = 0;
            for (var i = 1; i <= jumlah_item; i++) {
                var x = $('#jumlah_rusak-'+item_kode+'-'+i).val();
                jumlahtotal_rusak += parseInt(x); 
            }

            var status = $('input[name="status"]').val();

            var jumlah_stok = $tr.parents('.input-grup').attr('jumlah');
            if (parseInt(jumlah) > parseInt(jumlah_stok)) {
                $tr.parents('.input-grup').addClass('has-error');
                // console.log(jumlah_stok, jumlah);
            } else {
                // console.log(jumlah_stok, jumlah);
                $tr.parents('.input-grup').removeClass('has-error');
                if (parseInt(stoktotal) < parseInt(jumlahtotal_rusak) && status == 'sama') {
                    td_jumlah.addClass('has-error');
                } else {
                    td_jumlah.removeClass('has-error');
                    var harga = parseFloat($('#harga-'+item_kode).val());
                    var v_subtotal = harga * jumlah;

                    var tr = $('#tabelKeranjang').find('tr[data-id="'+item_kode+'"]');
                    tr.find('#inputSubTotal').val(parseFloat(v_subtotal).toLocaleString());

                    $('#jumlah-'+item_kode).val(jumlah);
                    $('#subtotal-'+item_kode).val(parseFloat(v_subtotal));

                    var harga_total = 0;
                    $('.subtotal').each(function(index, el) {
                        var tmp = parseFloat($(el).val());
                        if (isNaN(tmp)) tmp = 0;
                        harga_total += tmp;
                    });
                    // item_kode

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);

                    updateHargaOnKeyup();
                }
            }
        });

        $(document).on('click', '#pilihSatuan li', function(event) {
            event.preventDefault();

            var $tr = $(this);
            var button = $(this).parents('#pilihSatuan').find('button').find('.text');
            var kode_satuan = $(this).find('a').attr('kode');
            button.text(kode_satuan+' ');

            var item_kode = $(this).parents('tr').first().data('id');
            var rsp_id = parseFloat($(this).parents('#rsp_id').attr('rsp-id'));
            var jumlah = 0;
            var td_jumlah = $(this).parents('td').first();
            $(this).parents('td').first().find('input[name="inputJumlahItem"]').each(function(index, el) {
                var temp_jumlah = parseFloat($(el).val());
                if (isNaN(temp_jumlah)) temp_jumlah = 0;

                var temp_konversi = 0;
                var temp_kode_satuan = $(el).parents('#rsp_id').find('.text').text();
                temp_kode_satuan = temp_kode_satuan.split(' ')[0];
                $(el).parents('#rsp_id').find('ul li').each(function(index2, el2) {
                    var kode = $(el2).find('a').attr('kode');
                    if (kode == temp_kode_satuan) {
                        temp_konversi = parseFloat($(el2).find('a').attr('konversi'));
                    }
                });

                temp_jumlah *= temp_konversi;
                jumlah += temp_jumlah;

                if ($(el).parents('#rsp_id').attr('rsp-id') == rsp_id)
                    $('#jumlah_rusak-'+item_kode+'-'+rsp_id).val(temp_jumlah);
            });
            // console.log(jumlah);
            if (isNaN(jumlah)) jumlah = 0;

            //ngitung total masuk jika barang sama, stok e ada ato egak
            var stoktotal = $('#stoktotal-'+item_kode).val();
            // console.log(stoktotal);

            var jumlah_item = document.querySelectorAll('input[id^="jumlah_rusak-'+item_kode+'"]').length;
            // console.log(jumlah_item);
            var jumlahtotal_rusak = 0;
            for (var i = 1; i <= jumlah_item; i++) {
                var x = $('#jumlah_rusak-'+item_kode+'-'+i).val();
                jumlahtotal_rusak += parseInt(x); 
            }

            var status = $('input[name="status"]').val();

            var jumlah_stok = $tr.parents('.input-grup').attr('jumlah');
            if (parseInt(jumlah) > parseInt(jumlah_stok)) {
                $tr.parents('.input-grup').addClass('has-error');
            } else {
                $tr.parents('.input-grup').removeClass('has-error');
                if (parseInt(stoktotal) < parseInt(jumlahtotal_rusak) && status == 'sama') {
                    td_jumlah.addClass('has-error');
                } else {
                    td_jumlah.removeClass('has-error');
                    var harga = parseFloat($('#harga-'+item_kode).val());
                    var v_subtotal = harga * jumlah;

                    var tr = $('#tabelKeranjang').find('tr[data-id="'+item_kode+'"]');
                    tr.find('#inputSubTotal').val(parseFloat(v_subtotal).toLocaleString());

                    $('#jumlah-'+item_kode).val(jumlah);
                    $('#subtotal-'+item_kode).val(parseFloat(v_subtotal));

                    var harga_total = 0;
                    $('.subtotal').each(function(index, el) {
                        var tmp = parseFloat($(el).val());
                        if (isNaN(tmp)) tmp = 0;
                        harga_total += tmp;
                    });

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);

                    updateHargaOnKeyup();
                }
            }
        });

        $(document).on('change', '#checkRusak', function(event) {
            event.preventDefault();

            var item_kode = $(this).parents('tr').first().data('id');
            var rsp_id = parseFloat($(this).parents('#rsp_id').attr('rsp-id'));

            var checked = $(this).prop('checked');
            var $inputRusak = $(this).parents('#rsp_id').find('#inputRusak');
            if (checked) {
                $inputRusak.css('background-color', '#fff');
                $('#is_rusak-'+item_kode+'-'+rsp_id).val(1);
            } else {
                $inputRusak.css('background-color', '#eee');
                $('#is_rusak-'+item_kode+'-'+rsp_id).val(0);
            }
        });

        $(document).on('keyup', '#inputKeterangan', function(event) {
            event.preventDefault();

            var id = $(this).parents('tr').first().data('id');
            var $jumlah = $(this).parent().prev().find('input');

            var jumlah = parseInt($jumlah.val().replace(/\D/g, ''), 10);
            var keterangan = $(this).val();
            
            $('#form-simpan').find('input[id="item-' + id + '"]').remove();
            $('#form-simpan').find('input[id="jumlah-' + id + '"]').remove();
            $('#form-simpan').find('input[id="keterangan-' + id + '"]').remove();
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_id[]" id="item-' + id + '" value="' + id + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-' + id + '" value="' + jumlah + '" />');
            $('#form-simpan').find('#append-section').append('<input type="hidden" name="keterangan[]" id="keterangan-' + id + '" value="' + keterangan + '" />');
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var item_kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+item_kode+'"]').remove();

            var subtotal = parseFloat($('#subtotal-'+item_kode).val());
            var harga_total = parseFloat($('input[name="harga_total"]').val());

            if (isNaN(subtotal)) subtotal = 0;
            if (isNaN(harga_total)) harga_total = 0;

            harga_total -= subtotal;
            $('input[name="harga_total"]').val(harga_total);
            $('#inputHargaTotal').val(harga_total.toLocaleString());

            $('#form-simpan').find('#append-section').find('input[id*=In-'+item_kode+']').remove();
            $('select[name="item_retur"]').val('').trigger('change');

            removeSelectedItem(item_kode);
            updateHargaOnKeyup();

            // var kode = $(this).parents('tr').data('id');
            // var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            // var subtotal = $('#subtotal-'+kode).val();
            // var harga_total = $('input[name="harga_total"]').val();
            // // parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
            // // console.log(kode);

            // harga_total -= subtotal;
            // // kode_ = 'item-'+kode;
            // deleteMe(selected_items, kode);

            // $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 3}));

            // $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(3));

            // tr.remove();
            // $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });

        $(document).on('click', '#btnUang', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('uang');
            $('#submit').prop('disabled', true);
            $('#uangKeluar').show();
            $('#metodePembayaranButtonGroup').show();
            

            if ($('#btnSama').hasClass('btn-success')) {
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
            } else if ($('#btnLain').hasClass('btn-success')) {
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
            }

            $('#tabelKeranjang').find('#inputJumlahItem').trigger('keyup');
            updateHargaOnKeyup();
            // nol_jumah();
        });

        $(document).on('click', '#btnSama', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('sama');
            $('#submit').prop('disabled', false);
            $('#uangKeluar').hide();
            $('#uangKeluar').val('');

            if ($('#btnUang').hasClass('btn-success')) {
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
                $('#metodePembayaranButtonGroup').hide();
                uangClose();
            } else if ($('#btnLain').hasClass('btn-success')) {
                $('#btnLain').removeClass('btn-success');
                $('#btnLain').addClass('btn-default');
                $('#btnLain').find('i').hide('fast');
                $('#keranjangLain').hide();
                lainClose();
            }
            // nol_jumah();
            updateHargaOnKeyup();
            $('#tabelKeranjang').find('#inputJumlahItem').trigger('keyup');
        });

        $(document).on('click', '#btnLain', function(event) {
            event.preventDefault();

            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            // $(this).find('i').show('fast');

            $('input[name="status"]').val('lain');
            $('#submit').prop('disabled', true);

            lainClose();
            $('#keranjangLain').show();
            $('#uangKeluar').hide();
            $('#uangKeluar').val('');

            if ($('#btnSama').hasClass('btn-success')) {
                $('#btnSama').removeClass('btn-success');
                $('#btnSama').addClass('btn-default');
                $('#btnSama').find('i').hide('fast');
            } else if ($('#btnUang').hasClass('btn-success')) {
                $('#btnUang').removeClass('btn-success');
                $('#btnUang').addClass('btn-default');
                $('#btnUang').find('i').hide('fast');
                $('#metodePembayaranButtonGroup').hide();
                uangClose();
            }

            // nol_jumah();
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                // $(this).find('i').show('fast');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTunaiContainer').hide('fast', function() {
                    $('input[name="nominal_tunai"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            $('#tabelKeranjang').find('#inputJumlahItem').trigger('keyup');
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                // $(this).find('i').show('fast');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTransferBankContainer').hide('fast', function() {
                    $('input[name="no_transfer"]').val('');
                    $('input[name="bank_transfer"]').val('');
                    $('input[name="nominal_transfer"]').val('');

                    $(this).find('select[name="bank_transfer"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnKartu', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                // $(this).find('i').show('fast');
                $('#inputKartuContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputKartuContainer').hide('fast', function() {
                    $('input[name="no_kartu"]').val('');
                    $('input[name="bank_kartu"]').val('');
                    $('input[name="jenis_kartu"]').val('');
                    $('input[name="nominal_kartu"]').val('');

                    $(this).find('select[name="bank_kartu"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                // $(this).find('i').show('fast');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputCekContainer').hide('fast', function() {
                    $('input[name="no_cek"]').val('');
                    $('input[name="bank_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');

                    $(this).find('select[name="cek_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    $(this).find('p').hide();
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                // $(this).find('i').show('fast');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputBGContainer').hide('fast', function() {
                    $('input[name="no_bg"]').val('');
                    $('input[name="bank_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');

                    $(this).find('select[name="bg_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    $(this).find('p').hide();
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDeposito', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                // $(this).find('i').show('fast');
                $('#inputDepositoContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputDepositoContainer').hide('fast', function() {
                    $('input[name="nominal_titipan"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnPiutang', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                // $(this).find('i').show('fast');
                $('#inputPiutangContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputPiutangContainer').hide('fast', function() {
                    $('input[name="piutang_id"]').val('');
                    $('input[name="nominal_piutang"]').val('');

                    $(this).find('select[name="piutang_id"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        // INPUT TUNAI
        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            var laci = cash_drawer + nominal_tunai;
           
            $('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        // INPUT TRANSFER
        $(document).on('change', 'select[name="bank_transfer"]', function(event) {
            event.preventDefault();

            var bank_transfer = $(this).val();
            $('input[name="bank_transfer"]').val(bank_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            $('input[name="nominal_transfer"]').val(nominal_transfer);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT KARTU
        $(document).on('change', 'select[name="bank_kartu"]', function(event) {
            event.preventDefault();

            var bank_kartu = $(this).val();
            $('input[name="bank_kartu"]').val(bank_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu"]', function(event) {
            event.preventDefault();

            var jenis_kartu = $(this).val();
            $('input[name="jenis_kartu"]').val(jenis_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorKartu', function(event) {
            event.preventDefault();

            var nomor_kartu = $(this).val();
            $('input[name="no_kartu"]').val(nomor_kartu);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartu', function(event) {
            event.preventDefault();

            var nominal_kartu = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            $('input[name="nominal_kartu"]').val(nominal_kartu);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT CEK
        $(document).on('change', 'select[name="cek_id"]', function(event) {
            event.preventDefault();

            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_cek = parseFloat($('input[name="nominal_cek"]').val());

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;

            var utang = harga_total - (jumlah_bayar - nominal_cek);

            var cek_id = $(this).val();
            if (cek_id != 'cek_baru') {
                cek_id = parseInt(cek_id);
            }

            if (Number.isInteger(cek_id)) {
                var cek_text = $(this).select2('data')[0].text;
                var nominal_text = cek_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = cek_text.split(' [')[0];
                $('#cek_baru').hide('fast');
                $('#inputNominalCek').prop('disabled', true);
                $('input[name="bank_cek"]').val('');
                $('select[name="bank_cek"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputCekContainer').find('p').hide();
                    $('#inputNominalCek').val(nominal_text);
                    $('input[name="no_cek"]').val(nomor);
                    $('input[name="cek_id"]').val(cek_id);
                    $('input[name="nominal_cek"]').val(nominal);
                } else {
                    // Error
                    $('#inputCekContainer').find('p').show();
                    $('#inputNominalCek').val('');
                    $('input[name="no_cek"]').val('');
                    $('input[name="nominal_cek"]').val('');
                }

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');
            } else if (cek_id == 'cek_baru') {
                $('#cek_baru').show('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', false);

                $('input[name="no_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            } else {
                $('#cek_baru').hide('fast');
                $('#inputCekContainer').find('p').hide();

                $('#inputNominalCek').val('');
                $('#inputNominalCek').prop('disabled', true);

                $('#inputNomorCek').val('');
                $('select[name="bank_cek"]').val('').trigger('change');

                $('input[name="no_cek"]').val('');
                $('input[name="bank_cek"]').val('');
                $('input[name="nominal_cek"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorCek', function(event) {
            event.preventDefault();

            var nomor_cek = $(this).val();
            $('input[name="no_cek"]').val(nomor_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;
            $('input[name="nominal_cek"]').val(nominal_cek);
            // total_uang();
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_cek"]', function(event) {
            event.preventDefault();

            var bank_cek = $(this).val();
            $('input[name="bank_cek"]').val(bank_cek);
            updateHargaOnKeyup();
        });

        // INPUT BG
        $(document).on('change', 'select[name="bg_id"]', function(event) {
            event.preventDefault();

            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            var nominal_bg = parseFloat($('input[name="nominal_bg"]').val());

            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var utang = harga_total - (jumlah_bayar - nominal_bg);

            var bg_id = $(this).val();
            if (bg_id != 'bg_baru') {
                bg_id = parseInt(bg_id);
            }

            if (Number.isInteger(bg_id)) {
                var bg_text = $(this).select2('data')[0].text;
                var nominal_text = bg_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = bg_text.split(' [')[0];
                $('#bg_baru').hide('fast');
                $('#inputNominalBG').prop('disabled', true);
                $('input[name="bank_bg"]').val('');
                $('select[name="bank_bg"]').val('').change();
                if (nominal <= utang) {
                    // Success
                    $('#inputBGContainer').find('p').hide();
                    $('#inputNominalBG').val(nominal_text);
                    $('input[name="no_bg"]').val(nomor);
                    $('input[name="bg_id"]').val(bg_id);
                    $('input[name="nominal_bg"]').val(nominal);
                } else {
                    // Error
                    $('#inputBGContainer').find('p').show();
                    $('#inputNominalBG').val('');
                    $('input[name="no_bg"]').val('');
                    $('input[name="nominal_bg"]').val('');
                }

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');
            } else if (bg_id == 'bg_baru') {
                $('#bg_baru').show('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', false);

                $('input[name="no_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            } else {
                $('#bg_baru').hide('fast');
                $('#inputBGContainer').find('p').hide();

                $('#inputNominalBG').val('');
                $('#inputNominalBG').prop('disabled', true);

                $('#inputNomorBG').val('');
                $('select[name="bank_bg"]').val('').trigger('change');

                $('input[name="no_bg"]').val('');
                $('input[name="bank_bg"]').val('');
                $('input[name="nominal_bg"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNomorBG', function(event) {
            event.preventDefault();

            var nomor_bg = $(this).val();
            $('input[name="no_bg"]').val(nomor_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;
            $('input[name="nominal_bg"]').val(nominal_bg);
            // total_uang();
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_bg"]', function(event) {
            event.preventDefault();

            var bank_bg = $(this).val();
            $('input[name="bank_bg"]').val(bank_bg);
            updateHargaOnKeyup();
        });

        // INPUT TITIPAN
        $(document).on('keyup', '#inputNominalDeposito', function(event) {
            event.preventDefault();

            var nominal = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal)) nominal = 0;
            $('input[name="nominal_titipan"]').val(nominal);
            // total_uang();
            updateHargaOnKeyup();
        });

        // INPUT BAYAR PIUTANG
        $(document).on('change', 'select[name="piutang_id"]', function(event) {
            event.preventDefault();

            // var harga_total = parseFloat($('input[name="harga_total"]').val());
            // var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            // var kurang = harga_total - jumlah_bayar;

            var piutang_id = parseInt($(this).val());
            // console.log(piutang_id);
            if (Number.isInteger(piutang_id)) {
                var piutang_text = $(this).select2('data')[0].text;
                var nominal_text = piutang_text.split('Rp')[1].split(']')[0];
                var nominal = parseFloat(nominal_text.replace(/\D/g, ''), 10);
                var nomor = piutang_text.split(' [')[0];
                $('#inputNominalPiutang').prop('readonly', false);
                $('input[name="piutang_id"]').val(piutang_id);
                $('input[name="piutang_max"]').val(nominal);
                // console.log(nominal);
                // if (nominal <= kurang) {
                //     // Success
                //     $('#inputNominalPiutang').val(nominal.toLocaleString());
                //     $('input[name="nominal_piutang"]').val(nominal);
                // } else {
                //     // Error
                //     $('#inputNominalPiutang').val('');
                //     $('input[name="nominal_piutang"]').val('');
                // }
            } else {
                $('#inputNominalPiutang').val('');
                $('#inputNominalPiutang').prop('readonly', true);

                $('input[name="piutang_id"]').val('');
                $('input[name="nominal_piutang"]').val('');
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalPiutang', function(event) {
            event.preventDefault();

            var nominal_piutang = parseFloat($(this).val().replace(/\D/g, ''), 10);
            var piutang_max = parseFloat($('input[name="piutang_max"]').val());

            if (isNaN(nominal_piutang)) nominal_piutang = 0;
            if (isNaN(piutang_max)) piutang_max = 0;

            if (nominal_piutang <= piutang_max) {
                // boleh
                $(this).parents('.input-group').removeClass('has-error');
                $('input[name="nominal_piutang"]').val(nominal_piutang);
                // total_uang();
                // $('#submit').prop('disabled', isBtnSimpanDisabled());
                updateHargaOnKeyup();
            } else {
                // tidak boleh
                $(this).parents('.input-group').addClass('has-error');
            }

            // var harga_total = parseFloat($('input[name="harga_total"]').val());
            // var jumlah_bayar = parseFloat($('input[name="jumlah_bayar"]').val());
            // var kurang = harga_total - jumlah_bayar;
        });

        $(document).on('change', 'select[name="item_retur"]', function(event) {
            event.preventDefault();

            var item_kode = $(this).val();
            var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/item/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+item_kode+'"]').data('id');

            if (item_kode == '') {
                // $('#tabelKeranjangRetur tbody').empty();
            } else if (!selected_retur.includes(item_kode)) {
                selected_retur.push(item_kode);
                cariItemRetur(item_kode, url, tr);
            }
        });

        $(document).on('keyup', '#inputJumlahItemIn', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var td = $(this).parents('td').first();
            var item_kode = $(this).parents('tr').first().data('id');
            var stoktotal = parseFloat($('#stoktotalIn-'+item_kode).val());
            var satuan_id = $('#satuanIn-'+item_kode).val();
            var konversi = parseFloat($('#konversiIn-'+item_kode).val());
            var is_grosir = true;
            if (transaksi_penjualan.status == 'eceran') is_grosir = false;

            var jumlah = parseFloat($(this).val());
            if (jumlah === '') jumlah = 0;
            var jumlahtotal = jumlah * konversi;

            if (jumlahtotal > stoktotal) {
                // tidak boleh
                // $(this).parents('td').first().addClass('has-error');
                tr.find('#pilihSatuanInTD').addClass('has-error');
                tr.find('#inputJumlahItemInTD').addClass('has-error');
                $('#jumlahIn-'+item_kode).val(jumlah);
                $('#satuanIn-'+item_kode).val(satuan_id);
                $('#konversiIn-'+item_kode).val(konversi);
            } else {
                // boleh
                // $(this).parents('td').first().removeClass('has-error');
                tr.find('#pilihSatuanInTD').removeClass('has-error');
                tr.find('#inputJumlahItemInTD').removeClass('has-error');

                var url = "{{ url('transaksi-grosir') }}"+'/'+transaksi_penjualan.id+'/retur/'+item_kode+'/'+jumlah+'/'+satuan_id+'/json';
                var tr = $('#tabelKeranjangRetur').find('tr[data-id="'+item_kode+'"]');
                $('#jumlahIn-'+item_kode).val(jumlah);
                $('#satuanIn-'+item_kode).val(satuan_id);
                $('#konversiIn-'+item_kode).val(konversi);
                $.get(url, function(data) {
                    // console.log(url, data);
                    if (data.harga === null) {
                        td.addClass('has-error');
                    } else {
                        td.removeClass('has-error');

                        var harga = data.harga;
                        var item = data.item;
                        var v_harga = parseFloat(harga.eceran);
                        if (is_grosir) v_harga = parseFloat(harga.grosir);

                        // v_harga = v_harga - (v_harga * item.diskon / 100);
                        // v_harga = Math.ceil(v_harga / 100) * 100;

                        var v_subtotal = v_harga * jumlah;
                        // console.log(v_harga, v_subtotal);

                        $('#hargaIn-'+item_kode).val(v_harga);
                        $('#subtotalIn-'+item_kode).val(v_subtotal);

                         tr.find('#inputHargaPerSatuanIn').val(v_harga.toLocaleString());
                         tr.find('#inputSubTotalIn').val(v_subtotal.toLocaleString());

                        updateHargaOnKeyup();
                    }
                });
            }
        });

        $(document).on('change', '#pilihSatuanIn', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var td = $(this).parents('td').first();
            var item_kode = $(this).parents('tr').first().data('id');
            var stoktotal = parseFloat($('#stoktotalIn-'+item_kode).val());
            var pilih_satuan = $(this).val().split('-');
            var satuan_id = parseFloat(pilih_satuan[0]);
            var konversi = parseFloat(pilih_satuan[1]);
            var is_grosir = true;
            if (transaksi_penjualan.status == 'eceran') is_grosir = false;

            var jumlah = parseFloat($('#jumlahIn-'+item_kode).val());
            if (jumlah === '') jumlah = 0;
            var jumlahtotal = jumlah * konversi;

            if (jumlahtotal > stoktotal) {
                // tidak boleh
                // $(this).parents('td').first().addClass('has-error');
                tr.find('#pilihSatuanInTD').addClass('has-error');
                tr.find('#inputJumlahItemInTD').addClass('has-error');
                $('#satuanIn-'+item_kode).val(satuan_id);
                $('#konversiIn-'+item_kode).val(konversi);
                $('#jumlahIn-'+item_kode).val(jumlah);
            } else {
                // boleh
                // $(this).parents('td').first().removeClass('has-error');
                tr.find('#pilihSatuanInTD').removeClass('has-error');
                tr.find('#inputJumlahItemInTD').removeClass('has-error');

                var url = "{{ url('transaksi-grosir') }}"+'/'+transaksi_penjualan.id+'/retur/'+item_kode+'/'+jumlah+'/'+satuan_id+'/json';
                var tr = $('#tabelKeranjangRetur').find('tr[data-id="'+item_kode+'"]');
                $('#satuanIn-'+item_kode).val(satuan_id);
                $('#konversiIn-'+item_kode).val(konversi);
                $('#jumlahIn-'+item_kode).val(jumlah);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        // tr.find('#inputHargaPerSatuan').val(0);
                        // tr.find('#inputSubTotal').val(0);
                        // tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                    } else {
                        td.removeClass('has-error');

                        var harga = data.harga;
                        var item = data.item;
                        var v_harga = parseFloat(harga.eceran);
                        if (is_grosir) v_harga = parseFloat(harga.grosir);

                        // v_harga = v_harga - (v_harga * item.diskon / 100);
                        // v_harga = Math.ceil(v_harga / 100) * 100;
                        
                        var v_subtotal = v_harga * jumlah;
                        // console.log(v_harga, v_subtotal);

                        $('#hargaIn-'+item_kode).val(v_harga);
                        $('#subtotalIn-'+item_kode).val(v_subtotal);

                         tr.find('#inputHargaPerSatuanIn').val(v_harga.toLocaleString());
                         tr.find('#inputSubTotalIn').val(v_subtotal.toLocaleString());

                        updateHargaOnKeyup();
                    }
                });
            }
        });

        // BUTTON IN
        $(document).on('click', '#btnTunaiIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                // $(this).find('i').show('fast');
                $('#inputTunaiContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTunaiContainerIn').hide('fast', function() {
                    $('input[name="nominal_tunai_in"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTransferIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                // $(this).find('i').show('fast');
                $('#inputTransferBankContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputTransferBankContainerIn').hide('fast', function() {
                    $('input[name="no_transfer_in"]').val('');
                    $('input[name="bank_transfer_in"]').val('');
                    $('input[name="nominal_transfer_in"]').val('');

                    $(this).find('select[name="bank_transfer_in"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnKartuIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                // $(this).find('i').show('fast');
                $('#inputKartuContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputKartuContainerIn').hide('fast', function() {
                    $('input[name="no_kartu_in"]').val('');
                    $('input[name="bank_kartu_in"]').val('');
                    $('input[name="jenis_kartu_in"]').val('');
                    $('input[name="nominal_kartu_in"]').val('');

                    $(this).find('select[name="bank_kartu_in"]').val('').trigger('change');
                    $(this).find('select[name="jenis_kartu_in"]').val('').trigger('change');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnCekIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                // $(this).find('i').show('fast');
                $('#inputCekContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputCekContainerIn').hide('fast', function() {
                    $('input[name="no_cek_in"]').val('');
                    $('input[name="nominal_cek_in"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnBGIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-BG');
                // $(this).find('i').show('fast');
                $('#inputBGContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-BG')) {
                $(this).removeClass('btn-BG');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputBGContainerIn').hide('fast', function() {
                    $('input[name="no_bg_in"]').val('');
                    $('input[name="nominal_bg_in"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDepositoIn', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-purple');
                // $(this).find('i').show('fast');
                $('#inputDepositoContainerIn').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                    updateHargaOnKeyup();
                });
            } else if ($(this).hasClass('btn-purple')) {
                $(this).removeClass('btn-purple');
                $(this).addClass('btn-default');
                // $(this).find('i').hide('fast');
                $('#inputDepositoContainerIn').hide('fast', function() {
                    $('input[name="nominal_titipan_in"]').val('');

                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
            // updateHargaOnKeyup();
        });

        // INPUT IN
        $(document).on('keyup', '#inputNominalTunaiIn', function(event) {
            event.preventDefault();

            var nominal_tunai_in = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai_in)) nominal_tunai_in = 0;
            // $(this).val(nominal_tunai_in.toLocaleString());

            // if(user_level == 3){
            var laci = cash_drawer + nominal_tunai_in;
            if(laci - setoran_buka > money_limit && user_level == 3){
                $('#inputTunaiContainerIn').addClass('has-error');
                $('#eror_tunai').removeClass('sembunyi');
            }else{
                $('#inputTunaiContainerIn').removeClass('has-error');
                $('#eror_tunai').addClass('sembunyi');
                $('input[name="nominal_tunai_in"]').val(nominal_tunai_in);
                updateHargaOnKeyup();
            }
            // }
        });

        $(document).on('change', 'select[name="bank_transfer_in"]', function(event) {
            event.preventDefault();

            var bank_transfer_in = $(this).val();
            $('input[name="bank_transfer_in"]').val(bank_transfer_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransferIn', function(event) {
            event.preventDefault();

            var no_transfer_in = $(this).val();
            $('input[name="no_transfer_in"]').val(no_transfer_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransferIn', function(event) {
            event.preventDefault();

            var nominal_transfer_in = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer_in)) nominal_transfer_in = 0;
            $('input[name="nominal_transfer_in"]').val(nominal_transfer_in);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_kartu_in"]', function(event) {
            event.preventDefault();

            var bank_kartu_in = $(this).val();
            $('input[name="bank_kartu_in"]').val(bank_kartu_in);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="jenis_kartu_in"]', function(event) {
            event.preventDefault();

            var jenis_kartu_in = $(this).val();
            $('input[name="jenis_kartu_in"]').val(jenis_kartu_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoKartuIn', function(event) {
            event.preventDefault();

            var no_kartu_in = $(this).val();
            $('input[name="no_kartu_in"]').val(no_kartu_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKartuIn', function(event) {
            event.preventDefault();

            var nominal_kartu_in = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kartu_in)) nominal_kartu_in = 0;
            $('input[name="nominal_kartu_in"]').val(nominal_kartu_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoCekIn', function(event) {
            event.preventDefault();

            var no_cek_in = $(this).val();
            $('input[name="no_cek_in"]').val(no_cek_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCekIn', function(event) {
            event.preventDefault();

            var nominal_cek_in = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek_in)) nominal_cek_in = 0;
            $('input[name="nominal_cek_in"]').val(nominal_cek_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoBGIn', function(event) {
            event.preventDefault();

            var no_bg_in = $(this).val();
            $('input[name="no_bg_in"]').val(no_bg_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBGIn', function(event) {
            event.preventDefault();

            var nominal_bg_in = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg_in)) nominal_bg_in = 0;
            $('input[name="nominal_bg_in"]').val(nominal_bg_in);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalDepositoIn', function(event) {
            event.preventDefault();

            var nominal_titipan_in = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_titipan_max_in = parseFloat(transaksi_penjualan.pelanggan.titipan)

            if (isNaN(nominal_titipan_in)) nominal_titipan_in = 0;
            if (isNaN(nominal_titipan_max_in)) nominal_titipan_max_in = 0;

            if (nominal_titipan_in <= nominal_titipan_max_in) {
                $(this).parents('.input-group').first().removeClass('has-error');
                $('input[name="nominal_titipan_in"]').val(nominal_titipan_in);
                // updateHargaOnKeyup();
            } else {
                $(this).parents('.input-group').first().addClass('has-error');
                // updateHargaOnKeyup();
            }
            updateHargaOnKeyup();
        });

        $(document).on('click', '#removeIn', function(event) {
            event.preventDefault();

            var item_kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjangRetur').find('tr[data-id="'+item_kode+'"]').remove();

            $('#form-simpan').find('#append-section-in').find('input[id*=In-'+item_kode+']').remove();
            $('select[name="item_retur"]').val('').trigger('change');

            removeSelectedRetur(item_kode);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#submit', function(event) {
            event.preventDefault();
            
            swal({
                title: 'Anda yakin?',
                text: 'Transaksi Retur akan dilakukan!',
                type: 'question',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-money"></i> Ya, Retur',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                $('#form-simpan').submit();
                // console.log('po_penjualan');
            });
        });

    </script>
@endsection
