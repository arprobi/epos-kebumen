@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Transaksi Penjualan</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        #btnKembali {
            margin: 0;
        }
        td > .input-group {
            margin-bottom: 0;
        }
        #tabelInfo span {
            font-size: 0.85em;
            margin-right: 5px;
            margin-top: 0;
            margin-bottom: 0;
        }
        #tabelKeranjang {
            width: 100%;
        }
        #tabelKeranjang td {
            border: none;
        }
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }
        #metodePembayaranButtonGroup {
            width: 100%;
        }
        .has-error .btn {
            border-color: #a94442;
        }
        input[name="checkPPN"] {
            visibility: hidden;
        }
        /* #infoTransaksi {
            position: fixed;
            bottom: 15px;
            left: 15px;
            z-index: 1000;
            background: rgba(237, 237, 237, 0.3);
            color: #fefefe;
            font-size: 1.2em;
            padding: 10px;
        } */
    </style>
@endsection

@section('content')
    <!-- <div id="infoTransaksi">
        TOTAL 0 ITEM
    </div> -->
    <div class="col-md-5 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <div class="row">
                    <div class="col-md-9">
                        <h2 id="formSimpanTitle">Tambah Transaksi Penjualan</h2>
                        <span id="kodeTransaksiTitle"></span>
                    </div>
                    <!-- <div class="col-md-3 pull-right">
                        <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button">
                            <i class="fa fa-long-arrow-left"></i> Kembali
                        </a>
                    </div> -->
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-10 col-xs-10">
                        <label class="control-label">Pilih Pelanggan</label>
                        <select name="pelanggan_id" class="select2_single form-control">
                            <option value="">Pilih Pelanggan</option>
                            @foreach ($pelanggans as $pelanggan)
                            <option value="{{ $pelanggan->id }}">{{ $pelanggan->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2 col-xs-2" style="text-align: right;">
                        <label class="control-label" style="opacity: 0;">Aksi</label>
                        <button id="resetPelanggan" class="btn btn-default" style="margin-right: 0;"><i class="fa fa-trash"></i></button>
                    </div>
                    <div class="form-group col-sm-3 col-xs-3" style="display: none;">
                        <label class="control-label">Kode</label>
                        <input type="text" name="inputKodePelanggan" id="inputKodePelanggan" class="form-control">
                    </div>
                    <div class="form-group col-sm-7 col-xs-7" style="display: none;">
                        <label class="control-label">Nama</label>
                        <input type="text" name="inputNamaPelanggan" id="inputNamaPelanggan" class="form-control">
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelPelanggan" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Nama</th>
                                    <th style="text-align: left;">Alamat</th>
                                    <th style="text-align: left;">Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 hidden-xs">
        <div class="x_panel">
            <div class="x_title">
                <h2>Informasi Item</h2>
                <a href="{{ url('po-penjualan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label class="control-label">Nama Item</label>
                        <select name="item_id" class="select2_single form-control">
                            <option value="">Pilih Item</option>
                            @foreach($items as $item)
                            <option value="{{$item->kode}}" alt="{{$item->kode}}">[{{$item->kode}}] {{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12" style="margin-bottom: 0;">
                        <table class="table" id="tabelInfo" style="margin-bottom: 0;">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">Item</th>
                                    <th style="text-align: left;">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Keranjang Belanja</h2>
                {{-- <h2 id="tipe_penjualan" class="pull-right"></h2> --}}
                <div id="tipe_penjualan" class="label pull-right" style="font-size: 14px; font-weight: 400;" data-toggle="tooltip" data-placement="top" title="Status Penjualan">Eceran</div>
                <div id="level_pelanggan" class="label pull-right" style="font-size: 14px; font-weight: 400; margin-right: 5px;" data-toggle="tooltip" data-placement="top" title="Status Pelanggan">Eceran</div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <table class="table" id="tabelKeranjang">
                            <thead>
                                <tr>
                                    <th style="width: 20px;"></th>
                                    <th style="text-align: left;">Item</th>
                                    {{-- <th style="text-align: left; width: 200px;">Jumlah</th> --}}
                                    <th style="text-align: center; width: 300px;">Jumlah</th>
                                    {{-- <th style="text-align: left; width: 100px;">Satuan</th> --}}
                                    <th style="text-align: center; width: 140px;">Harga</th>
                                    <th style="text-align: center; width: 150px;">Total</th>
                                    <th style="text-align: center; width: 150px;">Nego Total (Rp)</th>
                                    <th style="text-align: left; width: 100px;">Bonus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Bonus Penjualan</label>
                                <table class="table table-bordered table-hover table-stripped" id="tabelBonus">
                                    <thead>
                                        <tr>
                                            <th style="width: 20px;">No</th>
                                            <th>Bonus</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            {{-- bagian metode pembayaran --}}
                            {{-- <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Metode Pembayaran</label>
                                <div id="metodePembayaranButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTunai" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Tunai</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnTransfer" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Transfer</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnKartu" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Kartu</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnCek" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Cek</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnBG" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> BG</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDeposito" class="btn btn-default"><i class="fa fa-check" style="display: none;"></i> Deposito</button>
                                    </div>
                                </div>
                            </div>
                            <div id="inputTunaiContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Tunai</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalTunai" id="inputNominalTunai" class="form-control angka">
                                </div>
                            </div>
                            <div id="inputTransferBankContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_id">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoTransfer" id="inputNoTransfer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Transfer</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalTransfer" id="inputNominalTransfer" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputKartuContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Bank</label>
                                        <select class="form-control select2_single" name="bank_kartu">
                                            <option value="">Pilih Bank</option>
                                            @foreach ($banks as $bank)
                                            <option value="{{ $bank->id }}">{{ $bank->nama_bank }} [{{ $bank->no_rekening }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pilih Jenis Kartu</label>
                                        <select class="form-control select2_single" name="jenis_kartu">
                                            <option value="">Pilih Kartu</option>
                                            <option value="debet">Kartu Debit</option>
                                            <option value="kredit">Kartu Kredit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nomor Transaksi</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoKartu" id="inputNoKartu" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Nominal Kartu</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalKartu" id="inputNominalKartu" class="form-control angka">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputCekContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoCek" id="inputNoCek" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal Cek</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalCek" id="inputNominalCek" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputBGContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nomor BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" name="inputNoBG" id="inputNoBG" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="control-label">Nominal BG</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">Rp</div>
                                            <input type="text" name="inputNominalBG" id="inputNominalBG" class="form-control angka" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="inputDepositoContainer" class="form-group col-sm-12 col-xs-12">
                                <div class="line"></div>
                                <label class="control-label">Nominal Deposito</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputNominalDeposito" id="inputNominalDeposito" class="form-control angka">
                                </div>
                            </div> --}}
                            {{-- selesai bagian metode pembayaran --}}
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px; margin-bottom: 0;">
                                <div class="line"></div>
                                <label class="control-label">Jatuh Tempo</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="inputJatuhTempo" id="inputJatuhTempo" class="form-control" disabled="">
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                {{-- <label class="control-label">Metode Pembayaran</label> --}}
                                <div id="diambilAtauDikirmButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDiambil" class="btn btn-default"><i class="fa"></i> Diambil</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnDikirim" class="btn btn-default"><i class="fa"></i> Dikirim</button>
                                    </div>
                                </div>
                            </div>
                            @if (in_array(Auth::user()->level_id, [1, 2, 3, 4]))
                            <div id="inputDikirmContainer" class="form-group col-sm-12 col-xs-12">
                                {{-- <div class="line"></div> --}}
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6" style="margin-bottom: 10px;">
                                        <label class="control-label">Pilih Karyawan</label>
                                        <select class="form-control select2_single" name="user_id">
                                            <option value="">Pilih Karyawan</option>
                                            @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                            @endforeach
                                            <option value="-">Pengirim Lain</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <label class="control-label">Pengirim Lain</label>
                                        <div class="input-group" style="margin: 0;">
                                            <div class="input-group-addon">#</div>
                                            <input type="text" id="inputPengirimLain" class="form-control" disabled="" style="height: 38px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if (in_array(Auth::user()->level_id, [1, 2, 3, 4]))
                            <div class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                {{-- <label class="control-label">Metode Pembayaran</label> --}}
                                <div id="alamatBiasaAtauLainButtonGroup" class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnAlamatBiasa" class="btn btn-default"><i class="fa"></i> Alamat Biasa</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button type="button" id="btnAlamatLain" class="btn btn-default"><i class="fa"></i> Alamat Lain</button>
                                    </div>
                                </div>
                            </div>
                            <div id="alamatBiasaAtauLainContainer" class="form-group col-sm-12 col-xs-12">
                                {{-- <div class="line"></div> --}}
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <label class="control-label">Input Alamat</label>
                                        <textarea id="inputAlamat" class="form-control" name="inputAlamat" style="min-width: 100%; max-width: 100%;"></textarea>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div id="warningHutangContainer" class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                <p class="text-danger p1">Batasan hutang Rp0,-</p>
                                <p class="text-danger p2">Batasan hutang yang telah dipakai Rp0,-</p>
                                <p class="text-danger p3">Sisa batasan hutang Rp0,-</p>
                                <p class="text-danger p4">Minimal jumlah bayar Rp0,-</p>
                            </div>
                            <div id="warningJatuhTempoContainer" class="form-group col-sm-12 col-xs-12" style="margin-top: 20px;">
                                <p class="text-danger">Tidak bisa melakukan transaksi karena pelanggan belum membayar hutang melewati jatuh tempo</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-6">
                                <label class="control-label">Sub Total</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control" readonly="readonly" />
                                    <input type="hidden" name="hiddenHargaTotal" id="hiddenHargaTotal" />
                                </div>
                            </div>
                            <div class="form-group col-sm-6 col-xs-6">
                                <label class="control-label">Nego Sub Total (Rp)</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><input type="checkbox" name="checkNegoTotal" id="checkNegoTotal" disabled="" /></div>
                                    <input type="hidden" name="inputNegoTotalMin" id="inputNegoTotalMin" />
                                    <input type="text" name="inputNegoTotal" id="inputNegoTotal" class="form-control angka" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Nego & Potongan Penjualan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputPotonganPenjualan" id="inputPotonganPenjualan" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Ongkos Kirim</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputOngkosKirim" id="inputOngkosKirim" class="form-control angka"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12">
                                <label class="control-label">Grand Total (+Ongkos Kirim)</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputHargaTotalPlusOngkosKirim" id="inputHargaTotalPlusOngkosKirim" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12 hidden">
                                <label class="control-label">Jumlah Bayar</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputJumlahBayar" id="inputJumlahBayar" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-xs-12 hidden">
                                <label class="control-label">Kembali</label>
                                <div class="input-group">
                                    <div class="input-group-addon">Rp</div>
                                    <input type="text" name="inputTotalKembali" id="inputTotalKembali" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="formSimpanContainer">
                                    <form id="form-simpan" action="{{ url('transaksi-grosir') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        {{-- <input type="hidden" name="kode_transaksi" value="" /> --}}
                                        <input type="hidden" name="pelanggan" value="" />
                                        <input type="hidden" name="kode_pelanggan" value="" />
                                        <input type="hidden" name="nama_pelanggan" value="" />
                                        <input type="hidden" name="level_pelanggan" value="" />
                                        <input type="hidden" name="lewat_jatuh_tempo" value="" />
                                        <input type="hidden" name="limit_piutang" value="" />
                                        <input type="hidden" name="batas_piutang" value="" />
                                        <input type="hidden" name="titipan" value="" />
                                        <input type="hidden" name="grosir" value="false" />
                                        {{-- <input type="hidden" name="potongan" value="" /> --}}

                                        <input type="hidden" name="harga_total" />
                                        <input type="hidden" name="harga_akhir" />
                                        <input type="hidden" name="nego_total" />
                                        <input type="hidden" name="check_nego_total" />
                                        <input type="hidden" name="nego_total_min" />
                                        <input type="hidden" name="nego_total_view" />
                                        <input type="hidden" name="potongan_penjualan" />
                                        <input type="hidden" name="ongkos_kirim" />
                                        <input type="hidden" name="jumlah_bayar" />
                                        <input type="hidden" name="kembali" />

                                        <input type="hidden" name="nominal_tunai" />

                                        <input type="hidden" name="no_transfer" />
                                        <input type="hidden" name="bank_transfer" />
                                        <input type="hidden" name="nominal_transfer" />

                                        <input type="hidden" name="no_kartu" />
                                        <input type="hidden" name="bank_kartu" />
                                        <input type="hidden" name="jenis_kartu" />
                                        <input type="hidden" name="nominal_kartu" />

                                        <input type="hidden" name="no_cek" />
                                        <input type="hidden" name="nominal_cek" />

                                        <input type="hidden" name="jatuh_tempo" />
                                        <input type="hidden" name="no_bg" />
                                        <input type="hidden" name="nominal_bg" />

                                        <input type="hidden" name="nominal_titipan" />

                                        <input type="hidden" name="jatuh_tempo" />

                                        <input type="hidden" name="pengirim" />
                                        <input type="hidden" name="pengirim_lain" />

                                        <input type="hidden" name="alamat_lain" />

                                        <div id="append-section"></div>
                                        <div id="bonus-section"></div>
                                        <div class="clearfix">
                                        </div>
                                    </form>
                                </div>
                                <div class="form-group pull-left">
                                    <button type="submit" name="btnSimpanPO" id="btnSimpanPO" class="btn btn-success" disabled=""><i class="fa fa-save"></i> Simpan Pesanan</button>
                                    {{--<button type="submit" name="btnCetakPengambilan" id="btnCetakPengambilan" class="btn btn-success" disabled=""><i class="fa fa-print"></i> Cetak Pengambilan</button>--}}
                                    {{--<button type="submit" name="btnBayar" id="btnBayar" class="btn btn-success" disabled=""><i class="fa fa-money"></i> Bayar</button>--}}
                                </div>
                                <div class="form-group pull-left">
                                    <div type="div" id="infoTransaksi">
                                        TOTAL 0 ITEM
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'PO berhasil disimpan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'PO gagal disimpan!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        var items = [];
        var satuans = [];
        var kode_items = [];
        var pelanggans = [];
        var pelanggan = null;
        var bonus_rules = [];
        var selected_items = [];

        // pelanggan eceran -> harga eceran
        // pelanggan grosir -> harga grosir -> bisa nego
        // status transaksi tergantung pada jumlah total semua

        function isBtnSimpanPODisabled() {
            // console.log('isBtnSimpanPODisabled');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var limit_piutang = parseFloat($('input[name="limit_piutang"]').val());
            var batas_piutang = parseFloat($('input[name="batas_piutang"]').val());
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);

            if (isNaN(limit_piutang)) limit_piutang = 0;
            if (isNaN(batas_piutang)) batas_piutang = 0;
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;

            if (pelanggan_id != '') {
                $('#warningHutangContainer').show('fast', function() {
                    var jumlah_bayar_minimal = harga_total_plus_ongkos_kirim - batas_piutang;
                    if (jumlah_bayar_minimal < 0) jumlah_bayar_minimal = 0;
                    $(this).find('.p1').text('Batasan hutang Rp'+limit_piutang.toLocaleString()+',-');
                    $(this).find('.p2').text('Batasan hutang yang telah dipakai Rp'+(limit_piutang - batas_piutang).toLocaleString()+',-');
                    $(this).find('.p3').text('Sisa batasan hutang Rp'+batas_piutang.toLocaleString()+',-');
                    $(this).find('.p4').text('Minimal jumlah bayar Rp'+(jumlah_bayar_minimal).toLocaleString()+',-');
                });

                var lewat_jatuh_tempo = pelanggan.lewat_jatuh_tempo;
                if (lewat_jatuh_tempo > 0) {
                    $('#warningJatuhTempoContainer').show('fast');
                    return true;
                } else {
                    $('#warningJatuhTempoContainer').hide('fast');
                }
            } else {
                $('#warningHutangContainer').hide('fast');
                $('#warningJatuhTempoContainer').hide('fast');
            }

            var harga_total = parseFloat($('input[name="harga_total"]').val());

            if (isNaN(harga_total) || harga_total <= 0) return true;

            // console.log('checkNegoTotal');
            if ($('#checkNegoTotal').prop('checked') && $('#checkNegoTotal').parents('.form-group').first().hasClass('has-error')) return true;

            var has_error = false;
            var temp_has_error = false;
            $('#tabelKeranjang tr').each(function(index, el) {
                temp_has_error = $(this).find('#inputJumlahItemContainer').hasClass('has-error');
                if (temp_has_error) has_error = true;

                 temp_has_error = $(this).find('#inputNegoContainer').hasClass('has-error');
                if (temp_has_error) has_error = true;
            });
            // console.log(has_error);
            if (has_error) return true;

            return false;
        }

        function cekEceranAtauGrosir() {
            // console.log('cekEceranAtauGrosir');
            var pelanggan_id = $('input[name="pelanggan"]').val();
            var level_pelanggan = $('input[name="level_pelanggan"]').val();

            if (pelanggan_id == '') {
                var is_grosir = true;
                var jumlah_item = 0;

                $('input[name="is_grosir[]"]').each(function(index, el) {
                    // jika ada yang salah maka is_grosir false
                    val = $(el).val();
                    if (val == 'false') is_grosir = false;
                    jumlah_item++;
                });

                $('input[name="grosir"]').val(is_grosir && jumlah_item > 0);

                $('#level_pelanggan').text('Eceran');
                $('#level_pelanggan').removeClass('label-warning');
                $('#level_pelanggan').addClass('label-success');

                if (is_grosir && jumlah_item > 0) {
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            } else {
                // cek selisih jumlah * konversi grosir dan eceran
                var jumlah_grosir = 0;
                var jumlah_eceran = 0;

                $('input[name="item_kode[]"]').each(function(index, el) {
                    var item_kode = $(el).val();
                    var is_grosir = $('#is_grosir-'+item_kode).val() == 'true' ? true : false;
                    var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                    var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                    var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                    var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                    if (is_grosir) jumlah_grosir += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                    else jumlah_eceran += (jumlah1 * konversi1) + (jumlah2 * konversi2);
                });

                if (level_pelanggan == 'eceran') {
                    $('#level_pelanggan').text('Eceran');
                    $('#level_pelanggan').removeClass('label-warning');
                    $('#level_pelanggan').addClass('label-success');
                } else {
                    $('#level_pelanggan').text('Grosir');
                    $('#level_pelanggan').removeClass('label-success');
                    $('#level_pelanggan').addClass('label-warning');
                }

                if (jumlah_grosir > jumlah_eceran) {
                    $('input[name="grosir"]').val(true);
                    $('#tipe_penjualan').text('Grosir');
                    $('#tipe_penjualan').removeClass('label-success');
                    $('#tipe_penjualan').addClass('label-warning');
                } else {
                    $('input[name="grosir"]').val(false);
                    $('#tipe_penjualan').text('Eceran');
                    $('#tipe_penjualan').removeClass('label-warning');
                    $('#tipe_penjualan').addClass('label-success');
                }
            }
        }

        function updateHargaTotal() {
            var harga_total = 0;
            var harga_akhir = 0;
            var nego_total = 0;
            var nego_total_min = 0;

            $('.subtotal').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');

                var tmp_harga = parseFloat($('#subtotal-'+item_kode).val());
                if (isNaN(tmp_harga)) tmp_harga = 0;
                harga_total += tmp_harga;

                var tmp_nego = parseFloat($('#nego-'+item_kode).val());
                if (isNaN(tmp_nego)) tmp_nego = 0;
                nego_total += tmp_nego;

                if (tmp_nego > 0) {
                    harga_akhir += tmp_nego;
                } else {
                    harga_akhir += tmp_harga;
                }

                var tmp_nego_min = parseFloat($('#nego_min-'+item_kode).val());
                if (isNaN(tmp_nego_min)) tmp_nego_min = 0;
                nego_total_min += tmp_nego_min;
            });

            var nego_total_view = parseFloat($('input[name="nego_total_view"]').val());
            if (nego_total_view > 0 && nego_total_view < harga_akhir) harga_akhir = nego_total_view;

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="harga_akhir"]').val(harga_akhir);
            $('input[name="nego_total"]').val(nego_total);
            $('input[name="nego_total_min"]').val(nego_total_min);

            $('#inputNegoTotalMin').val(nego_total_min);
        }

        function updateBonusTransaksi() {
            var harga_total = parseFloat($('input[name="harga_total"]').val());
            var potongan_penjualan = parseFloat($('input[name="potongan_penjualan"]').val());
            var pelanggan_id = parseFloat($('input[name="pelanggan"]').val());

            var nominal = 0;
            var profit = 0;
            var jumlah = 0;
            var status = 1;

            nominal = harga_total - potongan_penjualan;

            var hpp = 0;
            $('input[name="hpp[]"]').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');
                var temp_hpp = parseFloat($('#hpp-'+item_kode).val());
                var temp_jumlah_1 = parseFloat($('#jumlah1-'+item_kode).val());
                var temp_jumlah_2 = parseFloat($('#jumlah2-'+item_kode).val());
                var temp_konversi_1 = parseFloat($('#konversi1-'+item_kode).val());
                var temp_konversi_2 = parseFloat($('#konversi2-'+item_kode).val());

                if (isNaN(temp_hpp)) temp_hpp = 0;
                if (isNaN(temp_jumlah_1)) temp_jumlah_1 = 0;
                if (isNaN(temp_jumlah_2)) temp_jumlah_2 = 0;
                if (isNaN(temp_konversi_1)) temp_konversi_1 = 0;
                if (isNaN(temp_konversi_2)) temp_konversi_2 = 0;

                var temp_jumlah = (temp_jumlah_1 * temp_konversi_1) + (temp_jumlah_2 * temp_konversi_2);

                temp_hpp *= temp_jumlah;
                hpp += temp_hpp;
            });
            profit = nominal - hpp;

            $('#tabelKeranjang tbody tr').each(function(index, el) {
                jumlah++;
            });

            if (pelanggan_id != '' ) {
                for (var i = 0; i < pelanggans.length; i++) {
                    var pelanggan = pelanggans[i];
                    if (pelanggan.id == pelanggan_id) {
                        if (pelanggan.level == 'grosir') {
                            status = 2;
                        }
                        break;
                    }
                }
            }
            // console.log(nominal, profit, jumlah, status);

            $('#tabelBonus tbody').find('tr').remove();
            $('#form-simpan').find('#bonus-section').find('input').remove();
            var valid_bonus_rules = [];
            for (var i = 0; i < bonus_rules.length; i++) {
                var is_bonus_available = false;
                var is_nominal_valid = false;
                var is_profit_valid = false;
                var is_jumlah_valid = false;
                var is_status_valid = false;
                var jumlah_bonus = 1;
                var bonus_rule = bonus_rules[i];

                if (parseFloat(bonus_rule.nominal) == 0) {
                    is_nominal_valid = true;
                } else {
                    if (parseFloat(nominal) >= parseFloat(bonus_rule.nominal)) {
                        is_nominal_valid = true;
                        if (parseFloat(bonus_rule.kelipatan) > 0) {
                            jumlah_bonus = parseInt(parseFloat(nominal) / parseFloat(bonus_rule.nominal));
                        }
                    }
                }

                if (parseFloat(bonus_rule.profit) == 0) {
                    is_profit_valid = true;
                } else {
                    if (parseFloat(profit) >= parseFloat(bonus_rule.profit)) {
                        is_profit_valid = true;
                    }
                }

                if (parseFloat(bonus_rule.jumlah) == 0) {
                    is_jumlah_valid = true;
                } else {
                    if (parseFloat(jumlah) >= parseFloat(bonus_rule.jumlah)) {
                        is_jumlah_valid = true;
                    }
                }

                if (parseFloat(bonus_rule.status) == 0) {
                    is_status_valid = true;
                } else if (parseFloat(bonus_rule.status) == 1 && parseFloat(status) == 1) {
                    is_status_valid = true;
                } else if (parseFloat(bonus_rule.status) == 2 && parseFloat(status) == 2) {
                    is_status_valid = true;
                }
                // console.log(is_nominal_valid, is_profit_valid, is_jumlah_valid, is_status_valid, jumlah_bonus, i);

                if (is_nominal_valid && is_profit_valid && is_jumlah_valid && is_status_valid) {
                    valid_bonus_rules.push({
                        bonus_rule: bonus_rule,
                        jumlah_bonus: jumlah_bonus
                    });
                }
            }
            // console.log(valid_bonus_rules);

            var bonus_transaksis = [];
            for (var i = 0; i < valid_bonus_rules.length; i++) {
                var bonus_rule = valid_bonus_rules[i].bonus_rule;
                var jumlah_bonus = valid_bonus_rules[i].jumlah_bonus;
                for (var j = 0; j < bonus_rule.relasi_bonuses.length; j++) {
                    if (parseFloat(jumlah_bonus) > 0) {
                        var bonus = bonus_rule.relasi_bonuses[j].bonus;
                        var bonus_id = bonus.id;
                        var bonus_jumlah = parseFloat(jumlah_bonus) * parseFloat(bonus_rule.relasi_bonuses[j].jumlah);

                        if (parseFloat(bonus.stoktotal) < bonus_jumlah) {
                            bonus_jumlah = parseFloat(bonus.stoktotal);
                        }

                        var ada_yang_sama = false;
                        for (var k = 0; k < bonus_transaksis.length; k++) {
                            var bonus_transaksi = bonus_transaksis[k];
                            if (bonus_transaksi.bonus_id == bonus_id) {
                                bonus_transaksi.bonus_jumlah += bonus_jumlah;
                                ada_yang_sama = true;
                                break;
                            }
                        }

                        if (!ada_yang_sama) {
                            bonus_transaksis.push({
                                bonus: bonus,
                                bonus_id: bonus_id,
                                bonus_jumlah: bonus_jumlah,
                            });
                        }
                    }
                }
            }

            var valid_bonus_rules_index = 0;
            // console.log(bonus_transaksis);
            for (var i = 0; i < bonus_transaksis.length; i++) {
                var bonus_transaksi = bonus_transaksis[i];
                var bonus = bonus_transaksi.bonus;
                var bonus_id = bonus_transaksi.bonus_id;
                var bonus_jumlah = bonus_transaksi.bonus_jumlah;
                var bonus_jumlah_satuan = '';
                var temp_bonus_jumlah = bonus_jumlah;
                for (var k = 0; k < bonus.satuan_pembelians.length; k++) {
                    if (temp_bonus_jumlah > 0) {
                        var satuan = bonus.satuan_pembelians[k];
                        if (temp_bonus_jumlah % satuan.konversi == 0) {
                            bonus_jumlah_satuan += parseInt(temp_bonus_jumlah / satuan.konversi);
                            bonus_jumlah_satuan += ' ';
                            bonus_jumlah_satuan += satuan.satuan.kode;
                        }
                    }
                }

                var tr = ''+
                    '<tr>'+
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+bonus.nama+'</td>'+
                        '<td>'+bonus_jumlah_satuan+'</td>'+
                    '</tr>';
                $('#tabelBonus tbody').append($(tr));

                $('#form-simpan').find('#bonus-section').append('<input type="hidden" name="bonus_id[]" id="bonus_id-'+valid_bonus_rules_index+'" value="'+bonus_id+'" />');
                $('#form-simpan').find('#bonus-section').append('<input type="hidden" name="bonus_jumlah[]" id="bonus_jumlah-'+valid_bonus_rules_index+'" value="'+bonus_jumlah+'" />');
                valid_bonus_rules_index++;
            }

            // console.log(valid_bonus_rules_index);
            if (valid_bonus_rules_index > 0) {
                $('#tabelBonus').parents('.form-group').show();
            } else {
                $('#tabelBonus').parents('.form-group').hide();
            }
        }

        function updateInfoTransaksi() {
            $('#infoTransaksi').text(`TOTAL ${selected_items.length} ITEM`);
        }

        function updateHargaOnKeyup() {
            updateHargaTotal();

            var $harga_total = $('#inputHargaTotal');
            var $potongan_penjualan = $('#inputPotonganPenjualan');
            var $harga_total_plus_ongkos_kirim = $('#inputHargaTotalPlusOngkosKirim');
            var $jumlah_bayar = $('#inputJumlahBayar');
            var $kembali = $('#inputTotalKembali');

            var level_pelanggan = $('input[name="level_pelanggan"]').val();
            var nominal_tunai = $('input[name="nominal_tunai"]').val();
            var nominal_transfer = $('input[name="nominal_transfer"]').val();
            var nominal_cek = $('input[name="nominal_cek"]').val();
            var nominal_bg = $('input[name="nominal_bg"]').val();
            var nominal_kredit = $('input[name="nominal_kredit"]').val();
            var nominal_titipan = $('input[name="nominal_titipan"]').val();
            var harga_total = $('input[name="harga_total"]').val();
            if (harga_total === undefined) harga_total = '0';
            var harga_akhir = $('input[name="harga_akhir"]').val();
            if (harga_akhir === undefined) harga_akhir = '0';
            var nego_total = $('input[name="nego_total"]').val();
            if (nego_total === undefined) nego_total = '0';
            var nego_total_min = $('input[name="nego_total_min"]').val();
            if (nego_total_min === undefined) nego_total_min = '0';
            var nego_total_view = $('input[name="nego_total_view"]').val();
            if (nego_total_view === undefined) nego_total_view = '0';
            var ongkos_kirim = $('input[name="ongkos_kirim"]').val();
            if (ongkos_kirim === undefined) ongkos_kirim = '0';

            nominal_tunai = parseFloat(nominal_tunai);
            nominal_transfer = parseFloat(nominal_transfer);
            nominal_cek = parseFloat(nominal_cek);
            nominal_bg = parseFloat(nominal_bg);
            nominal_kredit = parseFloat(nominal_kredit);
            nominal_titipan = parseFloat(nominal_titipan);
            harga_total = parseFloat(harga_total);
            harga_akhir = parseFloat(harga_akhir);
            nego_total = parseFloat(nego_total);
            nego_total_min = parseFloat(nego_total_min);
            nego_total_view = parseFloat(nego_total_view);
            ongkos_kirim = parseFloat(ongkos_kirim);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_transfer)) nominal_transfer = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;
            if (isNaN(nominal_kredit)) nominal_kredit = 0;
            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(harga_akhir)) harga_akhir = 0;
            if (isNaN(nego_total)) nego_total = 0;
            if (isNaN(nego_total_min)) nego_total_min = 0;
            if (isNaN(nego_total_view)) nego_total_view = 0;
            if (isNaN(ongkos_kirim)) ongkos_kirim = 0;

            /*var nego_total_akhir = 0;
            if (nego_total_view <= 0) {
                if (nego_total_data > 0) {
                    nego_total_akhir = nego_total_data;
                }
            } else if (nego_total_data <= 0) {
                if (nego_total_view > 0 && nego_total_view >= nego_total_min) {
                    nego_total_akhir = nego_total_view;
                }
            } else {
                if (nego_total_data < nego_total_view) {
                    nego_total_akhir = nego_total_data;
                } else {
                    if (nego_total_view >= nego_total_min) {
                        nego_total_akhir = nego_total_view;
                    } else {
                        nego_total_akhir = nego_total_data;
                    }
                }
            }*/

            var jumlah_bayar = nominal_tunai + nominal_transfer + nominal_cek + nominal_bg + nominal_kredit + nominal_titipan;
            var kembali = 0;
            var harga_total_plus_ongkos_kirim = 0;

            // Tambah potongan penjualan dengan diskon tiap item
            var potongan = 0;
            var potongan_penjualan = 0;
            $('input[name="diskon[]"]').each(function(index, el) {
                var item_kode = $(el).attr('item_kode');

                var harga = 0;
                var harga1_eceran = parseFloat($('#harga1_eceran-'+item_kode).val());
                var harga1_grosir = parseFloat($('#harga1_grosir-'+item_kode).val());
                var harga2_eceran = parseFloat($('#harga2_eceran-'+item_kode).val());
                var harga2_grosir = parseFloat($('#harga2_grosir-'+item_kode).val());
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var diskon = parseFloat($(el).val());
                // var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                // var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                var subtotal = parseFloat($('#subtotal-'+item_kode).val());
                var nego = parseFloat($('#nego-'+item_kode).val());

                if (isNaN(harga1_eceran)) harga1_eceran = 0;
                if (isNaN(harga1_grosir)) harga1_grosir = 0;
                if (isNaN(harga2_eceran)) harga2_eceran = 0;
                if (isNaN(harga2_grosir)) harga2_grosir = 0;
                if (isNaN(jumlah1)) jumlah1 = 0;
                if (isNaN(jumlah2)) jumlah2 = 0;
                if (isNaN(konversi1)) konversi1 = 0;
                if (isNaN(konversi2)) konversi2 = 0;
                if (isNaN(diskon)) diskon = 0;
                // if (isNaN(harga_eceran)) harga_eceran = 0;
                // if (isNaN(harga_grosir)) harga_grosir = 0;
                if (isNaN(subtotal)) subtotal = 0;
                if (isNaN(nego)) nego = 0;

                if (diskon > 0) {
                    // console.log('diskon');
                    var harga = 0;
                    if (level_pelanggan == 'grosir') {
                        harga = (harga1_grosir * jumlah1) + (harga2_grosir * jumlah2);
                    } else {
                        harga = (harga1_eceran * jumlah1) + (harga2_eceran * jumlah2);
                    }

                    // var jumlah = (jumlah1 * konversi1) + (jumlah2 * konversi2);

                    // harga = harga_eceran;
                    // if (level_pelanggan == 'grosir') harga = harga_grosir;

                    // var temp_potongan = diskon / 100 * harga * jumlah;
                    // temp_potongan = Math.ceil(temp_potongan / 100) * 100;

                    // var potongan_diskon = diskon / 100 * harga * jumlah;
                    var potongan_diskon = diskon / 100 * harga;
                    potongan_diskon = Math.ceil(potongan_diskon / 100) * 100;

                    var potongan_nego = subtotal - nego;
                    if (nego <= 0) {
                        potongan_nego = 0;
                    }
                    potongan_nego = Math.ceil(potongan_nego / 100) * 100;

                    if (potongan_diskon > potongan_nego) {
                        potongan += potongan_diskon;
                    } else {
                        potongan += potongan_nego;
                    }
                    // console.log(potongan_diskon, potongan_nego, potongan);
                } else {
                    // console.log('tidak diskon');
                    if (nego > 0) {
                        var potongan_nego = subtotal - nego;
                        potongan_nego = Math.ceil(potongan_nego / 100) * 100;
                        potongan += potongan_nego;
                    }
                }
            });

            // if (nego_total_view > 0) {
            //     // potongan = harga_total - nego_total_view;
            //     potongan_penjualan = harga_total - nego_total_view;
            // } else {
            //     harga_akhir -= potongan;
            //     potongan_penjualan += (harga_total - harga_akhir);
            // }

            harga_akhir = harga_total - potongan;
            potongan_penjualan = potongan;
            if (nego_total_view > 0 && nego_total_view < harga_akhir) {
                harga_akhir = nego_total_view;
                potongan_penjualan = harga_total - nego_total_view;
            }
            // console.log(harga_akhir);

            var laba_rugi_total = harga_akhir - nego_total_min;

            if (harga_akhir <= 0) laba_rugi_total = 0;
            if (harga_akhir <= 0) potongan_penjualan = 0;

            /*if (nego_total_akhir > 0) {
                harga_total_plus_ongkos_kirim = nego_total_akhir + ongkos_kirim;
                kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;
            } else {
                harga_total_plus_ongkos_kirim = harga_total + ongkos_kirim;
                kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;
            }*/

            harga_total_plus_ongkos_kirim = harga_akhir + ongkos_kirim;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            // if (isNaN(harga_total)) harga_total = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            // var potongan_penjualan = harga_total - nego_total_akhir;
            // if (nego_total_akhir <= 0) potongan_penjualan = 0;

            $harga_total.val(harga_total.toLocaleString());
            // $nego_total_view.val(nego_total_akhir.toLocaleString());
            $potongan_penjualan.val(potongan_penjualan.toLocaleString());
            $harga_total_plus_ongkos_kirim.val((harga_total_plus_ongkos_kirim).toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());
            $kembali.val(kembali.toLocaleString());

            $('input[name="harga_total"]').val(harga_total);
            $('input[name="potongan_penjualan"]').val(potongan_penjualan);
            // $('input[name="nego_total"]').val(nego_total_akhir);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
            $('input[name="kembali"]').val(kembali);

            // cek eceran atau grosir
            if (harga_total > 0) {
                cekEceranAtauGrosir();
            }

            // cek boleh nego atau tidak
            if ($('input[name="pelanggan"]').val() == '') {
                $('input[name="checkNego"]').prop('disabled', true);
                $('input[name="checkNegoTotal"]').prop('disabled', true);
            } else {
                if (pelanggan.level == 'grosir') {
                    $('input[name="checkNego"]').prop('disabled', false);
                    $('input[name="checkNegoTotal"]').prop('disabled', false);

                    var jumlah_item_yang_bisa_dinego = 0;
                    $('#tabelKeranjang tr').each(function(index, el) {
                        var item_kode = $(el).attr('data-id');
                        var subtotal = parseFloat($('#subtotal-'+item_kode).val());
                        var nego_min = parseFloat($('#nego_min-'+item_kode).val());

                        if (isNaN(subtotal)) subtotal = 0;
                        if (isNaN(nego_min)) nego_min = 0;

                        // console.log(subtotal, nego_min);
                        if (subtotal <= nego_min) {
                            $(el).find('input[name="checkNego"]').prop('disabled', true);
                        } else {
                            $(el).find('input[name="checkNego"]').prop('disabled', false);
                            if (subtotal > 0 && nego_min > 0) {
                                jumlah_item_yang_bisa_dinego++;
                            }
                        }
                    });

                    if (jumlah_item_yang_bisa_dinego <= 0) {
                        $('input[name="checkNegoTotal"]').prop('disabled', true);
                    }

                    if ($('input[name="checkNegoTotal"]').prop('checked')) {
                        $('input[name="checkNego"]').prop('disabled', true);
                        $('input[name="inputNego"]').prop('readonly', true);
                    } else {
                        // $('input[name="checkNego"]').prop('disabled', false);
                        // $('input[name="inputNego"]').prop('readonly', false);
                    }
                } else {
                    $('input[name="checkNego"]').prop('disabled', true);
                    $('input[name="checkNegoTotal"]').prop('disabled', true);
                }
            }

            updateBonusTransaksi();
            updateInfoTransaksi();

            // $('#formSimpanContainer').find('button[type="submit"]').prop('disabled', isSubmitButtonDisabled());
            $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
        }

        function updateHargaKeEceran() {
            // console.log('updateHargaKeEceran');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var harga1_eceran = parseFloat($('#harga1_eceran-'+item_kode).val());
                var harga2_eceran = parseFloat($('#harga2_eceran-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                // var harga_eceran = parseFloat($('#harga_eceran-'+item_kode).val());
                // var harga_per_pcs = Math.ceil(harga_eceran / 100) * 100;
                // var harga_view = harga_per_pcs * konversi;
                // var subtotal = harga_per_pcs * jumlah;

                harga1_eceran = parseFloat(harga1_eceran);
                harga2_eceran = parseFloat(harga2_eceran);

                if (isNaN(harga1_eceran)) harga1_eceran = 0;
                if (isNaN(harga2_eceran)) harga2_eceran = 0;

                var harga_eceran = 0;
                var subtotal = harga1_eceran * jumlah1 + harga2_eceran * jumlah2;
                if (konversi1 >= konversi2) {
                    harga_eceran = harga1_eceran;
                } else {
                    harga_eceran = harga2_eceran;
                }

                $(el).find('#inputHargaPerSatuan').val(harga_eceran.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                // $('#harga_eceran-'+item_kode).val(harga_eceran);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }

        function updateHargaKeGrosir() {
            // console.log('updateHargaKeGrosir');
            $('#tabelKeranjang tbody tr').each(function(index, el) {
                var item_kode = $(el).attr('data-id');
                var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
                var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
                var konversi = parseFloat($('#konversi-'+item_kode).val());
                var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
                var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
                var harga1_grosir = parseFloat($('#harga1_grosir-'+item_kode).val());
                var harga2_grosir = parseFloat($('#harga2_grosir-'+item_kode).val());
                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                // var harga_grosir = parseFloat($('#harga_grosir-'+item_kode).val());
                // var harga_per_pcs = Math.ceil(harga_grosir / 100) * 100;
                // var harga_view = harga_per_pcs * konversi;
                // var subtotal = harga_per_pcs * jumlah;

                harga1_grosir = parseFloat(harga1_grosir);
                harga2_grosir = parseFloat(harga2_grosir);

                if (isNaN(harga1_grosir)) harga1_grosir = 0;
                if (isNaN(harga2_grosir)) harga2_grosir = 0;

                var harga_grosir = 0;
                var subtotal = harga1_grosir * jumlah1 + harga2_grosir * jumlah2;
                if (konversi1 >= konversi2) {
                    harga_grosir = harga1_grosir;
                } else {
                    harga_grosir = harga2_grosir;
                }

                $(el).find('#inputHargaPerSatuan').val(harga_grosir.toLocaleString());
                $(el).find('#inputSubTotal').val(subtotal.toLocaleString());

                // $('#harga_grosir-'+item_kode).val(harga_grosir);
                $('#subtotal-'+item_kode).val(subtotal);
            });
        }

        function cariItem(kode, url, tr) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = item.nama;
                var stoktotal = item.stoktotal;
                // console.log(stoktotal);
                var diskon = parseFloat(item.diskon);
                // var limit_grosir = item.limit_grosir;
                var bonus_id = item.bonus_id;
                var hpp = parseFloat(data.hpp.harga);
                var harga_eceran = parseFloat(data.harga.eceran);
                var harga_grosir = parseFloat(data.harga.grosir);
                var nego_min = Math.round(hpp * 110 * (1 + (item.profit / 100))) / 100;
                // console.log(nego_min);
                nego_min = Math.round(nego_min);
                // console.log(nego_min);
                nego_min = Math.ceil(nego_min / 100) * 100;
                // console.log(nego_min);
                // var nego_min = Math.ceil(hpp * 1.1 * (1 + (item.profit / 100)) / 100) * 100;
                var bonuses = data.relasi_bonus;

                hpp = Math.round(hpp * 110) / 100;

                var has_error = false;
                if (harga_eceran <= 0 || harga_grosir <= 0) has_error = true;

                var v_harga = 0;
                var pelanggan_id = $('select[name="pelanggan_id"]').val();
                var temp_pelanggan = null;
                for (var i = 0; i < pelanggans.length; i++) {
                    if (pelanggans[i].id == pelanggan_id) {
                        temp_pelanggan = pelanggans[i];
                    }
                }

                if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                    v_harga = harga_eceran;
                } else {
                    v_harga = harga_grosir;
                }

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < satuans.length; j++) {
                        if (satuans[j].id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: satuans[j].id,
                                kode: satuans[j].kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }
                // console.log(satuan_item);
                var ul_satuan = '<ul class="dropdown-menu">';
                var satuan_terkecil = {
                    id: '',
                    kode: '',
                    konversi: ''
                };

                for (var i = 0; i < satuan_item.length; i++) {
                    var satuan = satuan_item[i];
                    ul_satuan += '<li><a id="'+satuan.id+'" kode="'+satuan.kode+'" konversi="'+satuan.konversi+'">'+satuan.kode+'</a></li>';
                    if (i == satuan_item.length - 1) {
                        satuan_terkecil.id = satuan.id;
                        satuan_terkecil.kode = satuan.kode;
                        satuan_terkecil.konversi = satuan.konversi;
                    }
                }
                ul_satuan += '</ul>';

                var bonus = null;
                for (var i = 0; i < bonuses.length; i++) {
                    if (bonuses[i].syarat == 1) {
                        bonus = bonuses[i];
                    }
                }

                // Ngurus view
                if (tr === undefined) {
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="jumlah1[]" id="jumlah1-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="jumlah2[]" id="jumlah2-'+kode+'" value="0" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="satuan1[]" id="satuan1-'+kode+'" value="'+satuan_terkecil.id+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="satuan2[]" id="satuan2-'+kode+'" value="'+satuan_terkecil.id+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi[]" id="konversi-' +kode+'" value="'+satuan_terkecil.konversi+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi1[]" id="konversi1-' +kode+'" value="'+satuan_terkecil.konversi+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="konversi2[]" id="konversi2-' +kode+'" value="'+satuan_terkecil.konversi+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="diskon[]" id="diskon-' + kode + '" value="'+diskon+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="hpp[]" id="hpp-' + kode + '" value="'+hpp+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga[]" id="harga-' + kode + '" value="'+v_harga+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1[]" id="harga1-' + kode + '" value="'+v_harga+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1_eceran[]" id="harga1_eceran-' + kode + '" value="'+harga_eceran+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga1_grosir[]" id="harga1_grosir-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2[]" id="harga2-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2_eceran[]" id="harga2_eceran-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga2_grosir[]" id="harga2_grosir-' + kode + '" value="" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga_eceran[]" id="harga_eceran-' + kode + '" value="'+harga_eceran+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="harga_grosir[]" id="harga_grosir-' + kode + '" value="'+harga_grosir+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="subtotal[]" id="subtotal-'+kode+'" class="subtotal" value="'+v_harga+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="nego[]" id="nego-'+kode+'" class="nego" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="nego_min[]" id="nego_min-'+kode+'" class="nego_min" value="'+nego_min+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="stoktotal[]" id="stoktotal-'+kode+'" class="stoktotal" value="'+stoktotal+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" item_kode="'+kode+'" name="is_grosir[]" id="is_grosir-'+kode+'" class="is_grosir" value="false" />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Barang Belanja" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="vertical-align: middle;">'+nama+'</td>'+
                            '<td id="inputJumlahItemContainer" class="'+(has_error?'has-error':'')+'">'+
                                '<div class="row">'+
                                    '<div class="col-md-6" style="padding-right: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem1" id="inputJumlahItem1" class="form-control input-sm" value="1" />'+
                                            '<div id="pilihSatuan1" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-6" style="padding-left: 0;">'+
                                        '<div class="input-group" style="margin-bottom: 0;">'+
                                            '<input type="text" name="inputJumlahItem2" id="inputJumlahItem2" class="form-control input-sm" />'+
                                            '<div id="pilihSatuan2" class="input-group-btn">'+
                                                '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0; margin-right: 0; width: 60px;"><span class="text" style="line-height: inherit;">'+satuan_terkecil.kode+' </span><span class="caret"></span></button>'+
                                                ul_satuan+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm" value="' + v_harga.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputSubTotalContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm angka inputSubTotal" value="' + v_harga.toLocaleString() + '" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputNegoContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            '<input type="checkbox" name="checkNego" id="checkNego" />'+
                                        '</div>'+
                                        '<input type="hidden" name="inputNegoMin" id="inputNegoMin" class="form-control input-sm" value="' + nego_min + '" />'+
                                        '<input type="text" name="inputNego" id="inputNego" class="form-control input-sm angka" readonly="readonly" />'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="vertical-align: middle;">'+
                                '<div id="bonusContainer">'+
                                    (bonus == null?'Tidak ada':'1 '+bonus.bonus.nama)+
                                '</div>'+
                            '</td>'+
                        '</tr>');
                }

                var text_stoktotal = '-';
                var temp_stoktotal = stoktotal;
                for (var i = 0; i < satuan_item.length; i++) {
                    if (temp_stoktotal > 0) {
                        var satuan = satuan_item[i];
                        var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                        if (jumlah_stok > 0) {
                            if (text_stoktotal == '-') text_stoktotal = '';
                            text_stoktotal += jumlah_stok;
                            text_stoktotal += ' ';
                            text_stoktotal += satuan.kode;

                            temp_stoktotal = temp_stoktotal % satuan.konversi;
                            if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                        }
                    }
                }

                var tr_stoktotal = ''+
                    '<tr>'+
                        '<td>'+nama+'</td>'+
                        '<td>'+text_stoktotal+'</td>'+
                    '</tr>';

                $('#tabelInfo tbody').empty();
                $('#tabelInfo tbody').append(tr_stoktotal);

                var $harga_total = $('#inputHargaTotal');
                var $nego_total_min = $('#inputNegoTotalMin');

                var harga_total = parseFloat($harga_total.val().replace(/\D/g, ''), 10);
                var nego_total_min = parseFloat($nego_total_min.val().replace(/\D/g, ''), 10);

                harga_total = (isNaN(harga_total) || harga_total < 0) ? 0 : harga_total;
                nego_total_min = (isNaN(nego_total_min) || nego_total_min < 0) ? 0 : nego_total_min;

                // console.log(harga_total, v_harga);
                $('#formSimpanContainer').find('input[name="harga_total"]').val(harga_total + v_harga);
                $nego_total_min.val(nego_total_min + nego_min);

                updateHargaOnKeyup();

                $('#spinner').hide();
            });
        }

        /*function matchItemKode(params, data) {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // Do not display the item if there is no 'text' property
            if (typeof data.text === 'undefined') {
                return null;
            }

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) > -1) {
                var modifiedData = $.extend({}, data, true);
                // modifiedData.text += ' (matched)';

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            if (data.id.toUpperCase().indexOf(params.term.toUpperCase()) > -1) {
                return $.extend({}, data, true);
            }

            // Return `null` if the term should not be displayed
            return null;
        }*/

        $(document).ready(function() {
            $('#inputOngkosKirim').prop('readonly', true);
            // var url = "{{ url('po-penjualan') }}";
            // var a = $('a[href="' + url + '"]');
            // a.parent().addClass('current-page');
            // a.parent().parent().show();
            // a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            items = '{{ json_encode($items) }}';
            items = items.replace(/&quot;/g, '"');
            items = JSON.parse(items);
            for (var i = 0; i < items.length; i++) {
                kode_items.push(items[i].kode);
            }

            satuans = '{{ $satuans }}';
            satuans = satuans.replace(/&quot;/g, '"');
            satuans = JSON.parse(satuans);

            pelanggans = '{{ $pelanggans }}';
            pelanggans = pelanggans.replace(/&quot;/g, '"');
            pelanggans = JSON.parse(pelanggans);

            bonus_rules = '{{ $bonus_rules }}';
            bonus_rules = bonus_rules.replace(/&quot;/g, '"');
            bonus_rules = JSON.parse(bonus_rules);

            $('input[name="pelanggan"]').val('');
            $('input[name="kode_pelanggan"]').val('');
            $('input[name="nama_pelanggan"]').val('');
            $('input[name="level_pelanggan"]').val('');
            $('input[name="lewat_jatuh_tempo"]').val(0);
            $('input[name="limit_piutang"]').val('');
            $('input[name="batas_piutang"]').val('');
            $('input[name="titipan"]').val('');
            $('input[name="grosir"]').val('false');
            $('input[name="harga_total"]').val('');
            $('input[name="harga_akhir"]').val('');
            $('input[name="nego_total"]').val('');
            $('input[name="nego_total_min"]').val('');
            $('input[name="nego_total_view"]').val('');
            $('input[name="potongan_penjualan"]').val('');
            $('input[name="ongkos_kirim"]').val('');
            $('input[name="check_nego_total"]').val(0);

            $('#pilihanPotonganContainer').hide();
            $('#inputTunaiContainer').hide();
            $('#inputTransferBankContainer').hide();
            $('#inputTransferBankContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBGContainer').hide();
            $('#inputBGContainer').find('input').val('');
            $('#inputKreditContainer').hide();
            $('#inputTitipanContainer').hide();

            $('#btnDiambil').trigger('click');
            $('#btnAlamatBiasa').trigger('click');
            $('#warningHutangContainer').hide();

            $(".select2_single").select2({
                width: '100%'
                // allowClear: true
            });

            $(".select2_single").each(function(index, el) {
                $(el).val('').trigger('change');
            });

            $('select[name="item_id"]').select2({
                width: '100%',
                // matcher: matchItemKode
            });

            // $('select[name="pelanggan_id"]').val('').trigger('change');

            $('#warningJatuhTempoContainer').hide('fast');

            $('#inputHargaTotal').val(0);
            // $('#inputNegoTotal').val(0);
            $('#inputHargaTotalPlusOngkosKirim').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);
        });

        // var keyupFromScanner = false;
        $(document).scannerDetection({
            avgTimeByChar: 40,
            onComplete: function(code, qty) {
                // console.log('Kode: ' + code, qty);
            },
            onError: function(error) {
                // console.log('Barcode: ' + error);
                // var kode = error;
                // var url  = "{{ url('transaksi') }}"+'/'+kode+'/item/json';
                // var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

                // var terpilih = false;
                // $('input[name="item_kode[]"]').each(function(index, el) {
                //     if ($(el).val() == kode) {
                //         terpilih = true;
                //     }
                // });

                // if (terpilih) {
                //     // keyupFromScanner = true;
                //     var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').val());
                //     $('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').val(jumlah_awal + 1);
                //     $('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').trigger('keyup');
                //     $('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').trigger('blur');
                // } else {
                //     cariItem(kode, url, tr);
                // }

                var kode = error;
                // console.log(kode+'aw');
                // console.log(items);

                if (kode_items.includes(kode)) {
                    var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
                    var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

                    if (kode == '') {
                        $('#tabelInfo tbody').empty();
                    } else if (!selected_items.includes(kode)) {
                        selected_items.push(kode);
                        $('#spinner').show();
                        cariItem(kode, url, tr);
                    }

                    $('#checkNegoTotal').prop('checked', false);
                    $('#checkNegoTotal').trigger('change');
                }
            }
        });

        $(document).keypress(function(e) {
            if (e.key == 'Enter') {
                $('#btnSimpanPO').trigger('click');
            }
        });

        $(document).on('change', 'select[name="pelanggan_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if (id != '') {
                for (var i = 0; i < pelanggans.length; i++) {
                    if (pelanggans[i].id == id) {
                        pelanggan = pelanggans[i];
                        $('input[name="pelanggan"]').val(pelanggan.id);
                        $('input[name="kode_pelanggan"]').val(pelanggan.kode);
                        $('input[name="nama_pelanggan"]').val(pelanggan.nama);
                        $('input[name="level_pelanggan"]').val(pelanggan.level);
                        $('input[name="lewat_jatuh_tempo"]').val(pelanggan.lewat_jatuh_tempo);
                        $('input[name="limit_piutang"]').val(pelanggan.limit_jumlah_piutang);
                        $('input[name="batas_piutang"]').val(pelanggan.batas_piutang);

                        var nama = pelanggan.nama ? pelanggan.nama : '-';
                        var alamat = pelanggan.alamat ? pelanggan.alamat : '-';
                        var telepon = pelanggan.telepon ? pelanggan.telepon : '-';
                        var tr = ''+
                            '<tr>'+
                                '<td>'+nama+'</td>'+
                                '<td>'+alamat+'</td>'+
                                '<td>'+telepon+'</td>'+
                            '</tr>';

                        $('#tabelPelanggan tbody').empty();
                        $('#tabelPelanggan tbody').append(tr);

                        var lama_jatuh_tempo = pelanggan.jatuh_tempo;
                        var jatuh_tempo = getJatuhTempo(lama_jatuh_tempo);
                        // console.log(jatuh_tempo);
                        $('#inputJatuhTempo').val(ymd2dmy(jatuh_tempo));
                        $('input[name="jatuh_tempo"]').val(jatuh_tempo);

                        var alamat_lain = $('input[name="alamat_lain"]').val();
                        if (alamat_lain == '') {
                            $('#btnAlamatBiasa').trigger('click');
                        } else {
                            $('#btnAlamatLain').trigger('click');
                        }

                        break;
                    }
                }

                if (pelanggan.level == 'eceran') updateHargaKeEceran();
                else updateHargaKeGrosir();
            } else {
                pelanggan = null;
                $('#btnAlamatBiasa').trigger('click');
                $('input[name="pelanggan"]').val('');
                $('input[name="kode_pelanggan"]').val('');
                $('input[name="nama_pelanggan"]').val('');
                $('input[name="level_pelanggan"]').val('');
                $('input[name="lewat_jatuh_tempo"]').val(0);

                $('#tabelPelanggan tbody').empty();

                $('#inputJatuhTempo').val('');
                $('input[name="jatuh_tempo"]').val(0);

                updateHargaKeEceran();
            }

            cekEceranAtauGrosir();

            // (id !== '') ? $('#pilihanPotonganContainer').show('fast') : $('#pilihanPotonganContainer').hide('fast')

            var titipan = pelanggan == null || pelanggan.titipan == null ? '' : pelanggan.titipan;
            titipan = parseFloat(titipan.replace(/\D/g, ''), 10) / 100;
            if (isNaN(titipan)) titipan = 0;

            $('input[name="pelanggan"]').val(id);
            $('input[name="titipan"]').val(titipan);
            updateHargaOnKeyup();

            $('#inputJumlahItem1').trigger('keyup');
        });

        $(document).on('click', '#resetPelanggan', function(event) {
            event.preventDefault();

            pelanggan = null;
            $('select[name="pelanggan_id"]').val('');
            $('select[name="pelanggan_id"]').trigger('change');
            $('input[name="kode_pelanggan"]').val('');
            $('input[name="kode_pelanggan"]').removeClass('has-error');
            $('input[name="nama_pelanggan"]').val('');
            $('#inputNamaPelanggan').prop('readonly', false);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputKodePelanggan', function(event) {
            event.preventDefault();

            var ada_yang_sama = false;
            var kode_pelanggan = $(this).val();

            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }

            for (var i = 0; i < pelanggans.length; i++) {
                var pelanggan = pelanggans[i];
                if (kode_pelanggan == pelanggan.kode) {
                    ada_yang_sama = true;
                    break;
                }
            }

            if (ada_yang_sama) {
                $(this).parents('.form-group').addClass('has-error');
            } else {
                $(this).parents('.form-group').removeClass('has-error');
                $(this).val(kode_pelanggan);
                $('input[name="kode_pelanggan"]').val(kode_pelanggan);
            }
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNamaPelanggan', function(event) {
            event.preventDefault();

            var nama_pelanggan = $(this).val();
            $('input[name="nama_pelanggan"]').val(nama_pelanggan);
            
            if ($('select[name="pelanggan_id"]').val() != '') {
                $('select[name="pelanggan_id"]').val('');
                $('select[name="pelanggan_id"]').trigger('change');
            }
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="item_id"]', function(event) {
            event.preventDefault();

            var kode = $(this).val();
            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/item/json';
            var tr = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (kode == '') {
                $('#tabelInfo tbody').empty();
            } else if (!selected_items.includes(kode)) {
                selected_items.push(kode);
                $('#spinner').show();
                cariItem(kode, url, tr);
                $('select[name="item_id"]').val('').change();
            }

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');
        });

        /*var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah);
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();

            var jumlah = $(this).val();
            temp_jumlah = jumlah;

            $tr = $(this).parents('tr').first();

            var kode = $(this).parents('tr').first().data('id');
            var satuan = $('#satuan-'+kode).val();
            var konversi = $('#konversi-'+kode).val();
            var stoktotal = $('#stoktotal-'+kode).val();
            
            var td = $(this).parents('td').first();

            if (jumlah == '') {
                jumlah = 0;
                td.addClass('has-error');
            } else {
                td.removeClass('has-error');
            }

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            satuan = parseFloat(satuan);
            if (isNaN(satuan) || satuan <= 0) satuan = 0;
            konversi = parseFloat(konversi);
            if (isNaN(konversi) || konversi <= 0) konversi = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            $.get(url, function(data) {
                // console.log(data);
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var jumlahtotal = jumlah * konversi;
                    var limit_grosir = data.limit_grosir;
                    
                    if (jumlahtotal <= stoktotal) {
                        // stok mencukupi
                        td.removeClass('has-error');
                        td.next().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);
                        
                        if (limit_grosir == null) 
                            $('#is_grosir-'+kode).val(false);
                        else $('#is_grosir-'+kode).val(jumlahtotal >= limit_grosir);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        var bonus = data.bonus;
                        if (bonus.length > 0) {
                            // ada bonus
                            var text_bonus = '';
                            for (var i = 0; i < bonus.length; i++) {
                                text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                if (i != bonus.length - 1) text_bonus += ', '
                            }
                            $tr.find('#bonusContainer').text(text_bonus);
                        } else {
                            // tidak ada bonus
                            $tr.find('#bonusContainer').text('Tidak ada');
                        }

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#jumlah-'+kode).val(jumlah);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        var temp_jumlah_1 = 0;
        $(document).on('click', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_1 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah_1);
        });

        $(document).on('keyup', '#inputJumlahItem1', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_1 = $(this).val();

            var jumlah1 = parseFloat($(this).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah1-'+item_kode).val(jumlah1);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = pelanggan_is_grosir ? parseFloat(harga_darurat.grosir) : parseFloat(harga_darurat.eceran);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;

                            var harga1 = 0;
                            var harga2 = 0;

                            if (pelanggan_is_grosir) {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.grosir);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.grosir);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            } else {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.eceran);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.eceran);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            if (pelanggan_is_grosir) {
                                updateHargaKeGrosir();
                            } else {
                                updateHargaKeEceran();
                            }

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stoktotal) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            // console.log(nego_min);
                            nego_min = Math.round(nego_min);
                            // console.log(nego_min);
                            nego_min = Math.ceil(nego_min / 100) * 100;
                            // console.log(nego_min);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        var temp_jumlah_2 = 0;
        $(document).on('click', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');
            
            $('#checkNegoTotal').prop('checked', false);
            $('#inputNegoTotal').val('');
            $('#checkNegoTotal').trigger('change');

            temp_jumlah_2 = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            $(this).val(temp_jumlah_2);
        });

        $(document).on('keyup', '#inputJumlahItem2', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var td = $(this).parents('#inputJumlahItemContainer');

            temp_jumlah_2 = $(this).val();

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($(this).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            // if (jumlah2 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#jumlah2-'+item_kode).val(jumlah2);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga === null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = pelanggan_is_grosir ? parseFloat(harga_darurat.grosir) : parseFloat(harga_darurat.eceran);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;

                            var harga1 = 0;
                            var harga2 = 0;

                            if (pelanggan_is_grosir) {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.grosir);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.grosir);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            } else {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.eceran);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.eceran);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            if (pelanggan_is_grosir) {
                                updateHargaKeGrosir();
                            } else {
                                updateHargaKeEceran();
                            }

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stoktotal) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            // console.log(nego_min);
                            nego_min = Math.round(nego_min);
                            // console.log(nego_min);
                            nego_min = Math.ceil(nego_min / 100) * 100;
                            // console.log(nego_min);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            td.addClass('has-error');
                        }
                    }
                });
            // }
        });

        $(document).on('click', '#pilihSatuan1 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan1').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($(this).find('a').attr('konversi'));
            var konversi2 = parseFloat($('#konversi2-'+item_kode).val());
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah1 > 0) {
                var satuan1 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('id')),
                    kode: td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan2').find('a[konversi="'+konversi2+'"]').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan1-'+item_kode).val(satuan1.id);
                $('#konversi1-'+item_kode).val(satuan1.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // Jika ada harga yang dinolkan di database
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);

                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = pelanggan_is_grosir ? parseFloat(harga_darurat.grosir) : parseFloat(harga_darurat.eceran);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;

                            var harga1 = 0;
                            var harga2 = 0;

                            if (pelanggan_is_grosir) {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.grosir);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.grosir);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            } else {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.eceran);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.eceran);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            if (pelanggan_is_grosir) {
                                updateHargaKeGrosir();
                            } else {
                                updateHargaKeEceran();
                            }

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stoktotal) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            // console.log(nego_min);
                            nego_min = Math.round(nego_min);
                            // console.log(nego_min);
                            nego_min = Math.ceil(nego_min / 100) * 100;
                            // console.log(nego_min);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan1.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('input[name="harga_total"]').val(harga_total);
                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            button.text(satuan1.kode+' ');
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        $(document).on('click', '#pilihSatuan2 li', function(event) {
            event.preventDefault();

            var tr = $(this).parents('tr').first();
            var item_kode = tr.data('id');
            var button = $(this).parents('#pilihSatuan2').find('button').find('.text');
            var td = $(this).parents('#inputJumlahItemContainer');

            var jumlah1 = parseFloat($('#jumlah1-'+item_kode).val());
            var jumlah2 = parseFloat($('#jumlah2-'+item_kode).val());
            var konversi1 = parseFloat($('#konversi1-'+item_kode).val());
            var konversi2 = parseFloat($(this).find('a').attr('konversi'));
            var stoktotal = parseFloat($('#stoktotal-'+item_kode).val());

            if (isNaN(jumlah1) || jumlah1 < 0) jumlah1 = 0;
            if (isNaN(jumlah2) || jumlah2 < 0) jumlah2 = 0;
            if (isNaN(konversi1) || konversi1 < 0) konversi1 = 0;
            if (isNaN(konversi2) || konversi2 < 0) konversi2 = 0;
            if (isNaN(stoktotal) || stoktotal < 0) stoktotal = 0;

            if (jumlah2 > 0) {
                var satuan1 = {
                    id: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('id')),
                    kode: td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('kode'),
                    konversi: parseInt(td.find('#pilihSatuan1').find('a[konversi="'+konversi1+'"]').attr('konversi'))
                };
                var satuan2 = {
                    id: parseInt($(this).find('a').attr('id')),
                    kode: $(this).find('a').attr('kode'),
                    konversi: parseInt($(this).find('a').attr('konversi'))
                };

                var jumlah = jumlah1 * konversi1 + jumlah2 * konversi2;
                var url = "{{ url('transaksi-grosir') }}"+'/'+item_kode+'/harga/json/'+satuan1.id+'/'+jumlah1+'/'+satuan2.id+'/'+jumlah2+'/'+konversi1+'/'+konversi2;

                $('#satuan2-'+item_kode).val(satuan2.id);
                $('#konversi2-'+item_kode).val(satuan2.konversi);
                $.get(url, function(data) {
                    // console.log(data);
                    if (data.harga == null) {
                        tr.find('#inputHargaPerSatuan').val(0);
                        tr.find('#inputSubTotal').val(0);
                        tr.find('#inputNego').val(0);
                        td.addClass('has-error');
                        updateHargaOnKeyup();
                    } else {
                        var jumlahtotal = jumlah;
                        var limit_grosir = data.limit_grosir;

                        if (jumlahtotal <= stoktotal) {
                            // stok mencukupi
                            var harga_darurat = null;
                            var konversi_darurat = 0;
                            td.removeClass('has-error');
                            if (
                                    (
                                        (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) && jumlah1 > 0
                                    ) || 
                                    (
                                        (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) && jumlah2 > 0
                                    )
                                ) {
                                // console.log('oi', data.harga.eceran, data.harga.grosir);
                                td.addClass('has-error');

                                // Jika yang dinolkan adalah harga yang lebih tinggi
                                if (konversi1 > konversi2) {
                                    if (
                                            data.harga1 == null || 
                                            parseFloat(data.harga1.eceran) <= 0 || 
                                            parseFloat(data.harga1.grosir) <= 0
                                        ) {
                                        harga_darurat = data.harga2_darurat;
                                        konversi_darurat = konversi2;
                                        td.removeClass('has-error');
                                    }
                                } else if (konversi1 < konversi2) {
                                    if (
                                            data.harga2 == null || 
                                            parseFloat(data.harga2.eceran) <= 0 || 
                                            parseFloat(data.harga2.grosir) <= 0
                                        ) {
                                        // harga_darurat = data.harga1_darurat;
                                        // konversi_darurat = konversi1;
                                        // td.removeClass('has-error');
                                    }
                                } else {
                                    if (data.harga1_darurat.eceran <= 0 || data.harga1_darurat.grosir <= 0) {
                                        td.addClass('has-error');
                                    } else {
                                        td.removeClass('has-error');
                                    }
                                }
                            }

                            var item_is_grosir = false;
                            if (limit_grosir != null && limit_grosir > 0 && jumlahtotal >= limit_grosir) item_is_grosir = true;
                            $('#is_grosir-'+item_kode).val(item_is_grosir);

                            var pelanggan_is_grosir = false;
                            var pelanggan_id = $('select[name="pelanggan_id"]').val();
                            var temp_pelanggan = null;
                            for (var i = 0; i < pelanggans.length; i++) {
                                if (pelanggans[i].id == pelanggan_id) {
                                    temp_pelanggan = pelanggans[i];
                                }
                            }
                            if (temp_pelanggan == null || temp_pelanggan.level == 'eceran') {
                                var pelanggan_is_grosir = false;
                            } else {
                                var pelanggan_is_grosir = true;
                            }

                            // var harga = item_is_grosir && pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                            var harga = 0;
                            var konversi = 0;
                            var harga_eceran = 0;
                            var harga_grosir = 0;
                            if (harga_darurat != null) {
                                harga = pelanggan_is_grosir ? parseFloat(harga_darurat.grosir) : parseFloat(harga_darurat.eceran);
                                konversi = konversi_darurat;
                                harga_eceran = parseFloat(harga_darurat.eceran);
                                harga_grosir = parseFloat(harga_darurat.grosir);
                            } else {
                                harga = pelanggan_is_grosir ? parseFloat(data.harga.grosir) : parseFloat(data.harga.eceran);
                                konversi = parseFloat(data.konversi);
                                harga_eceran = parseFloat(data.harga_eceran);
                                harga_grosir = parseFloat(data.harga_grosir);
                            }
                            $('#konversi-'+item_kode).val(konversi);
                            $('#harga-'+item_kode).val(harga);
                            $('#harga_eceran-'+item_kode).val(harga_eceran);
                            $('#harga_grosir-'+item_kode).val(harga_grosir);

                            cekEceranAtauGrosir();
                            // var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;

                            var harga1 = 0;
                            var harga2 = 0;

                            if (pelanggan_is_grosir) {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.grosir);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.grosir);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            } else {
                                if (data.harga1 != null) {
                                    harga1 = parseFloat(data.harga1.eceran);
                                    if (isNaN(harga1)) harga1 = 0;
                                }
                                if (data.harga2 != null) {
                                    harga2 = parseFloat(data.harga2.eceran);
                                    if (isNaN(harga2)) harga2 = 0;
                                }
                            }

                            $('#harga1-'+item_kode).val(harga1);
                            $('#harga2-'+item_kode).val(harga2);
                            if (data.harga1 != null) {
                                $('#harga1_eceran-'+item_kode).val(data.harga1.eceran);
                                $('#harga1_grosir-'+item_kode).val(data.harga1.grosir);
                            }
                            if (data.harga2 != null) {
                                $('#harga2_eceran-'+item_kode).val(data.harga2.eceran);
                                $('#harga2_grosir-'+item_kode).val(data.harga2.grosir);
                            }
                            if (pelanggan_is_grosir) {
                                updateHargaKeGrosir();
                            } else {
                                updateHargaKeEceran();
                            }

                            var bonus = data.bonus;
                            if (bonus.length > 0) {
                                // ada bonus
                                var text_bonus = '';
                                for (var i = 0; i < bonus.length; i++) {
                                    if (bonus[i].jumlah <= bonus[i].bonus.stoktotal) {
                                        text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                        if (i != bonus.length - 1) text_bonus += ', ';
                                    } else {
                                        // tidak ada bonus
                                        text_bonus = 'Tidak ada';
                                    }
                                }
                                tr.find('#bonusContainer').text(text_bonus);
                            } else {
                                // tidak ada bonus
                                tr.find('#bonusContainer').text('Tidak ada');
                            }

                            var hpp = parseFloat(data.hpp.harga);
                            // var nego_min = parseFloat(data.nego_min);
                            var nego_min = Math.round(hpp * 110 * (1 + (data.item.profit / 100)) * jumlahtotal) / 100;
                            // console.log(nego_min);
                            nego_min = Math.round(nego_min);
                            // console.log(nego_min);
                            nego_min = Math.ceil(nego_min / 100) * 100;
                            // console.log(nego_min);
                            $('#nego_min-'+item_kode).val(nego_min);
                            tr.find('#inputNegoMin').val(nego_min);

                            button.text(satuan2.kode+' ');

                            // var harga_total = 0;
                            // $('.subtotal').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val().replace(/\D/g, ''), 10);
                            //     if (isNaN(tmp)) tmp = 0;
                            //     harga_total += tmp;
                            // });

                            // var nego_total_min = 0;
                            // $('input[name="inputNegoMin"]').each(function(index, el) {
                            //     var tmp = parseFloat($(el).val());
                            //     if (isNaN(tmp)) tmp = 0;
                            //     nego_total_min += tmp;
                            // });

                            // $('#inputHargaTotal').val(harga_total.toLocaleString());
                            // $('#inputNegoTotalMin').val(nego_total_min);
                            // $('input[name="harga_total"]').val(harga_total);

                            // var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
                            // var kembali = jumlah_bayar - harga_total;

                            // if (kembali < 0 || isNaN(kembali)) {
                            //     kembali = 0;
                            //     $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                            // } else {
                            //     $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                            // }

                            // $('#inputTotalKembali').val(kembali.toLocaleString());
                            // $('input[name="kembali"]').val(kembali);

                            updateHargaOnKeyup();
                        } else {
                            button.text(satuan2.kode+' ');
                            td.addClass('has-error');
                        }
                    }
                });
            }
        });

        /*$(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();

            var satuan = $(this).val();

            var $tr = $(this).parents('tr').first();
            $tr.find('#checkNego').prop('checked', false);
            $tr.find('#inputNego').val('');
            $tr.find('#checkNego').trigger('change');

            var kode = $(this).parents('tr').data('id');
            var stoktotal = $('#stoktotal-'+kode).val();
            var jumlah = $('#jumlah-'+kode).val();
            // var jumlah = $(this).parent().prev().children('input').val();

            if (jumlah === '') jumlah = 0;

            jumlah = parseFloat(jumlah.replace(/\D/g, ''), 10);
            if (isNaN(jumlah) || jumlah <= 0) jumlah = 0;
            stoktotal = parseFloat(stoktotal);
            if (isNaN(stoktotal) || stoktotal <= 0) stoktotal = 0;

            var url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/harga/json/'+satuan+'/'+jumlah;
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var td = $(this).parents('td').first();

            td.prev().find('#inputJumlahItem1').val(jumlah.toLocaleString());

            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#inputNego').val(0);
                } else {
                    var konversi = data.konversi.konversi;
                    konversi = parseFloat(konversi);
                    if (isNaN(konversi) || konversi <= 0) konversi = 0;

                    var jumlahtotal = jumlah * konversi;
                    var limit_grosir = data.limit_grosir;
                    
                    if (jumlahtotal <= stoktotal) {
                        // stok mencukupi
                        td.removeClass('has-error');
                        td.prev().removeClass('has-error');

                        $('#jumlah-'+kode).val(jumlah);
                        $('#satuan-'+kode).val(satuan);
                        $('#konversi-'+kode).val(konversi);
                        
                        if (limit_grosir == null) 
                            $('#is_grosir-'+kode).val(false);
                        else $('#is_grosir-'+kode).val(jumlahtotal >= limit_grosir);

                        var harga = data.harga.harga;
                        harga = parseFloat(harga.replace(/\D/g, ''), 10) / 100;
                        var subtotal = harga * jumlah;

                        var bonus = data.bonus;
                        if (bonus.length > 0) {
                            // ada bonus
                            var text_bonus = '';
                            for (var i = 0; i < bonus.length; i++) {
                                text_bonus += bonus[i].jumlah+' '+bonus[i].bonus.nama;
                                if (i != bonus.length - 1) text_bonus += ', '
                            }
                            $tr.find('#bonusContainer').text(text_bonus);
                        } else {
                            // tidak ada bonus
                            $tr.find('#bonusContainer').text('Tidak ada');
                        }

                        // var konversi = parseInt(data.konversi.konversi) * parseInt(jumlah);
                        var nego_min = parseFloat(data.nego_min);

                        tr.find('#inputHargaPerSatuan').val(harga.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputSubTotal').val(subtotal.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        tr.find('#inputNegoMin').val(nego_min);

                        $('#satuan-'+kode).val(satuan);
                        $('#harga-'+kode).val(parseInt(harga));
                        $('#subtotal-'+kode).val(subtotal);

                        var harga_total = 0;
                        $('.subtotal').each(function(index, el) {
                            var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                            if (isNaN(tmp)) tmp = 0;
                            harga_total += tmp;
                        });

                        var nego_total_min = 0;
                        $('input[name="inputNegoMin"]').each(function(index, el) {
                            var tmp = parseFloat($(el).val());
                            if (isNaN(tmp)) tmp = 0;
                            nego_total_min += tmp;
                        });

                        $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('#inputNegoTotalMin').val(nego_total_min);
                        $('input[name="harga_total"]').val(harga_total);

                        var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10) / 100;
                        var harga_total = parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10) / 100;
                        var kembali = jumlah_bayar - harga_total;
                        
                        if (kembali < 0 || isNaN(kembali)) {
                            kembali = 0;
                            $('#form-simpan').find('button[type="submit"]').attr('disabled', 'disabled');
                        } else {
                            $('#form-simpan').find('button[type="submit"]').removeAttr('disabled');
                        }

                        $('#inputTotalKembali').val(kembali.toLocaleString(undefined, {minimumFractionDigits: 2}));
                        $('input[name="kembali"]').val(kembali);
                        
                        updateHargaOnKeyup();

                        // var hpp_url = "{{ url('transaksi-grosir') }}"+'/'+kode+'/hpp/json';
                        // $.get(hpp_url, function(data) {
                        //  var arr   = [];
                        //  var index = 0;
                        //  var hpp   = 0;

                        //  for (var i = 0; i < data.stoks.length; i++) {
                        //      arr.push([data.stoks[i].jumlah, data.stoks[i].harga]);
                        //  }

                        //  for (var j = 0; j < parseInt(konversi); j++) {
                        //      arr[index][0] -= 1;
                        //      hpp += parseInt(arr[index][1]);

                        //      if (arr[index][0] === 0) index += 1;        
                        //  }

                        //  $('#hpp-'+kode).val(hpp);
                        // });
                    } else {
                        td.addClass('has-error');
                    }
                }
            });
        });*/

        $(document).on('change', '#checkNego', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            var kode = $(this).parents('tr').first().data('id');
            var harga_total = 0;

            if (checked) {
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').prop('readonly', false).focus();

                var nego = $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val();
                var nego_min = $(this).parents('tr[data-id="'+kode+'"]').find('#inputNegoMin').val();
                var jumlah = $(this).parents('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').val();

                nego = parseFloat(nego.replace(/\D/g, ''), 10);

                // Success
                if (nego >= nego_min) {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('#nego-'+kode).val(nego);

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                } else {
                    $(this).parents('.form-group').addClass('has-error');
                    $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val('');
                    $('#nego-'+kode).val('');

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                }

                updateHargaOnKeyup();
            } else {
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').val('');
                $(this).parents('tr[data-id="'+kode+'"]').find('#inputNego').prop('readonly', true);
                $(this).parents('.form-group').removeClass('has-error');

                // var subtotal = $(this).parents('tr[data-id="'+kode+'"]').find('#inputSubTotal').val();
                // subtotal = parseFloat(subtotal.replace(/\D/g, ''), 10) / 100;
                // $('#subtotal-'+kode).val(subtotal);
                $('#nego-'+kode).val('');
                // updateHargaTotal();
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNego', function(event) {
            event.preventDefault();

            var input = $(this);
            var nego = input.val();
            nego = parseFloat(nego.replace(/\D/g, ''), 10);
            var kode = input.parents('tr').data('id');
            var is_nego_ratusan = nego % 100 == 0;
            if (is_nego_ratusan) {
                var nego_min = input.parents('tr[data-id="'+kode+'"]').find('#inputNegoMin').val();
                var jumlah = input.parents('tr[data-id="'+kode+'"]').find('#inputJumlahItem1').val();

                // nego = parseFloat(nego.replace(/\D/g, ''), 10);
                nego_min = parseFloat(nego_min.replace(/\D/g, ''), 10);

                // Success
                if (nego >= nego_min) {
                    input.parents('.form-group').removeClass('has-error');
                    // $('#subtotal-'+kode).val(nego);
                    $('#nego-'+kode).val(nego);

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                } else {
                    input.parents('.form-group').addClass('has-error');
                    // var subtotal = input.parents('tr[data-id="'+kode+'"]').find('#inputSubTotal').val();
                    // subtotal = parseFloat(subtotal.replace(/\D/g, ''), 10) / 100;
                    // $('#subtotal-'+kode).val(subtotal);
                    $('#nego-'+kode).val('');

                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                }

                updateHargaOnKeyup();
            } else {
                input.parents('.form-group').addClass('has-error');
                // var subtotal = input.parents('tr[data-id="'+kode+'"]').find('#inputSubTotal').val();
                // subtotal = parseFloat(subtotal.replace(/\D/g, ''), 10) / 100;
                // $('#subtotal-'+kode).val(subtotal);
                $('#nego-'+kode).val('');

                // updateHargaTotal();
                // updateHargaOnKeyup();

                $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
            }
        });

        $(document).on('click', '#btnPotonganPersen', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).next().val(true);

                $(this).parent().next().find('button').removeClass('btn-primary');
                $(this).parent().next().find('button').addClass('btn-default');
                $(this).parent().next().find('button').next().val(false);

                $.get(url, function(data) {
                    var persen = data.pelanggan.diskon_persen;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total = harga_total - (harga_total * (persen/100));

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('click', '#btnPotonganTunai', function(event) {
            event.preventDefault();

            var pelanggan   = $('input[name="pelanggan"]').val();
            var url         = "{{ url('transaksi-grosir') }}"+'/'+pelanggan+'/pelanggan/json';
            var harga_total = 0;
            
            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).next().val(true);

                $(this).parent().prev().find('button').removeClass('btn-danger');
                $(this).parent().prev().find('button').addClass('btn-default');
                $(this).parent().prev().find('button').next().val(false);

                $.get(url, function(data) {
                    var potongan = data.pelanggan.potongan;

                    harga_total = $('#hiddenHargaTotal').val();
                    harga_total -= potongan;

                    $('#inputHargaTotal').val(harga_total.toLocaleString());
                    $('input[name="harga_total"]').val(harga_total);
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).next().val(false);

                harga_total = parseInt($('#hiddenHargaTotal').val().replace(/\D/g, ''), 10);

                $('#inputHargaTotal').val(harga_total.toLocaleString());
                $('input[name="harga_total"]').val(harga_total);
            }
        });

        $(document).on('change', '#checkNegoTotal', function(event) {
            event.preventDefault();

            var checked = $(this).prop('checked');
            if (checked) {
                $('input[name="check_nego_total"]').val(1);
                $('#inputNegoTotal').prop('readonly', false).focus();

                var nego_total = $('#inputNegoTotal').val();
                var nego_total_min = $('#inputNegoTotalMin').val();

                nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
                if (isNaN(nego_total)) nego_total = 0;

                nego_total_min = parseFloat(nego_total_min);
                if (isNaN(nego_total_min)) nego_total_min = 0;

                // Success
                if (nego_total >= nego_total_min) {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('input[name="nego_total"]').val(nego_total);
                    // updateHargaOnKeyup();
                } else {
                    $(this).parents('.form-group').addClass('has-error');
                    // $('input[name="nego_total"]').val('');
                    // updateHargaTotal();
                    // updateHargaOnKeyup();
                }

                updateHargaOnKeyup();
            } else {
                $('input[name="check_nego_total"]').val(0);
                $('#inputNegoTotal').val('');
                $('#inputNegoTotal').prop('readonly', true);
                $(this).parents('.form-group').removeClass('has-error');

                // var harga_total = $('#inputHargaTotal').val();
                // harga_total = parseFloat(harga_total.replace(/\D/g, ''), 10);
                // $('input[name="nego_total"]').val('');
                $('input[name="nego_total_view"]').val('');
                // updateHargaTotal();
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNegoTotal', function(event) {
            event.preventDefault();

            var nego_total = $('#inputNegoTotal').val();
            var nego_total_min = $('#inputNegoTotalMin').val();

            nego_total = parseFloat(nego_total.replace(/\D/g, ''), 10);
            if (isNaN(nego_total)) nego_total = 0;

            nego_total_min = parseFloat(nego_total_min);
            if (isNaN(nego_total_min)) nego_total_min = 0;

            var is_nego_ratusan = nego_total % 100 == 0;
            if (is_nego_ratusan) {
                // Success
                if (nego_total >= nego_total_min) {
                    // $('#labelLabaTotal').show();
                    // $('#labelRugiTotal').hide();
                    $(this).parents('.form-group').removeClass('has-error');
                    // $(this).parents('.form-group').addClass('has-success');
                    $('input[name="nego_total_view"]').val(nego_total);
                } else {
                    // $('#labelLabaTotal').hide();
                    // $('#labelRugiTotal').show();
                    // $(this).parents('.form-group').removeClass('has-success');
                    $(this).parents('.form-group').addClass('has-error');
                    $('input[name="nego_total_view"]').val('');
                }

                // $('input[name="nego_total_view"]').val(nego_total);
                updateHargaOnKeyup();
            } else {
                // $('#labelLabaTotal').hide();
                // $('#labelRugiTotal').show();
                // $(this).parents('.form-group').removeClass('has-success');
                $(this).parents('.form-group').addClass('has-error');
                $('input[name="nego_total_view"]').val('');

                $('#btnSimpanPO').prop('disabled', isBtnSimpanPODisabled());
            }
        });

        /*$(document).on('keyup', '#inputNegoTotal', function(event) {
            event.preventDefault();

            var input = $(this);
            var nego_total_view = input.val();
            var nego_total_min = $('#inputNegoTotalMin').val();
            var nego_total_data = $('input[name="nego_total"]').val();

            nego_total_view = parseFloat(nego_total_view.replace(/\D/g, ''), 10);
            nego_total_min = parseFloat(nego_total_min);
            nego_total_data = parseFloat(nego_total_data);
            
            // Success
            if (nego_total_view >= nego_total_min) {
                if (nego_total_data > 0) {
                    if (nego_total_view < nego_total_data) {
                        $(this).parents('.form-group').removeClass('has-error');
                        $('input[name="nego_total"]').val(nego_total_view);
                        updateHargaOnKeyup();
                    } else {
                        $(this).parents('.form-group').removeClass('has-error');
                        $('input[name="nego_total"]').val(nego_total_data);
                        updateHargaOnKeyup();
                    }
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                    $('input[name="nego_total"]').val(nego_total_view);
                    updateHargaOnKeyup();
                }
            } else {
                $(this).parents('.form-group').addClass('has-error');
                // $('input[name="nego_total"]').val('');
                updateHargaTotal();
                updateHargaOnKeyup();
            }
        });*/

        $(document).on('keyup', '#inputOngkosKirim', function(event) {
            event.preventDefault();

            var ongkos_kirim = $(this).val();
            $('input[name="ongkos_kirim"]').val(ongkos_kirim);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnTunai', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTunaiContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTunaiContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_tunai"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTransfer', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-warning');
                $(this).find('i').addClass('fa-check');
                $('#inputTransferBankContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-warning')) {
                $(this).removeClass('btn-warning');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputTransferBankContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_transfer"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_transfer"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnCek', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputCekContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).find('i').removeClass('fa-check');
                $(this).addClass('btn-default');
                $('#inputCekContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_cek"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_cek"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnBG', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
                $('#inputBGContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-primary')) {
                $(this).removeClass('btn-primary');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputBGContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_bg"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_bg"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnKredit', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-info');
                $(this).find('i').addClass('fa-check');
                $('#inputKreditContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-info')) {
                $(this).removeClass('btn-info');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputKreditContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="no_kredit"]').val('');
                    $('#formSimpanContainer').find('input[name="nominal_kredit"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnTitipan', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-danger');
                $(this).find('i').addClass('fa-check');
                $('#inputTitipanContainer').show('fast', function() {
                    $(this).find('input').first().trigger('focus');
                });
            } else if ($(this).hasClass('btn-danger')) {
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-default');
                $(this).find('i').removeClass('fa-check');
                $('#inputTitipanContainer').hide('hide', function() {
                    $('#formSimpanContainer').find('input[name="nominal_titipan"]').val('');
                    $(this).find('input').val('');
                    updateHargaOnKeyup();
                });
            }
        });

        $(document).on('click', '#btnDiambil', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirmContainer').hide('fast', function() {
                    $('select[name="user_id"]').select2('val', '');
                    $('select[name="user_id"]').val('').trigger('change');
                    $('#inputPengirimLain').val('');
                    $('#formSimpanContainer').find('input[name="pengirim"]').val('');
                    $('#formSimpanContainer').find('input[name="pengirim_lain"]').val('');
                    updateHargaOnKeyup();
                });
            }
            $('#inputOngkosKirim').val('');
            $('#inputOngkosKirim').prop('readonly', true);
            $('#btnDikirim').removeClass('btn-primary');
            $('#btnDikirim').addClass('btn-default');
            $('#btnDikirim').find('i').removeClass('fa-check');
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnDikirim', function(event) {
            event.preventDefault();

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
                $('#inputDikirmContainer').show('fast', function() {
                    updateHargaOnKeyup();
                });
            }
            $('#inputOngkosKirim').prop('readonly', false);
            $('#btnDiambil').removeClass('btn-success');
            $('#btnDiambil').addClass('btn-default');
            $('#btnDiambil').find('i').removeClass('fa-check');
        });

        $(document).on('change', 'select[name="user_id"]', function(event) {
            event.preventDefault();

            var user_id = $(this).val();
            if (user_id == '-') {
                $('#inputPengirimLain').prop('disabled', false);
                $('input[name="pengirim"]').val('');
            } else {
                $('#inputPengirimLain').prop('disabled', true);
                $('#inputPengirimLain').val('');
                $('input[name="pengirim"]').val(user_id);
                $('input[name="pengirim_lain"]').val('');
            }

            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputPengirimLain', function(event) {
            event.preventDefault();

            var pengirim_lain = $(this).val();
            $('input[name="pengirim_lain"]').val(pengirim_lain);
            updateHargaOnKeyup();
        });

        $(document).on('click', '#btnAlamatBiasa', function(event) {
            event.preventDefault();

            var alamat_biasa = '';
            if (pelanggan != null) alamat_biasa = pelanggan.alamat;
            $('#inputAlamat').prop('disabled', true);
            $('#inputAlamat').val(alamat_biasa);
            $('input[name="alamat_lain"]').val('');

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-success');
                $(this).find('i').addClass('fa-check');
            }
            $('#btnAlamatLain').removeClass('btn-primary');
            $('#btnAlamatLain').addClass('btn-default');
            $('#btnAlamatLain').find('i').removeClass('fa-check');
        });

        $(document).on('click', '#btnAlamatLain', function(event) {
            event.preventDefault();

            var alamat_lain = '';
            $('#inputAlamat').prop('disabled', false);
            $('#inputAlamat').val(alamat_lain).trigger('focus');
            $('input[name="alamat_lain"]').val(alamat_lain);

            if ($(this).hasClass('btn-default')) {
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                $(this).find('i').addClass('fa-check');
            }
            $('#btnAlamatBiasa').removeClass('btn-success');
            $('#btnAlamatBiasa').addClass('btn-default');
            $('#btnAlamatBiasa').find('i').removeClass('fa-check');
        });

        $(document).on('keyup', '#inputAlamat', function(event) {
            event.preventDefault();

            var alamat_lain = $(this).val();
            $('input[name="alamat_lain"]').val(alamat_lain);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTunai', function(event) {
            event.preventDefault();

            var nominal_tunai = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_tunai)) nominal_tunai = 0;

            $(this).val(nominal_tunai.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_tunai"]').val(nominal_tunai);
            updateHargaOnKeyup();
        });

        $(document).on('change', 'select[name="bank_id"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            $('input[name="bank_id"]').val(id);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNoTransfer', function(event) {
            event.preventDefault();

            var no_transfer = $(this).val();
            $('input[name="no_transfer"]').val(no_transfer);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalTransfrer', function(event) {
            event.preventDefault();

            var nominal_transfer = parseFloat($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_transfer)) nominal_transfer = 0;

            $(this).val(nominal_transfer.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
            updateHargaOnKeyup();

            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(kembali)) kembali = 0;

            if (nominal_tunai <= 0 && kembali > 0) {
                nominal_transfer += '';
                nominal_transfer = nominal_transfer.slice(0, -1);
                nominal_transfer = parseFloat(nominal_transfer);
                if (isNaN(nominal_transfer)) nominal_transfer = 0;

                $(this).val(nominal_transfer.toLocaleString());
                $('#formSimpanContainer').find('input[name="nominal_transfer"]').val(nominal_transfer);
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNoCek', function(event) {
            event.preventDefault();

            var no_cek = $(this).val();
            $('input[name="no_cek"]').val(no_cek);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalCek', function(event) {
            event.preventDefault();

            var nominal_cek = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_cek)) nominal_cek = 0;

            $(this).val(nominal_cek.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
            updateHargaOnKeyup();

            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(kembali)) kembali = 0;

            if (nominal_tunai <= 0 && kembali > 0) {
                nominal_cek += '';
                nominal_cek = nominal_cek.slice(0, -1);
                nominal_cek = parseFloat(nominal_cek);
                if (isNaN(nominal_cek)) nominal_cek = 0;

                $(this).val(nominal_cek.toLocaleString());
                $('#formSimpanContainer').find('input[name="nominal_cek"]').val(nominal_cek);
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNoBG', function(event) {
            event.preventDefault();

            var no_bg = $(this).val();
            $('input[name="no_bg"]').val(no_bg);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalBG', function(event) {
            event.preventDefault();

            var nominal_bg = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_bg)) nominal_bg = 0;

            $(this).val(nominal_bg.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
            updateHargaOnKeyup();

            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(kembali)) kembali = 0;

            if (nominal_tunai <= 0 && kembali > 0) {
                nominal_bg += '';
                nominal_bg = nominal_bg.slice(0, -1);
                nominal_bg = parseFloat(nominal_bg);
                if (isNaN(nominal_bg)) nominal_bg = 0;

                $(this).val(nominal_bg.toLocaleString());
                $('#formSimpanContainer').find('input[name="nominal_bg"]').val(nominal_bg);
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNoKredit', function(event) {
            event.preventDefault();

            var no_kredit = $(this).val();
            $('input[name="no_kredit"]').val(no_kredit);
            updateHargaOnKeyup();
        });

        $(document).on('keyup', '#inputNominalKredit', function(event) {
            event.preventDefault();

            var nominal_kredit = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (isNaN(nominal_kredit)) nominal_kredit = 0;

            $(this).val(nominal_kredit.toLocaleString());
            $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(nominal_kredit);
            updateHargaOnKeyup();

            var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
            var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(kembali)) kembali = 0;

            if (nominal_tunai <= 0 && kembali > 0) {
                nominal_kredit += '';
                nominal_kredit = nominal_kredit.slice(0, -1);
                nominal_kredit = parseFloat(nominal_kredit);
                if (isNaN(nominal_kredit)) nominal_kredit = 0;

                $(this).val(nominal_kredit.toLocaleString());
                $('#formSimpanContainer').find('input[name="nominal_kredit"]').val(nominal_kredit);
                updateHargaOnKeyup();
            }
        });

        $(document).on('keyup', '#inputNominalTitipan', function(event) {
            event.preventDefault();

            var nominal_titipan = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_titipan_max = parseFloat($('input[name="titipan"]').val());

            if (isNaN(nominal_titipan)) nominal_titipan = 0;
            if (isNaN(nominal_titipan_max)) nominal_titipan_max = 0;

            if (nominal_titipan < nominal_titipan_max) {
                $(this).parents('.input-group').first().removeClass('has-error');
                $(this).val(nominal_titipan.toLocaleString());
                $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(nominal_titipan);
                updateHargaOnKeyup();

                var nominal_tunai = parseFloat($('input[name="nominal_tunai"]').val().replace(/\D/g, ''), 10);
                var kembali = parseFloat($('input[name="kembali"]').val().replace(/\D/g, ''), 10);

                if (isNaN(nominal_tunai)) nominal_tunai = 0;
                if (isNaN(kembali)) kembali = 0;

                if (nominal_tunai <= 0 && kembali > 0) {
                    nominal_titipan += '';
                    nominal_titipan = nominal_titipan.slice(0, -1);
                    nominal_titipan = parseFloat(nominal_titipan);
                    if (isNaN(nominal_titipan)) nominal_titipan = 0;

                    $(this).val(nominal_titipan.toLocaleString());
                    $('#formSimpanContainer').find('input[name="nominal_titipan"]').val(nominal_titipan);
                    updateHargaOnKeyup();
                }
            } else {
                $(this).parents('.input-group').first().addClass('has-error');
                updateHargaOnKeyup();
            }
        });

        $(document).on('click', '#btnSimpanPO', function(event) {
            event.preventDefault();

            var pelanggan_id = $('input[name="pelanggan"]').val();
            var is_grosir = $('input[name="grosir"]').val() == 'true' ? true : false;
            if (pelanggan_id == '') {
                if (is_grosir) {
                    // tanya mau bikin member atau tidak
                    swal({
                        title: 'Buat Kartu Member?',
                        html: 'Transaksi ini tergolong grosir.<br>Apakah Anda ingin membuat kartu member?',
                        type: 'question',
                        width: 600,
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: '#26B99A',
                        cancelButtonColor: '#286090',
                        confirmButtonText: '<i class="fa fa-credit-card"></i> Buat Kartu Member',
                        cancelButtonText: '<i class="fa fa-money"></i> Langsung Bayar'
                    }).then(function(dismiss) {
                        // simpan po grosir
                        // bikin kartu member
                        // console.log(dismiss);
                        var action = "{{ url('transaksi-grosir/simpan-po/pelanggan-baru') }}";
                        $('#form-simpan').attr('action', action);
                        $('#form-simpan').submit();
                    }, function(dismiss) {
                        // simpan po eceran
                        // console.log(dismiss);
                        if (dismiss == 'cancel') {
                            // console.log('button');
                            var action = "{{ url('transaksi-grosir/simpan-po/eceran') }}";
                            $('#form-simpan').attr('action', action);
                            $('#form-simpan').submit();
                        } else {
                            // console.log('close');
                        }
                    });
                } else {
                    // simpan po eceran
                    var action = "{{ url('transaksi-grosir/simpan-po/eceran') }}";
                    $('#form-simpan').attr('action', action);
                    $('#form-simpan').submit();
                }
            } else {
                // cek grosir atau eceran berdasarkan selisih item grosir dan eceran
                if (is_grosir) {
                    // simpan po grosir
                    var action = "{{ url('transaksi-grosir/simpan-po/grosir') }}";
                    $('#form-simpan').attr('action', action);
                    $('#form-simpan').submit();
                } else {
                    // simpan po eceran
                    var action = "{{ url('transaksi-grosir/simpan-po/eceran') }}";
                    $('#form-simpan').attr('action', action);
                    $('#form-simpan').submit();
                }
            }
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();

            var kode = $(this).parents('tr').data('id');
            var tr = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            var index = selected_items.indexOf(kode);
            if (index >= -1) {
                selected_items.splice(index, 1);
            }

            var inputNegoContainer = tr.find('#inputNegoContainer');
            var checked = tr.find('#checkNego').prop('checked');
            var nego = parseFloat(tr.find('#inputNego').val().replace(/\D/g, ''), 10);
            var nego_min = parseFloat(tr.find('#inputNegoMin').val());
            var subtotal = 0;
            if (checked && !inputNegoContainer.hasClass('has-error')) {
                subtotal = nego;
            } else {
                subtotal = parseFloat(tr.find('#inputSubTotal').val().replace(/\D/g, ''), 10);
            }

            var harga_total = parseFloat($('input[name="harga_total"]').val().replace(/\D/g, ''), 10);
            var nego_total_min = parseFloat($('#inputNegoTotalMin').val().replace(/\D/g, ''), 10);
            var harga_total_plus_ongkos_kirim = parseFloat($('#inputHargaTotalPlusOngkosKirim').val().replace(/\D/g, ''), 10);
            var jumlah_bayar = parseFloat($('#inputJumlahBayar').val().replace(/\D/g, ''), 10);
            // var kembali = parseFloat($('#inputTotalKembali').val().replace(/\D/g, ''), 10);
            // console.log(harga_total, subtotal);

            harga_total -= subtotal;
            nego_total_min -= nego_min;
            harga_total_plus_ongkos_kirim -= subtotal;
            kembali = jumlah_bayar - harga_total_plus_ongkos_kirim;

            if (isNaN(harga_total) || harga_total == 0) harga_total = 0;
            if (isNaN(nego_total_min) || nego_total_min == 0) nego_total_min = '';
            if (isNaN(harga_total_plus_ongkos_kirim)) harga_total_plus_ongkos_kirim = 0;
            if (isNaN(kembali)) kembali = 0;
            if (kembali < 0) kembali = 0;

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('#inputNegoTotalMin').val(nego_total_min);
            $('#inputHargaTotalPlusOngkosKirim').val(harga_total_plus_ongkos_kirim.toLocaleString());
            $('#inputTotalKembali').val(kembali.toLocaleString());

            $('#checkNegoTotal').prop('checked', false);
            $('#checkNegoTotal').trigger('change');

            $('input[name="harga_total"]').val(harga_total);
            // $('input[name="kembali"]').val(kembali);

            if (selected_items.length <= 0) {
                $('#level_pelanggan').removeClass('label-success');
                $('#level_pelanggan').removeClass('label-warning');
                $('#tipe_penjualan').removeClass('label-success');
                $('#tipe_penjualan').removeClass('label-warning');
            }

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();

            updateHargaOnKeyup();
        });

    </script>
@endsection
