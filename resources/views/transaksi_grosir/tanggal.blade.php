@extends('layouts.admin')

@section('title')
    <title>EPOS | Ubah Tanggal Jatuh Tempo Piutang</title>
@endsection

@section('style')
    <style type="text/css" media="screen">
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Ubah Tanggal Jatuh Tempo Piutang</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <h3>Kode Transaksi Penjualan: </h3>
                <h3>{{ $transaksi->kode_transaksi }}</h3>
                <br>
                <h3>Jatuh Tempo pada :</h3>
                <h3>{{ \App\Util::tanggal(($tanggal)->format('d m Y')) }}</h3>
                <br>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Ubah Tanggal Jatuh Tempo Piutang</h2>
                <a href="{{ url('transaksi-grosir') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                    <i class="fa fa-long-arrow-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-4">
                    <label class="control-label">
                        Pilih Tanggal
                    </label>
                    <div class="form-group">
                        <input name="pilihTanggal" id="pilihTanggal" class="form-control input-sm tanggal-putih" value="" type="text" placeholder="Pilih Tanggal" readonly="">
                    </div>
                </div>
                <div class="col-md-8">
                    <form method="post" action="{{ url('transaksi-grosir/tanggal') }}" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="_method" value="post">
                        
                        <input type="hidden" name="tanggal">
                        <input type="hidden" name="id" value="{{ $transaksi->id }}">
                        <button class="btn btn-sm btn-success pull-right" id="btnUbah" type="submit" style="margin-top: 25px">
                            <span style="color:white">Ubah</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Tanggal jatuh tempo berhasil diubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Pembayaran Piutang Dagang Gagal!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">

        function updateHargaOnKeyup() {
            var $sisa_piutang  = $('#inputSisaPiutang');
            var $jumlah_bayar = $('#inputJumlahBayar');

            var nominal_tunai = $('#formSimpanContainer').find('input[name="nominal_tunai"]').val();
            var nominal_debit = $('#formSimpanContainer').find('input[name="nominal_debit"]').val();
            var nominal_kartu = $('#formSimpanContainer').find('input[name="nominal_kartu"]').val();
            var nominal_cek   = $('#formSimpanContainer').find('input[name="nominal_cek"]').val();
            var nominal_bg    = $('#formSimpanContainer').find('input[name="nominal_bg"]').val();

            nominal_tunai = parseInt(nominal_tunai.replace(/\D/g, ''), 10);
            nominal_debit = parseInt(nominal_debit.replace(/\D/g, ''), 10);
            nominal_kartu = parseInt(nominal_kartu.replace(/\D/g, ''), 10);
            nominal_cek   = parseInt(nominal_cek.replace(/\D/g, ''), 10);
            nominal_bg    = parseInt(nominal_bg.replace(/\D/g, ''), 10);

            if (isNaN(nominal_tunai)) nominal_tunai = 0;
            if (isNaN(nominal_debit)) nominal_debit = 0;
            if (isNaN(nominal_kartu)) nominal_kartu = 0;
            if (isNaN(nominal_cek)) nominal_cek = 0;
            if (isNaN(nominal_bg)) nominal_bg = 0;

            var sisa_piutang = $sisa_piutang.val();
            var jumlah_bayar = nominal_tunai + nominal_debit + nominal_kartu + nominal_cek + nominal_bg;

            if (isNaN(sisa_piutang)) sisa_piutang = 0;
            if (isNaN(jumlah_bayar)) jumlah_bayar = 0;

            $sisa_piutang.val(sisa_piutang.toLocaleString());
            $jumlah_bayar.val(jumlah_bayar.toLocaleString());

            $('input[name="sisa_piutang"]').val(sisa_piutang);
            $('input[name="jumlah_bayar"]').val(jumlah_bayar);
        }

        $('#tabelLogPiutang').DataTable();

        $(document).ready(function() {
            var url = "{{ url('transaksi-grosir') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');
            $('.right_col').css('min-height', $('.left_col').css('height'));

            $('#inputTunaiContainer').hide();
            $('#inputDebitContainer').hide();
            $('#inputDebitContainer').find('input').val('');
            $('#inputKartuContainer').hide();
            $('#inputKartuContainer').find('input').val('');
            $('#inputCekContainer').hide();
            $('#inputCekContainer').find('input').val('');
            $('#inputBgContainer').hide();
            $('#inputBgContainer').find('input').val('');

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#inputHargaTotal').val(0);
            $('#inputNegoTotal').val(0);
            $('#inputJumlahBayar').val(0);
            $('#inputTotalKembali').val(0);

            $('select[name="bank"]').next('span').css({
                'width': '100%',
                'margin-bottom': '10px'
            });
        });

        $(window).on('load', function(event) {
            event.preventDefault();

            var url   = "{{ url('transaksi-grosir/last/json') }}";
            var bulan = printBulanSekarang('mm/yyyy');

            $.get(url, function(data) {
                if (data.piutang === null) {
                    var kode_transaksi = '0001/BPD/' + bulan;
                } else {
                    var kode_transaksi  = data.piutang.kode_transaksi;
                    var mm_transaksi    = kode_transaksi.split('/')[2];
                    var yyyy_transaksi  = kode_transaksi.split('/')[3];
                    var bulan_transaksi = mm_transaksi + '/' + yyyy_transaksi;

                    if (bulan !== bulan_transaksi) {
                        kode_transaksi = '0001/BPD/' + bulan;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/BPD/' + bulan_transaksi;
                    }
                }

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#inputKodeTransaksi').val(kode_transaksi);
            });
        });

        $(document).on('focus', '#pilihTanggal', function(event) {
            event.preventDefault();

            $('#pilihTanggal').daterangepicker({
                autoApply: true,
                showDropdowns: true,
                calender_style: "picker_2",
                format: 'DD-MM-YYYY',
                locale: {
                    "applyLabel": "Pilih",
                    "cancelLabel": "Batal",
                    "fromLabel": "Awal",
                    "toLabel": "Akhir",
                    "customRangeLabel": "Custom",
                    "weekLabel": "M",
                    "daysOfWeek": [
                        "Min",
                        "Sen",
                        "Sel",
                        "Rab",
                        "Kam",
                        "Jum",
                        "Sab"
                    ],
                    "monthNames": [
                        "Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni",
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember"
                    ],
                    "firstDay": 1
                },
                singleDatePicker: true
            }, function(start) {
                $('#pilihTahun').val('');
                $('#pilihBulan').val('');
                $('#pilihRentangTanggal').val('');
                var beban = $('#pilihBeban').val();

                var tanggal = (start.toISOString()).substring(0,10);
                $('input[name="tanggal"]').val(tanggal);
            });
        });

    </script>
@endsection
