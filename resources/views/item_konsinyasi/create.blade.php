@extends('layouts.admin')

@section('title')
	<title>EPOS | Tambah Item</title>
@endsection

@section('style')
	<style media="screen">
		#btnUbah, #btnHapus {
			margin-bottom: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tambah Item</h2>
				<a href="{{ url('item-konsinyasi') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" type="button" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" id="formSimpanContainer">
				<form method="POST" action="{{ url('item-konsinyasi') }}">
					{!! csrf_field() !!}
					<input type="hidden" name="id">
					<input type="hidden" name="konsinyasi" value="1">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Kode Item
								</label>
								<input class="form-control" type="text" id="kode" name="kode">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Nama Item
								</label>
								<input class="form-control" type="text" id="nama" name="nama">
							</div>
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label class="control-label">Nama Suplier</label>
							<select name="suplier_id" class="select2_single form-control">
								<option id="default">Pilih Suplier</option>
								@foreach($supliers as $suplier)
								<option value="{{ $suplier->id }}">{{$suplier->nama}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Jenis Item</label>
								<select class="form-control select2_single" id="jenis_item_id" name="jenis_item_id">
									<option>Pilih jenis item</option>
									@foreach($jenis_items as $jenis_item)
									<option value="{{$jenis_item->id}}">{{ $jenis_item->kode }} : {{ $jenis_item->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Item Bonus</label>
								<select class="select2_single form-control" id="bonus_id" name="bonus_id">
									<option value="">Pilih Item Bonus</option>
									@foreach($bonuses as $bonus)
									<option value="{{$bonus->id}}">{{ $bonus->kode }} : {{ $bonus->nama }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">Syarat Bonus (Jumlah untuk mendapatkan bonus)</label>
								<input class="form-control" type="text" id="syarat_bonus" name="syarat_bonus">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									Stok Limit
								</label>
								<input class="form-control" type="text" id="stok_limit" name="stok_limit">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="col-md-12form-group">
								<button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
									<i class="fa fa-save"></i> Simpan
								</button>
								{{-- <button class="btn btn-sm btn-default" id="btnReset" type="button">
									<i class="fa fa-refresh"></i> Reset
								</button> --}}
							</div>
						</div>
					</div>
				</form>
			</div>
			<div id="formHapusContainer" style="display: none;">
				<form method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="delete">
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$(".select2_single").select2();
	});

	// $(document).on('click', '#btnReset', function() {
	// 	$('input[name="nama"]').val('');
	// 	$('input[name="jenis_item_id"]').val('');
	// 	$('input[name="syarat_bonus"]').val('');
	// 	$('input[name="harga_dasar"]').val('');

	// 	$('#formSimpanContainer').find('form').attr('action', '{{ url("item") }}');
	// 	$('#formSimpanContainer').find('input[name="_method"]').val('post');
	// 	$('#btnSimpan span').text('Tambah');

	// 	$('#formSimpanTitle').text('Tambah Jenis Item');
	// });
</script>
@endsection
