@extends('layouts.admin')

@section('title')
    <title>EPOS | History Harga Beli</title>
@endsection

@section('style')
    <style media="screen">
    	#btnDetail, #btnUbah, #btnHapus {
    		margin-bottom: 0;
    	}
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
        	width: 100%;
        }
        .link {
        	text-decoration: underline;
        }
    </style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		@foreach ($supliers as $i => $suplier)
		<div class="x_panel">
			<div class="x_title">
				<h2>History Harga {{ $suplier->item->nama }} dari <b>{{ $suplier->nama }}</b></h2>
				<a href="{{ url('item-konsinyasi') }}" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Kembali">
                	<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped table-bordered table-hover tableItem" style="margin-bottom: 0;" id="tableItem">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Harga Beli</th>
							<th>Kode Transaksi</th>
						</tr>
					</thead>
					<tbody>
						@foreach($suplier->relasis as $num => $relasi)
						<tr id="{{ $relasi->id }}">
							<td>{{ $num + 1 }}</td>
							<td>{{ $relasi->created_at->format('Y-m-d') }}</td>
							<td>{{ number_format($relasi->harga) }}</td>
							<td class="link"><a href="{{ url('transaksi-pembelian/'.$relasi->transaksi_pembelian->id.'/hhb/'.$relasi->item->id) }}">{{ $relasi->transaksi_pembelian->kode_transaksi }}</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endforeach
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			var url = "{{ url('item') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');

			$('.tableItem').DataTable();
		});
	</script>
@endsection
