@extends('layouts.admin')

@section('title')
	<title>EPOS | Detail Penyesuaian Persediaan</title>
@endsection

@section('style')
	<style type="text/css" media="screen">
		#btnKembali {
			margin: 0;
		}
		.thumbnail {
			padding: 20px;
		}
		.dataTables_filter {
			width: 100%;
		}
	</style>
@endsection

@section('content')
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Detail Penyesuaian Persediaan</h2>
				<a href="{{ url('penyesuaian_persediaan') }}" class="btn btn-sm btn-default pull-right" id="btnKembali" data-toggle="tooltip" data-placement="top" title="Kembali">
					<i class="fa fa-long-arrow-left"></i>
				</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<div class="col-md-5 col-xs-12">
						<div class="x_title">
							<h2>{{ $penyesuaian_persediaan->created_at->format('d-m-Y H:i:s') }}</h2>
							<div class="clearfix"></div>
						</div>
						<table class="table">
							<tbody>
								<tr>
									<th style="border-top: none;">Kode Transaksi</th>
									<td style="border-top: none;">{{ $penyesuaian_persediaan->kode_transaksi }}</td>
								</tr>
								<tr>
									@if($penyesuaian_persediaan->status == 'keluar')
										<th>Total Kerugian</th>
									@else
										<th>Total Keuntungan</th>
									@endif
										<td>{{ \App\Util::duit($penyesuaian_persediaan->nominal) }}</td>
								</tr>
								<tr>
									<th>Operator</th>
									<td>{{ $penyesuaian_persediaan->user->nama }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-7 col-xs-12">
						<div class="x_title">
							<h2>Item</h2>
							<div class="clearfix"></div>
						</div>
						<table class="table table-bordered table-striped table-hover" id="tabel-item">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Item</th>
									<th>Jumlah</th>
									<th>Harga</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($relasi_penyesuaian_persediaan as $i => $relasi)
								
								<tr data="{{ $a = $i }}">
									<td class="text-center">{{ ++$i }}</td>
									<td>{{ $relasi->item->nama }}</td>
									<td>
										@foreach($hasil[$a] as $x => $jumlah)
											<span class="label label-info">{{ $jumlah['jumlah'] }}  {{ $jumlah['satuan'] }}</span>
										@endforeach
									</td>
									<td class="text-right">{{ \App\Util::duit($relasi->subtotal) }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('#tabel-item').DataTable();

		$(document).ready(function() {
			var url = "{{ url('transaksi-grosir') }}";
			var a = $('a[href="' + url + '"]');
			a.parent().addClass('current-page');
			a.parent().parent().show();
			a.parent().parent().parent().addClass('active');
		});
	</script>
@endsection
