@extends('layouts.admin')

@section('title')
    <title>EPOS | Tambah Penyesuaian Persediaan</title>
@endsection

@section('style')
    <style media="screen">
        #btnKembali {
            margin-right: 0;
        }
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
        .form-danger, 
        .form-danger:focus {
            border-color: red;
        }

        .panel_toolbox li {
            cursor: pointer;
        }

        #metodePembayaranButtonGroup,
        #formSimpanTitle,
        #kodeTransaksiTitle {
            width: 100%;
        }

        #metodePembayaranButtonGroup > label {
            border-radius: 0;
        }

        #metodePembayaranButtonGroup > label.active {
            background: #26b99a;
            border: 1px solid #169f85;
            color: white;
            font-weight: bold;
            border-radius: 0;
        }

        button[data-target="#collapse-peralatan"] {
            background-color: transparent;
            border: none;
        }

        #tabelPenyesuaianPersediaan .btn {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
<!-- <div class="row"> -->
    <div class="col-md-12" id="formSimpanContainer">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tambah Penyesuaian Persediaan</h2>
                <ul class="nav navbar-right panel_toolbox" style="padding-left: 100px">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                </ul>
                {{-- <div class="row">
                    <div class="col-md-9">
                        <h2>Tambah Penyesuaian Persediaan</h2>
                    </div>
                    <ul class="nav navbar-right panel_toolbox">
                        <div class="pull-right" style="margin-top: 0px">
                            <li> 
                                <button data-target="#collapse-peralatan" data-toggle="collapse" class="sembunyit"> <i class="fa fa-chevron-down"> </i> </button> 
                            </li>
                        </div>
                    </ul>
                </div> --}}
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                  {{-- <div class="row collapse" id="collapse-peralatan"> --}}
                  <div class="row">
                      <div class="col-md-5 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 id="formSimpanTitle">Tambah Penyesuaian Persediaan</h2>
                                        <span id="kodeTransaksiTitle"></span>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="row">
                                    <div class="form-group col-sm-12 col-xs-12">
                                        <label class="control-label">Pilih Item</label>
                                        <select name="item_id" id="pilihItem" class="select2_single form-control">
                                            <option value="" id="">Pilih Item</option>
                                            @foreach ($items as $item)
                                            <option value="{{ $item->kode }}">[{{ $item->kode }}] {{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Informasi Item</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table class="table" id="tabelInfo">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left;">Item</th>
                                            <th style="text-align: left;">Stok</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <!-- <div class="x_title">
                                <h2>Keranjang Belanja</h2>
                                <div class="clearfix"></div>
                            </div> -->
                            <div class="x_content">
                                <table class="table" id="tabelKeranjang" style="border-bottom: 1px solid #dfdfdf;">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Item</th>
                                            <th>Jumlah</th>
                                            <th>Satuan</th>
                                            <th>Harga</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-md-4 col-xs-6">
                                <div class="row">
                                    <div id="pilihKasButtonGroup" class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnKeluar" class="btn btn-default">Keluar</button>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <button type="button" id="btnMasuk" class="btn btn-default">Masuk</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 25px">
                                    <div class="form-group">
                                        <label class="control-label">Keterangan</label>
                                        <input type="text" id="inputKeterangan" name="keterangan_" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-6 col-md-offset-4">
                                <div class="form-group">
                                    <label class="control-label">Sub Total</label>
                                    {{-- <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control text-right" readonly="readonly" /> --}}
                                    <div class="input-group" style="margin-bottom: 0;">
                                        <div class="input-group-addon">
                                            Rp
                                        </div>
                                        <input type="text" name="inputHargaTotal" id="inputHargaTotal" class="form-control input-sm text-right" readonly="readonly"/>
                                    </div>
                                </div>
                                <div id="formSimpanContainer" style="margin-top: 20px;">
                                    <form id="form-simpan" action="{{ url('penyesuaian_persediaan') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" name="kode_transaksi" value="" />
                                        <input type="hidden" name="harga_total"/>
                                        <input type="hidden" name="status" />
                                        <input type="hidden" name="keterangan" />

                                        <div id="append-section"></div>
                                        <div class="clearfix">
                                            <div class="form-group pull-right">
                                                <button id="buttonSubmit" type="submit" class="btn btn-success" style="width: 100px; margin: 0;"><i class="fa fa-check"></i> OK</button>	
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
              </div>
        </div>
    </div>
<!-- </div> -->

<!-- <div class="row"> -->
    <div class="col-md-12">
        <div class="x_panel">
              <div class="x_title">
                <h2>Daftar Penyesuaian Persediaan</h2>
                <ul class="nav navbar-right panel_toolbox" style="padding-left: 100px">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <table class="table table-bordered table-striped table-hover" style="margin-bottom: 0;" id="tabelPenyesuaianPersediaan">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Kode Transaksi</th>
                            <th>Nominal</th>
                            <th>Status</th>
                            {{-- <th>Operator</th> --}}
                            <th style="width: 25px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($datas as $i => $data)
                        <tr data-id="{{ $i++ }}">
                            <td class="tengah-v">{{ $i++ }}</td>
                            <td class="tengah-v">{{ $data->created_at->format('d-m-Y') }}</td>
                            <td class="tengah-v">{{ $data->kode_transaksi }}</td>
                            <td class="tengah-v">{{ \App\Util::duit($data->nominal) }}</td>
                            @if($data->status == 'keluar')
                            <td class="tengah-v" style="text-align:center">
                                <span class="label label-danger" >Keluar</span>
                            </td>
                            @else
                            <td class="tengah-v" style="text-align:center">
                                <span class="label label-info" >Masuk</span>
                            </td>
                            @endif
                            {{-- <td class="tengah-v">{{ $data->user->nama }}</td> --}}
                            <td class="tengah-v" style="text-align:center">
                                <a href="{{ url('penyesuaian_persediaan/'.$data->id.'/show') }}" class="btn btn-xs btn-info" id="btnDetail" data-toggle="tooltip" data-placement="top" title="Detail Penyesuaian Persediaan">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
        </div>
    </div>
<!-- </div> -->
@endsection

@section('script')
    @if (session('sukses') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                html: '<p>Transaksi Eceran berhasil ditambah!</p>' + 
                        '<table class="table table-bordered table-striped">' +
                            '<tr>' +
                                '<td>Total</td>' +
                                '<td>' + '{{ session('data')['total'] }}'.toLocaleString() + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Tunai</td>' +
                                '<td>' + '{{ session('data')['tunai'] }}'.toLocaleString() + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Kembalian</td>' +
                                '<td>' + '{{ session('data')['kembalian'] }}'.toLocaleString() + '</td>' +
                            '</tr>' +
                        '</table>',
                // timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Transaksi Eceran gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    {{-- <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                // allowClear: true,
                width: '100%'
            });

            $(".select2_single").each(function(index, el) {
                $(el).val('').trigger('change');
            });

            $('input[name="harga_total"]').val(0);
        });
    </script> --}}

    <script type="text/javascript">

        $('#tabelPenyesuaianPersediaan').DataTable();

        var selected_items = [];
        function buttonSubmitDisabled(){
            var satuan = false;
            var jumlah = false;
            $('.pilihSatuan_').each(function(index, el) {
                if($(el).hasClass('has-error')) {
                    satuan = true;
                    // break;
                }
            });

            $('.InputJumlah_').each(function(index, el) {
                if($(el).hasClass('has-error')) {
                    jumlah = true;
                    // break;
                }
            });

            if(satuan) return true;
            if(jumlah) return true;

            return false;
        }

        function updateHargaTotal() {
            var harga_total = 0;

            $('.subtotal').each(function(index, el) {
                var tmp = parseInt($(el).val().replace(/\D/g, ''), 10);
                if (isNaN(tmp)) tmp = 0;
                harga_total += tmp;
            });

            $('#inputHargaTotal').val(harga_total.toLocaleString());
            $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(2));
            $('#hiddenHargaTotal').val(harga_total);

            var harga_total  = parseInt($('#inputHargaTotal').val().replace(/\D/g, ''), 10);
        }

        function updateHargaOnKeyup() {
            var $harga_total  = $('#inputHargaTotal');
            var harga_total = parseInt($harga_total.val().replace(/\D/g, ''), 10);

            if (isNaN(harga_total)) harga_total = 0;
            
            $harga_total.val(harga_total.toLocaleString());
            
            $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(2));
        }

        function cariItem(kode, url, tr) {
            $.get(url, function(data) {
                // console.log(data);
                var item = data.item;
                var nama = data.item.nama;
                var harga = data.harga.harga;
                var hargas = harga;
                var stoktotal = data.stok.stok;
                hargax = parseFloat(harga);
                harga = hargax.toFixed(2);

                // var harga_total = parseFloat($('input[name="harga_total"]').val());
                // harga_total += parseFloat(harga);
                // $('input[name="harga_total"]').val(harga_total);

                var satuan_item = [];
                for (var i = 0; i < item.satuan_pembelians.length; i++) {
                    for (var j = 0; j < item.satuans.length; j++) {
                        // console.log(item.satuans[j]);
                        if (item.satuans[j].satuan_id == item.satuan_pembelians[i].satuan_id) {
                            var satuan = {
                                id: item.satuans[j].id,
                                satuan_id: item.satuan_pembelians[i].satuan.id,
                                kode: item.satuan_pembelians[i].satuan.kode,
                                konversi: item.satuan_pembelians[i].konversi
                            }
                            satuan_item.push(satuan);
                            break;
                        }
                    }
                }
                
                var select_satuans = "";

                if (tr === undefined) {
                    var text_stoktotal = '';
                    var temp_stoktotal = stoktotal;
                    for (var i = 0; i < satuan_item.length; i++) {
                        if (temp_stoktotal > 0) {
                            var satuan = satuan_item[i];
                            var jumlah_stok = parseInt(temp_stoktotal / satuan.konversi);
                            if (jumlah_stok > 0) {
                                text_stoktotal += jumlah_stok;
                                text_stoktotal += ' ';
                                text_stoktotal += satuan.kode;

                                temp_stoktotal = temp_stoktotal % satuan.konversi;
                                if (i != satuan_item.length - 1 && temp_stoktotal > 0) text_stoktotal += ' ';
                            }
                        }
                    }

                    var tr = ''+
                        '<tr>'+
                            '<td>'+nama+'</td>'+
                            '<td>'+text_stoktotal+'</td>'+
                        '</tr>';

                    $('#tabelInfo tbody').empty();
                    $('#tabelInfo tbody').append(tr);

                    satuan_item.sort(function(a, b){
                        return a.konversi-b.konversi
                    });	
                    // console.log( satuan_item[0]);
                    // for(var i=0; i<satuan_item.length; i++){
                    // 	// select_satuans[i] = satuan_item[i];
                    // 	select_satuans += `<option value="`+ satuan_item[i].satuan_id +`">`+ satuan_item[i].kode +`</option>`
                    // }

                    for(var i=satuan_item.length-1; i>=0; i--){
                        // select_satuans[i] = satuan_item[i];
                        if(i==0){
                            select_satuans += `<option value="`+ satuan_item[i].satuan_id +`" selected>`+ satuan_item[i].kode +`</option>`
                        }else{
                            select_satuans += `<option value="`+ satuan_item[i].satuan_id +`">`+ satuan_item[i].kode +`</option>`
                        }
                    }
                    // console.log(select_satuans);
                    // #satuan-'+kode
                    var pilihan_satuan = 
                        `<select name="satuan" class="form-control input-sm">`
                        +select_satuans+`</select>`;

                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="item_kode[]" id="item-'+kode+'" value="'+kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="jumlah[]" id="jumlah-'+kode+'" value="1" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" value="'+ satuan_item[0].satuan_id +'" name="satuan_id[]" id="satuan-' +kode+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" name="harga[]" id="harga-' + kode + '" value="'+parseFloat(harga)+'" />');
                    $('#form-simpan').find('#append-section').append('<input type="hidden" class="subtotal" name="subtotal[]" id="subtotal-'+kode+'" value="'+parseFloat(harga)+'" />');

                    $('#tabelKeranjang').find('tbody').append(
                        '<tr data-id="'+kode+'">'+
                            '<td>'+
                                '<i class="fa fa-times" title="Hapus Item" id="remove" style="cursor: pointer; color: tomato; padding-top: 8px;"></i>'+
                            '</td>'+
                            '<td style="padding-top: 13px">'+nama+'</td>'+
                            '<td id="InputJumlah_" class="InputJumlah_">'+
                                '<input type="text" name="inputJumlahItem" id="inputJumlahItem" class="form-control input-sm angka" value="1" />'+
                            '</td>'+
                            '<td id="pilihSatuan_" class="pilihSatuan_">'+ pilihan_satuan +'</td>'+
                            '<td>'+
                                // '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(harga).toLocaleString() + '" readonly="readonly" />'+
                                '<div id="inputHargaPerSatuanContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputHargaPerSatuan" id="inputHargaPerSatuan" class="form-control input-sm text-right" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
                                        '<input type="hidden" name="HargaPerSatuan" id="HargaPerSatuan" class="form-control text-right input-sm" value="' + parseFloat(harga)+ '"/>'+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div id="inputSubTotalContainer" class="form-group" style="margin-bottom: 0;">'+
                                    '<div class="input-group" style="margin-bottom: 0;">'+
                                        '<div class="input-group-addon">'+
                                            'Rp'+
                                        '</div>'+
                                        '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control input-sm text-right" value="' + harga.toLocaleString() + '" readonly="readonly" />'+
                                        '<input type="hidden" name="SubTotal" id="SubTotal" class="form-control input-sm" value="' + parseFloat(harga).toFixed(2) + '"/>'+
                                    '</div>'+
                                '</div>'+

                                // '<input type="text" name="inputSubTotal" id="inputSubTotal" class="form-control text-right input-sm angka" value="' + parseFloat(harga).toLocaleString() + '" readonly="readonly" />'+
                            '</td>'+
                        '</tr>');
                }

                var harga_total = $('input[name="harga_total"]').val();
                var total = parseFloat(harga_total) + parseFloat(harga);
                total = parseFloat(total.toFixed(2));

                // console.log(total);
                $('#inputHargaTotal').val((total).toLocaleString());

                $('#formSimpanContainer').find('input[name="harga_total"]').val(total);
            });
        }

        $(document).ready(function() {
            var url = "{{ url('penyesuaian_persediaan') }}";
            var a = $('a[href="' + url + '"]');
            a.parent().addClass('current-page');
            a.parent().parent().show();
            a.parent().parent().parent().addClass('active');

            $('#btnKeluar').removeClass('btn-default');
            $('#btnKeluar').addClass('btn-success');
            $('#formSimpanContainer').find('input[name="status"]').val('keluar');
            $('input[name="harga_total').val(0);

            $(".select2_single").select2({
                allowClear: true,
                width: '100%'
            });

            $('#inputHargaTotal').val(0);
        });

        $(document).on('click', '#btnMasuk', function(event) {
            event.preventDefault();
            
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');
            
            $('#btnKeluar').removeClass('btn-success');
            $('#btnKeluar').addClass('btn-default');

            $('#BankContainer').show('fast', function() {
                $(this).find('input').trigger('focus');
            });

            $('#formSimpanContainer').find('input[name="status"]').val('masuk');
        });

        $(document).on('click', '#btnKeluar', function(event) {
            event.preventDefault();
            $(this).removeClass('btn-default');
            $(this).addClass('btn-success');

            $('#btnMasuk').removeClass('btn-success');
            $('#btnMasuk').addClass('btn-default');

            $('#BankContainer').hide('fast', function() {
                $(this).find('input').val('');
            });

            $('#formSimpanContainer').find('input[name="status"]').val('keluar');
        });

        $(window).on('load', function(event) {

            var url = "{{ url('penyesuaian_persediaan/last/json') }}";
            var tanggal = printTanggalSekarang('dd/mm/yyyy');

            $.get(url, function(data) {
                if (data.penyesuaian_persediaan === null) {
                    var kode = int4digit(1);
                    var kode_transaksi = kode + '/PPR/' + tanggal;
                } else {
                    var kode_transaksi = data.penyesuaian_persediaan.kode_transaksi;
                    var dd_transaksi = kode_transaksi.split('/')[2];
                    var mm_transaksi = kode_transaksi.split('/')[3];
                    var yyyy_transaksi = kode_transaksi.split('/')[4];
                    var tanggal_transaksi = dd_transaksi + '/' + mm_transaksi + '/' + yyyy_transaksi;

                    if (tanggal != tanggal_transaksi) {
                        var kode = int4digit(1);
                        kode_transaksi = kode + '/PPR/' + tanggal;
                    } else {
                        var kode = int4digit(parseInt(kode_transaksi.split('/')[0]) + 1);
                        kode_transaksi = kode + '/PPR/' + tanggal_transaksi;
                    }
                }

                $('input[name="kode_transaksi"]').val(kode_transaksi);
                $('#kodeTransaksiTitle').text(kode_transaksi);
            });
        });

        // var keyupFromScanner = false;
        $(document).scannerDetection({
            avgTimeByChar: 40,
            onComplete: function(code, qty) {
                // console.log('Kode: ' + code, qty);
            },
            onError: function(error) {
                // console.log('Barcode: ' + error);
                var kode = error;
                var url  = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/item/json';
                var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

                var terpilih = false;
                $('input[name="item_kode[]"]').each(function(index, el) {
                    if ($(el).val() == kode) {
                        terpilih = true;
                    }
                });

                if (terpilih) {
                    // keyupFromScanner = true;
                    var jumlah_awal = parseInt($('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val());
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').val(jumlah_awal + 1);
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('keyup');
                    $('tr[data-id="'+kode+'"]').find('#inputJumlahItem').trigger('blur');
                } else {
                    cariItem(kode, url, tr);
                }
            }
        });

        $(document).on('change', '#pilihItem', function(event) {
            event.preventDefault();
            
            var kode = $(this).val();
            var url  = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/item/json';
            var tr   = $('#tabelKeranjang').find('tbody').children('tr[data-id="'+kode+'"]').data('id');

            if (!selected_items.includes(kode)) {
                if (kode) {
                    selected_items.push(kode);
                    cariItem(kode, url, tr);
                }
            }
        });

        var temp_jumlah = 0;
        $(document).on('click', '#inputJumlahItem', function(event) {
            event.preventDefault();
            temp_jumlah = $(this).val();
            $(this).val('');
        });

        $(document).on('blur', '#inputJumlahItem', function(event) {
            event.preventDefault();
            $(this).val(temp_jumlah);
        });

        $(document).on('keyup', '#inputJumlahItem', function(event) {
            event.preventDefault();
            
            var jumlah = $(this).val();
            temp_jumlah = jumlah;

            harga_total = 0;

            if (jumlah === '') jumlah = 0;

            var kode   = $(this).parents('tr').first().data('id');
            var satuan = $('#satuan-'+kode).val();

            var url    = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/konversi/json/'+satuan;
            var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            $('#jumlah-'+kode).val(jumlah);
            
                // console.log(url, 'cw');
            $.get(url, function(data) {

                if (data.satuan === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#HargaPerSatuan').val(0);
                    tr.find('#SubTotal').val(0);
                } else {
                    var konversi = data.satuan.konversi;
                    var count = parseFloat(jumlah) * parseFloat(data.satuan.konversi); 
                    var url_ = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/count/json/'+count;
                    console.log(url_, 'dw');

                    $.get(url_, function(data) {
                        // console.log(data.subtotal);
                        // var subtotal = parseFloat(data.subtotal).toFixed(2);	
                        // var harga = parseFloat(data.harga_satuan).toFixed(2);	
                        if(data.subtotal == 0 && data.jumlah == 0 && data.harga_satuan == 0) {
                            tr.find('#pilihSatuan_').addClass('has-error');
                            tr.find('#InputJumlah_').addClass('has-error');
                        } else {
                            tr.find('#pilihSatuan_').removeClass('has-error');
                            tr.find('#InputJumlah_').removeClass('has-error');
                            var harga = parseFloat(data.harga_satuan).toFixed(2);	
                            var harga_satuan = parseFloat(harga) * parseFloat(konversi);
                            var subtotal_ = parseFloat(harga) * parseFloat(data.jumlah);	
                            var subtotal = parseFloat(subtotal_).toFixed(2);	

                            tr.find('#inputHargaPerSatuan').val(harga_satuan.toLocaleString(undefined, {minimumFractionDigits: 2}));
                            tr.find('#inputSubTotal').val(parseFloat(subtotal).toLocaleString(undefined, {minimumFractionDigits: 2}));
                            tr.find('#HargaPerSatuan').val(harga);
                            tr.find('#SubTotal').val(subtotal);

                            $('#harga-'+kode).val(harga);
                            $('#subtotal-'+kode).val(parseFloat(subtotal).toFixed(2));

                            $('.subtotal').each(function(index, el) {
                                var tmp = parseFloat($(el).val());
                                if (isNaN(tmp)) tmp = 0;
                                harga_total += tmp;
                            });
                            
                            $('input[name="inputHargaTotal"]').val(parseFloat(harga_total.toFixed(2)).toLocaleString(undefined, {minimumFractionDigits: 2}));
                            $('input[name="harga_total"]').val(parseFloat(harga_total.toFixed(2)));
                        }
                        $('#buttonSubmit').prop('disabled', buttonSubmitDisabled());
                    });
                }
            });
            // $('#buttonSubmit').prop('disabled', buttonSubmitDisabled());
            console.log(buttonSubmitDisabled());
            console.log('bw');
        });

        $(document).on('change', 'select[name="satuan"]', function(event) {
            event.preventDefault();
            harga_total = 0;
            var satuan = $(this).val();
            // $('select[name="satuan"]').val(satuan).trigger('change');
            var kode = $(this).parents('tr').data('id');
            var jumlah = $(this).parent().prev().children('input').val();
            var $id = '#satuan-'+kode;
            $(document).find($id).val(satuan);

            if (jumlah === '') jumlah = 0;

            var url    = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/konversi/json/'+satuan;
            var tr     = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');

            // console.log(url, 'bw');
            $.get(url, function(data) {
                if (data.harga === null) {
                    tr.find('#inputHargaPerSatuan').val(0);
                    tr.find('#inputSubTotal').val(0);
                    tr.find('#HargaPerSatuan').val(0);
                    tr.find('#SubTotal').val(0);
                } else {
                    var konversi = parseFloat(data.satuan.konversi);
                    var count = parseFloat(jumlah) * parseFloat(data.satuan.konversi); 
                    var url_ = "{{ url('penyesuaian_persediaan') }}"+'/'+kode+'/count/json/'+count;
                    // console.log(url_, 'aw');
                    $.get(url_, function(data) {
                        // console.log(data);
                        if(data.subtotal == 0 && data.jumlah == 0 && data.harga_satuan == 0) {
                            tr.find('#pilihSatuan_').addClass('has-error');
                            tr.find('#InputJumlah_').addClass('has-error');
                        } else {
                            tr.find('#pilihSatuan_').removeClass('has-error');
                            tr.find('#InputJumlah_').removeClass('has-error');
                            var harga = parseFloat(data.harga_satuan).toFixed(2);
                            var harga_satuan = parseFloat(harga) * parseFloat(konversi);
                            var subtotal_ = parseFloat(harga) * parseFloat(data.jumlah);
                            var subtotal = parseFloat(subtotal_).toFixed(2);

                            tr.find('#inputHargaPerSatuan').val(harga_satuan.toLocaleString(undefined, {minimumFractionDigits: 2}));
                            tr.find('#inputSubTotal').val(parseFloat(subtotal).toLocaleString(undefined, {minimumFractionDigits: 2}));
                            tr.find('#HargaPerSatuan').val(harga);
                            tr.find('#SubTotal').val(subtotal);

                            $('#harga-'+kode).val(harga);
                            $('#subtotal-'+kode).val(parseFloat(subtotal_).toFixed(2));

                            $('.subtotal').each(function(index, el) {
                                var tmp = parseFloat($(el).val());
                                if (isNaN(tmp)) tmp = 0;
                                harga_total += tmp;
                            });
                            
                            $('input[name="inputHargaTotal"]').val(parseFloat(harga_total.toFixed(2)).toLocaleString(undefined, {minimumFractionDigits: 2}));
                            $('input[name="harga_total"]').val(parseFloat(harga_total.toFixed(2)));
                        }
                        $('#buttonSubmit').prop('disabled', buttonSubmitDisabled());
                    });
                }
            });
            // $('#buttonSubmit').prop('disabled', buttonSubmitDisabled());
            console.log(buttonSubmitDisabled());
            console.log('aw');
        });

        $(document).on('click', '#remove', function(event) {
            event.preventDefault();
            
            var kode         = $(this).parents('tr').data('id');
            var tr           = $('#tabelKeranjang').find('tr[data-id="'+kode+'"]');
            var subtotal     = $('#subtotal-'+kode).val();
            var harga_total  = $('input[name="harga_total"]').val();
            // parseFloat($('#inputHargaTotal').val().replace(/\D/g, ''), 10);

            harga_total -= subtotal;
            deleteMe(selected_items, kode);

            $('#inputHargaTotal').val(harga_total.toLocaleString(undefined, {minimumFractionDigits: 2}));

            $('input[name="harga_total"]').val(parseFloat(harga_total).toFixed(2));

            tr.remove();
            $('#form-simpan').find('#append-section').find('input[id*=-'+kode+']').remove();
        });
        
        // $(document).on('click', 'button[data-target="#collapse-peralatan"]', function(event) {
        // 	event.preventDefault();
            
        // 	if ($(this).hasClass('tampil')) {
        // 		$(this).removeClass('tampil');
        // 		$(this).addClass('sembunyit');
        // 		$(this).find('i').removeClass('fa-chevron-up');
        // 		$(this).find('i').addClass('fa-chevron-down');
        // 	} else if ($(this).hasClass('sembunyit')) {
        // 		$(this).removeClass('sembunyit');
        // 		$(this).addClass('tampil');
        // 		$(this).find('i').removeClass('fa-chevron-down');
        // 		$(this).find('i').addClass('fa-chevron-up');
        // 	}
        // });

        function deleteMe( arr, me ){
           var i = arr.length;
           while( i-- ) if(arr[i] === me ) arr.splice(i,1);
        }

        $(document).on('keyup', '#inputKeterangan', function(event) {
            event.preventDefault();
            var keterangan = $(this).val();
            // console.log(keterangan);
            $('input[name="keterangan"]').val(keterangan);
        });

    </script>
@endsection
