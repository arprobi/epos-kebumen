@extends('layouts.admin')

@section('title')
    <title>EPOS | Setoran Laci</title>
@endsection

@section('style')
    <style media="screen">
        /*#btnUbah, #btnHapus {
            margin-bottom: 0;
        }*/
        .thumbnail {
            padding: 20px;
        }
        .dataTables_filter {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-4 col-xs-12">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Ubah Nominal Laci Owner</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSimpanContainer">
                        <form method="post" action="{{ url('laci_owner') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal" required="">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpan" type="submit">
                                    <i class="fa fa-save"></i> <span>Ubah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Setoran ke Gudang</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('setoranke_gudang') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_buka">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($gudang_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanBuka" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Tambah Setoran ke Grosir</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('setoranke_grosir') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_buka">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($grosir_receiver as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanBuka" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Transfer Laci ke Pengguna Lain</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('setoran_dari_owner') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_buka" required="">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <select name="penerima" id="penerima" class="form-control select2_single">
                                    <option value="">Pilih Penerima</option>
                                    @foreach($all_receiver as $i => $user)
                                        @if($user->id != Auth::user()->id)
                                            <option value="{{$user->id}}">{{$user->nama}} [{{ $user->level->nama }}]</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanBuka" type="submit">
                                    <i class="fa fa-save"></i> <span>Tambah</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 id="formSimpanTitle">Setor Tunai ke Bank</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="formSetorContainer">
                        <form method="post" action="{{ url('transfer_bank_owner') }}" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="post">
                            <div class="form-group">
                                <label class="control-label">Pengirim</label>
                                <select name="pengirim" id="pengirim" class="form-control select2_single" required="">
                                    <option value="">Pilih Pengirim</option>
                                    @foreach($sender as $i => $user)
                                        <option value="{{$user->id}}">{{$user->nama}}</option>
                                    @endforeach
                                    <option value="lain">Pengirim Lain</option>
                                </select>
                            </div>
                            <div class="form-group" id="pengirim_form">
                                <label class="control-label">Nama Pengirim</label>
                                <input id="nama_pengirim" class="form-control" type="text" name="nama_pengirim" disabled="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nominal</label>
                                <input class="form-control angka" type="text" name="nominal_show" id="nominal_setor" required="">
                                <input type="hidden" name="nominal">
                            </div>
                            <div class="form-group" id="BankContainer">
                                <label class="control-label">Pilih Rekening Bank</label>
                                <select name="bank_asal" class="select2_single form-control" required="">
                                    <option value="">Pilih Bank</option>
                                    @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{$bank->nama_bank}} [{{$bank->no_rekening}}]</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="margin-bottom: 0;">
                                <button class="btn btn-sm btn-success" id="btnSimpanSetor" type="submit">
                                    <i class="fa fa-save"></i> <span>Transfer</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- kolom kanan -->
    <div class="col-md-8 col-xs-12">
        {{-- <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Gudang Hari Ini</h2>
                        <p class="pull-right">Uang di Laci : {{ \App\Util::ewon($laci->owner) }}</p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranGrosir">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setoran_gudang as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setor->user->nama }}</td>
                                    <td>{{ $setor->created_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td style="text-align: center">
                                        @if($setor->status == 0)
                                            <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                                Setujui
                                            </button>
                                        @else
                                            <span class="label label-info">Sudah disetujui</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran dari Owner Hari Ini</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranGrosir">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Penerima</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setoran_today as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setor->created_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td>({{ $setor->receiver->level->nama }}) {{ $setor->receiver->nama }}</td>
                                    <td style="text-align: center">
                                        @if($setor->approve == 0)
                                            <span class="label label-danger">
                                                Belum diterima
                                            </span>
                                        @else
                                            <span class="label label-info">Sudah diterima</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @foreach ($setoran_belum as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $setor->created_at->format('H:i') }}</td>
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td>({{ $setor->receiver->level->nama }}) {{ $setor->receiver->nama }}</td>
                                    <td style="text-align: center">
                                        @if($setor->approve == 0)
                                            <span class="label label-danger">
                                                Belum diterima
                                            </span>
                                        @else
                                            <span class="label label-info">Sudah diterima</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Masuk ke Owner</h2>
                        <p class="pull-right">Uang di Laci : {{ \App\Util::ewon($laci->owner) }}</p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranOwner1" var="{{ $a = 0 }}">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pengirim</th>
                                    <th>Nominal</th>
                                    <th>Waktu</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setoran_masuk as $i => $setor)
                                @if($setor->pengirim != null)
                                    <tr id="{{ $setor->id }}">
                                        <td>{{ $a + 1 }}</td>
                                        <td>{{ $setor->sender->nama }}</td>
                                        <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                        <td>{{ $setor->updated_at->format('d-m-Y H:i') }}</td>
                                        <td style="text-align: center">
                                            @if($setor->approve == 0)
                                                <button class="btn btn-xs btn-danger btnHapus" id="approve">
                                                    Setujui
                                                </button>
                                            @else
                                                <span class="label label-info">Sudah disetujui</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Setoran Keluar dari Owner</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" style="margin-bottom: 0;" id="tableSetoranOwner">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Penerima</th>
                                    <th>Nominal</th>
                                    <th>Waktu</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($setorans as $i => $setor)
                                <tr id="{{ $setor->id }}">
                                    <td>{{ $i + 1 }}</td>
                                    @if($setor->penerima != NULL)
                                        <td>({{ $setor->receiver->level->nama }}) {{ $setor->receiver->nama }}</td>
                                    @else
                                        <td>Setoran ke Bank</td>
                                    @endif
                                    <td class="text-right">{{ \App\Util::ewon($setor->nominal) }}</td>
                                    <td>{{ $setor->updated_at->format('d-m-Y H:i') }}</td>
                                    <td style="text-align: center">
                                        @if($setor->penerima != NULL)
                                            @if($setor->approve == 0)
                                                <span class="label label-danger">
                                                    Belum diterima
                                                </span>
                                            @else
                                                <span class="label label-info">Sudah diterima</span>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="formApprove" style="display: none;">
        <form method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="post">
        </form>
    </div>
@endsection

@section('script')
    @if (session('sukses') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Laci Owner berhasil ubah!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'tambah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal ditambah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'setuju')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Gudang berhasil disetujui!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'setor_sukses')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Transfer Laci ke Pengguna lain berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'ubah')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal diubah!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @elseif (session('sukses') == 'setor_gudang')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran ke Gudang berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('sukses') == 'transfer_owner')
        <script type="text/javascript">
            swal({
                title: 'Mantap!',
                text: 'Setoran Tunai ke Bank berhasil dilakukan!',
                timer: 3000,
                type: 'success'
            });
        </script>
    @elseif (session('gagal') == 'hapus')
        <script type="text/javascript">
            swal({
                title: 'Waduh!',
                text: 'Setoran Buka gagal dihapus!',
                timer: 3000,
                type: 'error'
            });
        </script>
    @endif

    <script type="text/javascript">
        $('#tableSetoranKasir').DataTable();
        $('#tableSetoranGrosir').DataTable();
        $('#tableSetoranOwner').DataTable();
        $('#tableSetoranOwner1').DataTable();

        
        
        $(document).on('click', '#approve', function(event) {
            event.preventDefault();

            var $tr = $(this).parents('tr').first();
            var id = $tr.attr('id');
            // console.log(id);
            $('#formApprove').find('form').attr('action', '{{ url("approve_owner") }}' + '/' + id);

            var nama = $tr.find('td').first().next().text();
            var jumlah = $tr.find('td').first().next().next().text();
            // $('input[name="id"]').val(id);

            swal({
                title: 'Setujui?',
                text: '\"Setoran dari ' + nama + ' sebesar ' + jumlah + '?\"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#009688',
                cancelButtonColor: '#ff5252',
                confirmButtonText: '<i class="fa fa-check"></i> Ya, Setujui!',
                cancelButtonText: '<i class="fa fa-close"></i> Batal'
            }).then(function() {
                //confirmed
                $('#formApprove').find('form').attr('action', '{{ url("approve_owner") }}' + '/' + id);
                $('#formApprove').find('form').submit();
            }, function(isConfirm) {
                //canceled
                console.log('gagal');
            });
        });

        $(document).on('keyup', '#nominal_buka', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->owner }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpanBuka').prop('disabled', false);
            } else {
                $('#btnSimpanBuka').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('keyup', '#nominal_setor', function(event) {
            event.preventDefault();

            var nominal = parseInt($(this).val().replace(/\D/g, ''), 10);
            var nominal_max = parseFloat({{ $laci->owner }});

            if (isNaN(nominal)) nominal = 0;
            if (isNaN(nominal_max)) nominal_max = 0;

            if (nominal <= nominal_max) {
                $(this).parents('.form-group').first().removeClass('has-error');
                $(this).val(nominal.toLocaleString());
                $('#btnSimpanSetor').prop('disabled', false);
            } else {
                $('#btnSimpanSetor').prop('disabled', true);
                $(this).parents('.form-group').first().addClass('has-error');
            }
        });

        $(document).on('change', 'select[name="pengirim"]', function(event) {
            event.preventDefault();

            var id = $(this).val();
            if(id == 'lain'){
                $('#nama_pengirim').val('');
                $('#nama_pengirim').prop('disabled', false);
            }else{
                $('#nama_pengirim').val('');
                $('#nama_pengirim').prop('disabled', true);
            }
        });
    </script>
@endsection
