<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nama', 'alamat', 'telepon', 'email', 'suplier_id'
	];

	public function suplier()
	{
		return $this->belongsTo('App\Suplier');
	}

	public function transaksi_pembelians()
	{
		return $this->hasMany('App\TransaksiPembelian');
	}
}
