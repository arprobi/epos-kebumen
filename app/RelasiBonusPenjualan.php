<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiBonusPenjualan extends Model
{
    protected $fillable = [
        'bonus_id', 'relasi_transaksi_penjualan_id', 'jumlah'
    ];

    public function bonus()
    {
        return $this->belongsTo('App\Item');
    }

    public function relasi_transaksi_penjualan()
    {
        return $this->belongsTo('App\RelasiTransaksiPenjualan');
    }
}
