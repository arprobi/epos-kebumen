<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $fillable = [
    	'nama'
    ];

    public function provinsi()
    {
    	return $this->belongsTo('App\Provinsi');
    }

    public function kecamatan()
    {
    	return $this->hasMany('App\Kecamatan');
    }

    // public function desas()
    // {
    //     return $this->hasMany('App\Kecamatan');
    // }
    public function desas()
    {
        return $this->hasManyThrough('App\Desa', 'App\Kecamatan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
