<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMasuk extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id', 'suplier_id', 'jumlah', 'harga_dasar'
    ];

    public function item()
    {
    	return $this->belongsTo('App\Item','item_id');
    }

    public function suplier()
    {
    	return $this->belongsTo('App\Suplier');
    }

    public function item_keluar()
    {
        return $this->hasMany('App\ItemKeluar');
    }
}
