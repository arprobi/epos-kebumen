<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Item1 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'nama_pendek', 'kode', 'jenis', 'satuan_1', 'satuan_2', 'satuan_3', 'satuan_4', 'satuan_5', 'satuan_6', 'suplier_1', 'suplier_2', 'suplier_3', 'suplier_4', 'suplier_5', 'keterangan'
    ];
}