<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Item6 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'nama', 'nama_pendek', 'kode', 'jenis', 'satuan_1', 'satuan_2', 'satuan_3', 'satuan_4', 'satuan_5', 'satuan_6', 'suplier_1', 'suplier_2', 'suplier_3', 'suplier_4', 'suplier_5', 'keterangan'
    // ];
    protected $fillable = [
        'kode',
        'kode_barang',
        'nama',
        'nama_pendek',
        'suplier_id',
        'jenis_item_id',
        'stoktotal',
        'konsinyasi',
        'aktif',
        'retur',
        'bonus',
        'limit_stok',
        'limit_grosir',
        'diskon',
        'profit',
        'profit_eceran',
        'rentang'
    ];

    public function jenis_item()
    {
        return $this->belongsTo('App\JenisItem');
    }
}
