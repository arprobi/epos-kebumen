<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiPenyesuaianPersediaan extends Model
{
    public function penyesuaian_persediaan() {
		return $this->belongsTo('App\PenyesuaianPersediaan');
	}

	public function item()
	{
		return $this->belongsTo('App\Item', 'item_kode', 'kode');
	}
}
