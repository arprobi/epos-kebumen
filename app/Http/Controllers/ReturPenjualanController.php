<?php

namespace App\Http\Controllers;

use Auth;

use App\Bank;
use App\Akun;
use App\Arus;
use App\Item;
use App\Stok;
use App\User;
use App\Util;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\RelasiSatuan;
use App\ReturPenjualan;
use App\TransaksiPenjualan;
use App\RelasiReturPenjualan;
use App\RelasiTransaksiPenjualan;
use App\PembayaranReturPenjualan;

use Illuminate\Http\Request;

class ReturPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function all()
    {
        $bayar = array();
        $pembayaran_data = array();
        $retur_penjualan = ReturPenjualan::all();

        foreach ($retur_penjualan as $i => $data) {
            $bayar[$i] = 0;
            if($data->total_uang_keluar > 0){
                $bayar[$i] = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $data->id)->first();
                $pembayaran_data[$i] = $pembayaran;
            }elseif($data->total_uang_masuk > 0){
                $bayar[$i] = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $data->id)->first();
                $pembayaran_data[$i] = $pembayaran;
            }else{
                $pembayaran_data[$i] = null;
            }
        }
        // return $retur_penjualan;

        return view('retur_penjualan.all', compact('retur_penjualan', 'pembayaran_data', 'bayar'));
    } */

    public function all()
    {
        return view('retur_penjualan.all');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'retur_penjualans.' . $field;
        if ($request->field == 'kode_transaksi') {
            $field = 'transaksi_penjualans.kode_transaksi';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $retur_penjualans = ReturPenjualan
            ::select(
                'retur_penjualans.id',
                'retur_penjualans.created_at',
                'retur_penjualans.kode_retur',
                'transaksi_penjualans.kode_transaksi'
            )
            ->leftJoin('transaksi_penjualans', 'transaksi_penjualans.id', 'retur_penjualans.transaksi_penjualan_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('retur_penjualans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('retur_penjualans.kode_retur', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = ReturPenjualan
            ::select(
                'retur_penjualans.id'
            )
            ->leftJoin('transaksi_penjualans', 'transaksi_penjualans.id', 'retur_penjualans.transaksi_penjualan_id')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('retur_penjualans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('retur_penjualans.kode_retur', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($retur_penjualans as $i => $retur_penjualan) {

            $buttons['detail'] = null;
            $buttons['cek'] = null;
            $buttons['bg'] = null;

            // $retur_penjualan->mdt_kode_transaksi = $retur_penjualan->transaksi_penjualan->kode_transaksi;

            $bayar = 0;
            $pembayaran_data = null;

            if ($retur_penjualan->total_uang_keluar > 0) {

                $bayar = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
                $pembayaran_data = $pembayaran;

            }
            else if ($retur_penjualan->total_uang_masuk > 0) {

                $bayar = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
                $pembayaran_data = $pembayaran;

            }

            $buttons['detail'] = ['url' => url('transaksi/'.$retur_penjualan->transaksi_penjualan_id.'/retur/'.$retur_penjualan->id)];

            if ($bayar == 1) {

                if ($pembayaran_data->cek_lunas == 0 ||
                    $pembayaran_data->cek_lunas == 0) {

                        if ($pembayaran_data->cek_lunas == 0 &&
                            $pembayaran_data->nominal_cek > 0) {

                            $buttons['cek'] = ['url' => ''];

                        }

                        if ($pembayaran_data->bg_lunas == 0 &&
                            $pembayaran_data->nominal_bg > 0) {

                            $buttons['bg'] = ['url' => ''];

                        }
                    }

            }

            // return $buttons;
            $retur_penjualan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $retur_penjualans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function index($id)
    {
        $bayaran = array();
        $pembayaran_data = array();
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $retur_penjualan = ReturPenjualan::where('transaksi_penjualan_id', $id)->get();
        // return $retur_penjualan;
        foreach ($retur_penjualan as $i => $data) {
            $bayaran[$i] = 0;
            if($data->total_uang_keluar > 0){
                $bayaran[$i] = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $data->id)->first();
                $pembayaran_data[$i] = $pembayaran;
            }elseif($data->total_uang_masuk > 0){
                $bayaran[$i] = 1;
                $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $data->id)->first();
                $pembayaran_data[$i] = $pembayaran;
            }else{
                $bayaran[$i] = 0;
                $pembayaran_data[$i] = null;
            }
        }

        // return $pembayaran_data;
        return view('retur_penjualan.index', compact('retur_penjualan', 'transaksi_penjualan', 'pembayaran_data', 'pembayaran', 'bayaran'));
    }

    public function lastJson()
    {
        $retur_penjualan = ReturPenjualan::all()->last();
        return response()->json(['retur_penjualan' => $retur_penjualan]);
    }

    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->where('stoktotal', '>', 0)->get();
        return response()->json(['item' => $item]);
    }

    public function hargaJson($transaksi_penjualan_id, $item_kode)
    {
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('item_kode', $item_kode)->where('transaksi_penjualan_id', $transaksi_penjualan_id)->first();
        return response()->json(['relasi_transaksi_penjualan' => $relasi_transaksi_penjualan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::with('item', 'transaksi_penjualan')->where('transaksi_penjualan_id', $id)->get();
        $items = Item::distinct()->select('kode', 'nama')->get();
        $satuans = Satuan::all();

        foreach ($relasi_transaksi_penjualan as $i => $relasi) {
            $retur_penjualan_ids = ReturPenjualan::where('transaksi_penjualan_id', $relasi->transaksi_penjualan_id)
                                    ->pluck('id');
            $total_retur = RelasiReturPenjualan::whereIn('retur_penjualan_id', $retur_penjualan_ids)
                            ->where('item_kode', $relasi->item_kode)->sum('jumlah');
            
            $max_retur = intval($relasi->jumlah) - intval($total_retur);
            $relasi->max_retur = $max_retur;
        }
        // return $relasi_transaksi_penjualan;

        return view('retur_penjualan.create', compact('transaksi_penjualan', 'relasi_transaksi_penjualan', 'items', 'satuans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = TransaksiPenjualan::find($request->transaksi_penjualan_id);

        $persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $penjualan = Akun::where('kode', Akun::Penjualan)->first();
        $ppn_outcome = Akun::where('kode', Akun::PPNKeluaran)->first();
        $aset = Akun::where('kode', Akun::Aset)->first();
        $harta_lancar = Akun::where('kode', Akun::HartaLancar)->first();
        $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $hpp = Akun::where('kode', Akun::HPP)->first();

        $data = array();
        foreach ($request->item_retur as $i => $kode) {
            $item = Item::where('kode', $kode)->get()->first();
            $item->stoktotal += $request->jumlah_retur[$i];
            $item->update();

            if ($request->aksi_retur[$i] == 'ganti_item') {
                // Nambah stok pada tabel stok
                for ($j = 0; $j < $request->jumlah_retur[$i]; $j++) {
                    $stoks = Stok::where('item_kode', $kode)
                                ->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
                    $stoks->jumlah += 1;
                    $stoks->update();
                }

                // Nambah stok total di tabel item
                for ($j = 0; $j < $request->jumlah_ganti[$i]; $j++) {
                    $item = Item::where('kode', $kode)->orderBy('updated_at', 'desc')->first();
                    $item->stoktotal += 1;
                    $item->update();
                }

                // Update Akun dan Tambah Jurnal

            } else if ($request->aksi_retur[$i] == 'ganti_uang') {
                $stok = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
                $harga_beli = $stok->harga;
                $harga_ganti = $request->harga_ganti[$i];

                $persediaan->debet += $harga_beli;
                $persediaan->update();
                Jurnal::create([
                    'kode_akun' => $persediaan->kode,
                    'referensi' => $request->kode_retur,
                    'keterangan' => 'Retur Penjualan',
                    'debet' => $harga_beli
                ]);

                $penjualan->kredit -= $harga_ganti / 11 * 10;
                $penjualan->update();
                Jurnal::create([
                    'kode_akun' => $penjualan->kode,
                    'referensi' => $request->kode_retur,
                    'keterangan' => 'Retur Penjualan',
                    'debet' => $harga_ganti / 11 * 10
                ]);

                $ppn_outcome->kredit -= $harga_ganti / 11;
                $ppn_outcome->update();
                Jurnal::create([
                    'kode_akun' => $ppn_outcome->kode,
                    'referensi' => $request->kode_retur,
                    'keterangan' => 'Retur Penjualan',
                    'debet' => $harga_ganti / 11
                ]);

                $hpp->debet -= $harga_beli;
                $hpp->update();
                Jurnal::create([
                    'kode_akun' => $hpp->kode,
                    'referensi' => $request->kode_retur,
                    'keterangan' => 'Retur Penjualan',
                    'kredit' => $harga_beli
                ]);

                $aset->debet -= $harga_ganti;
                $aset->update();
                $harta_lancar->debet -= $harga_ganti;
                $harta_lancar->update();
                $kas_tunai->debet -= $harga_ganti;
                $kas_tunai->update();
                Jurnal::create([
                    'kode_akun' => $kas_tunai->kode,
                    'referensi' => $request->kode_retur,
                    'keterangan' => 'Retur Penjualan',
                    'kredit' => $harga_ganti
                ]);
            }

            $data[$kode] = [
                'jumlah_retur' => $request->jumlah_retur[$i],
            ];
        }

        // return $data;

        $retur_penjualan = new ReturPenjualan();
        $retur_penjualan->kode_retur = $request->kode_retur;
        $retur_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
        $retur_penjualan->user_id = Auth::id();

        if ($retur_penjualan->save()) {
            $retur = ReturPenjualan::all()->last();
            $retur_penjualan_id = $retur->id;

            foreach ($request->item_retur as $i => $kode) {
                $relasi_retur_penjualan = new RelasiReturPenjualan();
                $relasi_retur_penjualan->retur_penjualan_id = $retur_penjualan_id;
                $relasi_retur_penjualan->item_kode = $kode;
                $relasi_retur_penjualan->jumlah = $data[$kode]['jumlah_retur'];
                $relasi_retur_penjualan->keterangan = $request->keterangan_retur[$i];
                if ($request->aksi_retur[$i] == 'ganti_uang') $relasi_retur_penjualan->subtotal = $request->harga_ganti[$i];

                $relasi_retur_penjualan->save();
            }

            return redirect('/transaksi/'.$transaksi_penjualan->id.'/retur')->with('sukses', 'tambah');
        } else {
            return redirect('/transaksi/'.$transaksi_penjualan->id.'/retur')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($transaksi_penjualan_id, $retur_penjualan_id)
    {
        $bayar = 0;
        $retur_penjualan = ReturPenjualan::find($retur_penjualan_id);
        $transaksi_penjualan = TransaksiPenjualan::find($transaksi_penjualan_id);
        $relasi_retur_penjualan = RelasiReturPenjualan::where('retur_penjualan_id', $retur_penjualan_id)->get();
        if($retur_penjualan->total_uang_keluar > 0){
            $bayar =1 ;
            $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
        }elseif($retur_penjualan->total_uang_masuk > 0){
            $bayar =1 ;
            $pembayaran = PembayaranReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->first();
        }
        // return $pembayaran;
        $items = array();
        $hasil = array();
        foreach ($relasi_retur_penjualan as $i => $data) {
            // $items[$i] = Item::find($data->item_id)->kode;
            $satuans[$i] = RelasiSatuan::where('item_kode', $data->item_kode)->orderBy('konversi', 'dsc')->get();
            $sisa_jumlah[$i] = $data->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                if($hitung > 0){
                    $a = $a + 1;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                    $hasil[$i]['harga'] = $data->subtotal / $hitung;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }
        // return $hasil;

        return view('retur_penjualan.show', compact('retur_penjualan', 'transaksi_penjualan', 'relasi_retur_penjualan', 'hasil', 'pembayaran', 'bayar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cekLunas($id)
    {
        $pembayaran = PembayaranReturPenjualan::find($id);
        $returPembelian = ReturPenjualan::find($pembayaran->retur_penjualan_id);
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_pembayaran = Akun::where('kode', Akun::PembayaranProses)->first();
        $bank = Bank::find($pembayaran->bank_cek)->first();
        
        //ngurangi nominal bank
        $bank->nominal -= $pembayaran->nominal_cek;
        $bank->update();
        
        //ngurangi akun bank
        $akun_bank->debet -= $pembayaran->nominal_cek;
        $akun_bank->update();
        
        //ngurangi akun pembayaran
        $akun_pembayaran->kredit -= $pembayaran->nominal_cek;
        $akun_pembayaran->update();
        
        //update kolom akif cek
        $pembayaran->cek_lunas = 1;
        $pembayaran->update();
        
        //jurnal debet pembayaran
        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $returPembelian->kode_retur,
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$returPembelian->kode_retur,
            'debet' => $pembayaran->nominal_cek
        ]);
        //jurnal kredit bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $returPembelian->kode_retur,
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$returPembelian->kode_retur,
            'kredit' => $pembayaran->nominal_cek
        ]);
        //kolom arus kas
        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $pembayaran->nominal_cek
        ]);


        return redirect('/transaksi/'.$returPembelian->transaksi_penjualan_id.'/retur/'.$returPembelian->id)->with('sukses', 'cek');
    }

    public function bgLunas($id)
    {
        $pembayaran = PembayaranReturPenjualan::find($id);
        // return $pembayaran;
        $returPembelian = ReturPenjualan::find($pembayaran->retur_penjualan_id);
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_pembayaran = Akun::where('kode', Akun::PembayaranProses)->first();
        $bank = Bank::find($pembayaran->bank_bg)->first();
        
        //ngurangi nominal bank
        $bank->nominal -= $pembayaran->nominal_bg;
        $bank->update();
        
        //ngurangi akun bank
        $akun_bank->debet -= $pembayaran->nominal_bg;
        $akun_bank->update();
        
        //ngurangi akun pembayaran
        $akun_pembayaran->kredit -= $pembayaran->nominal_bg;
        $akun_pembayaran->update();
        
        //update kolom akif bg
        $pembayaran->bg_lunas = 1;
        $pembayaran->update();
        
        //jurnal debet pembayaran
        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $returPembelian->kode_retur,
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$returPembelian->kode_retur,
            'debet' => $pembayaran->nominal_bg
        ]);
        
        //jurnal kredit bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $returPembelian->kode_retur,
            'keterangan' => 'Penyesuaian Pembayaran Dalam Proses '.$returPembelian->kode_retur,
            'kredit' => $pembayaran->nominal_bg
        ]);
        
        //kolom arus kas
        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $pembayaran->nominal_bg
        ]);


        return redirect('/transaksi/'.$returPembelian->transaksi_penjualan_id.'/retur/'.$returPembelian->id)->with('sukses', 'bg');
    }

}
