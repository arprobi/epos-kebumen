<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Piutang;
use App\CashDrawer;
use App\MoneyLimit;
use App\SetoranBuka;
use App\RelasiSatuan;
use App\TransaksiPenjualan;
use App\PenyesuaianPersediaan;
use App\RelasiTransaksiPenjualan;
use App\RelasiPenyesuaianPersediaan;

use Illuminate\Http\Request;

class PenyesuaianPersediaanController extends Controller
{
    public function lastJson()
    {
        $penyesuaian_persediaan = PenyesuaianPersediaan::all()->last();
        return response()->json(['penyesuaian_persediaan' => $penyesuaian_persediaan]);
    }

    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->get()->first();
        // $stok = Item::select(DB::raw('SUM(stoktotal) as stok'))->groupBy('kode')->having('kode', '=', $kode)->get()->first();
        $stok = Item::select(DB::raw('stoktotal as stok'))->groupBy('kode')->having('kode', '=', $kode)->get()->first();
        $harga = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
        return response()->json(['item' => $item, 'stok' => $stok, 'harga' => $harga]);
    }

    public function KonversiJson($kode, $satuan)
    {
        $satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $satuan)->first();
        return response()->json(['satuan' => $satuan]);
    }

    public function countJson($kode, $jumlah)
    {
        $subtotal = 0;
        $hitung = 0;
        $status = 0;
        $stok = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->get();

        $stok_jumlah = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->sum('jumlah');
        // return $stok_jumlah;
        if ($stok_jumlah >= $jumlah) {
            for($i=0; $i < $jumlah; $i++){
                $hitung++;
                if($stok[$status]->jumlah >= $hitung){
                    $subtotal += $stok[$status]->harga;
                }else{
                    $hitung = 0;
                    $status++;
                    $subtotal += $stok[$status]->harga;
                }
            }
            $harga_satuan = $subtotal/$jumlah;
            
            return response()->json(['jumlah' => $jumlah, 'subtotal' => $subtotal, 'harga_satuan' => $harga_satuan]);
        } else {
            $jumlah = 0;
            $subtotal = 0;
            $harga_satuan = 0;
            return response()->json(['jumlah' => $jumlah, 'subtotal' => $subtotal, 'harga_satuan' => $harga_satuan]);
        }
    }

    /* public function index()
    {
        // $money_limit = MoneyLimit::find(1);
        // $cash = CashDrawer::where('user_id', Auth::id())->first();
        // $banks = Bank::all();
        $items = Item::distinct()->select('kode', 'nama')->where('stoktotal', '>', 0)->get();
        // $satuans = Satuan::all();
        $datas = PenyesuaianPersediaan::all();

        // return view('penyesuaian_persediaan.index', compact('items', 'satuans', 'banks', 'datas'));
        return view('penyesuaian_persediaan.index', compact('items', 'datas'));
    } */

    public function index()
    {
        $items = Item::distinct()->select('kode', 'nama')->where('stoktotal', '>', 0)->get();
        return view('penyesuaian_persediaan.index', compact('items'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'penyesuaian_persediaans.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $penyesuaian_persediaans = PenyesuaianPersediaan
            ::select(
                'penyesuaian_persediaans.id',
                'penyesuaian_persediaans.created_at',
                'penyesuaian_persediaans.kode_transaksi',
                'penyesuaian_persediaans.nominal',
                'penyesuaian_persediaans.status'
            )
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('penyesuaian_persediaans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('penyesuaian_persediaans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('penyesuaian_persediaans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('penyesuaian_persediaans.status', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = PenyesuaianPersediaan
            ::select(
                'penyesuaian_persediaans.id'
            )
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('penyesuaian_persediaans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('penyesuaian_persediaans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('penyesuaian_persediaans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('penyesuaian_persediaans.status', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($penyesuaian_persediaans as $i => $penyesuaian_persediaan) {

            $penyesuaian_persediaan->nominal = Util::duit($penyesuaian_persediaan->nominal);

            $buttons['detail'] = ['url' => url('penyesuaian_persediaan/'.$penyesuaian_persediaan->id.'/show')];

            // return $buttons;
            $penyesuaian_persediaan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $penyesuaian_persediaans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request) 
    {
        // return $request->all();
        $data = array();

        foreach ($request->item_kode as $i => $kode) {  
            // Get jumlah item per pieces
            $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
            $konversi = $relasi_satuan->konversi;
            $jumlah = $request->jumlah[$i] * $konversi;

            // modif stok
            for ($j = 0; $j < $jumlah; $j++) {
                $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
                if($request->status == 'keluar'){
                    $stoks->jumlah -= 1;

                    // $items = Item::where('kode', $kode)->first();
                    $cari_item = Item::where('kode', $kode)->first();
                    $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                    foreach ($items as $key => $item) {
                        $item->stoktotal -= 1;
                        $item->update();
                    }
                    $stoks->update();
                }elseif($request->status == 'masuk'){
                    $stoks->jumlah += 1;

                    // $items = Item::where('kode', $kode)->get();
                    $cari_item = Item::where('kode', $kode)->first();
                    $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                    foreach ($items as $key => $item) {
                        $item->stoktotal += 1;
                        $item->update();
                    }
                    $stoks->update();
                }
            }

            $data[$kode] = [
                'harga' => $request->harga[$i],
                'subtotal' => $request->subtotal[$i],
                'jumlah' => $jumlah,
            ];
        }

        $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $jurnal_rugi = new Jurnal();
        $jurnal_untung = new Jurnal();
        $jurnal_persediaan = new Jurnal();
        if($request->status == 'keluar'){
            // $akun_rugi_persediaan = Akun::where('kode', Akun::BebanKerugianPersediaan)->first();
            // $akun_rugi_persediaan->debet += $request->harga_total;
            // $akun_rugi_persediaan->update();

            $akun_persediaan->debet -= $request->harga_total;

            $jurnal_rugi->kode_akun = Akun::BebanKerugianPersediaan;
            $jurnal_rugi->referensi = $request->kode_transaksi;
            $jurnal_rugi->debet = $request->harga_total;
            $jurnal_rugi->keterangan = $request->keterangan;
            $jurnal_rugi->save();

            $jurnal_persediaan->kode_akun = Akun::Persediaan;
            $jurnal_persediaan->referensi = $request->kode_transaksi;
            $jurnal_persediaan->kredit = $request->harga_total;
            $jurnal_persediaan->keterangan = $request->keterangan;
            $jurnal_persediaan->save();

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'debet' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::BebanKerugianPersediaan,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'kredit' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => $request->keterangan,
                'debet' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' =>  $request->keterangan,
                'kredit' => $request->harga_total
            ]);

            $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $laba_tahan->kredit -= $request->harga_total;
            $laba_tahan->update();
        }elseif($request->status == 'masuk'){
            // $akun_untung_persediaan = Akun::where('kode', Akun::UntungPersediaan)->first();
            // $akun_untung_persediaan->kredit += $request->harga_total;
            // $akun_untung_persediaan->update();

            $akun_persediaan->debet += $request->harga_total;

            $jurnal_persediaan->kode_akun = Akun::Persediaan;
            $jurnal_persediaan->referensi = $request->kode_transaksi;
            $jurnal_persediaan->debet = $request->harga_total;
            $jurnal_persediaan->keterangan = $request->keterangan;
            $jurnal_persediaan->save();

            $jurnal_rugi->kode_akun = Akun::UntungPersediaan;
            $jurnal_rugi->referensi = $request->kode_transaksi;
            $jurnal_rugi->kredit = $request->harga_total;
            $jurnal_rugi->keterangan = $request->keterangan;
            $jurnal_rugi->save();

            Jurnal::create([
                'kode_akun' => Akun::UntungPersediaan,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'debet' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'kredit' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => $request->keterangan,
                'debet' => $request->harga_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' =>  $request->keterangan,
                'kredit' => $request->harga_total
            ]);

            $laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $laba_tahan->kredit += $request->harga_total;
            $laba_tahan->update();
        }
        $akun_persediaan->update();

        //store penyesuaian_persediaan
        $penyesuaian_persediaan = new PenyesuaianPersediaan();

        $penyesuaian_persediaan->kode_transaksi = $request->kode_transaksi;
        $penyesuaian_persediaan->nominal = $request->harga_total;
        $penyesuaian_persediaan->user_id = Auth::id();
        $penyesuaian_persediaan->status = $request->status;

        if ($penyesuaian_persediaan->save()) {
            $transaksi = PenyesuaianPersediaan::all()->last();
            $transaksi_id = $transaksi->id;

            foreach ($request->item_kode as $i => $kode) {
                $relasi_penyesuaian_persediaan = new RelasiPenyesuaianPersediaan();
                $relasi_penyesuaian_persediaan->penyesuaian_persediaan_id = $transaksi_id;
                $relasi_penyesuaian_persediaan->item_kode = $kode;
                $relasi_penyesuaian_persediaan->jumlah = $data[$kode]['jumlah'];
                $relasi_penyesuaian_persediaan->harga = $data[$kode]['harga'];
                $relasi_penyesuaian_persediaan->subtotal = $data[$kode]['subtotal'];

                $relasi_penyesuaian_persediaan->save();
            }
        }

        return redirect('/penyesuaian_persediaan')->with('berhasil', 'tambah');
    }

    public function show($id) 
    {
        $penyesuaian_persediaan = PenyesuaianPersediaan::find($id);
        $relasi_penyesuaian_persediaan = RelasiPenyesuaianPersediaan::where('penyesuaian_persediaan_id', $id)->get();

        $items = array();
        $hasil = array();
        foreach ($relasi_penyesuaian_persediaan as $i => $data) {
            // $items[$i] = Item::find($data->item_id)->kode;
            $items[$i] = Item::where('kode', $data->item_kode)->first()->kode;
            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$i])->orderBy('konversi', 'dsc')->get();
            $sisa_jumlah[$i] = $data->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                if($hitung > 0){
                    $a = $a + 1;
                    $hasil[$i][$a]['jumlah'] = $hitung;
                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }
        // return $hasil;

        return view('penyesuaian_persediaan.show', compact('hasil', 'relasi_penyesuaian_persediaan', 'penyesuaian_persediaan'));
    }

}
