<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;

use App\Util;
// use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class GrafikLabaController extends Controller
{
    public function index()
    {
        return view('grafik.laba', compact(''));
    }

    public function tahun($tahun)
    {
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $datas = array();

        for($i = 1; $i <= 12; $i++){
            $awal_ = new DateTime($tahun.'-'.$i.'-01');
            $awal = $awal_->format('Y-m-d');

            if($i == 12){
                $akhir_ = new DateTime(($tahun+1).'-01-01');
                $akhir = $akhir_->format('Y-m-d');
            }else{
                $akhir_ = new DateTime($tahun.'-'.($i+1).'-01');
                $akhir = $akhir_->format('Y-m-d');
            }
            $cal = LabaRugiController::mhitung($awal, $akhir);
            $val = $cal['laba_sebelum_pajak'];
            $val = str_replace('.', '', $val);
            $val = str_replace(',', '.', $val);

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }

    public function bulan($tahun, $bulan)
    {
        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $datas = array();

        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            $awal_ = new DateTime($tahun.'-'.$bulan.'-'.$i);
            $awal = $awal_->format('Y-m-d');
                
            if($i == intval($last_day)){
                $akhir_ = (new DateTime($tahun.'-'.$bulan.'-'.$i))->modify('+1 day');
                $akhir = $akhir_->format('Y-m-d');
            }else{
                $akhir_ = new DateTime($tahun.'-'.$bulan.'-'.($i+1));
                $akhir = $akhir_->format('Y-m-d');
            }

            $cal = LabaRugiController::mhitung($awal, $akhir);
            $val = $cal['laba_sebelum_pajak'];
            $val = str_replace('.', '', $val);
            $val = str_replace(',', '.', $val);

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }

    public function rentang($awal, $akhir)
    {
        $awal_ = (new DateTime($awal))->modify('+1 day');
        $add_awal = (new DateTime($awal))->modify('+2 day');
        $akhir = new DateTime($akhir);

        $interval = date_diff($akhir, $awal_);
        $labels = array();
        $datas = array();

        for ($i = 0; $i <= $interval->days; $i++) {
            $val = 0;

            if ($i == 0 ) {
                $start = $awal_->add(new DateInterval('P0D'));
                $end = $add_awal->add(new DateInterval('P0D'));
            } else {
                $start = $awal_->add(new DateInterval('P1D'));
                $end = $add_awal->add(new DateInterval('P1D'));
            }

            $tanggal = $start->format('d m Y');
            $start = $start->format('Y-m-d');
            $end = $end->format('Y-m-d');

            $cal = LabaRugiController::mhitung($start, $end);
            if($cal != NULL){
                $val = $cal['laba_sebelum_pajak'];
                $val = str_replace('.', '', $val);
                $val = str_replace(',', '.', $val);
            }

            array_push($labels, \App\Util::tanggal($tanggal));
            array_push($datas, $val);
        }
        return response()->json(compact('labels', 'datas'));
    }

    public function tanggal($tanggal)
    {
        $labels = array();
        $datas = array();
        for ($i = 6; $i <= 22; $i++) {
            $val = 0;
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $jam = $label;
            $label .= '.00';
            array_push($labels, $label);

            $cal = LabaRugiController::todayHitung($tanggal, $jam);
            if($cal != NULL){
                $val = $cal['laba_sebelum_pajak'];
                $val = str_replace('.', '', $val);
                $val = str_replace(',', '.', $val);
            }

            array_push($datas, $val);
        }

        return response()->json(compact('labels', 'datas'));
    }
}
