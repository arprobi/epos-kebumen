<?php

namespace App\Http\Controllers;

use App\Level;

use Illuminate\Http\Request;

class LevelController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$levels = Level::all();
		return view ('level.index', compact('levels'));
	}

	public function store(Request $request) {
    	$this->validate($request,[
    		'kode' => 'required|max:255',
    		'nama' => 'required|max:255'
    	]);

    	$level = new Level();
    	$level->kode = $request->kode;
    	$level->nama = $request->nama;
    	if ($level->save()) {
            return redirect('/level')->with('sukses', 'tambah');
        } else {
            return redirect('/level')->with('gagal', 'tambah');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255'
        ]);

        $level = Level::find($id);
        $level->kode = $request->kode;
        $level->nama = $request->nama;
        if ($level->save()) {
            return redirect('/level')->with('sukses', 'ubah');
        } else {
            return redirect('/level')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $level = Level::find($id);
            if ($level->delete()) {
                return redirect('/level')->with('sukses', 'hapus');
            } else {
                return redirect('/level')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/level')->with('gagal', 'hapus');
        }
    }
}
