<?php

namespace App\Http\Controllers;

use App\Item;
use App\Suplier;
use App\ItemMasuk;

use Illuminate\Http\Request;

class ItemMasukController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$item_masuks = ItemMasuk::all();
		return view('item_masuk.index', compact('item_masuks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$items = Item::all();
		$supliers = Suplier::all();
		return view('item_masuk.create', compact('items', 'supliers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'harga_dasar' => 'required|integer|min:1'
		]);

		$item_masuk = new ItemMasuk();
		$item_masuk->item_id = $request->item_id;
		$item_masuk->suplier_id = $request->suplier_id;
		$item_masuk->jumlah = $request->jumlah;
		$item_masuk->harga_dasar = $request->harga_dasar;
		if ($item_masuk->save()) {
			$item = Item::find($item_masuk->item_id);
			$item->stok += $item_masuk->jumlah;
			$item->save();

			return redirect('/item-masuk')->with('sukses', 'tambah');
		} else {
			return redirect('/item-masuk')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$item_masuk = ItemMasuk::find($id);
		return view('item_masuk.show', compact('item_masuk'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$item_masuk = ItemMasuk::find($id);
		$items = Item::all();
		$supliers = Suplier::all();
		return view('item_masuk.edit', compact('item_masuk', 'items', 'supliers'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'jumlah' => 'required|integer|min:1',
			'harga_dasar' => 'required|integer|min:1'
		]);

		$item_masuk = ItemMasuk::find($id);
		$stok = $item_masuk->jumlah;
		$item_masuk->item_id = $request->item_id;
		$item_masuk->suplier_id = $request->suplier_id;
		$item_masuk->jumlah = $request->jumlah;
		$item_masuk->harga_dasar = $request->harga_dasar;
		if ($item_masuk->save()) {
			$item = Item::find($item_masuk->item_id);
			$item->stok += $item_masuk->jumlah - $stok;
			$item->save();

			return redirect('/item-masuk')->with('sukses', 'ubah');
		} else {
			return redirect('/item-masuk')->with('gagal', 'ubah');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	// public function destroy($id)
	// {
	//     $item_masuk = ItemMasuk::find($id);
	//     if ($item_masuk->delete()) {
	//         return redirect('/item-masuk')->with('sukses', 'hapus');
	//     } else {
	//         return redirect('/item-masuk')->with('gagal', 'hapus');
	//     }
	// }
}
