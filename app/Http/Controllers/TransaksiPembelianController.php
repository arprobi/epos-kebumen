<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Item;
use App\Stok;
use App\Laci;
use App\Util;
use App\Harga;
use App\Jurnal;
use App\Kredit;
use App\Notice;
use App\Satuan;
use App\Seller;
use App\Suplier;
use App\LogLaci;
use App\RelasiBundle;
use App\RelasiSatuan;
use App\CashBackPembelian;
use App\TransaksiPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class TransaksiPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $temp_transaksi_pembelians = TransaksiPembelian::where('po', 0)
            ->where('kode_transaksi', 'like', '%TPM%')
            ->orderBy('created_at', 'desc')->get();

        $transaksi_pembelians = [];
        foreach ($temp_transaksi_pembelians as $i => $transaksi_pembelian) {
            if (strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]) == 'tpm') {
                array_push($transaksi_pembelians, $transaksi_pembelian);
            }
        }
        return view('transaksi_pembelian.index', compact('transaksi_pembelians'));
    } */

    public function index()
    {
        return view('transaksi_pembelian.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_pembelians.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $transaksi_pembelians = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id',
                'transaksi_pembelians.created_at',
                'transaksi_pembelians.kode_transaksi',
                'transaksi_pembelians.nota',
                'transaksi_pembelians.harga_total',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 0)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPM/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.nota', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 0)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPM/%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.nota', 'like', '%'.$request->search_query.'%')
                ->orWhere('transaksi_pembelians.harga_total', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($transaksi_pembelians as $i => $transaksi_pembelian) {

            $transaksi_pembelian->harga_total = Util::duit($transaksi_pembelian->harga_total);

            $buttons['detail'] = ['url' => url('transaksi-pembelian/'.$transaksi_pembelian->id)];

            if (in_array(Auth::user()->level_id, [1, 2])) {
                $buttons['penyesuaian'] = ['url' => url('transaksi-pembelian/create/'.$transaksi_pembelian->id)];
            } else {
                $buttons['penyesuaian'] = null;
            }

            if ($transaksi_pembelian->sisa_utang > 0) {
                $buttons['bayar'] = ['url' => url('hutang/show/'.$transaksi_pembelian->id)];
            } else {
                $buttons['bayar'] = null;
            }

            if ($transaksi_pembelian->aktif_cek == 1) {
                $buttons['cek'] = ['url' => ''];
            } else {
                $buttons['cek'] = null;
            }

            if ($transaksi_pembelian->aktif_bg == 1) {
                $buttons['bg'] = ['url' => ''];
            } else {
                $buttons['bg'] = null;
            }

            // return $buttons;
            $transaksi_pembelian->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $transaksi_pembelians,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function lastJson()
    {
        // $transaksi_pembelian = TransaksiPembelian::where('kode_transaksi', 'like', '%TPM%')->get()->last();
        $transaksi_pembelian = TransaksiPembelian::where('kode_transaksi', 'like', '%TPM/%')->get()->last();
        return response()->json([
            'transaksi_pembelian' => $transaksi_pembelian
        ]);
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        // $transaksi_pembelian_terakhir = TransaksiPembelian::where('kode_transaksi', 'like', '%TPM%')->get()->last();
        $transaksi_pembelian_terakhir = TransaksiPembelian::where('kode_transaksi', 'like', '%TPM/%')->get()->last();
        if ($transaksi_pembelian_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPM/'.$tanggal;
        } else {
            $kode_transaksi_pembelian_terakhir = $transaksi_pembelian_terakhir->kode_transaksi;
            $tanggal_transaksi_pembelian_terakhir = substr($kode_transaksi_pembelian_terakhir, 9, 10);
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_pembelian_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_pembelian_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPM/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPM/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supliers = Suplier::whereHas('items', function ($query) {
                $query->whereIn('konsinyasi', [0, 3]);
            })->whereNotIn('id', [Suplier::notSuplier])->orderBy('nama', 'asc')->get();
        $kredits = Kredit::all();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();
        $laci = Laci::find(1);
        return view('transaksi_pembelian.create', compact('supliers', 'kredits', 'banks', 'ceks', 'bgs', 'laci'));
    }

    public function createAgain($id)
    {
        $supliers = Suplier::whereHas('items', function ($query) {
                $query->whereIn('konsinyasi', [0, 3]);
            })->whereNotIn('id', [Suplier::notSuplier])->orderBy('nama', 'asc')->get();
        $kredits = Kredit::all();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();
        $laci = Laci::find(1);

        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();
        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $id)->orderBy('id', 'asc')->get();
        // return $relasi_transaksi_pembelian;
        // $stoks = Stok::with('item')->where('transaksi_pembelian_id', $id)->where('retur', 0)->get();
        $stoks = Stok::with('item')->where('transaksi_pembelian_id', $id)->where('retur_pembelian_id', null)->orderBy('id', 'asc')->get();

        $cek_terpilih = null;
        if ($transaksi_pembelian->bank_cek == null && $transaksi_pembelian->nominal_cek > 0) {
            $cek_terpilih = Cek::where('nominal', $transaksi_pembelian->nominal_cek)->where('nomor', $transaksi_pembelian->no_cek)->first();
        }

        $bg_terpilih = null;
        if ($transaksi_pembelian->bank_bg == null && $transaksi_pembelian->nominal_bg > 0) {
            $bg_terpilih = BG::where('nominal', $transaksi_pembelian->nominal_bg)->where('nomor', $transaksi_pembelian->no_bg)->first();
        }

        return view('transaksi_pembelian.create_again', compact('supliers', 'kredits', 'banks', 'ceks', 'bgs', 'laci', 'transaksi_pembelian', 'relasi_transaksi_pembelian', 'stoks', 'cek_terpilih', 'bg_terpilih'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        // return $request->utang;
        // return $request->jumlah_bayar - $request->harga_total;
        if ($request->beli_po == 1) {
            $trapem = TransaksiPembelian::where('kode_transaksi', $request->kode_transaksi)->first();
            $reltrapem = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $trapem->id)->delete();
            $trapem->delete();
        }

        $kas_keluar = 0;

        $jumlah_stok = 0;
        $data = array();
        $stok_masuk = array();
        /*if ($request->seller_id != NULL) {
            $seller = $request->seller_id;
        } else {
            $seller_save = new Seller();
            $seller_save->nama = $request->seller_name;
            $seller_save->suplier_id = $request->suplier_id;
            $seller_save->save();

            $seller = Seller::latest()->first()->id;
        }*/

        // $harta_lancar = Akun::where('kode', Akun::HartaLancar)->first();
        $persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $ppn_income = Akun::where('kode', Akun::PPNMasukan)->first();
        $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $kartu_kredit = Akun::where('kode', Akun::KartuKredit)->first();
        $hutang_dagang = Akun::where('kode', Akun::HutangDagang)->first();
        $potongan_pembelian = Akun::where('kode', Akun::PotonganPembelian)->first();
        $beban_ongkir = Akun::where('kode', Akun::BebanOngkir)->first();

        $temp_persediaan = 0;
        $temp_ppn_income = 0;
        $temp_kas_tunai = 0;
        $temp_kas_bank = 0;
        $temp_kas_cek = 0;
        $temp_kas_bg = 0;
        $temp_kartu_kredit = 0;
        $temp_hutang_dagang = 0;
        $temp_potongan_pembelian = 0;
        $temp_beban_ongkir = 0;

        $index_bundle = 0;
        $bundle_change = [];

        $perlu_notice = false;
        if($request->ppn_total == null || $request->ppn_total == 0){
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();
                $limit_grosir = Item::where('kode_barang', $cari_item->kode_barang)->first()->limit_grosir;
                $satuan_eceran_grosir = 0;
                $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
                
                if ($limit_grosir != 0) {
                    foreach ($relasi_satuan_item as $i => $relasi) {
                        if($limit_grosir % $relasi->konversi == 0){
                            $satuan_eceran_grosir = $relasi->satuan_id;
                            break;
                        }
                    }
                }

                $plus_grosir = $items[0]->plus_grosir;

                foreach ($items as $j => $item) {
                    $item->stoktotal += $request->jumlah[$i];
                    $item->update();

                    array_push($item_kodes, $item->kode);
                }

                $temp_persediaan += $request->harga[$i] * $request->jumlah[$i];

                $data[''.$id] = [
                    'subtotal' => $request->subtotal[$i],
                    'harga' => $request->harga[$i],
                    'jumlah' => $request->jumlah[$i]
                ];

                // Update harga jual jika terjadi selisih dengan harga beli
                $stok_terakhir = Stok::where('item_kode', $cari_item->kode)->orderBy('created_at', 'desc')->first();
                if ($stok_terakhir != null) {
                    $harga_beli_lama = 1.1 * $stok_terakhir->harga;
                    $harga_beli_baru = $request->subtotal[$i] / $request->jumlah[$i];
                    if (abs($harga_beli_baru - $harga_beli_lama) > $cari_item->rentang) {
                        $perlu_notice = true;
                        // Naikkan harga jual menjadi sama dengan harga beli
                        $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                        foreach ($hargas as $j => $harga) {
                            $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                            $konversi = $relasi_satuan->konversi;

                            $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                            $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                            $harga->eceran = $harga_eceran;
                            $harga->grosir = $harga_grosir;
                            $harga->update();
                        }

                        $bundle_change[$index_bundle] = $item_kodes;
                        $index_bundle += 1;
                    }
                } else {
                    $perlu_notice = true;
                    $harga_beli_baru = $request->subtotal[$i] / $request->jumlah[$i];
                    $hargas = Harga::whereIn('item_kode', $item_kodes)->get();

                    foreach ($hargas as $j => $harga) {
                        $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                        $konversi = $relasi_satuan->konversi;

                        $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                        $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                        $harga->eceran = $harga_eceran;
                        $harga->grosir = $harga_grosir;
                        $harga->update();
                    }

                    $bundle_change[$index_bundle] = $item_kodes;
                    $index_bundle += 1;
                }

                if($satuan_eceran_grosir != 0 && $perlu_notice){
                    foreach ($item_kodes as $i => $item_kode) {
                        $satuans = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();

                        $is_grosir = false;
                        foreach ($satuans as $a => $satuan) {
                            if ($satuan_eceran_grosir == $satuan->satuan_id) {
                                $is_grosir = true;
                            }

                            if ($is_grosir) {
                                $harga = Harga::where('item_kode', $item_kode)->where('satuan_id', $satuan->satuan_id)->first();

                                $harga->eceran = $harga->grosir + $plus_grosir;
                                $harga->update();
                            }
                        }
                    }
                }

            }
            foreach ($request->item_stok as $k => $id_stok) {
                $cari_item = Item::find($id_stok);
                $kadaluarsa = '';
                if ($request->kadaluarsa_stok[$k] != '') {
                    $kadaluarsas = explode('-', $request->kadaluarsa_stok[$k]);
                    $dd = $kadaluarsas[0];
                    $mm = $kadaluarsas[1];
                    $yyyy = $kadaluarsas[2];
                    $kadaluarsa = join('-', [$yyyy, $mm, $dd]);
                }

                $stok_masuk[$k] = [
                    'item_id' => $id_stok,
                    'item_kode' => $cari_item->kode,
                    'harga' => $request->hpp_stok[$k],
                    'jumlah' => $request->jumlah_stok[$k],
                    'rusak' => 0,
                    'kadaluarsa' => $kadaluarsa,
                ];
            }
        }else{
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();
                $limit_grosir = Item::where('kode_barang', $cari_item->kode_barang)->first()->limit_grosir;
                $satuan_eceran_grosir = 0;
                $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
                
                if ($limit_grosir != 0) {
                    foreach ($relasi_satuan_item as $i => $relasi) {
                        if($limit_grosir % $relasi->konversi == 0){
                            $satuan_eceran_grosir = $relasi->satuan_id;
                            break;
                        }
                    }
                }

                $plus_grosir = $items[0]->plus_grosir;

                foreach ($items as $j => $item) {
                    $item->stoktotal += $request->jumlah[$i];
                    $item->update();
                    array_push($item_kodes, $item->kode);
                }

                $temp_persediaan += $request->subtotal[$i];
                $data[''.$id] = [
                    'subtotal' => $request->subtotal[$i],
                    'harga' => $request->harga[$i],
                    'jumlah' => $request->jumlah[$i]
                ];

                // Update harga jual jika terjadi selisih dengan harga beli
                $stok_terakhir = Stok::where('item_kode', $cari_item->kode)->orderBy('created_at', 'desc')->first();
                if ($stok_terakhir != null) {
                    $harga_beli_lama = 1.1 * $stok_terakhir->harga;
                    $harga_beli_baru = 1.1 * $request->subtotal[$i] / $request->jumlah[$i];
                    if (abs($harga_beli_baru - $harga_beli_lama) > $cari_item->rentang) {
                        $perlu_notice = true;
                        $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                        foreach ($hargas as $j => $harga) {
                            $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                            $konversi = $relasi_satuan->konversi;

                            $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                            $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                            $harga->eceran = $harga_eceran;
                            $harga->grosir = $harga_grosir;
                            $harga->update();
                        }

                        $bundle_change[$index_bundle] = $item_kodes;
                        $index_bundle += 1;
                    }
                } else {
                    $perlu_notice = true;
                    $harga_beli_baru = 1.1 * $request->subtotal[$i] / $request->jumlah[$i];
                    $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                    foreach ($hargas as $j => $harga) {
                        $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                        $konversi = $relasi_satuan->konversi;

                        $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                        $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                        $harga->eceran = $harga_eceran;
                        $harga->grosir = $harga_grosir;
                        $harga->update();
                    }

                    $bundle_change[$index_bundle] = $item_kodes;
                    $index_bundle += 1;
                }

                if($satuan_eceran_grosir != 0 && $perlu_notice){
                    foreach ($item_kodes as $i => $item_kode) {
                        $satuans = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();

                        $is_grosir = false;
                        foreach ($satuans as $a => $satuan) {
                            if ($satuan_eceran_grosir == $satuan->satuan_id) {
                                $is_grosir = true;
                            }

                            if ($is_grosir) {
                                $harga = Harga::where('item_kode', $item_kode)->where('satuan_id', $satuan->satuan_id)->first();

                                $harga->eceran = $harga->grosir + $plus_grosir;
                                $harga->update();
                            }
                        }
                    }
                }

                //what gonna wrong?
                $temp_subtotal = 0;
                foreach ($request->item_stok as $j => $item) {
                    if ($item == $id) {
                        $kadaluarsa = '';
                        if ($request->kadaluarsa_stok[$j] != '') {
                            $kadaluarsas = explode('-', $request->kadaluarsa_stok[$j]);
                            $dd = $kadaluarsas[0];
                            $mm = $kadaluarsas[1];
                            $yyyy = $kadaluarsas[2];
                            $kadaluarsa = join('-', [$yyyy, $mm, $dd]);
                        }

                        $temp_subtotal += $request->hpp_stok[$j] * $request->jumlah_stok[$j];

                        array_push($stok_masuk, [
                            'item_id' => $id,
                            'item_kode' => $cari_item->kode,
                            'harga' => $request->hpp_stok[$j],
                            'jumlah' => $request->jumlah_stok[$j],
                            'rusak' => 0,
                            'kadaluarsa' => $kadaluarsa,
                        ]);
                    }
                }

                if ( sizeof(explode('.', $temp_subtotal)) != 1){
                    $temp_subtotal = (float) ceil($temp_subtotal * 100) / 100;
                }

                $selisih_subtotal = $request->subtotal[$i] - $temp_subtotal;
                $selisih_subtotal = (float) (number_format($selisih_subtotal, 2));

                // return [$selisih_subtotal, ceil($request->subtotal[$i]*100)/100, ceil($temp_subtotal * 100) / 100];
                if($selisih_subtotal != 0){
                    $index_stok_trakhir = count($stok_masuk) - 1;
                    $stok_masuk[$index_stok_trakhir]['jumlah'] -= 1;
                    $harga = $stok_masuk[$index_stok_trakhir]['harga'] + $selisih_subtotal;

                    array_push($stok_masuk, [
                        'item_id' => $stok_masuk[$index_stok_trakhir]['item_id'],
                        'item_kode' => $stok_masuk[$index_stok_trakhir]['item_kode'],
                        'harga' => $harga,
                        'jumlah' => 1,
                        // 'aktif' => $stok_masuk[$index_stok_trakhir]['aktif'],
                        'rusak' => 0,
                        'kadaluarsa' => $stok_masuk[$index_stok_trakhir]['kadaluarsa'],
                    ]);
                }
            }
        }
        // return $stok_masuk;

        if($request->ppn_total != null && $request->ppn_total != '' && $request->ppn_total > 0){
            $request->harga_total += $request->ppn_total;
        }

        $selisih_bayar_total = $request->jumlah_bayar - $request->harga_total;
        if ($request->utang > 0) {
            if($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
                $temp_hutang_dagang += 0;
                $request->utang = 0;
            }
        }

        $transaksi_pembelian = new TransaksiPembelian();
        // $transaksi_pembelian->kode_transaksi = $request->kode_transaksi;
        $kode_transaksi = self::kodeTransaksiBaru();
        $transaksi_pembelian->kode_transaksi = $kode_transaksi;
        $transaksi_pembelian->nota = $request->nota;
        $transaksi_pembelian->suplier_id = $request->suplier_id;
        $transaksi_pembelian->seller_id = $request->seller_id;
        $transaksi_pembelian->po = $request->po;
        $transaksi_pembelian->potongan_pembelian = $request->potongan_pembelian;
        $transaksi_pembelian->ongkos_kirim = $request->ongkos_kirim;
        $transaksi_pembelian->harga_total = $request->harga_total;
        $transaksi_pembelian->jumlah_bayar = $request->jumlah_bayar;
        $transaksi_pembelian->utang = $request->utang;
        $transaksi_pembelian->sisa_utang = $request->utang;
        $transaksi_pembelian->user_id = Auth::user()->id;
        if($request->utang > 0){
            $transaksi_pembelian->jatuh_tempo = $request->jatuh_tempo;
        }
        if($request->ppn_total != null || $request->ppn_total == 0){
            $transaksi_pembelian->ppn_total = $request->ppn_total;
        }

        $temp_potongan_pembelian += $request->potongan_pembelian;
        $temp_beban_ongkir += $request->ongkos_kirim;

        $transaksi_pembelian->nominal_tunai = $request->nominal_tunai;
        $temp_kas_tunai += $request->nominal_tunai;
        
        $transaksi_pembelian->no_transfer = $request->no_transfer;
        $transaksi_pembelian->bank_transfer = $request->bank_transfer;
        $transaksi_pembelian->nominal_transfer = $request->nominal_transfer;
        $temp_kas_bank += $request->nominal_transfer;

        $transaksi_pembelian->no_kartu = $request->no_kartu;
        $transaksi_pembelian->bank_kartu = $request->bank_kartu;
        $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;
        $transaksi_pembelian->nominal_kartu = $request->nominal_kartu;

        $transaksi_pembelian->no_cek = $request->no_cek;
        $transaksi_pembelian->bank_cek = $request->bank_cek;
        $transaksi_pembelian->nominal_cek = $request->nominal_cek;
        $temp_kas_cek += $request->nominal_cek;

        $transaksi_pembelian->no_bg = $request->no_bg;
        $transaksi_pembelian->bank_bg = $request->bank_bg;
        $transaksi_pembelian->nominal_bg = $request->nominal_bg;
        $temp_kas_bg += $request->nominal_bg;

        $transaksi_pembelian->piutang = $request->nominal_piutang;
        // $transaksi_pembelian->kredit_id = $request->no_kredit;
        // $transaksi_pembelian->nominal_kredit = $request->nominal_kredit;
        // $temp_kartu_kredit += $request->nominal_kredit;
        $selisih_bayar_total = $request->jumlah_bayar - $request->harga_total;
        if ($request->utang > 0) {
            if($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
                $transaksi_pembelian->status_hutang = null;
                $temp_hutang_dagang += 0;
            }else{
                $transaksi_pembelian->status_hutang = 1;
                $temp_hutang_dagang += $request->utang;
            }
        } else {
            $transaksi_pembelian->status_hutang = null;
        }

        $persediaan->debet += $temp_persediaan;
        Jurnal::create([
            'kode_akun' => $persediaan->kode,
            'referensi' => $kode_transaksi,
            'keterangan' => 'Pembelian Item Dagang',
            'debet' => $temp_persediaan
        ]);

        if($request->ppn_total == null || $request->ppn_total == 0){
            $temp_ppn_income = $request->harga_total - $temp_persediaan;
        }else{
            $temp_ppn_income = $request->ppn_total;
        }
        // $ppn_income->debet += $temp_ppn_income;
        // dd($request->harga_total, $temp_ppn_income, $temp_persediaan);
        if($temp_ppn_income > 0){
            Jurnal::create([
                'kode_akun' => $ppn_income->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $temp_ppn_income
            ]);

            $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
            $akun_ppn_masuk->debet += $temp_ppn_income;
            $akun_ppn_masuk->update();
        }

        if ($temp_beban_ongkir > 0) {
            $beban_ongkir->debet += $temp_beban_ongkir;
            Jurnal::create([
                'kode_akun' => $beban_ongkir->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $temp_beban_ongkir
            ]);
        }

        if($selisih_bayar_total < 100 && $selisih_bayar_total > 0){
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_bayar_total
            ]);
        }elseif($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_bayar_total * -1
            ]);
        }

        if ($temp_kas_tunai > 0) {
            $kas_tunai->debet -= $temp_kas_tunai;
            Jurnal::create([
                'kode_akun' => $kas_tunai->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $temp_kas_tunai
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $temp_kas_tunai,
            ]);
        }

        if ($temp_kas_bank > 0 || $request->nominal_kartu > 0) {
            $keterangan_bank = 'Pembelian Item Dagang';
            $nominal_bank = 0;
            if ($temp_kas_bank > 0) {
                $kas_bank->debet -= $temp_kas_bank;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal -= $temp_kas_bank;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - '.$temp_kas_bank.' via '.$bank->nama_bank;
                $nominal_bank += $temp_kas_bank;
            }

            if ($request->nominal_kartu > 0) {
                $transaksi_pembelian->jenis_kartu = $request->jenis_kartu;

                if ($request->jenis_kartu == 'kredit') {
                    $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                    $akun_kartu->kredit += $request->nominal_kartu;
                    $akun_kartu->update();

                    Jurnal::create([
                        'kode_akun' => Akun::KartuKredit,
                        'referensi' => $kode_transaksi,
                        'keterangan' => 'Pembelian via kartu kredit',
                        'kredit' => $request->nominal_kartu
                    ]);
                } else {
                    $kas_bank->debet -= $request->nominal_kartu;

                    $bank = Bank::find($request->bank_kartu);
                    $bank->nominal -= $request->nominal_kartu;
                    $bank->update();

                    $keterangan_bank = $keterangan_bank.' - '.$request->nominal_kartu.' via '.$bank->nama_bank;

                    $nominal_bank += $request->nominal_kartu;
                }
            }

            Jurnal::create([
                'kode_akun' => $kas_bank->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => $keterangan_bank,
                'kredit' => $nominal_bank
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $nominal_bank,
            ]);
        }

        if ($temp_kas_cek > 0) {
            if ($request->bank_cek == NULL) {
                $kas_cek->debet -= $temp_kas_cek;

                $cek_id = Cek::find($request->cek_id);
                $cek_id->aktif = 0;
                $cek_id->update();

                Jurnal::create([
                    'kode_akun' => $kas_cek->kode,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang',
                    'kredit' => $temp_kas_cek
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $temp_kas_cek,
                ]);
            } else {
                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $temp_kas_cek;
                $akun->update();

                $cek = new Cek();
                $cek->nomor = $request->no_cek;
                $cek->nominal = $request->nominal_cek;
                $cek->aktif = 0;
                $cek->save();

                $transaksi_pembelian->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $temp_kas_cek
                ]);
            }
        }

        if ($temp_kas_bg > 0) {
            if ($request->bank_bg == NULL) {
                $kas_bg->debet -= $temp_kas_bg;
                Jurnal::create([
                    'kode_akun' => $kas_bg->kode,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang',
                    'kredit' => $temp_kas_bg
                ]);

                $bg_id = BG::find($request->bg_id);
                $bg_id->aktif = 0;
                $bg_id->update();

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $temp_kas_bg,
                ]);
            } else {
                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $temp_kas_bg;
                $akun->update();

                $bg = new BG();
                $bg->nomor = $request->no_bg;
                $bg->nominal = $request->nominal_bg;
                $bg->aktif = 0;
                $bg->save();

                $transaksi_pembelian->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $kode_transaksi,
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $temp_kas_bg
                ]);
            }
        }

        if ($request->nominal_piutang > 0) {
            $piutang = Akun::where('kode', Akun::PiutangDagang)->first();
            $piutang->debet -= $request->nominal_piutang;
            $piutang->update();

            Jurnal::create([
                'kode_akun' => Akun::PiutangDagang,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $request->nominal_piutang
            ]);

            $suplier = Suplier::find($request->suplier_id);
            $suplier->piutang -= $request->nominal_piutang;
            $suplier->update();
        }

        if ($temp_potongan_pembelian > 0) {
            $potongan_pembelian->kredit += $temp_potongan_pembelian;
            Jurnal::create([
                'kode_akun' => $potongan_pembelian->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $temp_potongan_pembelian
            ]);
        }

        if ($temp_hutang_dagang > 0) {
            $hutang_dagang->kredit += $temp_hutang_dagang;
            Jurnal::create([
                'kode_akun' => $hutang_dagang->kode,
                'referensi' => $kode_transaksi,
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $temp_hutang_dagang
            ]);
        }

        //jurnal ppn in pindah ke beban pajak
        // if($temp_ppn_income > 0){
        //     Jurnal::create([
        //         'kode_akun' => Akun::BebanPajak,
        //         'referensi' => $kode_transaksi.' Beban',
        //         'keterangan' => 'Pembelian Item Dagang PPN',
        //         'debet' => $temp_ppn_income
        //     ]);
        //     Jurnal::create([
        //         'kode_akun' => Akun::PPNMasukan,
        //         'referensi' => $kode_transaksi.' Beban',
        //         'keterangan' => 'Pembelian Item Dagang PPN',
        //         'kredit' => $temp_ppn_income
        //     ]);

        //     //jurnal laba rugi
        //     Jurnal::create([
        //         'kode_akun' => Akun::LabaRugi,
        //         'referensi' => $kode_transaksi.' L/R',
        //         'keterangan' => 'Pembelian Item Dagang PPN',
        //         'debet' => $temp_ppn_income
        //     ]);
        //     Jurnal::create([
        //         'kode_akun' => Akun::BebanPajak,
        //         'referensi' => $kode_transaksi.' L/R',
        //         'keterangan' => 'Pembelian Item Dagang PPN',
        //         'kredit' => $temp_ppn_income
        //     ]);

        //     Jurnal::create([
        //         'kode_akun' => Akun::LabaTahan,
        //         'referensi' => $kode_transaksi.' LDT',
        //         'keterangan' => 'Pembelian Item Dagang',
        //         'debet' => $temp_ppn_income
        //     ]);
        //     Jurnal::create([
        //         'kode_akun' => Akun::LabaRugi,
        //         'referensi' => $kode_transaksi.' LDT',
        //         'keterangan' => 'Pembelian Item Dagang',
        //         'kredit' => $temp_ppn_income
        //     ]);
        // }

        //jurnal laba ditahan
        // $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
        // $akun_laba_tahan->kredit -= $temp_ppn_income;
        // $akun_laba_tahan->update();

        if($selisih_bayar_total < 100 && $selisih_bayar_total > 0){
            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit -= $selisih_bayar_total;
            $akun_laba_tahan->update();

            //jurnal laba rugi
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' L/R-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'debet' => $selisih_bayar_total
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $kode_transaksi.' L/R-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'kredit' => $selisih_bayar_total
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' LDT-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'debet' => $selisih_bayar_total
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' LDT-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'kredit' => $selisih_bayar_total
            ]);
        }elseif($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_bayar_total * -1;
            $akun_laba_tahan->update();

            //jurnal laba rugi
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kode_transaksi.' L/R-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'debet' => $selisih_bayar_total * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' L/R-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'kredit' => $selisih_bayar_total * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' LDT-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'debet' => $selisih_bayar_total * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' LDT-SB',
                'keterangan' => 'Pembelian Item Dagang - Selisih Bayar',
                'kredit' => $selisih_bayar_total * -1
            ]);
        }

        if ($temp_potongan_pembelian > 0) {
            //jurnal laba rugi potongan pembelian
            Jurnal::create([
                'kode_akun' => Akun::PotonganPembelian,
                'referensi' => $kode_transaksi.' L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $temp_potongan_pembelian
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $temp_potongan_pembelian
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $temp_potongan_pembelian
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $temp_potongan_pembelian
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $temp_potongan_pembelian;
            $akun_laba_tahan->update();
        }

        if ($temp_beban_ongkir > 0) {
            //jurnal laba rugi potongan pembelian
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $temp_beban_ongkir
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanOngkir,
                'referensi' => $kode_transaksi.' L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $temp_beban_ongkir
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kode_transaksi.' LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $temp_beban_ongkir
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kode_transaksi.' LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $temp_beban_ongkir
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit -= $temp_beban_ongkir;
            $akun_laba_tahan->update();
        }

        if ($transaksi_pembelian->save() &&
            // $harta_lancar->update() &&
            $persediaan->update() &&
            // $ppn_income->update() &&
            $kas_tunai->update() &&
            $kas_bank->update() &&
            $kas_cek->update() &&
            $kas_bg->update() &&
            $hutang_dagang->update() &&
            $beban_ongkir->update()) {

            $transaksi_pembelian->items()->sync($data);
            $transaksi = TransaksiPembelian::latest()->first();
            foreach ($stok_masuk as $key => $value) {
                $stok = new Stok();
                $stok->item_id = $value['item_id'];
                $stok->item_kode = $value['item_kode'];
                if($value['harga'] == null || $value['harga'] == ''){
                    $stok->harga = 0;
                }else{
                    $stok->harga = $value['harga'];
                }
                $stok->transaksi_pembelian_id = $transaksi->id;
                $stok->jumlah = $value['jumlah'];
                $stok->jumlah_beli = $value['jumlah'];
                $stok->rusak = $value['rusak'];
                $stok->kadaluarsa = $value['kadaluarsa'];

                $stok->save();
            }
            // $transaksi_pembelian->stoks()->sync($stok_masuk);

            if ($perlu_notice) {
                // Insert new notice
                $notice = new Notice();
                $notice->transaksi_pembelian_id = $transaksi_pembelian->id;
                $notice->aktif = 1;
                $notice->save();

                foreach ($bundle_change as $a => $bundle) {
                    $cek_bundle = RelasiBundle::whereIn('item_child', $bundle)->get();
                    if (sizeof($cek_bundle) > 0) {
                        foreach ($cek_bundle as $i => $bundle_) {
                            ItemController::ubahHargaBundle($bundle_->item_parent);
                        }
                    }
                }
            }

            if(Auth::user()->level_id == 5 && $request->nominal_tunai > 0){
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 7;
                $log->nominal = $request->nominal_tunai;
                $log->save();

                $laci = Laci::find(1);
                $laci->gudang -= $request->nominal_tunai;
                $laci->update();
            }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
            {
                if($request->nominal_tunai > 0){
                    $log = new LogLaci();
                    $log->pengirim = Auth::user()->id;
                    // $log->penerima = $request->penerima;
                    $log->approve = 1;
                    $log->status = 7;
                    $log->nominal = $request->nominal_tunai;
                    $log->save();

                    $laci = Laci::find(1);
                    $laci->owner -= $request->nominal_tunai;
                    $laci->update();
                }
            }

            return redirect('/transaksi-pembelian')->with('sukses', 'tambah');
        } else {
            return redirect('/transaksi-pembelian')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'seller', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();
        // return $transaksi_pembelian;

        $stoks = Stok::where('transaksi_pembelian_id', $id)
            ->where('retur_pembelian_id', null)
            ->orderBy('id')
            ->get();

        if ($transaksi_pembelian == null ||
            strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]) != 'tpm') {
            return redirect('/transaksi-pembelian')->with('gagal', '404');
        }

        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)
            ->orderBy('id')
            ->get();

        $cashback = CashBackPembelian::where('transaksi_pembelian_id', $id)->first();
        
        // $items = array();
        // $hasil = array();
        // foreach ($relasi_transaksi_pembelian as $i => $data) {
        //     $items[$i] = Item::find($data->item_id)->kode;
        //     $satuans[$i] = RelasiSatuan::where('item_kode', $items[$i])->orderBy('konversi', 'dsc')->get();
        //     $sisa_jumlah[$i] = $data->jumlah;
        //     $a=-1;
        //     for($x=0; $x < count($satuans[$i]); $x++){
        //         $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
        //         if($hitung > 0){
        //             $a = $a + 1;
        //             $hasil[$i][$a]['jumlah'] = $hitung;
        //             $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
        //         }
        //         $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
        //     }
        // }
        // return [$relasi_transaksi_pembelian, $hasil];

        // $item_stoks = array();
        // $hasil_stoks = array();
        // foreach ($stoks as $i => $data) {
        //     $item_stoks[$i] = Item::find($data->item_id)->kode;
        //     $satuans[$i] = RelasiSatuan::where('item_kode', $item_stoks[$i])->orderBy('konversi', 'dsc')->get();
        //     $sisa_jumlah[$i] = $data->jumlah_beli;
        //     $a=-1;
        //     // return $sisa_jumlah[$i];
        //     for($x=0; $x < count($satuans[$i]); $x++){
        //         $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
        //         if($hitung > 0){
        //             $a = $a + 1;
        //             $hasil_stoks[$i][$a]['jumlah'] = $hitung;
        //             $hasil_stoks[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
        //         }
        //         $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
        //     }
        // }

        // return view('transaksi_pembelian.show', compact('hasil', 'transaksi_pembelian', 'relasi_transaksi_pembelian', 'stoks', 'hasil_stoks'));
        return view('transaksi_pembelian.show', compact('transaksi_pembelian', 'relasi_transaksi_pembelian', 'stoks', 'items', 'cashback'));
    }

    public function showAgain($id)
    {
        $notices = Notice::where('transaksi_pembelian_id', $id)->where('aktif', 1)->get();
        foreach ($notices as $i => $notice) {
            $notice->aktif = 0;
            $notice->update();
        }

        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'seller', 'items')
            ->where('id', $id)
            ->where('po', 0)
            ->first();
            // return [$transaksi_pembelian, strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1])];

        $stoks = Stok::where('transaksi_pembelian_id', $transaksi_pembelian->id)
            ->orderBy('id')
            ->get();
            // return $stoks;

        if ($transaksi_pembelian == null ||
            (strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]) != 'tpm' &&
            strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]) != 'tpmk'))
        {
            // return strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]);
            return redirect('/transaksi-pembelian')->with('gagal', '404');
        }

        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)
            ->orderBy('id')
            ->get();

        $items = array();
        $hasil = array();
        $item_index = 0;
        foreach ($relasi_transaksi_pembelian as $i => $data) {
            $harga_terakhir = Harga::where('item_kode', Item::find($data->item_id)->kode)->orderBy('updated_at', 'desc')->first();
            $timestamp1 = $transaksi_pembelian->updated_at;
            $timestamp2 = $harga_terakhir->updated_at;
            // $timestamp1 = new DateTime($timestamp1);
            // $timestamp2 = new DateTime($timestamp2);
            $timestamp1 = strtotime($timestamp1);
            $timestamp2 = strtotime($timestamp2);

            $selisih = abs($timestamp1 - $timestamp2);
            // return $selisih;
            if (strtolower(explode('/', $transaksi_pembelian->kode_transaksi)[1]) != 'tpmk') {
                if ($selisih <= 30) {
                    foreach ($stoks as $j => $stok) {
                        if ($stok->item_id == $data->item_id && $stok->aktif != 0) {
                            $items[$item_index] = Item::find($data->item_id);
                            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$item_index]->kode)->orderBy('konversi', 'dsc')->get();
                            $sisa_jumlah[$i] = $data->jumlah;
                            $a=-1;
                            for($x=0; $x < count($satuans[$i]); $x++){
                                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                                if($hitung > 0){
                                    $a = $a + 1;
                                    $hasil[$i][$a]['jumlah'] = $hitung;
                                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                                }
                                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
                            }

                            $relasi_satuans = RelasiSatuan::where('item_kode', $items[$item_index]->kode)->orderBy('konversi', 'desc')->get();
                            $hargas = array();
                            foreach ($relasi_satuans as $i => $relasi_satuan) {
                                array_push($hargas, Harga::where('item_kode', $items[$item_index]->kode)->where('satuan_id', $relasi_satuan->satuan_id)->first());
                            }

                            $items[$item_index]['hargas'] = $hargas;
                            $items[$item_index]['relasi_satuans'] = $relasi_satuans;
                            $item_index++;
                        }
                    }
                }
            } else {
            // return $stoks;
            // if ($i == 1)
            // return [$selisih, $data, $harga_terakhir];
                // if ($selisih <= 30) {
                    foreach ($stoks as $j => $stok) {
                        if ($stok->item_id == $data->item_id && $stok->aktif == 0) {
                            $items[$item_index] = Item::find($data->item_id);
                            $satuans[$i] = RelasiSatuan::where('item_kode', $items[$item_index]->kode)->orderBy('konversi', 'dsc')->get();
                            $sisa_jumlah[$i] = $data->jumlah;
                            $a=-1;
                            for($x=0; $x < count($satuans[$i]); $x++){
                                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                                if($hitung > 0){
                                    $a = $a + 1;
                                    $hasil[$i][$a]['jumlah'] = $hitung;
                                    $hasil[$i][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                                }
                                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
                            }

                            $relasi_satuans = RelasiSatuan::where('item_kode', $items[$item_index]->kode)->orderBy('konversi', 'desc')->get();
                            $hargas = array();
                            foreach ($relasi_satuans as $i => $relasi_satuan) {
                                array_push($hargas, Harga::where('item_kode', $items[$item_index]->kode)->where('satuan_id', $relasi_satuan->satuan_id)->first());
                            }

                            $items[$item_index]['hargas'] = $hargas;
                            $items[$item_index]['relasi_satuans'] = $relasi_satuans;
                            $item_index++;
                        }
                    }
                // }
            }
            // return $selisih;
        }
        // return [$hasil, $items];
        // return $relasi_transaksi_pembelian;

        $item_stoks = array();
        $hasil_stoks_index = 0;
        $hasil_stoks = array();
        foreach ($stoks as $i => $data) {
            $item_stoks[$i] = Item::find($data->item_id)->kode;
            $satuans[$i] = RelasiSatuan::where('item_kode', $item_stoks[$i])->orderBy('konversi', 'dsc')->get();
            $sisa_jumlah[$i] = $data->jumlah;
            $a=-1;
            for($x=0; $x < count($satuans[$i]); $x++){
                $hitung = explode('.', $sisa_jumlah[$i] / $satuans[$i][$x]->konversi)[0];
                if($hitung > 0){
                    $a = $a + 1;
                    $hasil_stoks[$hasil_stoks_index][$a]['jumlah'] = $hitung;
                    $hasil_stoks[$hasil_stoks_index][$a]['satuan'] = Satuan::find($satuans[$i][$x]->satuan_id)->kode;
                    $hasil_stoks_index++;
                } else {
                    $a = $a + 1;
                    $hasil_stoks[$hasil_stoks_index][$a]['jumlah'] = null;
                    $hasil_stoks_index++;
                }
                $sisa_jumlah[$i] = $sisa_jumlah[$i] % $satuans[$i][$x]->konversi;
            }
        }
        // return [$stoks, $hasil_stoks];

        return view('transaksi_pembelian.show_again', compact('hasil', 'transaksi_pembelian', 'relasi_transaksi_pembelian', 'stoks', 'hasil_stoks', 'items'));
    }

    /*public function showHHB($transaksi_pembelian_id, $item_id)
    {
        $transaksi_pembelian = TransaksiPembelian::with('suplier', 'items')->find($transaksi_pembelian_id);
        $relasi_transaksi_pembelian = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $transaksi_pembelian_id)->get();
        $item = Item::find($item_id);
        return view('transaksi_pembelian.show', compact('transaksi_pembelian', 'relasi_transaksi_pembelian', 'item'));
    }*/

    public function showItemJson($id)
    {
        $item = Item::with('satuan_pembelian')->find($id);
        $pcs = Satuan::where('kode', 'BIJI')->first();
        return response()->json([
            'item' => $item,
            'pcs' => $pcs
        ]);
    }

    public function showSuplierItemsJson($id)
    {
        $item_ = Item::where('suplier_id', $id)->where('aktif', 1)->where('valid_konversi', 1)->get();
        $items = [];

        foreach ($item_ as $i => $item) {
            $cek_relasi = RelasiSatuan::where('item_kode', $item->kode)->get();
            if (sizeof($cek_relasi) > 0 ) array_push($items, $item);
        }

        return response()->json([
            'items' => $items
        ]);
    }

    public function showSuplierSellersJson($id)
    {
        $sellers = Seller::where('suplier_id', $id)->get();
        return response()->json([
            'sellers' => $sellers
        ]);
    }

    public function suplierJson($id)
    {
        $suplier = Suplier::find($id);
        return response()->json([
            'suplier' => $suplier
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Edit($id)
    {
        $transaksi_pembelian = TransaksiPembelian::find($id);
        $supliers = Suplier::whereHas('items', function ($query) {
                $query->whereIn('konsinyasi', [0, 3]);
            })->whereNotIn('id', [Suplier::notSuplier])->orderBy('nama', 'asc')->get();

        return view('transaksi_pembelian.edit', compact('supliers', 'transaksi_pembelian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Update(Request $request, $id)
    {
        // return $request->all();
        $trapem = TransaksiPembelian::find($id);
        $trapem->nota = $request->nota; 
        $trapem->suplier_id = $request->suplier_id;
        $trapem->seller_id = $request->seller_id;
        $trapem->update();

        return redirect('/transaksi-pembelian/'.$id)->with('sukses', 'ubah1');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CairCek($id)
    {
        $transaksi_pembelian = TransaksiPembelian::find($id);
        $transaksi_pembelian->aktif_cek = 0;
        // $transaksi_pembelian->sisa_utang -= $transaksi_pembelian->nominal_cek;
        $transaksi_pembelian->update();

        $bank = Bank::find($transaksi_pembelian->bank_cek);
        $bank->nominal -= $transaksi_pembelian->nominal_cek;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $transaksi_pembelian->nominal_cek;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $transaksi_pembelian->nominal_cek;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $transaksi_pembelian->kode_transaksi.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Cek via Bank '.$bank->nama_bank,
            'debet' => $transaksi_pembelian->nominal_cek,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $transaksi_pembelian->kode_transaksi.'/CEK',
            'keterangan' => 'Penyesuaian Pembayaran Cek via Bank '.$bank->nama_bank,
            'kredit' => $transaksi_pembelian->nominal_cek,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $transaksi_pembelian->nominal_cek,
        ]);
        return redirect('/transaksi-pembelian/'.$id)->with('sukses', 'cek');
    }

    public function CairBG($id)
    {
        $transaksi_pembelian = TransaksiPembelian::find($id);
        $transaksi_pembelian->aktif_bg = 0;
        // $transaksi_pembelian->sisa_utang -= $transaksi_pembelian->nominal_;
        $transaksi_pembelian->update();

        $bank = Bank::find($transaksi_pembelian->bank_bg);
        $bank->nominal -= $transaksi_pembelian->nominal_bg;
        $bank->update();

        $bank_akun = Akun::where('kode', Akun::KasBank)->first();
        $bank_akun->debet -= $transaksi_pembelian->nominal_bg;
        $bank_akun->update();

        $pembayaran_proses = Akun::where('kode', Akun::PembayaranProses)->first();
        $pembayaran_proses->kredit -= $transaksi_pembelian->nominal_bg;
        $pembayaran_proses->update();

        Jurnal::create([
            'kode_akun' => Akun::PembayaranProses,
            'referensi' => $transaksi_pembelian->kode_transaksi.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran BG via Bank '.$bank->nama_bank,
            'debet' => $transaksi_pembelian->nominal_bg,
        ]);

        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $transaksi_pembelian->kode_transaksi.'/BG',
            'keterangan' => 'Penyesuaian Pembayaran Cek via Bank '.$bank->nama_bank,
            'kredit' => $transaksi_pembelian->nominal_bg,
        ]);

        Arus::create([
            'nama' => Arus::PBD,
            'nominal' => $transaksi_pembelian->nominal_bg,
        ]);
        return redirect('/transaksi-pembelian/'.$id)->with('sukses', 'bg');
    }

    public function SupplierIdJSON($id)
    {
        $suplier = Suplier::find($id);

        return response()->json([
            'suplier' => $suplier
        ]);
    }

    public function ubahJatuhTempo(Request $request, $id)
    {
        // return $request->all();
        $transaksi_pembelian = TransaksiPembelian::find($id);
        $transaksi_pembelian->jatuh_tempo = $request->jatuh_tempo_baru_;
        $transaksi_pembelian->update();

        return redirect('/transaksi-pembelian/'.$id)->with('sukses', 'jatuh_tempo');
    }

    public function storeAgain(Request $request, $id)
    {
        // return [$request->all(), $id];
        $kas_keluar = 0;
        $transaksi_pembelian_id = $id;
        $jumlah_stok = 0;
        $data = array();
        $rel_trapem = array();
        $stok_masuk = array();
        $temp_stok_masuk = array();
        $total_relasi_baru = 0;
        $index_bundle = 0;
        $bundle_change = [];

        $persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $ppn_income = Akun::where('kode', Akun::PPNMasukan)->first();
        $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $kartu_kredit = Akun::where('kode', Akun::KartuKredit)->first();
        $hutang_dagang = Akun::where('kode', Akun::HutangDagang)->first();
        $potongan_pembelian = Akun::where('kode', Akun::PotonganPembelian)->first();
        $beban_ongkir = Akun::where('kode', Akun::BebanOngkir)->first();

        $temp_persediaan_lama = 0;
        $selisih_persediaan = 0;

        $temp_persediaan = 0;
        $temp_ppn_income = 0;
        $temp_kas_tunai = 0;
        $temp_kas_bank = 0;
        $temp_kas_cek = 0;
        $temp_kas_bg = 0;
        $temp_kartu_kredit = 0;
        $temp_hutang_dagang = 0;
        $temp_potongan_pembelian = 0;
        $temp_beban_ongkir = 0;

        $perlu_notice = false;
        if($request->ppn_total == null || $request->ppn_total == 0){
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();

                $temp_persediaan += $request->harga[$i] * $request->jumlah[$i];

                $data[''.$id] = [
                    'subtotal' => $request->subtotal[$i],
                    'harga' => $request->harga[$i],
                    'jumlah' => $request->jumlah[$i]
                ];
                $total_relasi_baru += $request->subtotal[$i];
            }

            foreach ($request->item_stok as $k => $id_stok) {
                $cari_item = Item::find($id_stok);
                $kadaluarsa = '';
                if ($request->kadaluarsa_stok[$k] != '') {
                    $kadaluarsas = explode('-', $request->kadaluarsa_stok[$k]);
                    $dd = $kadaluarsas[0];
                    $mm = $kadaluarsas[1];
                    $yyyy = $kadaluarsas[2];
                    $kadaluarsa = join('-', [$yyyy, $mm, $dd]);
                }

                $stok_masuk[$k] = [
                    'item_id' => $id_stok,
                    'item_kode' => $cari_item->kode,
                    'harga' => $request->hpp_stok[$k],
                    'jumlah' => $request->jumlah_stok[$k],
                    'jumlah_beli' => $request->jumlah_stok[$k],
                    'rusak' => 0,
                    'kadaluarsa' => $kadaluarsa,
                ];
            }
        }else{
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();
                // foreach ($items as $j => $item) {
                //     $item->stoktotal += $request->jumlah[$i];
                //     $item->update();
                //     array_push($item_kodes, $item->kode);
                // }

                $temp_persediaan += $request->subtotal[$i];
                $data[''.$id] = [
                    'subtotal' => $request->subtotal[$i],
                    'harga' => $request->harga[$i],
                    'jumlah' => $request->jumlah[$i]
                ];
                $total_relasi_baru += $request->subtotal[$i];

                $temp_subtotal = 0;
                foreach ($request->item_stok as $j => $item) {
                    if ($item == $id) {
                        $kadaluarsa = '';
                        if ($request->kadaluarsa_stok[$j] != '') {
                            $kadaluarsas = explode('-', $request->kadaluarsa_stok[$j]);
                            $dd = $kadaluarsas[0];
                            $mm = $kadaluarsas[1];
                            $yyyy = $kadaluarsas[2];
                            $kadaluarsa = join('-', [$yyyy, $mm, $dd]);
                        }

                        $temp_subtotal += $request->hpp_stok[$j] * $request->jumlah_stok[$j];

                        if($request->jumlah_stok[$j] > 0){
                            array_push($stok_masuk, [
                                'item_id' => $id,
                                'item_kode' => $cari_item->kode,
                                'harga' => $request->hpp_stok[$j],
                                'jumlah' => $request->jumlah_stok[$j],
                                'jumlah_beli' => $request->jumlah_stok[$j],
                                'rusak' => 0,
                                'kadaluarsa' => $kadaluarsa,
                            ]);
                        }
                    }
                }

                if ( sizeof(explode('.', $temp_subtotal)) != 1){
                    $temp_subtotal = (float) ceil($temp_subtotal * 100) / 100;
                }

                $selisih_subtotal = $request->subtotal[$i] - $temp_subtotal;
                $selisih_subtotal = (float) (number_format($selisih_subtotal, 2));

                if($selisih_subtotal != 0){
                    $index_stok_trakhir = count($stok_masuk) - 1;
                    $harga = $stok_masuk[$index_stok_trakhir]['harga'] + $selisih_subtotal;
                    $stok_masuk[$index_stok_trakhir]['jumlah'] -= 1;
                    $stok_masuk[$index_stok_trakhir]['jumlah_beli'] -= 1;

                    array_push($stok_masuk, [
                        'item_id' => $stok_masuk[$index_stok_trakhir]['item_id'],
                        'item_kode' => $stok_masuk[$index_stok_trakhir]['item_kode'],
                        'harga' => $harga,
                        'jumlah' => 1,
                        'jumlah_beli' => 1,
                        'rusak' => 0,
                        'kadaluarsa' => $stok_masuk[$index_stok_trakhir]['kadaluarsa'],
                    ]);
                }
            }
        }

        //hitung jumlah persediaan baru
        $temp_persediaan_baru = 0;
        $selisih_stok_harga = 0;
        foreach ($stok_masuk as $i => $stok)
         {
            $temp_persediaan_baru += ($stok['harga'] * $stok['jumlah']);
        }

        //hitung jumlah persediaan lama
        $stok_lama = Stok::where('transaksi_pembelian_id', $transaksi_pembelian_id)->where('retur_pembelian_id', null)->get();
        $stok_berkurang = array();
        foreach ($stok_lama as $i => $stok) {
            $temp_persediaan_lama += ($stok->harga * $stok->jumlah_beli);

            if ($stok->jumlah != $stok->jumlah_beli) {
                $selisih_jumlah = $stok->jumlah_beli - $stok->jumlah;
                array_push($stok_berkurang, [
                    'item_id' => $stok->item_id,
                    'item_kode' => $stok->item_kode,
                    'harga' => $stok->harga,
                    'jumlah' => $selisih_jumlah,
                ]);
            }
        }
        // return [$stok_lama, $stok_masuk];

        //menyamakan stok baru yang masuk dengan selisih stok lama
        $stok_sudah_dipakai = array();
        foreach ($stok_berkurang as $i => $stok) {
            foreach ($stok_masuk as $key => $_stok) {
                if ($stok['item_id'] == $_stok['item_id']
                    && $stok['item_kode'] == $_stok['item_kode']
                    && $_stok['jumlah'] >= $stok['jumlah']) 
                {
                    if (!in_array($_stok['item_id'], $stok_sudah_dipakai)) {
                        $_stok['jumlah'] -= $stok['jumlah'];
                        $stok_masuk[$key] = $_stok;
                        $selisih = ($stok['jumlah'] * $stok['harga']) - ($stok['jumlah'] * $_stok['harga']);
                        $selisih_stok_harga += $selisih;
                        array_push($stok_sudah_dipakai, $_stok['item_id']);
                        break;
                    }
                }
            }
        }

        // return $selisih_stok_harga;
        $temp_persediaan_lama = round($temp_persediaan_lama * 100)/100;
        $temp_persediaan_baru = round($temp_persediaan_baru * 100)/100;
        // return [$temp_persediaan_lama, $temp_persediaan_baru];

        //selisih persediaan lama dan baru
        $selisih_persediaan = $temp_persediaan_baru - $temp_persediaan_lama;

        $transaksi_pembelian_lama = TransaksiPembelian::find($transaksi_pembelian_id);
        $transaksi_pembelian_compare = clone $transaksi_pembelian_lama;
        $relasi_transaksi_pembelian_lama = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $transaksi_pembelian_id)->get();
        $stoks_lama = Stok::where('transaksi_pembelian_id', $transaksi_pembelian_id)->where('retur_pembelian_id', null)->get();
        
        $total_relasi_lama = 0;
        foreach ($relasi_transaksi_pembelian_lama as $i => $relasi) {
            $total_relasi_lama += $relasi->subtotal;
            $relasi->delete();
        }

        foreach ($stoks_lama as $i => $stok) {
            $item = Item::find($stok->item_id);
            $items = Item::where('kode_barang', $item->kode_barang)->get();
            
            foreach ($items as $i => $_item) {
                $_item->stoktotal -= $stok->jumlah_beli;
                $_item->update();
            }

            $stok->delete();
        }

        // return 'aio';
        if($request->ppn_total == null || $request->ppn_total == 0){
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();
                $limit_grosir = Item::where('kode_barang', $cari_item->kode_barang)->first()->limit_grosir;
                $satuan_eceran_grosir = 0;
                $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
                
                foreach ($items as $j => $item) {
                    array_push($item_kodes, $item->kode);
                }

                if ($limit_grosir != 0) {
                    foreach ($relasi_satuan_item as $i => $relasi) {
                        if($limit_grosir % $relasi->konversi == 0){
                            $satuan_eceran_grosir = $relasi->satuan_id;
                            break;
                        }
                    }
                }

                $plus_grosir = $items[0]->plus_grosir;
                // Update harga jual jika terjadi selisih dengan harga beli
                $stok_terakhir = Stok::where('item_kode', $cari_item->kode)->orderBy('created_at', 'desc')->first();
                // return $stok_terakhir;
                if ($stok_terakhir != null) {
                    $harga_beli_lama = 1.1 * $stok_terakhir->harga;
                    $harga_beli_baru = $request->subtotal[$i] / $request->jumlah[$i];
                    if (abs($harga_beli_baru - $harga_beli_lama) > $cari_item->rentang) {
                        $perlu_notice = true;
                        // Naikkan harga jual menjadi sama dengan harga beli
                        $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                        foreach ($hargas as $j => $harga) {
                            $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                            $konversi = $relasi_satuan->konversi;

                            $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                            $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                            $harga->eceran = $harga_eceran;
                            $harga->grosir = $harga_grosir;
                            $harga->update();
                        }

                        $bundle_change[$index_bundle] = $item_kodes;
                        $index_bundle += 1;
                    }
                } else {
                    // return 'uihyf';
                    $perlu_notice = true;
                    $harga_beli_baru = $request->subtotal[$i] / $request->jumlah[$i];
                    $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                    foreach ($hargas as $j => $harga) {
                        $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                        $konversi = $relasi_satuan->konversi;

                        $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                        $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                        $harga->eceran = $harga_eceran;
                        $harga->grosir = $harga_grosir;
                        $harga->update();
                    }
                    $bundle_change[$index_bundle] = $item_kodes;
                    $index_bundle += 1;
                }

                if($satuan_eceran_grosir != 0 && $perlu_notice){
                    foreach ($item_kodes as $i => $item_kode) {
                        $satuans = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();

                        $is_grosir = false;
                        foreach ($satuans as $a => $satuan) {
                            if ($satuan_eceran_grosir == $satuan->satuan_id) {
                                $is_grosir = true;
                            }

                            if ($is_grosir) {
                                $harga = Harga::where('item_kode', $item_kode)->where('satuan_id', $satuan->satuan_id)->first();

                                $harga->eceran = $harga->grosir + $plus_grosir;
                                $harga->update();
                            }
                        }
                    }
                }
            }
        }else{
            foreach ($request->item_id as $i => $id) {
                $cari_item = Item::find($id);
                $items = Item::where('kode_barang', $cari_item->kode_barang)->get();
                $item_kodes = array();
                $limit_grosir = Item::where('kode_barang', $cari_item->kode_barang)->first()->limit_grosir;
                $satuan_eceran_grosir = 0;
                $relasi_satuan_item = RelasiSatuan::where('item_kode', $items[0]->kode)->orderBy('konversi', 'dsc')->get();
                
                foreach ($items as $j => $item) {
                    array_push($item_kodes, $item->kode);
                }

                if ($limit_grosir != 0) {
                    foreach ($relasi_satuan_item as $i => $relasi) {
                        if($limit_grosir % $relasi->konversi == 0){
                            $satuan_eceran_grosir = $relasi->satuan_id;
                            break;
                        }
                    }
                }

                $plus_grosir = $items[0]->plus_grosir;
                $temp_persediaan += $request->subtotal[$i];

                // Update harga jual jika terjadi selisih dengan harga beli
                $stok_terakhir = Stok::where('item_kode', $cari_item->kode)->orderBy('created_at', 'desc')->first();
                if ($stok_terakhir != null) {
                    $harga_beli_lama = 1.1 * $stok_terakhir->harga;
                    $harga_beli_baru = 1.1 * $request->subtotal[$i] / $request->jumlah[$i];
                    if (abs($harga_beli_baru - $harga_beli_lama) > $cari_item->rentang) {
                        $perlu_notice = true;
                        $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                        foreach ($hargas as $j => $harga) {
                            $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                            $konversi = $relasi_satuan->konversi;

                            $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                            $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                            $harga->eceran = $harga_eceran;
                            $harga->grosir = $harga_grosir;
                            $harga->update();
                        }

                        $bundle_change[$index_bundle] = $item_kodes;
                        $index_bundle += 1;
                    }
                } else {
                    $perlu_notice = true;
                    $harga_beli_baru = 1.1 * $request->subtotal[$i] / $request->jumlah[$i];
                    $hargas = Harga::whereIn('item_kode', $item_kodes)->get();
                    foreach ($hargas as $j => $harga) {
                        $relasi_satuan = RelasiSatuan::where('item_kode', $harga->item_kode)->where('satuan_id', $harga->satuan_id)->first();
                        $konversi = $relasi_satuan->konversi;

                        $harga_eceran = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit_eceran / 100) / 100) * 100;
                        $harga_grosir = ceil($harga_beli_baru * $konversi * (1 + $relasi_satuan->item->profit / 100) / 100) * 100;

                        $harga->eceran = $harga_eceran;
                        $harga->grosir = $harga_grosir;
                        $harga->update();
                    }

                    $bundle_change[$index_bundle] = $item_kodes;
                    $index_bundle += 1;
                }

                if($satuan_eceran_grosir != 0 && $perlu_notice){
                    foreach ($item_kodes as $i => $item_kode) {
                        $satuans = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();

                        $is_grosir = false;
                        foreach ($satuans as $a => $satuan) {
                            if ($satuan_eceran_grosir == $satuan->satuan_id) {
                                $is_grosir = true;
                            }

                            if ($is_grosir) {
                                $harga = Harga::where('item_kode', $item_kode)->where('satuan_id', $satuan->satuan_id)->first();

                                $harga->eceran = $harga->grosir + $plus_grosir;
                                $harga->update();
                            }
                        }
                    }
                }
            }
        }

        if ($perlu_notice) {
            // Insert new notice
            foreach ($bundle_change as $a => $bundle) {
                $cek_bundle = RelasiBundle::whereIn('item_child', $bundle)->get();
                if (sizeof($cek_bundle) > 0) {
                    foreach ($cek_bundle as $i => $bundle_) {
                        ItemController::ubahHargaBundle($bundle_->item_parent);
                    }
                }
            }
        }

        $transaksi_pembelian_lama->items()->sync($data);
        foreach ($stok_masuk as $key => $value) {
            if($value['jumlah'] > 0){
                $item = Item::find($value['item_id']);
                $items = Item::where('kode_barang', $item->kode_barang)->get();
                
                foreach ($items as $i => $_item) {
                    $_item->stoktotal += $value['jumlah_beli'];
                    $_item->update();
                }

                $stok = new Stok();
                $stok->item_id = $value['item_id'];
                $stok->item_kode = $value['item_kode'];
                if($value['harga'] == null || $value['harga'] == ''){
                    $stok->harga = 0;
                }else{
                    $stok->harga = $value['harga'];
                }
                $stok->transaksi_pembelian_id = $transaksi_pembelian_id;
                $stok->jumlah = $value['jumlah'];
                $stok->jumlah_beli = $value['jumlah_beli'];
                $stok->rusak = $value['rusak'];
                $stok->kadaluarsa = $value['kadaluarsa'];

                $stok->save();
            }
        }

        // return [$selisih_stok_harga,  $selisih_persediaan];

        //jika $selisih_persediaan positif => akun persediaan bertambah, harga total juga berubah,

        //jika $selisih_persediaan negatif => akun persediaan berkurang, harga total juga berubah,

        if($request->ppn_total != null && $request->ppn_total != ''){
            $request->harga_total += $request->ppn_total;
        }

        $selisih_bayar_total = $request->jumlah_bayar - $request->harga_total;
        if ($request->utang > 0) {
            if($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
                $temp_hutang_dagang += 0;
                $request->utang = 0;
            }
        }

        $transaksi_pembelian_lama->kode_transaksi = $transaksi_pembelian_lama->kode_transaksi;
        // $transaksi_pembelian_lama->nota = $request->nota;
        // $transaksi_pembelian_lama->suplier_id = $request->suplier_id;
        // $transaksi_pembelian_lama->seller_id = $request->seller_id;
        // $transaksi_pembelian_lama->po = $request->po;
        $transaksi_pembelian_lama->potongan_pembelian = $request->potongan_pembelian;
        $transaksi_pembelian_lama->ongkos_kirim = $request->ongkos_kirim;
        $transaksi_pembelian_lama->harga_total = $request->harga_total;
        $transaksi_pembelian_lama->jumlah_bayar = $request->jumlah_bayar;
        $transaksi_pembelian_lama->utang = $request->utang;
        
        //selisih utang baru dan lama
        $selisih_utang_bayar = $transaksi_pembelian_compare->utang - $transaksi_pembelian_compare->sisa_utang;
        $transaksi_pembelian_lama->sisa_utang = $request->utang - $selisih_utang_bayar;

        $transaksi_pembelian_lama->user_id = Auth::user()->id;
        if($request->utang > 0){
            // $transaksi_pembelian_lama->jatuh_tempo = $request->jatuh_tempo;
        }
        if($request->ppn_total != null){
            $transaksi_pembelian_lama->ppn_total = $request->ppn_total;
        }

        $temp_potongan_pembelian += $request->potongan_pembelian;
        $temp_beban_ongkir += $request->ongkos_kirim;

        $transaksi_pembelian_lama->nominal_tunai = $request->nominal_tunai;
        $temp_kas_tunai += $request->nominal_tunai;
        
        $transaksi_pembelian_lama->no_transfer = $request->no_transfer;
        $transaksi_pembelian_lama->bank_transfer = $request->bank_transfer;
        $transaksi_pembelian_lama->nominal_transfer = $request->nominal_transfer;
        $temp_kas_bank += $request->nominal_transfer;

        $transaksi_pembelian_lama->no_kartu = $request->no_kartu;
        $transaksi_pembelian_lama->bank_kartu = $request->bank_kartu;
        $transaksi_pembelian_lama->jenis_kartu = $request->jenis_kartu;
        $transaksi_pembelian_lama->nominal_kartu = $request->nominal_kartu;

        $transaksi_pembelian_lama->no_cek = $request->no_cek;
        $transaksi_pembelian_lama->bank_cek = $request->bank_cek;
        $transaksi_pembelian_lama->nominal_cek = $request->nominal_cek;
        $temp_kas_cek += $request->nominal_cek;

        $transaksi_pembelian_lama->no_bg = $request->no_bg;
        $transaksi_pembelian_lama->bank_bg = $request->bank_bg;
        $transaksi_pembelian_lama->nominal_bg = $request->nominal_bg;
        $temp_kas_bg += $request->nominal_bg;

        $selisih_bayar_total = $request->jumlah_bayar - $request->harga_total;
        if ($request->utang > 0) {
            if($selisih_bayar_total > -100 && $selisih_bayar_total < 0){
                $transaksi_pembelian_lama->status_hutang = null;
                $temp_hutang_dagang += 0;
            }else{
                $transaksi_pembelian_lama->status_hutang = 1;
                $temp_hutang_dagang += $request->utang;
            }
        } else {
            $transaksi_pembelian_lama->status_hutang = null;
        }

        if($request->ppn_total == null || $request->ppn_total == 0){
            $temp_ppn_income = $request->harga_total - $temp_persediaan;
        }else{
            $temp_ppn_income = $request->ppn_total;
        }

        //PROSES CASHBACK
        if($request->nominal_tunai_cashback > 0 || $request->nominal_transfer_cashback > 0 || $request->nominal_kartu_cashback > 0){
            $cashback = new CashBackPembelian();
            $cashback->transaksi_pembelian_id = $transaksi_pembelian_compare->id;
            $cashback->nominal_tunai = $request->nominal_tunai_cashback;
            $cashback->no_transfer = $request->no_transfer_cashback;
            $cashback->nominal_transfer = $request->bank_transfer_cashback;
            $cashback->bank_transfer = $request->bank_transfer_cashback;
            $cashback->no_kartu = $request->no_kartu_cashback;
            $cashback->jenis_kartu = $request->jenis_kartu_cashback;
            $cashback->nominal_kartu = $request->nominal_kartu_cashback;
            $cashback->bank_kartu = $request->bank_kartu_cashback;
            $cashback->user_id = Auth::user()->id;
            $cashback->save();


            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => ($request->nominal_tunai_cashback + $request->nominal_transfer_cashback + $request->nominal_kartu_cashback) * -1
            ]);
        }

        $cek_aktif = true;
        $cek_baru = false;
        if ($transaksi_pembelian_compare->aktif_cek == 0) {
        	$cek_aktif = false;
        }
        if ($transaksi_pembelian_compare->aktif_cek == NULL) {
            $cek_baru = true;
        }

        $bg_aktif = true;
        $bg_baru = false;
        if ($transaksi_pembelian_compare->aktif_bg == 0) {
        	$bg_aktif = false;
        }
        if ($transaksi_pembelian_compare->aktif_bg == NULL) {
            $bg_baru = true;
        }

        //PERHITUNGAN SELISIH DATA YANG DIBUTUHKAN
        //hitung selisih ppn baru
        $selisih_relasi = $total_relasi_baru - $total_relasi_lama;

        // if($total_relasi_lama == $total_relasi_baru ){
        //     $selisih_ppn_masuk = 0;
        //     $selisih_persediaan = 0;
        // }else{
            // $jurnal_ppn_lama = Jurnal::where('kode_akun', Akun::PPNMasukan)->where('referensi', $transaksi_pembelian_compare->kode_transaksi)->first()->debet;
            // $selisih_ppn_masuk = $temp_ppn_income - $jurnal_ppn_lama;
            
            $ppn_lama = $total_relasi_lama - $temp_persediaan_lama;
            $selisih_ppn_masuk = $temp_ppn_income - $ppn_lama;
        // }

        //selisih ongkir
        $selisih_ongkir = $temp_beban_ongkir - $transaksi_pembelian_compare->ongkos_kirim;

        //hitung selisih tunai
        $selisih_tunai = $request->nominal_tunai - $transaksi_pembelian_compare->nominal_tunai;

        //hitung selisih cek
        $selisih_cek = $temp_kas_cek - $transaksi_pembelian_compare->nominal_cek;

        //hitung selisih BG
        $selisih_bg = $temp_kas_bg - $transaksi_pembelian_compare->nominal_bg;

        //hitung selisih bank/kartu
        $selisih_bank = $temp_kas_bank - $transaksi_pembelian_compare->nominal_transfer;
        $selisih_kartu = $request->nominal_kartu - $transaksi_pembelian_compare->nominal_kartu;

        //selisih potongan pembelian dan bikin jurnal nya kalo lebih dari 0
        $selisih_potongan = $temp_potongan_pembelian - $transaksi_pembelian_compare->potongan_pembelian;

        //hitung selisih hutang
        $hutang_lama = $transaksi_pembelian_compare->utang;
        // $jurnal_hutang_lama = ;
        // if ($jurnal_hutang_lama != null) {
        //     $hutang_lama += $jurnal_hutang_lama->kredit;
        // }
        $selisih_hutang = $temp_hutang_dagang - $hutang_lama;

        //jurnal selisih persediaan
        //jika selisih persedian positif (jurnal debet)
        if($selisih_persediaan > 0){
            $persediaan->debet += $selisih_persediaan;
            Jurnal::create([
                'kode_akun' => $persediaan->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_persediaan
            ]);
        }

        //jika selisih ppn masuk positif (jurnal debet)
        if($selisih_ppn_masuk > 0){
            Jurnal::create([
                'kode_akun' => $ppn_income->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_ppn_masuk
            ]);

            $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
            $akun_ppn_masuk->debet += $selisih_ppn_masuk;
            $akun_ppn_masuk->update();
        }

        //jika selisih ongkir positif (jurnal debet)
        if ($selisih_ongkir > 0) {
            $beban_ongkir->debet += $selisih_ongkir;
            Jurnal::create([
                'kode_akun' => $beban_ongkir->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_ongkir
            ]);
        }

        //jika selisih tunai negatif (jurnal debet)
        if ($selisih_tunai < 0 || $request->nominal_tunai_cashback > 0) {
            // $kas_tunai->debet -= ($selisih_tunai - ($request->nominal_tunai_cashback * -1));
            // $tunai_proses = ($selisih_tunai * -1) + $request->nominal_tunai_cashback;
            $kas_tunai->debet += $tunai_proses;
            Jurnal::create([
                'kode_akun' => $kas_tunai->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $tunai_proses
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $selisih_tunai
            ]);
        }   

        //jika selisih bank n kartu negatif (jurnal debet)
        if ($selisih_bank < 0 || $selisih_kartu < 0 || $request->nominal_transfer_cashback > 0 || $request->nominal_kartu_cashback > 0) {
            $keterangan_bank = 'Pembelian Item Dagang';
            $nominal_bank = 0;
            if ($selisih_bank < 0) {
                $kas_bank->debet -= $temp_kas_bank;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal -= $selisih_bank;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - '.$selisih_bank.' via '.$bank->nama_bank;
                $nominal_bank += ($selisih_bank * -1);
            }

            if ($selisih_kartu < 0) {
                $transaksi_pembelian_lama->jenis_kartu = $request->jenis_kartu;

                if ($request->jenis_kartu == 'kredit') {
                    $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                    $akun_kartu->kredit += $selisih_kartu;
                    $akun_kartu->update();

                    Jurnal::create([
                        'kode_akun' => Akun::KartuKredit,
                        'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                        'keterangan' => 'Pembelian via kartu kredit',
                        'debet' => $selisih_kartu * -1
                    ]);
                } else {
                    $kas_bank->debet -= $selisih_kartu;

                    $bank = Bank::find($request->bank_kartu);
                    $bank->nominal -= $selisih_kartu;
                    $bank->update();

                    $keterangan_bank = $keterangan_bank.' - '.($selisih_kartu * -1).' via '.$bank->nama_bank;

                    $nominal_bank += ($selisih_kartu * -1);
                }
            }

            if ($request->nominal_transfer_cashback > 0) {
                $kas_bank->debet += $request->nominal_transfer_cashback;

                $bank = Bank::find($request->bank_transfer_cashback);
                $bank->nominal += $request->nominal_transfer_cashback;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - [cashback] - '.$request->nominal_transfer_cashback.' via '.$bank->nama_bank;
                $nominal_bank += $request->nominal_transfer_cashback;
            }

            if ($request->nominal_kartu_cashback > 0) {
                $kas_bank->debet += $request->nominal_kartu_cashback;

                $bank = Bank::find($request->bank_kartu_cashback);
                $bank->nominal += $request->nominal_kartu_cashback;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - [cashback] - '.$request->nominal_kartu_cashback.' via '.$bank->nama_bank;
                $nominal_bank += $request->nominal_kartu_cashback;
            }

            Jurnal::create([
                'kode_akun' => $kas_bank->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => $keterangan_bank,
                'debet' => $nominal_bank
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $nominal_bank
            ]);
        }

        //jika selisih cek negatif (jurnal debet)
        if ($cek_baru){
            if ($selisih_cek < 0 && $request->bank_cek != NULL) {
                $cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();
                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }

                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_cek;
                $akun->update();

                $transaksi_pembelian_lama->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'debet' => $selisih_cek * -1
                ]);
            }
        }elseif(!$cek_baru)
            if ($selisih_cek < 0 && $request->bank_cek != NULL && $cek_aktif) {
                $cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();
                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }

                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_cek;
                $akun->update();

                $transaksi_pembelian_lama->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'debet' => $selisih_cek * -1
                ]);
            } elseif($selisih_cek < 0 && $request->bank_cek != NULL && $cek_aktif == false) {
        		$cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();
                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }
                // $cek->nominal += $selisih_cek;
                // $cek->update();

                $bank = Bank::find($request->bank_cek);
                $bank->nominal -= $selisih_cek;
                $bank->update();

                $akun = Akun::where('kode', Akun::KasBank)->first();
                $akun->debet -= $selisih_cek;
                $akun->update();

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via CEK '.$bank->nama_bank,
                    'debet' => $selisih_cek * -1
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $selisih_cek * -1
                ]);
            }

        //jika selisih bg negatif (jurnal debet)
        if ($bg_baru){
            if ($selisih_bg < 0 && $request->bank_bg != NULL) {
                $bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }

                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_bg;
                $akun->update();

                $transaksi_pembelian_lama->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'debet' => $selisih_bg * -1
                ]);
            }
        } elseif(!$bg_baru){
            if ($selisih_bg < 0 && $request->bank_bg != NULL && $bg_aktif) {
                $bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }

                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_bg;
                $akun->update();

                $transaksi_pembelian_lama->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'debet' => $selisih_bg * -1
                ]);
            } elseif($selisih_bg < 0 && $request->bank_bg != NULL && $bg_aktif == false) {
        		$bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }
                // $bg->nominal += $selisih_bg;
                // $bg->update();

                $bank = Bank::find($request->bank_bg);
                $bank->nominal -= $selisih_bg;
                $bank->update();

                $akun = Akun::where('kode', Akun::KasBank)->first();
                $akun->debet -= $selisih_bg;
                $akun->update();

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'debet' => $selisih_bg * -1
                ]);

                Arus::create([
                    'nama' => Arus::PBD,
                    'nominal' => $selisih_bg * -1
                ]);
            }
        }

        //jika selisih hutang negatif (jurnal debet)
        if ($selisih_hutang < 0) {
            $hutang_dagang->kredit += $selisih_hutang;
            Jurnal::create([
                'kode_akun' => $hutang_dagang->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_hutang * -1
            ]);
        }

        //jika selisih potongan negatif (jurnal debet)
        if ($selisih_potongan < 0) {
            $potongan_pembelian->kredit += $selisih_potongan;
            Jurnal::create([
                'kode_akun' => $potongan_pembelian->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'debet' => $selisih_potongan * -1
            ]);
        }

        //jika selisih persedian negatif (jurnal kredit)
        if($selisih_persediaan < 0){
            $persediaan->debet += $selisih_persediaan;
            Jurnal::create([
                'kode_akun' => $persediaan->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_persediaan * -1
            ]);
        }

        //jika selisih ppn masuk negatif (jurnal kredit)
        if($selisih_ppn_masuk < 0){
            Jurnal::create([
                'kode_akun' => $ppn_income->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_ppn_masuk * -1
            ]);

            $akun_ppn_masuk = Akun::where('kode', Akun::PPNMasukan)->first();
            $akun_ppn_masuk->debet += $selisih_ppn_masuk;
            $akun_ppn_masuk->update();
        }

        //jika selisih ongkir negatif (jurnal kredit)
        if ($selisih_ongkir < 0) {
            $beban_ongkir->debet += $selisih_ongkir;
            Jurnal::create([
                'kode_akun' => $beban_ongkir->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_ongkir *-1
            ]);
        }

        //jika selisih tunai positif (jurnal kredit)
        if ($selisih_tunai > 0) {
            $kas_tunai->debet -= $selisih_tunai;
            Jurnal::create([
                'kode_akun' => $kas_tunai->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_tunai
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $selisih_tunai
            ]);
        }

        //jika selisih bank n kartu positif (jurnal kredit)
        if ($selisih_bank > 0 || $selisih_kartu > 0) {
            $keterangan_bank = 'Pembelian Item Dagang';
            $nominal_bank = 0;
            if ($selisih_bank > 0) {
                $kas_bank->debet -= $temp_kas_bank;

                $bank = Bank::find($request->bank_transfer);
                $bank->nominal -= $selisih_bank;
                $bank->update();

                $keterangan_bank = $keterangan_bank.' - '.$selisih_bank.' via '.$bank->nama_bank;
                $nominal_bank += $selisih_bank;
            }

            if ($selisih_kartu > 0) {
                $transaksi_pembelian_lama->jenis_kartu = $request->jenis_kartu;

                if ($request->jenis_kartu == 'kredit') {
                    $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                    $akun_kartu->kredit += $selisih_kartu;
                    $akun_kartu->update();

                    Jurnal::create([
                        'kode_akun' => Akun::KartuKredit,
                        'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                        'keterangan' => 'Pembelian via kartu kredit',
                        'kredit' => $request->nominal_kartu
                    ]);
                } else {
                    $kas_bank->debet -= $selisih_kartu;

                    $bank = Bank::find($request->bank_kartu);
                    $bank->nominal -= $selisih_kartu;
                    $bank->update();

                    $keterangan_bank = $keterangan_bank.' - '.$selisih_kartu.' via '.$bank->nama_bank;

                    $nominal_bank += $selisih_kartu;
                }
            }

            Jurnal::create([
                'kode_akun' => $kas_bank->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => $keterangan_bank,
                'kredit' => $nominal_bank
            ]);

            Arus::create([
                'nama' => Arus::PBD,
                'nominal' => $nominal_bank,
            ]);
        }

        //jika selisih cek positif (jurnal kredit)
        if ($cek_baru) {
            if ($selisih_cek > 0 && $request->bank_cek != NULL) {
                $cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();

                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }

                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_cek;
                $akun->update();

                $transaksi_pembelian_lama->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $selisih_cek
                ]);
            }
        } elseif(!$cek_baru){
            if ($selisih_cek > 0 && $request->bank_cek != NULL && $cek_aktif) {
                $cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();

                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }

                $bank = Bank::find($request->bank_cek);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_cek;
                $akun->update();

                $transaksi_pembelian_lama->aktif_cek = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $selisih_cek
                ]);
            } elseif($selisih_cek > 0 && $request->bank_cek != NULL && $cek_aktif == false) {
            	$cek = Cek::where('nomor', $transaksi_pembelian_compare->no_cek)->where('nominal', $transaksi_pembelian_compare->nominal_cek)->first();

                if($cek == null){
                    $cek = new Cek();
                    $cek->nomor = $request->no_cek;
                    $cek->nominal = $request->nominal_cek;
                    $cek->aktif = 0;
                    $cek->save();
                }else{
                    $cek->nominal += $selisih_cek;
                    $cek->update();
                }
                // $cek->nominal += $selisih_cek;
                // $cek->update();

                $bank = Bank::find($request->bank_cek);
                $bank->nominal -= $selisih_cek;
                $bank->update();

                $akun = Akun::where('kode', Akun::KasBank)->first();
                $akun->kredit -= $selisih_cek;
                $akun->update();

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via Cek '.$bank->nama_bank,
                    'kredit' => $selisih_cek
                ]);
            }
        }

        //jika selisih bg positif (jurnal kredit)
        if($bg_baru) {
            if ($selisih_bg > 0 && $request->bank_bg) {
                $bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }

                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_bg;
                $akun->update();

                $transaksi_pembelian_lama->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'kredit' => $selisih_bg
                ]);
            }
        } elseif(!$bg_baru){
            if ($selisih_bg > 0 && $request->bank_bg != NULL && $bg_aktif) {
                $bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }

                $bank = Bank::find($request->bank_bg);
                $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                $akun->kredit += $selisih_bg;
                $akun->update();

                $transaksi_pembelian_lama->aktif_bg = 1;

                Jurnal::create([
                    'kode_akun' => Akun::PembayaranProses,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'kredit' => $selisih_bg
                ]);
            } elseif($selisih_bg > 0 && $request->bank_bg != NULL && $bg_aktif == false) {
        		$bg = BG::where('nomor', $transaksi_pembelian_compare->no_bg)->where('nominal', $transaksi_pembelian_compare->nominal_bg)->first();
                if($bg == null){
                    $bg = new BG();
                    $bg->nomor = $request->no_bg;
                    $bg->nominal = $request->nominal_bg;
                    $bg->aktif = 0;
                    $bg->save();
                }else{
                    $bg->nominal += $selisih_bg;
                    $bg->update();
                }
                // $bg->nominal += $selisih_bg;
                // $bg->update();

                $bank = Bank::find($request->bank_bg);
                $bank->nominal -= $selisih_bg;
                $bank->update();

                $akun = Akun::where('kode', Akun::KasBank)->first();
                $akun->kredit -= $selisih_bg;
                $akun->update();

                Jurnal::create([
                    'kode_akun' => Akun::KasBank,
                    'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                    'keterangan' => 'Pembelian Item Dagang - via BG '.$bank->nama_bank,
                    'kredit' => $selisih_bg
                ]);
            }
        }

        //jika selisih potongan positif (jurnal kredit)
        if ($selisih_potongan > 0) {
            $potongan_pembelian->kredit += $selisih_potongan;
            Jurnal::create([
                'kode_akun' => $potongan_pembelian->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_potongan
            ]);
        }

        //jika selisih hutang positif (jurnal kredit)
        if ($selisih_hutang > 0) {
            $hutang_dagang->kredit += $selisih_hutang;
            Jurnal::create([
                'kode_akun' => $hutang_dagang->kode,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV]',
                'keterangan' => 'Pembelian Item Dagang',
                'kredit' => $selisih_hutang
            ]);
        }

        //JURNAL POTONGAN PEMBELIAN BARU
        if ($selisih_potongan > 0) {
            //jurnal laba rugi potongan pembelian
            Jurnal::create([
                'kode_akun' => Akun::PotonganPembelian,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $selisih_potongan
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $selisih_potongan
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $selisih_potongan
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $selisih_potongan
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_potongan;
            $akun_laba_tahan->update();
        }elseif($selisih_potongan < 0){
            //jurnal laba rugi potongan pembelian
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $selisih_potongan * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::PotonganPembelian,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $selisih_potongan * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'debet' => $selisih_potongan * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-PP',
                'keterangan' => 'Pembelian Item Dagang - Potongan Pembelian',
                'kredit' => $selisih_potongan * -1
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_potongan;
            $akun_laba_tahan->update();
        }

        //JURNAL ONGKIR BARU
        if ($selisih_ongkir > 0) {
            //jurnal laba rugi potongan pembelian
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'debet' => $selisih_ongkir
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanOngkir,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'kredit' => $selisih_ongkir
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'debet' => $selisih_ongkir
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'kredit' => $selisih_ongkir
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit -= $selisih_ongkir;
            $akun_laba_tahan->update();
        }elseif($selisih_ongkir < 0){
            //jurnal laba rugi Beban Ongkos Kirim
            Jurnal::create([
                'kode_akun' => Akun::BebanOngkir,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'debet' => $selisih_ongkir * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'kredit' => $selisih_ongkir * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'debet' => $selisih_ongkir * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-BO',
                'keterangan' => 'Pembelian Item Dagang - Beban Ongkos Kirim',
                'kredit' => $selisih_ongkir * -1
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit -= $selisih_ongkir;
            $akun_laba_tahan->update();
        }

        //JURNAL SELISIH STOK
        //JURNAL ONGKIR BARU (-) BebanRugiBeli | (+) 
        if ($selisih_stok_harga > 0) {
            //jurnal laba rugi SELISIH STOK
            Jurnal::create([
                'kode_akun' => Akun::Persediaan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] [STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga
            ]);
            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] [STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga
            ]);

            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_stok_harga;
            $akun_laba_tahan->update();

            $_akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
            $_akun_persediaan->debet += $selisih_stok_harga;
            $_akun_persediaan->update();
        }elseif($selisih_stok_harga < 0){
            //jurnal laba rugi SELISIH STOK
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] [STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::Persediaan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] [STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::BebanRugiBeli,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] L/R-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga * -1
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'kredit' => $selisih_stok_harga * -1
            ]);
            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $transaksi_pembelian_lama->kode_transaksi.' [REV] LDT-[STOK]',
                'keterangan' => 'Pembelian Item Dagang, Selisih Persediaan',
                'debet' => $selisih_stok_harga * -1
            ]);

            $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_tahan->kredit += $selisih_stok_harga;
            $akun_laba_tahan->update();

            $_akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
            $_akun_persediaan->debet += $selisih_stok_harga;
            $_akun_persediaan->update();
        }

        $transaksi_pembelian_lama->update();
        $hutang_dagang->update();
        $kas_tunai->update();
        $kas_bank->update();
        $kas_cek->update();
        $kas_bg->update();
        $persediaan->update();

        // return 'selisih persediaan: &emsp;'.$selisih_persediaan.
        //         ' <br> selisih stok &emsp;'.$selisih_stok_harga.
        //         // ' <br> selisih bayar total &emsp;'.$selisih_bayar_total.
        //         ' <br> selisih ppn &emsp;'.$selisih_ppn_masuk.
        //         ' <br> selisih_ongkir &emsp;'.$selisih_ongkir.
        //         // ' <br> selisih_bayar_beban &emsp;'.$selisih_bayar_beban.
        //         ' <br> selisih_tunai &emsp;'.$selisih_tunai.
        //         ' <br> selisih_bank &emsp;'.$selisih_bank.
        //         ' <br> selisih_kartu &emsp;'.$selisih_kartu.
        //         ' <br> selisih_cek &emsp;'.$selisih_cek.
        //         ' <br> selisih_bg &emsp;'.$selisih_bg.
        //         ' <br> selisih_potongan &emsp;'.$selisih_potongan.
        //         ' <br> selisih_hutang &emsp;'.$selisih_hutang;

        return redirect('/transaksi-pembelian/'.$transaksi_pembelian_lama->id)->with('sukses', 'ubah');
    }
}
