<?php

namespace App\Http\Controllers;

use DateTime;
use DateInterval;

use App\Saldo;
use App\Pengeluaran;
use App\JenisPengeluaran;

use Carbon\Carbon;

use Illuminate\Http\Request;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now();
        $pengeluarans = Pengeluaran::whereMonth('created_at', '=', $now->month)->get();
        return view('pengeluaran.index', compact('pengeluarans', 'now'));
    }

    public function tampil(request $request)
    {
        $awal = new DateTime($request->awal);
        $awal->add(new DateInterval('P1D'));
        $awal = $awal->format('Y-m-d');
        $akhir = new DateTime($request->akhir);
        $akhir->add(new DateInterval('P1D'));
        $akhir = $akhir->format('Y-m-d');
        $status = 0;
        $newAwal = explode("-", $awal);
        $newAkhir = explode("-", $akhir);
        $newAwal = Carbon::create($newAwal[0], $newAwal[1], $newAwal[2]);
        $newAkhir = Carbon::create($newAkhir[0], $newAkhir[1], $newAkhir[2]);


        if ($request->akhir == $awal) {
            $status = 1;
            $pengeluarans = Pengeluaran::whereBetween('created_at', [$awal, $akhir])->get();
        } else {
            $pengeluarans = Pengeluaran::whereBetween('created_at', [$awal, $akhir])->get();
        }
        return view('pengeluaran.tampil', compact('pengeluarans', 'request', 'status', 'newAwal', 'newAkhir'));
    }    

    // public function indexJson()
    // {
    //     $items = Item::all();
    //     return $items->toJson();
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_pengeluarans = JenisPengeluaran::all();
        return view('pengeluaran.create', compact('jenis_pengeluarans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pengeluaran = $request->all();
        return $pengeluaran;

        // $this->validate($request,[
        //     'jenis_pengeluaran' => 'required|max:255',
        //     'nominal' => 'required|max:255',
        //     'metode_pembayaran' => 'required|max:255',
        //     'keterangan' => 'required|max:255',
        // ]);

        // $id = 1;
        // $saldo = Saldo::find($id);
        // $pengeluaran = new Pengeluaran();
        
        // $pengeluaran->jenis_pengeluaran_id = $request->jenis_pengeluaran;
        // $pengeluaran->nominal = $request->nominal;
        // $pengeluaran->metode_pembayaran = $request->metode_pembayaran;
        // $pengeluaran->no_transfer = $request->no_transfer;
        // $pengeluaran->keterangan = $request->keterangan;
       
        // if ($request->metode_pembayaran == 'debit') {
        //     $pengeluaran->no_transfer = $request->no_transfer;
        //     $saldo->bank = $saldo->bank - $request->nominal;
        // }else{
        //     $saldo->tunai = $saldo->tunai - $request->nominal;
        // }

        // if ($pengeluaran->save() && $saldo->update()){
        //     return redirect('/pengeluaran')->with('sukse', 'tambah');    
        // } else {
        //     return redirect('/pengeluaran/create')->with('gagal', 'tambah');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
        // $satuans = Satuan::all();
        // $relasi_satuans = RelasiSatuan::where('item_id', $id)->get();
        // $bonuses = Bonus::all();
        // $item = Item::find($id);
        // return view('item.show', compact('item','satuans', 'bonuses', 'relasi_satuans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // $bonuses = Bonus::all();
        // $supliers = Suplier::all();
        // $jenis_items = JenisItem::all();
        // $item = Item::find($id);
        // $request->session()->forget(['sukses', 'error']);
        // return view('item.edit', compact('item', 'jenis_items', 'bonuses', 'supliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request,[
        //     'kode' => 'required|max:255',
        //     'nama' => 'required|max:255',
        //     'suplier_id' => 'required|max:255',
        //     'jenis_item_id' => 'required|max:255',
        //     'konsinyasi' => 'max:255',
        // ]);

        // if($request->bonus_id==NULL){
        //     $request->syarat_bonus=0;
        // }

        // $item = Item::find($id);

        // $item->kode = $request->kode;
        // $item->nama = $request->nama;
        // $item->suplier_id = $request->suplier_id;
        // $item->jenis_item_id = $request->jenis_item_id;
        // $item->konsinyasi = $request->konsinyasi;
        // $item->bonus_id = $request->bonus_id;
        // $item->syarat_bonus = $request->syarat_bonus;

        // if ($item->update()) {
        //     return redirect('/item')->with('sukses', 'ubah');
        // } else {
        //     return redirect('/item')->with('gagal', 'ubah');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $item = Item::find($id);
        // if ($item->delete()) {
        //     return redirect('/item')->with('sukses', 'hapus');
        // } else {
        //     return redirect('/item')->with('gagal', 'hapus');
        // }
    }
}
