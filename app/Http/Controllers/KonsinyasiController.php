<?php

namespace App\Http\Controllers;

use App\Item;
use App\Satuan;
use App\JenisItem;

use Illuminate\Http\Request;

class KonsinyasiController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$jenis_items = JenisItem::all();
		$supliers = Suplier::all();
		$konsinyasis = Item::where('konsinyasi', 1)->get();
		return view('konsinyasi.index', compact('konsinyasis', 'jenis_items', 'supliers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$jenis_items = JenisItem::all();
		$satuans = Satuan::all();
		return view('konsinyasi.create', compact('jenis_items', 'satuans'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'kode' => 'required|max:255',
			'nama' => 'required|max:255',
			'jenis_item_id' => 'required|max:255',
			'satuan_id' => 'required|max:255',
			'konversi' => 'required|max:255',
			'harga_dasar' => 'required|max:255',
			'stok' => 'required|max:255'
		]);

		$konsinyasi = new Item();
		
		$konsinyasi->kode = $request->kode;
		$konsinyasi->nama = $request->nama;
		$konsinyasi->jenis_item_id = $request->jenis_item_id;
		$konsinyasi->satuan_id = $request->satuan_id;
		$konsinyasi->konversi = $request->konversi;
		$konsinyasi->harga_dasar = $request->harga_dasar;
		$konsinyasi->stok = $request->stok;
		$konsinyasi->konsinyasi = 1;

		if ($konsinyasi->save()) {
			return redirect('/konsinyasi')->with('sukses', 'tambah');
		} else {
			return redirect('/konsinyasi')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$satuans = Satuan::all();
		$konsinyasi = Item::find($id);
		return view('konsinyasi.show', compact('konsinyasi','satuans'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$jenis_items = JenisItem::all();
		$satuans = Satuan::all();
		$konsinyasi = Item::find($id);
		return view('konsinyasi.edit', compact('konsinyasi', 'jenis_items', 'satuans'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'kode' => 'required|max:255',
			'nama' => 'required|max:255',
			'jenis_item_id' => 'required|max:255',
			'satuan_id' => 'required|max:255',
			'konversi' => 'required|max:255',
			'harga_dasar' => 'required|max:255',
			'stok' => 'required|max:255'
		]);

		$konsinyasi = Item::find($id);

		$konsinyasi->kode = $request->kode;
		$konsinyasi->nama = $request->nama;
		$konsinyasi->jenis_item_id = $request->jenis_item_id;
		$konsinyasi->satuan_id = $request->satuan_id;
		$konsinyasi->konversi = $request->konversi;
		$konsinyasi->harga_dasar = $request->harga_dasar;
		$konsinyasi->stok = $request->stok;

		if ($konsinyasi->update()) {
			return redirect('/konsinyasi')->with('sukses', 'ubah');
		} else {
			return redirect('/konsinyasi')->with('gagal', 'ubah');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$konsinyasi = Item::find($id);
		if ($konsinyasi->delete()) {
			return redirect('/konsinyasi')->with('sukses', 'hapus');
		} else {
			return redirect('/konsinyasi')->with('gagal', 'hapus');
		}
	}
}
