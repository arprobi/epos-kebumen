<?php

namespace App\Http\Controllers;

use DB;
use Datetime;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Jurnal;
use App\Perkap;
use Illuminate\Http\Request;

class PeralatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lastJson()
    {
        $perkap = Perkap::where('kode', 'like', '%KMK/PL%')->get()->last();
        return response()->json(['perkap' => $perkap]);
    }

    /* public function index()
    {
        $peralatans = Perkap::where('umur', '>', 0)->where('status' , 1)->get();
        $rusak_peralatans = Perkap::where('umur', '>', 0)->where('status' , 0)->get();
        $banks = Bank::all();
        // return $peralatans;
        return view('peralatan.index', compact('peralatans', 'rusak_peralatans', 'banks'));
    } */

    public function index()
    {
        $banks = Bank::all();
        return view('peralatan.index', compact('banks'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'perkaps.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $peralatans = Perkap
            ::select(
                'perkaps.id',
                'perkaps.kode',
                'perkaps.nama',
                'perkaps.umur',
                'perkaps.harga',
                'perkaps.nominal',
                'perkaps.residu',
                'perkaps.created_at',
                'perkaps.keterangan',
                'perkaps.no_transaksi'
            )
            ->where('perkaps.umur', '>', 0)
            ->where('perkaps.status', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Perkap
            ::select(
                'perkaps.id'
            )
            ->where('perkaps.umur', '>', 0)
            ->where('perkaps.status', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($peralatans as $i => $peralatan) {

            $peralatan->harga = Util::duit0($peralatan->harga);
            $peralatan->nominal = Util::duit0($peralatan->nominal);
            $peralatan->residu = Util::duit0($peralatan->residu);

            if ($peralatan->keterangan == null) {
                $peralatan->keterangan = ' - ';
            }

            $buttons['detail'] = ['url' => url('peralatan/show/'.$peralatan->id)];

            $buttons['hapus'] = ['url' => ''];
            $buttons['jual'] = ['url' => ''];

            // return $buttons;
            $peralatan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $peralatans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'perkaps.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $peralatans = Perkap
            ::select(
                'perkaps.id',
                'perkaps.kode',
                'perkaps.nama',
                'perkaps.umur',
                'perkaps.harga',
                'perkaps.nominal',
                'perkaps.residu',
                'perkaps.created_at',
                'perkaps.keterangan',
                'perkaps.no_transaksi'
            )
            ->where('perkaps.umur', '>', 0)
            ->where('perkaps.status', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Perkap
            ::select(
                'perkaps.id'
            )
            ->where('perkaps.umur', '>', 0)
            ->where('perkaps.status', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($peralatans as $i => $peralatan) {

            $peralatan->harga = Util::duit0($peralatan->harga);
            $peralatan->nominal = Util::duit0($peralatan->nominal);
            $peralatan->residu = Util::duit0($peralatan->residu);

            if ($peralatan->keterangan == null) {
                $peralatan->keterangan = ' - ';
            }

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $peralatans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $kode = explode("/", $request->kode);
        $mm = explode("/", $request->kode)[3];
        $yy = explode("/", $request->kode)[4];
        $bulan = $mm.'/'.$yy;
        for($i=1; $i<=$request->jumlah; $i++){
            if ($i==1) {
                $code = Util::int4digit(intval($kode[0]));
                $kode_barang = $code.'/KMK/PL/'.$bulan;
                // echo $kode_barang;
                $alat = new Perkap();
                $alat->kode = $kode_barang;
                $alat->nama = $request->nama;
                $alat->harga = $request->harga;
                $alat->nominal = $request->harga;
                $alat->residu = $request->residu;
                $alat->umur = $request->umur;
                $alat->status = 1;
                $alat->no_transaksi = $request->no_transaksi;
                $alat->created_at = new Datetime($request->tanggal);;
                $alat->keterangan = $request->keterangan;

                $alat->save();
            }else{
                $code = Util::int4digit(intval($kode[0]) + ($i-1));
                $kode_barang = $code.'/KMK/PL/'.$bulan;
                // echo $kode_barang; 
                $alat = new Perkap();
                $alat->kode = $kode_barang;
                $alat->nama = $request->nama;
                $alat->harga = $request->harga;
                $alat->nominal = $request->harga;
                $alat->residu = $request->residu;
                $alat->umur = $request->umur;
                $alat->status = 1;
                $alat->no_transaksi = $request->no_transaksi;
                $alat->keterangan = $request->keterangan;
                $alat->created_at = new Datetime($request->tanggal);;

                $alat->save();
            }
        }
        $jurnal_alat = new Jurnal();
        $jurnal_alat->kode_akun = Akun::Peralatan;
        $jurnal_alat->referensi = $request->kode;
        $jurnal_alat->debet = ($request->harga * $request->jumlah);
        $jurnal_alat->keterangan = 'Pembelian Peralatan | '.$request->keterangan;
        $jurnal_alat->save();

        $akun_alat = Akun::where('kode', Akun::Peralatan)->first();
        $akun_alat->debet = $akun_alat->debet + ($request->harga * $request->jumlah);
        $akun_alat->update();

        if($request->kas=='tunai'){
            //update akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet = $akun_tunai->debet - ($request->harga * $request->jumlah);
            $akun_tunai->update();
            //jurnal tunai
            $jurnal_tunai = new Jurnal();
            $jurnal_tunai->kode_akun = Akun::KasTunai;
            $jurnal_tunai->referensi = $request->kode;
            $jurnal_tunai->kredit = ($request->harga * $request->jumlah);
            $jurnal_tunai->keterangan = 'Pembelian Peralatan | '.$request->keterangan;
            $jurnal_tunai->save();
        }else{
            //update akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet = $akun_bank->debet - ($request->harga * $request->jumlah);
            $akun_bank->update();
            //update bank harga
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal - ($request->harga * $request->jumlah);
            $bank->update();
            //jurnal akun bank
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode;
            $jurnal_bank->kredit = ($request->harga * $request->jumlah);
            $jurnal_bank->keterangan = 'Pembelian Peralatan | '.$request->keterangan." - ".$bank->nama_bank;
            $jurnal_bank->save();
        }

        Arus::create([
            'nama' => Arus::BeliAset,
            'nominal' => $request->harga * $request->jumlah
        ]);

        return redirect('/peralatan')->with('sukses', 'tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alat = Perkap::find($id);
        $history = Jurnal::where('referensi', $alat->kode)->where('kode_akun', Akun::BebanPerawatan)->get();
        $banks = Bank::all();
        // return $history;
        return view('peralatan.show', compact('alat', 'history', 'banks'));
    }

    public function rawat_alat(request $request)
    {
        // return $request->all();
        // $akun_perawatan = Akun::where('kode', Akun::BebanPerawatan)->first();
        // $akun_perawatan->debet += $request->nominal;
        // $akun_perawatan->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -=  $request->nominal;
        $akun_laba_ditahan->update();

        //jurnal perawatan
        $jurnal_perawatan = new Jurnal();
        $jurnal_perawatan->kode_akun = Akun::BebanPerawatan;
        $jurnal_perawatan->referensi = $request->kode;
        $jurnal_perawatan->keterangan = $request->keterangan;
        $jurnal_perawatan->debet = $request->nominal;
        $jurnal_perawatan->save();
        if($request->kas=='tunai'){
            //update akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->nominal;
            $akun_tunai->update();
            //jurnal tunai
            $jurnal_tunai = new Jurnal();
            $jurnal_tunai->kode_akun = Akun::KasTunai;
            $jurnal_tunai->referensi = $request->kode;
            $jurnal_tunai->keterangan = $request->keterangan;
            $jurnal_tunai->kredit = $request->nominal;
            $jurnal_tunai->save();
        }else{
            //update akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->nominal;
            $akun_bank->update();
            //update bank nominal
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal -= $request->nominal;
            $bank->update();
            //jurnal akun bank
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode;
            $jurnal_bank->keterangan = $request->keterangan." - ".$bank->nama_bank;
            $jurnal_bank->kredit = $request->nominal;
            $jurnal_bank->save();
        }

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanPerawatan,
            'referensi' => $request->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $request->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Arus::create([
            'nama' => Arus::Beban,
            'nominal' => $request->nominal
        ]);

        return redirect('/peralatan/show/'.$request->id)->with('sukses', 'tambah');
    }

    public function rusak(Request $request){
        $alat = Perkap::find($request->id);
        if($alat->keterangan==NULL){
            $keterangan = $request->keterangan;
        }else{
            $keterangan = $alat->keterangan." | ".$request->keterangan;
        }        
            
        $alat->keterangan = $keterangan;
        $alat->status = 0;
        $alat->update();

        $tahun = (new Datetime())->format('Y');
        
        $beban = DB::table('jurnals')
                ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                ->where('kode_akun', Akun::BebanDepPeralatan)
                ->where('referensi', $alat->kode)
                ->whereYear('created_at', $tahun)
                ->get();

        $rugi = $alat->nominal - $beban[0]->jumlah_beban;
        if($beban[0]->jumlah_beban !== NULL){
            $akun_dep_alat = Akun::where('kode', Akun::ADPeralatan)->first();
            $akun_dep_alat->kredit -= $beban[0]->jumlah_beban;
            $akun_dep_alat->update(); 

            $jurnal_dep_alat = new Jurnal();
            $jurnal_dep_alat->kode_akun = Akun::ADPeralatan;
            $jurnal_dep_alat->referensi = $alat->kode;
            $jurnal_dep_alat->keterangan = $keterangan;
            $jurnal_dep_alat->debet = $beban[0]->jumlah_beban;
            $jurnal_dep_alat->save();
        }

        $akun_alat = Akun::where('kode', Akun::Peralatan)->first();
        $akun_alat->debet -= $alat->nominal;
        $akun_alat->update();

        // $akun_rugi = Akun::where('kode', Akun::BebanRugiAset)->first();
        // $akun_rugi->debet += $rugi;
        // $akun_rugi->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -=  $rugi;
        $akun_laba_ditahan->update();

        $jurnal_rugi = new Jurnal();
        $jurnal_rugi->kode_akun = Akun::BebanRugiAset;
        $jurnal_rugi->referensi = $alat->kode;
        $jurnal_rugi->keterangan = $keterangan;
        $jurnal_rugi->debet = $rugi;
        $jurnal_rugi->save();

        $jurnal_peralatan = new Jurnal();
        $jurnal_peralatan->kode_akun = Akun::Peralatan;
        $jurnal_peralatan->referensi = $alat->kode;
        $jurnal_peralatan->keterangan = $keterangan;
        $jurnal_peralatan->kredit = $alat->nominal;
        $jurnal_peralatan->save();

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $alat->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'debet' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanDepPeralatan,
            'referensi' => $alat->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'kredit' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $alat->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'debet' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $alat->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'kredit' => $rugi
        ]);

        return redirect('/peralatan')->with('sukses', 'hapus');        
    }

    public function jual(Request $request){
        // return $request->all();
        $alat = Perkap::find($request->id);
        if($alat->keterangan==NULL){
            $keterangan = $request->keterangan;
        }else{
            $keterangan = $alat->keterangan." | ".$request->keterangan." | Penjualan Aset";
        }        
            
        $alat->keterangan = $keterangan;
        $alat->status = 0;
        $alat->update();

        $tahun = (new Datetime())->format('Y');
        
        $beban = DB::table('jurnals')
                ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                ->where('kode_akun', Akun::BebanDepPeralatan)
                ->where('referensi', $alat->kode)
                ->whereYear('created_at', $tahun)
                ->get();

        //update kas
        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet += $request->nominal;
            $akun_tunai->update();
           
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $alat->kode;
            $tunai->keterangan = $keterangan;
            $tunai->debet = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
        }else{
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet += $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal += $request->nominal;
            $bank->update();
            // create baris akun tunai
            $keterangan_bank = $keterangan.' - masuk Bank '.$bank->nama_bank;
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $alat->kode;
            $jurnal_bank->keterangan = $keterangan_bank;
            $jurnal_bank->debet = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
        }

        //kalo punya depresiasi
        if($beban[0]->jumlah_beban !== NULL){
            $akun_dep_alat = Akun::where('kode', Akun::ADPeralatan)->first();
            $akun_dep_alat->kredit -= $beban[0]->jumlah_beban;
            $akun_dep_alat->update(); 

            $jurnal_dep_alat = new Jurnal();
            $jurnal_dep_alat->kode_akun = Akun::ADPeralatan;
            $jurnal_dep_alat->referensi = $alat->kode;
            $jurnal_dep_alat->keterangan = $keterangan;
            $jurnal_dep_alat->debet = $beban[0]->jumlah_beban;
            $jurnal_dep_alat->save();
        }

        //hitung  untung atau rugi
        $selisih = ($beban[0]->jumlah_beban + $request->nominal) - $alat->nominal;
        // return $selisih;
        if($selisih < 0){
            // $akun_rugi = Akun::where('kode', Akun::BebanRugiAset)->first();
            // $akun_rugi->debet += ($selisih * -1);
            // $akun_rugi->update(); 

            Jurnal::create([
                'kode_akun' => Akun::BebanRugiAset,
                'referensi' => $alat->kode,
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::Peralatan,
                'referensi' => $alat->kode,
                'keterangan' => $keterangan,
                'kredit' => $alat->nominal
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $alat->kode.' - L/R',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::BebanRugiAset,
                'referensi' => $alat->kode.' - L/R',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $alat->kode.' - LDT',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $alat->kode.' - LDT',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);
        }else{
            // $akun_untung = Akun::where('kode', Akun::PendapatanLain)->first();
            // $akun_untung->kredit += $selisih;
            // $akun_untung->update();

            Jurnal::create([
                'kode_akun' => Akun::Peralatan,
                'referensi' => $alat->kode,
                'keterangan' => $keterangan,
                'kredit' => $alat->nominal
            ]);

            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $alat->kode,
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $alat->kode.' - L/R',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $alat->kode.' - L/R',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $alat->kode.' - LDT',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $alat->kode.' - LDT',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);
        }  

        $akun_alat = Akun::where('kode', Akun::Peralatan)->first();
        $akun_alat->debet -= $alat->nominal;
        $akun_alat->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit +=  $selisih;
        $akun_laba_ditahan->update();

        Arus::create([
            'nama' => Arus::JualAset,
            'nominal' => $request->nominal,
        ]);
        
        return redirect('/peralatan')->with('sukses', 'jual');        
    }    
}
