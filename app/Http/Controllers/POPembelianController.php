<?php

namespace App\Http\Controllers;

use Auth;

use App\BG;
use App\Cek;
use App\Bank;
use App\Laci;
use App\Util;
use App\Kredit;
use App\LogLaci;
use App\Suplier;
use App\RelasiSatuan;
use App\TransaksiPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class POPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $po_pembelians = TransaksiPembelian::where('po', 1)->orderBy('created_at', 'desc')->get();
        // $po_pembelians = TransaksiPembelian::where('po', 1)
        //  ->where('kode_transaksi', 'like', '%TPM%')
        //  ->orderBy('kode_transaksi', 'desc')->get();
        return view('po_pembelian.index', compact('po_pembelians'));
    } */

    public function index()
    {
        return view('po_pembelian.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_pembelians.' . $field;
        if ($request->field == 'suplier_nama') {
            $field = 'supliers.nama';
        }

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $po_pembelians = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id',
                'transaksi_pembelians.created_at',
                'transaksi_pembelians.kode_transaksi',
                'supliers.nama as suplier_nama'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 1)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPM%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = TransaksiPembelian
            ::select(
                'transaksi_pembelians.id'
            )
            ->leftJoin('supliers', 'supliers.id', 'transaksi_pembelians.suplier_id')
            ->where('transaksi_pembelians.po', 1)
            ->where('transaksi_pembelians.kode_transaksi', 'like', '%TPM%')
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('transaksi_pembelians.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('transaksi_pembelians.kode_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($po_pembelians as $i => $po_pembelian) {

            $buttons['detail'] = ['url' => url('po-pembelian/'.$po_pembelian->id)];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $po_pembelian->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $po_pembelians,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_pembelian_terakhir = TransaksiPembelian::where('kode_transaksi', 'like', '%TPM%')->get()->last();
        if ($transaksi_pembelian_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPM/'.$tanggal;
        } else {
            $kode_transaksi_pembelian_terakhir = $transaksi_pembelian_terakhir->kode_transaksi;
            $tanggal_transaksi_pembelian_terakhir = substr($kode_transaksi_pembelian_terakhir, 9, 10);
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_pembelian_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_pembelian_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPM/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPM/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $supliers = Suplier::all();
        $supliers = Suplier::whereHas('items', function ($query) {
            $query->where('konsinyasi', 0);
        })->orderBy('nama', 'asc')->get();
        return view('po_pembelian.create', compact('supliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $data = array();
        foreach ($request->item_id as $i => $id) {
            $data[''.$id] = [
                'jumlah' => $request->jumlah[$i]
            ];
        }

        $transaksi_pembelian = new TransaksiPembelian();
        $kode_transaksi = self::kodeTransaksiBaru();
        $transaksi_pembelian->kode_transaksi = $kode_transaksi;
        $transaksi_pembelian->suplier_id = $request->suplier_id;
        $transaksi_pembelian->po = $request->po;
        $transaksi_pembelian->user_id = Auth::user()->id;


        if ($transaksi_pembelian->save()) {
            $transaksi_pembelian->items()->sync($data);
            return redirect('/po-pembelian')->with('sukses', 'tambah');
        } else {
            return redirect('/po-pembelian')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $po_pembelian = TransaksiPembelian::with('suplier', 'items')->find($id);
        $po_pembelian = TransaksiPembelian::with('suplier', 'items')
            ->where('id', $id)
            ->where('po', 1)
            ->first();

        if ($po_pembelian == null ||
            strtolower(explode('/', $po_pembelian->kode_transaksi)[1]) != 'tpm') {
            return redirect('/po-pembelian')->with('gagal', '404');
        }

        $relasi_po_pembelian = RelasiTransaksiPembelian::with('item', 'transaksi_pembelian')
            ->where('transaksi_pembelian_id', $id)->get();

        foreach ($relasi_po_pembelian as $i => $relasi) {
            $relasi_satuan = RelasiSatuan::with('satuan')
                ->where('item_kode', $relasi->item->kode)
                ->orderBy('konversi', 'desc')->get();

            $temp = $relasi->jumlah;
            $jumlah = array();
            foreach ($relasi_satuan as $j => $rs) {
                $hasil = intval($temp / $rs->konversi);
                if ($hasil > 0) $temp = $temp % $rs->konversi;
                array_push($jumlah, array(
                    'jumlah' => $hasil,
                    'satuan' => $rs->satuan
                ));
            }
            
            $relasi->jumlah = $jumlah;
        }

        return view('po_pembelian.show', compact('po_pembelian', 'relasi_po_pembelian'));
    }

    public function beli($id)
    {
        $supliers = Suplier::whereHas('items', function ($query) {
            $query->where('konsinyasi', 0);
        })->get();
        $kredits = Kredit::all();
        $banks = Bank::all();
        $ceks = Cek::where('aktif', 1)->get();
        $bgs = BG::where('aktif', 1)->get();

        $po_pembelian = TransaksiPembelian::with('suplier', 'items')
            ->where('id', $id)
            ->where('po', 1)
            ->first();

        $laci = Laci::find(1);

        return view('po_pembelian.beli', compact('supliers', 'ceks', 'kredits', 'bgs', 'banks', 'po_pembelian', 'laci'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        // return $id;
        $relasi_pembelians = RelasiTransaksiPembelian::where('transaksi_pembelian_id', $id)->get();

        foreach ($relasi_pembelians as $i => $relasi_pembelian) {
            $relasi_pembelian->delete();
        }

        TransaksiPembelian::find($id)->delete();
        return redirect('po-pembelian')->with('sukses', 'hapus');
    }
}
