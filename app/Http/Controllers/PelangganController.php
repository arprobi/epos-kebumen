<?php

namespace App\Http\Controllers;
use Auth;
use Datetime;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Desa;
use App\Laci;
use App\User;
use App\Util;
use App\Helper;
use App\Jurnal;
use App\LogLaci;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Pelanggan;
use App\PiutangDagang;
use App\TransaksiPenjualan;
use App\PembayaranDeposito;

use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function lastJson()
    {
        $titipan = Jurnal::where('kode_akun', Akun::PendapatanDimuka)
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['titipan' => $titipan]);
    }

    public function kodeJson($id)
    {
        $kode = Pelanggan::where('kode', $id)->first();
        return response()->json(['kode' => $kode]);
    }

    public function ktpJson($id)
    {
        $ktp = Pelanggan::where('ktp', $id)->first();
        return response()->json(['ktp' => $ktp]);
    }

    public function teleponJson($id)
    {
        $telepon = Pelanggan::where('telepon', $id)->first();
        return response()->json(['telepon' => $telepon]);
    }

    /*public function kodeEditJson($id, $data)
    {
        $kode = Pelanggan::where('kode', $id)->whereNotIn('id', $data)->first();
        return response()->json(['kode' => $kode]);
    }*/

    public function ktpEditJson($id, $data)
    {
        $ktp = Pelanggan::where('ktp', $id)->whereNotIn('id', [$data])->first();
        // return $ktp;
        return response()->json(['ktp' => $ktp]);
    }

    public function teleponEditJson($id, $data)
    {
        $telepon = Pelanggan::where('telepon', $id)->whereNotIn('id', [$data])->first();
        return response()->json(['telepon' => $telepon]);
    }

    /*public function lastJson()
    {
        $titipan = Jurnal::where('referensi', 'like', '%/TTP/%')
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['titipan' => $titipan]);
    }*/

    /* public function index()
    {
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $pelanggan_off = Pelanggan::where('aktif', 0)->get();
        return view('pelanggan.index', compact('pelanggans', 'pelanggan_off'));
    } */

    public function index()
    {
        return view('pelanggan.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'pelanggans.' . $field;

        $pelanggans = Pelanggan
            ::select(
                'pelanggans.id',
                'pelanggans.nama',
                'pelanggans.toko',
                'pelanggans.level',
                'pelanggans.titipan'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.toko', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.level', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.titipan', 'like', '%'.$request->search_query.'%');
            })
            ->where('pelanggans.aktif', 1)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Pelanggan
            ::select(
                'pelanggans.id'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.toko', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.level', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.titipan', 'like', '%'.$request->search_query.'%');
            })
            ->where('pelanggans.aktif', 1)
            ->count();

        foreach ($pelanggans as $i => $pelanggan) {

            $pelanggan->titipan = Util::duit0($pelanggan->titipan);

            $buttons['detail'] = ['url' => url('pelanggan/show/'.$pelanggan->id)];

            if (in_array(Auth::user()->level_id, [1, 2])) {
                $buttons['nonaktifkan'] = ['url' => ''];
            }

            // return $buttons;
            $pelanggan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $pelanggans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'pelanggans.' . $field;

        $pelanggans = Pelanggan
            ::select(
                'pelanggans.id',
                'pelanggans.nama',
                'pelanggans.toko',
                'pelanggans.level',
                'pelanggans.titipan'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.toko', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.level', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.titipan', 'like', '%'.$request->search_query.'%');
            })
            ->where('pelanggans.aktif', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Pelanggan
            ::select(
                'pelanggans.id'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.toko', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.level', 'like', '%'.$request->search_query.'%')
                ->orWhere('pelanggans.titipan', 'like', '%'.$request->search_query.'%');
            })
            ->where('pelanggans.aktif', 0)
            ->count();

        foreach ($pelanggans as $i => $pelanggan) {

            $pelanggan->titipan = Util::duit0($pelanggan->titipan);

            $buttons['detail'] = ['url' => url('pelanggan/show/'.$pelanggan->id)];

            if (in_array(Auth::user()->level_id, [1, 2])) {
                $buttons['aktifkan'] = ['url' => ''];
            }

            // return $buttons;
            $pelanggan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $pelanggans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function pelangganJson($id)
    {
        $pelanggan = Pelanggan::find($id);
        return response()->json(['pelanggan' => $pelanggan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $po_penjualan = null;
        $enumLevel = Helper::getEnumValues('pelanggans', 'level');
        $provinsis = Provinsi::all();
        $kabupatens = Kabupaten::all();
        $kecamatans = Kecamatan::all();
        $desas = Desa::all();
        return view('pelanggan.create', compact('enumLevel', 'provinsis', 'kabupatens', 'kecamatans', 'desas', 'po_penjualan'));
    }

    public function createWithPO($id)
    {
        $po_penjualan = TransaksiPenjualan::find($id);
        $enumLevel = Helper::getEnumValues('pelanggans', 'level');
        $provinsis = Provinsi::all();
        $kabupatens = Kabupaten::all();
        $kecamatans = Kecamatan::all();
        $desas = Desa::all();
        return view('pelanggan.create', compact('enumLevel', 'provinsis', 'kabupatens', 'kecamatans', 'desas', 'po_penjualan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request, [
            'ktp' => 'required|max:255',
            'telepon' => 'required|max:255',
        ]);
        if ($request->jatuh_tempo == null) {
            $request->jatuh_tempo = 7;
        }
        $cek_telepon = Pelanggan::where('telepon', $request->telepon)->first();
        $cek_ktp = Pelanggan::where('ktp', $request->ktp)->first();
        if ($cek_telepon != null || $cek_ktp != null){
            if ($cek_telepon != null) {
                return redirect('/pelanggan/create')->with('gagal', 'telepon');
            } elseif ($cek_ktp != null) {
                return redirect('/pelanggan/create')->with('gagal', 'ktp');
            }
        } else {
            $pelanggan = new Pelanggan();
            $pelanggan->status_identitas = $request->status_identitas;
            $pelanggan->ktp = $request->ktp;
            $pelanggan->nama = $request->nama;
            $pelanggan->toko = $request->toko;
            $pelanggan->alamat = $request->alamat;
            $pelanggan->desa_id = $request->desa_id;
            $pelanggan->telepon = $request->telepon;
            $pelanggan->fax = $request->fax;
            $pelanggan->email = $request->email;
            $pelanggan->no_rekening = $request->no_rekening;
            $pelanggan->atas_nama = $request->atas_nama;
            $pelanggan->bank = $request->bank;
            $pelanggan->level = 'grosir';
            $pelanggan->time_change = new Datetime();
            $pelanggan->diskon_persen = $request->diskon_persen;
            $pelanggan->potongan = $request->potongan;
            $pelanggan->limit_jumlah_piutang = $request->limit_jumlah_piutang;
            $pelanggan->jatuh_tempo = $request->jatuh_tempo;
            $pelanggan->user_id = Auth::user()->id;

            if ($pelanggan->save()) {
                if ($request->po_penjualan_id != null) {
                    $pelanggan->level = 'grosir';
                    $pelanggan->update();

                    $po_penjualan = TransaksiPenjualan::find($request->po_penjualan_id);
                    $po_penjualan->pelanggan_id = $pelanggan->id;
                    $po_penjualan->update();
                    return redirect('transaksi-grosir/create/'.$po_penjualan->id);
                    // return redirect('po-penjualan/'.$po_penjualan->id)->with('sukses', 'tambah');
                }
                return redirect('/pelanggan')->with('sukses', 'tambah');
            } else {
                return redirect('/pelanggan')->with('gagal', 'tambah');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelanggan = Pelanggan::find($id);
        // dd($pelanggan);
        if($pelanggan->desa_id != NULL){
            $kecamatan = Desa::where('id', $pelanggan->desa_id)->first();
            $kabupaten = Kecamatan::where('id', $kecamatan->kecamatan_id)->first();
        }
        
        return view('pelanggan.show', compact('pelanggan', 'kecamatan', 'kabupaten'));
    }

    public function titipan($id)
    {
        $pelanggan = Pelanggan::find($id);
        $banks = Bank::all();
        $jurnals = Jurnal::where('referensi', 'like', '%/DPS/'.$pelanggan->id.'%')->where('kode_akun', Akun::PendapatanDimuka)->orderBy('created_at', 'dsc')->get();

        $kas = [];
        foreach ($jurnals as $i => $jurnal) {
            $text = explode('Masuk', $jurnal->keterangan);
            $jurnal->keterangan = str_replace('<br>', '', $text[0]);

            $index = 0;
            for ( $j=1; $j < sizeof($text); $j++ ) { 
                if ( strpos($text[$j], 'Tunai') ) {
                    $kas[$i][$index] = 'Tunai';
                    $index++;
                } elseif ( strpos($text[$j], 'Transfer') ) {
                    $kas[$i][$index] = 'Transfer';
                    $index++;
                } elseif (strpos($text[$j], 'Kartu') ) {
                    $kas[$i][$index] = 'Kartu';
                    $index++;
                } elseif (strpos($text[$j], 'Cek') ) {
                    $kas[$i][$index] = 'Cek';
                    $index++;
                } elseif (strpos($text[$j], 'BG') ) {
                    $kas[$i][$index] = 'BG';
                    $index++;
                }
            }
        }

        // $users = User::where('status', 1)->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();

        $log_deposito = PembayaranDeposito::where('pelanggan_id', $id)->orderBy('created_at', 'dsc')->get();
        $pembayarans = [];

        /*foreach ($log_deposito as $i => $log) {
            if ($log->nominal_tunai > 0) {
                array_push($pembayarans, [
                    'id' => $log->id,
                    'created_at' => $log->created_at,
                    'jumlah' => $log->nominal_tunai,
                    'metode' => 'tunai',
                    'keterangan' => '',
                    'user' => $log->user->nama,
                    'kode' => $log->kode_transaksi
                ]);
            }

            if ($log->nominal_transfer > 0) {
                array_push($pembayarans, [
                    'id' => $log->id,
                    'created_at' => $log->created_at,
                    'jumlah' => $log->nominal_transfer,
                    'bank' => $log->banktransfer->nama,
                    'nomor' => $log->no_transfer,
                    'jumlah' => $log->nominal_transfer,
                    'metode' => 'transfer',
                    'keterangan' => '',
                    'user' => $log->user->nama,
                    'kode' => $log->kode_transaksi
                ]);
            }

            if ($log->nominal_kartu > 0) {
                array_push($pembayarans, [
                    'id' => $log->id,
                    'created_at' => $log->created_at,
                    'jumlah' => $log->nominal_kartu,
                    'bank' => $log->bankkartu->nama,
                    'nomor' => $log->no_kartu,
                    'jumlah' => $log->nominal_kartu,
                    'metode' => 'kartu',
                    'keterangan' => '',
                    'user' => $log->user->nama,
                    'kode' => $log->kode_transaksi
                ]);
            }

            if ($log->nominal_cek > 0) {
                array_push($pembayarans, [
                    'id' => $log->id,
                    'created_at' => $log->created_at,
                    'jumlah' => $log->nominal_cek,
                    'nomor' => $log->no_cek,
                    'metode' => 'cek',
                    'keterangan' => '',
                    'user' => $log->user->nama,
                    'kode' => $log->kode_transaksi
                ]);
            }

            if ($log->nominal_bg > 0) {
                array_push($pembayarans, [
                    'id' => $log->id,
                    'created_at' => $log->created_at,
                    'jumlah' => $log->nominal_bg,
                    'nomor' => $log->no_bg,
                    'metode' => 'bg',
                    'keterangan' => '',
                    'user' => $log->user->nama,
                    'kode' => $log->kode_transaksi
                ]);
            }
        }*/

        return view('pelanggan.titipan', compact('pelanggan', 'banks', 'jurnals', 'kas', 'pembayarans', 'log_deposito', 'users'));
    }

    public function titipanShow($id)
    {
        $log = PembayaranDeposito::find($id);
        $pelanggan = Pelanggan::find($log->pelanggan->id);
        // $pembayarans = [];

        // // foreach ($log_deposito as $i => $log) {
        //     if ($log->nominal_tunai > 0) {
        //         array_push($pembayarans, [
        //             'id' => $log->id,
        //             'created_at' => $log->created_at,
        //             'jumlah' => $log->nominal_tunai,
        //             'metode' => 'tunai',
        //         ]);
        //     }

        //     if ($log->nominal_transfer > 0) {
        //         array_push($pembayarans, [
        //             'id' => $log->id,
        //             'created_at' => $log->created_at,
        //             'jumlah' => $log->nominal_transfer,
        //             'bank' => $log->banktransfer->nama,
        //             'nomor' => $log->no_transfer,
        //             'jumlah' => $log->nominal_transfer,
        //             'metode' => 'transfer',
        //         ]);
        //     }

        //     if ($log->nominal_kartu > 0) {
        //         array_push($pembayarans, [
        //             'id' => $log->id,
        //             'created_at' => $log->created_at,
        //             'jumlah' => $log->nominal_kartu,
        //             'bank' => $log->bankkartu->nama,
        //             'nomor' => $log->no_kartu,
        //             'jumlah' => $log->nominal_kartu,
        //             'metode' => 'kartu',
        //         ]);
        //     }

        //     if ($log->nominal_cek > 0) {
        //         array_push($pembayarans, [
        //             'id' => $log->id,
        //             'created_at' => $log->created_at,
        //             'jumlah' => $log->nominal_cek,
        //             'nomor' => $log->no_cek,
        //             'metode' => 'cek',
        //         ]);
        //     }

        //     if ($log->nominal_bg > 0) {
        //         array_push($pembayarans, [
        //             'id' => $log->id,
        //             'created_at' => $log->created_at,
        //             'jumlah' => $log->nominal_bg,
        //             'nomor' => $log->no_bg,
        //             'metode' => 'bg',
        //         ]);
        //     }
        // // }

        // return $log;
        return view('pelanggan.titipan_show', compact('log', 'pelanggan'));
    }

    public function titipan_save(Request $request)
    {
        // return $request->all();
        $pelanggan = Pelanggan::find($request->id);
        $pelanggan->titipan = ($pelanggan->titipan == NULL ? $request->nominal_total : $pelanggan->titipan+$request->nominal_total);
        $pelanggan->update();

        $deposito = new PembayaranDeposito();
        $deposito->kode_transaksi = $request->kode_transaksi;
        $deposito->pelanggan_id = $request->id;
        $deposito->jumlah_bayar = $request->nominal_total;
        if($request->penyerah == null) {
            $deposito->penyerah = $pelanggan->nama;
        } else {
            $deposito->penyerah = $request->penyerah;
        }

        $akun_pendapatandimuka = Akun::where('kode', Akun::PendapatanDimuka)->first();
        $akun_pendapatandimuka->kredit += $request->nominal_total;
        $akun_pendapatandimuka->update();

        $jurnal_akun_pendapatan = new Jurnal();
        $jurnal_akun_pendapatan->kode_akun = Akun::PendapatanDimuka;
        $jurnal_akun_pendapatan->referensi = $request->kode_transaksi;
        $jurnal_akun_pendapatan->kredit = $request->nominal_total;

        $keterangan = '';
        $keterangan .= $request->keterangan;
        if ($request->nominal_tunai > 0) {
            $kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $kas_tunai->debet += $request->nominal_tunai;
            $kas_tunai->update();

            $deposito->nominal_tunai = $request->nominal_tunai;
            $keterangan .= ' <br> Masuk Kas Tunai';

            Jurnal::create([
                'kode_akun' => Akun::KasTunai,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $request->nominal_tunai,
                'user_id' => Auth::user()->id
            ]);

            $log_laci = new LogLaci();
            $log_laci->penerima = Auth::user()->id;
            $log_laci->approve = 1;
            $log_laci->status = 5;
            $log_laci->nominal = $request->nominal_tunai;
            $log_laci->save();

            $laci = Laci::find(1);
            if(Auth::user()->level_id == 4){
                $laci->grosir += $request->nominal_tunai;
            }else{
                $laci->owner += $request->nominal_tunai;
            }
            $laci->update();
        }

        if ($request->nominal_transfer > 0) {
            $bank = Bank::find($request->bank_transfer);
            $bank->nominal += $request->nominal_transfer;
            $bank->update();

            $deposito->no_transfer = $request->no_transfer;
            $deposito->nominal_transfer = $request->nominal_transfer;
            $deposito->bank_transfer = $request->bank_transfer;

            $keterangan .= ' <br> Masuk Kas Bank via Transfer '.$request->nominal_transfer.' '.$bank->nama.' '.$request->no_transfer;

            $kas = Akun::where('kode', Akun::KasBank)->first();
            $kas->debet += $request->nominal_transfer;
            $kas->update();

            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $request->nominal_transfer,
                'user_id' => Auth::user()->id
            ]);
        }

        if ($request->nominal_kartu > 0) {
            $bank = Bank::find($request->bank_kartu);
            $bank->nominal += $request->nominal_kartu;
            $bank->update();

            $deposito->no_kartu = $request->no_kartu;
            $deposito->nominal_kartu = $request->nominal_kartu;
            $deposito->bank_kartu = $request->bank_kartu;
            $deposito->jenis_kartu = $request->jenis_kartu;
            $keterangan .= ' <br> Masuk Kas Bank via Kartu '.$request->jenis_kartu.' '.$request->nominal_kartu.' '.$bank->nama.' '.$request->no_kartu;

            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $request->nominal_kartu,
                'user_id' => Auth::user()->id
            ]);

            $kas = Akun::where('kode', Akun::KasBank)->first();
            $kas->debet += $request->nominal_kartu;
            $kas->update();
        }

        if ($request->nominal_cek > 0) {
            $cek = new Cek();
            $cek->nomor = $request->no_cek;
            $cek->nominal = $request->nominal_cek;
            $cek->aktif = 1;
            $cek->save();

            $deposito->no_cek = $request->no_cek;
            $deposito->nominal_cek = $request->nominal_cek;
            $keterangan .= ' <br> Masuk Kas Cek '.$request->no_cek;

            Jurnal::create([
                'kode_akun' => Akun::KasCek,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $request->nominal_cek,
                'user_id' => Auth::user()->id
            ]);

            $kas = Akun::where('kode', Akun::KasCek)->first();
            $kas->debet += $request->nominal_cek;
            $kas->update();
        }

        if ($request->nominal_bg > 0) {
            $bg = new BG();
            $bg->nomor = $request->no_bg;
            $bg->nominal = $request->nominal_bg;
            $bg->aktif = 1;
            $bg->save();

            $deposito->no_bg = $request->no_bg;
            $deposito->nominal_bg = $request->nominal_bg;
            $keterangan .= ' <br> Masuk Kas BG '.$request->no_bg;

            Jurnal::create([
                'kode_akun' => Akun::KasBG,
                'referensi' => $request->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $request->nominal_bg,
                'user_id' => Auth::user()->id
            ]);

            $kas = Akun::where('kode', Akun::KasBG)->first();
            $kas->debet += $request->nominal_bg;
            $kas->update();
        }

        $jurnal_akun_pendapatan->keterangan = $keterangan;
        $jurnal_akun_pendapatan->user_id = Auth::user()->id;

        $jurnal_akun_pendapatan->save();

        $deposito->keterangan = $request->keterangan;
        $deposito->user_id = $request->penerima;
        $deposito->grosir_id = Auth::user()->id;
        $deposito->save();

        Arus::create([
            'nama' => Arus::Deposito,
            'nominal' => $request->nominal_total
        ]);

        // return redirect('/pelanggan/titipan/'.$request->id)->with('sukses', 'tambah');
        $deposito_id = PembayaranDeposito::all()->last()->id;
        return redirect('cetak/titipan/'.$deposito_id);
    }

    public function edit($id)
    {
        $pelanggan = Pelanggan::find($id);
        $enumLevel = Helper::getEnumValues('pelanggans', 'level');
        $provinsis = Provinsi::all();
        $kabupatens = Kabupaten::all();
        $kecamatans = Kecamatan::all();
        $desas = Desa::all();

        if($pelanggan->desa_id != NULL){
            $kecamatan = Desa::where('id', $pelanggan->desa_id)->first();
            $kabupaten = Kecamatan::where('id', $kecamatan->kecamatan_id)->first();
        }
        return view ('pelanggan.edit', compact('pelanggan', 'enumLevel', 'provinsis', 'kabupatens', 'kecamatans', 'desas'));
    }

    public function update(Request $request, $id)
    {
        $pelanggan = Pelanggan::find($id);
        
        if($pelanggan->level != $request->level){
            $pelanggan->time_change = new Datetime();
        }
        $pelanggan->status_identitas = $request->status_identitas;
        $pelanggan->ktp = $request->ktp;
        $pelanggan->nama = $request->nama;
        $pelanggan->toko = $request->toko;
        $pelanggan->alamat = $request->alamat;
        $pelanggan->desa_id = $request->desa_id;
        $pelanggan->telepon = $request->telepon;
        $pelanggan->fax = $request->fax;
        $pelanggan->email = $request->email;
        $pelanggan->no_rekening = $request->no_rekening;
        $pelanggan->atas_nama = $request->atas_nama;
        $pelanggan->bank = $request->bank;
        $pelanggan->level = $request->level;
        $pelanggan->diskon_persen = $request->diskon_persen;
        $pelanggan->potongan = $request->potongan;
        $pelanggan->jatuh_tempo = $request->jatuh_tempo;
        $pelanggan->limit_jumlah_piutang = $request->limit_jumlah_piutang;
        $pelanggan->user_id = Auth::user()->id;

        if ($pelanggan->update()) {
            return redirect('/pelanggan/show/'.$pelanggan->id)->with('sukses', 'ubah');
        } else {
            return redirect('/pelanggan/show/'.$pelanggan->id)->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $pelanggan  = Pelanggan::find($id);

        $is_deposito = false;
        if ($pelanggan->titipan > 0) {
            $is_deposito = true;
        }


        $transaksis = TransaksiPenjualan::where('pelanggan_id', $id)->get();
        $is_fail = false;
        foreach ($transaksis as $i => $transaksi) {
            $cek_piutang = PiutangDagang::where('transaksi_penjualan_id', $transaksi->id)->first();
            if ($cek_piutang != null && $cek_piutang->sisa > 0) {
                $is_fail = true;
            }
        }

        if (!$is_fail && !$is_deposito) {
            $pelanggan->aktif = 0;
            $pelanggan->user_id = Auth::user()->id;
            $pelanggan->update();
            return redirect('/pelanggan')->with('sukses', 'hapus');
        } else {
            return redirect('/pelanggan')->with('gagal', 'hapus');
        }
        // try {
        //     $pelanggan->delete();
        //     $status = 1;
        // } catch (\Illuminate\Database\QueryException $e) {
        //     return redirect('/pelanggan')->with('gagal', 'hapus');
        // }

        // if($status == 1){
        //     return redirect('/pelanggan')->with('sukses', 'hapus');
        // }else{
        //     return redirect('/pelanggan')->with('gagal', 'hapus');
        // }
    }

    public function reAktif($id) {
        $pelanggan = Pelanggan::find($id);
        $pelanggan->aktif = 1;
        $pelanggan->user_id = Auth::user()->id;
        $pelanggan->update();

        return redirect('/pelanggan')->with('sukses', 'aktif');
    }
}
