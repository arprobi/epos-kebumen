<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Datetime;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Laci;
use App\Util;
use App\Jurnal;
use App\Perkap;
use App\LogLaci;
use Illuminate\Http\Request;

class BebanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lastJson()
    {
        $beban = Jurnal::where('referensi', 'like', '%PBB%')
                    ->where('debet', '!=', null)
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['beban' => $beban]);
    }

    /* public function index()
    {
        $laci = Laci::find(1);
        $bebans = Akun::where('kode', Akun::BebanIklan)
                    ->orWhere('kode', Akun::BebanSewa)
                    ->orWhere('kode', Akun::BebanAsuransi)
                    ->orWhere('kode', Akun::BebanAdministrasiBank)
                    ->orWhere('kode', Akun::BebanBunga)
                    ->orWhere('kode', Akun::BebanDenda)
                    ->orWhere('kode', Akun::BebanPajak)
                    ->orWhere('kode', Akun::BebanUtilitas)
                    ->orWhere('kode', Akun::BebanTransportasi)
                    ->orWhere('kode', Akun::BebanKonsumsi)
                    ->orWhere('kode', Akun::BebanSosial)
                    ->orWhere('kode', Akun::BebanListrik)
                    ->orWhere('kode', Akun::BebanLain)
                    ->orderBy('kode', 'asc')
                    ->get();
        $banks = Bank::all();
        $dones = Jurnal::
                    where('kode_akun', Akun::BebanIklan)
                    ->orWhere('kode_akun', Akun::BebanSewa)
                    ->orWhere('kode_akun', Akun::BebanAsuransi)
                    ->orWhere('kode_akun', Akun::BebanAdministrasiBank)
                    ->orWhere('kode_akun', Akun::BebanDenda)
                    ->orWhere('kode_akun', Akun::BebanUtilitas)
                    ->orWhere('kode_akun', Akun::BebanTransportasi)
                    ->orWhere('kode_akun', Akun::BebanKonsumsi)
                    ->orWhere('kode_akun', Akun::BebanSosial)
                    ->orWhere('kode_akun', Akun::BebanLain)
                    ->orWhere('kode_akun', Akun::BebanListrik)

                    ->where('debet', '!=', NULL)
                    // ->where('debet', '!=', NULL)
                    ->get();
        $kas_kecil = Akun::where('kode', Akun::KasKecil)->first()->debet;
        $kartu_kredit = Akun::where('kode', Akun::KartuKredit)->first();
        return view('beban.index', compact('bebans', 'banks', 'dones', 'kas_kecil', 'kartu_kredit', 'laci'));
    } */

    public function index()
    {
        $laci = Laci::find(1);
        $bebans = Akun::where('kode', Akun::BebanIklan)
                    ->orWhere('kode', Akun::BebanSewa)
                    ->orWhere('kode', Akun::BebanAsuransi)
                    ->orWhere('kode', Akun::BebanAdministrasiBank)
                    ->orWhere('kode', Akun::BebanBunga)
                    ->orWhere('kode', Akun::BebanDenda)
                    ->orWhere('kode', Akun::BebanPajak)
                    ->orWhere('kode', Akun::BebanUtilitas)
                    ->orWhere('kode', Akun::BebanTransportasi)
                    ->orWhere('kode', Akun::BebanKonsumsi)
                    ->orWhere('kode', Akun::BebanSosial)
                    ->orWhere('kode', Akun::BebanListrik)
                    ->orWhere('kode', Akun::BebanLain)
                    ->orderBy('kode', 'asc')
                    ->get();
        $banks = Bank::all();
        $kas_kecil = Akun::where('kode', Akun::KasKecil)->first()->debet;
        $kartu_kredit = Akun::where('kode', Akun::KartuKredit)->first();
        return view('beban.index', compact('bebans', 'banks', 'kas_kecil', 'kartu_kredit', 'laci'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'jurnals.' . $field;
        if ($request->field == 'akun_nama') {
            $field = 'akuns.nama';
        }
        if ($request->field == 'user_nama') {
            $field = 'users.nama';
        }

        $kode_akuns = [
            Akun::BebanIklan,
            Akun::BebanSewa,
            Akun::BebanAsuransi,
            Akun::BebanAdministrasiBank,
            Akun::BebanDenda,
            Akun::BebanUtilitas,
            Akun::BebanTransportasi,
            Akun::BebanKonsumsi,
            Akun::BebanSosial,
            Akun::BebanLain,
            Akun::BebanListrik,
        ];

        $bebans = Jurnal
            ::select(
                'jurnals.id',
                'jurnals.kode_akun',
                'jurnals.debet',
                'jurnals.keterangan',
                'akuns.nama as nama_akun',
                'users.nama as operator'
            )
            ->leftJoin('akuns', 'akuns.kode', 'jurnals.kode_akun')
            ->leftJoin('users', 'users.id', 'jurnals.user_id')
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.debet', '!=', null)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.kode_akun', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('akuns.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Jurnal
            ::select(
                'jurnals.id'
            )
            ->leftJoin('akuns', 'akuns.kode', 'jurnals.kode_akun')
            ->leftJoin('users', 'users.id', 'jurnals.user_id')
            ->whereIn('jurnals.kode_akun', $kode_akuns)
            ->where('jurnals.debet', '!=', null)
            ->where(function($query) use ($request) {
                $query
                ->where('jurnals.kode_akun', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.debet', 'like', '%'.$request->search_query.'%')
                ->orWhere('jurnals.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('akuns.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('users.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($bebans as $i => $beban) {

            $beban->debet = Util::duit($beban->debet);

            if ($beban->operator == null) {
                $beban->operator = ' - ';
            }

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $bebans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        if($request->beban == Akun::KartuKredit){
            $akun_kredit = Akun::where('kode', Akun::KartuKredit)->first();
            $akun_kredit->kredit -= $request->nominal;
            $akun_kredit->update();

            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->nominal;
            $akun_bank->update();

            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal -= $request->nominal;
            $bank->update();

            Jurnal::create([
                'kode_akun' => Akun::KartuKredit,
                'referensi' => $request->kode_transaksi,
                'keterangan' => 'Bayar Kartu Kredit Bank '.$bank->nama_bank,
                'debet' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);

            Jurnal::create([
                'kode_akun' => Akun::KasBank,
                'referensi' => $request->kode_transaksi,
                'keterangan' => 'Bayar Kartu Kredit Bank '.$bank->nama_bank,
                'kredit' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);
        }else{
            $jurnal_beban = new Jurnal();
            $jurnal_beban->kode_akun = $request->beban;
            $jurnal_beban->referensi = $request->kode_transaksi;
            $jurnal_beban->keterangan = $request->keterangan;
            $jurnal_beban->debet = $request->nominal;
            $jurnal_beban->user_id = Auth::user()->id;
            $jurnal_beban->save();
            //update akun beban
            // $akun_beban = Akun::where('kode', '=', $request->beban)->first();
            // $akun_beban->debet += $request->nominal;
            // $akun_beban->update();

            $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_ditahan->kredit -=  $request->nominal;
            $akun_laba_ditahan->update();

            if($request->kas =='tunai'){
                // update nominal di akun tunai
                $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
                $akun_tunai->debet -= $request->nominal;
                $akun_tunai->update();
                // create jurnal akun tunai
                $tunai = new Jurnal();
                $tunai->kode_akun = Akun::KasTunai;
                $tunai->referensi = $request->kode_transaksi;
                $tunai->keterangan = $request->keterangan;
                $tunai->kredit = $request->nominal;
                // simpan jurnal tunai
                $tunai->save();
            }elseif($request->kas =='bank'){
                // update nominal di akun bank
                $akun_bank = Akun::where('kode', Akun::KasBank)->first();
                $akun_bank->debet = $akun_bank->debet - $request->nominal;
                $akun_bank->update();
                // update nominal di tabel bank
                $bank = Bank::where('id', $request->bank)->first();
                $bank->nominal -= $request->nominal;
                $bank->update();
                // create baris akun tunai
                $jurnal_bank = new Jurnal();
                $jurnal_bank->kode_akun = Akun::KasBank;
                $jurnal_bank->referensi = $request->kode_transaksi;
                $jurnal_bank->keterangan = $request->keterangan.' via Bank '.$bank->nama_bank;
                $jurnal_bank->kredit = $request->nominal;
                // simpan jurnal bank
                $jurnal_bank->save();
            }elseif($request->kas=='kecil'){
                // update nominal di akun kas kecil
                $akun_kecil = Akun::where('kode', Akun::KasKecil)->first();
                $akun_kecil->debet -= $request->nominal;
                $akun_kecil->update();
                // create jurnal akun tunai
                $kecil = new Jurnal();
                $kecil->kode_akun = Akun::KasKecil;
                $kecil->referensi = $request->kode_transaksi;
                $kecil->keterangan = $request->keterangan;
                $kecil->kredit = $request->nominal;
                // simpan jurnal kecil
                $kecil->save();
            }

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'debet' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);

            Jurnal::create([
                'kode_akun' => $request->beban,
                'referensi' => $request->kode_transaksi.' - L/R',
                'keterangan' => $request->keterangan,
                'kredit' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' => $request->keterangan,
                'debet' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $request->kode_transaksi.' - LDT',
                'keterangan' =>  $request->keterangan,
                'kredit' => $request->nominal, 
                'user_id' => Auth::user()->id
            ]);
        }

        if($request->kas == 'tunai' || $request->kas == 'kecil'){
            if(Auth::user()->level_id == 5){
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 6;
                $log->nominal = $request->nominal;
                $log->save();

                $laci = Laci::find(1);
                $laci->gudang -= $request->nominal;
                $laci->update();
            }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
            {
                $log = new LogLaci();
                $log->pengirim = Auth::user()->id;
                // $log->penerima = $request->penerima;
                $log->approve = 1;
                $log->status = 6;
                $log->nominal = $request->nominal;
                $log->save();

                $laci = Laci::find(1);
                $laci->owner -= $request->nominal;
                $laci->update();
            }
        }

        Arus::create([
            'nama' => Arus::Beban,
            'nominal' => $request->nominal,
        ]);
        return redirect('/beban')->with('sukses', 'tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
