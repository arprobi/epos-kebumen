<?php

namespace App\Http\Controllers;

use App\Satuan;

use Illuminate\Http\Request;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index() {
        $satuans = Satuan::all();
        return view('satuan.index', compact('satuans'));
    } */

    public function index() {
        return view('satuan.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'satuans.' . $field;

        $satuans = Satuan
            ::select(
                'satuans.kode',
                'satuans.nama'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('satuans.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('satuans.nama', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Satuan
            ::select(
                'satuans.id'
            )
            ->where(function($query) use ($request) {
                $query
                ->where('satuans.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('satuans.nama', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($satuans as $i => $satuan) {

            $buttons['ubah'] = ['url' => ''];
            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $satuan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $satuans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function indexJson() {
        $satuans = Satuan::all();
        return response()->json(compact('satuans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255'
        ]);


        $jenis_item_available = Satuan::where('kode', $request->kode)->orWhere('nama', $request->nama)->get();
        
        if(sizeof($jenis_item_available) <= 0){
            $satuan = new Satuan();
            $satuan->kode = $request->kode;
            $satuan->nama = $request->nama;
            if ($satuan->save()) {
                return redirect('/satuan')->with('sukses', 'tambah');
            } else {
                return redirect('/satuan')->with('gagal', 'tambah');
            }
        }else{
            return redirect('/satuan')->with('gagal', 'tambah_kode');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kode' => 'required|max:255',
            'nama' => 'required|max:255'
        ]);

        $satuan = Satuan::find($id);
        $satuan->kode = $request->kode;
        $satuan->nama = $request->nama;
        if ($satuan->save()) {
            return redirect('/satuan')->with('sukses', 'ubah');
        } else {
            return redirect('/satuan')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $satuan = Satuan::find($id);
            if ($satuan->delete()) {
                return redirect('/satuan')->with('sukses', 'hapus');
            } else {
                return redirect('/satuan')->with('gagal', 'hapus');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/satuan')->with('gagal', 'hapus');
        }
    }
    
}
