<?php

namespace App\Http\Controllers;

use App\Catatan;

use Illuminate\Http\Request;

class CatatanController extends Controller
{
    public function index()
    {
        $fakturs = Catatan::where('jenis_catatan', 'faktur')->get();
        $faktur_kredits = Catatan::where('jenis_catatan', 'faktur_kredit')->get();
        $depositos = Catatan::where('jenis_catatan', 'deposito')->get();
        $surat_jalan = Catatan::where('jenis_catatan', 'surat_jalan')->first();
        $struk = Catatan::where('jenis_catatan', 'struk')->get();
        $returs = Catatan::where('jenis_catatan', 'retur')->get();
        $pengambilan = Catatan::where('jenis_catatan', 'pengambilan')->get();
        $po_belis = Catatan::where('jenis_catatan', 'po_beli')->get();

        $catatans = Catatan::all();
        return view('catatan.index', compact('fakturs', 'depositos', 'surat_jalan', 'struk', 'pengambilan', 'faktur_kredits', 'returs', 'po_belis', 'catatans'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        $satu_arr = ['surat_jalan'];
        $thermal_arr = ['struk', 'pengambilan', 'retur', 'po_beli'];
        $tiga_arr = ['faktur', 'faktur_kredit', 'deposito'];

        // if($request->jenis != 'struk' && $request->jenis != 'pengambilan'){
        //     $catatans = Catatan::where('jenis_catatan', $request->jenis)->get();
        //     $index = 0;
        //     foreach ($request->baris as $i => $baris) {
        //         $catatans[$i]->catatan = $request->baris[$i];
        //         $catatans[$i]->update();
        //         $index++;
        //     }
        // }else{
        //     $catatans = Catatan::where('jenis_catatan', $request->jenis)->first();
        //     $catatans->catatan = $request->baris[0];
        //     $catatans->update();
        // }

        if (in_array($request->jenis, $satu_arr)) {
            $catatan = Catatan::where('jenis_catatan', $request->jenis)->first();
            $catatan->catatan = $request->baris[0];
            $catatan->update();
        } else {
            $catatans = Catatan::where('jenis_catatan', $request->jenis)->get();
            $index = 0;
            foreach ($request->baris as $i => $baris) {
                $catatans[$i]->catatan = $request->baris[$i];
                $catatans[$i]->update();
                $index++;
            }
        }

        return redirect('/catatan')->with('sukses', 'ubah');
    }
}
