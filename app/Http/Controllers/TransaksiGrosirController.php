<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;
use DateInterval;

use App\BG;
use App\Cek;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Item;
use App\Laci;
use App\Stok;
use App\User;
use App\Util;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\LogLaci;
use App\ReturRule;
use App\BonusRule;
use App\CetakNota;
use App\Pelanggan;
use App\CashDrawer;
use App\MoneyLimit;
use App\SetoranBuka;
use App\RelasiBonus;
use App\RelasiBundle;
use App\RelasiSatuan;
use App\PiutangDagang;
use App\ReturPenjualan;
use App\HistoryJualBundle;
use App\BayarPiutangDagang;
use App\TransaksiPenjualan;
use App\RelasiStokPenjualan;
use App\RelasiBonusPenjualan;
use App\RelasiReturPenjualan;
use App\RelasiTransaksiPenjualan;
use App\RelasiStokReturPenjualan;
use App\PembayaranReturPenjualan;
use App\RelasiBonusRulesPenjualan;

use App\Kredit;
use App\TransaksiPembelian;
use App\RelasiTransaksiPembelian;

use Illuminate\Http\Request;

class TransaksiGrosirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $rule = ReturRule::find(1);
        $batas_retur = array();
        $interval = "P".$rule->syarat."D";
        $today = new DateTime();

        $user = Auth::user();
        if (in_array($user->id, [3, 4])) {
            $transaksis = TransaksiPenjualan::whereIn('status', ['eceran', 'grosir'])->orderBy('created_at', 'desc')->get();
        } else {
            $transaksis = TransaksiPenjualan::whereIn('status', ['eceran', 'grosir', 'vip'])->orderBy('created_at', 'desc')->get();
        }

        foreach ($transaksis as $i => $data) {
            $batas = new DateTime($data->created_at);
            $batas_retur[$i] = $batas->add(new DateInterval($interval));
            $retur_show = false;
            $aktivasi_show = false;

            if(in_array($user->level_id, [1,2,3])){
                $retur_show = true;
                if(in_array($user->level_id, [1,2])){
                    $aktivasi_show = true;
                }
            }

            $data->retur_show = $retur_show;
            $data->aktivasi_show = $aktivasi_show;
        }

        // return $transaksis;
        return view('transaksi_grosir.index', compact('transaksis', 'batas_retur', 'rule', 'today'));
    } */

    public function index()
    {
        return view('transaksi_grosir.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'transaksi_penjualans.' . $field;
        if ($request->field == 'pelanggan_nama') {
            $field = 'pelanggans.nama';
        }

        $user = Auth::user();
        $transaksi_penjualans = [];
        $count = 0;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            if (count(explode(' ', $request->search_query)) >= 2) {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]) . ' ' . explode(' ', $request->search_query)[1];
            } else {
                $tanggal = Util::date(explode(' ', $request->search_query)[0]);
            }
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $status = strtolower($request->search_query);
        if (strpos($status, 'prioritas') !== false) {
            $status = 'vip';
        }

        // Untuk kasir eceran & kasir grosir
        if (in_array($user->level_id, [3, 4])) {

            $transaksi_penjualans = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id',
                    'transaksi_penjualans.created_at',
                    'transaksi_penjualans.kode_transaksi',
                    'transaksi_penjualans.status',
                    'transaksi_penjualans.harga_total',
                    'pelanggans.nama as pelanggan_nama',
                    'pelanggans.level as pelanggan_level'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['eceran', 'grosir'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%')
                    ->orWhere('transaksi_penjualans.harga_total', 'like', '%'.$request->search_query.'%');
                })
                ->limit($request->data_per_halaman)
                ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
                ->orderBy($field, $order)
                ->get();

            $count = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['eceran', 'grosir'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%')
                    ->orWhere('transaksi_penjualans.harga_total', 'like', '%'.$request->search_query.'%');
                })
                ->count();

        }
        // Untuk admin & owner
        else {

            $transaksi_penjualans = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id',
                    'transaksi_penjualans.created_at',
                    'transaksi_penjualans.kode_transaksi',
                    'transaksi_penjualans.status',
                    'transaksi_penjualans.harga_total',
                    'pelanggans.nama as pelanggan_nama',
                    'pelanggans.level as pelanggan_level'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['eceran', 'grosir', 'vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%')
                    ->orWhere('transaksi_penjualans.harga_total', 'like', '%'.$request->search_query.'%');
                })
                ->limit($request->data_per_halaman)
                ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
                ->orderBy($field, $order)
                ->get();

            $count = TransaksiPenjualan
                ::select(
                    'transaksi_penjualans.id'
                )
                ->leftJoin('pelanggans', 'pelanggans.id', 'transaksi_penjualans.pelanggan_id')
                ->whereIn('transaksi_penjualans.status', ['eceran', 'grosir', 'vip'])
                ->where(function($query) use ($request, $tanggal, $status) {
                    $query
                    ->where('transaksi_penjualans.created_at', 'like', '%'.$tanggal.'%')
                    ->orWhere('transaksi_penjualans.kode_transaksi', 'like', '%'.$request->search_query.'%')
                    ->orWhere('pelanggans.nama', 'like', '%'.$request->search_query.'%')
                    ->orWhere('transaksi_penjualans.status', 'like', '%'.$status.'%')
                    ->orWhere('transaksi_penjualans.harga_total', 'like', '%'.$request->search_query.'%');
                })
                ->count();

        }

        $rule = ReturRule::find(1);
        $interval = "P".$rule->syarat."D";
        $today = new DateTime();

        $retur_show = false;
        $aktivasi_show = false;

        if (in_array($user->level_id, [1, 2, 3])) {

            $retur_show = true;

            if (in_array($user->level_id, [1, 2])) {
                $aktivasi_show = true;
            }

        }

        foreach ($transaksi_penjualans as $i => $transaksi_penjualan) {

            $buttons['detail'] = null;
            $buttons['retur'] = null;
            $buttons['aktifkan'] = null;

            $transaksi_penjualan->mdt_harga_total = Util::duit0($transaksi_penjualan->harga_total);

            if ($transaksi_penjualan->pelanggan_nama != null) {
                $transaksi_penjualan->pelanggan_nama = ucwords($transaksi_penjualan->pelanggan_nama);
            } else {
                $transaksi_penjualan->pelanggan_nama = ' - ';
            }

            if ($transaksi_penjualan->status == 'vip') {
                $transaksi_penjualan->mdt_status = 'Prioritas';
            } else {
                $transaksi_penjualan->mdt_status = ucfirst($transaksi_penjualan->status);
            }

            $buttons['detail'] = ['url' => url('po-penjualan/'.$transaksi_penjualan->id)];

            $batas = new DateTime($transaksi_penjualan->created_at);
            $batas_retur = $batas->add(new DateInterval($interval));

            if ($transaksi_penjualan->can_retur == 0) {

                if ($rule->syarat != null &&
                    $rule->syarat != 0) {

                    if ($today <= $batas_retur && $retur_show) {

                        $buttons['retur'] = ['url' => url('transaksi-grosir/'.$transaksi_penjualan->id.'/retur/create')];

                    } else if ($aktivasi_show) {

                        $buttons['aktifkan'] = ['url' => ''];

                    }

                }

            } else {

                $buttons['retur'] = ['url' => url('transaksi-grosir/'.$transaksi_penjualan->id.'/retur/create')];

            }

            // return $buttons;
            $transaksi_penjualan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $transaksi_penjualans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function lastJson()
    {
        $transaksi_penjualan = TransaksiPenjualan::all()->last();
        return response()->json(['transaksi_penjualan' => $transaksi_penjualan]);
    }

    public function itemJson($kode)
    {
        $item = Item::where('kode', $kode)->get()->first();
        // $hpp = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->first();
        $hpp = null;
        $satuan = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi')->first()->satuan;
        $harga = Harga::where('item_kode', $item->kode)
                    ->where('jumlah', 1)
                    ->where('satuan_id', $satuan->id)
                    ->get()->last();
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $item->kode)->get();

        // Cari stoktotal untuk item biasa dan item konsinyasi
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
        // return $stoks;
        $today = date('Y-m-d');
        $stoktotal = 0;
        foreach ($stoks as $j => $stok) {
            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                $stoktotal += $stok->jumlah;
                if ($hpp == null) $hpp = $stok;
            }
        }
        // return $stoktotal;
        $item->stoktotal = $stoktotal;

        // Cari stoktotal untuk item bundle
        // Jika item bundle, cari stoktotal item bundle minimal
        if ($item->konsinyasi == 2) {
            $hpp = null;
            $stoktotals = [];
            $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
            foreach ($relasi_bundles as $i => $relasi_bundle) {
                $item_child = $relasi_bundle->item_child;
                $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                $temp_hpp = null;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                        if ($temp_hpp == null) $temp_hpp = $stok;
                    }
                }
                if ($temp_hpp != null) {
                    if ($hpp == null) {
                        $hpp = $temp_hpp;
                    } else {
                        $hpp->harga += $temp_hpp->harga;
                    }
                }
                if ($stoktotal > 0) {
                    array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                }
            }

            if (count($stoktotals) == count($relasi_bundles)) {
                // Cari stoktotal terkecil
                $stoktotal_terkecil = $stoktotals[0];
                foreach ($stoktotals as $i => $stoktotal) {
                    if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                        $stoktotal_terkecil = $stoktotal;
                    }
                }
                $item->stoktotal = $stoktotal_terkecil;
            } else {
                $item->stoktotal = 0;
            }
        }

        // return response()->json(['item' => $item, 'hpp' => $hpp, 'harga' => $harga]);
        return response()->json(compact('item', 'hpp', 'harga', 'relasi_bonus'));
    }

    public function hargaJson($kode, $satuanId1, $jumlah1, $satuanId2, $jumlah2, $konversi1, $konversi2)
    {
        // return [$jumlah1, $jumlah2, $satuanId1, $satuanId2, $konversi1, $konversi2];

        $item = Item::where('kode', $kode)->first();
        $limit_grosir = $item->limit_grosir;

        $satuan1 = $satuanId1;
        $satuan2 = $satuanId2;

        // $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;
        // $jumlah1 = 0;
        // $jumlah2 = 0;
        // $satuan1 = 0;
        // $satuan2 = 0;
        // $konversi1 = 0;
        // $konversi2 = 0;
        // foreach ($item->satuan_pembelians as $i => $satuan) {
        //     if ($total > 0) {
        //         if ($total >= $satuan->konversi) {
        //             if ($jumlah1 <= 0) {
        //                 $jumlah1 = intval($total / $satuan->konversi);
        //                 $satuan1 = $satuan->satuan->id;
        //                 $konversi1 = $satuan->konversi;
        //             } else {
        //                 $jumlah2 = intval($total / $satuan->konversi);
        //                 $satuan2 = $satuan->satuan->id;
        //                 $konversi2 = $satuan->konversi;
        //             }

        //             $total = $total % $satuan->konversi;
        //         }
        //     } else {
        //         break;
        //     }
        // }
        // return [$jumlah1, $jumlah2, $satuan1, $satuan2, $konversi1, $konversi2];

        $harga1 = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1)->orderBy('jumlah', 'asc')->get()->last();
        $harga2 = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah2)->where('satuan_id', $satuan2)->orderBy('jumlah', 'asc')->get()->last();
        $harga1_darurat = Harga::where('item_kode', $kode)->where('jumlah', '<=', 1)->where('satuan_id', $satuanId1)->orderBy('jumlah', 'asc')->get()->last();
        $harga2_darurat = Harga::where('item_kode', $kode)->where('jumlah', '<=', 1)->where('satuan_id', $satuanId2)->orderBy('jumlah', 'asc')->get()->last();
        // return [$jumlah1, $jumlah2, $harga1, $harga2, $harga1_darurat, $harga2_darurat];
        $harga = null;
        $konversi = 0;
        if ($konversi1 >= $konversi2) {
            if ($jumlah1 > 0) {
                // ambil harga di konversi1
                // $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1)->orderBy('jumlah', 'asc')->get()->last();
                $harga = $harga1;
                $harga_eceran = $harga->eceran / $konversi1;
                $harga_grosir = $harga->grosir / $konversi1;
                $konversi = $konversi1;
            } else if ($jumlah2 > 0) {
                // $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah2)->where('satuan_id', $satuan2)->orderBy('jumlah', 'asc')->get()->last();
                $harga = $harga2;
                $harga_eceran = $harga->eceran / $konversi2;
                $harga_grosir = $harga->grosir / $konversi2;
                $konversi = $konversi2;
            }
        } else {
            if ($jumlah2 > 0) {
                // $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah2)->where('satuan_id', $satuan2)->orderBy('jumlah', 'asc')->get()->last();
                $harga = $harga2;
                $harga_eceran = $harga->eceran / $konversi2;
                $harga_grosir = $harga->grosir / $konversi2;
                $konversi = $konversi2;
            } else if ($jumlah1 > 0) {
                // $harga = Harga::where('item_kode', $kode)->where('jumlah', '<=', $jumlah1)->where('satuan_id', $satuan1)->orderBy('jumlah', 'asc')->get()->last();
                $harga = $harga2;
                $harga_eceran = $harga->eceran / $konversi1;
                $harga_grosir = $harga->grosir / $konversi1;
                $konversi = $konversi1;
            }
        }

        // nggolet nego_min
        $nego_min = 0;
        $hpp = null;
        // $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;

        // Cari stoktotal untuk item biasa dan item konsinyasi
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
        $today = date('Y-m-d');
        foreach ($stoks as $j => $stok) {
            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                if ($hpp == null) $hpp = $stok;
            }
        }
        /*foreach ($stoks as $i => $stok) {
            if ($total > 0) {
                if ($stok->jumlah < $total) {
                    $nego_min += round(1.1 * $stok->harga * $stok->jumlah, 2);
                    $total -= $stok->jumlah;
                } else {
                    $nego_min += round(1.1 * $stok->harga * $total, 2);
                    $total = 0;
                }
            } else {
                break;
            }
        }*/

        // Cari stoktotal untuk item bundle
        // Jika item bundle, cari stoktotal item bundle minimal
        if ($item->konsinyasi == 2) {
            $hpp = null;
            // $stoktotals = [];
            $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
            foreach ($relasi_bundles as $i => $relasi_bundle) {
                $item_child = $relasi_bundle->item_child;
                $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                // $stoktotal = 0;
                $temp_hpp = null;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        // $stoktotal += $stok->jumlah;
                        if ($temp_hpp == null) $temp_hpp = $stok;
                    }
                }
                if ($temp_hpp != null) {
                    if ($hpp == null) {
                        $hpp = $temp_hpp;
                    } else {
                        $hpp->harga += $temp_hpp->harga;
                    }
                }
                // if ($stoktotal > 0) {
                //     array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                // }
            }

            // if (count($stoktotals) == count($relasi_bundles)) {
            //     // Cari stoktotal terkecil
            //     $stoktotal_terkecil = $stoktotals[0];
            //     foreach ($stoktotals as $i => $stoktotal) {
            //         if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
            //             $stoktotal_terkecil = $stoktotal;
            //         }
            //     }
            //     $item->stoktotal = $stoktotal_terkecil;
            // } else {
            //     $item->stoktotal = 0;
            // }
        }

        // nyari bonus
        $index_bonus = 0;
        $bonus = array();
        $total = $jumlah1 * $konversi1 + $jumlah2 * $konversi2;
        $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
        foreach ($relasi_bonus as $i => $relasi) {
            $syarat = $relasi->syarat;

            if ($total >= $syarat) {
                $jumlah_bonus = intval($total / $syarat);
                if ($relasi->bonus->stoktotal > 0) {
                    if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                         $bonus[$index_bonus] = [
                            'bonus' => $relasi->bonus,
                            'syarat' => $syarat,
                            'jumlah' => $jumlah_bonus
                        ];
                        $index_bonus++;
                }
                // if ($index_bonus == 0) {
                //     $bonus[$index_bonus] = [
                //         'bonus' => $relasi->bonus,
                //         'syarat' => $syarat,
                //         'jumlah' => intval($total / $syarat)
                //     ];
                //     $index_bonus++;
                // } else {
                //     if ($syarat != $bonus[$index_bonus - 1]['syarat']) {
                //         $bonus = array();
                //         $index_bonus = 0;
                //     }

                //     $bonus[$index_bonus] = [
                //         'bonus' => $relasi->bonus,
                //         'syarat' => $syarat,
                //         'jumlah' => intval($total / $syarat)
                //     ];
                //     $index_bonus++;
                // }
            }
        }

        // return $bonus;

        return response()->json(compact('limit_grosir', 'nego_min', 'hpp', 'bonus', 'harga', 'harga1', 'harga2', 'harga1_darurat', 'harga2_darurat', 'harga_eceran', 'harga_grosir', 'konversi', 'item'));
    }

    public function hppJson($kode)
    {
        $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
        return response()->json(['stoks' => $stoks]);
    }

    public function pelanggansJson()
    {
        $pelanggans = Pelanggan::all('id', 'kode', 'nama', 'titipan');
        return response()->json(compact('pelanggans'));
    }

    public function pelangganJson($id)
    {
        $pelanggan = Pelanggan::find($id);
        return response()->json(['pelanggan' => $pelanggan]);
    }

    /*public function coba()
    {
        return self::kodeReturBaru();
    }*/

    private function kodeTransaksiBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_penjualan_terakhir = TransaksiPenjualan::all()->last();
        if ($transaksi_penjualan_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPN/'.$tanggal;
        } else {
            $kode_transaksi_penjualan_terakhir = $transaksi_penjualan_terakhir->kode_transaksi;
            $tanggal_transaksi_penjualan_terakhir = substr($kode_transaksi_penjualan_terakhir, 9, 10);
            // return [$kode_transaksi_penjualan_terakhir, $tanggal_transaksi_penjualan_terakhir];
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_penjualan_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_penjualan_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPN/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPN/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    private function kodeReturBaru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transaksi_penjualan_terakhir = ReturPenjualan::all()->last();
        if ($transaksi_penjualan_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TPNR/'.$tanggal;
        } else {
            $kode_transaksi_penjualan_terakhir = $transaksi_penjualan_terakhir->kode_retur;
            $tanggal_transaksi_penjualan_terakhir = substr($kode_transaksi_penjualan_terakhir, 10, 10);
            // return [$kode_transaksi_penjualan_terakhir, $tanggal_transaksi_penjualan_terakhir];
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_transaksi_penjualan_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_transaksi_penjualan_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TPNR/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TPNR/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $today = new DateTime();
        $today = $today->format('Y-m-d');

        $banks = Bank::all();
        // $users = User::where('status', 1)->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $items = array();
        $temp_items = Item::distinct()->select('kode', 'nama', 'konsinyasi', 'aktif', 'bonus_jual')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $bonus_rules = BonusRule::where('aktif', 1)->get();

        $is_setoran_buka = false;
        if (Auth::user()->level_id == 3) {
            $cek_setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->where('created_at', 'like', $today.'%')->first();
            if(sizeof($cek_setoran_buka) > 0) $is_setoran_buka = true;
        }

        foreach ($temp_items as $i => $item) {
            if($item->aktif == 1) {
                $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('aktif', 1)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                    }
                }

                if ($stoktotal > 0 && $item->konsinyasi != 3) {
                    array_push($items, $item);
                }

                //cek item bonusnya boleh terpisah egak
                if ($stoktotal > 0 && $item->konsinyasi == 3) {
                    if ($item->bonus_jual == 1) {
                        array_push($items, $item);
                    } elseif ($item->bonus_jual == 0) {
                        $items_bonuses = Item::where('kode', $item->kode)->get();
                        $is_allowed = false;
                        $cek_bonus = [];
                        foreach ($items_bonuses as $i => $item_bonus) {
                            $cek_relasi_bonus = RelasiBonus::where('bonus_id', $item_bonus->id)->first();
                            if ($cek_relasi_bonus != null) {
                                $cek_bonus = $cek_relasi_bonus;
                                break;
                            }
                        }
                        
                        if ($cek_bonus != null) {
                            $item_cek_asli = Item::where('kode', $cek_bonus->item_kode)->first();
                            if($item_cek_asli->stoktotal <= 0 ) {
                                array_push($items, $item);
                            }
                        }
                    }
                }

                // Cari stoktotal untuk item bundle
                // Jika item bundle, cari stoktotal item bundle minimal
                if ($item->konsinyasi == 2) {
                    $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $i => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                $stoktotal += $stok->jumlah;
                            }
                        }
                        if ($stoktotal > 0) {
                            array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                        }
                    }
                    // return [$stoktotals, $relasi_bundles];

                    if (count($stoktotals) > 0 && count($stoktotals) == count($relasi_bundles)) {
                        // Cari stoktotal terkecil
                        $stoktotal_terkecil = $stoktotals[0];
                        foreach ($stoktotals as $i => $stoktotal) {
                            if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                                $stoktotal_terkecil = $stoktotal;
                            }
                        }
                        array_push($items, $item);
                    }
                }
            }
        }
        
        foreach ($pelanggans as $i => $pelanggan) {
            $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $pelanggan->id)->get();
            $sisa_piutang = 0;
            $lewat_jatuh_tempo = 0;
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                $temp_sisa_piutang = 0;
                foreach ($transaksi_penjualan->piutang_dagang as $k => $piutang_dagang) {
                    $sisa_piutang += $piutang_dagang->sisa;
                    $temp_sisa_piutang += $piutang_dagang->sisa;
                }

                if ($lewat_jatuh_tempo <= 0 && $temp_sisa_piutang > 0) {
                    $jatuh_tempo = $transaksi_penjualan->jatuh_tempo;
                    $jatuh_tempo = str_replace('-', '', $jatuh_tempo);
                    $jatuh_tempo = intval($jatuh_tempo);

                    $hari_ini = new DateTime('NOW');
                    $hari_ini = $hari_ini->format('Y-m-d');
                    $hari_ini = str_replace('-', '', $hari_ini);
                    $hari_ini = intval($hari_ini);

                    if ($jatuh_tempo < $hari_ini) {
                        $lewat_jatuh_tempo = 1;
                    }
                }
            }

            $batas_piutang = $pelanggan->limit_jumlah_piutang - $sisa_piutang;
            if ($batas_piutang < 0) {
                $batas_piutang = 0;
            }

            $pelanggan->batas_piutang = $batas_piutang;
            $pelanggan->lewat_jatuh_tempo = $lewat_jatuh_tempo;
        }

        return view('transaksi_grosir.create', compact('items', 'satuans', 'pelanggans', 'banks', 'users', 'bonus_rules', 'is_setoran_buka'));
    }

    public function createAgain($id)
    {
        $today = new DateTime();
        $today = $today->format('Y-m-d');

        $banks = Bank::all();
        // $users = User::where('status', 1)->get();
        $users = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();
        $items = array();
        $temp_items = Item::distinct()->select('kode', 'nama', 'konsinyasi', 'aktif', 'bonus_jual')->get();
        $satuans = Satuan::all();
        $pelanggans = Pelanggan::where('aktif', 1)->get();
        $bonus_rules = BonusRule::where('aktif', 1)->get();

        $is_setoran_buka = false;
        if (Auth::user()->level_id == 3) {
            $cek_setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->where('created_at', 'like', $today.'%')->first();
            if(sizeof($cek_setoran_buka) > 0) $is_setoran_buka = true;
        }

        foreach ($temp_items as $i => $item) {
            if($item->aktif == 1) {
                $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('aktif', 1)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                    }
                }

                if ($stoktotal > 0 && $item->konsinyasi != 3) {
                    array_push($items, $item);
                }

                //cek item bonusnya boleh terpisah egak
                if ($stoktotal > 0 && $item->konsinyasi == 3) {
                    if ($item->bonus_jual == 1) {
                        array_push($items, $item);
                    } elseif ($item->bonus_jual == 0) {
                        $items_bonuses = Item::where('kode', $item->kode)->get();
                        $is_allowed = false;
                        $cek_bonus = [];
                        foreach ($items_bonuses as $i => $item_bonus) {
                            $cek_relasi_bonus = RelasiBonus::where('bonus_id', $item_bonus->id)->first();
                            if ($cek_relasi_bonus != null) {
                                $cek_bonus = $cek_relasi_bonus;
                                break;
                            }
                        }
                        
                        if ($cek_bonus != null) {
                            $item_cek_asli = Item::where('kode', $cek_bonus->item_kode)->first();
                            if($item_cek_asli->stoktotal <= 0 ) {
                                array_push($items, $item);
                            }
                        }
                    }
                }

                // Cari stoktotal untuk item bundle
                // Jika item bundle, cari stoktotal item bundle minimal
                if ($item->konsinyasi == 2) {
                    $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $i => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                $stoktotal += $stok->jumlah;
                            }
                        }
                        if ($stoktotal > 0) {
                            array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                        }
                    }
                    // return [$stoktotals, $relasi_bundles];

                    if (count($stoktotals) > 0 && count($stoktotals) == count($relasi_bundles)) {
                        // Cari stoktotal terkecil
                        $stoktotal_terkecil = $stoktotals[0];
                        foreach ($stoktotals as $i => $stoktotal) {
                            if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                                $stoktotal_terkecil = $stoktotal;
                            }
                        }
                        array_push($items, $item);
                    }
                }
            }
        }
        
        foreach ($pelanggans as $i => $pelanggan) {
            $transaksi_penjualans = TransaksiPenjualan::where('pelanggan_id', $pelanggan->id)->get();
            $sisa_piutang = 0;
            $lewat_jatuh_tempo = 0;
            foreach ($transaksi_penjualans as $j => $transaksi_penjualan) {
                $temp_sisa_piutang = 0;
                foreach ($transaksi_penjualan->piutang_dagang as $k => $piutang_dagang) {
                    $sisa_piutang += $piutang_dagang->sisa;
                    $temp_sisa_piutang += $piutang_dagang->sisa;
                }

                if ($lewat_jatuh_tempo <= 0 && $temp_sisa_piutang > 0) {
                // if ($lewat_jatuh_tempo <= 0) {
                    $jatuh_tempo = $transaksi_penjualan->jatuh_tempo;
                    $jatuh_tempo = str_replace('-', '', $jatuh_tempo);
                    $jatuh_tempo = intval($jatuh_tempo);

                    $hari_ini = new DateTime('NOW');
                    $hari_ini = $hari_ini->format('Y-m-d');
                    $hari_ini = str_replace('-', '', $hari_ini);
                    $hari_ini = intval($hari_ini);

                    if ($jatuh_tempo < $hari_ini) {
                        $lewat_jatuh_tempo = 1;
                    }
                }
            }

            $batas_piutang = $pelanggan->limit_jumlah_piutang - $sisa_piutang;
            if ($batas_piutang < 0) {
                $batas_piutang = 0;
            }

            $pelanggan->batas_piutang = $batas_piutang;
            $pelanggan->lewat_jatuh_tempo = $lewat_jatuh_tempo;
        }
        // return $pelanggans;
        $transaksi_penjualan = TransaksiPenjualan::where('id', $id)->whereIn('status', ['po_eceran', 'po_grosir'])->first();
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            return view('transaksi_grosir.create_again', compact('banks', 'users', 'items', 'satuans', 'pelanggans', 'transaksi_penjualan', 'relasi_transaksi_penjualan', 'bonus_rules', 'is_setoran_buka'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        // return $request->all();

        $data  = array();
        $akuns = array();

        $laba = 0;
        $harga_beli = 0;
        $harga_jual = 0;
        $ppn_keluar = 0;
        $netto_jual = 0;

        $akun_aset = Akun::where('kode', Akun::Aset)->first();

        foreach ($request->item_kode as $i => $kode) {
            $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
            $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;

            $hargas = Harga::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->where('jumlah', '<=', $request->jumlah[$i])->get()->last();
            $tmp_jual = $hargas->harga * $request->jumlah[$i];
            $harga_jual += $tmp_jual;

            for ($j = 0; $j < $jumlah; $j++) {
                $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('created_at', 'asc')->first();
                $stoks->jumlah -= 1;
                $harga_beli += $stoks->harga;

                $items = Item::where('kode', $kode)->where('stoktotal', '>', 0)->first();
                $items->stoktotal -= 1;

                $stoks->save();
                $items->save();
            }

            $laba = $harga_jual - $harga_beli;

            $item = Item::where('kode', $kode)->first();
            $jumlah_bonus = floor($jumlah/$item->syarat_bonus);

            $bonus = Bonus::find($item->bonus_id);
            $bonus->stoktotal -= $jumlah_bonus;
            $bonus->save();

            $data[$kode] = [
                'jumlah' => $jumlah,
                'jumlah_bonus' => $jumlah_bonus,
                'harga' => $request->harga[$i],
                'subtotal' => $request->subtotal[$i]
            ];
        }

        $transaksi_grosir = new TransaksiPenjualan();

        $transaksi_grosir->kode_transaksi = $request->kode_transaksi;
        $transaksi_grosir->harga_total = $request->harga_total;
        $transaksi_grosir->jumlah_bayar = $request->jumlah_bayar;
        $transaksi_grosir->pelanggan_id = $request->pelanggan;
        $transaksi_grosir->status = 'grosir';
        $transaksi_grosir->user_id = Auth::id();

        $transaksi_grosir->no_debit = $request->no_debit;
        $transaksi_grosir->no_cek = $request->no_cek;
        $transaksi_grosir->no_bg = $request->no_bg;

        $transaksi_grosir->nominal_tunai = $request->nominal_tunai;
        $transaksi_grosir->nominal_debit = $request->nominal_debit;
        $transaksi_grosir->nominal_cek = $request->nominal_cek;
        $transaksi_grosir->nominal_bg = $request->nominal_bg;

        $ppn_keluar = $harga_jual / 10;
        $netto_jual = $harga_jual - $ppn_keluar;

        $total_piutang = $request->harga_total - $request->jumlah_bayar;

        $akuns = [
            0 => ['kode_akun' => Akun::KasTunai, 'nominal' => $request->nominal_tunai],
            1 => ['kode_akun' => Akun::KasBank, 'nominal' => $request->nominal_debit],
            2 => ['kode_akun' => Akun::KasCek, 'nominal' => $request->nominal_cek],
            3 => ['kode_akun' => Akun::KasBG, 'nominal' => $request->nominal_bg],
            4 => ['kode_akun' => Akun::PiutangDagang, 'nominal' => $total_piutang],         
            5 => ['kode_akun' => Akun::Penjualan, 'nominal' => $netto_jual],
            6 => ['kode_akun' => Akun::PPNKeluaran, 'nominal' => $ppn_keluar],
            7 => ['kode_akun' => Akun::HPP, 'nominal' => -$harga_beli],
            8 => ['kode_akun' => Akun::Persediaan, 'nominal' => -$harga_beli]
        ];

        if (!empty($request->bank)) {
            $banks = Bank::find($request->bank);
            $banks->nominal += intval($request->nominal_debit);
            $banks->update();
        }

        if ($transaksi_grosir->save()) {
            $transaksi = TransaksiPenjualan::all()->last();

            foreach ($request->item_kode as $i => $kode) {
                $relasi_transaksi_grosir = new RelasiTransaksiPenjualan();
                $relasi_transaksi_grosir->item_kode = $kode;
                $relasi_transaksi_grosir->transaksi_penjualan_id = $transaksi->id;
                $relasi_transaksi_grosir->harga = $data[$kode]['harga'];
                $relasi_transaksi_grosir->jumlah = $data[$kode]['jumlah'];
                $relasi_transaksi_grosir->jumlah_bonus = $data[$kode]['jumlah_bonus'];
                $relasi_transaksi_grosir->subtotal = $data[$kode]['subtotal'];

                $relasi_transaksi_grosir->save();
            }

            if ($total_piutang <= 0) {
                $total_piutang = null;
            } else {
                $piutang = PiutangDagang::all()->last();

                if (empty($piutang)) {
                    $kode_piutang = '0001/PD/'.Util::printBulanSekarang('mm/yyyy');
                } else {
                    $mm_piutang = explode('/', $piutang->kode_transaksi)[2];
                    $yyyy_piutang = explode('/', $piutang->kode_transaksi)[3];
                    $bulan_piutang = $mm_piutang.'/'.$yyyy_piutang;

                    if ($bulan_piutang != Util::printBulanSekarang('mm/yyyy')) {
                        $kode_piutang = '0001/PD/'.Util::printBulanSekarang('mm/yyyy');
                    } else {
                        $nomor = intval(explode('/', $piutang->kode_transaksi)[0]) + 1;
                        $kode_piutang = Util::int4digit($nomor).'/PD/'.Util::printBulanSekarang('mm/yyyy');
                    }
                }

                $piutangs = new PiutangDagang();

                $piutangs->kode_transaksi = $kode_piutang;
                $piutangs->transaksi_penjualan_id = $transaksi->id;
                $piutangs->nominal = $total_piutang;
                $piutangs->sisa = $total_piutang;

                $piutangs->save();
            }

            for ($i = 0; $i < count($akuns); $i++) {
                if ($akuns[$i]['nominal'] != null) {
                    $jurnal = new Jurnal();
                    $akun = Akun::where('kode', $akuns[$i]['kode_akun'])->first();

                    $jurnal->kode_akun = $akuns[$i]['kode_akun'];

                    if ($i == 4) {
                        $jurnal->referensi = $kode_piutang;
                        $jurnal->keterangan = 'Piutang Dagang';
                    } else {
                        $jurnal->referensi = $request->kode_transaksi;
                        $jurnal->keterangan = 'Transaksi Grosir';
                    }

                    $kode = substr($akuns[$i]['kode_akun'], 0, 1);
                    if ($kode == 1 || $kode == 5) {
                        if ($akuns[$i]['nominal'] > 0) {
                            $jurnal->debet = abs($akuns[$i]['nominal']);
                            $akun->debet += abs($akuns[$i]['nominal']);
                            $akun_aset->debet += abs($akuns[$i]['nominal']);
                        } else {
                            $jurnal->kredit = abs($akuns[$i]['nominal']);
                            $akun->kredit += abs($akuns[$i]['nominal']);
                            $akun_aset->kredit += abs($akuns[$i]['nominal']);
                        }

                        $akun_aset->update();
                    } else {
                        if ($akuns[$i]['nominal'] > 0) {
                            $jurnal->kredit = abs($akuns[$i]['nominal']);
                            $akun->kredit += abs($akuns[$i]['nominal']);
                        } else {
                            $jurnal->debet = abs($akuns[$i]['nominal']);
                            $akun->debet += abs($akuns[$i]['nominal']);
                        }
                    }

                    $jurnal->save();
                    $akun->update();
                }
            }

            return redirect('/transaksi-grosir')->with('sukses', 'tambah');
        } else {
            return redirect('/transaksi-grosir')->with('gagal', 'tambah');
        }
    }*/

    public function simpanPOGrosir(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        $transaksi_penjualan->nego_total = $request->harga_total;
        if ($request->harga_akhir < $request->harga_total) $transaksi_penjualan->nego_total = $request->harga_akhir;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = 'po_grosir';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
        // $transaksi_penjualan->user_id = Auth::id();

        if ($request->jumlah_bayar != null) {
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->ongkos_kirim != '') {
            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
        } else {
            $transaksi_penjualan->ongkos_kirim = 0;
        }

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_grosir[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;
                    
                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }

                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function simpanPOEceran(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        $transaksi_penjualan->nego_total = $request->harga_total;
        if ($request->harga_akhir < $request->harga_total) $transaksi_penjualan->nego_total = $request->harga_akhir;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = 'po_eceran';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
        // $transaksi_penjualan->user_id = Auth::id();

        if ($request->jumlah_bayar != null) {
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->ongkos_kirim != '') {
            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
        } else {
            $transaksi_penjualan->ongkos_kirim = 0;
        }

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_eceran[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;

                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function simpanPOPelangganBaru(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        $transaksi_penjualan->nego_total = $request->harga_total;
        if ($request->harga_akhir < $request->harga_total) $transaksi_penjualan->nego_total = $request->harga_akhir;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = 'po_grosir';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
        // $transaksi_penjualan->user_id = Auth::id();

        if ($request->jumlah_bayar != null) {
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->ongkos_kirim != '') {
            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
        } else {
            $transaksi_penjualan->ongkos_kirim = 0;
        }

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_grosir[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->save();

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;

                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return redirect('pelanggan/create/'.$transaksi_penjualan->id);
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }

    public function ubahPO(Request $request, $id)
    {
        // return $request->all();
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        if ($request->nego_total_view > 0) $transaksi_penjualan->nego_total = $request->nego_total_view;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = $request->grosir == 'true' ? 'po_grosir' : 'po_eceran';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        // $transaksi_penjualan->user_id = Auth::id();

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->jumlah_bayar != null) {
            // $harga_total = $request->nego_total == null ? (float) $request->harga_total : (float) $request->nego_total;
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            // if ($jumlah_bayar > $harga_total) {
            //     $nominal_tunai = $harga_total - $nominal_transfer - $nominal_cek - $nominal_bg - $nominal_kartu - $nominal_titipan;
            // }

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $rtj) {
            RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $rtj->id)->delete();
            $rtj->delete();
        }

        if ($transaksi_penjualan->update()) {
            $nego_total = 0;
            foreach ($request->item_kode as $i => $kode) {
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->is_grosir == 'true' ? (float) $request->harga_grosir[$i] : (float) $request->harga_eceran[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                if ($request->nego[$i] > 0) $nego_total += $request->nego[$i];
                else $nego_total += $request->subtotal[$i];

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;

                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            if ($request->nego_total_view <= 0 || ($request->nego_total_view > 0 && $request->nego_total_view > $request->harga_akhir)) {
                $transaksi_penjualan->nego_total = $nego_total;
                $transaksi_penjualan->update();
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('sukses', 'ubah');
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah');
        }
    }

    public function bayar($id)
    {
        $is_stok_aman = true;
        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $relasi) {
            $items = Item::where('kode', $relasi->item_kode)->get();
            $jumlah = $relasi->jumlah;

            // Cek stoktotal item di tabel items
            foreach ($items as $j => $item) {
                if ($item->konsinyasi != 2) {
                    $cari_item = Item::where('kode_barang', $item->kode_barang)->first();
                    if ($cari_item->stoktotal < $jumlah) $is_stok_aman = false;
                    
                } elseif ($item->konsinyasi == 2) {
                    $items_bundle = RelasiBundle::where('item_parent', $item->kode)->get();

                    foreach ($items_bundle as $k => $item_bundle) {
                        $item_bundle_child = Item::where('kode', $item_bundle->item_child)->first();
                        $cari_item = Item::where('kode_barang', $item_bundle_child->kode_barang)->first();
                        
                        if ($cari_item->stoktotal < ($item_bundle->jumlah * $relasi->jumlah)) $is_stok_aman = false;
                    }
                }
            }

            $relasi_bonus_penjualan = RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $relasi->id)->get();
            foreach ($relasi_bonus_penjualan as $j => $rbp) {
                $bonus = Item::find($rbp->bonus_id);
                $items_bonus = Item::where('kode_barang', $bonus->kode_barang)->first();

                if ($items_bonus->stoktotal < $rbp->jumlah) $is_stok_aman = false;
            }
        }

        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $relasi_bonus_rule_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->get();
        // Cek stoktotal bonus penjualan di tabel items
        foreach ($relasi_bonus_rule_penjualan as $j => $rbrp) {
            $bonus = Item::find($rbrp->bonus_id);
            $items_bonus = Item::where('kode_barang', $bonus->kode_barang)->first();
            if ($items_bonus->stoktotal < $rbrp->jumlah) $is_stok_aman = false;
        }

        if (!$is_stok_aman) {
            if($transaksi_penjualan->status == 'po_vip') {
                if(in_array(Auth::user()->level_id, [1,2])) {
                    return redirect('transaksi-grosir-vip/create/'.$transaksi_penjualan->id)->with('gagal', 'ubah_stok');
                } else {
                    return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah_stok');
                }
            } else {
                if(in_array(Auth::user()->level_id, [1,2])) {
                    return redirect('transaksi-grosir/create/'.$transaksi_penjualan->id)->with('gagal', 'ubah_stok');
                } else {
                    return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah_stok');
                }
            }
        } else {
            $user = User::find(Auth::id());
            $transaksi_penjualan = TransaksiPenjualan::find($id);
            $transaksi_penjualan->grosir_id = $user->id;
            // return $transaksi_penjualan;
            $jumlah_bayar_temp = $transaksi_penjualan->nominal_tunai 
                            + $transaksi_penjualan->nominal_transfer 
                            + $transaksi_penjualan->nominal_kartu 
                            + $transaksi_penjualan->nominal_cek 
                            + $transaksi_penjualan->nominal_bg 
                            + $transaksi_penjualan->nominal_titipan;
            // $kembali_temp = $jumlah_bayar_temp - 
            //             ($transaksi_penjualan->nego_total > 0 ? $transaksi_penjualan->nego_total : $transaksi_penjualan->harga_total) 
            //             + $transaksi_penjualan->potongan_penjualan 
            //             - $transaksi_penjualan->ongkos_kirim;
            $kembali_temp = $jumlah_bayar_temp - $transaksi_penjualan->harga_total
                        + $transaksi_penjualan->potongan_penjualan 
                        - $transaksi_penjualan->ongkos_kirim;
            $duit_masuk = $transaksi_penjualan->nominal_tunai;

            if ($kembali_temp > 0) $duit_masuk -= $kembali_temp;

            // return $duit_masuk;
            if ($transaksi_penjualan->status == 'po_eceran') {
                $transaksi_penjualan->status = 'eceran';
                $status_transaksi = 'eceran';

                if ($user->level_id == 3) {
                    $bukaan = SetoranBuka::where('user_id', Auth::id())->first()->nominal;
                    // return Auth::id();
                    if ($bukaan == NULL) $bukaan = 0;
                    $limit = MoneyLimit::find(1);
                    $cash_drawer = CashDrawer::where('user_id', Auth::id())->first();
                    $cash_drawer->nominal += $duit_masuk;

                    $log = new LogLaci();
                    $log->penerima = Auth::user()->id;
                    $log->approve = 1;
                    $log->status = 8;
                    $log->nominal = $duit_masuk;

                    if ($cash_drawer->nominal - $bukaan > $limit->nominal) {
                        return redirect('/setoran-kasir')->with('sukses', 'tambah');
                    } else {
                        $log->save();
                        $cash_drawer->update();
                    }
                }elseif($user->level_id == 2 || $user->level_id == 1 || $user->level_id == 4){
                    
                    $laci_grosir = Laci::find(1);
                    if(Auth::user()->level->id == 1 || Auth::user()->level->id == 2){
                        $laci_grosir->owner += $duit_masuk;
                    }else{
                        $laci_grosir->grosir += $duit_masuk;
                    }
                    $laci_grosir->update();

                    if ($duit_masuk) {
                        $log = new LogLaci();
                        $log->penerima = Auth::user()->id;
                        $log->approve = 1;
                        $log->nominal = $duit_masuk;
                        $log->status = 9;
                        $log->save();
                    }
                }
            } else if ($transaksi_penjualan->status == 'po_grosir') {
                $transaksi_penjualan->status = 'grosir';
                $status_transaksi = 'grosir';

                $laci_grosir = Laci::find(1);
                if(Auth::user()->level->id == 1 || Auth::user()->level->id == 2){
                    $laci_grosir->owner += $duit_masuk;
                }else{
                    $laci_grosir->grosir += $duit_masuk;
                }
                $laci_grosir->update();

                $log = new LogLaci();
                $log->penerima = Auth::user()->id;
                $log->approve = 1;
                $log->nominal = $duit_masuk;
                $log->status = 9;
                $log->save();
            } else if ($transaksi_penjualan->status == 'po_vip') {
                $transaksi_penjualan->status = 'grosir';
                $status_transaksi = 'grosir';

                $laci_grosir = Laci::find(1);
                if(Auth::user()->level->id == 1 || Auth::user()->level->id == 2){
                    $laci_grosir->owner += $duit_masuk;
                }else{
                    $laci_grosir->grosir += $duit_masuk;
                }
                $laci_grosir->update();

                $log = new LogLaci();
                $log->penerima = Auth::user()->id;
                $log->approve = 1;
                $log->nominal = $duit_masuk;
                $log->status = 9;
                $log->save();
            }

            $transaksi_penjualan->update();
            // if($status_transaksi == 'eceran'){
            //     $cash_drawer = CashDrawer::where('user_id', Auth::id)->first();
            //     $cash_drawer->nominal += $nomnal_laci;
            //     $cash_drawer->update();
            // }elseif($transaksi_penjualan->status == 'po_grosir'){
            //     $laci_grosir = LaciGrosir::find(1);
            //     $laci_grosir->nominal += $nomnal_laci;
            //     $laci_grosir->update();
            // }

            // ngurangi jumlah pada stoks dan stoktotal pada items
            $today = date('Y-m-d');
            $nominal_persediaan = 0;
            $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();

            // $coba = $relasi_transaksi_penjualan[0];
            // $stoks = Stok::where('item_kode', $coba->item_kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
            // return [$stoks, $today, $stoks[0]->kadaluarsa > $today];

            foreach ($relasi_transaksi_penjualan as $i => $relasi) {
                $items = Item::where('kode', $relasi->item_kode)->get();
                $item_caris = Item::where('kode_barang', $items[0]->kode_barang)->get();
                // Pengurangan jumlah pada stoks untuk item biasa dan konsinyasi
                $jumlah = $relasi->jumlah;
                $stoks = Stok::where('item_kode', $relasi->item_kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
                foreach ($stoks as $j => $stok) {
                    if ($jumlah <= 0) {
                        break;
                    } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        if ($stok->jumlah <= $jumlah) {
                            // masih loop
                            $nominal_persediaan += $stok->harga * $stok->jumlah;

                            RelasiStokPenjualan::create([
                                'stok_id' => $stok->id,
                                'relasi_transaksi_penjualan_id' => $relasi->id,
                                'jumlah' => $stok->jumlah,
                                'hpp' => $stok->harga
                            ]);

                            $jumlah -= $stok->jumlah;
                            $stok->jumlah = 0;
                            $stok->update();
                        } else {
                            // loop berhenti
                            $nominal_persediaan += $stok->harga * $jumlah;

                            RelasiStokPenjualan::create([
                                'stok_id' => $stok->id,
                                'relasi_transaksi_penjualan_id' => $relasi->id,
                                'jumlah' => $jumlah,
                                'hpp' => $stok->harga
                            ]);

                            $stok->jumlah -= $jumlah;
                            $stok->update();
                            $jumlah = 0;
                        }
                    }
                }

                // Pengurangan jumlah pada stoks untuk item bundle
                if ($items[0]->konsinyasi == 2) {
                    // $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $items[0]->kode)->get();
                    foreach ($relasi_bundles as $k => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $jumlah = $relasi->jumlah * $relasi_bundle->jumlah;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($jumlah <= 0) {
                                break;
                            } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                if ($stok->jumlah <= $jumlah) {
                                    // masih loop
                                    $nominal_persediaan += $stok->harga * $stok->jumlah;

                                    RelasiStokPenjualan::create([
                                        'stok_id' => $stok->id,
                                        'relasi_transaksi_penjualan_id' => $relasi->id,
                                        'jumlah' => $stok->jumlah,
                                        'hpp' => $stok->harga
                                    ]);

                                    $jumlah -= $stok->jumlah;
                                    $stok->jumlah = 0;
                                    $stok->update();
                                } else {
                                    // loop berhenti
                                    $nominal_persediaan += $stok->harga * $jumlah;

                                    RelasiStokPenjualan::create([
                                        'stok_id' => $stok->id,
                                        'relasi_transaksi_penjualan_id' => $relasi->id,
                                        'jumlah' => $jumlah,
                                        'hpp' => $stok->harga
                                    ]);

                                    $stok->jumlah -= $jumlah;
                                    $stok->update();
                                    $jumlah = 0;
                                }
                            }
                        }

                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        // Simpan history
                        $history_jual_bundle = new HistoryJualBundle();
                        $history_jual_bundle->relasi_transaksi_penjualan_id = $relasi->id;
                        $history_jual_bundle->item_kode = $item_child;
                        $history_jual_bundle->jumlah = $relasi->jumlah * $relasi_bundle->jumlah;
                        $history_jual_bundle->harga = $stoks->last()->harga;
                        $history_jual_bundle->save();
                    }
                }

                // Update stoktotal item di tabel items
                foreach ($item_caris as $j => $item) {
                    if ($item->konsinyasi != 2) {
                        $item->stoktotal -= $relasi->jumlah;
                        $item->update();
                        // $item->stoktotal -= $relasi->jumlah;
                        // $item->update();
                    }elseif($item->konsinyasi == 2){
                        $items_bundle = RelasiBundle::where('item_parent', $item->kode)->get();
                        foreach ($items_bundle as $k => $item_bundle) {
                            $item_bundle_child = Item::where('kode', $item_bundle->item_child)->first();
                            $cari_item = Item::where('kode_barang', $item_bundle_child->kode_barang)->get();
                            foreach ($cari_item as $l => $_cari_item) {
                                $_cari_item->stoktotal -= ($item_bundle->jumlah * $relasi->jumlah);
                                $_cari_item->update();
                            }
                        }
                    }
                }

                // foreach ($item_caris as $j => $item) {
                //     if ($item->konsinyasi != 2) {
                //         $cari_item = Item::where('kode_barang', $item->kode_barang)->get();
                //         foreach ($cari_item as $key => $c_item) {
                //             $c_item->stoktotal -= $relasi->jumlah;
                //             $c_item->update();
                //         }
                //         // $item->stoktotal -= $relasi->jumlah;
                //         // $item->update();
                //     }elseif($item->konsinyasi == 2){
                //         $items_bundle = RelasiBundle::where('item_parent', $item->kode)->get();
                //         foreach ($items_bundle as $k => $item_bundle) {
                //             $item_bundle_child = Item::where('kode', $item_bundle->item_child)->first();
                //             $cari_item = Item::where('kode_barang', $item_bundle_child->kode_barang)->get();
                //             foreach ($cari_item as $l => $_cari_item) {
                //                 $_cari_item->stoktotal -= ($item_bundle->jumlah * $relasi->jumlah);
                //                 $_cari_item->update();
                //             }
                //         }
                //     }
                // }

                // Update stoktotal bonus di tabel items
                $temp_persedian_bonus = 0;
                $relasi_bonus_penjualan = RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $relasi->id)->get();
                foreach ($relasi_bonus_penjualan as $j => $rbp) {
                    $bonus = Item::find($rbp->bonus_id);
                    $items_bonus = Item::where('kode_barang', $bonus->kode_barang)->get();
                    foreach ($items_bonus as $a => $item_) {
                        $item_->stoktotal -= $rbp->jumlah;
                        $item_->update();
                    }

                    // Update stok bonus
                    $jumlah = $rbp->jumlah;
                    $stoks = Stok::where('item_kode', $bonus->kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
                    $today = date('Y-m-d');
                    foreach ($stoks as $j => $stok) {
                        if ($jumlah <= 0) {
                            break;
                        } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                            if ($stok->jumlah <= $jumlah) {
                                // masih loop
                                $temp_persedian_bonus += $stok->jumlah * $stok->harga;
                                $jumlah -= $stok->jumlah;
                                $stok->jumlah = 0;
                                $stok->update();
                            } else {
                                // loop berhenti
                                $temp_persedian_bonus += $jumlah * $stok->harga;
                                $stok->jumlah -= $jumlah;
                                $stok->update();
                                $jumlah = 0;
                            }
                        }
                    }
                }
            }

            $relasi_bonus_rule_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->get();
            // Update stoktotal bonus penjualan di tabel items
            foreach ($relasi_bonus_rule_penjualan as $j => $rbrp) {
                $bonus = Item::find($rbrp->bonus_id);
                $items_bonus = Item::where('kode_barang', $bonus->kode_barang)->get();
                foreach ($items_bonus as $a => $item_) {
                    $item_->stoktotal -= $rbrp->jumlah;
                    $item_->update();
                }

                // Update stok bonus transaksi
                $jumlah = $rbrp->jumlah;
                // return $jumlah;
                $stoks = Stok::where('item_kode', $bonus->kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
                $today = date('Y-m-d');
                foreach ($stoks as $k => $stok) {
                    if ($jumlah <= 0) {
                        break;
                    } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        if ($stok->jumlah <= $jumlah) {
                            // masih loop
                            $temp_persedian_bonus += $stok->jumlah * $stok->harga;
                            $jumlah -= $stok->jumlah;
                            $stok->jumlah = 0;
                            $stok->update();
                        } else {
                            // loop berhenti
                            $temp_persedian_bonus += $jumlah * $stok->harga;
                            $stok->jumlah -= $jumlah;
                            $stok->update();
                            $jumlah = 0;
                        }
                    }
                }
            }

            // variabel akun
            $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_kas_cek = Akun::where('kode', Akun::KasCek)->first();
            $akun_kas_bg = Akun::where('kode', Akun::KasBG)->first();
            $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
            $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
            $akun_pendapatan_dimuka = Akun::where('kode', Akun::PendapatanDimuka)->first();
            $akun_ppn_keluaran = Akun::where('kode', Akun::PPNKeluaran)->first();
            $akun_penjualan = Akun::where('kode', Akun::Penjualan)->first();
            $akun_penjualan_kredit = Akun::where('kode', Akun::PenjualanKredit)->first();
            $akun_hpp = Akun::where('kode', Akun::HPP)->first();
            $pelanggan = Pelanggan::find($transaksi_penjualan->pelanggan_id);

            // variabel perhitungan
            // $harga_total = $transaksi_penjualan->nego_total < $transaksi_penjualan->harga_total && $transaksi_penjualan->nego_total > 0 ? $transaksi_penjualan->nego_total : $transaksi_penjualan->harga_total + $transaksi_penjualan->ongkos_kirim; // harga total setelah nego
            $harga_total = $transaksi_penjualan->harga_total - $transaksi_penjualan->potongan_penjualan + $transaksi_penjualan->ongkos_kirim;

            // $harga_total = $transaksi_penjualan->harga_total;
            $potongan_penjualan = $transaksi_penjualan->potongan_penjualan;
            $ongkos_kirim = $transaksi_penjualan->ongkos_kirim;
            $no_transfer = $transaksi_penjualan->no_transfer;
            $bank_transfer = $transaksi_penjualan->bank_transfer;
            $no_kartu = $transaksi_penjualan->no_kartu;
            $bank_kartu = $transaksi_penjualan->bank_kartu;
            $jenis_kartu = $transaksi_penjualan->jenis_kartu;
            $no_cek = $transaksi_penjualan->no_cek;
            $no_bg = $transaksi_penjualan->no_bg;
            $nominal_tunai = $transaksi_penjualan->nominal_tunai;
            if ($nominal_tunai == null) $nominal_tunai = 0;
            $nominal_transfer = $transaksi_penjualan->nominal_transfer;
            if ($nominal_transfer == null) $nominal_transfer = 0;
            $nominal_kartu = $transaksi_penjualan->nominal_kartu;
            if ($nominal_kartu == null) $nominal_kartu = 0;
            $nominal_cek = $transaksi_penjualan->nominal_cek;
            if ($nominal_cek == null) $nominal_cek = 0;
            $nominal_bg = $transaksi_penjualan->nominal_bg;
            if ($nominal_bg == null) $nominal_bg = 0;
            $nominal_titipan = $transaksi_penjualan->nominal_titipan;
            if ($nominal_titipan == null) $nominal_titipan = 0;
            $jumlah_bayar = $nominal_tunai + $nominal_transfer + $nominal_kartu + $nominal_cek + $nominal_bg + $nominal_titipan;
            // $nominal_ppn_keluaran = $nominal_persediaan / 10;
            $total_penjualan =  $transaksi_penjualan->harga_total + $transaksi_penjualan->ongkos_kirim;
            $nominal_ppn_keluaran = ceil($total_penjualan / 11 * 100)/100;
            $nominal_utang = 0;
            $nominal_penjualan = $total_penjualan - $nominal_ppn_keluaran;
            // $nominal_penjualan = $harga_total - $nominal_kartu - $nominal_ppn_keluaran;
            // $nominal_penjualan_kredit = $nominal_kartu;
            // return [$jumlah_bayar - $nominal_kartu, $harga_total];

            $kembali = $jumlah_bayar - $harga_total;
            if ($kembali > 0) $nominal_tunai -= $kembali;

            // return 'jumlah bayar '.$jumlah_bayar.
            //         ' <br> nominal penjualan '.$nominal_penjualan.
            //         ' <br> nominal_ppn_keluaran '.$nominal_ppn_keluaran.
            //         ' <br> harga_total '.$harga_total.
            //         ' <br> kembali '.$kembali.
            //         ' <br> nominal_ppn_keluaran '.$nominal_ppn_keluaran.
            //         ' <br> nominal_tunai '.$nominal_tunai;

            $bank = null;
            $cek = null;
            $bg = null;
            $piutang_dagang = null;
            $keterangan = 'Transaksi Penjualan Grosir';
            //jika ada potongan
            if ($transaksi_penjualan->potongan_penjualan > 0) {
                Jurnal::create([
                    'kode_akun' => Akun::PotonganPenjualan,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $transaksi_penjualan->potongan_penjualan
                ]);
            }

            // proses update saldo akun -> insert/update pembayaran
            if ($nominal_tunai > 0) {
                $akun_kas_tunai->debet += $nominal_tunai;
                $akun_kas_tunai->update();
            }

            if ($nominal_transfer > 0) {
                $akun_kas_bank->debet += $nominal_transfer;
                $akun_kas_bank->update();
                // update saldo bank
                $bank = Bank::find($bank_transfer);
                $bank->nominal += $nominal_transfer;
                $bank->update();
                // update keterangan
                $keterangan .= "<br>" . $bank->nama_bank . ' [' . $bank->no_rekening . '] - ' . $no_transfer;
            }

            if ($nominal_kartu > 0) {
                $akun_kas_bank->debet += $nominal_kartu;
                $akun_kas_bank->update();
                // update saldo bank
                $bank = Bank::find($bank_kartu);
                $bank->nominal += $nominal_kartu;
                $bank->update();
                // update keterangan
                $keterangan .= "<br>" . $bank->nama_bank . ' [' . $bank->no_rekening . '] - ' . $no_kartu;
                // insert piutang_dagang
                // $piutang_dagang = PiutangDagang::create([
                //     'kode_transaksi' => $transaksi_penjualan->kode_transaksi,
                //     'transaksi_penjualan_id' => $transaksi_penjualan->id,
                //     'nominal' => $nominal_kartu,
                //     'sisa' => $nominal_kartu
                // ]);
            }

            if ($nominal_cek > 0) {
                $akun_kas_cek->debet += $nominal_cek;
                $akun_kas_cek->update();
                // insert cek
                $cek = Cek::create([
                    'nomor' => $no_cek,
                    'nominal' => $nominal_cek,
                    'aktif' => 1
                ]);
            }

            if ($nominal_bg > 0) {
                $akun_kas_bg->debet += $nominal_bg;
                $akun_kas_bg->update();
                // insert bg
                $bg = BG::create([
                    'nomor' => $no_bg,
                    'nominal' => $nominal_bg,
                    'aktif' => 1
                ]);
            }

            if ($nominal_titipan > 0) {
                $akun_pendapatan_dimuka->kredit -= $nominal_titipan;
                $akun_pendapatan_dimuka->update();
                $pelanggan->titipan -= $nominal_titipan;
                $pelanggan->update();
            }

            $akun_hpp->debet += $nominal_persediaan;
            $akun_hpp->update();

            // $akun_penjualan->kredit += $nominal_penjualan;
            // $akun_penjualan->update();

            // $akun_penjualan_kredit->kredit += $nominal_penjualan_kredit;
            // $akun_penjualan_kredit->update();

            // $akun_ppn_keluaran->kredit += $nominal_ppn_keluaran;
            // $akun_ppn_keluaran->update();

            $akun_persediaan->debet -= ($nominal_persediaan + $temp_persedian_bonus);
            $akun_persediaan->update();

            $nominal_utang = 0;
            // if ($jumlah_bayar < $harga_total - $transaksi_penjualan->potongan_penjualan) {
            //     $nominal_utang = $harga_total - $jumlah_bayar - $transaksi_penjualan->potongan_penjualan;
            if ($jumlah_bayar < $harga_total) {
                $nominal_utang = $harga_total - $jumlah_bayar;
                $akun_piutang_dagang->debet += $nominal_utang;
                $akun_piutang_dagang->update();
                // insert/update piutang_dagang
                if ($piutang_dagang == null) {
                    // insert
                    $piutang_dagang = PiutangDagang::create([
                        'kode_transaksi' => $transaksi_penjualan->kode_transaksi,
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'nominal' => $nominal_utang,
                        'sisa' => $nominal_utang
                    ]);
                } else {
                    // update
                    $piutang_dagang->nominal += $nominal_utang;
                    $piutang_dagang->sisa += $nominal_utang;
                    $piutang_dagang->update();
                }
            }

            $nominal_total = $nominal_tunai +
                            $nominal_transfer +
                            $nominal_kartu +
                            $nominal_cek +
                            $nominal_bg;

            Arus::create([
                'nama' => Arus::JBD,
                'nominal' => $nominal_total,
            ]);

            // proses pembuatan jurnal
            // jurnal untuk kas tunai
            if ($nominal_tunai > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_kas_tunai->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_tunai
                ]);
            }

            // jurnal untuk kas bank
            if ($nominal_transfer > 0 || $nominal_kartu > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_kas_bank->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_transfer + $nominal_kartu
                ]);
            }

            // // jurnal untuk kredit (piutang dagang)
            // if ($nominal_kartu > 0) {
            //     Jurnal::create([
            //         'kode_akun' => $akun_piutang_dagang->kode,
            //         'referensi' => $transaksi_penjualan->kode_transaksi,
            //         'keterangan' => $keterangan,
            //         'debet' => $nominal_kartu
            //     ]);
            // }

            // jurnal untuk kas cek
            if ($nominal_cek > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_kas_cek->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_cek
                ]);
            }

            // jurnal untuk kas bg
            if ($nominal_bg > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_kas_bg->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_bg
                ]);
            }

            // jurnal untuk titipan (pendapatan dimuka)
            if ($nominal_titipan > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_pendapatan_dimuka->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_titipan
                ]);
            }

            // jurnal untuk utang (piutang dagang)
            if ($nominal_utang > 0) {
                Jurnal::create([
                    'kode_akun' => $akun_piutang_dagang->kode,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_utang
                ]);
            }

            // jurnal untuk hpp
            Jurnal::create([
                'kode_akun' => $akun_hpp->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'debet' => $nominal_persediaan
            ]);

            if($temp_persedian_bonus > 0){
                Jurnal::create([
                    'kode_akun' => Akun::BonusPenjualan,
                    'referensi' => $transaksi_penjualan->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $temp_persedian_bonus
                ]);

                $akun_persediaan_bonus =  Akun::where('kode', Akun::Persediaan)->first();
                $akun_persediaan_bonus->debet -= $temp_persedian_bonus;
                $akun_persediaan_bonus->update();
            }

            // jurnal untuk penjualan
            Jurnal::create([
                'kode_akun' => $akun_penjualan->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_penjualan
            ]);

            // // jurnal untuk penjualan kredit
            // if ($nominal_kartu > 0) {
            //     Jurnal::create([
            //         'kode_akun' => $akun_penjualan_kredit->kode,
            //         'referensi' => $transaksi_penjualan->kode_transaksi,
            //         'keterangan' => $keterangan,
            //         'kredit' => $nominal_penjualan_kredit
            //     ]);
            // }

            // jurnal untuk ppn keluaran
            Jurnal::create([
                'kode_akun' => $akun_ppn_keluaran->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_ppn_keluaran
            ]);

            // jurnal untuk persediaan
            Jurnal::create([
                'kode_akun' => $akun_persediaan->kode,
                'referensi' => $transaksi_penjualan->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_persediaan + $temp_persedian_bonus
            ]);

            //jurnal puter duit
            //ppn ke pendapatan lain
            //ppn ke pendapatan lain - GAJADI
            // Jurnal::create([
            //     'kode_akun' => Akun::PPNKeluaran,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' PPN',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'debet' => $nominal_ppn_keluaran
            // ]);
            // Jurnal::create([
            //     'kode_akun' => Akun::PendapatanLain,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' PPN',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'kredit' => $nominal_ppn_keluaran
            // ]);
            // Jurnal::create([
            //     'kode_akun' => Akun::PendapatanLain,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'debet' => $nominal_ppn_keluaran
            // ]);
            // Jurnal::create([
            //     'kode_akun' => Akun::LabaRugi,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'kredit' => $nominal_ppn_keluaran
            // ]);

            // //jurnal laba ditahan
            // Jurnal::create([
            //     'kode_akun' => Akun::LabaRugi,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'debet' => $nominal_ppn_keluaran
            // ]);
            // Jurnal::create([
            //     'kode_akun' => Akun::LabaTahan,
            //     'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
            //     'keterangan' => 'PPN Penjualan, Pendapatan Lain-lain',
            //     'kredit' => $nominal_ppn_keluaran
            // ]);
            
            // $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            // $akun_laba_tahan->kredit += $nominal_ppn_keluaran;
            // $akun_laba_tahan->update();

            $akun_ppn_masuk = Akun::where('kode', Akun::PPNKeluaran)->first();
            $akun_ppn_masuk->kredit += $nominal_ppn_keluaran;
            $akun_ppn_masuk->update();

            //jurnal laba rugi
            $laba = $nominal_penjualan - ($nominal_persediaan + $temp_persedian_bonus + $transaksi_penjualan->potongan_penjualan);

            // $lrugi = $laba - $transaksi_penjualan->potongan_penjualan;
            if ($laba > 0) {
                Jurnal::create([
                    'kode_akun' => Akun::Penjualan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
                    'keterangan' => 'Laba Penjualan',
                    'debet' => $laba
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
                    'keterangan' => 'Laba Penjualan',
                    'kredit' => $laba
                ]);

                //jurnal laba ditahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
                    'keterangan' => 'Laba penjualan',
                    'debet' => $laba
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
                    'keterangan' => 'Laba penjualan',
                    'kredit' => $laba
                ]);
                $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_tahan->kredit += $laba;
                $akun_laba_tahan->update();
            } else if ($laba < 0) {
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
                    'keterangan' => 'Laba Penjualan',
                    'debet' => $laba * -1
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::Penjualan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
                    'keterangan' => 'Laba Penjualan',
                    'kredit' => $laba * -1
                ]);
                //jurnal laba ditahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
                    'keterangan' => 'Laba penjualan',
                    'debet' => $laba * -1
                ]);    
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
                    'keterangan' => 'Laba penjualan',
                    'kredit' => $laba * -1
                ]);
                $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_tahan->kredit += $laba;
                $akun_laba_tahan->update();
            }

            // if ($transaksi_penjualan->potongan_penjualan > 0) {
            //     Jurnal::create([
            //         'kode_akun' => Akun::LabaRugi,
            //         'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
            //         'keterangan' => 'Potongan Penjualan',
            //         'debet' => $transaksi_penjualan->potongan_penjualan
            //     ]);
            //     Jurnal::create([
            //         'kode_akun' => Akun::PotonganPenjualan,
            //         'referensi' => $transaksi_penjualan->kode_transaksi.' L/R',
            //         'keterangan' => 'Potongan Penjualan',
            //         'kredit' => $transaksi_penjualan->potongan_penjualan
            //     ]);
            //     //jurnal laba ditahan
            //     Jurnal::create([
            //         'kode_akun' => Akun::LabaTahan,
            //         'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
            //         'keterangan' => 'Potongan penjualan',
            //         'debet' => $transaksi_penjualan->potongan_penjualan
            //     ]);    
            //     Jurnal::create([
            //         'kode_akun' => Akun::LabaRugi,
            //         'referensi' => $transaksi_penjualan->kode_transaksi.' LDT',
            //         'keterangan' => 'Potongan penjualan',
            //     'kredit' => $transaksi_penjualan->potongan_penjualan
            //     ]);

            //     $akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
            //     $akun_laba_tahan->kredit -= $transaksi_penjualan->potongan_penjualan;
            //     $akun_laba_tahan->update();
            // }

            if($temp_persedian_bonus > 0){
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R - BP',
                    'keterangan' => 'Potongan Penjualan',
                    'debet' => $temp_persedian_bonus
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::BonusPenjualan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' L/R - BP',
                    'keterangan' => 'Potongan Penjualan',
                    'kredit' => $temp_persedian_bonus
                ]);
                //jurnal laba ditahan
                Jurnal::create([
                    'kode_akun' => Akun::LabaTahan,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT - BP',
                    'keterangan' => 'Potongan penjualan',
                    'debet' => $temp_persedian_bonus
                ]);
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $transaksi_penjualan->kode_transaksi.' LDT - BP',
                    'keterangan' => 'Potongan penjualan',
                    'kredit' => $temp_persedian_bonus
                ]);
            }
            // if ($transaksi_penjualan->status == 'po_eceran') {
            //     return redirect('transaksi-grosir/'.$transaksi_penjualan->id)->with('sukses', 'tambah_eceran');
            // } else {
            //     return redirect('transaksi-grosir/'.$transaksi_penjualan->id)->with('sukses', 'tambah');
            // }


            if($transaksi_penjualan->pelanggan_id != NULL){
                Util::down_pelanggan_transaksi($transaksi_penjualan->pelanggan_id);
                Util::up_pelanggan($transaksi_penjualan->pelanggan_id);
            }

            /*if ($transaksi_penjualan->pelanggan == null || $transaksi_penjualan->pelanggan->level == 'eceran') {
                if ($transaksi_penjualan->status == 'eceran') {
                    // Cetak eceran
                    return redirect('cetak/eceran/'.$transaksi_penjualan->id);
                } else {
                    // Cetak grosir
                    return redirect('cetak/grosir/'.$transaksi_penjualan->id);
                }
            } else {
                // Cetak grosir
                return redirect('cetak/grosir/'.$transaksi_penjualan->id);
            }*/
            if ($transaksi_penjualan->pelanggan_id == null) {
                // Cetak eceran
                return redirect('cetak/eceran/'.$transaksi_penjualan->id);
            } else if ($transaksi_penjualan->pelanggan->level == 'eceran') {
                if ($transaksi_penjualan->status == 'eceran') {
                    // Cetak eceran
                    return redirect('cetak/eceran/'.$transaksi_penjualan->id);
                } else {
                    // Cetak grosir
                    return redirect('cetak/grosir/'.$transaksi_penjualan->id);
                }
            } else {
                // Cetak grosir
                return redirect('cetak/grosir/'.$transaksi_penjualan->id);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi_grosir = TransaksiPenjualan::with('piutang_dagang')->where('id', $id)->whereIn('status', ['eceran', 'grosir', 'vip'])->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $satuans = Satuan::all();
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $id)->first();
            $relasi_transaksi_grosir = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
            $item_list = Item::select(
                    DB::raw('kode'),
                    DB::raw('nama'),
                    DB::raw('diskon'),
                    DB::raw('aktif'),
                    DB::raw('konsinyasi'))
                ->where('konsinyasi', '!=', 2)
                // ->where('konsinyasi', 1)
                ->where('aktif', 1)
                ->groupBy('kode')
                ->orderBy('kode', 'asc')->get();

            $jumlah_item_asli = 0;
            $show_relasi_bundles = array();
            $index_show_relasi_bundles = 0;
            $is_nego = false;
            foreach ($relasi_transaksi_grosir as $i => $relasi) {
                if ($relasi->nego > 0) $is_nego = true;
                if ($relasi->item->konsinyasi < 2) {
                    $jumlah_item_asli++;
                } else {
                    $relasi_bundles = RelasiBundle::where('item_parent', $relasi->item_kode)->get();
                    $jumlah_item_asli += $relasi_bundles->count();
                    $show_relasi_bundles[$index_show_relasi_bundles] = $relasi_bundles;
                    $index_show_relasi_bundles++;
                }

                $relasi_bonus_penjualan = RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $relasi->id)->get();
                if ($relasi_bonus_penjualan->count() <= 0) {
                    $relasi->bonuses = null;
                } else {
                    $bonuses = array();
                    foreach ($relasi_bonus_penjualan as $j => $rbj) {
                        // $bonuses[$j] = [
                        //     'bonus' => $rbj->bonus,
                        //     'jumlah' => $rbj->jumlah
                        // ];
                        $item_kode =  Item::find($rbj->bonus_id)->kode;
                        $satuan_bonus = RelasiSatuan::where('item_kode', $item_kode)->orderBy('konversi', 'dsc')->get();
                        // return $satuan_bonus;
                        $text_jumlah = '';
                        $temp_jumlah = $rbj->jumlah;
                        foreach ($satuan_bonus as $a => $satuan) {
                            if ($temp_jumlah > 0) {
                                $jumlah_ = intval($temp_jumlah / $satuan->konversi);
                                if ($jumlah_ > 0) {
                                    $text_jumlah .= $jumlah_;
                                    $text_jumlah .= ' ';
                                    $text_jumlah .= $satuan->satuan->kode;
                                    $temp_jumlah = $temp_jumlah % $satuan->konversi;

                                    if ($a != sizeof($satuan_bonus) - 1 && $temp_jumlah > 0) $text_jumlah .= ' ';
                                }
                            }
                        }

                        $bonuses[$j] = [
                            'bonus' => $rbj->bonus,
                            'jumlah' => $text_jumlah
                        ];
                    }
                    $relasi->bonuses = $bonuses;
                }
            }

            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $id)->get();

            // return [$transaksi_grosir->jumlah_bayar, $transaksi_grosir->nego_total, $transaksi_grosir->potongan_penjualan] ;
            $kembali = 0;
            // if ($transaksi_grosir->jumlah_bayar != null && $transaksi_grosir->jumlah_bayar > 0) {
                // $kembali = $transaksi_grosir->jumlah_bayar - ($transaksi_grosir->nego_total>0?$transaksi_grosir->nego_total:$transaksi_grosir->harga_total) + $transaksi_grosir->potongan_penjualan - $transaksi_grosir->ongkos_kirim;
                $kembali = $transaksi_grosir->jumlah_bayar - $transaksi_grosir->harga_total + $transaksi_grosir->potongan_penjualan - $transaksi_grosir->ongkos_kirim;
            // }
            // if ($kembali <= 0) $kembali = 0;

            // return view('transaksi_grosir.show', compact('transaksi_grosir', 'relasi_transaksi_grosir'));
            $rule = ReturRule::find(1);
            $interval = "P".$rule->syarat."D";
            $today = new DateTime();

            $batas = new DateTime($transaksi_grosir->created_at);
            $batas_retur = $batas->add(new DateInterval($interval));

            $boleh_cetak = false;
            $cetak_nota = CetakNota::where('transaksi_penjualan_id', $id)->first();
            if ($cetak_nota->cetak == 1) $boleh_cetak = true;

            $rule = ReturRule::find(1);
            $interval = "P".$rule->syarat."D";
            $today = new DateTime();

            $batas = new DateTime($transaksi_grosir->created_at);
            $batas_retur = $batas->add(new DateInterval($interval));
            $retur_show = false;
            $aktivasi_show = false;

            if((in_array(Auth::user()->level_id, [1,2,3]) && $rule->syarat != 0 && $today <= $batas_retur) || $transaksi_grosir->can_retur == 1){
                $retur_show = true;

            }
            $struk = true;
            if ($transaksi_grosir->pelanggan_id == null ||
                ($transaksi_grosir->pelanggan->level == 'eceran' && $transaksi_grosir->status == 'eceran')) {
                $struk = true;
            } elseif ($transaksi_grosir->pelanggan_id != null){
                if ($transaksi_grosir->pelanggan->level == 'grosir' ||
                    ($transaksi_grosir->pelanggan->level == 'eceran' && $transaksi_grosir->status == 'grosir')) {
                    $struk = false;
                }
            }

            if(in_array(Auth::user()->level_id, [1,2])){
                $aktivasi_show = true;
            }
            // return [$retur_show, $aktivasi_show];
            return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'piutang_dagang', 'relasi_transaksi_grosir', 'relasi_bonus_rules_penjualan', 'is_nego', 'kembali', 'show_relasi_bundles', 'item_list', 'today', 'rule', 'batas_retur', 'boleh_cetak', 'retur_show', 'aktivasi_show', 'struk'));
        }
    }

    public function cetak_nota($transaksi_penjualan_id)
    {
        $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->whereIn('status', ['eceran', 'grosir', 'vip'])->first();
        if ($transaksi_penjualan == null) {
            return redirect('transaksi-grosir');
        } else {
            // Disable button
            // $user = Auth::user();
            // $user->cetak_nota = 0;
            // $user->update();
            $cetak_nota = CetakNota::where('transaksi_penjualan_id', $transaksi_penjualan_id)->first();
            $cetak_nota->cetak = 0;
            $cetak_nota->update();

            // Redirect
            /*if ($transaksi_penjualan->pelanggan == null || $transaksi_penjualan->pelanggan->level == 'eceran') {
                if ($transaksi_penjualan->status == 'eceran') {
                    // Cetak eceran
                    return redirect('cetak/eceran/'.$transaksi_penjualan->id);
                } else {
                    // Cek eceran
                    return redirect('cetak/grosir/'.$transaksi_penjualan->id);
                }
            } else {
                // Cek eceran 
                return redirect('cetak/grosir/'.$transaksi_penjualan->id);
            }*/
            if ($transaksi_penjualan->pelanggan_id == null) {
                // Cetak eceran
                return redirect('cetak/eceran/'.$transaksi_penjualan->id);
            } else if ($transaksi_penjualan->pelanggan->level == 'eceran') {
                if ($transaksi_penjualan->status == 'eceran') {
                    // Cetak eceran
                    return redirect('cetak/eceran/'.$transaksi_penjualan->id);
                } else {
                    // Cetak grosir
                    return redirect('cetak/grosir/'.$transaksi_penjualan->id);
                }
            } else {
                // Cetak grosir
                return redirect('cetak/grosir/'.$transaksi_penjualan->id);
            }
        }
    }

    public function formCairkanKredit($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $id)->first();
            if ($piutang_dagang->sisa > 0) {
                $banks = Bank::all();
                return view('transaksi_grosir.form_cairkan_kredit', compact('transaksi_grosir', 'banks'));
            } else {
                return redirect('transaksi-grosir');
            }
        }
    }

    public function SimpanCairkanKredit(Request $request, $id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            // return [$request->all(), $transaksi_grosir];
            $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
            $piutang_dagang = PiutangDagang::where('transaksi_penjualan_id', $transaksi_grosir->id)->first();

            $nominal_kredit = $transaksi_grosir->nominal_kredit;
            $keterangan = $request->keterangan;
            $tujuan = $request->tujuan;

            if ($tujuan == 'tunai') {
                $akun_kas_tunai->debet += $nominal_kredit;
                $akun_kas_tunai->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_tunai->kode,
                    'referensi' => $transaksi_grosir->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_kredit
                ]);
            } else if ($tujuan == 'bank') {
                $akun_kas_bank->debet += $nominal_kredit;
                $akun_kas_bank->update();

                $bank = Bank::find($request->bank_id);
                $bank->nominal += $nominal_kredit;
                $bank->update();

                Jurnal::create([
                    'kode_akun' => $akun_kas_bank->kode,
                    'referensi' => $transaksi_grosir->kode_transaksi,
                    'keterangan' => $keterangan,
                    'debet' => $nominal_kredit
                ]);
            }

            $piutang_dagang->sisa -= $nominal_kredit;
            $piutang_dagang->update();

            $akun_piutang_dagang->debet -= $nominal_kredit;
            $akun_piutang_dagang->update();

            Jurnal::create([
                'kode_akun' => $akun_piutang_dagang->kode,
                'referensi' => $transaksi_grosir->kode_transaksi,
                'keterangan' => $keterangan,
                'kredit' => $nominal_kredit
            ]);

             return redirect('transaksi-grosir/'.$id)->with('sukses', 'kredit');
        }
    }

    public function returIndex($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->whereIn('status', ['eceran', 'grosir', 'vip'])->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $retur_penjualans = ReturPenjualan::where('transaksi_penjualan_id', $transaksi_grosir->id)->get();
            // foreach ($retur_penjualans as $i => $retur_penjualan) {
            //     $relasi_retur_penjualan = RelasiReturPenjualan::where('retur_penjualan_id', $retur_penjualan->id)->get();
            //     $retur_penjualan->relasi_retur_penjualan = $relasi_retur_penjualan;
            // }
            // return $transaksi_grosir;
            return view('transaksi_grosir.retur_index', compact('transaksi_grosir', 'retur_penjualans'));
        }
    }

    /*public function returCreate($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->where('status', 'grosir')->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            // buat kode_retur_baru
            $tanggal = date('d/m/Y');
            $kode_retur_baru = '';
            $retur_penjualan_terakhir = ReturPenjualan::all()->last();
            if ($retur_penjualan_terakhir == null) {
                // buat kode_retur_baru
                $kode_retur_baru = '0001/RTJ/'.$tanggal;
            } else {
                $kode_retur_penjualan_terakhir = $retur_penjualan_terakhir->kode_retur;
                $tanggal_retur_penjualan_terakhir = substr($kode_retur_penjualan_terakhir, 9, 10);
                // buat kode_retur dari kode_retur terakhir + 1
                if ($tanggal_retur_penjualan_terakhir == $tanggal) {
                    $kode_terakhir = substr($kode_retur_penjualan_terakhir, 0, 4);
                    $kode_baru = intval($kode_terakhir) + 1;
                    $kode_retur_baru = Util::int4digit($kode_baru).'/RTJ/'.$tanggal;
                } else {
                    // buat kode_retur_baru
                    $kode_retur_baru = '0001/RTJ/'.$tanggal;
                }
            }

            // $satuans = Satuan::all();
            // $relasi_transaksi_grosir = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();

            // // return view('transaksi_grosir.show', compact('transaksi_grosir', 'relasi_transaksi_grosir'));
            // return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'relasi_transaksi_grosir'));
        }
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function returCreate($id)
    {
        $transaksi_grosir = TransaksiPenjualan::where('id', $id)->whereIn('status', ['eceran', 'grosir', 'vip'])->first();
        if ($transaksi_grosir == null) {
            return redirect('transaksi-grosir');
        } else {
            $items = Item::distinct()->select('kode', 'nama', 'aktif')->get();
            $satuans = Satuan::all();
            $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
            $item_list = array();
            $j = 0;
            $is_nego = false;
            foreach ($relasi_transaksi_penjualan as $i => $relasi) {
                if ($relasi->nego > 0) $is_nego = true;

                $item = Item::where('kode', $relasi->item_kode)->first();
                if ($item->konsinyasi == 2 && $item->retur == 1) {
                    $relasi_bundles =  RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $key => $relasi_bundle) {
                        $item_bundle = Item::where('kode', $relasi_bundle->item_child)->first();
                        if ($item_bundle->retur == 1) {
                            $item_list[$j]['item_kode'] = $relasi_bundle->item_child;
                            $item_list[$j]['item_parent'] = $relasi_bundle->item_parent;
                            $item_list[$j]['nama'] = $item_bundle->nama;

                            $j += 1;
                        }
                    }
                } else {
                    $item_relasi = Item::where('kode', $relasi->item_kode)->first();
                    if ($item_relasi->retur == 1) {
                        $item_list[$j]['item_kode'] = $relasi->item_kode;
                        $item_list[$j]['item_parent'] = '';
                        $item_list[$j]['nama'] = $item_relasi->nama;

                        $j += 1;
                    }
                }

                $relasi_bonus_penjualan = RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $relasi->id)->get();
                if ($relasi_bonus_penjualan->count() <= 0) {
                    $relasi->bonuses = null;
                } else {
                    $bonuses = array();
                    foreach ($relasi_bonus_penjualan as $j => $rbj) {
                        $bonuses[$j] = [
                            'bonus' => $rbj->bonus,
                            'jumlah' => $rbj->jumlah
                        ];
                    }
                    $relasi->bonuses = $bonuses;
                }
            }

            $temp_items = Item::distinct()->select('kode', 'nama', 'konsinyasi', 'aktif', 'bonus_jual')->where('aktif', 1)->whereNotIn('konsinyasi', [2])->get();

            $item_list2 = array();
            foreach ($temp_items as $i => $item) {
                $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                $today = date('Y-m-d');
                $stoktotal = 0;
                foreach ($stoks as $j => $stok) {
                    if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        $stoktotal += $stok->jumlah;
                    }
                }

                if ($stoktotal > 0 && $item->konsinyasi != 3) {
                    array_push($item_list2, $item);
                }

                //cek item bonusnya boleh terpisah egak
                if ($stoktotal > 0 && $item->konsinyasi == 3 && $item->bonus_jual == 1) {
                    array_push($item_list2, $item);
                }

                // Cari stoktotal untuk item bundle
                // Jika item bundle, cari stoktotal item bundle minimal
                if ($item->konsinyasi == 2) {
                    $stoktotals = [];
                    $relasi_bundles = RelasiBundle::where('item_parent', $item->kode)->get();
                    foreach ($relasi_bundles as $i => $relasi_bundle) {
                        $item_child = $relasi_bundle->item_child;
                        $stoks = Stok::where('item_kode', $item_child)->where('jumlah', '>', 0)->where('rusak', 0)->orderBy('harga', 'desc')->get();
                        $today = date('Y-m-d');
                        $stoktotal = 0;
                        foreach ($stoks as $j => $stok) {
                            if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                                $stoktotal += $stok->jumlah;
                            }
                        }
                        if ($stoktotal > 0) {
                            array_push($stoktotals, intval($stoktotal / $relasi_bundle->jumlah));
                        }
                    }
                    // return [$stoktotals, $relasi_bundles];
                    if (count($stoktotals) > 0 && count($stoktotals) == count($relasi_bundles)) {
                        // return $stoktotals[0];
                        // Cari stoktotal terkecil
                        $stoktotal_terkecil = $stoktotals[0];
                        // $stoktotal_terkecil = 0;
                        foreach ($stoktotals as $i => $stoktotal) {
                            if ($i > 0 && $stoktotal < $stoktotal_terkecil) {
                                $stoktotal_terkecil = $stoktotal;
                            }
                        }
                        array_push($item_list2, $item);
                    }
                }
            }

            $relasi_bonus_rules_penjualan = RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $id)->get();

            $kembali = 0;
            if ($transaksi_grosir->jumlah_bayar != null && $transaksi_grosir->jumlah_bayar > 0) {
                $kembali = $transaksi_grosir->jumlah_bayar 
                        - $transaksi_grosir->harga_total + $transaksi_grosir->potongan_penjualan - $transaksi_grosir->ongkos_kirim;


                        // ($transaksi_grosir->nego_total > 0 ? $transaksi_grosir->nego_total : $transaksi_grosir->harga_total) + $transaksi_grosir->potongan_penjualan - $transaksi_grosir->ongkos_kirim;
            }
            // if ($kembali <= 0) $kembali = 0;
            $user = Auth::user();

            $piutangs = PiutangDagang::where('transaksi_penjualan_id',  $transaksi_grosir->id)->where('sisa', '>', 0)->get();

            $kredits = Kredit::all();
            $banks = Bank::all();
            $ceks = Cek::where('aktif', 1)->get();
            $bgs = BG::where('aktif', 1)->get();
            $item_all_ = Item::groupBy('kode_barang')->get();
            // return $item_list2;
            return view('transaksi_grosir.retur_create', compact('kredits', 'banks', 'ceks', 'bgs', 'piutangs', 'satuans', 'items', 'transaksi_grosir', 'relasi_transaksi_penjualan', 'relasi_bonus_rules_penjualan', 'is_nego', 'kembali', 'user', 'item_list', 'item_list2', 'item_all_'));

            // return view('transaksi_grosir.show', compact('satuans', 'transaksi_grosir', 'relasi_po_penjualan'));
        }
    }

    public function returItemJson($transaksi_penjualan_id, $item_kode, $item_parent)
    {
        $transaksi_penjualan = TransaksiPenjualan::where('id', $transaksi_penjualan_id)->whereIn('status', ['eceran', 'grosir', 'vip'])->first();
        if ($transaksi_penjualan == null) {
            return redirect('po-penjualan');
        } else {
            $item = Item::where('kode', $item_kode)->first();
            $stoks = Stok::where('item_kode', $item_kode)->where('jumlah', '>', 0)->where('rusak', 0)->get();
            $today = date('Y-m-d');
            $stoktotal = 0;
            foreach ($stoks as $j => $stok) {
                if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                    $stoktotal += $stok->jumlah;
                }
            }
            // return $stoktotal;
            $item->stoktotal = $stoktotal;

            $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->where('item_kode', $item_kode)->first();
            $history_jual_bundle = null;

            if ($item_parent != '-') {
                $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan_id)->where('item_kode', $item_parent)->first();
                $history_jual_bundle = HistoryJualBundle::where('relasi_transaksi_penjualan_id', $relasi_transaksi_penjualan->id)->where('item_kode', $item_kode)->first();
            }

            return response()->json(compact('transaksi_penjualan', 'relasi_transaksi_penjualan', 'item', 'history_jual_bundle'));
        }
    }

    public function returHargaJson($transaksi_penjualan_id, $item_kode, $jumlah, $satuan_id)
    {
        $harga = Harga::where('item_kode', $item_kode)->where('jumlah', '<=',  $jumlah)->where('satuan_id', $satuan_id)->get()->last();
        $item = Item::where('kode', $item_kode)->first();

        return response()->json(compact('harga', 'item'));
    }

    /*public function konversiJson($transaksi_penjualan_id, $item_kode, $satuan)
    {
        $satuan = RelasiSatuan::where('item_kode', $item_kode)->where('satuan_id', $satuan)->first();
        return response()->json(compact('satuan'));
    }*/

    public function returSimpan(Request $request, $id)
    {
        // return $request->all();
        // variabel akun
        $akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_kas_cek = Akun::where('kode', Akun::KasCek)->first();
        $akun_kas_bg = Akun::where('kode', Akun::KasBG)->first();
        $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
        $akun_piutang_dagang = Akun::where('kode', Akun::PiutangDagang)->first();
        $akun_pendapatan_dimuka = Akun::where('kode', Akun::PendapatanDimuka)->first();
        $akun_ppn_keluaran = Akun::where('kode', Akun::PPNKeluaran)->first();
        $akun_penjualan = Akun::where('kode', Akun::Penjualan)->first();
        $akun_penjualan_kredit = Akun::where('kode', Akun::PenjualanKredit)->first();
        $akun_hpp = Akun::where('kode', Akun::HPP)->first();

        $transaksi_penjualan = TransaksiPenjualan::find($id);
        if($transaksi_penjualan->pelanggan_id != NULL){
            $pelanggan = Pelanggan::find($transaksi_penjualan->pelanggan_id);
        }
        $kode_retur = $this->kodeReturBaru();
        $today = date('Y-m-d');
        $hpp_in = 0;
        $hpp_out = 0;
        $relasi_retur = array();
        $stok_retur = array();
        $pembayaran = array();
        $w = 0;
        $y = 0;

        $pembayaran['tunai'] = NULL;
        $pembayaran['nominal_transfer'] = NULL;
        $pembayaran['no_transfer'] = NULL;
        $pembayaran['bank_transfer'] = NULL;
        $pembayaran['nominal_kartu'] = NULL;
        $pembayaran['no_kartu'] = NULL;
        $pembayaran['bank_kartu'] = NULL;
        $pembayaran['jenis_kartu'] = NULL;
        $pembayaran['nominal_cek'] = NULL;
        $pembayaran['no_cek'] = NULL;
        $pembayaran['bank_cek'] = NULL;
        $pembayaran['cek_lunas'] = NULL;
        $pembayaran['nominal_bg'] = NULL;
        $pembayaran['no_bg'] = NULL;
        $pembayaran['bank_bg'] = NULL;
        $pembayaran['bg_lunas'] = NULL;
        $pembayaran['nominal_titipan'] = NULL;
        $pembayaran['nominal_piutang'] = NULL;

        $pembayaran['tunai_in'] = NULL;
        $pembayaran['nominal_transfer_in'] = NULL;
        $pembayaran['no_transfer_in'] = NULL;
        $pembayaran['bank_transfer_in'] = NULL;
        $pembayaran['nominal_kartu_in'] = NULL;
        $pembayaran['no_kartu_in'] = NULL;
        $pembayaran['bank_kartu_in'] = NULL;
        $pembayaran['jenis_kartu_in'] = NULL;
        $pembayaran['nominal_cek_in'] = NULL;
        $pembayaran['no_cek_in'] = NULL;
        $pembayaran['nominal_bg_in'] = NULL;
        $pembayaran['no_bg_in'] = NULL;
        $pembayaran['nominal_titipan_in'] = NULL;

        foreach ($request->item_kode as $i => $kode) {
            // row baru di stok untuk barang masuk sesuai jumlah dan kerusakan
            // GANTI UANG, BARANG SAMA, BARANG LAIN
            // barang masuk
            foreach ($request->jumlah_rusak[$kode] as $j => $jumlah_rusak) {
                if ($jumlah_rusak > 0) {
                    $is_rusak = false;
                    if ($request->is_rusak[$kode][$j] == 1) {
                        $is_rusak = true;
                    }
                    $item_ = Item::where('kode', $kode)->first();
                    $item_cari = Item::where('kode_barang', $item_->kode_barang)->get();

                    $relasi_stok_penjualan = RelasiStokPenjualan::find($request->relasi_stok_penjualan[$kode][$j]);
                    // insert row
                    Stok::create([
                        'item_id' => $relasi_stok_penjualan->stok->item_id,
                        'item_kode' => $kode,
                        'transaksi_pembelian_id' => $relasi_stok_penjualan->stok->transaksi_pembelian_id,
                        'harga' => $relasi_stok_penjualan->hpp,
                        'jumlah' => $jumlah_rusak,
                        'rusak' => $is_rusak,
                        'retur' => true,
                        'kadaluarsa' => $relasi_stok_penjualan->stok->kadaluarsa
                    ]);

                    $stok_retur[$y] = [
                        'stok_id' => $relasi_stok_penjualan->stok_id,
                        'jumlah' => $jumlah_rusak,
                        'retur' => 0,
                        'hpp' => $relasi_stok_penjualan->hpp,
                    ];

                    $hpp_in += $relasi_stok_penjualan->hpp * $jumlah_rusak;
                    $y += 1;

                    foreach ($item_cari as $j => $item) {
                        $item->stoktotal += intval($jumlah_rusak);
                        $item->update();
                    }
                }
            }

            // //barang masuk relasi
            foreach ($request->item_kode as $a => $item){
                $relasi_retur[$w] = [
                    'item_kode' => $item,
                    'jumlah' => $request->jumlah[$a],
                    'harga' => $request->harga[$a],
                    'subtotal' => $request->subtotal[$a],
                    'retur' => 0,
                ];
                $w +=1;
            }

            // update row di stok untuk barang keluar sesuai jumlah total (item)
            // BARANG SAMA
            if ($request->status == 'sama') {
                $item_ = Item::where('kode', $kode)->first();
                $item_cari = Item::where('kode_barang', $item_->kode_barang)->get();
                $jumlah = intval($request->jumlah[$i]);
                $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->where('item_kode', $kode)->first();

                $stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->where('rusak', 0)->where('retur', 0)->get();
                foreach ($stoks as $j => $stok) {
                    if ($jumlah <= 0) {
                        break;
                    } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                        if ($stok->jumlah <= $jumlah) {
                            // masih loop
                            $stok_retur[$y] = [
                                'stok_id' => $stok->id,
                                'jumlah' => $stok->jumlah,
                                'retur' => 1,
                                'hpp' => $stok->harga,
                            ];

                            $hpp_out += $stok->jumlah * $stok->harga;
                            $jumlah -= $stok->jumlah;
                            $stok->jumlah = 0;
                            $stok->update();
                        } else {
                            // loop berhenti
                            $stok_retur[$y] = [
                                'stok_id' => $stok->id,
                                'jumlah' => $jumlah,
                                'retur' => 1,
                                'hpp' => $stok->harga,
                            ];

                            $hpp_out += $jumlah * $stok->harga;
                            $stok->jumlah -= $jumlah;
                            $stok->update();
                            $jumlah = 0;
                        }
                    }
                }

                foreach ($item_cari as $j => $item) {
                    $item->stoktotal -= intval($request->jumlah[$i]);
                    $item->update();
                }
                //jurnal barang sama
                //persediaan IN
                $selisih_in = $request->harga_total - $hpp_in;
                $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
                $akun_persediaan->debet += $hpp_in;
                $akun_persediaan->update();
                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'debet' => $hpp_in
                ]);

                if($selisih_in < 0){
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_in * -1
                    ]);
                }elseif($selisih_in > 0){
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_in
                    ]);
                }

                Jurnal::create([
                    'kode_akun' => Akun::HPP,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'kredit' => $hpp_in
                ]);                    

                if($selisih_in < 0){
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_in *-1
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - LDT1',
                        'keterangan' => 'Laba - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_in * -1
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - LDT1',
                        'keterangan' =>  'Laba - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_in * -1
                    ]);
                }elseif($selisih_in > 0){
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_in
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - LDT1',
                        'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_in
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - LDT1',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_in
                    ]);
                }

                $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_ditahan->kredit -=  $selisih_in;
                $akun_laba_ditahan->update();
                

                //Persediaan OUT
                $selisih_out = $request->harga_total - $hpp_out;
                $akun_persediaan = Akun::where('kode', Akun::Persediaan)->first();
                $akun_persediaan->debet -= $hpp_out;
                $akun_persediaan->update();
                Jurnal::create([
                    'kode_akun' => Akun::HPP,
                    'referensi' => $kode_retur.' -R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'debet' => $hpp_out
                ]);

                if($selisih_out < 0){
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' -R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_out * -1
                    ]);
                }elseif($selisih_out > 0){
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur.' -R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_out
                    ]);
                }

                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur.' -R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'kredit' => $hpp_out
                ]);

                if($selisih_out < 0){
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur.' -R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_out * -1
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - LDT2',
                        'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_out * -1
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - LDT2',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_out * -1
                    ]);
                }elseif($selisih_out > 0){
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' -R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_out
                    ]);

                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - LDT2',
                        'keterangan' =>  'Laba - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_out
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - LDT2',
                        'keterangan' => 'Laba - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_out
                    ]);
                }

                $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_ditahan->kredit +=  $selisih_out;
                $akun_laba_ditahan->update();
                // $selisih_hpp = $hpp_in - $hpp_out;

                // $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                // $akun_laba_ditahan->kredit +=  $selisih_hpp;
                // $akun_laba_ditahan->update();

                // $akun_hpp = Akun::where('kode', Akun::HPP)->first();
                // $akun_hpp->debet +=  $selisih_hpp;
                // $akun_hpp->update();

                // $akun_retur = Akun::where('kode', Akun::ReturPenjualan)->first();
                // $akun_retur->debet +=  $selisih_hpp;
                // $akun_retur->update();

                // $akun_retur = Akun::where('kode', Akun::Persediaan)->first();
                // $akun_retur->debet +=  $selisih_hpp;
                // $akun_retur->update();
                
            }

            if ($request->status == 'uang'){
                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'debet' => $hpp_in
                ]);

                $akun_retur = Akun::where('kode', Akun::Persediaan)->first();
                $akun_retur->debet +=  $hpp_in;
                $akun_retur->update();

                $ppn_out = ceil($request->harga_total / 11 * 100)/100;
                $retur_penjualan = $request->harga_total - $ppn_out;

                Jurnal::create([
                    'kode_akun' => Akun::PPNKeluaran,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'debet' => $ppn_out
                ]);

                // $akun_ppn = Akun::where('kode', Akun::PPNKeluaran)->first();
                // $akun_ppn->kredit -=  $ppn_out;
                // $akun_ppn->update();

                Jurnal::create([
                    'kode_akun' => Akun::ReturPenjualan,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'debet' => $request->harga_total - $ppn_out
                ]);

                if ($request->nominal_tunai > 0) {
                    $akun_kas_tunai->debet -= $request->nominal_tunai;
                    $akun_kas_tunai->save();
                    
                    Jurnal::create([
                        'kode_akun' => Akun::KasTunai,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $request->nominal_tunai
                    ]);

                    Arus::create([
                        'nama' => Arus::ReturPenjualan,
                        'nominal' => $request->nominal_tunai
                    ]);

                    $pembayaran['tunai'] = $request->nominal_tunai;

                    if($transaksi_penjualan->status == 'eceran'){
                        if(Auth::user()->level_id == 3){
                            $cash_drawer = CashDrawer::where('user_id', Auth::id())->first();
                            $cash_drawer->nominal -= $duit_masuk;
                            $cash_drawer->update();

                            $log = new LogLaci();
                            $log->pengirim = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $request->nominal_tunai;
                            $log->save();
                        }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
                            $log = new LogLaci();
                            $log->pengirim = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $request->nominal_tunai;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->owner -= $request->nominal_tunai;
                            $laci_grosir->update();
                        }
                    }else{
                        if(Auth::user()->level_id == 4){
                            $log = new LogLaci();
                            $log->pengirim = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $request->nominal_tunai;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->grosir -= $request->nominal_tunai;
                            $laci_grosir->update();
                        }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
                            $log = new LogLaci();
                            $log->pengirim = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $request->nominal_tunai;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->owner -= $request->nominal_tunai;
                            $laci_grosir->update();
                        }
                    }
                }

                if ($request->nominal_transfer > 0 || $request->nominal_kartu > 0) {
                    $nominal_bank = 0;
                    if($request->nominal_transfer > 0){
                        $akun_kas_bank->debet -= $request->nominal_transfer;
                        $akun_kas_bank->update();

                        $bank = Bank::find($request->bank_transfer);
                        $bank->nominal -= $request->nominal_transfer;
                        $bank->update();
                        
                        $nominal_bank += $request->nominal_transfer;
                        $pembayaran['nominal_transfer'] = $request->nominal_transfer;
                        $pembayaran['no_transfer'] = $request->no_transfer;
                        $pembayaran['bank_transfer'] = $request->bank_transfer;
                    }

                    if($request->nominal_kartu > 0){
                        $pembayaran['nominal_kartu'] = $request->nominal_kartu;
                        $pembayaran['no_kartu'] = $request->no_kartu;
                        $pembayaran['bank_kartu'] = $request->bank_kartu;
                        $pembayaran['jenis_kartu'] = $request->jenis_kartu;

                        if($request->jenis_kartu == 'kredit'){
                            $akun_kartu = Akun::where('kode', Akun::KartuKredit)->first();
                            $akun_kartu->kredit += $request->nominal_kartu;
                            $akun_kartu->update();

                            Jurnal::create([
                                'kode_akun' => Akun::KartuKredit,
                                'referensi' => $kode_retur,
                                'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                                'kredit' => $request->nominal_kartu
                            ]);
                        }else{
                            $akun_kas_bank->debet -= $request->nominal_kartu;
                            $akun_kas_bank->update();

                            $bank = Bank::find($request->bank_kartu);
                            $bank->nominal -= $request->nominal_kartu;
                            $bank->update();

                            $nominal_bank += $request->nominal_kartu;
                        }
                    }

                    Jurnal::create([
                        'kode_akun' => Akun::KasBank,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $nominal_bank
                    ]);

                    Arus::create([
                        'nama' => Arus::ReturPenjualan,
                        'nominal' => $nominal_bank,
                    ]);
                }

                if ($request->nominal_cek > 0) {
                    $pembayaran['nominal_cek'] = $request->nominal_cek;
                    $pembayaran['no_cek'] = $request->no_cek;
                    $pembayaran['bank_cek'] = $request->bank_cek;
                    if($request->bank_cek == NULL){
                        $akun_kas_cek->debet -= $request->nominal_cek;
                        $akun_kas_cek->update();

                        $cek_id = Cek::find($request->cek_id);
                        $cek_id->aktif = 0;
                        $cek_id->update();

                        $pembayaran['cek_lunas'] = 1;

                        Jurnal::create([
                            'kode_akun' => Akun::KasCek,
                            'referensi' => $kode_retur,
                            'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                            'kredit' => $request->nominal_cek
                        ]);

                        Arus::create([
                            'nama' => Arus::ReturPenjualan,
                            'nominal' => $request->nominal_cek
                        ]);
                    }else{
                        $bank = Bank::find($request->bank_cek);
                        $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                        $akun->kredit += $request->nominal_cek;
                        $akun->update();

                        $cek = new Cek();
                        $cek->nomor = $request->no_cek;
                        $cek->nominal = $request->nominal_cek;
                        $cek->aktif = 0;
                        $cek->save();

                        $pembayaran['cek_lunas'] = 0;

                        Jurnal::create([
                            'kode_akun' => Akun::PembayaranProses,
                            'referensi' => $kode_retur,
                            'keterangan' => 'Cek - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                            'kredit' => $request->nominal_cek
                        ]);
                    }
                }
                
                if ($request->nominal_bg > 0) {
                    $pembayaran['nominal_bg'] = $request->nominal_bg;
                    $pembayaran['no_bg'] = $request->no_bg;
                    $pembayaran['bank_bg'] = $request->bank_bg;
                    if($request->bank_bg == NULL){
                        $akun_kas_bg->debet -= $request->nominal_bg;
                        $akun_kas_bg->update();

                        $bg_id = BG::find($request->bg_id);
                        $bg_id->aktif = 0;
                        $bg_id->update();

                        $pembayaran['bg_lunas'] = 1;

                        Jurnal::create([
                            'kode_akun' => AKun::KasBG,
                            'referensi' => $kode_retur,
                            'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                            'kredit' => $request->nominal_bg
                        ]);

                        Arus::create([
                            'nama' => Arus::ReturPenjualan,
                            'nominal' => $request->nominal_bg
                        ]);
                    }else{
                        $bank = Bank::find($request->bank_bg);
                        $akun = Akun::where('kode', Akun::PembayaranProses)->first();
                        $akun->kredit += $request->nominal_bg;
                        $akun->update();

                        $bg = new BG();
                        $bg->nomor = $request->no_bg;
                        $bg->nominal = $request->nominal_bg;
                        $bg->aktif = 0;
                        $bg->save();

                        $pembayaran['bg_lunas'] = 0;
                        
                        Jurnal::create([
                            'kode_akun' => Akun::PembayaranProses,
                            'referensi' => $kode_retur,
                            'keterangan' => 'BG - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                            'kredit' => $request->nominal_bg
                        ]);
                    }
                }

                if ($request->nominal_titipan > 0) {
                    $pelanggan->titipan += $request->nominal_titipan;
                    $pelanggan->update();
                    
                    $akun_pendapatan_dimuka->kredit += $request->nominal_titipan;
                    $akun_pendapatan_dimuka->update();

                    Jurnal::create([
                        'kode_akun' => Akun::PendapatanDimuka,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $request->nominal_titipan,
                        'user_id' => Auth::user()->id
                    ]);

                    $pembayaran['nominal_titipan'] = $request->nominal_titipan;
                }

                if ($request->nominal_piutang > 0) {
                    $piutang = PiutangDagang::find($request->piutang_id);
                    $piutang->sisa -= $request->nominal_piutang;
                    $piutang->update();

                    $bayar_piutang = new BayarPiutangDagang();
                    $bayar_piutang->kode_transaksi = $kode_retur;
                    $bayar_piutang->piutang_dagang_id = $request->piutang_id;
                    $bayar_piutang->jumlah_bayar = $request->nominal_piutang;
                    $bayar_piutang->user_id = Auth::id();
                    $bayar_piutang->save();
                    
                    $akun_piutang_dagang->debet -= $request->nominal_piutang;
                    $akun_piutang_dagang->update();

                    Jurnal::create([
                        'kode_akun' => Akun::PiutangDagang,
                        'referensi' => $kode_retur,
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $request->nominal_piutang
                    ]);

                    $pembayaran['nominal_piutang'] = $request->nominal_piutang;
                }

                Jurnal::create([
                    'kode_akun' => Akun::HPP,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                    'kredit' => $hpp_in
                ]);

                $akun_hpp = Akun::where('kode', Akun::HPP)->first();
                $akun_hpp->debet -=  $hpp_in;
                $akun_hpp->update();

                // jurnal labarugi PPN Keluaran -> GAJADI
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                //     'debet' => $ppn_out
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::PPNKeluaran,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_out
                // ]);
                // //jurnal labarugi debet
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur. 'PPN -'. 'L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // //jurnal labarugi kredit
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. 'PPN -'. 'L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                //     'kredit' => $ppn_out
                // ]);
                // //jurnal labatahan debet
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaTahan,
                //     'referensi' => $kode_retur. 'PPN -'. 'LDT',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // //jurnal labatahan kredit
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur. 'PPN -'. 'LDT',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_out
                // ]);
                // $akun_laba = Akun::where('kode', Akun::LabaTahan)->first();
                // $akun_laba->kredit -=  $ppn_out;
                // $akun_laba->update();

                $akun_ppn_keluar = Akun::where('kode', Akun::PPNKeluaran)->first();
                $akun_ppn_keluar->kredit -= $ppn_out;
                $akun_ppn_keluar->update();

                $selisih = $retur_penjualan - $hpp_in;
                if($selisih < 0){
                    //jurnal labarugi debet
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur. 'L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                        'debet' => ($selisih * -1)
                    ]);
                    //jurnal labarugi kredit
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur. 'L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => ($selisih * -1)
                    ]);
                    //jurnal labatahan debet
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur. 'LDT',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => ($selisih * -1)
                    ]);

                    //jurnal labatahan kredit                    
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur. 'LDT',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => ($selisih * -1)
                    ]);
                }elseif ($selisih > 0) {
                    //jurnal labarugi debet
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur. 'L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih
                    ]);
                    //jurnal labarugi kredit
                    Jurnal::create([
                        'kode_akun' => Akun::ReturPenjualan,
                        'referensi' => $kode_retur. 'L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                        'kredit' => $selisih
                    ]);
                    //jurnal labatahan debet
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur. 'LDT',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih
                    ]);
                    //jurnal labatahan kredit
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur. 'LDT',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih
                    ]);
                }

                $akun_laba = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba->kredit -=  $selisih;
                $akun_laba->update();

                // $akun_laba = Akun::where('kode', Akun::ReturPenjualan)->first();
                // $akun_laba->debet -=  $selisih;
                // $akun_laba->update();
            }

            if ($request->status == 'lain'){
                //barang masuk relasi
                foreach ($request->item_kode_in as $a => $item){
                    // return $request->konversi[$a];
                    $jumlah_keluar = $request->jumlah_in[$a] * $request->konversi_in[$a];
                    $relasi_retur[$w] = [
                        'item_kode' => $item,
                        // 'jumlah' => $request->jumlah_in[$a],
                        'jumlah' => $jumlah_keluar,
                        'harga' => $request->harga_in[$a],
                        'subtotal' => $request->subtotal_in[$a],
                        'retur' => 1,
                    ];
                    $w +=1;
                    // return $relasi_retur;
                    // tambahan ngitung stoktotal
                    $item_x = Item::where('kode', $item)->first();
                    $items_x = Item::where('kode_barang', $item_x->kode_barang)->get();
                    // return $request->jumlah_in;
                    foreach ($items_x as $j => $item) {
                        $item->stoktotal -= $jumlah_keluar;
                        $item->update();
                    }

                    $jumlah = $jumlah_keluar;
                    // return $item;
                    $stoks = Stok::where('item_kode', $item->kode)->where('jumlah', '>', 0)->where('rusak', 0)->where('retur', 0)->get();
                    // return $stoks;
                    foreach ($stoks as $j => $stok) {
                        // return $jumlah;
                        if ($jumlah <= 0) {
                            break;
                        } else if ($stok->kadaluarsa == '0000-00-00' || $stok->kadaluarsa > $today) {
                            if ($stok->jumlah <= $jumlah) {
                                // masih loop
                                $stok_retur[$y] = [
                                    'stok_id' => $stok->id,
                                    'jumlah' => $stok->jumlah,
                                    'retur' => 1,
                                    'hpp' => $stok->harga,
                                ];

                                $hpp_out += $stok->jumlah * $stok->harga;
                                $jumlah -= $stok->jumlah;
                                $stok->jumlah = 0;
                                $stok->update();
                            } else {
                                // loop berhenti
                                $stok_retur[$y] = [
                                    'stok_id' => $stok->id,
                                    'jumlah' => $jumlah,
                                    'retur' => 1,
                                    'hpp' => $stok->harga,
                                ];

                                $hpp_out += $jumlah * $stok->harga;
                                $stok->jumlah -= $jumlah;
                                $stok->update();
                                $jumlah = 0;
                            }
                        }
                    }
                }

                // return $hpp_out;

                $ppn_in = ceil($request->harga_total / 11 * 100)/100;
                $laba_rugi_in = $request->harga_total - $ppn_in;
                // $selisih_in = $laba_rugi_in - $hpp_in - $ppn_in;
                $selisih_in = $laba_rugi_in - $hpp_in;
                // $retur_penjualan = $request->harga_total - $hpp_in;
                //jurnal barang masuk
                //jurnal persediaan masuk
                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'debet' => $hpp_in
                ]);
                $akun_persediaan_in = Akun::where('kode', Akun::Persediaan)->first();
                $akun_persediaan_in->debet +=  $hpp_in;
                $akun_persediaan_in->update();
                //jurnal laba rugi masuk
                Jurnal::create([
                    'kode_akun' => Akun::LabaRugi,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',  
                    'debet' => $laba_rugi_in
                ]);
                //jurnal ppn out masuk
                Jurnal::create([
                    'kode_akun' => Akun::PPNKeluaran,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'debet' => $ppn_in
                ]);
                // $akun_ppn_in = Akun::where('kode', Akun::PPNKeluaran)->first();
                // $akun_ppn_in->kredit -=  $ppn_in;
                // $akun_ppn_in->update();
                //jurnal hpp masuk
                Jurnal::create([
                    'kode_akun' => Akun::HPP,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'kredit' => $hpp_in
                ]);
                $akun_hpp_in = Akun::where('kode', Akun::HPP)->first();
                $akun_hpp_in->debet -=  $hpp_in;
                $akun_hpp_in->update();

                Jurnal::create([
                    'kode_akun' => Akun::ReturPenjualan,
                    'referensi' => $kode_retur,
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'kredit' => $request->harga_total
                ]);  

                //ppn puteran -> GAJADI
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                //     'debet' => $ppn_in
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::PPNKeluaran,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_in
                // ]);

                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur.' PPN - L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_in
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur.' PPN - L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - TPNRetur',
                //     'kredit' => $ppn_in
                // ]);

                // Jurnal::create([
                //     'kode_akun' => Akun::LabaTahan,
                //     'referensi' => $kode_retur.' PPN - LDT',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_in
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur.' PPN - LDT',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_in
                // ]);

                // $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                // $akun_laba_ditahan->kredit -=  $ppn_in;
                // $akun_laba_ditahan->update(); 

                $akun_ppn_masuk = Akun::where('kode', Akun::PPNKeluaran)->first();
                $akun_ppn_masuk->kredit -= $ppn_in;
                $akun_ppn_masuk->update();

                //labarugi -> laba ditahan
                // if($selisih_in > 0){
                //     Jurnal::create([
                //         'kode_akun' => Akun::LabaTahan,
                //         'referensi' => $kode_retur.' - LDT',
                //         'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //         'debet' => $selisih_in
                //     ]);

                //     Jurnal::create([
                //         'kode_akun' => Akun::LabaRugi,
                //         'referensi' => $kode_retur.' - LDT',
                //         'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //         'kredit' => $selisih_in
                //     ]);
                // }elseif($selisih_in < 0){
                //     Jurnal::create([
                //         'kode_akun' => Akun::LabaRugi,
                //         'referensi' => $kode_retur.' - LDT',
                //         'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //         'debet' => $selisih_in
                //     ]);
                //     Jurnal::create([
                //         'kode_akun' => Akun::LabaTahan,
                //         'referensi' => $kode_retur.' - LDT',
                //         'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //         'kredit' => $selisih_in
                //     ]);
                // }

                // $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                // $akun_laba_ditahan->kredit -=  $selisih_in;
                // $akun_laba_ditahan->update();                

                if($request->nominal_tunai_in > 0){
                    $jumlah_bayar = $request->nominal_tunai_in + $request->nominal_transfer_in + $request->nominal_kartu_in + $request->nominal_cek_in + $request->nominal_bg_in + $request->nominal_titipan_in;

                    $kembali_temp = $jumlah_bayar - $request->jumlah_bayar + $request->harga_total;
                    $duit_masuk = $request->nominal_tunai_in;

                    if ($kembali_temp > 0) $duit_masuk -= $kembali_temp;

                    // return $duit_masuk;
                    $akun_kas_tunai->debet += $duit_masuk;
                    $akun_kas_tunai->update();

                    Jurnal::create([
                        'kode_akun' => Akun::KasTunai,
                        'referensi' => $kode_retur.' - R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $duit_masuk
                    ]);

                    Arus::create([
                        'nama' => Arus::JBD,
                        'nominal' => $duit_masuk
                    ]);

                    $pembayaran['tunai_in'] = $request->nominal_tunai_in;
                    if($transaksi_penjualan->status == 'eceran'){
                        if(Auth::user()->level_id == 3){
                            $cash_drawer = CashDrawer::where('user_id', Auth::id())->first();
                            $cash_drawer->nominal += $duit_masuk;
                            $cash_drawer->update();

                            $log = new LogLaci();
                            $log->penerima = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $duit_masuk;
                            $log->save();
                        }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
                            $log = new LogLaci();
                            $log->penerima = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $duit_masuk;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->owner += $duit_masuk;
                            $laci_grosir->update();
                        }
                    }else{
                        if(Auth::user()->level_id == 4){
                            $log = new LogLaci();
                            $log->penerima = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $duit_masuk;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->grosir += $duit_masuk;
                            $laci_grosir->update();
                        }elseif(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
                            $log = new LogLaci();
                            $log->penerima = Auth::user()->id;
                            $log->approve = 1;
                            $log->status = 15;
                            $log->nominal = $duit_masuk;
                            $log->save();

                            $laci_grosir = Laci::find(1);
                            $laci_grosir->owner += $duit_masuk;
                            $laci_grosir->update();
                        }
                    }
                }

                if($request->nominal_transfer_in > 0 || $request->nominal_kartu_in > 0){
                    $nominal_bank = 0;
                    if($request->nominal_transfer_in > 0){
                        $bank = Bank::find($request->bank_transfer_in);
                        $bank->nominal += $request->nominal_transfer_in;
                        $bank->update();

                        $akun_kas_bank->debet += $request->nominal_transfer_in;
                        $akun_kas_bank->update();
                        
                        $nominal_bank += $request->nominal_transfer_in;

                        $pembayaran['nominal_transfer_in'] = $request->nominal_transfer_in;
                        $pembayaran['no_transfer_in'] = $request->no_transfer_in;
                        $pembayaran['bank_transfer_in'] = $request->bank_transfer_in;
                    }
                    if($request->nominal_kartu_in > 0){
                        $bank = Bank::find($request->bank_kartu_in);
                        $bank->nominal += $request->nominal_kartu_in;
                        $bank->update();
                        
                        $nominal_bank += $request->nominal_kartu_in;

                        $akun_kas_bank->debet += $request->nominal_kartu_in;
                        $akun_kas_bank->update();
                        
                        $pembayaran['nominal_kartu_in'] = $request->nominal_kartu_in;
                        $pembayaran['no_kartu_in'] = $request->no_kartu_in;
                        $pembayaran['bank_kartu_in'] = $request->bank_kartu_in;
                        $pembayaran['jenis_kartu_in'] = $request->jenis_kartu_in;
                    }

                    Jurnal::create([
                        'kode_akun' => Akun::KasBank,
                        'referensi' => $kode_retur.' - R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $nominal_bank
                    ]);

                    Arus::create([
                        'nama' => Arus::JBD,
                        'nominal' => $nominal_bank,
                    ]);
                }

                if ($request->nominal_cek_in > 0) {
                    $pembayaran['nominal_cek_in'] = $request->nominal_cek_in;
                    $pembayaran['no_cek_in'] = $request->no_cek_in;

                    $akun_kas_cek->debet += $request->nominal_cek_in;
                    $akun_kas_cek->update();

                    $cek = new Cek();
                    $cek->nomor = $request->no_cek_in;
                    $cek->nominal = $request->nominal_cek_in;
                    $cek->aktif = 1;
                    $cek->save();

                    Jurnal::create([
                        'kode_akun' => Akun::KasCek,
                        'referensi' => $kode_retur.' - R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                        'debet' => $request->nominal_cek_in
                    ]);

                    Arus::create([
                        'nama' => Arus::JBD,
                        'nominal' => $request->nominal_cek_in,
                    ]);
                }

                if ($request->nominal_bg_in > 0) {
                    $pembayaran['nominal_bg_in'] = $request->nominal_bg_in;
                    $pembayaran['no_bg_in'] = $request->no_bg_in;

                    $akun_kas_bg->debet += $request->nominal_bg_in;
                    $akun_kas_bg->update();

                    $bg = new BG();
                    $bg->nomor = $request->no_bg_in;
                    $bg->nominal = $request->nominal_bg_in;
                    $bg->aktif = 1;
                    $bg->save();

                    Jurnal::create([
                        'kode_akun' => Akun::KasBG,
                        'referensi' => $kode_retur.' - R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                        'debet' => $request->nominal_bg_in
                    ]);

                    Arus::create([
                        'nama' => Arus::JBD,
                        'nominal' => $request->nominal_bg_in,
                    ]);
                }

                if ($request->nominal_titipan_in > 0) {
                    $pembayaran['nominal_titipan_in'] = $request->nominal_titipan_in;

                    $pelanggan->titipan -= $request->nominal_titipan_in;
                    $pelanggan->update();

                    Jurnal::create([
                        'kode_akun' => Akun::PendapatanDimuka,
                        'referensi' => $kode_retur.' - R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                        'debet' => $request->nominal_titipan_in
                    ]);
                }
                // return $relasi_retur;
                // return $hpp_out;
                $ppn_out = ceil($request->jumlah_bayar / 11 * 100)/100;
                $penjualan = $request->jumlah_bayar - $ppn_out;
                $selisih_penjualan = $penjualan - $hpp_out;
                $selisih_out = $penjualan - $hpp_out + $ppn_out;
                // $retur_penjualan_out = $request->jumlah_bayar - $request->jumlah_bayar_in  - $hpp_out;

                Jurnal::create([
                    'kode_akun' => Akun::HPP,
                    'referensi' => $kode_retur.' - R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'debet' => $hpp_out
                ]);
                $akun_hpp_out = Akun::where('kode', Akun::HPP)->first();
                $akun_hpp_out->debet +=  $hpp_out;
                $akun_hpp_out->update();

                Jurnal::create([
                    'kode_akun' => Akun::ReturPenjualan,
                    'referensi' => $kode_retur.' - R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'debet' => $request->harga_total
                ]);

                Jurnal::create([
                    'kode_akun' => Akun::Persediaan,
                    'referensi' => $kode_retur.' - R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'kredit' => $hpp_out
                ]);
                $akun_persediaan_out = Akun::where('kode', Akun::Persediaan)->first();
                $akun_persediaan_out->debet -=  $hpp_out;
                $akun_persediaan_out->update();

                Jurnal::create([
                    'kode_akun' => Akun::PPNKeluaran,
                    'referensi' => $kode_retur.' - R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,  
                    'kredit' => $ppn_out
                ]);
                // $akun_ppn_in = Akun::where('kode', Akun::PPNKeluaran)->first();
                // $akun_ppn_in->kredit +=  $ppn_out;
                // $akun_ppn_in->update();

                Jurnal::create([
                    'kode_akun' => Akun::Penjualan,
                    'referensi' => $kode_retur.' - R',
                    'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi.' - Transaksi Penjualan',  
                    'kredit' => $penjualan
                ]);

                //ppn puteran
                // Jurnal::create([
                //     'kode_akun' => Akun::PPNKeluaran,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. 'PPN',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_out
                // ]);

                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. ' PPN - L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur. ' PPN - L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_out
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur.' PPN - LDT',
                //     'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // //labarugi -> laba ditahan
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaTahan,
                //     'referensi' => $kode_retur.' PPN - LDT',
                //     'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $ppn_out
                // ]);
                // $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                // $akun_laba_ditahan->kredit +=  $ppn_out;
                // $akun_laba_ditahan->update();

                $akun_ppn_keluar = Akun::where('kode', Akun::PPNKeluaran)->first();
                $akun_ppn_keluar->kredit += $ppn_out;
                $akun_ppn_keluar->update();

                $selisih_akhir = $selisih_penjualan - $selisih_in;
                if($selisih_akhir > 0){
                    // labarugi -> laba ditahan
                    Jurnal::create([
                        'kode_akun' => Akun::Penjualan,
                        'referensi' => $kode_retur. ' L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_akhir
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' L/R',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_akhir
                    ]);

                    //labarugi -> laba ditahan
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - RLDT',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_akhir
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - RLDT',
                        'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_akhir
                    ]);
                }elseif($selisih_akhir < 0){
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' L/R',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_akhir * -1
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::Penjualan,
                        'referensi' => $kode_retur. ' L/R',
                        'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_akhir * -1
                    ]);

                    //labarugi -> laba ditahan
                    Jurnal::create([
                        'kode_akun' => Akun::LabaTahan,
                        'referensi' => $kode_retur.' - RLDT',
                        'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'debet' => $selisih_akhir * -1
                    ]);
                    Jurnal::create([
                        'kode_akun' => Akun::LabaRugi,
                        'referensi' => $kode_retur.' - RLDT',
                        'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                        'kredit' => $selisih_akhir * -1
                    ]);

                }

                //labarugi -> laba ditahan
                // Jurnal::create([
                //     'kode_akun' => Akun::PendapatanLain,
                //     'referensi' => $kode_retur. ' L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $ppn_out
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::Penjualan,
                //     'referensi' => $kode_retur. ' L/R',
                //     'keterangan' => 'Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $selisih_penjualan
                // ]);
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur.' L/R',
                //     'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $selisih_out
                // ]);

                // //labarugi -> laba ditahan
                // Jurnal::create([
                //     'kode_akun' => Akun::LabaRugi,
                //     'referensi' => $kode_retur.' - RLDT',
                //     'keterangan' => 'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'debet' => $selisih_out
                // ]);

                // Jurnal::create([
                //     'kode_akun' => Akun::LabaTahan,
                //     'referensi' => $kode_retur.' - RLDT',
                //     'keterangan' =>  'Rugi - Retur Penjualan '.$transaksi_penjualan->kode_transaksi,
                //     'kredit' => $selisih_out
                // ]);
                $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
                $akun_laba_ditahan->kredit +=  $selisih_akhir;
                $akun_laba_ditahan->update();
            }
        }

        $retur_penjualan = new ReturPenjualan();
        $retur_penjualan->kode_retur = $kode_retur;
        $retur_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
        $retur_penjualan->status = $request->status;
        $retur_penjualan->harga_total = $request->harga_total;
        $retur_penjualan->total_uang_masuk = $request->jumlah_bayar_in;
        $retur_penjualan->total_uang_keluar = $request->jumlah_bayar;
        $retur_penjualan->pelanggan_id = $transaksi_penjualan->pelanggan_id;
        $retur_penjualan->user_id = Auth::id();

        if($retur_penjualan->save()){
            $retur_penjualan_id = ReturPenjualan::latest()->first()->id;
            for($i = 0; $i < sizeof($relasi_retur); $i++){
                $relasi_retur_draft = new RelasiReturPenjualan();
                $relasi_retur_draft->retur_penjualan_id = $retur_penjualan_id;
                $relasi_retur_draft->item_kode = $relasi_retur[$i]['item_kode'];
                $relasi_retur_draft->jumlah = $relasi_retur[$i]['jumlah'];
                $relasi_retur_draft->harga = $relasi_retur[$i]['harga'];
                $relasi_retur_draft->subtotal = $relasi_retur[$i]['subtotal'];
                $relasi_retur_draft->retur = $relasi_retur[$i]['retur'];
                $relasi_retur_draft->save();
            }

            for($i = 0; $i < sizeof($stok_retur); $i++){
                $stok_retur_draft = new RelasiStokReturPenjualan();
                $stok_retur_draft->stok_id = $stok_retur[$i]['stok_id'];
                $stok_retur_draft->retur_penjualan_id = $retur_penjualan_id;
                $stok_retur_draft->jumlah = $stok_retur[$i]['jumlah'];
                $stok_retur_draft->retur = $stok_retur[$i]['retur'];
                $stok_retur_draft->hpp = $stok_retur[$i]['hpp'];
                $stok_retur_draft->save();
            }

            if($request->status == 'uang' || $request->status == 'lain' ){
                $pembayaran_retur = new PembayaranReturPenjualan();
                $pembayaran_retur->retur_penjualan_id = $retur_penjualan_id;
                //pembayaran uang keluar
                if($request->jumlah_bayar > 0 && $request->jumlah_bayar != NULL){
                    $pembayaran_retur->nominal_tunai = $pembayaran['tunai'];
                    $pembayaran_retur->nominal_transfer = $pembayaran['nominal_transfer'];
                    $pembayaran_retur->no_transfer = $pembayaran['no_transfer'];
                    $pembayaran_retur->bank_transfer = $pembayaran['bank_transfer'];
                    $pembayaran_retur->nominal_kartu = $pembayaran['nominal_kartu'];
                    $pembayaran_retur->no_kartu = $pembayaran['no_kartu'];
                    $pembayaran_retur->bank_kartu = $pembayaran['bank_kartu'];
                    $pembayaran_retur->jenis_kartu = $pembayaran['jenis_kartu'];
                    $pembayaran_retur->nominal_cek = $pembayaran['nominal_cek'];
                    $pembayaran_retur->no_cek = $pembayaran['no_cek'];
                    $pembayaran_retur->bank_cek = $pembayaran['bank_cek'];
                    $pembayaran_retur->cek_lunas = $pembayaran['cek_lunas'];
                    $pembayaran_retur->nominal_bg = $pembayaran['nominal_bg'];
                    $pembayaran_retur->no_bg = $pembayaran['no_bg'];
                    $pembayaran_retur->bank_bg = $pembayaran['bank_bg'];
                    $pembayaran_retur->bg_lunas = $pembayaran['bg_lunas'];
                    $pembayaran_retur->nominal_titipan = $pembayaran['nominal_titipan'];
                    $pembayaran_retur->nominal_piutang = $pembayaran['nominal_piutang'];
                }
                //pembayaran uang keluar
                if($request->jumlah_bayar_in > 0 && $request->jumlah_bayar_in != NULL){
                    $pembayaran_retur->nominal_tunai_in = $pembayaran['tunai_in'];
                    $pembayaran_retur->nominal_transfer_in = $pembayaran['nominal_transfer_in'];
                    $pembayaran_retur->no_transfer_in = $pembayaran['no_transfer_in'];
                    $pembayaran_retur->bank_transfer_in = $pembayaran['bank_transfer_in'];
                    $pembayaran_retur->nominal_kartu_in = $pembayaran['nominal_kartu_in'];
                    $pembayaran_retur->no_kartu_in = $pembayaran['no_kartu_in'];
                    $pembayaran_retur->bank_kartu_in = $pembayaran['bank_kartu_in'];
                    $pembayaran_retur->jenis_kartu_in = $pembayaran['jenis_kartu_in'];
                    $pembayaran_retur->nominal_cek_in = $pembayaran['nominal_cek_in'];
                    $pembayaran_retur->no_cek_in = $pembayaran['no_cek_in'];
                    $pembayaran_retur->nominal_bg_in = $pembayaran['nominal_bg_in'];
                    $pembayaran_retur->no_bg_in = $pembayaran['no_bg_in'];
                    $pembayaran_retur->nominal_titipan_in = $pembayaran['nominal_titipan_in'];
                }
                $pembayaran_retur->save();
            }
            
            return redirect('/cetak/'.$transaksi_penjualan->id.'/retur/'.$retur_penjualan_id);
        }
    }

    public function userJson($id)
    {
        $cash_drawer = CashDrawer::where('user_id', $id)->first();
        $bukaan = SetoranBuka::where('user_id', $id)->first();
        $limit = MoneyLimit::find(1);

        return response()->json(compact('cash_drawer', 'bukaan', 'limit'));
    }

    /*public function countJson($transaksi_penjualan_id, $item_kode, $count)
    {
        $harga = RelasiTransaksiPembelian::where('item_id', $kode)->where('transaksi_pembelian_id', $transaksi)->first();
        $harga_satuan = $harga->harga;

        return response()->json(compact('jumlah', 'harga_satuan'));
    }*/

    public function tanggal($id)
    {
        $transaksi = TransaksiPenjualan::find($id);
        $tanggal = new DateTime($transaksi->jatuh_tempo);

        return view('transaksi_grosir.tanggal', compact('transaksi', 'tanggal'));
    }

    public function tanggalStore(Request $request)
    {
        $transaksi = TransaksiPenjualan::find($request->id);
        $transaksi->jatuh_tempo = $request->tanggal;
        $transaksi->update();

        return redirect('/transaksi-grosir/tanggal/'.$request->id)->with('sukses', 'ubah');
    }

    public function ReturAktif($id, Request $request)
    {
        // return $request->all();
        if ($request->ids != null) {
            $transaksi_penjualan = TransaksiPenjualan::find($request->ids);
            $transaksi_penjualan->can_retur = 1;
            $transaksi_penjualan->update();
            return redirect('/transaksi-grosir/'.$request->ids)->with('sukses', 'aktif');
        } else {
            $transaksi_penjualan = TransaksiPenjualan::find($id);
            $transaksi_penjualan->can_retur = 1;
            $transaksi_penjualan->update();
            return redirect('/transaksi-grosir')->with('sukses', 'aktif');
        }

    }

    public function jatuhTempoStore(Request $request, $id)
    {
        // return $request->all();
        $transaksi = TransaksiPenjualan::find($id);
        $transaksi->jatuh_tempo = $request->jatuh_tempo_baru_;
        $transaksi->update();
        // return $transaksi;

        return redirect('/transaksi-grosir/'.$transaksi->id)->with('sukses', 'ubah_jatuh_tempo');
    }

    public function bayarPO(Request $request, $id)
    {
        // return $request->all();
        $transaksi_penjualan = TransaksiPenjualan::find($id);
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        if ($request->nego_total_view > 0) $transaksi_penjualan->nego_total = $request->nego_total_view;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = $request->grosir == 'true' ? 'po_grosir' : 'po_eceran';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        // $transaksi_penjualan->user_id = Auth::id();

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->jumlah_bayar != null) {
            // $harga_total = $request->nego_total == null ? (float) $request->harga_total : (float) $request->nego_total;
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            // if ($jumlah_bayar > $harga_total) {
            //     $nominal_tunai = $harga_total - $nominal_transfer - $nominal_cek - $nominal_bg - $nominal_kartu - $nominal_titipan;
            // }

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();
        foreach ($relasi_transaksi_penjualan as $i => $rtj) {
            RelasiBonusPenjualan::where('relasi_transaksi_penjualan_id', $rtj->id)->delete();
            $rtj->delete();
        }

        if ($transaksi_penjualan->update()) {
            $nego_total = 0;
            foreach ($request->item_kode as $i => $kode) {
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = $request->is_grosir == 'true' ? (float) $request->harga_grosir[$i] : (float) $request->harga_eceran[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                if ($request->nego[$i] > 0) $nego_total += $request->nego[$i];
                else $nego_total += $request->subtotal[$i];

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;

                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            if ($request->nego_total_view <= 0 || ($request->nego_total_view > 0 && $request->nego_total_view > $request->harga_akhir)) {
                $transaksi_penjualan->nego_total = $nego_total;
                $transaksi_penjualan->update();
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return self::bayar($id);
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'ubah');
        }
    }

    public function bayarPONew(Request $request)
    {
        // return $request->all();
        $transaksi_penjualan = new TransaksiPenjualan();
        $transaksi_penjualan->kode_transaksi = $this->kodeTransaksiBaru();
        $transaksi_penjualan->harga_total = $request->harga_total;
        $transaksi_penjualan->check_nego_total = $request->check_nego_total;
        $transaksi_penjualan->nego_total = $request->harga_total;
        if ($request->harga_akhir < $request->harga_total) $transaksi_penjualan->nego_total = $request->harga_akhir;
        $transaksi_penjualan->potongan_penjualan = $request->potongan_penjualan;
        $transaksi_penjualan->status = 'po_eceran';
        $transaksi_penjualan->pelanggan_id = $request->pelanggan;
        $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
        // $transaksi_penjualan->user_id = Auth::id();

        if ($request->jumlah_bayar != null) {
            $jumlah_bayar = (float) $request->jumlah_bayar;
            $nominal_tunai = (float) $request->nominal_tunai;
            $nominal_transfer = (float) $request->nominal_transfer;
            $nominal_kartu = (float) $request->nominal_kartu;
            $nominal_cek = (float) $request->nominal_cek;
            $nominal_bg = (float) $request->nominal_bg;
            $nominal_titipan = (float) $request->nominal_titipan;

            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
            $transaksi_penjualan->jumlah_bayar = $jumlah_bayar;
            $transaksi_penjualan->nominal_tunai = $nominal_tunai;
            $transaksi_penjualan->no_transfer = $request->no_transfer;
            $transaksi_penjualan->bank_transfer = $request->bank_transfer;
            $transaksi_penjualan->nominal_transfer = $nominal_transfer;
            $transaksi_penjualan->no_kartu = $request->no_kartu;
            $transaksi_penjualan->bank_kartu = $request->bank_kartu;
            $transaksi_penjualan->jenis_kartu = $request->jenis_kartu;
            $transaksi_penjualan->nominal_kartu = $nominal_kartu;
            $transaksi_penjualan->no_cek = $request->no_cek;
            $transaksi_penjualan->nominal_cek = $nominal_cek;
            $transaksi_penjualan->no_bg = $request->no_bg;
            $transaksi_penjualan->nominal_bg = $nominal_bg;
            $transaksi_penjualan->jatuh_tempo = $request->jatuh_tempo;
            $transaksi_penjualan->nominal_titipan = $nominal_titipan;
        }

        $user = Auth::user();
        $transaksi_penjualan->user_id = $user->id;
        // if ($user->level_id == 3) {
        //     $transaksi_penjualan->user_id = $user->id;
        // } else {
        //     $transaksi_penjualan->grosir_id = $user->id;
        // }

        if ($request->pengirim == '' && $request->pengirim_lain == '') {
            $transaksi_penjualan->pengirim = null;
        } else {
            if ($request->pengirim != '') $transaksi_penjualan->pengirim = User::find($request->pengirim)->nama;
            else $transaksi_penjualan->pengirim = $request->pengirim_lain;
        }

        if ($request->alamat_lain == '') {
            $transaksi_penjualan->alamat = null;
        } else {
            $transaksi_penjualan->alamat = $request->alamat_lain;
        }

        if ($request->ongkos_kirim != '') {
            $transaksi_penjualan->ongkos_kirim = $request->ongkos_kirim;
        } else {
            $transaksi_penjualan->ongkos_kirim = 0;
        }

        if ($transaksi_penjualan->save()) {
            foreach ($request->item_kode as $i => $kode) {
                // untuk mencari konversi
                // $relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
                // $jumlah = $request->jumlah[$i] * $relasi_satuan->konversi;
                $jumlah = $request->jumlah1[$i] * $request->konversi1[$i] + $request->jumlah2[$i] * $request->konversi2[$i];

                // buat relasi baru
                $relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
                $relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_penjualan->id;
                $relasi_transaksi_penjualan->item_kode = $kode;
                $relasi_transaksi_penjualan->jumlah = $jumlah;
                $relasi_transaksi_penjualan->harga = (float) $request->harga_eceran[$i];
                $relasi_transaksi_penjualan->subtotal = (float) $request->subtotal[$i];
                $relasi_transaksi_penjualan->nego = (float) $request->nego[$i];
                $relasi_transaksi_penjualan->save();

                // cek apakan item dapat bonus atau tidak, lalu cari bonusnya dan hitung jumlahnya
                $item = Item::where('kode', $kode)->first();
                $index_bonus = 0;
                $bonuses = array();
                $relasi_bonus = RelasiBonus::with('bonus')->where('item_kode', $kode)->orderBy('syarat', 'asc')->get();
                foreach ($relasi_bonus as $j => $relasi) {
                    $syarat = $relasi->syarat;

                    if ($jumlah >= $syarat) {
                        $jumlah_bonus = intval($jumlah / $syarat);
                        if ($relasi->bonus->stoktotal > 0) {
                            if ($jumlah_bonus >= $relasi->bonus->stoktotal) $jumlah_bonus = $relasi->bonus->stoktotal;
                                 $bonuses[$index_bonus] = [
                                    'bonus' => $relasi->bonus,
                                    'syarat' => $syarat,
                                    'jumlah' => $jumlah_bonus
                                ];
                                $index_bonus++;
                        }
                    /*if ($jumlah >= $syarat && intval($jumlah / $syarat) <= $relasi->bonus->stoktotal) {
                        if ($index_bonus == 0) {
                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        } else {
                            if ($syarat != $bonuses[$index_bonus - 1]['syarat']) {
                                $bonuses = array();
                                $index_bonus = 0;
                            }

                            $bonuses[$index_bonus] = [
                                'bonus' => $relasi->bonus,
                                'syarat' => $syarat,
                                'jumlah' => intval($jumlah / $syarat)
                            ];
                            $index_bonus++;
                        }*/
                    }
                }

                // buat relasi bonus penjualan
                foreach ($bonuses as $j => $bonus) {
                    $relasi_bonus_penjualan = new RelasiBonusPenjualan();
                    $relasi_bonus_penjualan->bonus_id = $bonus['bonus']->id;
                    $relasi_bonus_penjualan->relasi_transaksi_penjualan_id = $relasi_transaksi_penjualan->id;
                    $relasi_bonus_penjualan->jumlah = $bonus['jumlah'];
                    $relasi_bonus_penjualan->save();
                }
            }

            // tambah bonus per transaksi
            RelasiBonusRulesPenjualan::where('transaksi_penjualan_id', $transaksi_penjualan->id)->delete();
            if ($request->bonus_id != null) {
                foreach ($request->bonus_id as $i => $bi) {
                    $bj = $request->bonus_jumlah[$i];
                    RelasiBonusRulesPenjualan::create([
                        'transaksi_penjualan_id' => $transaksi_penjualan->id,
                        'bonus_id' => $bi,
                        'jumlah' => $bj
                    ]);
                }
            }

            return self::bayar($transaksi_penjualan->id);
        } else {
            return redirect('po-penjualan/'.$transaksi_penjualan->id)->with('gagal', 'tambah');
        }
    }
}
