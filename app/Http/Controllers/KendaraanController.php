<?php

namespace App\Http\Controllers;

use DB;
use Datetime;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Jurnal;
use App\Kendaraan;
use Illuminate\Http\Request;

class KendaraanController extends Controller
{
    /* public function index(){
        $kendaraans = Kendaraan::where('rusak', 1)->get();
        $rusak_kendaraans = Kendaraan::where('rusak', 0)->get();
        $banks = Bank::all();
        // return $peralatans;
        return view('kendaraan.index', compact('rusak_kendaraans', 'kendaraans', 'banks'));
    } */

    public function index() {
        $banks = Bank::all();
        return view('kendaraan.index', compact('banks'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'kendaraans.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $kendaraans = Kendaraan
            ::select(
                'kendaraans.id',
                'kendaraans.nopol',
                'kendaraans.atas_nama',
                'kendaraans.umur',
                'kendaraans.tahun_perakitan',
                'kendaraans.harga',
                'kendaraans.nominal',
                'kendaraans.residu',
                'kendaraans.created_at',
                'kendaraans.keterangan'
            )
            ->where('kendaraans.rusak', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('kendaraans.nopol', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.tahun_perakitan', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('kendaraans.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Kendaraan
            ::select(
                'kendaraans.id'
            )
            ->where('kendaraans.rusak', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('kendaraans.nopol', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.tahun_perakitan', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('kendaraans.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($kendaraans as $i => $kendaraan) {

            $kendaraan->harga = Util::duit($kendaraan->harga);
            $kendaraan->nominal = Util::duit($kendaraan->nominal);
            $kendaraan->residu = Util::duit($kendaraan->residu);

            if ($kendaraan->keterangan == null) {
                $kendaraan->keterangan = ' - ';
            }

            $buttons['detail'] = ['url' => url('kendaraan/show/'.$kendaraan->id)];

            $buttons['hapus'] = ['url' => ''];
            $buttons['jual'] = ['url' => ''];

            // return $buttons;
            $kendaraan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $kendaraans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'kendaraans.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $kendaraans = Kendaraan
            ::select(
                'kendaraans.id',
                'kendaraans.nopol',
                'kendaraans.atas_nama',
                'kendaraans.umur',
                'kendaraans.tahun_perakitan',
                'kendaraans.harga',
                'kendaraans.nominal',
                'kendaraans.residu',
                'kendaraans.created_at',
                'kendaraans.keterangan'
            )
            ->where('kendaraans.rusak', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('kendaraans.nopol', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.tahun_perakitan', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('kendaraans.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Kendaraan
            ::select(
                'kendaraans.id'
            )
            ->where('kendaraans.rusak', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('kendaraans.nopol', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.atas_nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.umur', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.tahun_perakitan', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.nominal', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.residu', 'like', '%'.$request->search_query.'%')
                ->orWhere('kendaraans.created_at', 'like', '%'.$tanggal.'%')
                ->orWhere('kendaraans.keterangan', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($kendaraans as $i => $kendaraan) {

            $kendaraan->harga = Util::duit($kendaraan->harga);
            $kendaraan->nominal = Util::duit($kendaraan->nominal);
            $kendaraan->residu = Util::duit($kendaraan->residu);

            if ($kendaraan->keterangan == null) {
                $kendaraan->keterangan = ' - ';
            }

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $kendaraans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function store(Request $request){
        // return $request->all();
        $jurnal_kendaraan = new Jurnal();
        $jurnal_kas = new Jurnal();
        $akun_kendaraan = Akun::where('kode', Akun::Kendaraan)->first();
        // $harta_tetap = Akun::where('kode', Akun::HartaTetap)->first();
        // $harta_lancar = Akun::where('kode', Akun::HartaLancar)->first();
        $kendaraan = new Kendaraan();

        $kendaraan->nopol = $request->nopol;
        $kendaraan->umur = $request->umur;
        $kendaraan->atas_nama = $request->atas_nama;
        $kendaraan->tahun_perakitan = $request->tahun_perakitan;
        $kendaraan->harga = $request->harga;
        $kendaraan->nominal = $request->harga;
        $kendaraan->residu = $request->residu;
        $kendaraan->rusak = 1;
        $kendaraan->created_at = new Datetime($request->tanggal);
        $kendaraan->keterangan = $request->keterangan;
        $kendaraan->save();

        $akun_kendaraan->debet += $request->harga;
        $akun_kendaraan->update();

        $jurnal_kendaraan->kode_akun = Akun::Kendaraan;
        $jurnal_kendaraan->referensi = $request->nopol;
        $jurnal_kendaraan->debet = $request->harga;
        $jurnal_kendaraan->keterangan = 'pembelian kendaraan | '.$request->keterangan;
        $jurnal_kendaraan->save();

        if($request->kas=='tunai'){
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet -= $request->harga;
            $akun_tunai->update();

            $jurnal_kas->kode_akun = Akun::KasTunai;
            $jurnal_kas->referensi = $request->nopol;
            $jurnal_kas->kredit = $request->harga;
            $jurnal_kas->keterangan = 'pembelian kendaraan | '.$request->keterangan;
        }else{
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet -= $request->harga;
            $akun_bank->update();

            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal -= $request->harga;
            $bank->update();

            $jurnal_kas->kode_akun = Akun::KasBank;
            $jurnal_kas->referensi = $request->nopol;
            $jurnal_kas->kredit = $request->harga;
            $jurnal_kas->keterangan = 'pembelian kendaraan | '.$request->keterangan." - ".$bank->nama_bank;
        }

        $jurnal_kas->save();

        Arus::create([
            'nama' => Arus::BeliAset,
            'nominal' => $request->harga,
        ]);

        return redirect('/kendaraan')->with('sukses', 'tambah');
    }

    public function show($id)
    {
        $kendaraan = Kendaraan::find($id);
        $history = Jurnal::where('referensi', $kendaraan->nopol)->where('kode_akun', Akun::BebanPerawatan)->get();
        $banks = Bank::all();
        // return $history;
        return view('kendaraan.show', compact('kendaraan', 'history', 'banks'));
    }

    public function rawat_kendaraan(request $request)
    {
        // return $request->all();
        // $akun_perawatan = Akun::where('kode', Akun::BebanPerawatan)->first();
        // $akun_perawatan->debet += $request->nominal;
        // $akun_perawatan->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -=  $request->nominal;
        $akun_laba_ditahan->update();
        
        //jurnal perawatan
        $jurnal_perawatan = new Jurnal();
        $jurnal_perawatan->kode_akun = Akun::BebanPerawatan;
        $jurnal_perawatan->referensi = $request->kode;
        $jurnal_perawatan->keterangan = $request->keterangan;
        $jurnal_perawatan->debet = $request->nominal;
        $jurnal_perawatan->save();
        if($request->kas=='tunai'){
            //update akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet = $akun_tunai->debet - $request->nominal;
            $akun_tunai->update();
            //jurnal tunai
            $jurnal_tunai = new Jurnal();
            $jurnal_tunai->kode_akun = Akun::KasTunai;
            $jurnal_tunai->referensi = $request->kode;
            $jurnal_tunai->keterangan = $request->keterangan;
            $jurnal_tunai->kredit = $request->nominal;
            $jurnal_tunai->save();
        }else{
            //update akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet = $akun_bank->debet - $request->nominal;
            $akun_bank->update();
            //update bank nominal
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal - $request->nominal;
            $bank->update();
            //jurnal akun bank
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode;
            $jurnal_bank->keterangan = $request->keterangan." - ".$bank->nama_bank;
            $jurnal_bank->kredit = $request->nominal;
            $jurnal_bank->save();
        }

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanPerawatan,
            'referensi' => $request->kode.' - L/R',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $request->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'debet' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $request->kode.' - LDT',
            'keterangan' => $request->keterangan,
            'kredit' => $request->nominal
        ]);

        Arus::create([
            'nama' => Arus::Beban,
            'nominal' => $request->nominal,
        ]);

        return redirect('/kendaraan/show/'.$request->id)->with('sukses', 'tambah');
    }

    public function rusak(Request $request){
        // return $request->all();
        $kendaraan = Kendaraan::find($request->id);
        if($kendaraan->keterangan==NULL){
            $keterangan = $request->keterangan;
        }else{
            $keterangan = $kendaraan->keterangan." | ".$request->keterangan;
        }        
            
        $kendaraan->keterangan = $keterangan;
        $kendaraan->rusak = 0;
        $kendaraan->update();

        $tahun = (new Datetime())->format('Y');

        $beban = DB::table('jurnals')
                ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                ->where('kode_akun', Akun::BebanDepKendaraan)
                ->where('referensi', $kendaraan->nopol)
                ->whereYear('created_at', $tahun)
                ->get();

        $rugi = $kendaraan->nominal - $beban[0]->jumlah_beban;

        if($beban[0]->jumlah_beban !== null){
            $akun_dep_kendaraan = Akun::where('kode', Akun::ADKendaraan)->first();
            $akun_dep_kendaraan->kredit -= $beban[0]->jumlah_beban;
            $akun_dep_kendaraan->update(); 

            $jurnal_dep_kendaraan = new Jurnal();
            $jurnal_dep_kendaraan->kode_akun = Akun::ADKendaraan;
            $jurnal_dep_kendaraan->referensi = $kendaraan->nopol;
            $jurnal_dep_kendaraan->keterangan = $keterangan;
            $jurnal_dep_kendaraan->debet = $beban[0]->jumlah_beban;
            $jurnal_dep_kendaraan->save();
        }

        $akun_kendaraan = Akun::where('kode', Akun::Kendaraan)->first();
        $akun_kendaraan->debet -= $kendaraan->nominal;
        $akun_kendaraan->update();

        // $akun_beban = Akun::where('kode', Akun::Beban)->first();
        // $akun_beban->debet += $rugi;
        // $akun_beban->update();

        // $akun_rugi = Akun::where('kode', Akun::BebanRugiAset)->first();
        // $akun_rugi->debet += $rugi;
        // $akun_rugi->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -=  $rugi;
        $akun_laba_ditahan->update();

        $jurnal_rugi = new Jurnal();
        $jurnal_rugi->kode_akun = Akun::BebanRugiAset;
        $jurnal_rugi->referensi = $kendaraan->nopol;
        $jurnal_rugi->keterangan = $keterangan;
        $jurnal_rugi->debet = $rugi;
        $jurnal_rugi->save();

        $jurnal_kendaraan = new Jurnal();
        $jurnal_kendaraan->kode_akun = Akun::Kendaraan;
        $jurnal_kendaraan->referensi = $kendaraan->nopol;
        $jurnal_kendaraan->keterangan = $keterangan;
        $jurnal_kendaraan->kredit = $kendaraan->nominal;
        $jurnal_kendaraan->save();

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $kendaraan->nopol.' DT',
            'keterangan' => $request->keterangan,
            'debet' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanRugiAset,
            'referensi' => $kendaraan->nopol.' - L/R',
            'keterangan' => $request->keterangan,
            'kredit' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $kendaraan->nopol.' - LDT',
            'keterangan' => $request->keterangan,
            'debet' => $rugi
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $kendaraan->nopol.' - LDT',
            'keterangan' => $request->keterangan,
            'kredit' => $rugi
        ]);

        return redirect('/kendaraan')->with('sukses', 'hapus');        
    }

    public function jual(Request $request){
        // return $request->all();
        $kendaraan = Kendaraan::find($request->id);
        if($kendaraan->keterangan==NULL){
            $keterangan = $request->keterangan;
        }else{
            $keterangan = $kendaraan->keterangan." | ".$request->keterangan." | Penjualan Aset";
        }        
            
        $kendaraan->keterangan = $keterangan;
        $kendaraan->rusak = 0;
        $kendaraan->update();

        $tahun = (new Datetime())->format('Y');
        
        $beban = DB::table('jurnals')
                ->select('debet', DB::raw('SUM(debet) as jumlah_beban'))
                ->where('kode_akun', Akun::BebanDepKendaraan)
                ->where('referensi', $kendaraan->nopol)
                ->whereYear('created_at', $tahun)
                ->get();

        //update kas
        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet += $request->nominal;
            $akun_tunai->update();
           
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $kendaraan->nopol;
            $tunai->keterangan = $keterangan;
            $tunai->debet = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
        }else{
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet += $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal += $request->nominal;
            $bank->update();
            // create baris akun tunai
            $keterangan_bank = $keterangan.' - masuk Bank '.$bank->nama_bank;
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $kendaraan->nopol;
            $jurnal_bank->keterangan = $keterangan_bank;
            $jurnal_bank->debet = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
        }

        //kalo punya depresiasi
        if($beban[0]->jumlah_beban !== NULL){
            $akun_dep_kendaraan = Akun::where('kode', Akun::ADKendaraan)->first();
            $akun_dep_kendaraan->kredit -= $beban[0]->jumlah_beban;
            $akun_dep_kendaraan->update(); 

            $jurnal_dep_kendaraan = new Jurnal();
            $jurnal_dep_kendaraan->kode_akun = Akun::ADKendaraan;
            $jurnal_dep_kendaraan->referensi = $kendaraan->nopol;
            $jurnal_dep_kendaraan->keterangan = $keterangan;
            $jurnal_dep_kendaraan->debet = $beban[0]->jumlah_beban;
            $jurnal_dep_kendaraan->save();
        }

        //hitung  untung atau rugi
        $selisih = ($beban[0]->jumlah_beban + $request->nominal) - $kendaraan->nominal;
        // return $selisih;
        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
            $akun_laba_ditahan->kredit +=  $selisih;
            $akun_laba_ditahan->update();

        if($selisih < 0){
            // $akun_rugi = Akun::where('kode', Akun::BebanRugiAset)->first();
            // $akun_rugi->debet += ($selisih * -1);
            // $akun_rugi->update();             

            Jurnal::create([
                'kode_akun' => Akun::BebanRugiAset,
                'referensi' => $kendaraan->nopol,
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::Kendaraan,
                'referensi' => $kendaraan->nopol,
                'keterangan' => $keterangan,
                'kredit' => $kendaraan->nominal
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kendaraan->nopol.' - L/R',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::BebanRugiAset,
                'referensi' => $kendaraan->nopol.' - L/R',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kendaraan->nopol.' - LDT',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kendaraan->nopol.' - LDT',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);
        }else{
            // $akun_untung = Akun::where('kode', Akun::PendapatanLain)->first();
            // $akun_untung->kredit += $selisih;
            // $akun_untung->update(); 

            Jurnal::create([
                'kode_akun' => Akun::Kendaraan,
                'referensi' => $kendaraan->nopol,
                'keterangan' => $keterangan,
                'kredit' => $kendaraan->harga
            ]);

            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kendaraan->nopol,
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::PendapatanLain,
                'referensi' => $kendaraan->nopol.' - L/R',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kendaraan->nopol.' - L/R',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaRugi,
                'referensi' => $kendaraan->nopol.' - LDT',
                'keterangan' => $keterangan,
                'debet' => $selisih
            ]);

            Jurnal::create([
                'kode_akun' => Akun::LabaTahan,
                'referensi' => $kendaraan->nopol.' - LDT',
                'keterangan' => $keterangan,
                'kredit' => $selisih
            ]);
        }  

        $akun_alat = Akun::where('kode', Akun::Kendaraan)->first();
        $akun_alat->debet -= $kendaraan->nominal;
        $akun_alat->update();

        Arus::create([
            'nama' => Arus::JualAset,
            'nominal' => $request->nominal,
        ]);

        return redirect('/kendaraan')->with('sukses', 'jual');        
    }
}
