<?php

namespace App\Http\Controllers;

use App\Item;
use App\Suplier;
use App\ItemMasuk;
use App\ItemKeluar;

use Illuminate\Http\Request;

class ItemKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $item_keluars = ItemKeluar::with('item_masuk', 'item_masuk.item', 'item_masuk.suplier')->get();
        $item_keluars = ItemKeluar::all();
        return view('item_keluar.index', compact('item_keluars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $item_masuk = ItemMasuk::find($id);
        $items = Item::all();
        $supliers = Suplier::all();
        return view('item_keluar.create', compact('item_masuk', 'items', 'supliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'jumlah' => 'required|integer|min:1',
            'keterangan' => 'required|string'
        ]);

        $item_keluar = new ItemKeluar();
        $item_keluar->item_masuk_id = $request->item_masuk_id;
        $item_keluar->jumlah = $request->jumlah;
        $item_keluar->keterangan = $request->keterangan;
        if ($item_keluar->save()) {
            $item = Item::find($item_keluar->item_masuk->id);
            $item->stok -= $item_keluar->jumlah;
            $item->save();

            return redirect('/item-keluar')->with('sukses', 'tambah');
        } else {
            return redirect('/item-keluar')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $item_keluar = ItemKeluar::with('item_masuk', 'item_masuk.item', 'item_masuk.suplier')->find($id);
        $item_keluar = ItemKeluar::find($id);
        return view('item_keluar.show', compact('item_keluar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $item_keluar = ItemKeluar::with('item_masuk', 'item_masuk.item', 'item_masuk.suplier')->find($id);
        $item_keluar = ItemKeluar::find($id);
        $items = Item::all();
        $supliers = Suplier::all();
        return view('item_keluar.edit', compact('item_keluar', 'items', 'supliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'jumlah' => 'required|integer|min:1',
            'keterangan' => 'required|string'
        ]);

        $item_keluar = ItemKeluar::find($id);
        $stok = $item_keluar->jumlah;
        $item_keluar->jumlah = $request->jumlah;
        $item_keluar->keterangan = $request->keterangan;
        if ($item_keluar->save()) {
            $item = Item::find($item_keluar->item_masuk->id);
            $item->stok -= ($item_keluar->jumlah - $stok);
            $item->save();

            return redirect('/item-keluar')->with('sukses', 'ubah');
        } else {
            return redirect('/item-keluar')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $item_keluar = ItemKeluar::find($id);
    //     if ($item_keluar->delete()) {
    //         return redirect('/item-keluar')->with('sukses', 'hapus');
    //     } else {
    //         return redirect('/item-keluar')->with('gagal', 'hapus');
    //     }
    // }
}
