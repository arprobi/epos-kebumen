<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Akun;
use App\Jurnal;
use Illuminate\Http\Request;

class LabaDitahanController extends Controller
{
	public function lastJson()
    {
        $laba_ditahan = Jurnal::where('kode_akun', Akun::LabaTahan)
                    ->orderBy('id', 'desc')
                    ->first();
        return response()->json(['laba_ditahan' => $laba_ditahan]);
    }

    public function index(){
        $banks = Bank::all();
        return view('laba_ditahan.index', compact('banks'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        //jurnal beban
        if($request->beban == Akun::BebanIklan){
            //sewa dimuka 
            $jurnal_sewa_dimuka = new Jurnal();
            $jurnal_sewa_dimuka->kode_akun = Akun::SewaDimuka;
            $jurnal_sewa_dimuka->referensi = $request->kode_transaksi;
            $jurnal_sewa_dimuka->keterangan = $request->keterangan;
            $jurnal_sewa_dimuka->debet = $request->nominal;
            $jurnal_sewa_dimuka->save();
            //update akun beban
            $akun_beban = Akun::where('kode', '=', Akun::SewaDimuka)->first();
            $akun_beban->debet = $akun_beban->debet + $request->nominal;
            $akun_beban->update();
        }elseif($request->beban == Akun::BebanAsuransi){
            //asuransi dimuka
            $jurnal_asuransi_dimuka = new Jurnal();
            $jurnal_asuransi_dimuka->kode_akun = Akun::AsuransiDimuka;
            $jurnal_asuransi_dimuka->referensi = $request->kode_transaksi;
            $jurnal_asuransi_dimuka->keterangan = $request->keterangan;
            $jurnal_asuransi_dimuka->debet = $request->dimuka;
            $jurnal_asuransi_dimuka->save();
            //update akun beban
            $akun_beban = Akun::where('kode', '=', Akun::AsuransiDimuka)->first();
            $akun_beban->debet = $akun_beban->debet + $request->nominal;
            $akun_beban->update();

        }else{
            //create jurnal beban
            $jurnal_beban = new Jurnal();
            $jurnal_beban->kode_akun = $request->beban;
            $jurnal_beban->referensi = $request->kode_transaksi;
            $jurnal_beban->keterangan = $request->keterangan;
            $jurnal_beban->debet = $request->nominal;
            $jurnal_beban->save();
            //update akun beban
            $akun_beban = Akun::where('kode', '=', $request->beban)->first();
            $akun_beban->debet += $request->nominal;
            $akun_beban->update();
        }

        if($request->kas=='tunai'){
            // update nominal di akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet = $akun_tunai->debet - $request->nominal;
            $akun_tunai->update();
            // create jurnal akun tunai
            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->keterangan;
            $tunai->kredit = $request->nominal;
            // simpan jurnal tunai
            $tunai->save();
        }else{
            // update nominal di akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet = $akun_bank->debet - $request->nominal;
            $akun_bank->update();
            // update nominal di tabel bank
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal + $request->nominal;
            $bank->update();
            // create baris akun tunai
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode_transaksi;
            $jurnal_bank->keterangan = $request->keterangan;
            $jurnal_bank->kredit = $request->nominal;
            // simpan jurnal bank
            $jurnal_bank->save();
        }

        return redirect('/beban')->with('sukses', 'tambah');        
    }
}
