<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;

use App\Util;

use Illuminate\Http\Request;

class GrafikPembelianController extends Controller
{
    public function index()
    {
        $transaksi_pembelians = DB::table('transaksi_pembelians')
            ->select(
                DB::raw('IFNULL(harga_total, 0) - IFNULL(potongan_pembelian, 0) + IFNULL(ongkos_kirim, 0) as harga'))
            ->where('po', 0)
            ->get();
        return view('grafik.pembelian', compact('transaksi_pembelians'));
    }

    public function tahun($tahun)
    {
        $transaksi_pembelians = DB::table('transaksi_pembelians')
            ->select(
                DB::raw('SUM(IFNULL(harga_total, 0) - IFNULL(potongan_pembelian, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('MONTH(created_at) bulan'))
            ->groupBy('bulan')
            ->where('po', 0)
            ->where('created_at', 'like', $tahun.'-%')
            ->get();
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $data = array();
        for ($i = 1; $i <= 12; $i++) {
            foreach ($transaksi_pembelians as $j => $transaksi_pembelian) {
                if ($transaksi_pembelian->bulan == $i) {
                    $data[$i - 1] = $transaksi_pembelian->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function bulan($tahun, $bulan)
    {
        $transaksi_pembelians = DB::table('transaksi_pembelians')
            ->select(
                DB::raw('SUM(IFNULL(harga_total, 0) - IFNULL(potongan_pembelian, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('DAY(created_at) tanggal'))
            ->groupBy('tanggal')
            ->where('po', 0)
            ->where('created_at', 'like', $tahun.'-'.$bulan.'-%')
            ->get();

        $datetime = new DateTime($tahun.'-'.$bulan);
        $last_day = $datetime->format('t');
        $labels = array();
        $data = array();
        for ($i = 1; $i <= intval($last_day); $i++) {
            array_push($labels, $i);
            foreach ($transaksi_pembelians as $j => $transaksi_pembelian) {
                if ($transaksi_pembelian->tanggal == $i) {
                    $data[$i - 1] = $transaksi_pembelian->harga;
                    break;
                } else {
                    $data[$i - 1] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }

    public function rentang($awal, $akhir)
    {
        $awal_ = new DateTime($awal);
        $awal_ = $awal_->add(new DateInterval('P1D'));
        $awal = $awal_->format('Y-m-d');
        $akhir_ = new DateTime($akhir);
        $akhir = $akhir_->format('Y-m-d');

        $interval = date_diff($akhir_, $awal_);

        $labels = array();
        $data = array();
        for ($i = 0; $i <= $interval->days; $i++) {
            $val = 0;
            if ($i == 0 ) {
                $tanggal_ = $awal_->add(new DateInterval('P0D'));
            } else {
                $tanggal_ = $awal_->add(new DateInterval('P1D'));
            }

            $tanggal = $tanggal_->format('d m Y');
            $date = $tanggal_->format('Y-m-d');

            $transaksi_pembelians = DB::table('transaksi_pembelians')
                ->select(DB::raw('SUM(IFNULL(harga_total, 0) - IFNULL(potongan_pembelian, 0) + IFNULL(ongkos_kirim, 0)) as harga'))
                ->where('po', 0)
                ->whereDate('created_at', $date)
                ->get();

            if (count($transaksi_pembelians) > 0 && $transaksi_pembelians[0]->harga != null) {
                $val = $transaksi_pembelians[0]->harga;
            }

            array_push($labels, \App\Util::tanggal($tanggal));
            array_push($data, $val);
        }

        return response()->json(compact('labels', 'data'));
    }

    public function tanggal($tanggal)
    {
        $transaksi_pembelians = DB::table('transaksi_pembelians')
            ->select(
                DB::raw('SUM(IFNULL(harga_total, 0) - IFNULL(potongan_pembelian, 0) + IFNULL(ongkos_kirim, 0)) as harga'),
                DB::raw('HOUR(created_at) jam'))
            ->groupBy('jam')
            ->where('po', 0)
            ->where('created_at', 'like', $tanggal.'%')
            ->get();
        $labels = array();
        $data = array();
        for ($i = 6; $i <= 22; $i++) {
            $label = $i;
            if ($i < 10) $label = '0'.$i;
            $label .= '.00';
            array_push($labels, $label);
            foreach ($transaksi_pembelians as $j => $transaksi_pembelian) {
                if ($transaksi_pembelian->jam == $i) {
                    $data[$i - 6] = $transaksi_pembelian->harga;
                    break;
                } else {
                    $data[$i - 6] = 0;
                }
            }
        }

        return response()->json(compact('labels', 'data'));
    }
}
