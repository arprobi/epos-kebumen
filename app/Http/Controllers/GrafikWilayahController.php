<?php

namespace App\Http\Controllers;

use App\Desa;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Pelanggan;
use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class GrafikWilayahController extends Controller
{
    public function index()
    {
    	$provinsis = Provinsi::all();
        $kabupatens = Kabupaten::all();
        $kecamatans = Kecamatan::with('kabupaten')->get();

        return view('grafik.wilayah', compact('provinsis', 'kabupatens', 'kecamatans'));
    }

    public function ProvinsiAll()
    {
        $provinsis = Provinsi::all();
        if (sizeof($provinsis) == 1) {
            $labels = ['-'];
            $datas = ['-'];
        } else {
            $labels = [];
            $datas = [];
        }
        foreach ($provinsis as $l => $_provinsi) {
            array_push($labels, $_provinsi->nama);
            $kabupatens = Kabupaten::where('provinsi_id', $_provinsi->id)->get();
            $sum = 0;
            foreach ($kabupatens as $i => $_kabupaten) {
                $desas = [];
                foreach ($_kabupaten->desas as $key => $desa) {
                    array_push($desas, $desa->id);
                }
                $sum += Pelanggan::whereIn('desa_id', $desas)->count();
            }
            array_push($datas, $sum);
        }
        
        return response()->json(compact('labels', 'datas'));
    }

    public function Provinsi($id)
    {
        $kabupatens = Kabupaten::where('provinsi_id', $id)->get();
        if (sizeof($kabupatens) == 1) {
            $labels = ['-'];
            $datas = ['-'];
        } else {
            $labels = [];
            $datas = [];
        }
        foreach ($kabupatens as $i => $_kabupaten) {
            array_push($labels, $_kabupaten->nama);
            $desas = [];
            foreach ($_kabupaten->desas as $key => $desa) {
                array_push($desas, $desa->id);
            }
            array_push($datas, Pelanggan::whereIn('desa_id', $desas)->count());
        }
        
        return response()->json(compact('labels', 'datas'));
    }

    public function Kabupaten($id)
    {
        $kecamatans = Kecamatan::where('kabupaten_id', $id)->get();
        if (sizeof($kecamatans) == 1) {
            $labels = ['-'];
            $datas = ['-'];
        } else {
            $labels = [];
            $datas = [];
        }
        foreach ($kecamatans as $i => $_kecamatan) {
            array_push($labels, $_kecamatan->nama);
            $desas = [];
            foreach ($_kecamatan->desas as $key => $desa) {
                array_push($desas, $desa->id);
            }
            array_push($datas, Pelanggan::whereIn('desa_id', $desas)->count());
        }

        return response()->json(compact('labels', 'datas'));
    }

    public function Kecamatan($id)
    {
        $desas = Desa::where('kecamatan_id', $id)->get();
        $labels = [];
        $datas = [];
        foreach ($desas as $i => $desa) {
            array_push($labels, $desa->nama);
            array_push($datas, Pelanggan::where('desa_id', $desa->id)->count());
        }

        return response()->json(compact('labels', 'datas'));
    }
}
