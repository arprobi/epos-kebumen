<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;

use App\SetoranBuka;
use App\CashDrawer;
use App\MoneyLimit;

use Illuminate\Http\Request;

class SidebarController extends Controller
{
    public function setoranBuka()
    {
        $user = Auth::user();
        $now = new DateTime();
        $now = $now->format('Y-m-d');
        $setoran_buka = SetoranBuka::where('user_id', $user->id)->where('created_at', 'like', '%'.$now.'%')->first();
        $cash_drawer = CashDrawer::where('user_id', $user->id)->first();
        $limit = MoneyLimit::find(1);

        return response()->json(compact('setoran_buka', 'cash_drawer', 'user', 'limit'));
    }
}
