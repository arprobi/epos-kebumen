<?php

namespace App\Http\Controllers;

use App\Akun;
use App\Arus;
use App\Bank;
use App\User;
use App\Util;
use App\Jurnal;
use App\PiutangLain;
use App\BayarPiutangLain;

use Illuminate\Http\Request;

class PiutangLainController extends Controller
{
    public function lastJson()
    {
        $piutang_lain = PiutangLain::all()->last();
        return response()->json(['piutang_lain' => $piutang_lain]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $piutang_lains = PiutangLain::where('sisa', '>', 0)->orderBy('created_at', 'dsc')->get();
        $piutang_offs = PiutangLain::where('sisa', 0)->orderBy('created_at', 'dsc')->get();
        $banks = Bank::all();
        $karyawans = User::whereIN('level_id', [3,4,5])->get();
       
        return view('piutang_lain.index', compact('piutang_lains', 'banks', 'karyawans', 'piutang_offs')); 
    } */

    public function index()
    {
        $banks = Bank::all();
        $karyawans = User::whereIN('level_id', [3,4,5])->get();
       
        return view('piutang_lain.index', compact('banks', 'karyawans'));
    }

    public function mdt1(Request $request)
    {
        $piutangs = PiutangLain
            ::where('sisa', '>', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = PiutangLain
            ::where('sisa', '>', 0)
            ->count();

        foreach ($piutangs as $i => $piutang) {
            $piutang->nominal = Util::ewon($piutang->nominal);
            $piutang->sisa = Util::ewon($piutang->sisa);

            if ($piutang->karyawan == 1) {
                $piutang->mdt_karyawan = 'Karyawan';
            } else {
                $piutang->mdt_karyawan = 'Bukan';
            }

            if ($piutang->keterangan == null) {
                $piutang->keterangan = ' - ';
            }

            $buttons['bayar'] = ['url' => url('piutang_lain/show/'.$piutang->id)];
            $buttons['transfer'] = ['url' => url('transfer_piutang_lain/'.$piutang->id)];

            // return $buttons;
            $piutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $piutangs,
        ]);
    }

    public function mdt2(Request $request)
    {
        $piutangs = PiutangLain
            ::where('sisa', '=', 0)
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy('created_at', 'desc')
            ->get();

        $count = PiutangLain
            ::where('sisa', '=', 0)
            ->count();

        foreach ($piutangs as $i => $piutang) {
            $piutang->nominal = Util::ewon($piutang->nominal);

            if ($piutang->karyawan == 1) {
                $piutang->mdt_karyawan = 'Karyawan';
            } else {
                $piutang->mdt_karyawan = 'Bukan';
            }

            if ($piutang->keterangan == null) {
                $piutang->keterangan = ' - ';
            }

            $buttons['riwayat'] = ['url' => url('piutang_lain/show/'.$piutang->id)];

            // return $buttons;
            $piutang->buttons = $buttons;
        }

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $piutangs,
        ]);
    }

    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'nama' => 'required|max:255',
            'nominal' => 'required|max:255',
            'kas' => 'required|max:255',
            'kode_transaksi' => 'required|max:255',
        ]);

        $piutang_lain = new PiutangLain();
        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();

        $piutang_lain->nama = $request->nama;
        $piutang_lain->karyawan = $request->karyawan;
        $piutang_lain->nominal = $request->nominal;
        $piutang_lain->sisa = $request->nominal;
        $piutang_lain->user_id = $request->user;
        $piutang_lain->keterangan = $request->keterangan;
        $piutang_lain->kode_transaksi = $request->kode_transaksi;

        if ($request->kas == 'tunai') {
            $akun_tunai->debet = $akun_tunai->debet - $request->nominal;
            $akun_tunai->update();

            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->keterangan;
            $tunai->kredit = $request->nominal;
            if ($request->karyawan == 1) {
                $piutang_karyawan = new Jurnal();
                $piutang_karyawan->kode_akun = Akun::PiutangKaryawan;
                $piutang_karyawan->referensi = $request->kode_transaksi;
                $piutang_karyawan->keterangan = $request->keterangan;
                $piutang_karyawan->debet = $request->nominal;
                $piutang_karyawan->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangKaryawan)->first();
                $akun_piutang->debet += $request->nominal;
                $akun_piutang->update();

                $tunai->save();
            } else {
                $jurnal_piutang_lain = new Jurnal();
                $jurnal_piutang_lain->kode_akun = Akun::PiutangLain;
                $jurnal_piutang_lain->referensi = $request->kode_transaksi;
                $jurnal_piutang_lain->keterangan = $request->keterangan;
                $jurnal_piutang_lain->debet = $request->nominal;
                $jurnal_piutang_lain->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangLain)->first();
                $akun_piutang->debet += $request->nominal;
                $akun_piutang->update();

                $tunai->save();
            }
        } else {
            $akun_bank->debet = $akun_bank->debet - $request->nominal;
            $akun_bank->update();
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal + $request->nominal;
            $bank->update();

            $j_bank = new Jurnal();
            $j_bank->kode_akun = Akun::KasBank;
            $j_bank->referensi = $request->kode_transaksi;
            $j_bank->keterangan = $request->keterangan;
            $j_bank->kredit = $request->nominal;
            if($request->karyawan==1){
                $piutang_karyawan = new Jurnal();
                $piutang_karyawan->kode_akun = Akun::PiutangKaryawan;
                $piutang_karyawan->referensi = $request->kode_transaksi;
                $piutang_karyawan->keterangan = $request->keterangan;
                $piutang_karyawan->debet = $request->nominal;
                $piutang_karyawan->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangKaryawan)->first();
                $akun_piutang->debet += $request->nominal;
                $akun_piutang->update();

                $j_bank->save();
            } else {
                $jurnal_piutang_lain = new Jurnal();
                $jurnal_piutang_lain->kode_akun = Akun::PiutangLain;
                $jurnal_piutang_lain->referensi = $request->kode_transaksi;
                $jurnal_piutang_lain->keterangan = $request->keterangan;
                $jurnal_piutang_lain->debet = $request->nominal;
                $jurnal_piutang_lain->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangLain)->first();
                $akun_piutang->debet += $request->nominal;
                $akun_piutang->update();

                $j_bank->save();
            }
        }

        Arus::create([
            'nama' => Arus::PiutangLain,
            'nominal' => $request->nominal,
        ]);
        
        if ($piutang_lain->save()) {
            return redirect('/piutang_lain')->with('sukses', 'tambah');
        } else {
            return redirect('/piutang_lain')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $piutang_lain = PiutangLain::where('id', $id)->first();
        $bayar = BayarPiutangLain::where('piutang_lain_id', $id)->get();
        $banks = Bank::all();

        return view('piutang_lain.show', compact('bayar', 'piutang_lain', 'banks'));   
    }

    public function transfer_piutang($id)
    {
        $piutang_lain = PiutangLain::where('id', $id)->first();
        $beban_tt = Akun::where('kode', Akun::BebanKerugianPiutang)->first();
        
        return view('piutang_lain.transfer', compact('piutang_lain','beban_tt'));
    }

    public function store_transfer_piutang(request $request)
    {
        // return $request->all();
        $piutang_asal = PiutangLain::where('id', $request->piutang_lain_id)->first();
        $piutang_asal->sisa -= $request->nominal;
        $piutang_asal->PT += $request->nominal;

        $laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $laba_ditahan->kredit -= $request->nominal;
        $laba_ditahan->update();

        Jurnal::create([
            'kode_akun' => Akun::BebanKerugianPiutang,
            'referensi' => $piutang_asal->kode_transaksi,
            'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
            'debet' => $request->nominal
        ]);

        if ($request->asal == 1) {
            $piutang_karyawan = Akun::where('kode', Akun::PiutangKaryawan)->first();
            $piutang_karyawan->debet = $piutang_karyawan->debet - $request->nominal;
            $piutang_karyawan->update();

            Jurnal::create([
                'kode_akun' => Akun::PiutangKaryawan,
                'referensi' => $piutang_asal->kode_transaksi,
                'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
                'kredit' => $request->nominal
            ]);
        } else {
            $piutang_lain = Akun::where('kode', Akun::PiutangLain)->first();
            $piutang_lain->debet = $piutang_lain->debet - $request->nominal;
            $piutang_lain->update();

            Jurnal::create([
                'kode_akun' => Akun::PiutangLain,
                'referensi' => $piutang_asal->kode_transaksi,
                'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
                'kredit' => $request->nominal
            ]);
        }

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $piutang_asal->kode_transaksi.' L/R',
            'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
            'debet' => $request->nominal
        ]);
        Jurnal::create([
            'kode_akun' => Akun::BebanKerugianPiutang,
            'referensi' => $piutang_asal->kode_transaksi.' L/R',
            'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
            'kredit' => $request->nominal
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $piutang_asal->kode_transaksi.' LDT',
            'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
            'debet' => $request->nominal
        ]);
        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $piutang_asal->kode_transaksi.' LDT',
            'keterangan' => $request->nama.' - '.$request->keterangan.' Transfer',
            'kredit' => $request->nominal
        ]);

        if($piutang_asal->update()){
            return redirect('/piutang_lain')->with('sukses', 'tambah');
        } else {
            return redirect('/piutang_lain')->with('gagal', 'tambah');
        }
    }
}
