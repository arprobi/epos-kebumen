<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use DateTime;

use App\Akun;
use App\Util;
use App\Bank;
use App\Laci;
use App\User;
use App\Jurnal;
use App\LogLaci;
use App\CashDrawer;
use App\SetoranBuka;
use App\SetoranKasir;
use Illuminate\Http\Request;

class LaciController extends Controller
{
    public function index()
    {
        $laci = Laci::find(1);
        $level = Auth::user()->level_id;
        $today = new DateTime();
        $today = $today->format('Y-m-d');
        $banks = Bank::all();

        $gudang_receiver = User::where('level_id', 5)->where('status', 1)->get();
        $owner_receiver = User::where('level_id', 1)->where('status', 1)->orWhere('level_id', 2)->get();
        $eceran_receiver = User::where('level_id', 3)->where('status', 1)->get();
        $grosir_receiver = User::where('level_id', 4)->where('status', 1)->get();
        $sender = User::where('status', 1)->get();

        // $all_receiver = User::where('status', 1)->get();
        $all_receiver = User::join('levels', 'users.level_id', '=', 'levels.id')->orderBy('levels.kode')->select('users.*')->where('users.status', 1)->get();

        $user_gudang = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 5)->get();
        $gudang_array = array();
        foreach ($user_gudang as $i => $user) {
            array_push($gudang_array, $user->id);
        }

        $user_grosir = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 4)->get();
        $grosir_array = array();
        foreach ($user_grosir as $i => $user) {
            array_push($grosir_array, $user->id);
        }

        $user_eceran = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 3)->get();
        $eceran_array = array();
        foreach ($user_eceran as $i => $user) {
            array_push($eceran_array, $user->id);
        }
        $users = User::where('level_id', '3')->get();

        $user_owner = User::select(DB::raw('id'), DB::raw('level_id'))->where('level_id', 1)->orWhere('level_id', 2)->get();
        $owner_array = array();
        foreach ($user_owner as $i => $user) {
            array_push($owner_array, $user->id);
        }

        if ($level == 1 || $level == 2) {
            // $setorans = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $gudang_array)->latest()->get();
            // $setoran_gudang = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $gudang_array)->latest()->get();
            // $from_owner = LogLaci::whereIN('pengirim', $owner_array)->orderBy('created_at', 'desc')->get();

            // $from_gudang = LogLaci::whereIn('pengirim', $gudang_array)->orwhereIn('pengirim', $grosir_array)->orderBy('created_at', 'desc')->get();
            // $from_grosir = LogLaci::whereIn('pengirim', $grosir_array)->orderBy('created_at', 'desc')->get();
            $setoran_masuk = LogLaci::whereIn('penerima', $owner_array)->where('pengirim', '!=', null)->orderBy('created_at', 'desc')->get();
            $setorans = LogLaci::whereIn('pengirim', $owner_array)->where('penerima', '!=', null)->orderBy('created_at', 'desc')->get();

           return view('laci.owner', compact('laci', 'setorans', 'banks', 'gudang_receiver', 'grosir_receiver', 'from_grosir', 'from_gudang', 'setoran_masuk', 'sender', 'all_receiver'));

        } else if ($level == 4) {
            $setorans = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $grosir_array)->latest()->get();
            $setoran_buka = SetoranBuka::where('created_at', '>=', $today)->orderBy('created_at', 'desc')->get();
            $from_grosir = LogLaci::whereIN('pengirim', $grosir_array)->where('penerima', '!=', null)->orderBy('created_at', 'desc')->get();
            $setors = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $eceran_array)->latest()->get();

            $to_grosir = LogLaci::whereIn('penerima', $grosir_array)->where('pengirim', '!=', null)->orderBy('created_at', 'desc')->get();

            return view('laci.grosir', compact('laci', 'setorans', 'setoran_buka', 'users', 'from_grosir', 'setors', 'to_grosir', 'owner_receiver', 'banks', 'sender', 'gudang_receiver', 'all_receiver'));
        } else if ($level == 5) {
            // $setorans = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $gudang_array)->latest()->get();
            // $setoran_grosir = SetoranKasir::where('created_at', '>=', $today)->whereIn('user_id', $grosir_array)->latest()->get();
            $from_gudang = LogLaci::whereIN('pengirim', $gudang_array)->where('penerima', '!=', null)->orderBy('created_at', 'desc')->get();
            $to_gudang = LogLaci::whereIN('penerima', $gudang_array)->where('pengirim', '!=', null)->orderBy('created_at', 'desc')->get();

           return view('laci.gudang', compact('laci', 'banks', 'from_gudang', 'from_owner', 'owner_receiver', 'grosir_receiver', 'to_gudang', 'all_receiver'));
        }
    }

    /*public function store(Request $request) {
     // return $request;
     $laci = Laci::find(1);
     $laci->grosir = $request->nominal;
     $laci->update();

        return redirect('/laci')->with('sukses', 'ubah');
    }*/

    public function setorGrosir(Request $request)
    {
        // return $request->all();
        // $laci = Laci::find(1);
        // $laci->grosir += $request->nominal;
        // $laci->gudang -= $request->nominal;
        // $laci->update();

        // $penerima = User::where('level_id', 4)->first()->id;

        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->approve = 0;
        $log->penerima = $request->penerima;
        $log->nominal = $request->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setor_grosir');
    }

    public function setorGudang(Request $request)
    {
        // return $request->all();
        // $laci = Laci::find(1);
        // $laci->gudang += $request->nominal;
        // $laci->owner -= $request->nominal;
        // $laci->update();

        // $penerima = User::where('level_id', 5)->first()->id;
        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $request->penerima;
        $log->approve = 0;
        $log->nominal = $request->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setor_gudang');
    }

    public function storeOwner(Request $request)
    {
        // return $request->all();
        $laci = Laci::find(1);
        $laci->owner = $request->nominal;
        $laci->update();

        $log = new LogLaci();
        // $log->pengirim = Auth::user()->id;
        $log->approve = 1;
        $log->penerima = Auth::user()->id;
        $log->nominal = $request->nominal;
        $log->keterangan = 'Set Nominal Baru Laci Owner';
        $log->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function transferBank(Request $request)
    {
        // return $request->all();
        $kode = self::kode_baru();
        $keterangan_laci = 'Pengirim : ';

        // nambah nominal di tabel bank
        $bank = Bank::find($request->bank_asal);
        $bank->nominal += $request->nominal;
        $bank->update();

        $keterangan = $request->keterangan.' ke Bank '.$bank->nama_bank;

        // ngurangin laci
        $laci = Laci::find(1);
        $laci->owner -= $request->nominal;
        $laci->update();

        // log laci
        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->nominal = $request->nominal;
        $log->status = 3;
        if($request->pengirim == 'lain'){
            $keterangan_laci .= $request->nama_pengirim;
        }else{
            $user = User::find($request->pengirim);
            $keterangan_laci .= $user->nama;
        }
        $log->keterangan = $keterangan_laci;

        $log->save();

        // ngurangin akun tunai
        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_tunai->debet -= $request->nominal;
        $akun_tunai->update();

        // nambah akun bank
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_bank->debet += $request->nominal;
        $akun_bank->update();

        // jurnal akun bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $kode,
            'keterangan' => $keterangan,
            'debet' => $request->nominal
        ]);

        // jurnal akun tunai
        Jurnal::create([
            'kode_akun' => Akun::KasTunai,
            'referensi' => $kode,
            'keterangan' => $keterangan,
            'kredit' => $request->nominal
        ]);

        return redirect('/laci')->with('sukses', 'transfer_owner');
    }

    public function transferBankGrosir(Request $request)
    {
        // return $request->all();
        $kode = self::kode_baru();
        $keterangan_laci = 'Pengirim : ';

        // nambah nominal di tabel bank
        $bank = Bank::find($request->bank_asal);
        $bank->nominal += $request->nominal;
        $bank->update();

        $keterangan = $request->keterangan.' ke Bank '.$bank->nama_bank;

        // ngurangin laci
        $laci = Laci::find(1);
        $laci->grosir -= $request->nominal;
        $laci->update();

        // log laci
        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->nominal = $request->nominal;
        $log->status = 3;
        if($request->pengirim == 'lain'){
            $keterangan_laci .= $request->nama_pengirim;
        }else{
            $user = User::find($request->pengirim);
            $keterangan_laci .= $user->nama;
        }
        $log->keterangan = $keterangan_laci;

        $log->save();

        // ngurangin akun tunai
        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_tunai->debet -= $request->nominal;
        $akun_tunai->update();

        // nambah akun bank
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $akun_bank->debet += $request->nominal;
        $akun_bank->update();

        // jurnal akun bank
        Jurnal::create([
            'kode_akun' => Akun::KasBank,
            'referensi' => $kode,
            'keterangan' => $keterangan,
            'debet' => $request->nominal
        ]);

        // jurnal akun tunai
        Jurnal::create([
            'kode_akun' => Akun::KasTunai,
            'referensi' => $kode,
            'keterangan' => $keterangan,
            'kredit' => $request->nominal
        ]);

        return redirect('/laci')->with('sukses', 'transfer_owner');
    }

    private function kode_baru()
    {
        $tanggal = date('d/m/Y');
        $kode_transaksi_baru = '';
        $transfer_terakhir = Jurnal::where('referensi', 'like', '%%%%/TFK/%%%%%%%')
                    ->orderBy('id', 'desc')
                    ->first();

        if ($transfer_terakhir == null) {
            // buat kode_transaksi_baru
            $kode_transaksi_baru = '0001/TFK/'.$tanggal;
        } else {
            $kode_terakhir = $transfer_terakhir->referensi;
            $tanggal_kode_terakhir = substr($kode_terakhir, 9, 10);
            // buat kode_transaksi dari kode_transaksi terakhir + 1
            if ($tanggal_kode_terakhir == $tanggal) {
                $kode_terakhir = substr($kode_terakhir, 0, 4);
                $kode_baru = intval($kode_terakhir) + 1;
                $kode_transaksi_baru = Util::int4digit($kode_baru).'/TFK/'.$tanggal;
            } else {
                // buat kode_transaksi_baru
                $kode_transaksi_baru = '0001/TFK/'.$tanggal;
            }
        }

        return $kode_transaksi_baru;
    }

    public function approveGudang($id){
        // return $id;
        // $setoran = SetoranKasir::find($id);
        // $setoran->status = 1;
        // $setoran->operator = Auth::user()->id;
        // $log = new LogLaci();
        // $log->pengirim = $setoran->user_id;
        // $log->penerima = Auth::user()->id;
        // $log->nominal = $setoran->nominal;
        // $log->save();

        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        $laci->owner += $log_laci->nominal;
        $laci->gudang -= $log_laci->nominal;
        $laci->update();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function approveOwner($id){
        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        if($log_laci->sender->level->id == 4){
            $laci->owner += $log_laci->nominal;
            $laci->grosir -= $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 5){
            $laci->owner += $log_laci->nominal;
            $laci->gudang -= $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 3){
            $laci->owner += $log_laci->nominal;
            $cash_drawer = CashDrawer::where('user_id', $log_laci->pengirim)->first();
            $cash_drawer->nominal -= $log_laci->nominal;
            $cash_drawer->update();
        }
        $laci->update();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function approveGrosir($id){
        // return $id;
        // $setoran = SetoranKasir::find($id);
        // $setoran->status = 1;
        // $setoran->operator = Auth::user()->id;
        // $log = new LogLaci();
        // $log->pengirim = $setoran->user_id;
        // $log->penerima = Auth::user()->id;
        // $log->nominal = $setoran->nominal;
        // $log->save();

        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        if($log_laci->sender->level->id == 1 || $log_laci->sender->level->id == 2){
            $laci->owner -= $log_laci->nominal;
            $laci->grosir += $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 3){
            $cash_drawer = CashDrawer::where('user_id', $log_laci->pengirim)->first();
            $laci->grosir += $log_laci->nominal;
            $cash_drawer->nominal -= $log_laci->nominal;
            $cash_drawer->update();
        }
        $laci->update();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function approveEcer($id){
        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        $cash_drawer = CashDrawer::where('user_id', $log_laci->penerima)->first();
        if($log_laci->sender->level->id == 4 ){
            //jika log_laci -> grosir make grosir
            $laci->grosir -= $log_laci->nominal;
        }else{
            //jika log_laci -> owner make owner
            $laci->owner -= $log_laci->nominal;
        }
        $cash_drawer->nominal += $log_laci->nominal;
        $cash_drawer->update();
        $laci->update();

        $today = new DateTime();
        $today = $today->format('Y-m-d');
        $setoran_buka = SetoranBuka::where('user_id', Auth::user()->id)->whereDate('created_at', $today)->get();

        if(sizeof($setoran_buka) == 0){
            $setoran = new SetoranBuka;
            $setoran->nominal = $log_laci->nominal;
            $setoran->user_id = Auth::user()->id;
            $setoran->operator = $log_laci->pengirim;
            $setoran->save();
        }

        return redirect('/setoran-kasir')->with('sukses', 'setuju');
    }

    public function storeGudang(Request $request){
        $log_laci = new LogLaci();
        $log_laci->penerima = $request->penerima;
        $log_laci->pengirim = Auth::user()->id;
        $log_laci->approve = 0;
        $log_laci->nominal = $request->nominal;
        $log_laci->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function storeGrosir(Request $request){
        $log_laci = new LogLaci();
        $log_laci->penerima = $request->penerima;
        $log_laci->pengirim = Auth::user()->id;
        $log_laci->approve = 0;
        $log_laci->nominal = $request->nominal;
        $log_laci->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function storeEcerGrosir(Request $request){
        $log_laci = new LogLaci();
        $log_laci->penerima = $request->penerima;
        $log_laci->pengirim = Auth::user()->id;
        $log_laci->approve = 0;
        $log_laci->nominal = $request->nominal;
        $log_laci->save();

        return redirect('/setoran-kasir')->with('sukses', 'tambah');
    }

    public function storeGudangG(Request $request){
        $log_laci = new LogLaci();
        $log_laci->penerima = $request->penerima;
        $log_laci->pengirim = Auth::user()->id;
        $log_laci->approve = 0;
        $log_laci->nominal = $request->nominal;
        $log_laci->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function storeGrosirG(Request $request){
        $log_laci = new LogLaci();
        $log_laci->penerima = $request->penerima;
        $log_laci->pengirim = Auth::user()->id;
        $log_laci->approve = 0;
        $log_laci->nominal = $request->nominal;
        $log_laci->save();

        return redirect('/laci')->with('sukses', 'tambah');
    }

    public function GudangApprove($id){
        // return $id;
        // $setoran = SetoranKasir::find($id);
        // $setoran->status = 1;
        // $setoran->operator = Auth::user()->id;
        // $log = new LogLaci();
        // $log->pengirim = $setoran->user_id;
        // $log->penerima = Auth::user()->id;
        // $log->nominal = $setoran->nominal;
        // $log->save();

        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        if($log_laci->sender->level->id == 1 || $log_laci->sender->level->id == 2){
            $laci->owner -= $log_laci->nominal;
            $laci->gudang += $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 4){
            $laci->grosir -= $log_laci->nominal;
            $laci->gudang += $log_laci->nominal;
        }
        $laci->update();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function GrosirApprove($id){
        $log_laci = LogLaci::find($id);
        $log_laci->approve = 1; 
        $log_laci->penerima = Auth::user()->id; 
        $log_laci->update();

        $laci = Laci::find(1);
        if($log_laci->sender->level->id == 1 || $log_laci->sender->level->id == 2){
            $laci->owner -= $log_laci->nominal;
            $laci->grosir += $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 5){
            $laci->gudang -= $log_laci->nominal;
            $laci->grosir += $log_laci->nominal;
        }elseif($log_laci->sender->level->id == 3){
            $cash_drawer = CashDrawer::where('user_id', $log_laci->pengirim)->first();
            $laci->grosir += $log_laci->nominal;
            $cash_drawer->nominal -= $log_laci->nominal;
            $cash_drawer->update();
        }
        $laci->update();

        return redirect('/laci')->with('sukses', 'setuju');
    }

    public function storeFromOwner(Request $request)
    {
        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $request->penerima;
        $log->approve = 0;
        $log->nominal = $request->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setor_sukses');
    }

    public function storeFromGrosir(Request $request)
    {
        $log = new LogLaci();
        $log->pengirim = Auth::user()->id;
        $log->penerima = $request->penerima;
        $log->approve = 0;
        $log->nominal = $request->nominal;
        $log->save();

        return redirect('/laci')->with('sukses', 'setor_sukses');
    }

}
