<?php

namespace App\Http\Controllers;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Akun;
use App\Util;
use App\Jurnal;

use Illuminate\Http\Request;

class JurnalController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index()
	{	
		$jurnals = Jurnal::orderBy('created_at', 'asc')->get();

		$hasil = array();
		$count_hari = 0;
		$count_bulan = 0;
		$count_tahun = 0;

		foreach ($jurnals as $i => $jurnal) {
			$created_at = substr($jurnal->created_at, 0, 10);
			$created_at = explode('-', $created_at);

			if ($i == 0) {
				$jurnal->awal_hari = true;
				$jurnal->awal_bulan = true;
				$jurnal->awal_tahun = true;
				$count_hari++;
				$count_bulan++;
				$count_tahun++;
				$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

				$count_hari++;
				$count_bulan++;
				$count_tahun++;
			} else {
				$prev_created_at = substr($jurnals[$i-1]->created_at, 0, 10);
				$prev_created_at = explode('-', $prev_created_at);

				// Tahunnya sama
				if ($created_at[0] == $prev_created_at[0]) {
					$count_tahun++;
					// Bulannya sama
					if ($created_at[1] == $prev_created_at[1]) {
						$count_bulan++;
						// Harinya sama
						if ($created_at[2] == $prev_created_at[2]) {
							if ($jurnal->referensi != $jurnals[$i - 1]->referensi) {
								$jurnal->awal_ref = true;
								$count_hari++;
								$count_bulan++;
								$count_tahun++;
							}
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
						// Harinya beda
						else {
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

							$count_hari = 0;

							$jurnal->awal_ref = true;
							$count_hari++;
							$count_bulan++;
							$count_tahun++;
							$jurnal->awal_hari = true;

							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
					}
					// Bulannya beda
					else {
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

						$count_bulan = 0;
						$count_hari = 0;

						$jurnal->awal_ref = true;
						$count_hari++;
						$count_bulan++;
						$count_tahun++;
						$jurnal->awal_hari = true;
						$jurnal->awal_bulan = true;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

						$count_hari++;
						$count_bulan++;

						if ($i == count($jurnals) - 1) {
							$hasil[$created_at[0]]['row'] = $count_tahun;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
						}
					}
				}
				// Tahunnya beda
				else {
					$hasil[$prev_created_at[0]]['row'] = $count_tahun;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

					$count_tahun = 0;
					$count_bulan = 0;
					$count_hari = 0;

					$jurnal->awal_ref = true;
					$count_hari++;
					$count_bulan++;
					$count_tahun++;
					$jurnal->awal_hari = true;
					$jurnal->awal_bulan = true;
					$jurnal->awal_tahun = true;
					$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

					$count_hari++;
					$count_bulan++;
					$count_tahun++;

					if ($i == count($jurnals) - 1) {
						$hasil[$created_at[0]]['row'] = $count_tahun;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
					}
				}
			}
		}


		$jurnals = $hasil;
		// return $jurnals;
		return view('jurnal.index', compact('jurnals'));
	}

	public function tampil(request $request)
	{
		// return $request->all();
		$awal = new DateTime($request->awal);
		$awal->add(new DateInterval('P1D'));
		$awal = $awal->format('Y-m-d');
		$akhir = new DateTime($request->akhir);
		$akhir_show = new DateTime($request->akhir);
		$akhir->add(new DateInterval('P1D'));
		$akhir = $akhir->format('Y-m-d');
		$status = 0;
		$newAwal = explode("-", $awal);
		$newAkhir = explode("-", $akhir);
		$newAwal = Carbon::create($newAwal[0], $newAwal[1], $newAwal[2]);
		$newAkhir = Carbon::create($newAkhir[0], $newAkhir[1], $newAkhir[2]);

		$jurnals = Jurnal::whereBetween('created_at', [$awal, $akhir])->orderBy('created_at', 'asc')->get();
		// return $jurnals;

		$hasil = array();
		$count_hari = 0;
		$count_bulan = 0;
		$count_tahun = 0;

		if($request->akhir == $awal){
			$status = 1;
		}

		foreach ($jurnals as $i => $jurnal) {
			$created_at = substr($jurnal->created_at, 0, 10);
			$created_at = explode('-', $created_at);

			if ($i == 0) {
				$jurnal->awal_hari = true;
				$jurnal->awal_bulan = true;
				$jurnal->awal_tahun = true;
				$count_hari++;
				$count_bulan++;
				$count_tahun++;
				$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

				$count_hari++;
				$count_bulan++;
				$count_tahun++;
			} else {
				$prev_created_at = substr($jurnals[$i-1]->created_at, 0, 10);
				$prev_created_at = explode('-', $prev_created_at);

				// Tahunnya sama
				if ($created_at[0] == $prev_created_at[0]) {
					$count_tahun++;
					// Bulannya sama
					if ($created_at[1] == $prev_created_at[1]) {
						$count_bulan++;
						// Harinya sama
						if ($created_at[2] == $prev_created_at[2]) {
							if ($jurnal->referensi != $jurnals[$i - 1]->referensi) {
								$jurnal->awal_ref = true;
								$count_hari++;
								$count_bulan++;
								$count_tahun++;
							}
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
						// Harinya beda
						else {
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

							$count_hari = 0;

							$jurnal->awal_ref = true;
							$count_hari++;
							$count_bulan++;
							$count_tahun++;
							$jurnal->awal_hari = true;

							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
					}
					// Bulannya beda
					else {
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

						$count_bulan = 0;
						$count_hari = 0;

						$jurnal->awal_ref = true;
						$count_hari++;
						$count_bulan++;
						$count_tahun++;
						$jurnal->awal_hari = true;
						$jurnal->awal_bulan = true;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

						$count_hari++;
						$count_bulan++;

						if ($i == count($jurnals) - 1) {
							$hasil[$created_at[0]]['row'] = $count_tahun;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
						}
					}
				}
				// Tahunnya beda
				else {
					$hasil[$prev_created_at[0]]['row'] = $count_tahun;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

					$count_tahun = 0;
					$count_bulan = 0;
					$count_hari = 0;

					$jurnal->awal_ref = true;
					$count_hari++;
					$count_bulan++;
					$count_tahun++;
					$jurnal->awal_hari = true;
					$jurnal->awal_bulan = true;
					$jurnal->awal_tahun = true;
					$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

					$count_hari++;
					$count_bulan++;
					$count_tahun++;

					if ($i == count($jurnals) - 1) {
						$hasil[$created_at[0]]['row'] = $count_tahun;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
					}
				}
			}
		}
		// dd($akhir_show);
		$jurnals = $hasil;
		return view('jurnal.tampil', compact('jurnals', 'newAwal', 'newAkhir', 'status', 'akhir_show'));
	}    

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$jurnal = Jurnal::where('referensi', 'like', '%JUR%')->get()->last();
		return view('jurnal.create', compact('items', 'supliers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
