<?php

namespace App\Http\Controllers;

use DateTime;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Modal;
use App\Jurnal;

use Illuminate\Http\Request;

class ModalController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$akun = Akun::where('kode',Akun::Modal)->first();
		$modals = Modal::where('nominal', '>', 0)->get();
		$banks = Bank::all();
		return view('modal.index', compact('modals', 'banks', 'akun'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request,[
			'nominal' => 'required|max:255',
			'kas_id' => 'required|max:5',
			'keterangan' => 'required|max:255'
		]);

		$akun_kas_tunai = Akun::where('kode', Akun::KasTunai)->first();
		$akun_kas_bank = Akun::where('kode', Akun::KasBank)->first();
		$akun_modal = Akun::where('kode', Akun::Modal)->first();
		$today = new DateTime();

		$modal = new Modal();
		$modal->nominal = $request->nominal;
		$modal->keterangan = $request->keterangan;
		$modal->save();

		$jenis_kas = $request->kas_id == 'tunai' ? 'tunai' : 'bank';

		if ($jenis_kas == 'tunai') {
			$akun_kas_tunai->debet += $request->nominal;
		} else if ($jenis_kas == 'bank') {
			$akun_kas_bank->debet += $request->nominal;
		}
		if($akun_modal->kredit == 0){
			$akun_modal->kredit += $request->nominal;
			$akun_modal->created_at = $today;
		}else{
			$akun_modal->kredit += $request->nominal;
		}

		$jumlah_modal = Modal::where('nominal', '>', 0)->count();
		$referensi = Util::int4digit($jumlah_modal + 1) . '/MDLT';
		$created_at = substr($modal->created_at, 0, 10);
		$created_at = explode('-', $created_at);
		$referensi .= '/' . $created_at[2] . '/' . $created_at[1] . '/' . $created_at[0];

		if ($jenis_kas == 'tunai') {
			Jurnal::create([
				'kode_akun' => $akun_kas_tunai->kode,
				'referensi' => $referensi,
				'keterangan' => 'Penambahan Modal',
				'debet' => $request->nominal
			]);
		} else if ($jenis_kas == 'bank') {
			$bank = Bank::find($request->kas_id);
			$bank->nominal += $request->nominal;
			$bank->update();

			Jurnal::create([
				'kode_akun' => $akun_kas_bank->kode,
				'referensi' => $referensi,
				'keterangan' => 'Penambahan Modal Masuk Kas Bank via - '.$bank->nama_bank,
				'debet' => $request->nominal
			]);
		}

		Jurnal::create([
			'kode_akun' => $akun_modal->kode,
			'referensi' => $referensi,
			'keterangan' => 'Penambahan Modal',
			'kredit' => $request->nominal
		]);

		Arus::create([
			'nama' => Arus::Modal,
			'nominal' => $request->nominal
		]);

		if (
			// $akun_harta_lancar->update() &&
			$akun_kas_tunai->update() &&
			$akun_kas_bank->update() &&
			$akun_modal->update() ) {
			return redirect('/modal')->with('sukses', 'tambah');
		} else {
			return redirect('/modal')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
