<?php

namespace App\Http\Controllers;

use DB;
use Auth;

use App\Akun;
use App\Bank;
use App\Item;
use App\Stok;
use App\Bonus;
use App\Harga;
use App\Jurnal;
use App\Satuan;
use App\Piutang;
use App\CashDrawer;
use App\MoneyLimit;
use App\SetoranBuka;
use App\RelasiSatuan;
use App\TransaksiPenjualan;
use App\RelasiTransaksiPenjualan;

use Illuminate\Http\Request;

class TransaksiPenjualanController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{        
		$transaksis = TransaksiPenjualan::where('status', 'eceran')->latest()->get();
		return view('transaksi_penjualan.index', compact('transaksis'));
	}

	public function lastJson()
	{
		$transaksi_penjualan = TransaksiPenjualan::all()->last();
		return response()->json(['transaksi_penjualan' => $transaksi_penjualan]);
	}

	public function itemJson($kode)
	{
		$item = Item::where('kode', $kode)->get()->first();
		$satuan = RelasiSatuan::where('item_kode', $item->kode)->orderBy('konversi')->first()->satuan;
		$harga = Harga::where('item_kode', $item->kode)
					->where('jumlah', 1)
					->where('satuan_id', $satuan->id)
					->where('status', 1)->get()->last();

		return response()->json(['item' => $item, 'harga' => $harga]);
	}

	public function hargaJson($kode, $satuan, $jumlah)
	{
		$konversi = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $satuan)->first()->konversi;
		$harga = Harga::where('item_kode', $kode)
					  ->where('jumlah', '<=', $jumlah)
					  ->where('satuan_id', $satuan)
					  ->where('status', 1)->get()->last();
		return response()->json(['harga' => $harga, 'konversi' => $konversi]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$money_limit = MoneyLimit::find(1);
		$cash = CashDrawer::where('user_id', Auth::id())->first();
		$banks = Bank::all();
		$items = Item::distinct()->select('kode', 'nama')->get();
		$satuans = Satuan::all();
		$bukaan = SetoranBuka::where('user_id', Auth::id())->first();
		
		$pembuka = ($bukaan == NULL) ? 0 : $bukaan->nominal;
		
		if (($cash->nominal - $pembuka) >=  $money_limit->nominal) {
			// return $cash->nominal;
			return redirect('/setoran-kasir')->with('gagal', 'limit');
		} else {
			return view('transaksi_penjualan.create', compact('items', 'satuans', 'banks'));
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// return $request->all();

		$data = array();
		$akuns = array();

		$laba = 0;
		$harga_beli = 0;
		$harga_jual = 0;

		$akun_aset = Akun::where('kode', Akun::Aset)->first();

		foreach ($request->item_kode as $i => $kode) {
			// Get jumlah item per pieces
			$relasi_satuan = RelasiSatuan::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->first();
			$konversi = $relasi_satuan->konversi;
			$jumlah = $request->jumlah[$i] * $konversi;

			// Get harga jual per item
			$hargas = Harga::where('item_kode', $kode)->where('satuan_id', $request->satuan_id[$i])->where('jumlah', '<=', $request->jumlah[$i])->where('status', 1)->get()->last();
			$tmp_jual = $hargas->harga * $request->jumlah[$i];
			$harga_jual += $tmp_jual;

			// Ngurangi stok
			for ($j = 0; $j < $jumlah; $j++) {
				$stoks = Stok::where('item_kode', $kode)->where('jumlah', '>', 0)->orderBy('created_at', 'asc')->first();
				$stoks->jumlah -= 1;
				$harga_beli += $stoks->harga;

				$items = Item::where('kode', $kode)->where('stoktotal', '>', 0)->first();
				$items->stoktotal -= 1;

				$stoks->update();
				$items->update();
			}

			$laba = $harga_jual - $harga_beli;

			//  Ngitung bonus
			$items = Item::where('kode', $kode)->first();
			$syarat_bonus = $items->syarat_bonus;
			$jumlah_bonus = floor($jumlah/$syarat_bonus);

			$bonuses = Bonus::find($items->bonus_id);
			$bonuses->stok -= $jumlah_bonus;
			$bonuses->update();

			$data[$kode] = [
				'harga' => $request->harga[$i],
				'subtotal' => $request->subtotal[$i],
				'jumlah' => $jumlah,
				'jumlah_bonus' => $jumlah_bonus
			];
		}

		$ppn_keluar = $harga_jual / 11;
		$netto_jual = $harga_jual - $ppn_keluar;

		$akuns = [
			0 => ['kode_akun' => Akun::KasTunai, 'nominal' => $request->harga_total],
			1 => ['kode_akun' => Akun::KasBank, 'nominal' => $request->harga_total],
			2 => ['kode_akun' => Akun::HPP, 'nominal' => -$harga_beli],
			3 => ['kode_akun' => Akun::Penjualan, 'nominal' => $netto_jual],
			4 => ['kode_akun' => Akun::PPNKeluaran, 'nominal' => $ppn_keluar],
			5 => ['kode_akun' => Akun::Persediaan, 'nominal' => -$harga_beli]
		];

		if ($request->nominal_tunai == null) $akuns[0]['nominal'] = null;
		if ($request->nominal_transfer == null) $akuns[1]['nominal'] = null;

		$transaksi_penjualan = new TransaksiPenjualan();

		$transaksi_penjualan->kode_transaksi = $request->kode_transaksi;
		$transaksi_penjualan->harga_total = $request->harga_total;
		$transaksi_penjualan->jumlah_bayar = $request->jumlah_bayar;
		$transaksi_penjualan->status = 'eceran';
		$transaksi_penjualan->user_id = Auth::id();

		if ($request->nominal_tunai != null) {
			$transaksi_penjualan->nominal_tunai = $request->harga_total;
		}

		if ($request->nominal_transfer != null) {
			$transaksi_penjualan->no_debit = $request->no_transfer;
			$transaksi_penjualan->nominal_debit = $request->harga_total;
		}

		$hutang = $request->jumlah_bayar - $request->harga_total;

		$cash = CashDrawer::where('user_id', Auth::id())->first();
		$cash->nominal += $request->harga_total;

		if ($transaksi_penjualan->save() && $cash->update()) {
			$transaksi = TransaksiPenjualan::all()->last();
			$transaksi_id = $transaksi->id;

			foreach ($request->item_kode as $i => $kode) {
				$relasi_transaksi_penjualan = new RelasiTransaksiPenjualan();
				$relasi_transaksi_penjualan->item_kode = $kode;
				$relasi_transaksi_penjualan->transaksi_penjualan_id = $transaksi_id;
				$relasi_transaksi_penjualan->jumlah = $data[$kode]['jumlah'];
				$relasi_transaksi_penjualan->jumlah_bonus = $data[$kode]['jumlah_bonus'];
				$relasi_transaksi_penjualan->harga = $data[$kode]['harga'];
				$relasi_transaksi_penjualan->subtotal = $data[$kode]['subtotal'];

				$relasi_transaksi_penjualan->save();
			}

			for ($i = 0; $i < count($akuns); $i++) {
				if ($akuns[$i]['nominal'] != null) {
					$jurnal = new Jurnal();
					$akun = Akun::where('kode', $akuns[$i]['kode_akun'])->first();

					$jurnal->kode_akun = $akuns[$i]['kode_akun'];
					$jurnal->referensi = $request->kode_transaksi;
					$jurnal->keterangan = 'Transaksi Eceran';

					$kode = substr($akuns[$i]['kode_akun'], 0, 1);
					if ($kode == 1 || $kode == 5) {
						if ($akuns[$i]['nominal'] > 0) {
							$jurnal->debet = abs($akuns[$i]['nominal']);
							$akun->debet += abs($request->harga_total);
							$akun_aset->debet += abs($akuns[$i]['nominal']);
						} else {
							$jurnal->kredit = abs($akuns[$i]['nominal']);
							$akun->kredit += abs($request->harga_total);
							$akun_aset->kredit += abs($akuns[$i]['nominal']);
						}
						$akun_aset->update();
					} else {
						if ($akuns[$i]['nominal'] > 0) {
							$jurnal->kredit = abs($akuns[$i]['nominal']);
							$akun->kredit += abs($request->harga_total);
						} else {
							$jurnal->debet = abs($akuns[$i]['nominal']);
							$akun->debet += abs($request->harga_total);
						}
					}

					$jurnal->save();
					$akun->update();
				}
			}

			if (!empty($request->bank)) {
				$banks = Bank::find($request->bank);
				$banks->nominal += intval($request->nominal_transfer);
				$banks->update();
			}

			// return redirect('/transaksi')->with('sukses', 'tambah');
			return redirect('/transaksi/create')->with('sukses', 'tambah')->with('data', [
				'total' => $request->harga_total,
				'tunai' => $request->jumlah_bayar,
				'kembalian' => $request->jumlah_bayar - $request->harga_total
			]);
		} else {
			return redirect('/transaksi/create')->with('gagal', 'tambah');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) 
	{
		$transaksi_penjualan = TransaksiPenjualan::find($id);
		$relasi_transaksi_penjualan = RelasiTransaksiPenjualan::where('transaksi_penjualan_id', $id)->get();

		return view('transaksi_penjualan.show', compact('relasi_transaksi_penjualan', 'transaksi_penjualan'));
	}

	public function piutang($id, Request $request) 
	{
		$transaksi_penjualan = TransaksiPenjualan::find($id);
		$piutang = new Piutang();

		$piutang->transaksi_penjualan_id = $transaksi_penjualan->id;
		$piutang->user_id = Auth::id();

		$piutang->no_debit = $request->no_debit;
		$piutang->no_cek = $request->no_cek;
		$piutang->no_bg = $request->no_bg;

		$piutang->nominal_tunai = $request->nominal_tunai;
		$piutang->nominal_debit = $request->nominal_debit;
		$piutang->nominal_cek = $request->nominal_cek;
		$piutang->nominal_bg = $request->nominal_bg;

		$transaksi_penjualan->sisa_hutang = $request->sisa_hutang - $request->jumlah_bayar;

		if ($transaksi_penjualan->sisa_hutang == 0) $transaksi_penjualan->status_hutang = 0;

		if ($transaksi_penjualan->save()) {
			$piutang->save();
			return redirect('/transaksi/'.$id)->with('sukses', 'ubah');
		} else {
			return redirect('/transaksi/'.$id)->with('gagal', 'ubah');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
