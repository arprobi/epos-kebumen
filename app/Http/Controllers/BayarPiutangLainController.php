<?php

namespace App\Http\Controllers;

use App;
use App\Akun;
use App\Arus;
use App\Bank;
use App\Jurnal;
use App\PiutangLain;
use App\BayarPiutangLain;

use Illuminate\Http\Request;

class BayarPiutangLainController extends Controller
{
    public function lastJson()
    {
        $bayar_piutang_lain = BayarPiutangLain::all()->last();
        return response()->json(['bayar_piutang_lain' => $bayar_piutang_lain]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'piutang_lain_id' => 'required|max:255',
            'nominal' => 'required|max:255',
            'kas' => 'required|max:255',
            'karyawan' => 'required|max:255',
        ]);

        $bayar_piutang_lain = new BayarPiutangLain();
        $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
        $akun_bank = Akun::where('kode', Akun::KasBank)->first();
        $piutang_lain = PiutangLain::where('id', $request->piutang_lain_id)->first();

        // return $piutang_lain;
        $bayar_piutang_lain->piutang_lain_id = $request->piutang_lain_id;
        $bayar_piutang_lain->nominal = $request->nominal;
        $bayar_piutang_lain->no_transfer = $request->no_transfer;
        $bayar_piutang_lain->kode_transaksi = $request->kode_transaksi;

        $piutang_lain->sisa = $piutang_lain->sisa - $request->nominal;
        $piutang_lain->update();

        if ($request->kas == 'tunai') {
            $akun_tunai->debet = $akun_tunai->debet + $request->nominal;
            $akun_tunai->update();

            $bayar_piutang_lain->metode_pembayaran = 'tunai';

            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->nama.' - '.$request->keterangan;
            $tunai->debet = $request->nominal;
            $tunai->save();

            Arus::create([
                'nama' => Arus::PembayaranPiutang,
                'nominal' => $request->nominal,
            ]);

            if ($request->karyawan ==1 ) {
                $piutang_karyawan = new Jurnal();
                $piutang_karyawan->kode_akun = Akun::PiutangKaryawan;
                $piutang_karyawan->referensi = $request->kode_transaksi;
                $piutang_karyawan->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_karyawan->kredit = $request->nominal;
                
                $piutang_karyawan->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangKaryawan)->first();
                $akun_piutang->debet -= $request->nominal;
                $akun_piutang->update();
            } else {
                $piutang_lain = new Jurnal();
                $piutang_lain->kode_akun = Akun::PiutangLain;
                $piutang_lain->referensi = $request->kode_transaksi;
                $piutang_lain->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_lain->kredit = $request->nominal;
                
                $piutang_lain->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangLain)->first();
                $akun_piutang->debet -= $request->nominal;
                $akun_piutang->update();
            }
        } elseif($request->kas == 'bank') {
            $akun_bank->debet = $akun_bank->debet + $request->nominal;
            $akun_bank->update();

            $bayar_piutang_lain->metode_pembayaran = 'debit';

            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal + $request->nominal;
            $bank->update();

            Arus::create([
                'nama' => Arus::PembayaranPiutang,
                'nominal' => $request->nominal,
            ]);

            $j_bank = new Jurnal();
            $j_bank->kode_akun = Akun::KasBank;
            $j_bank->referensi = $request->kode_transaksi;
            $j_bank->keterangan = $request->nama.' -' .$request->keterangan;;
            $j_bank->debet = $request->nominal;
            $j_bank->save();

            if ($request->karyawan == 1) {
                $piutang_karyawan = new Jurnal();
                $piutang_karyawan->kode_akun = Akun::PiutangKaryawan;
                $piutang_karyawan->referensi = $request->kode_transaksi;
                $piutang_karyawan->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_karyawan->kredit = $request->nominal;
                $piutang_karyawan->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangKaryawan)->first();
                $akun_piutang->debet -= $request->nominal;
                $akun_piutang->update();
            } else {
                $piutang_lain = new Jurnal();
                $piutang_lain->kode_akun = Akun::PiutangLain;
                $piutang_lain->referensi = $request->kode_transaksi;
                $piutang_lain->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_lain->kredit = $request->nominal;
                $piutang_lain->save();

                $akun_piutang = Akun::where('kode', Akun::PiutangLain)->first();
                $akun_piutang->debet -= $request->nominal;
                $akun_piutang->update();
            }
        }elseif($request->kas == 'gaji'){
            $akun_tunai->debet = $akun_tunai->debet + $request->nominal;
            $akun_tunai->update();

            $bayar_piutang_lain->metode_pembayaran = 'tunai';

            $tunai = new Jurnal();
            $tunai->kode_akun = Akun::KasTunai;
            $tunai->referensi = $request->kode_transaksi;
            $tunai->keterangan = $request->nama.' - '.$request->keterangan;
            $tunai->debet = $request->nominal;

            Arus::create([
                'nama' => Arus::PembayaranPiutang,
                'nominal' => $request->nominal,
            ]);

            if ($request->karyawan ==1 ) {
                $piutang_karyawan = new Jurnal();
                $piutang_karyawan->kode_akun = Akun::PiutangKaryawan;
                $piutang_karyawan->referensi = $request->kode_transaksi;
                $piutang_karyawan->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_karyawan->kredit = $request->nominal;
                
                $piutang_karyawan->save();
                $tunai->save();
            } else {
                $piutang_lain = new Jurnal();
                $piutang_lain->kode_akun = Akun::PiutangLain;
                $piutang_lain->referensi = $request->kode_transaksi;
                $piutang_lain->keterangan = $request->nama.' -' .$request->keterangan;;
                $piutang_lain->kredit = $request->nominal;
                
                $piutang_lain->save();
                $tunai->save();
            }
        }

        if ($bayar_piutang_lain->save()) {
            return redirect('/piutang_lain/show/'.$request->piutang_lain_id)->with('sukses', 'tambah');
        } else {
            return redirect('/piutang_lain/show/'.$request->piutang_lain_id)->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
