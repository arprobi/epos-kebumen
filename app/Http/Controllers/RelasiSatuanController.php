<?php

namespace App\Http\Controllers;

use App\Item;
use App\Harga;
use App\RelasiBonus;
use App\RelasiSatuan;

use Illuminate\Http\Request;

class RelasiSatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        // $this->validate($request,[
        //     'item_kode' => 'required|max:255',
        //     'satuanID' => 'required|max:255',
        //     'konversi' => 'required|max:255'
        // ]);

        $item = Item::where('kode', $request->item_kode)->first();
        $kode_barang = $item->kode_barang;
        $items = Item::where('kode_barang', $item->kode_barang)->distinct()->get(['kode']);
        $new_relasi_count = 0;
        foreach ($items as $i => $item) {
            $relasi_satuan = new RelasiSatuan();
            $relasi_satuan->item_kode = $item->kode;
            $relasi_satuan->satuan_id = $request->satuanID;
            $relasi_satuan->konversi = $request->konversi;
            $relasi_satuan->save();

            $relasi_harga = new Harga();
            $relasi_harga->item_kode = $item->kode;
            $relasi_harga->jumlah = 1;
            $relasi_harga->satuan_id = $request->satuanID;
            $relasi_harga->eceran = 0;
            $relasi_harga->grosir = 0;
            $relasi_harga->save();

            $new_relasi_count++;
        }

        ItemController::resetHargas($kode_barang);

        if ($new_relasi_count == count($items)) {
            return redirect()->back()->with('sukses', 'tambah_satuan');
        } else {
            return redirect()->back()->with('gagal', 'tambah_satuan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RelasiSatuan  $relasiSatuan
     * @return \Illuminate\Http\Response
     */
    public function show(RelasiSatuan $relasiSatuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RelasiSatuan  $relasiSatuan
     * @return \Illuminate\Http\Response
     */
    public function edit(RelasiSatuan $relasiSatuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RelasiSatuan  $relasiSatuan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $relasi_satuan = RelasiSatuan::find($id);
        $satuan_id = $relasi_satuan->satuan_id;
        $item = $relasi_satuan->item;
        $items = Item::where('kode_barang', $item->kode_barang)->distinct()->get(['kode']);
        $new_relasi_count = 0;
        foreach ($items as $i => $item) {
            $relasi_satuan = RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $satuan_id)->first();
            $relasi_satuan->item_kode = $item->kode;
            $relasi_satuan->satuan_id = $request->satuanID;
            $relasi_satuan->konversi = $request->konversi;
            $relasi_satuan->update();

            $harga = Harga::where('item_kode', $item->kode)->where('satuan_id', $satuan_id)->first();
            $harga->satuan_id = $request->satuanID;
            $harga->update();

            $new_relasi_count++;
        }

        if ($new_relasi_count == count($items)) {
            return redirect()->back()->with('sukses', 'ubah');
        } else {
            return redirect()->back()->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RelasiSatuan  $relasiSatuan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        $relasi_satuan = RelasiSatuan::find($id);
        $relasi_bonus = RelasiBonus::where('item_kode', $relasi_satuan->item_kode)->get();

        if ($relasi_bonus->count() > 0) {
            return redirect()->back()->with('gagal', 'hapus');
        }

        $item = $relasi_satuan->item;
        $items = Item::where('kode_barang', $item->kode_barang)->distinct()->get(['kode']);
        $deleted_relasi_count = 0;
        foreach ($items as $i => $item) {
            Harga::where('item_kode', $item->kode)->where('satuan_id', $relasi_satuan->satuan_id)->delete();
            RelasiSatuan::where('item_kode', $item->kode)->where('satuan_id', $relasi_satuan->satuan_id)->delete();

            $deleted_relasi_count++;
        }

        if ($deleted_relasi_count == count($items)) {
            return redirect()->back()->with('sukses', 'hapus');
        } else {
            return redirect()->back()->with('gagal', 'hapus');
        }
    }
}
