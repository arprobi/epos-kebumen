<?php

namespace App\Http\Controllers;

use App\Item;
use App\Util;
use App\Suplier;

use Illuminate\Http\Request;

class SuplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index() {
        $supliers = Suplier::where('aktif', 1)->get();
        $suplier_off = Suplier::where('aktif', 0)->get();
        return view('suplier.index', compact('supliers', 'suplier_off'));
    } */

    public function index()
    {
        return view('suplier.index');
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'supliers.' . $field;

        $supliers = Suplier
            ::select(
                'supliers.id',
                'supliers.nama',
                'supliers.alamat',
                'supliers.telepon',
                'supliers.email',
                'supliers.piutang'
            )
            ->where('supliers.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('supliers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.piutang', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Suplier
            ::select(
                'supliers.id'
            )
            ->where('supliers.aktif', 1)
            ->where(function($query) use ($request) {
                $query
                ->where('supliers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.piutang', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($supliers as $i => $suplier) {

            $suplier->piutang = Util::ewon($suplier->piutang);

            if ($suplier->alamat == null) $suplier->alamat = ' - ';
            if ($suplier->telepon == null) $suplier->telepon = ' - ';
            if ($suplier->email == null) $suplier->email = ' - ';

            $buttons['ubah'] = ['url' => ''];
            $buttons['nonaktifkan'] = ['url' => ''];

            // return $buttons;
            $suplier->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $supliers,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'supliers.' . $field;

        $supliers = Suplier
            ::select(
                'supliers.id',
                'supliers.nama',
                'supliers.alamat',
                'supliers.telepon',
                'supliers.email',
                'supliers.piutang'
            )
            ->where('supliers.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('supliers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.piutang', 'like', '%'.$request->search_query.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Suplier
            ::select(
                'supliers.id'
            )
            ->where('supliers.aktif', 0)
            ->where(function($query) use ($request) {
                $query
                ->where('supliers.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.alamat', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.telepon', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.email', 'like', '%'.$request->search_query.'%')
                ->orWhere('supliers.piutang', 'like', '%'.$request->search_query.'%');
            })
            ->count();

        foreach ($supliers as $i => $suplier) {

            if ($suplier->alamat == null) $suplier->alamat = ' - ';
            if ($suplier->telepon == null) $suplier->telepon = ' - ';
            if ($suplier->email == null) $suplier->email = ' - ';

            $buttons['aktifkan'] = ['url' => ''];

            // return $buttons;
            $suplier->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $request->field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $supliers,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $suplier = new Suplier();
        $suplier->nama = $request->nama;
        $suplier->telepon = $request->telepon;
        $suplier->email = $request->email;
        $suplier->alamat = $request->alamat;
        if ($suplier->save()) {
            return redirect('/suplier')->with('sukses', 'tambah');
        } else {
            return redirect('/suplier')->with('gagal', 'tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showJson($id)
    {
        $suplier = Suplier::find($id);
        return $suplier->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required|max:255',
        ]);

        $suplier = Suplier::find($id);
        $suplier->nama = $request->nama;
        $suplier->telepon = $request->telepon;
        $suplier->email = $request->email;
        $suplier->alamat = $request->alamat;
        if ($suplier->save()) {
            return redirect('/suplier')->with('sukses', 'ubah');
        } else {
            return redirect('/suplier')->with('gagal', 'ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // try {
        //     $suplier = Suplier::find($id);
        //     if ($suplier->delete()) {
        //         return redirect('/suplier')->with('sukses', 'hapus');
        //     } else {
        //         return redirect('/suplier')->with('gagal', 'hapus');
        //     }
        // } catch(\Illuminate\Database\QueryException $e) {
        //     return redirect('/suplier')->with('gagal', 'hapus');
        // }

        $items = Item::where('suplier_id', $id)->where('aktif', 1)->get();
        if (count($items) > 0) {
            return redirect('/suplier')->with('gagal', 'hapus');
        }

        $status = 0;
        try {
            $suplier = Suplier::find($id);
            $suplier->aktif = 0;
            $suplier->update();
            $status = 1;
        } catch(\Illuminate\Database\QueryException $e) {
            return redirect('/suplier')->with('gagal', 'hapus');
        }

        if($status == 1){
            return redirect('/suplier')->with('sukses', 'hapus');
        }else{
            return redirect('/suplier')->with('gagal', 'hapus');
        }
    }

    public function aktif($id) {
        $suplier = Suplier::find($id);
        $suplier->aktif = 1;
        $suplier->update();
        
        return redirect('/suplier')->with('sukses', 'aktif');
    }
}
