<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Akun;
use App\Util;
use App\Jurnal;
use App\Paralaba;
use App\JurnalPenutup;

class JurnalPenutupController extends Controller
{
    public function index(){
        Util::akun();
        $akun['Pendapatan'] = Akun::where('kode', Akun::Pendapatan)->first();
		$akun['Beban'] = Akun::where('kode', Akun::Beban)->first();

		$akun['BebanIklan'] = Akun::where('kode', Akun::BebanIklan)->first();
		$akun['BebanSewa'] = Akun::where('kode', Akun::BebanSewa)->first();
		$akun['BebanPerlengkapan'] = Akun::where('kode', Akun::BebanPerlengkapan)->first();
		$akun['BebanPerawatan'] = Akun::where('kode', Akun::BebanPerawatan)->first();
		$akun['BebanKerugianPiutang'] = Akun::where('kode', Akun::BebanKerugianPiutang)->first();
		$akun['BebanKerugianPersediaan'] = Akun::where('kode', Akun::BebanKerugianPersediaan)->first();
		$akun['BebanRugiAset'] = Akun::where('kode', Akun::BebanRugiAset)->first();
		$akun['BebanDepBangunan'] = Akun::where('kode', Akun::BebanDepBangunan)->first();
		$akun['BebanDepPeralatan'] = Akun::where('kode', Akun::BebanDepPeralatan)->first();
		$akun['BebanDepKendaraan'] = Akun::where('kode', Akun::BebanDepKendaraan)->first();
		$akun['BebanAsuransi'] = Akun::where('kode', Akun::BebanAsuransi)->first();
		$akun['BebanGaji'] = Akun::where('kode', Akun::BebanGaji)->first();
		$akun['BebanAdministrasiBank'] = Akun::where('kode', Akun::BebanAdministrasiBank)->first();
		$akun['BebanDenda'] = Akun::where('kode', Akun::BebanDenda)->first();
		$akun['BebanUtilitas'] = Akun::where('kode', Akun::BebanUtilitas)->first();
		$akun['BebanOngkir'] = Akun::where('kode', Akun::BebanOngkir)->first();
		$akun['BebanTransportasi'] = Akun::where('kode', Akun::BebanTransportasi)->first();
		$akun['BebanGaransi'] = Akun::where('kode', Akun::BebanGaransi)->first();
		$akun['BebanPajak'] = Akun::where('kode', Akun::BebanPajak)->first();
		$akun['BebanLain'] = Akun::where('kode', Akun::BebanLain)->first();
		
		$akun['LabaTahan'] = Akun::where('kode', Akun::LabaTahan)->first();
		$akun['BagiHasil'] = Akun::where('kode', Akun::BagiHasil)->first();


		$akun['LabaRugi'] = Akun::where('kode', Akun::LabaRugi)->first();

		$selisih = $akun['Pendapatan']->kredit - $akun['Beban']->debet;

    	$cek_laba = Paralaba::get()->last();
		if($cek_laba == NULL){
			$cek_penjualan = Jurnal::where('kode_akun', Akun::Penjualan)->orWhere('kode_akun', Akun::PenjualanKredit)->first();
			if($cek_penjualan == NULL){
				$cek_akun = Akun::where('kode', Akun::Pendapatan)->first();
				$start = ($cek_akun->updated_at)->format('d m Y');
				$end_str = $cek_akun->updated_at;
			}else{
				$start = ($cek_penjualan->created_at)->format('d m Y');
				$end_str = $cek_penjualan->created_at;
			}
		}else{
			$end_str = new DateTime(date($cek_laba->akhir));
			$end_str->add(new DateInterval('P1D'));
			$start = $end_str->format('d m Y');
		}
		
		$akhir = new DateTime();
		$x = $akhir->format('d m Y');

		$status = 0;
		if($x == $start){
			$status = 1;
		}
		
		$jurnals = JurnalPenutup::all();

		$hasil = array();
		$count_hari = 0;
		$count_bulan = 0;
		$count_tahun = 0;

		foreach ($jurnals as $i => $jurnal) {
			$created_at = substr($jurnal->created_at, 0, 10);
			$created_at = explode('-', $created_at);

			if ($i == 0) {
				$jurnal->awal_hari = true;
				$jurnal->awal_bulan = true;
				$jurnal->awal_tahun = true;
				$count_hari++;
				$count_bulan++;
				$count_tahun++;
				$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

				$count_hari++;
				$count_bulan++;
				$count_tahun++;
			} else {
				$prev_created_at = substr($jurnals[$i-1]->created_at, 0, 10);
				$prev_created_at = explode('-', $prev_created_at);

				// Tahunnya sama
				if ($created_at[0] == $prev_created_at[0]) {
					$count_tahun++;
					// Bulannya sama
					if ($created_at[1] == $prev_created_at[1]) {
						$count_bulan++;
						// Harinya sama
						if ($created_at[2] == $prev_created_at[2]) {
							if ($jurnal->referensi != $jurnals[$i - 1]->referensi) {
								$jurnal->awal_ref = true;
								$count_hari++;
								$count_bulan++;
								$count_tahun++;
							}
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
						// Harinya beda
						else {
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

							$count_hari = 0;

							$jurnal->awal_ref = true;
							$count_hari++;
							$count_bulan++;
							$count_tahun++;
							$jurnal->awal_hari = true;

							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

							$count_hari++;

							if ($i == count($jurnals) - 1) {
								$hasil[$created_at[0]]['row'] = $count_tahun;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
								$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
							}
						}
					}
					// Bulannya beda
					else {
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

						$count_bulan = 0;
						$count_hari = 0;

						$jurnal->awal_ref = true;
						$count_hari++;
						$count_bulan++;
						$count_tahun++;
						$jurnal->awal_hari = true;
						$jurnal->awal_bulan = true;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

						$count_hari++;
						$count_bulan++;

						if ($i == count($jurnals) - 1) {
							$hasil[$created_at[0]]['row'] = $count_tahun;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
							$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
						}
					}
				}
				// Tahunnya beda
				else {
					$hasil[$prev_created_at[0]]['row'] = $count_tahun;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['row'] = $count_bulan;
					$hasil[$prev_created_at[0]]['isi'][$prev_created_at[1]]['isi'][$prev_created_at[2]]['row'] = $count_hari;

					$count_tahun = 0;
					$count_bulan = 0;
					$count_hari = 0;

					$jurnal->awal_ref = true;
					$count_hari++;
					$count_bulan++;
					$count_tahun++;
					$jurnal->awal_hari = true;
					$jurnal->awal_bulan = true;
					$jurnal->awal_tahun = true;
					$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['isi'][$count_hari] = $jurnal;

					$count_hari++;
					$count_bulan++;
					$count_tahun++;

					if ($i == count($jurnals) - 1) {
						$hasil[$created_at[0]]['row'] = $count_tahun;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['row'] = $count_bulan;
						$hasil[$created_at[0]]['isi'][$created_at[1]]['isi'][$created_at[2]]['row'] = $count_hari;
					}
				}
			}
		}

		$jurnals = $hasil;

		return view('jurnal_penutup.index', compact('akhir', 'data', 'start', 'selisih', 'akun', 'status', 'end_str', 'jurnals'));
    }

    public function store(Request $request){
    	// return $request->all();    	
		$akun_jurnal_pendapatan = Akun::where('kode', Akun::Pendapatan)->first();
		$akun_jurnal_laba_rugi = Akun::where('kode', Akun::LabaRugi)->first();
		$akun_jurnal_beban = Akun::where('kode', Akun::Beban)->first();
		$akun_jurnal_bebans = Akun::whereNotIn('kode', [Akun::Beban])->where('kode', 'like', '5%')->get();

		$jurnal_pendapatan = new JurnalPenutup();
		$jurnal_pendapatan->kode_akun = $akun_jurnal_pendapatan->kode;
		$jurnal_pendapatan->referensi = 'JPP/'.$request->akhir;
		$jurnal_pendapatan->debet = $akun_jurnal_pendapatan->kredit;
		$jurnal_pendapatan->save();

		$jurnal_LR = new JurnalPenutup();
		$jurnal_LR->kode_akun = $akun_jurnal_laba_rugi->kode;
		$jurnal_LR->referensi = 'JPP/'.$request->akhir;
		$jurnal_LR->kredit = $akun_jurnal_pendapatan->kredit;
		$jurnal_LR->save();


		$jurnal_LRB = new JurnalPenutup();
		$jurnal_LRB->kode_akun = $akun_jurnal_laba_rugi->kode;
		$jurnal_LRB->referensi = 'JPB/'.$request->akhir;
		$jurnal_LRB->debet = $akun_jurnal_beban->debet;
		$jurnal_LRB->save();

		foreach ($akun_jurnal_bebans as $beban) {
			$jurnal_Beban = new JurnalPenutup();
			$jurnal_Beban->kode_akun = $beban->kode;
			$jurnal_Beban->referensi = 'JPB/'.$request->akhir;
			$jurnal_Beban->kredit = $beban->debet;
			$jurnal_Beban->save();			
		}

		$jurnal_ILR = new JurnalPenutup();
		$jurnal_ILR->kode_akun = $akun_jurnal_laba_rugi->kode;
		$jurnal_ILR->referensi = 'JPLB/'.$request->akhir;
		$jurnal_ILR->debet = $request->selisih;
		$jurnal_ILR->save();

		if($request->laba_tahan > 0){
			$jurnal_LB = new JurnalPenutup();
			$jurnal_LB->kode_akun = Akun::LabaTahan;
			$jurnal_LB->referensi = 'JPLB/'.$request->akhir;
			$jurnal_LB->kredit = $request->laba_tahan;
			$jurnal_LB->save();
		}
		
		if($request->bagi_hasil > 0){
			$jurnal_BH = new JurnalPenutup();
			$jurnal_BH->kode_akun = Akun::BagiHasil;
			$jurnal_BH->referensi = 'JPLB/'.$request->akhir;
			$jurnal_BH->kredit = $request->bagi_hasil;
			$jurnal_BH->save();
		}

		$akuns = Akun::where('kode', 'like', '4%')->orWhere('kode', 'like', '5%')->get();
    	foreach ($akuns as $akun) {
    		$akun->debet = 0;
    		$akun->kredit = 0;
    		$akun->update();
    	}

    	$akun_laba_tahan = Akun::where('kode', Akun::LabaTahan)->first();
    	$akun_bagi_hasil = Akun::where('kode', Akun::BagiHasil)->first();

    	$akun_laba_tahan->kredit += $request->laba_tahan;
    	$akun_bagi_hasil->kredit += $request->bagi_hasil;

		$akun_laba_tahan->update();
    	$akun_bagi_hasil->update();

    	$paralaba = new Paralaba();
    	$paralaba->awal = $request->awal;
    	$paralaba->akhir = $request->akhir;
    	$paralaba->save();
		
		return redirect('/jurnal_penutup')->with('berhasil', 'tambah');
    }
}
