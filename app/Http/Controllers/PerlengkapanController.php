<?php

namespace App\Http\Controllers;

use App\Akun;
use App\Arus;
use App\Bank;
use App\Util;
use App\Jurnal;
use App\Perkap;
use Illuminate\Http\Request;

class PerlengkapanController extends Controller
{
    public function lastJson()
    {
        $perkap = Perkap::where('kode', 'like', '%KMK/PR%')->get()->last();
        return response()->json(['perkap' => $perkap]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $perlengkapans = Perkap::where('umur', '=', 0)->where('status', '=', 1)->get();
        $banks = Bank::all();
        $perlengkapan_offs = Perkap::where('umur', '=', 0)->where('status', '=', 0)->get();
        return view('perlengkapan.index', compact('perlengkapans', 'banks', 'perlengkapan_offs'));
    } */

    public function index()
    {
        $banks = Bank::all();
        return view('perlengkapan.index', compact('banks'));
    }

    public function mdt1(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'perkaps.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $perlengkapans = Perkap
            ::select(
                'perkaps.id',
                'perkaps.kode',
                'perkaps.no_transaksi',
                'perkaps.nama',
                'perkaps.harga',
                'perkaps.keterangan',
                'perkaps.created_at'
            )
            ->where('perkaps.umur', 0)
            ->where('perkaps.status', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Perkap
            ::select(
                'perkaps.id'
            )
            ->where('perkaps.umur', 0)
            ->where('perkaps.status', 1)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%');
            })
            ->count();

        foreach ($perlengkapans as $i => $perlengkapan) {

            $perlengkapan->harga = Util::ewon($perlengkapan->harga);

            if ($perlengkapan->keterangan == null) {
                $perlengkapan->keterangan = ' - ';
            }

            $buttons['hapus'] = ['url' => ''];

            // return $buttons;
            $perlengkapan->buttons = $buttons;

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $perlengkapans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function mdt2(Request $request)
    {
        $field = $request->field;
        $order = $request->order;

        if ($request->field == 'created_at') {

            if ($request->order == 'asc') {
                $order = 'desc';
            }
            else if ($request->order == 'desc') {
                $order = 'asc';
            }

        }
        else if ($request->field == '') {

            $field = 'created_at';
            $order = 'desc';

        }

        $field = 'perkaps.' . $field;

        $tanggal = '';
        if (count(explode('-', $request->search_query)) >= 3) {
            $tanggal = Util::date(explode(' ', $request->search_query)[0]);
        }

        if ($tanggal == '') {
            $tanggal = '0000-00-00';
        }

        $perlengkapans = Perkap
            ::select(
                'perkaps.id',
                'perkaps.kode',
                'perkaps.no_transaksi',
                'perkaps.nama',
                'perkaps.harga',
                'perkaps.keterangan',
                'perkaps.created_at'
            )
            ->where('perkaps.umur', 0)
            ->where('perkaps.status', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%');
            })
            ->limit($request->data_per_halaman)
            ->offset(($request->halaman_sekarang - 1) * $request->data_per_halaman)
            ->orderBy($field, $order)
            ->get();

        $count = Perkap
            ::select(
                'perkaps.id'
            )
            ->where('perkaps.umur', 0)
            ->where('perkaps.status', 0)
            ->where(function($query) use ($request, $tanggal) {
                $query
                ->where('perkaps.kode', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.no_transaksi', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.nama', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.harga', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.keterangan', 'like', '%'.$request->search_query.'%')
                ->orWhere('perkaps.created_at', 'like', '%'.$tanggal.'%');
            })
            ->count();

        foreach ($perlengkapans as $i => $perlengkapan) {

            $perlengkapan->harga = Util::ewon($perlengkapan->harga);

            if ($perlengkapan->keterangan == null) {
                $perlengkapan->keterangan = ' - ';
            }

        }

        $typing = $request->typing == 'true' ? true : false;
        $first = $request->first == 'true' ? true : false;

        $inverse = $order == 'desc' ? true : false;
        $inverse = $field == 'created_at' ? !$inverse : $inverse;
        $inverse = $first ? false : $inverse;

        return response()->json([
            'data_per_halaman' => $request->data_per_halaman,
            'search_query' => $request->search_query,
            'data_total' => $count,
            'halaman_sekarang' => $request->halaman_sekarang,
            'data' => $perlengkapans,
            'typing' => $typing,
            'inverse' => $inverse,
        ]);
    }

    public function off()
    {
        $perlengkapans = Perkap::where('umur', '=', 0)->where('status', '=', 0)->get();
        // return $perlengkapans;
        return view('perlengkapan.off', compact('perlengkapans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $kode = explode("/", $request->kode);
        $mm = explode("/", $request->kode)[3];
        $yy = explode("/", $request->kode)[4];
        $bulan = $mm.'/'.$yy;
        for($i=1; $i<=$request->jumlah; $i++){
            if ($i==1) {
                $code = Util::int6digit(intval($kode[0]));
                $kode_barang = $code.'/KMK/PR/'.$bulan;
                // echo $kode_barang;
                $perlengkapan = new Perkap();
                $perlengkapan->kode = $kode_barang;
                $perlengkapan->nama = $request->nama;
                $perlengkapan->harga = $request->harga;
                $perlengkapan->umur = $request->umur;
                $perlengkapan->status = $request->status;
                $perlengkapan->no_transaksi = $request->no_transaksi;
                $perlengkapan->keterangan = $request->keterangan;

                $perlengkapan->save();
            }else{
                $code = Util::int6digit(intval($kode[0]) + ($i-1));
                $kode_barang = $code.'/KMK/PR/'.$bulan;
                // echo $kode_barang; 
                $perlengkapan = new Perkap();
                $perlengkapan->kode = $kode_barang;
                $perlengkapan->nama = $request->nama;
                $perlengkapan->harga = $request->harga;
                $perlengkapan->umur = $request->umur;
                $perlengkapan->status = $request->status;
                $perlengkapan->no_transaksi = $request->no_transaksi;
                $perlengkapan->keterangan = $request->keterangan;

                $perlengkapan->save();                   
            }
        }
        $jurnal_perlengkapan = new Jurnal();
        $jurnal_perlengkapan->kode_akun = Akun::Perlengkapan;
        $jurnal_perlengkapan->referensi = $request->kode;
        $jurnal_perlengkapan->debet = ($request->harga * $request->jumlah);
        $jurnal_perlengkapan->keterangan = 'pembelian '.$request->keterangan;
        $jurnal_perlengkapan->save();

        $akun_perlengkapan = Akun::where('kode', Akun::Perlengkapan)->first();
        $akun_perlengkapan->debet = $akun_perlengkapan->debet + ($request->harga * $request->jumlah);
        $akun_perlengkapan->update();

        if($request->kas=='tunai'){
            //update akun tunai
            $akun_tunai = Akun::where('kode', Akun::KasTunai)->first();
            $akun_tunai->debet = $akun_tunai->debet - ($request->harga * $request->jumlah);
            $akun_tunai->update();
            //jurnal tunai
            $jurnal_tunai = new Jurnal();
            $jurnal_tunai->kode_akun = Akun::KasTunai;
            $jurnal_tunai->referensi = $request->kode;
            $jurnal_tunai->kredit = ($request->harga * $request->jumlah);
            $jurnal_tunai->keterangan = 'pembelian '.$request->keterangan;
            $jurnal_tunai->save();
        }else{
            //update akun bank
            $akun_bank = Akun::where('kode', Akun::KasBank)->first();
            $akun_bank->debet = $akun_bank->debet - ($request->harga * $request->jumlah);
            $akun_bank->update();
            //update bank harga
            $bank = Bank::where('id', $request->bank)->first();
            $bank->nominal = $bank->nominal - ($request->harga * $request->jumlah);
            $bank->update();
            //jurnal akun bank
            $jurnal_bank = new Jurnal();
            $jurnal_bank->kode_akun = Akun::KasBank;
            $jurnal_bank->referensi = $request->kode;
            $jurnal_bank->kredit = ($request->harga * $request->jumlah);
            $jurnal_bank->keterangan = 'pembelian '.$request->keterangan;
            $jurnal_bank->save();
        }

        Arus::create([
            'nama' => Arus::BeliAset,
            'nominal' => $request->harga * $request->jumlah
        ]);
        
        return redirect('/perlengkapan')->with('sukses', 'tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perlengkapan = Perkap::find($id);
        $perlengkapan->status = 0;
        $perlengkapan->update();

        $akun_perlengkapan = Akun::where('kode', Akun::Perlengkapan)->first();
        $akun_perlengkapan->debet -= $perlengkapan->harga;
        $akun_perlengkapan->update();

        $akun_laba_ditahan = Akun::where('kode', Akun::LabaTahan)->first();
        $akun_laba_ditahan->kredit -= $perlengkapan->harga;
        $akun_laba_ditahan->update();

        // $akun_beban_perlengkapan = Akun::where('kode', Akun::BebanPerlengkapan)->first();
        // $akun_beban_perlengkapan->debet = $akun_beban_perlengkapan->debet + $perlengkapan->harga;
        // $akun_beban_perlengkapan->update();

        Jurnal::create([
            'kode_akun' => Akun::BebanPerlengkapan,
            'referensi' => $perlengkapan->kode,
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'debet' => $perlengkapan->harga
        ]);

        Jurnal::create([
            'kode_akun' => Akun::Perlengkapan,
            'referensi' => $perlengkapan->kode,
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'kredit' => $perlengkapan->harga
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $perlengkapan->kode.' - L/R',
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'debet' => $perlengkapan->harga
        ]);

        Jurnal::create([
            'kode_akun' => Akun::BebanPerlengkapan,
            'referensi' => $perlengkapan->kode.' - L/R',
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'kredit' => $perlengkapan->harga
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaTahan,
            'referensi' => $perlengkapan->kode.' - LDT',
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'debet' => $perlengkapan->harga
        ]);

        Jurnal::create([
            'kode_akun' => Akun::LabaRugi,
            'referensi' => $perlengkapan->kode.' - LDT',
            'keterangan' => 'Perlengkapan Rusak/ Habis | '.$perlengkapan->keterangan,
            'kredit' => $perlengkapan->harga
        ]);
        
        return redirect('/perlengkapan')->with('sukses', 'hapus');
    }
}
