<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Datetime;

use App\Akun;
use App\Item;
use App\Laci;
use App\LogLaci;
use App\Stok;
use App\User;
use App\Util;
use App\Notice;
use App\CashDrawer;
use App\LaciGrosir;
use App\MoneyLimit;
use App\PiutangDagang;
use App\RelasiSatuan;
use App\SetoranBuka;
use App\DataPerusahaan;
use App\TransaksiPembelian;
use App\TransaksiPenjualan;

use Illuminate\Http\Request;

class AlertController extends Controller
{
    public function stokLimitJson()
    {
        $items = Item::where('konsinyasi', 0)->groupBy('kode_barang')->get();
        $kurang = array();
        foreach ($items as $item) {
            if($item->stoktotal < $item->limit_stok){
                $kurang[] = $item->kode;
            }
        }
        return response()->json(['kurang' => $kurang]);
    }

    public function noticeJson()
    {
        $notices = Notice::with('transaksi_pembelian')->where('aktif', 1)->orderBy('created_at', 'asc')->get();
        return response()->json(compact('notices'));
    }

    public function kadaluarsaJson()
    {
        $today = (new Datetime)->format('Y-m-d');
        $execpt = ['0000-00-00'];
        // $kadaluarsa = Stok::select(DB::raw('item_kode'), DB::raw('kadaluarsa'))->where('kadaluarsa', '<', $today )->whereNotIn('kadaluarsa', $execpt)->get();
        $kadaluarsa = Stok::whereNotIn('kadaluarsa', $execpt)->where('kadaluarsa', '<', $today )->get();

        return response()->json(['kadaluarsa' => $kadaluarsa]);
    }

    public function cashKasirJson()
    {
        $user = Auth::User();
        $money_limit = MoneyLimit::find(1);
        $cash = array();
        if ($user->level_id == 3) {
            $cash_drawer = CashDrawer::where('user_id', $user->id)->first();
            $cash['level_id'] = $user->level_id;
            $cash['nominal'] = $cash_drawer->nominal;

            $bukaan = SetoranBuka::where('user_id', Auth::id())->first()->nominal;
            return response()->json(compact('cash', 'money_limit', 'bukaan'));
        }
    }

    public function cashGrosirJson()
    {
        $user = Auth::User();
        if($user->level_id == 1 || $user->level_id == 2 || $user->level_id == 4){
            $users = User::where('level_id', 3)->where('status', 1)->limit(2)->get();
            $cash = array();
            foreach ($users as $x => $user) {
                $cash_drawer = CashDrawer::where('user_id', $user->id)->first();
                // $nama = explode(" ", $user->nama);
                $akro = $user->username;
                // foreach ($nama as $key => $i) {
                //     if($key == 0){
                //         $akro .= $i;
                //     } else {
                //         $akro .= ' '.$i[0];
                //     }
                // }

                $cash[$x] = [
                    'nama' => 'Laci '.$akro,
                    'nominal' => number_format($cash_drawer->nominal, 0,",", ".")
                ];
            }
            $laci_grosir = Laci::find(1);
            $cash[$x+1] = [
                'nama' => 'Laci Grosir',
                'nominal' => number_format($laci_grosir->grosir, 0,",", ".")
            ];
            return response()->json(['cash' => $cash]);
        }
    }

    public function cashGudangJson()
    {
        $user = Auth::User();
        if($user->level_id == 5){
            $cash = array();
            
            $laci_grosir = Laci::find(1);
            $cash[] = [
                'nama' => 'Laci Gudang',
                'nominal' => number_format($laci_grosir->grosir, 0,",", ".")
            ];
            return response()->json(['cash' => $cash]);
        }
    }

    public function alert()
    {
        $items = Item::where('konsinyasi', 0)->groupBy('kode_barang')->get();
        // return $items;
        $kurang = array();
        foreach ($items as $item) {
            if($item->stoktotal < $item->limit_stok){
                $kurang[] = $item->kode_barang;
            }
        }

        $item_kurangs = Item::whereIn('kode_barang', $kurang)->groupBy('kode_barang')->get();
        $item_show = array();
        foreach ($item_kurangs as $x => $item) {
            $relasi_satuans = RelasiSatuan::where('item_kode', $item->kode)->get();
            $item_show[$x]['nama'] = $item->nama;
            $item_show[$x]['kode'] = $item->kode;
            $item_show[$x]['kode_barang'] = $item->kode_barang;
            for ($i = 0; $i < count($relasi_satuans); $i++) {
                $hasil_total = explode('.', $item->stoktotal / $relasi_satuans[$i]->konversi)[0];
                $sisa_total  = $item->stoktotal % $relasi_satuans[$i]->konversi;

                $item->stoktotal = $sisa_total;

                $item_show[$x]['stok'][$i] = ['jumlah' => $hasil_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            }
        }

        return view('alert.stok', compact('item_show'));
    }

    public function kadaluarsa()
    {
        $today = (new Datetime)->format('Y-m-d');
        $execpt = ['0000-00-00'];
        $kadaluarsa = Stok::select(DB::raw('item_kode'), DB::raw('kadaluarsa'), DB::raw('SUM(jumlah) as jumlah'))->where('kadaluarsa', '<', $today )->whereNotIn('kadaluarsa', $execpt)->groupBy('item_kode')->get();
        // $kadaluarsa = Stok::whereNotIn('kadaluarsa', $execpt)->where('kadaluarsa', '<', $today )->get();

        $item_show = array();
        foreach ($kadaluarsa as $x => $item) {
            $relasi_satuans = RelasiSatuan::where('item_kode', $item->item_kode)->get();
            $item_show[$x]['nama'] = $item->items->nama;
            $item_show[$x]['kode'] = $item->item_kode;
            for ($i = 0; $i < count($relasi_satuans); $i++) {
                $hasil_total = explode('.', $item->jumlah / $relasi_satuans[$i]->konversi)[0];
                $sisa_total  = $item->jumlah % $relasi_satuans[$i]->konversi;

                $item->jumlah = $sisa_total;

                if ($hasil_total > 0) $item_show[$x]['stok'][$i] = ['jumlah' => $hasil_total, 'satuan' => $relasi_satuans[$i]->satuan->kode];
            }
        }

        // return $item_show;
        return view('alert.kadaluarsa', compact('item_show'));
    }

    public function logoJson(){
        $logo = DataPerusahaan::find(1);

        return response()->json(['logo' => $logo]);
    }

    public function JTPembelianJSON(){
        if(Auth::user()->level_id == 1 || Auth::user()->level_id == 2){
            $today = new Datetime();
            // $today7 = $today->modify('+7 days');
            $today7 = $today->format('Y-m-d');

            $list = TransaksiPembelian::where('sisa_utang', '>', 0)->where('jatuh_tempo', '<', $today7)->get();
            return count($list);
            // return response()->json(['isi' => count($list)]);
        }
    }

    public function JTPembelian(){
        $today = new Datetime();
        // $today7 = $today->modify('+7 days');
        $today7 = $today->format('Y-m-d');

        $list = TransaksiPembelian::where('sisa_utang', '>', 0)->where('jatuh_tempo', '<', $today7)->get();

        return view('alert.jatuh_tempo', compact('list'));
    }

    public function LaciLaci(){
        $user_level = Auth::user()->level_id;
        if($user_level == 3){
            $log = LogLaci::where('penerima', Auth::user()->id)->where('approve', 0)->get();
        }else{
            $users = User::where('level_id', $user_level)->get();
            $user_array = array();
            foreach ($users as $i => $user) {
                array_push($user_array, $user->id);
            }
            $log = LogLaci::whereIn('penerima', $user_array)->where('approve', 0)->get();
        }

        return response()->json(compact('user_level', 'log'));
    }

    public function JTPenjualanJSON(){
        if (in_array(Auth::user()->level_id, [1, 2, 4])) {
            $today = new Datetime();
            $today7 = $today->format('Y-m-d');

            $list = PiutangDagang::where('sisa', '>', 0)->get();
            $lewat_jatuh_tempo = [];
            foreach ($list as $i => $data) {
                $transaksi = TransaksiPenjualan::find($data->transaksi_penjualan_id);
                if($transaksi->jatuh_tempo < $today7) array_push($lewat_jatuh_tempo, $transaksi);
            }
            return count($lewat_jatuh_tempo);
        }
    }

    public function JTPenjualan(){
        $today = new Datetime();
        $today7 = $today->format('Y-m-d');

        $list = PiutangDagang::where('sisa', '>', 0)->get();
        $lewat_jatuh_tempo = [];
        $index = 0;
        foreach ($list as $i => $data) {
            $transaksi = TransaksiPenjualan::find($data->transaksi_penjualan_id);
            if($transaksi->jatuh_tempo < $today7) array_push($lewat_jatuh_tempo, $transaksi);
        }
        // return $lewat_jatuh_tempo;
        return view('alert.jatuh_tempo_penjualan', compact('lewat_jatuh_tempo'));
    }

    public function ValidasiList() {
        $list = Item::where('valid_konversi', 0)->groupBy('kode_barang')->get();
        // // return $items;
        // $list = array();
        // foreach ($items as $item) {
        //     array_push($list, $item);
        // }

        return view('alert.validasi', compact('list'));
    }

    public function ValidasiListJSON() {
        $accept = [1,2];
        if (in_array(Auth::user()->level_id, $accept)){
            $items = Item::where('aktif', 1)->where('valid_konversi', 0)->groupBy('kode_barang')->get();

            return count($items);
        }
    }
}
