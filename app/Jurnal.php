<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_akun',
        'referensi',
        'keterangan',
        'debet',
        'kredit',
        'user_id',
    ];

    public function akun()
    {
        return $this->belongsTo('App\Akun', 'kode_akun', 'kode');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
