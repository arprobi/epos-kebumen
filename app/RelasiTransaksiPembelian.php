<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelasiTransaksiPembelian extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id', 'transaksi_pembelian_id', 'jumlah', 'harga', 'subtotal',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function transaksi_pembelian()
    {
        return $this->belongsTo('App\TransaksiPembelian');
    }

}
