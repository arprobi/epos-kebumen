<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiutangLain extends Model
{
    // protected $fillable = [
    //     'nama', 'nominal', 'karyawan', 'CPR', 'CPT',
    // ];

    protected $fillable = [
        'kode_transaksi',
        'nama',
        'nominal',
        'karyawan',
        'user_id',
        'sisa',
        'PT',
        'keterangan',
    ];

    public function bayar_piutang_lain()
    {
        return $this->hasMany('App\BayarPiutangLain');
    }
}
