<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturPenjualan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_retur',
        'transaksi_penjualan_id',
        'status',
        'harga_total',
        'total_uang_masuk',
        'total_uang_keluar',
        'pelanggan_id',
        'cetak',
        'user_id'
    ];

    public function transaksi_penjualan()
    {
    	return $this->belongsTo('App\TransaksiPenjualan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'relasi_retur_penjualans')
            ->withPivot('jumlah', 'keterangan')
            ->withTimestamps();
    }
}
