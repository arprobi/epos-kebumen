<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HutangBank extends Model
{
    protected $fillable = [
        'kode_transaksi', 'bank_id', 'nominal', 'sisa_hutang', 'no_transaksi', 'keterangan'
    ];

    public function bank()
    {	
        return $this->belongsTo('App\Bank');
    }
}
