<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harga extends Model
{
    protected $fillable = [
        'item_kode', 'satuan_id', 'jumlah', 'eceran', 'grosir'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_kode', 'kode');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Satuan', 'satuan_id');
    }
}
