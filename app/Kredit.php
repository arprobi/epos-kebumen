<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kredit extends Model
{
    protected $fillable = [
        'nomor', 'validasi', 'vcc'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }    
}
