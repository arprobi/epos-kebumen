<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayarPiutangLain extends Model
{
    protected $fillable = [
        'piutang_lain_id', 'nominal', 'metode_pembayaran', 'nomer_trasnfer'
    ];

    public function piutang_lain()
    {
        return $this->belongsTo('App\PiutangLain');
    }
}
