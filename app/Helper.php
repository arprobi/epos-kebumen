<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Helper extends Model
{
     public static function getEnumValues($table, $coloumn)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$coloumn}'"))[0]->Type;
        // $type = DB::select("SHOW COLOUMNS FROM $table WHERE Field = '{$coloumn}'")[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach ( explode(',', $matches[1]) as $value) {
            // $v = trim( $value, "'" );
            // $enum = array_add($enum, $v, $v);

            $v = trim( $value, "'" );
            $enum[] = $v;
        }

        return $enum;
    }
}

