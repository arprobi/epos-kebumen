<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranReturPenjualan extends Model
{
    public function bank_transferp()
    {
    	return $this->belongsTo('App\Bank', 'bank_transfer');
    }

    public function bank_kartup()
    {
    	return $this->belongsTo('App\Bank', 'bank_kartu');
    }

    public function bank_transferp_in()
    {
    	return $this->belongsTo('App\Bank', 'bank_transfer_in');
    }

    public function bank_kartup_in()
    {
    	return $this->belongsTo('App\Bank', 'bank_kartu_in');
    }
}
