<?php

use Illuminate\Database\Seeder;

class KabupatensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kabupatens')->delete();
        
        \DB::table('kabupatens')->insert(array (
            0 => 
            array (
                'id' => 1,
                'provinsi_id' => 1,
                'nama' => 'Kebumen',
                'created_at' => '2018-03-01 00:23:40',
                'updated_at' => '2018-03-01 00:23:40',
            ),
        ));
        
        
    }
}