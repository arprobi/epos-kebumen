<?php

use Illuminate\Database\Seeder;

class SatuansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('satuans')->delete();
        
        \DB::table('satuans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => 'BIJI',
                'nama' => 'BIJI',
                'created_at' => '2018-01-30 15:00:20',
                'updated_at' => '2018-02-03 14:13:12',
            ),
            1 => 
            array (
                'id' => 2,
                'kode' => 'BAL',
                'nama' => 'BAL',
                'created_at' => '2018-01-30 15:00:28',
                'updated_at' => '2018-02-03 14:13:22',
            ),
            2 => 
            array (
                'id' => 3,
                'kode' => 'BKS',
                'nama' => 'BUNGKUS',
                'created_at' => '2018-01-30 15:00:38',
                'updated_at' => '2018-01-30 15:00:38',
            ),
            3 => 
            array (
                'id' => 4,
                'kode' => 'GRAM',
                'nama' => 'GRAM',
                'created_at' => '2018-01-30 15:01:24',
                'updated_at' => '2018-01-30 15:01:24',
            ),
            4 => 
            array (
                'id' => 5,
                'kode' => 'KG',
                'nama' => 'KILOGRAM',
                'created_at' => '2018-01-30 15:02:01',
                'updated_at' => '2018-01-30 15:02:01',
            ),
            5 => 
            array (
                'id' => 6,
                'kode' => 'KRTN',
                'nama' => 'KARTON',
                'created_at' => '2018-01-30 15:02:43',
                'updated_at' => '2018-01-30 15:02:43',
            ),
            6 => 
            array (
                'id' => 7,
                'kode' => 'LSN',
                'nama' => 'LUSIN',
                'created_at' => '2018-01-30 15:02:58',
                'updated_at' => '2018-01-30 15:02:58',
            ),
            7 => 
            array (
                'id' => 8,
                'kode' => 'PAK',
                'nama' => 'PAK',
                'created_at' => '2018-01-30 15:03:10',
                'updated_at' => '2018-01-30 15:03:10',
            ),
            8 => 
            array (
                'id' => 9,
                'kode' => 'ROL',
                'nama' => 'ROL',
                'created_at' => '2018-01-30 15:03:23',
                'updated_at' => '2018-01-30 15:03:23',
            ),
            9 => 
            array (
                'id' => 10,
                'kode' => 'RTG',
                'nama' => 'RENTENG',
                'created_at' => '2018-01-30 15:03:37',
                'updated_at' => '2018-01-30 15:03:37',
            ),
            10 => 
            array (
                'id' => 11,
                'kode' => 'SAK',
                'nama' => 'SAK',
                'created_at' => '2018-01-30 15:03:43',
                'updated_at' => '2018-01-30 15:03:43',
            ),
            11 => 
            array (
                'id' => 12,
                'kode' => 'KRG',
                'nama' => 'KARUNG',
                'created_at' => '2018-03-29 15:41:41',
                'updated_at' => '2018-03-29 15:41:41',
            ),
            12 => 
            array (
                'id' => 13,
                'kode' => 'KRAT',
                'nama' => 'KRAT',
                'created_at' => '2018-03-29 17:07:37',
                'updated_at' => '2018-03-29 17:07:37',
            ),
            13 => 
            array (
                'id' => 14,
                'kode' => 'DUS',
                'nama' => 'DUS',
                'created_at' => '2018-03-29 20:13:04',
                'updated_at' => '2018-03-29 20:13:04',
            ),
            14 => 
            array (
                'id' => 99,
                'kode' => 'PKT',
                'nama' => 'PAKET',
                'created_at' => '2018-04-29 14:30:15',
                'updated_at' => '2018-04-29 14:30:16',
            ),
        ));
        
        
    }
}