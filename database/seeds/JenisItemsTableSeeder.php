<?php

use Illuminate\Database\Seeder;

class JenisItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jenis_items')->delete();
        
        \DB::table('jenis_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => 'BNS',
                'nama' => 'BONUS',
                'created_at' => '2018-01-30 14:53:34',
                'updated_at' => '2018-01-30 14:59:55',
            ),
            1 => 
            array (
                'id' => 2,
                'kode' => 'ELK',
                'nama' => 'ELEKTRONIK',
                'created_at' => '2018-01-30 14:54:27',
                'updated_at' => '2018-01-30 14:59:55',
            ),
            2 => 
            array (
                'id' => 3,
                'kode' => 'OBT',
                'nama' => 'OBAT-OBATAN',
                'created_at' => '2018-01-30 14:54:46',
                'updated_at' => '2018-01-30 14:59:55',
            ),
            3 => 
            array (
                'id' => 4,
                'kode' => 'PLS',
                'nama' => 'PLASTIK',
                'created_at' => '2018-01-30 14:55:15',
                'updated_at' => '2018-01-30 14:59:55',
            ),
            4 => 
            array (
                'id' => 5,
                'kode' => 'SBK',
                'nama' => 'SEMBAKO',
                'created_at' => '2018-01-30 14:55:23',
                'updated_at' => '2018-01-30 14:59:55',
            ),
            5 => 
            array (
                'id' => 6,
                'kode' => 'ATK',
                'nama' => 'ALAT TULIS KANTOR',
                'created_at' => '2018-02-03 14:09:06',
                'updated_at' => '2018-02-03 14:09:06',
            ),
            6 => 
            array (
                'id' => 8,
                'kode' => 'RKK',
                'nama' => 'ROKOK',
                'created_at' => '2018-03-27 19:04:33',
                'updated_at' => '2018-03-27 19:04:33',
            ),
            7 => 
            array (
                'id' => 9,
                'kode' => 'ACK',
                'nama' => 'ALAT CUKUR',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            8 => 
            array (
                'id' => 10,
                'kode' => 'B&P',
                'nama' => 'PEMBALUT & PEMPES',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            9 => 
            array (
                'id' => 11,
                'kode' => 'BDK',
                'nama' => 'BEDAK',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            10 => 
            array (
                'id' => 12,
                'kode' => 'BHK',
                'nama' => 'BAHAN KUE',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            11 => 
            array (
                'id' => 13,
                'kode' => 'DDR',
                'nama' => 'DEODORAN',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            12 => 
            array (
                'id' => 14,
                'kode' => 'GRT',
                'nama' => 'GAYA RAMBUT',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            13 => 
            array (
                'id' => 15,
                'kode' => 'KRK',
                'nama' => 'KOREK',
                'created_at' => '2018-03-27 23:16:41',
                'updated_at' => '2018-03-27 23:16:41',
            ),
            14 => 
            array (
                'id' => 16,
                'kode' => 'LEM',
                'nama' => 'LEM',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            15 => 
            array (
                'id' => 17,
                'kode' => 'LLN',
                'nama' => 'LILIN',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            16 => 
            array (
                'id' => 18,
                'kode' => 'ONY',
                'nama' => 'OBAT NYAMUK',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            17 => 
            array (
                'id' => 19,
                'kode' => 'P&P',
                'nama' => 'PEWANGI & PARFUM',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            18 => 
            array (
                'id' => 20,
                'kode' => 'PLK',
                'nama' => 'PELEMBAB KULIT',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            19 => 
            array (
                'id' => 21,
                'kode' => 'PMM',
                'nama' => 'PEMBERSIH MUKA',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            20 => 
            array (
                'id' => 22,
                'kode' => 'SBN',
                'nama' => 'SABUN',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            21 => 
            array (
                'id' => 23,
                'kode' => 'SDL',
                'nama' => 'SENDAL',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            22 => 
            array (
                'id' => 24,
                'kode' => 'SHP',
                'nama' => 'SHAMPO',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            23 => 
            array (
                'id' => 25,
                'kode' => 'SMR',
                'nama' => 'SEMIR',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            24 => 
            array (
                'id' => 26,
                'kode' => 'SPG',
                'nama' => 'SIKAT & PASTA GIGI',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            25 => 
            array (
                'id' => 27,
                'kode' => 'SRS',
                'nama' => 'SERBA-SERBI',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
            26 => 
            array (
                'id' => 28,
                'kode' => 'TSS',
                'nama' => 'TISSUE',
                'created_at' => '2018-03-27 23:16:42',
                'updated_at' => '2018-03-27 23:16:42',
            ),
        ));
        
        
    }
}