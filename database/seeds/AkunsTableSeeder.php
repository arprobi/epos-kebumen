<?php

use Illuminate\Database\Seeder;

class AkunsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('akuns')->delete();
        
        \DB::table('akuns')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => 100000,
                'nama' => 'Aset',
                'debet' => '2251126327.580',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            1 => 
            array (
                'id' => 2,
                'kode' => 110000,
                'nama' => 'Aset Lancar',
                'debet' => '335061618.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            2 => 
            array (
                'id' => 3,
                'kode' => 111100,
                'nama' => 'Kas Tunai',
                'debet' => '54329300.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-27 14:14:39',
            ),
            3 => 
            array (
                'id' => 4,
                'kode' => 111200,
                'nama' => 'Kas Kecil',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            4 => 
            array (
                'id' => 5,
                'kode' => 111310,
                'nama' => 'Kas Bank',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 13:50:00',
            ),
            5 => 
            array (
                'id' => 6,
                'kode' => 111320,
                'nama' => 'Kas Cek',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            6 => 
            array (
                'id' => 7,
                'kode' => 111330,
                'nama' => 'Kas BG',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            7 => 
            array (
                'id' => 8,
                'kode' => 112000,
                'nama' => 'Persediaan',
                'debet' => '240291069.290',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-14 14:04:10',
            ),
            8 => 
            array (
                'id' => 9,
                'kode' => 113000,
                'nama' => 'Perlengkapan',
                'debet' => '16412700.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-01 02:03:34',
            ),
            9 => 
            array (
                'id' => 10,
                'kode' => 114100,
                'nama' => 'Piutang Dagang',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            10 => 
            array (
                'id' => 11,
                'kode' => 114200,
                'nama' => 'Piutang Karyawan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            11 => 
            array (
                'id' => 12,
                'kode' => 114300,
                'nama' => 'Piutang Lain-lain',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            12 => 
            array (
                'id' => 13,
                'kode' => 114500,
                'nama' => 'Piutang Tak Tertagih',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            13 => 
            array (
                'id' => 14,
                'kode' => 115100,
                'nama' => 'Asuransi Dibayar Dimuka',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            14 => 
            array (
                'id' => 15,
                'kode' => 115200,
                'nama' => 'Sewa Dibayar Dimuka',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            15 => 
            array (
                'id' => 16,
                'kode' => 116000,
                'nama' => 'PPN Masukan',
                'debet' => '24028548.710',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-14 14:04:09',
            ),
            16 => 
            array (
                'id' => 17,
                'kode' => 120000,
                'nama' => 'Aset Tetap',
                'debet' => '1916064709.580',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            17 => 
            array (
                'id' => 18,
                'kode' => 121000,
                'nama' => 'Tanah',
                'debet' => '924000000.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-01 00:00:00',
                'updated_at' => '2018-03-01 02:00:24',
            ),
            18 => 
            array (
                'id' => 19,
                'kode' => 122000,
                'nama' => 'Bangunan',
                'debet' => '941000100.000',
                'kredit' => '0.000',
                'created_at' => '2018-03-01 00:00:00',
                'updated_at' => '2018-03-29 12:58:21',
            ),
            19 => 
            array (
                'id' => 20,
                'kode' => 123000,
                'nama' => 'Peralatan',
                'debet' => '55917400.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-29 12:58:21',
            ),
            20 => 
            array (
                'id' => 21,
                'kode' => 124000,
                'nama' => 'Kendaraan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-29 12:58:21',
            ),
            21 => 
            array (
                'id' => 22,
                'kode' => 125100,
                'nama' => 'Akumulasi Depresiasi Bangunan',
                'debet' => '0.000',
                'kredit' => '3920833.750',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-01 00:39:24',
            ),
            22 => 
            array (
                'id' => 23,
                'kode' => 125200,
                'nama' => 'Akumulasi Depresiasi Peralatan',
                'debet' => '0.000',
                'kredit' => '931956.670',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-01 00:39:23',
            ),
            23 => 
            array (
                'id' => 24,
                'kode' => 125300,
                'nama' => 'Akumulasi Depresiasi Kendaraan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-29 12:58:22',
            ),
            24 => 
            array (
                'id' => 25,
                'kode' => 200000,
                'nama' => 'Kewajiban',
                'debet' => '0.000',
                'kredit' => '263626434.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            25 => 
            array (
                'id' => 26,
                'kode' => 210000,
                'nama' => 'Hutang Jangka Pendek',
                'debet' => '0.000',
                'kredit' => '263626434.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            26 => 
            array (
                'id' => 27,
                'kode' => 211000,
                'nama' => 'Kartu Kredit',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            27 => 
            array (
                'id' => 28,
                'kode' => 212100,
                'nama' => 'Hutang Dagang',
                'debet' => '0.000',
                'kredit' => '263626434.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-14 14:04:10',
            ),
            28 => 
            array (
                'id' => 29,
                'kode' => 212300,
                'nama' => 'Hutang Pajak',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            29 => 
            array (
                'id' => 30,
                'kode' => 213000,
                'nama' => 'Taksiran Utang Garansi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            30 => 
            array (
                'id' => 31,
                'kode' => 214000,
                'nama' => 'Hutang Konsinyasi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            31 => 
            array (
                'id' => 32,
                'kode' => 215100,
                'nama' => 'Hutang PPN',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            32 => 
            array (
                'id' => 33,
                'kode' => 215200,
                'nama' => 'PPN Keluaran',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-03-29 12:58:22',
            ),
            33 => 
            array (
                'id' => 34,
                'kode' => 216000,
                'nama' => 'Deposito Pelanggan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            34 => 
            array (
                'id' => 35,
                'kode' => 220000,
                'nama' => 'Hutang Jangka Panjang',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 23:37:00',
            ),
            35 => 
            array (
                'id' => 36,
                'kode' => 221000,
                'nama' => 'Hutang Bank',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            36 => 
            array (
                'id' => 37,
                'kode' => 300000,
                'nama' => 'Modal',
                'debet' => '0.000',
                'kredit' => '2000000000.000',
                'created_at' => '2018-03-01 01:57:43',
                'updated_at' => '2018-03-01 01:57:43',
            ),
            37 => 
            array (
                'id' => 38,
                'kode' => 310000,
                'nama' => 'Laba Ditahan',
                'debet' => '0.000',
                'kredit' => '-12500106.420',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-01 00:39:24',
            ),
            38 => 
            array (
                'id' => 39,
                'kode' => 320000,
                'nama' => 'Prive',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 14:23:00',
            ),
            39 => 
            array (
                'id' => 40,
                'kode' => 330000,
                'nama' => 'Bagi Hasil',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            40 => 
            array (
                'id' => 41,
                'kode' => 400000,
                'nama' => 'Pendapatan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            41 => 
            array (
                'id' => 42,
                'kode' => 411000,
                'nama' => 'Penjualan ',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            42 => 
            array (
                'id' => 43,
                'kode' => 412000,
                'nama' => 'Penjualan Kredit',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            43 => 
            array (
                'id' => 44,
                'kode' => 420000,
                'nama' => 'Pendapatan lain-lain',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-20 19:41:37',
            ),
            44 => 
            array (
                'id' => 45,
                'kode' => 430000,
                'nama' => 'Potongan Pembelian',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            45 => 
            array (
                'id' => 46,
                'kode' => 431000,
                'nama' => 'Retur Pembelian',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            46 => 
            array (
                'id' => 47,
                'kode' => 440000,
                'nama' => 'Potongan Penjualan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            47 => 
            array (
                'id' => 48,
                'kode' => 450000,
                'nama' => 'Retur Penjualan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            48 => 
            array (
                'id' => 49,
                'kode' => 470000,
                'nama' => 'HPP',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            49 => 
            array (
                'id' => 50,
                'kode' => 480000,
                'nama' => 'Keuntungan Persediaan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            50 => 
            array (
                'id' => 51,
                'kode' => 500000,
                'nama' => 'BEBAN',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2018-04-28 23:39:59',
            ),
            51 => 
            array (
                'id' => 52,
                'kode' => 511000,
                'nama' => 'Beban Iklan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            52 => 
            array (
                'id' => 53,
                'kode' => 512000,
                'nama' => 'Beban Sewa',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            53 => 
            array (
                'id' => 54,
                'kode' => 531000,
                'nama' => 'Beban Perlengkapan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 13:23:00',
            ),
            54 => 
            array (
                'id' => 55,
                'kode' => 532000,
                'nama' => 'Beban Perawatan dan Perbaikan ',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 13:29:00',
            ),
            55 => 
            array (
                'id' => 56,
                'kode' => 541000,
                'nama' => 'Beban Kerugian Piutang',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            56 => 
            array (
                'id' => 57,
                'kode' => 542000,
                'nama' => 'Beban Kerugian Persediaan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            57 => 
            array (
                'id' => 58,
                'kode' => 543000,
                'nama' => 'Beban Kerugian Aset',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            58 => 
            array (
                'id' => 59,
                'kode' => 551000,
                'nama' => 'Beban Depresiasi Bangunan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            59 => 
            array (
                'id' => 60,
                'kode' => 552000,
                'nama' => 'Beban Depresiasi Peralatan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-14 21:08:00',
            ),
            60 => 
            array (
                'id' => 61,
                'kode' => 553000,
                'nama' => 'Beban Depresiasi Kendaraan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-10-20 19:43:01',
            ),
            61 => 
            array (
                'id' => 62,
                'kode' => 561000,
                'nama' => 'Beban Asuransi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            62 => 
            array (
                'id' => 63,
                'kode' => 562000,
                'nama' => 'Beban Gaji',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            63 => 
            array (
                'id' => 64,
                'kode' => 563000,
                'nama' => 'Beban Administrasi Bank',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            64 => 
            array (
                'id' => 65,
                'kode' => 563100,
                'nama' => 'Beban Bunga',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            65 => 
            array (
                'id' => 66,
                'kode' => 564000,
                'nama' => 'Beban Denda',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            66 => 
            array (
                'id' => 67,
                'kode' => 571000,
                'nama' => 'Beban Internet dan Telepon',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            67 => 
            array (
                'id' => 68,
                'kode' => 572000,
                'nama' => 'Beban Ongkir',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            68 => 
            array (
                'id' => 69,
                'kode' => 573000,
                'nama' => 'Beban BBM',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            69 => 
            array (
                'id' => 71,
                'kode' => 580000,
                'nama' => 'Beban Pajak',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            70 => 
            array (
                'id' => 72,
                'kode' => 593000,
                'nama' => 'Beban Lain-Lain',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            71 => 
            array (
                'id' => 73,
                'kode' => 600000,
                'nama' => 'Ikhtisar Laba Rugi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            72 => 
            array (
                'id' => 74,
                'kode' => 575000,
                'nama' => 'Beban Garansi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            73 => 
            array (
                'id' => 75,
                'kode' => 217000,
                'nama' => 'Pembayaran Dalam Proses',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            74 => 
            array (
                'id' => 76,
                'kode' => 591000,
                'nama' => 'Beban Konsumsi',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            75 => 
            array (
                'id' => 77,
                'kode' => 592000,
                'nama' => 'Beban Sosial',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            76 => 
            array (
                'id' => 78,
                'kode' => 544000,
                'nama' => 'Beban Kerugian Pembelian',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            77 => 
            array (
                'id' => 79,
                'kode' => 490000,
                'nama' => 'Bonus Penjualan',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
            78 => 
            array (
                'id' => 80,
                'kode' => 574000,
                'nama' => 'Beban Listrik',
                'debet' => '0.000',
                'kredit' => '0.000',
                'created_at' => '2017-08-30 18:27:00',
                'updated_at' => '2017-08-31 18:27:00',
            ),
        ));
        
        
    }
}