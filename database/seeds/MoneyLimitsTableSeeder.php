<?php

use Illuminate\Database\Seeder;

class MoneyLimitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('money_limits')->delete();
        
        \DB::table('money_limits')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nominal' => 1000000,
                'user_id' => 1,
                'created_at' => '2018-03-01 00:23:40',
                'updated_at' => '2018-03-01 00:23:40',
            ),
        ));
        
        
    }
}