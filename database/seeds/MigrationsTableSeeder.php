<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 5122,
                'migration' => '2014_10_11_230438_create_levels_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 5123,
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 1,
            ),
            2 => 
            array (
                'id' => 5124,
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 1,
            ),
            3 => 
            array (
                'id' => 5125,
                'migration' => '2017_03_01_160124_create_satuans_table',
                'batch' => 1,
            ),
            4 => 
            array (
                'id' => 5126,
                'migration' => '2017_03_03_160052_create_jenis_items_table',
                'batch' => 1,
            ),
            5 => 
            array (
                'id' => 5127,
                'migration' => '2017_03_03_160131_create_bonuses_table',
                'batch' => 1,
            ),
            6 => 
            array (
                'id' => 5128,
                'migration' => '2017_03_03_160132_create_supliers_table',
                'batch' => 1,
            ),
            7 => 
            array (
                'id' => 5129,
                'migration' => '2017_03_03_160133_create_items_table',
                'batch' => 1,
            ),
            8 => 
            array (
                'id' => 5130,
                'migration' => '2017_03_03_160134_create_sellers_table',
                'batch' => 1,
            ),
            9 => 
            array (
                'id' => 5131,
                'migration' => '2017_03_04_233349_create_relasi_satuans_table',
                'batch' => 1,
            ),
            10 => 
            array (
                'id' => 5132,
                'migration' => '2017_03_04_233350_create_relasi_bonuses_table',
                'batch' => 1,
            ),
            11 => 
            array (
                'id' => 5133,
                'migration' => '2017_03_10_154424_create_item_masuks_table',
                'batch' => 1,
            ),
            12 => 
            array (
                'id' => 5134,
                'migration' => '2017_03_10_154428_create_item_keluars_table',
                'batch' => 1,
            ),
            13 => 
            array (
                'id' => 5135,
                'migration' => '2017_03_11_224055_create_hargas_table',
                'batch' => 1,
            ),
            14 => 
            array (
                'id' => 5136,
                'migration' => '2017_03_21_132500_create_provinsis_table',
                'batch' => 1,
            ),
            15 => 
            array (
                'id' => 5137,
                'migration' => '2017_03_21_132530_create_kabupatens_table',
                'batch' => 1,
            ),
            16 => 
            array (
                'id' => 5138,
                'migration' => '2017_03_21_132540_create_kecamatans_table',
                'batch' => 1,
            ),
            17 => 
            array (
                'id' => 5139,
                'migration' => '2017_03_21_132556_create_desas_table',
                'batch' => 1,
            ),
            18 => 
            array (
                'id' => 5140,
                'migration' => '2017_03_21_132557_create_banks_table',
                'batch' => 1,
            ),
            19 => 
            array (
                'id' => 5141,
                'migration' => '2017_03_21_132558_create_kredits_table',
                'batch' => 1,
            ),
            20 => 
            array (
                'id' => 5142,
                'migration' => '2017_03_21_211915_create_pelanggans_table',
                'batch' => 1,
            ),
            21 => 
            array (
                'id' => 5143,
                'migration' => '2017_03_22_221952_create_transaksi_pembelians_table',
                'batch' => 1,
            ),
            22 => 
            array (
                'id' => 5144,
                'migration' => '2017_03_22_222007_create_relasi_transaksi_pembelians_table',
                'batch' => 1,
            ),
            23 => 
            array (
                'id' => 5145,
                'migration' => '2017_03_22_223010_create_transaksi_penjualans_table',
                'batch' => 1,
            ),
            24 => 
            array (
                'id' => 5146,
                'migration' => '2017_03_22_223157_create_relasi_transaksi_penjualans_table',
                'batch' => 1,
            ),
            25 => 
            array (
                'id' => 5147,
                'migration' => '2017_04_01_213128_create_hutangs_table',
                'batch' => 1,
            ),
            26 => 
            array (
                'id' => 5148,
                'migration' => '2017_04_15_134241_create_log_transfer_banks_table',
                'batch' => 1,
            ),
            27 => 
            array (
                'id' => 5149,
                'migration' => '2017_04_16_095654_create_retur_pembelians_table',
                'batch' => 1,
            ),
            28 => 
            array (
                'id' => 5150,
                'migration' => '2017_04_16_095655_create_stoks_table',
                'batch' => 1,
            ),
            29 => 
            array (
                'id' => 5151,
                'migration' => '2017_04_16_100147_create_relasi_retur_pembelians_table',
                'batch' => 1,
            ),
            30 => 
            array (
                'id' => 5152,
                'migration' => '2017_04_17_193107_create_modals_table',
                'batch' => 1,
            ),
            31 => 
            array (
                'id' => 5153,
                'migration' => '2017_04_17_193539_create_log_bebans_table',
                'batch' => 1,
            ),
            32 => 
            array (
                'id' => 5154,
                'migration' => '2017_04_19_222811_create_retur_penjualans_table',
                'batch' => 1,
            ),
            33 => 
            array (
                'id' => 5155,
                'migration' => '2017_04_19_223111_create_relasi_retur_penjualans_table',
                'batch' => 1,
            ),
            34 => 
            array (
                'id' => 5156,
                'migration' => '2017_04_21_224149_create_stock_of_nums_table',
                'batch' => 1,
            ),
            35 => 
            array (
                'id' => 5157,
                'migration' => '2017_04_21_224201_create_relasi_stock_of_nums_table',
                'batch' => 1,
            ),
            36 => 
            array (
                'id' => 5158,
                'migration' => '2017_04_22_211838_create_data_perusahaans_table',
                'batch' => 1,
            ),
            37 => 
            array (
                'id' => 5159,
                'migration' => '2017_04_30_150751_create_persediaan_awals_table',
                'batch' => 1,
            ),
            38 => 
            array (
                'id' => 5160,
                'migration' => '2017_05_05_192348_create_money_limits_table',
                'batch' => 1,
            ),
            39 => 
            array (
                'id' => 5161,
                'migration' => '2017_05_05_192409_create_cash_drawers_table',
                'batch' => 1,
            ),
            40 => 
            array (
                'id' => 5162,
                'migration' => '2017_05_05_192508_create_setoran_kasirs_table',
                'batch' => 1,
            ),
            41 => 
            array (
                'id' => 5163,
                'migration' => '2017_05_09_113504_create_akuns_table',
                'batch' => 1,
            ),
            42 => 
            array (
                'id' => 5164,
                'migration' => '2017_05_09_113535_create_jurnals_table',
                'batch' => 1,
            ),
            43 => 
            array (
                'id' => 5165,
                'migration' => '2017_05_19_195958_create_hutang_banks_table',
                'batch' => 1,
            ),
            44 => 
            array (
                'id' => 5166,
                'migration' => '2017_05_19_200314_create_bayar_hutang_banks_table',
                'batch' => 1,
            ),
            45 => 
            array (
                'id' => 5167,
                'migration' => '2017_05_19_200347_create_piutang_lains_table',
                'batch' => 1,
            ),
            46 => 
            array (
                'id' => 5168,
                'migration' => '2017_05_19_200412_create_bayar_piutang_lains_table',
                'batch' => 1,
            ),
            47 => 
            array (
                'id' => 5169,
                'migration' => '2017_05_25_234916_create_piutang_dagangs_table',
                'batch' => 1,
            ),
            48 => 
            array (
                'id' => 5170,
                'migration' => '2017_05_26_002723_create_bayar_piutang_dagangs_table',
                'batch' => 1,
            ),
            49 => 
            array (
                'id' => 5171,
                'migration' => '2017_05_29_141004_create_perkaps_table',
                'batch' => 1,
            ),
            50 => 
            array (
                'id' => 5172,
                'migration' => '2017_07_23_135850_create_ceks_table',
                'batch' => 1,
            ),
            51 => 
            array (
                'id' => 5173,
                'migration' => '2017_07_23_135925_create_bgs_table',
                'batch' => 1,
            ),
            52 => 
            array (
                'id' => 5174,
                'migration' => '2017_07_26_115021_create_kendaraans_table',
                'batch' => 1,
            ),
            53 => 
            array (
                'id' => 5175,
                'migration' => '2017_08_24_105029_create_events_table',
                'batch' => 1,
            ),
            54 => 
            array (
                'id' => 5176,
                'migration' => '2017_09_18_174034_create_setoran_bukas_table',
                'batch' => 1,
            ),
            55 => 
            array (
                'id' => 5177,
                'migration' => '2017_09_18_175659_create_lacis_table',
                'batch' => 1,
            ),
            56 => 
            array (
                'id' => 5178,
                'migration' => '2017_09_19_081203_create_bangunans_table',
                'batch' => 1,
            ),
            57 => 
            array (
                'id' => 5179,
                'migration' => '2017_09_21_210410_create_penyesuaian_persediaans_table',
                'batch' => 1,
            ),
            58 => 
            array (
                'id' => 5180,
                'migration' => '2017_09_21_210433_create_relasi_penyesuaian_persediaans_table',
                'batch' => 1,
            ),
            59 => 
            array (
                'id' => 5181,
                'migration' => '2017_10_07_213058_create_saldos_table',
                'batch' => 1,
            ),
            60 => 
            array (
                'id' => 5182,
                'migration' => '2017_10_09_112808_create_buka_tahuns_table',
                'batch' => 1,
            ),
            61 => 
            array (
                'id' => 5183,
                'migration' => '2017_10_11_103716_create_paralabas_table',
                'batch' => 1,
            ),
            62 => 
            array (
                'id' => 5184,
                'migration' => '2017_10_12_073443_create_jurnal_penutups_table',
                'batch' => 1,
            ),
            63 => 
            array (
                'id' => 5185,
                'migration' => '2017_10_15_170857_create_relasi_bonus_penjualans_table',
                'batch' => 1,
            ),
            64 => 
            array (
                'id' => 5186,
                'migration' => '2017_10_16_105648_create_periode_orders_table',
                'batch' => 1,
            ),
            65 => 
            array (
                'id' => 5187,
                'migration' => '2017_11_18_182555_create_relasi_stok_penjualans_table',
                'batch' => 1,
            ),
            66 => 
            array (
                'id' => 5188,
                'migration' => '2017_11_23_223601_create_aruses_table',
                'batch' => 1,
            ),
            67 => 
            array (
                'id' => 5189,
                'migration' => '2017_11_30_200201_create_relasi_stok_retur_penjualans_table',
                'batch' => 1,
            ),
            68 => 
            array (
                'id' => 5190,
                'migration' => '2017_12_01_170901_create_pembayaran_retur_penjualans_table',
                'batch' => 1,
            ),
            69 => 
            array (
                'id' => 5191,
                'migration' => '2017_12_20_221215_create_catatans_table',
                'batch' => 1,
            ),
            70 => 
            array (
                'id' => 5192,
                'migration' => '2017_12_23_041206_create_notices_table',
                'batch' => 1,
            ),
            71 => 
            array (
                'id' => 5193,
                'migration' => '2018_01_06_000849_create_relasi_bundles_table',
                'batch' => 1,
            ),
            72 => 
            array (
                'id' => 5194,
                'migration' => '2018_01_06_000911_create_history_jual_bundles_table',
                'batch' => 1,
            ),
            73 => 
            array (
                'id' => 5195,
                'migration' => '2018_01_17_131747_create___item1s_table',
                'batch' => 1,
            ),
            74 => 
            array (
                'id' => 5196,
                'migration' => '2018_01_17_131800_create___item2s_table',
                'batch' => 1,
            ),
            75 => 
            array (
                'id' => 5197,
                'migration' => '2018_01_17_132247_create___item3s_table',
                'batch' => 1,
            ),
            76 => 
            array (
                'id' => 5198,
                'migration' => '2018_01_17_132252_create___item4s_table',
                'batch' => 1,
            ),
            77 => 
            array (
                'id' => 5199,
                'migration' => '2018_01_17_132256_create___item5s_table',
                'batch' => 1,
            ),
            78 => 
            array (
                'id' => 5200,
                'migration' => '2018_01_26_202734_create_log_lacis_table',
                'batch' => 1,
            ),
            79 => 
            array (
                'id' => 5201,
                'migration' => '2018_02_03_154828_create_bonus_rules_table',
                'batch' => 1,
            ),
            80 => 
            array (
                'id' => 5202,
                'migration' => '2018_02_03_154857_create_relasi_bonus_rules_table',
                'batch' => 1,
            ),
            81 => 
            array (
                'id' => 5203,
                'migration' => '2018_02_03_154907_create_relasi_bonus_rules_penjualans_table',
                'batch' => 1,
            ),
            82 => 
            array (
                'id' => 5204,
                'migration' => '2018_02_28_184020_create_retur_rules_table',
                'batch' => 1,
            ),
            83 => 
            array (
                'id' => 5205,
                'migration' => '2018_03_18_135017_create_cetak_notas_table',
                'batch' => 1,
            ),
            84 => 
            array (
                'id' => 5206,
                'migration' => '2018_03_24_203922_create_persen_pajaks_table',
                'batch' => 1,
            ),
        ));
        
        
    }
}