<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('levels')->delete();
        
        \DB::table('levels')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode' => '001',
                'nama' => 'Owner',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
            1 => 
            array (
                'id' => 2,
                'kode' => '002',
                'nama' => 'Admin',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
            2 => 
            array (
                'id' => 3,
                'kode' => '003',
                'nama' => 'Kasir Eceran',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
            3 => 
            array (
                'id' => 4,
                'kode' => '004',
                'nama' => 'Kasir Grosir',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
            4 => 
            array (
                'id' => 5,
                'kode' => '005',
                'nama' => 'Gudang',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
            5 => 
            array (
                'id' => 6,
                'kode' => '006',
                'nama' => 'Terdaftar',
                'created_at' => '2018-03-01 00:23:35',
                'updated_at' => '2018-03-01 00:23:35',
            ),
        ));
        
        
    }
}