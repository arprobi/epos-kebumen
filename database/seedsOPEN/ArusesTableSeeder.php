<?php

use Illuminate\Database\Seeder;

class ArusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aruses')->delete();
        
        \DB::table('aruses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'Modal',
                'nominal' => '2000000000.000',
                'created_at' => '2018-03-01 01:57:43',
                'updated_at' => '2018-03-01 01:57:43',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'Pembelian Aset',
                'nominal' => '924000000.000',
                'created_at' => '2018-03-01 02:00:24',
                'updated_at' => '2018-03-01 02:00:24',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'Pembelian Aset',
                'nominal' => '941000100.000',
                'created_at' => '2018-03-01 02:01:11',
                'updated_at' => '2018-03-01 02:01:11',
            ),
            3 => 
            array (
                'id' => 4,
                'nama' => 'Pembelian Aset',
                'nominal' => '16412700.000',
                'created_at' => '2018-03-01 02:03:34',
                'updated_at' => '2018-03-01 02:03:34',
            ),
            4 => 
            array (
                'id' => 5,
                'nama' => 'Pembelian Aset',
                'nominal' => '55917400.000',
                'created_at' => '2018-03-01 02:05:03',
                'updated_at' => '2018-03-01 02:05:03',
            ),
            5 => 
            array (
                'id' => 6,
                'nama' => 'Biaya Beban',
                'nominal' => '3382500.000',
                'created_at' => '2018-03-01 02:01:11',
                'updated_at' => '2018-03-01 02:01:11',
            ),
            6 => 
            array (
                'id' => 7,
                'nama' => 'Biaya Beban',
                'nominal' => '250000.000',
                'created_at' => '2018-03-01 02:01:11',
                'updated_at' => '2018-03-01 02:01:11',
            ),
            7 => 
            array (
                'id' => 8,
                'nama' => 'Biaya Beban',
                'nominal' => '4708000.000',
                'created_at' => '2018-03-01 02:01:11',
                'updated_at' => '2018-03-01 02:01:11',
            ),
        ));
        
        
    }
}