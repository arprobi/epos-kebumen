<?php

use Illuminate\Database\Seeder;

class RelasiSatuansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('relasi_satuans')->delete();
        
        \DB::table('relasi_satuans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_kode' => '1100120010473',
                'satuan_id' => 2,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:21:20',
                'updated_at' => '2018-03-29 12:21:20',
            ),
            1 => 
            array (
                'id' => 2,
                'item_kode' => '1100120010473',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            2 => 
            array (
                'id' => 3,
                'item_kode' => '1100120010473',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            3 => 
            array (
                'id' => 4,
                'item_kode' => '1101020010273',
                'satuan_id' => 2,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            4 => 
            array (
                'id' => 5,
                'item_kode' => '1101020010273',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            5 => 
            array (
                'id' => 6,
                'item_kode' => '1101020010273',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            6 => 
            array (
                'id' => 7,
                'item_kode' => '8996001600153',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            7 => 
            array (
                'id' => 8,
                'item_kode' => '8996001600153',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:21',
                'updated_at' => '2018-03-29 12:21:21',
            ),
            8 => 
            array (
                'id' => 9,
                'item_kode' => '8998866500319',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            9 => 
            array (
                'id' => 10,
                'item_kode' => '8998866500319',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            10 => 
            array (
                'id' => 11,
                'item_kode' => '8998866500326',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            11 => 
            array (
                'id' => 12,
                'item_kode' => '8998866500326',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            12 => 
            array (
                'id' => 13,
                'item_kode' => '8998866500401',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            13 => 
            array (
                'id' => 14,
                'item_kode' => '8998866500401',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:22',
                'updated_at' => '2018-03-29 12:21:22',
            ),
            14 => 
            array (
                'id' => 15,
                'item_kode' => '8998866500395',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            15 => 
            array (
                'id' => 16,
                'item_kode' => '8998866500395',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            16 => 
            array (
                'id' => 17,
                'item_kode' => '8998866500715',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            17 => 
            array (
                'id' => 18,
                'item_kode' => '8998866500715',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            18 => 
            array (
                'id' => 19,
                'item_kode' => '8997007300016',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            19 => 
            array (
                'id' => 20,
                'item_kode' => '8997007300016',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:23',
                'updated_at' => '2018-03-29 12:21:23',
            ),
            20 => 
            array (
                'id' => 21,
                'item_kode' => '8997007300023',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:24',
                'updated_at' => '2018-03-29 12:21:24',
            ),
            21 => 
            array (
                'id' => 22,
                'item_kode' => '8997007300023',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:24',
                'updated_at' => '2018-03-29 12:21:24',
            ),
            22 => 
            array (
                'id' => 23,
                'item_kode' => '8886008101046',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:24',
                'updated_at' => '2018-03-29 12:21:24',
            ),
            23 => 
            array (
                'id' => 24,
                'item_kode' => '8886008101046',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:21:24',
                'updated_at' => '2018-03-29 12:21:24',
            ),
            24 => 
            array (
                'id' => 25,
                'item_kode' => '4902430563864',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            25 => 
            array (
                'id' => 26,
                'item_kode' => '4902430563864',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            26 => 
            array (
                'id' => 27,
                'item_kode' => '4987176004611',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            27 => 
            array (
                'id' => 28,
                'item_kode' => '4987176004611',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            28 => 
            array (
                'id' => 29,
                'item_kode' => '711844330146',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            29 => 
            array (
                'id' => 30,
                'item_kode' => '711844330146',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:21:25',
                'updated_at' => '2018-03-29 12:21:25',
            ),
            30 => 
            array (
                'id' => 31,
                'item_kode' => '8886001100855',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            31 => 
            array (
                'id' => 32,
                'item_kode' => '8886001100855',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            32 => 
            array (
                'id' => 33,
                'item_kode' => '8886008101053',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            33 => 
            array (
                'id' => 34,
                'item_kode' => '8886008101053',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            34 => 
            array (
                'id' => 35,
                'item_kode' => '8886008101091',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            35 => 
            array (
                'id' => 36,
                'item_kode' => '8886008101091',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:26',
                'updated_at' => '2018-03-29 12:21:26',
            ),
            36 => 
            array (
                'id' => 37,
                'item_kode' => '8886008101336',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            37 => 
            array (
                'id' => 38,
                'item_kode' => '8886008101336',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            38 => 
            array (
                'id' => 39,
                'item_kode' => '8886022830243',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            39 => 
            array (
                'id' => 40,
                'item_kode' => '8886022830243',
                'satuan_id' => 6,
                'konversi' => 288,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            40 => 
            array (
                'id' => 41,
                'item_kode' => '8886022830243',
                'satuan_id' => 8,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            41 => 
            array (
                'id' => 42,
                'item_kode' => '8886029101049',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            42 => 
            array (
                'id' => 43,
                'item_kode' => '8886029101049',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:21:27',
                'updated_at' => '2018-03-29 12:21:27',
            ),
            43 => 
            array (
                'id' => 44,
                'item_kode' => '8886029511053',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            44 => 
            array (
                'id' => 45,
                'item_kode' => '8886029511053',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            45 => 
            array (
                'id' => 46,
                'item_kode' => '8886057883665',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            46 => 
            array (
                'id' => 47,
                'item_kode' => '8886057883665',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            47 => 
            array (
                'id' => 48,
                'item_kode' => '8886057883665',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 16:09:49',
            ),
            48 => 
            array (
                'id' => 49,
                'item_kode' => '89686017076',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            49 => 
            array (
                'id' => 50,
                'item_kode' => '89686017076',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:21:28',
                'updated_at' => '2018-03-29 12:21:28',
            ),
            50 => 
            array (
                'id' => 51,
                'item_kode' => '8991002101265',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            51 => 
            array (
                'id' => 52,
                'item_kode' => '8991002101265',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            52 => 
            array (
                'id' => 53,
                'item_kode' => '8991002101333',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            53 => 
            array (
                'id' => 54,
                'item_kode' => '8991002101333',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            54 => 
            array (
                'id' => 55,
                'item_kode' => '8991002101630',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            55 => 
            array (
                'id' => 56,
                'item_kode' => '8991002101630',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:29',
                'updated_at' => '2018-03-29 12:21:29',
            ),
            56 => 
            array (
                'id' => 57,
                'item_kode' => '8991002103238',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 16:14:48',
            ),
            57 => 
            array (
                'id' => 58,
                'item_kode' => '8991002103238',
                'satuan_id' => 6,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            58 => 
            array (
                'id' => 59,
                'item_kode' => '8991002103238',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            59 => 
            array (
                'id' => 60,
                'item_kode' => '8991002103832',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            60 => 
            array (
                'id' => 61,
                'item_kode' => '8991002103832',
                'satuan_id' => 6,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            61 => 
            array (
                'id' => 62,
                'item_kode' => '8991002103832',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            62 => 
            array (
                'id' => 63,
                'item_kode' => '8991002105423',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            63 => 
            array (
                'id' => 64,
                'item_kode' => '8991002105423',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:21:30',
                'updated_at' => '2018-03-29 12:21:30',
            ),
            64 => 
            array (
                'id' => 65,
                'item_kode' => '8991002105430',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 16:16:29',
            ),
            65 => 
            array (
                'id' => 66,
                'item_kode' => '8991002105430',
                'satuan_id' => 10,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 16:16:21',
            ),
            66 => 
            array (
                'id' => 67,
                'item_kode' => '8991002105454',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 12:21:31',
            ),
            67 => 
            array (
                'id' => 68,
                'item_kode' => '8991002105454',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 12:21:31',
            ),
            68 => 
            array (
                'id' => 69,
                'item_kode' => '8991002105485',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 12:21:31',
            ),
            69 => 
            array (
                'id' => 70,
                'item_kode' => '8991002105485',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:31',
                'updated_at' => '2018-03-29 12:21:31',
            ),
            70 => 
            array (
                'id' => 71,
                'item_kode' => '8991002109148',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:32',
                'updated_at' => '2018-03-29 12:21:32',
            ),
            71 => 
            array (
                'id' => 72,
                'item_kode' => '8991002109148',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:32',
                'updated_at' => '2018-03-29 12:21:32',
            ),
            72 => 
            array (
                'id' => 73,
                'item_kode' => '8991102020152',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:32',
                'updated_at' => '2018-03-29 12:21:32',
            ),
            73 => 
            array (
                'id' => 74,
                'item_kode' => '8991102020152',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:21:32',
                'updated_at' => '2018-03-29 12:21:32',
            ),
            74 => 
            array (
                'id' => 75,
                'item_kode' => '8991102020152',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:33',
                'updated_at' => '2018-03-29 12:21:33',
            ),
            75 => 
            array (
                'id' => 76,
                'item_kode' => '8991102222004',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:33',
                'updated_at' => '2018-03-29 12:21:33',
            ),
            76 => 
            array (
                'id' => 77,
                'item_kode' => '8991102222004',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:33',
                'updated_at' => '2018-03-29 12:21:33',
            ),
            77 => 
            array (
                'id' => 78,
                'item_kode' => '8991102228008',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:33',
                'updated_at' => '2018-03-29 12:21:33',
            ),
            78 => 
            array (
                'id' => 79,
                'item_kode' => '8991102228008',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:33',
                'updated_at' => '2018-03-29 12:21:33',
            ),
            79 => 
            array (
                'id' => 80,
                'item_kode' => '8991689368012',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            80 => 
            array (
                'id' => 81,
                'item_kode' => '8991689368012',
                'satuan_id' => 6,
                'konversi' => 288,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            81 => 
            array (
                'id' => 82,
                'item_kode' => '8991689368012',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            82 => 
            array (
                'id' => 83,
                'item_kode' => '8991689368029',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            83 => 
            array (
                'id' => 84,
                'item_kode' => '8991689368029',
                'satuan_id' => 6,
                'konversi' => 288,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            84 => 
            array (
                'id' => 85,
                'item_kode' => '8991689368029',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            85 => 
            array (
                'id' => 86,
                'item_kode' => '8992112014018',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 13:35:30',
            ),
            86 => 
            array (
                'id' => 87,
                'item_kode' => '8992112014018',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:21:34',
                'updated_at' => '2018-03-29 12:21:34',
            ),
            87 => 
            array (
                'id' => 88,
                'item_kode' => '8992112025021',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:35',
                'updated_at' => '2018-03-29 12:21:35',
            ),
            88 => 
            array (
                'id' => 89,
                'item_kode' => '8992112025021',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:21:35',
                'updated_at' => '2018-03-29 12:21:35',
            ),
            89 => 
            array (
                'id' => 90,
                'item_kode' => '8992222010214',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:35',
                'updated_at' => '2018-03-29 12:21:35',
            ),
            90 => 
            array (
                'id' => 92,
                'item_kode' => '8992694242502',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:35',
                'updated_at' => '2018-03-29 12:21:35',
            ),
            91 => 
            array (
                'id' => 93,
                'item_kode' => '8992694247163',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:35',
                'updated_at' => '2018-03-29 12:21:35',
            ),
            92 => 
            array (
                'id' => 94,
                'item_kode' => '8992695110206',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            93 => 
            array (
                'id' => 95,
                'item_kode' => '8992695110206',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            94 => 
            array (
                'id' => 96,
                'item_kode' => '8992695190208',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            95 => 
            array (
                'id' => 97,
                'item_kode' => '8992695190208',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            96 => 
            array (
                'id' => 98,
                'item_kode' => '8992695203441',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            97 => 
            array (
                'id' => 99,
                'item_kode' => '8992695203441',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:36',
                'updated_at' => '2018-03-29 12:21:36',
            ),
            98 => 
            array (
                'id' => 100,
                'item_kode' => '8992753031894',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:21:37',
                'updated_at' => '2018-03-29 12:21:37',
            ),
            99 => 
            array (
                'id' => 101,
                'item_kode' => '8992753031894',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:37',
                'updated_at' => '2018-03-29 12:21:37',
            ),
            100 => 
            array (
                'id' => 102,
                'item_kode' => '8992753100101',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:37',
                'updated_at' => '2018-03-29 12:21:37',
            ),
            101 => 
            array (
                'id' => 103,
                'item_kode' => '8992753100101',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:21:37',
                'updated_at' => '2018-03-29 12:21:37',
            ),
            102 => 
            array (
                'id' => 104,
                'item_kode' => '8992753101207',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:37',
                'updated_at' => '2018-03-29 12:21:37',
            ),
            103 => 
            array (
                'id' => 105,
                'item_kode' => '8992753101207',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:21:38',
                'updated_at' => '2018-03-29 12:21:38',
            ),
            104 => 
            array (
                'id' => 106,
                'item_kode' => '8992753102204',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:38',
                'updated_at' => '2018-03-29 12:21:38',
            ),
            105 => 
            array (
                'id' => 107,
                'item_kode' => '8992753102204',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:21:38',
                'updated_at' => '2018-03-29 12:21:38',
            ),
            106 => 
            array (
                'id' => 108,
                'item_kode' => '8992753102006',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:21:38',
                'updated_at' => '2018-03-29 12:21:38',
            ),
            107 => 
            array (
                'id' => 109,
                'item_kode' => '8992753102006',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:38',
                'updated_at' => '2018-03-29 12:21:38',
            ),
            108 => 
            array (
                'id' => 110,
                'item_kode' => '8992753631209',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 12:21:39',
            ),
            109 => 
            array (
                'id' => 111,
                'item_kode' => '8992753631209',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 12:21:39',
            ),
            110 => 
            array (
                'id' => 112,
                'item_kode' => '8992761002022',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 12:21:39',
            ),
            111 => 
            array (
                'id' => 113,
                'item_kode' => '8992761002022',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 16:22:20',
            ),
            112 => 
            array (
                'id' => 114,
                'item_kode' => '8992761002039',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 12:21:39',
            ),
            113 => 
            array (
                'id' => 115,
                'item_kode' => '8992761002039',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:39',
                'updated_at' => '2018-03-29 16:22:35',
            ),
            114 => 
            array (
                'id' => 116,
                'item_kode' => '8992761145026',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 12:21:40',
            ),
            115 => 
            array (
                'id' => 117,
                'item_kode' => '8992761145026',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 16:23:03',
            ),
            116 => 
            array (
                'id' => 118,
                'item_kode' => '8992761145033',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 12:21:40',
            ),
            117 => 
            array (
                'id' => 119,
                'item_kode' => '8992761145033',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 16:23:33',
            ),
            118 => 
            array (
                'id' => 120,
                'item_kode' => '8992761166038',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 12:21:40',
            ),
            119 => 
            array (
                'id' => 121,
                'item_kode' => '8992761166038',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:40',
                'updated_at' => '2018-03-29 16:23:48',
            ),
            120 => 
            array (
                'id' => 122,
                'item_kode' => '8992765101011',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:41',
                'updated_at' => '2018-03-29 12:21:41',
            ),
            121 => 
            array (
                'id' => 123,
                'item_kode' => '8992765101011',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:21:41',
                'updated_at' => '2018-03-29 13:01:48',
            ),
            122 => 
            array (
                'id' => 126,
                'item_kode' => '8992770033130',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:21:41',
                'updated_at' => '2018-03-29 12:21:41',
            ),
            123 => 
            array (
                'id' => 127,
                'item_kode' => '8992770033130',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:41',
                'updated_at' => '2018-03-29 12:21:41',
            ),
            124 => 
            array (
                'id' => 128,
                'item_kode' => '8992770033130',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:41',
                'updated_at' => '2018-03-29 12:21:41',
            ),
            125 => 
            array (
                'id' => 129,
                'item_kode' => '8992770033147',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            126 => 
            array (
                'id' => 130,
                'item_kode' => '8992770033147',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            127 => 
            array (
                'id' => 131,
                'item_kode' => '8992770033147',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            128 => 
            array (
                'id' => 132,
                'item_kode' => '8992775406038',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            129 => 
            array (
                'id' => 133,
                'item_kode' => '8992775406038',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            130 => 
            array (
                'id' => 134,
                'item_kode' => '8992775406069',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            131 => 
            array (
                'id' => 135,
                'item_kode' => '8992775406069',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:42',
                'updated_at' => '2018-03-29 12:21:42',
            ),
            132 => 
            array (
                'id' => 136,
                'item_kode' => '8992775406366',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 12:21:43',
            ),
            133 => 
            array (
                'id' => 137,
                'item_kode' => '8992775406366',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 12:21:43',
            ),
            134 => 
            array (
                'id' => 138,
                'item_kode' => '8992780010022',
                'satuan_id' => 2,
                'konversi' => 100,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 16:26:00',
            ),
            135 => 
            array (
                'id' => 139,
                'item_kode' => '8992780010022',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 16:25:45',
            ),
            136 => 
            array (
                'id' => 140,
                'item_kode' => '8992780030099',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 16:27:09',
            ),
            137 => 
            array (
                'id' => 141,
                'item_kode' => '8992780030099',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:21:43',
                'updated_at' => '2018-03-29 12:21:43',
            ),
            138 => 
            array (
                'id' => 142,
                'item_kode' => '8992780030174',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:44',
                'updated_at' => '2018-03-29 16:27:49',
            ),
            139 => 
            array (
                'id' => 143,
                'item_kode' => '8992780030174',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:21:44',
                'updated_at' => '2018-03-29 12:21:44',
            ),
            140 => 
            array (
                'id' => 144,
                'item_kode' => '8992780030228',
                'satuan_id' => 6,
                'konversi' => 8,
                'created_at' => '2018-03-29 12:21:44',
                'updated_at' => '2018-03-29 12:21:44',
            ),
            141 => 
            array (
                'id' => 145,
                'item_kode' => '8992780030228',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:44',
                'updated_at' => '2018-03-29 12:21:44',
            ),
            142 => 
            array (
                'id' => 146,
                'item_kode' => '8992780030235',
                'satuan_id' => 6,
                'konversi' => 8,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 12:21:45',
            ),
            143 => 
            array (
                'id' => 147,
                'item_kode' => '8992780030235',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 12:21:45',
            ),
            144 => 
            array (
                'id' => 148,
                'item_kode' => '8992780030723',
                'satuan_id' => 2,
                'konversi' => 100,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 16:29:36',
            ),
            145 => 
            array (
                'id' => 149,
                'item_kode' => '8992780030723',
                'satuan_id' => 8,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 16:29:07',
            ),
            146 => 
            array (
                'id' => 150,
                'item_kode' => '8992780030730',
                'satuan_id' => 2,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 16:30:06',
            ),
            147 => 
            array (
                'id' => 151,
                'item_kode' => '8992780030730',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:45',
                'updated_at' => '2018-03-29 16:29:49',
            ),
            148 => 
            array (
                'id' => 152,
                'item_kode' => '8992821100026',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            149 => 
            array (
                'id' => 153,
                'item_kode' => '8992821100026',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            150 => 
            array (
                'id' => 154,
                'item_kode' => '8992843992012',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            151 => 
            array (
                'id' => 155,
                'item_kode' => '8992843992012',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            152 => 
            array (
                'id' => 156,
                'item_kode' => '8992858245516',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            153 => 
            array (
                'id' => 157,
                'item_kode' => '8992858544817',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:46',
                'updated_at' => '2018-03-29 12:21:46',
            ),
            154 => 
            array (
                'id' => 158,
                'item_kode' => '8992858658217',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 13:42:44',
            ),
            155 => 
            array (
                'id' => 159,
                'item_kode' => '8992858658217',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 12:21:47',
            ),
            156 => 
            array (
                'id' => 160,
                'item_kode' => '8992858664713',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 13:43:37',
            ),
            157 => 
            array (
                'id' => 161,
                'item_kode' => '8992858664713',
                'satuan_id' => 8,
                'konversi' => 4,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 12:21:47',
            ),
            158 => 
            array (
                'id' => 162,
                'item_kode' => '8992858667202',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 12:21:47',
            ),
            159 => 
            array (
                'id' => 163,
                'item_kode' => '8992858667202',
                'satuan_id' => 8,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:21:47',
                'updated_at' => '2018-03-29 12:21:47',
            ),
            160 => 
            array (
                'id' => 164,
                'item_kode' => '8992866110608',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:48',
                'updated_at' => '2018-03-29 12:21:48',
            ),
            161 => 
            array (
                'id' => 166,
                'item_kode' => '8992870410206',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:48',
                'updated_at' => '2018-03-29 13:45:15',
            ),
            162 => 
            array (
                'id' => 167,
                'item_kode' => '8992870410206',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:48',
                'updated_at' => '2018-03-29 12:21:48',
            ),
            163 => 
            array (
                'id' => 168,
                'item_kode' => '8992933320114',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:48',
                'updated_at' => '2018-03-29 12:21:48',
            ),
            164 => 
            array (
                'id' => 170,
                'item_kode' => '8992933321111',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:49',
                'updated_at' => '2018-03-29 12:21:49',
            ),
            165 => 
            array (
                'id' => 172,
                'item_kode' => '8992933321111',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:49',
                'updated_at' => '2018-03-29 12:21:49',
            ),
            166 => 
            array (
                'id' => 173,
                'item_kode' => '8992933325119',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:49',
                'updated_at' => '2018-03-29 12:21:49',
            ),
            167 => 
            array (
                'id' => 175,
                'item_kode' => '8992933325119',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:50',
                'updated_at' => '2018-03-29 12:21:50',
            ),
            168 => 
            array (
                'id' => 176,
                'item_kode' => '8992933326116',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:50',
                'updated_at' => '2018-03-29 12:21:50',
            ),
            169 => 
            array (
                'id' => 178,
                'item_kode' => '8992933326116',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:50',
                'updated_at' => '2018-03-29 12:21:50',
            ),
            170 => 
            array (
                'id' => 179,
                'item_kode' => '8992933411119',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:50',
                'updated_at' => '2018-03-29 12:21:50',
            ),
            171 => 
            array (
                'id' => 181,
                'item_kode' => '8992933411119',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:50',
                'updated_at' => '2018-03-29 12:21:50',
            ),
            172 => 
            array (
                'id' => 182,
                'item_kode' => '8992946521409',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:51',
                'updated_at' => '2018-03-29 12:21:51',
            ),
            173 => 
            array (
                'id' => 183,
                'item_kode' => '8992946521409',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:21:51',
                'updated_at' => '2018-03-29 12:21:51',
            ),
            174 => 
            array (
                'id' => 184,
                'item_kode' => '8992959053232',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:51',
                'updated_at' => '2018-03-29 12:21:51',
            ),
            175 => 
            array (
                'id' => 185,
                'item_kode' => '8992959053232',
                'satuan_id' => 2,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:51',
                'updated_at' => '2018-03-29 13:15:08',
            ),
            176 => 
            array (
                'id' => 186,
                'item_kode' => '8993058105013',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 12:21:52',
            ),
            177 => 
            array (
                'id' => 187,
                'item_kode' => '8993058105013',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 12:21:52',
            ),
            178 => 
            array (
                'id' => 188,
                'item_kode' => '8993058105013',
                'satuan_id' => 8,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 16:32:35',
            ),
            179 => 
            array (
                'id' => 189,
                'item_kode' => '8993058300401',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 12:21:52',
            ),
            180 => 
            array (
                'id' => 191,
                'item_kode' => '8993058300401',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 12:21:52',
            ),
            181 => 
            array (
                'id' => 192,
                'item_kode' => '8993058300500',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:52',
                'updated_at' => '2018-03-29 12:21:52',
            ),
            182 => 
            array (
                'id' => 194,
                'item_kode' => '8993058300500',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:21:53',
                'updated_at' => '2018-03-29 12:21:53',
            ),
            183 => 
            array (
                'id' => 195,
                'item_kode' => '8993058300807',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:53',
                'updated_at' => '2018-03-29 12:21:53',
            ),
            184 => 
            array (
                'id' => 197,
                'item_kode' => '8993058300807',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:21:53',
                'updated_at' => '2018-03-29 12:21:53',
            ),
            185 => 
            array (
                'id' => 198,
                'item_kode' => '8993058302900',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:53',
                'updated_at' => '2018-03-29 12:21:53',
            ),
            186 => 
            array (
                'id' => 199,
                'item_kode' => '8993058302900',
                'satuan_id' => 3,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:21:53',
                'updated_at' => '2018-03-29 13:53:49',
            ),
            187 => 
            array (
                'id' => 200,
                'item_kode' => '8993058303105',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 12:21:54',
            ),
            188 => 
            array (
                'id' => 201,
                'item_kode' => '8993058303105',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 13:54:50',
            ),
            189 => 
            array (
                'id' => 202,
                'item_kode' => '8993176110081',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 12:21:54',
            ),
            190 => 
            array (
                'id' => 203,
                'item_kode' => '8993176110081',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 12:21:54',
            ),
            191 => 
            array (
                'id' => 204,
                'item_kode' => '8993176110098',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 12:21:54',
            ),
            192 => 
            array (
                'id' => 205,
                'item_kode' => '8993176110098',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:54',
                'updated_at' => '2018-03-29 12:21:54',
            ),
            193 => 
            array (
                'id' => 206,
                'item_kode' => '8993176110104',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            194 => 
            array (
                'id' => 207,
                'item_kode' => '8993176110104',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            195 => 
            array (
                'id' => 208,
                'item_kode' => '8993176110111',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            196 => 
            array (
                'id' => 209,
                'item_kode' => '8993176110111',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            197 => 
            array (
                'id' => 210,
                'item_kode' => '8993176110135',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            198 => 
            array (
                'id' => 211,
                'item_kode' => '8993176110135',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:55',
                'updated_at' => '2018-03-29 12:21:55',
            ),
            199 => 
            array (
                'id' => 212,
                'item_kode' => '8993176120004',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 12:21:56',
            ),
            200 => 
            array (
                'id' => 213,
                'item_kode' => '8993176120004',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 12:21:56',
            ),
            201 => 
            array (
                'id' => 214,
                'item_kode' => '8993176120028',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 12:21:56',
            ),
            202 => 
            array (
                'id' => 215,
                'item_kode' => '8993176120028',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 12:21:56',
            ),
            203 => 
            array (
                'id' => 216,
                'item_kode' => '8993176120080',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 14:04:05',
            ),
            204 => 
            array (
                'id' => 217,
                'item_kode' => '8993176120080',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:56',
                'updated_at' => '2018-03-29 12:21:56',
            ),
            205 => 
            array (
                'id' => 218,
                'item_kode' => '8993176803099',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:57',
                'updated_at' => '2018-03-29 12:21:57',
            ),
            206 => 
            array (
                'id' => 219,
                'item_kode' => '8993176803099',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:57',
                'updated_at' => '2018-03-29 12:21:57',
            ),
            207 => 
            array (
                'id' => 220,
                'item_kode' => '8993176803105',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:57',
                'updated_at' => '2018-03-29 12:21:57',
            ),
            208 => 
            array (
                'id' => 221,
                'item_kode' => '8993176803105',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:57',
                'updated_at' => '2018-03-29 12:21:57',
            ),
            209 => 
            array (
                'id' => 222,
                'item_kode' => '8993176812022',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:57',
                'updated_at' => '2018-03-29 12:21:57',
            ),
            210 => 
            array (
                'id' => 223,
                'item_kode' => '8993176812022',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 12:21:58',
            ),
            211 => 
            array (
                'id' => 224,
                'item_kode' => '8993176812039',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 12:21:58',
            ),
            212 => 
            array (
                'id' => 225,
                'item_kode' => '8993176812039',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 12:21:58',
            ),
            213 => 
            array (
                'id' => 226,
                'item_kode' => '8993188111120',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 14:56:54',
            ),
            214 => 
            array (
                'id' => 227,
                'item_kode' => '8993188111120',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 12:21:58',
            ),
            215 => 
            array (
                'id' => 228,
                'item_kode' => '8993212100069',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:58',
                'updated_at' => '2018-03-29 12:21:58',
            ),
            216 => 
            array (
                'id' => 229,
                'item_kode' => '8993212100069',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:21:59',
                'updated_at' => '2018-03-29 12:21:59',
            ),
            217 => 
            array (
                'id' => 230,
                'item_kode' => '8993338005033',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:59',
                'updated_at' => '2018-03-29 14:04:24',
            ),
            218 => 
            array (
                'id' => 231,
                'item_kode' => '8993338005033',
                'satuan_id' => 8,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:21:59',
                'updated_at' => '2018-03-29 12:21:59',
            ),
            219 => 
            array (
                'id' => 232,
                'item_kode' => '8993338008126',
                'satuan_id' => 6,
                'konversi' => 100,
                'created_at' => '2018-03-29 12:21:59',
                'updated_at' => '2018-03-29 12:21:59',
            ),
            220 => 
            array (
                'id' => 233,
                'item_kode' => '8993338008126',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:21:59',
                'updated_at' => '2018-03-29 12:21:59',
            ),
            221 => 
            array (
                'id' => 234,
                'item_kode' => '8993498210230',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            222 => 
            array (
                'id' => 235,
                'item_kode' => '8993498210230',
                'satuan_id' => 8,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            223 => 
            array (
                'id' => 236,
                'item_kode' => '8994171101289',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            224 => 
            array (
                'id' => 237,
                'item_kode' => '8994171101289',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            225 => 
            array (
                'id' => 238,
                'item_kode' => '8994297101446',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            226 => 
            array (
                'id' => 239,
                'item_kode' => '8994297101446',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            227 => 
            array (
                'id' => 240,
                'item_kode' => '8994397000045',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:00',
                'updated_at' => '2018-03-29 12:22:00',
            ),
            228 => 
            array (
                'id' => 241,
                'item_kode' => '8994397000045',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            229 => 
            array (
                'id' => 242,
                'item_kode' => '8994397000052',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            230 => 
            array (
                'id' => 243,
                'item_kode' => '8994397000052',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            231 => 
            array (
                'id' => 244,
                'item_kode' => '8994472000014',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            232 => 
            array (
                'id' => 245,
                'item_kode' => '8994472000014',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            233 => 
            array (
                'id' => 246,
                'item_kode' => '8994472000038',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:01',
                'updated_at' => '2018-03-29 12:22:01',
            ),
            234 => 
            array (
                'id' => 247,
                'item_kode' => '8994472000038',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            235 => 
            array (
                'id' => 248,
                'item_kode' => '8994472000045',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            236 => 
            array (
                'id' => 249,
                'item_kode' => '8994472000045',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            237 => 
            array (
                'id' => 250,
                'item_kode' => '8994472000052',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            238 => 
            array (
                'id' => 251,
                'item_kode' => '8994472000052',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            239 => 
            array (
                'id' => 252,
                'item_kode' => '8995179100724',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            240 => 
            array (
                'id' => 253,
                'item_kode' => '8995179100724',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:02',
                'updated_at' => '2018-03-29 12:22:02',
            ),
            241 => 
            array (
                'id' => 254,
                'item_kode' => '8995757412003',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:22:03',
                'updated_at' => '2018-03-29 12:22:03',
            ),
            242 => 
            array (
                'id' => 255,
                'item_kode' => '8995757412003',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:03',
                'updated_at' => '2018-03-29 12:22:03',
            ),
            243 => 
            array (
                'id' => 256,
                'item_kode' => '8995858999991',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:03',
                'updated_at' => '2018-03-29 12:22:03',
            ),
            244 => 
            array (
                'id' => 257,
                'item_kode' => '8995858999991',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:03',
                'updated_at' => '2018-03-29 12:22:03',
            ),
            245 => 
            array (
                'id' => 258,
                'item_kode' => '8996001301142',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            246 => 
            array (
                'id' => 259,
                'item_kode' => '8996001301142',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 16:34:39',
            ),
            247 => 
            array (
                'id' => 260,
                'item_kode' => '8996001301142',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            248 => 
            array (
                'id' => 261,
                'item_kode' => '8996001320051',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            249 => 
            array (
                'id' => 262,
                'item_kode' => '8996001320051',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            250 => 
            array (
                'id' => 263,
                'item_kode' => '8996001326220',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            251 => 
            array (
                'id' => 264,
                'item_kode' => '8996001326220',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:04',
                'updated_at' => '2018-03-29 12:22:04',
            ),
            252 => 
            array (
                'id' => 265,
                'item_kode' => '8996001326275',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 12:22:05',
            ),
            253 => 
            array (
                'id' => 266,
                'item_kode' => '8996001326275',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 12:22:05',
            ),
            254 => 
            array (
                'id' => 267,
                'item_kode' => '8996001350416',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 16:37:10',
            ),
            255 => 
            array (
                'id' => 268,
                'item_kode' => '8996001350416',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 16:36:47',
            ),
            256 => 
            array (
                'id' => 269,
                'item_kode' => '8886001038011',
                'satuan_id' => 6,
                'konversi' => 8,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 12:22:05',
            ),
            257 => 
            array (
                'id' => 270,
                'item_kode' => '8886001038011',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:05',
                'updated_at' => '2018-03-29 12:22:05',
            ),
            258 => 
            array (
                'id' => 271,
                'item_kode' => '8996001440049',
                'satuan_id' => 6,
                'konversi' => 16,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            259 => 
            array (
                'id' => 272,
                'item_kode' => '8996001440049',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            260 => 
            array (
                'id' => 273,
                'item_kode' => '8996001440087',
                'satuan_id' => 6,
                'konversi' => 16,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            261 => 
            array (
                'id' => 274,
                'item_kode' => '8996001440087',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            262 => 
            array (
                'id' => 275,
                'item_kode' => '8996001440124',
                'satuan_id' => 6,
                'konversi' => 16,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            263 => 
            array (
                'id' => 276,
                'item_kode' => '8996001440124',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:06',
                'updated_at' => '2018-03-29 12:22:06',
            ),
            264 => 
            array (
                'id' => 277,
                'item_kode' => '8996001600146',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            265 => 
            array (
                'id' => 278,
                'item_kode' => '8996001600146',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            266 => 
            array (
                'id' => 279,
                'item_kode' => '8996001600269',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            267 => 
            array (
                'id' => 280,
                'item_kode' => '8996001600269',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            268 => 
            array (
                'id' => 281,
                'item_kode' => '8996006853127',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            269 => 
            array (
                'id' => 282,
                'item_kode' => '8996006853127',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:07',
                'updated_at' => '2018-03-29 12:22:07',
            ),
            270 => 
            array (
                'id' => 283,
                'item_kode' => '8997012330664',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:08',
                'updated_at' => '2018-03-29 12:22:08',
            ),
            271 => 
            array (
                'id' => 285,
                'item_kode' => '8997012330664',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:08',
                'updated_at' => '2018-03-29 12:22:08',
            ),
            272 => 
            array (
                'id' => 286,
                'item_kode' => '8997013148688',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:08',
                'updated_at' => '2018-03-29 12:22:08',
            ),
            273 => 
            array (
                'id' => 287,
                'item_kode' => '8997013148688',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:08',
                'updated_at' => '2018-03-29 12:22:08',
            ),
            274 => 
            array (
                'id' => 288,
                'item_kode' => '8997021870014',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:08',
                'updated_at' => '2018-03-29 12:22:08',
            ),
            275 => 
            array (
                'id' => 290,
                'item_kode' => '8997021870052',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:09',
                'updated_at' => '2018-03-29 12:22:09',
            ),
            276 => 
            array (
                'id' => 292,
                'item_kode' => '8997021870076',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:09',
                'updated_at' => '2018-03-29 12:22:09',
            ),
            277 => 
            array (
                'id' => 294,
                'item_kode' => '8997021870090',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:09',
                'updated_at' => '2018-03-29 12:22:09',
            ),
            278 => 
            array (
                'id' => 296,
                'item_kode' => '8997021870137',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:10',
                'updated_at' => '2018-03-29 12:22:10',
            ),
            279 => 
            array (
                'id' => 298,
                'item_kode' => '8997021870151',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:10',
                'updated_at' => '2018-03-29 12:22:10',
            ),
            280 => 
            array (
                'id' => 300,
                'item_kode' => '8997024910045',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:10',
                'updated_at' => '2018-03-29 12:22:10',
            ),
            281 => 
            array (
                'id' => 301,
                'item_kode' => '8997027300072',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 14:57:11',
            ),
            282 => 
            array (
                'id' => 302,
                'item_kode' => '8997027300072',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 12:22:11',
            ),
            283 => 
            array (
                'id' => 303,
                'item_kode' => '8997035563414',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 12:22:11',
            ),
            284 => 
            array (
                'id' => 304,
                'item_kode' => '8997035563414',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 12:22:11',
            ),
            285 => 
            array (
                'id' => 305,
                'item_kode' => '8997035563544',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 12:22:11',
            ),
            286 => 
            array (
                'id' => 306,
                'item_kode' => '8997035563544',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:11',
                'updated_at' => '2018-03-29 12:22:11',
            ),
            287 => 
            array (
                'id' => 308,
                'item_kode' => '8997204240030',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:12',
                'updated_at' => '2018-03-29 16:41:34',
            ),
            288 => 
            array (
                'id' => 309,
                'item_kode' => '8997204240030',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:12',
                'updated_at' => '2018-03-29 16:41:28',
            ),
            289 => 
            array (
                'id' => 310,
                'item_kode' => '8997207260035',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:12',
                'updated_at' => '2018-03-29 12:22:12',
            ),
            290 => 
            array (
                'id' => 311,
                'item_kode' => '8997207260035',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:12',
                'updated_at' => '2018-03-29 12:22:12',
            ),
            291 => 
            array (
                'id' => 312,
                'item_kode' => '8997207260059',
                'satuan_id' => 2,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            292 => 
            array (
                'id' => 313,
                'item_kode' => '8997207260059',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            293 => 
            array (
                'id' => 314,
                'item_kode' => '8997207260059',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            294 => 
            array (
                'id' => 315,
                'item_kode' => '8998008151089',
                'satuan_id' => 6,
                'konversi' => 15,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            295 => 
            array (
                'id' => 316,
                'item_kode' => '8998008151089',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            296 => 
            array (
                'id' => 317,
                'item_kode' => '8998008151096',
                'satuan_id' => 6,
                'konversi' => 14,
                'created_at' => '2018-03-29 12:22:13',
                'updated_at' => '2018-03-29 12:22:13',
            ),
            297 => 
            array (
                'id' => 318,
                'item_kode' => '8998008151096',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 12:22:14',
            ),
            298 => 
            array (
                'id' => 319,
                'item_kode' => '8998008151140',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 12:22:14',
            ),
            299 => 
            array (
                'id' => 320,
                'item_kode' => '8998008151140',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 12:22:14',
            ),
            300 => 
            array (
                'id' => 321,
                'item_kode' => '8998168001071',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 12:22:14',
            ),
            301 => 
            array (
                'id' => 322,
                'item_kode' => '8998168001071',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 14:27:05',
            ),
            302 => 
            array (
                'id' => 323,
                'item_kode' => '8998168001095',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:14',
                'updated_at' => '2018-03-29 12:22:14',
            ),
            303 => 
            array (
                'id' => 324,
                'item_kode' => '8998168001095',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            304 => 
            array (
                'id' => 325,
                'item_kode' => '8998168001118',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            305 => 
            array (
                'id' => 326,
                'item_kode' => '8998168001118',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            306 => 
            array (
                'id' => 327,
                'item_kode' => '8998168001125',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            307 => 
            array (
                'id' => 328,
                'item_kode' => '8998168001125',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            308 => 
            array (
                'id' => 329,
                'item_kode' => '8998168002023',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:15',
                'updated_at' => '2018-03-29 12:22:15',
            ),
            309 => 
            array (
                'id' => 331,
                'item_kode' => '8998168002023',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:16',
                'updated_at' => '2018-03-29 14:29:08',
            ),
            310 => 
            array (
                'id' => 332,
                'item_kode' => '8998168002030',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:16',
                'updated_at' => '2018-03-29 12:22:16',
            ),
            311 => 
            array (
                'id' => 334,
                'item_kode' => '8998168002030',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:16',
                'updated_at' => '2018-03-29 12:22:16',
            ),
            312 => 
            array (
                'id' => 335,
                'item_kode' => '8998168002047',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:16',
                'updated_at' => '2018-03-29 12:22:16',
            ),
            313 => 
            array (
                'id' => 337,
                'item_kode' => '8998168002047',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:16',
                'updated_at' => '2018-03-29 12:22:16',
            ),
            314 => 
            array (
                'id' => 338,
                'item_kode' => '8998168002054',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:17',
                'updated_at' => '2018-03-29 12:22:17',
            ),
            315 => 
            array (
                'id' => 339,
                'item_kode' => '8998168002054',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:17',
                'updated_at' => '2018-03-29 12:22:17',
            ),
            316 => 
            array (
                'id' => 340,
                'item_kode' => '8998168003037',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:17',
                'updated_at' => '2018-03-29 12:22:17',
            ),
            317 => 
            array (
                'id' => 342,
                'item_kode' => '8998168003037',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:17',
                'updated_at' => '2018-03-29 12:22:17',
            ),
            318 => 
            array (
                'id' => 343,
                'item_kode' => '8998168003044',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:17',
                'updated_at' => '2018-03-29 14:31:37',
            ),
            319 => 
            array (
                'id' => 345,
                'item_kode' => '8998168003044',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:18',
                'updated_at' => '2018-03-29 14:31:54',
            ),
            320 => 
            array (
                'id' => 346,
                'item_kode' => '8998168003051',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:18',
                'updated_at' => '2018-03-29 12:22:18',
            ),
            321 => 
            array (
                'id' => 348,
                'item_kode' => '8998168003051',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:18',
                'updated_at' => '2018-03-29 12:22:18',
            ),
            322 => 
            array (
                'id' => 349,
                'item_kode' => '8998168003068',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:18',
                'updated_at' => '2018-03-29 14:32:41',
            ),
            323 => 
            array (
                'id' => 351,
                'item_kode' => '8998168003068',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:18',
                'updated_at' => '2018-03-29 14:32:35',
            ),
            324 => 
            array (
                'id' => 352,
                'item_kode' => '8998168007028',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            325 => 
            array (
                'id' => 353,
                'item_kode' => '8998168007028',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            326 => 
            array (
                'id' => 354,
                'item_kode' => '8998667100084',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            327 => 
            array (
                'id' => 355,
                'item_kode' => '8998667100084',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            328 => 
            array (
                'id' => 356,
                'item_kode' => '8998667100169',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            329 => 
            array (
                'id' => 357,
                'item_kode' => '8998667100169',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            330 => 
            array (
                'id' => 358,
                'item_kode' => '8998667100206',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:19',
                'updated_at' => '2018-03-29 12:22:19',
            ),
            331 => 
            array (
                'id' => 359,
                'item_kode' => '8998667100206',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            332 => 
            array (
                'id' => 360,
                'item_kode' => '8998667100732',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            333 => 
            array (
                'id' => 361,
                'item_kode' => '8998667100732',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            334 => 
            array (
                'id' => 362,
                'item_kode' => '8998667100947',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            335 => 
            array (
                'id' => 363,
                'item_kode' => '8998667100947',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            336 => 
            array (
                'id' => 364,
                'item_kode' => '8998667101548',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:20',
                'updated_at' => '2018-03-29 12:22:20',
            ),
            337 => 
            array (
                'id' => 365,
                'item_kode' => '8998667101548',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:22:21',
                'updated_at' => '2018-03-29 12:22:21',
            ),
            338 => 
            array (
                'id' => 366,
                'item_kode' => '8998777110218',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:21',
                'updated_at' => '2018-03-29 12:22:21',
            ),
            339 => 
            array (
                'id' => 367,
                'item_kode' => '8998777110218',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:21',
                'updated_at' => '2018-03-29 12:22:21',
            ),
            340 => 
            array (
                'id' => 368,
                'item_kode' => '8998866200301',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:21',
                'updated_at' => '2018-03-29 12:22:21',
            ),
            341 => 
            array (
                'id' => 369,
                'item_kode' => '8998866200301',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:21',
                'updated_at' => '2018-03-29 12:22:21',
            ),
            342 => 
            array (
                'id' => 370,
                'item_kode' => '8998866200318',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            343 => 
            array (
                'id' => 371,
                'item_kode' => '8998866200318',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            344 => 
            array (
                'id' => 372,
                'item_kode' => '8998866200325',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            345 => 
            array (
                'id' => 373,
                'item_kode' => '8998866200325',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            346 => 
            array (
                'id' => 374,
                'item_kode' => '8998866200578',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            347 => 
            array (
                'id' => 375,
                'item_kode' => '8998866200578',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:22',
                'updated_at' => '2018-03-29 12:22:22',
            ),
            348 => 
            array (
                'id' => 376,
                'item_kode' => '8998866200646',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            349 => 
            array (
                'id' => 377,
                'item_kode' => '8998866200646',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            350 => 
            array (
                'id' => 378,
                'item_kode' => '8998866200745',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            351 => 
            array (
                'id' => 379,
                'item_kode' => '8998866200745',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            352 => 
            array (
                'id' => 380,
                'item_kode' => '8998866200929',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            353 => 
            array (
                'id' => 381,
                'item_kode' => '8998866200929',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:23',
                'updated_at' => '2018-03-29 12:22:23',
            ),
            354 => 
            array (
                'id' => 382,
                'item_kode' => '8998866200981',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 17:00:01',
            ),
            355 => 
            array (
                'id' => 383,
                'item_kode' => '8998866200981',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 16:59:55',
            ),
            356 => 
            array (
                'id' => 384,
                'item_kode' => '8998866201001',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 12:22:24',
            ),
            357 => 
            array (
                'id' => 385,
                'item_kode' => '8998866201001',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 12:22:24',
            ),
            358 => 
            array (
                'id' => 386,
                'item_kode' => '8998866500708',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 12:22:24',
            ),
            359 => 
            array (
                'id' => 387,
                'item_kode' => '8998866500708',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:24',
                'updated_at' => '2018-03-29 17:00:46',
            ),
            360 => 
            array (
                'id' => 388,
                'item_kode' => '8998866602389',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            361 => 
            array (
                'id' => 389,
                'item_kode' => '8998866602389',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            362 => 
            array (
                'id' => 390,
                'item_kode' => '8998866605199',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            363 => 
            array (
                'id' => 391,
                'item_kode' => '8998866605199',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            364 => 
            array (
                'id' => 392,
                'item_kode' => '8998866606769',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            365 => 
            array (
                'id' => 393,
                'item_kode' => '8998866606769',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:25',
                'updated_at' => '2018-03-29 12:22:25',
            ),
            366 => 
            array (
                'id' => 394,
                'item_kode' => '8998866607322',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:26',
                'updated_at' => '2018-03-29 12:22:26',
            ),
            367 => 
            array (
                'id' => 395,
                'item_kode' => '8998866607322',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:26',
                'updated_at' => '2018-03-29 12:22:26',
            ),
            368 => 
            array (
                'id' => 396,
                'item_kode' => '8998866607872',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:26',
                'updated_at' => '2018-03-29 12:22:26',
            ),
            369 => 
            array (
                'id' => 397,
                'item_kode' => '8998866607872',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:26',
                'updated_at' => '2018-03-29 12:22:26',
            ),
            370 => 
            array (
                'id' => 398,
                'item_kode' => '8998866608305',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:26',
                'updated_at' => '2018-03-29 12:22:26',
            ),
            371 => 
            array (
                'id' => 399,
                'item_kode' => '8998866608305',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            372 => 
            array (
                'id' => 400,
                'item_kode' => '8998866608589',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            373 => 
            array (
                'id' => 401,
                'item_kode' => '8998866608589',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            374 => 
            array (
                'id' => 402,
                'item_kode' => '8998866608602',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            375 => 
            array (
                'id' => 403,
                'item_kode' => '8998866608602',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            376 => 
            array (
                'id' => 404,
                'item_kode' => '8998866608718',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            377 => 
            array (
                'id' => 405,
                'item_kode' => '8998866608718',
                'satuan_id' => 6,
                'konversi' => 22,
                'created_at' => '2018-03-29 12:22:27',
                'updated_at' => '2018-03-29 12:22:27',
            ),
            378 => 
            array (
                'id' => 406,
                'item_kode' => '8998866608961',
                'satuan_id' => 6,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:22:28',
                'updated_at' => '2018-03-29 17:19:46',
            ),
            379 => 
            array (
                'id' => 409,
                'item_kode' => '8998866609234',
                'satuan_id' => 6,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:28',
                'updated_at' => '2018-03-29 12:22:28',
            ),
            380 => 
            array (
                'id' => 410,
                'item_kode' => '8998866609234',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:28',
                'updated_at' => '2018-03-29 12:22:28',
            ),
            381 => 
            array (
                'id' => 411,
                'item_kode' => '8998866610070',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            382 => 
            array (
                'id' => 412,
                'item_kode' => '8998866610070',
                'satuan_id' => 6,
                'konversi' => 22,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            383 => 
            array (
                'id' => 413,
                'item_kode' => '8998866610261',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            384 => 
            array (
                'id' => 414,
                'item_kode' => '8998866610261',
                'satuan_id' => 6,
                'konversi' => 22,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            385 => 
            array (
                'id' => 415,
                'item_kode' => '8998866671729',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            386 => 
            array (
                'id' => 416,
                'item_kode' => '8998866671729',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:29',
                'updated_at' => '2018-03-29 12:22:29',
            ),
            387 => 
            array (
                'id' => 417,
                'item_kode' => '8998866803281',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 17:01:26',
            ),
            388 => 
            array (
                'id' => 418,
                'item_kode' => '8998866803281',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 12:22:30',
            ),
            389 => 
            array (
                'id' => 419,
                'item_kode' => '899886700',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 12:22:30',
            ),
            390 => 
            array (
                'id' => 420,
                'item_kode' => '899886700',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 12:22:30',
            ),
            391 => 
            array (
                'id' => 421,
                'item_kode' => '8998866888622',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 12:22:30',
            ),
            392 => 
            array (
                'id' => 422,
                'item_kode' => '8998866888622',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:30',
                'updated_at' => '2018-03-29 12:22:30',
            ),
            393 => 
            array (
                'id' => 423,
                'item_kode' => '8998866888653',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:31',
                'updated_at' => '2018-03-29 12:22:31',
            ),
            394 => 
            array (
                'id' => 424,
                'item_kode' => '8998866888653',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:31',
                'updated_at' => '2018-03-29 12:22:31',
            ),
            395 => 
            array (
                'id' => 425,
                'item_kode' => '8998898101409',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:31',
                'updated_at' => '2018-03-29 12:22:31',
            ),
            396 => 
            array (
                'id' => 426,
                'item_kode' => '8998898101409',
                'satuan_id' => 6,
                'konversi' => 360,
                'created_at' => '2018-03-29 12:22:31',
                'updated_at' => '2018-03-29 12:22:31',
            ),
            397 => 
            array (
                'id' => 427,
                'item_kode' => '8998898101409',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:31',
                'updated_at' => '2018-03-29 12:22:31',
            ),
            398 => 
            array (
                'id' => 428,
                'item_kode' => '8998898151404',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:32',
                'updated_at' => '2018-03-29 12:22:32',
            ),
            399 => 
            array (
                'id' => 429,
                'item_kode' => '8998898151404',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:32',
                'updated_at' => '2018-03-29 12:22:32',
            ),
            400 => 
            array (
                'id' => 430,
                'item_kode' => '8998898830125',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:32',
                'updated_at' => '2018-03-29 12:22:32',
            ),
            401 => 
            array (
                'id' => 431,
                'item_kode' => '8998898830125',
                'satuan_id' => 6,
                'konversi' => 120,
                'created_at' => '2018-03-29 12:22:32',
                'updated_at' => '2018-03-29 12:22:32',
            ),
            402 => 
            array (
                'id' => 432,
                'item_kode' => '8998898830125',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:32',
                'updated_at' => '2018-03-29 12:22:32',
            ),
            403 => 
            array (
                'id' => 433,
                'item_kode' => '8998898853100',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 12:22:33',
            ),
            404 => 
            array (
                'id' => 434,
                'item_kode' => '8998898853100',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 17:02:38',
            ),
            405 => 
            array (
                'id' => 435,
                'item_kode' => '8998899001500',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 14:57:25',
            ),
            406 => 
            array (
                'id' => 436,
                'item_kode' => '8998899001500',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 12:22:33',
            ),
            407 => 
            array (
                'id' => 437,
                'item_kode' => '89991039',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 12:22:33',
            ),
            408 => 
            array (
                'id' => 438,
                'item_kode' => '89991039',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:33',
                'updated_at' => '2018-03-29 12:22:33',
            ),
            409 => 
            array (
                'id' => 439,
                'item_kode' => '8999908000200',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 14:39:06',
            ),
            410 => 
            array (
                'id' => 441,
                'item_kode' => '8999908000200',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 12:22:34',
            ),
            411 => 
            array (
                'id' => 442,
                'item_kode' => '8999908000705',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 14:39:55',
            ),
            412 => 
            array (
                'id' => 443,
                'item_kode' => '8999908000705',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 12:22:34',
            ),
            413 => 
            array (
                'id' => 444,
                'item_kode' => '8999908057709',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 12:22:34',
            ),
            414 => 
            array (
                'id' => 446,
                'item_kode' => '8999908057709',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:34',
                'updated_at' => '2018-03-29 12:22:34',
            ),
            415 => 
            array (
                'id' => 447,
                'item_kode' => '8999908071903',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            416 => 
            array (
                'id' => 448,
                'item_kode' => '8999908071903',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            417 => 
            array (
                'id' => 449,
                'item_kode' => '8999908284907',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            418 => 
            array (
                'id' => 450,
                'item_kode' => '8999908284907',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            419 => 
            array (
                'id' => 451,
                'item_kode' => '8999908285003',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            420 => 
            array (
                'id' => 452,
                'item_kode' => '8999908285003',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:35',
                'updated_at' => '2018-03-29 12:22:35',
            ),
            421 => 
            array (
                'id' => 453,
                'item_kode' => '8999908302205',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:36',
                'updated_at' => '2018-03-29 12:22:36',
            ),
            422 => 
            array (
                'id' => 454,
                'item_kode' => '8999908302205',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:22:36',
                'updated_at' => '2018-03-29 12:22:36',
            ),
            423 => 
            array (
                'id' => 455,
                'item_kode' => '8999988888811',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:36',
                'updated_at' => '2018-03-29 12:22:36',
            ),
            424 => 
            array (
                'id' => 456,
                'item_kode' => '8999988888811',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:36',
                'updated_at' => '2018-03-29 12:22:36',
            ),
            425 => 
            array (
                'id' => 457,
                'item_kode' => '8999988888842',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:36',
                'updated_at' => '2018-03-29 12:22:36',
            ),
            426 => 
            array (
                'id' => 458,
                'item_kode' => '8999988888842',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            427 => 
            array (
                'id' => 459,
                'item_kode' => '8999988888859',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            428 => 
            array (
                'id' => 460,
                'item_kode' => '8999988888859',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            429 => 
            array (
                'id' => 461,
                'item_kode' => '8999988888866',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            430 => 
            array (
                'id' => 462,
                'item_kode' => '8999988888866',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            431 => 
            array (
                'id' => 463,
                'item_kode' => '8999988888989',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:37',
                'updated_at' => '2018-03-29 12:22:37',
            ),
            432 => 
            array (
                'id' => 464,
                'item_kode' => '8999988888989',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:38',
                'updated_at' => '2018-03-29 12:22:38',
            ),
            433 => 
            array (
                'id' => 465,
                'item_kode' => '8999988888989',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:38',
                'updated_at' => '2018-03-29 12:22:38',
            ),
            434 => 
            array (
                'id' => 466,
                'item_kode' => '8999999001117',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:38',
                'updated_at' => '2018-03-29 12:22:38',
            ),
            435 => 
            array (
                'id' => 467,
                'item_kode' => '8999999001124',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:38',
                'updated_at' => '2018-03-29 12:22:38',
            ),
            436 => 
            array (
                'id' => 468,
                'item_kode' => '8999999001230',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:38',
                'updated_at' => '2018-03-29 12:22:38',
            ),
            437 => 
            array (
                'id' => 470,
                'item_kode' => '8999999001247',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:39',
                'updated_at' => '2018-03-29 12:22:39',
            ),
            438 => 
            array (
                'id' => 472,
                'item_kode' => '8999999003074',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:39',
                'updated_at' => '2018-03-29 12:22:39',
            ),
            439 => 
            array (
                'id' => 474,
                'item_kode' => '8999999003708',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:39',
                'updated_at' => '2018-03-29 12:22:39',
            ),
            440 => 
            array (
                'id' => 475,
                'item_kode' => '8999999003708',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:39',
                'updated_at' => '2018-03-29 12:22:39',
            ),
            441 => 
            array (
                'id' => 476,
                'item_kode' => '8999999003753',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:39',
                'updated_at' => '2018-03-29 12:22:39',
            ),
            442 => 
            array (
                'id' => 478,
                'item_kode' => '8999999004194',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:40',
                'updated_at' => '2018-03-29 12:22:40',
            ),
            443 => 
            array (
                'id' => 479,
                'item_kode' => '8999999004194',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:40',
                'updated_at' => '2018-03-29 12:22:40',
            ),
            444 => 
            array (
                'id' => 480,
                'item_kode' => '8999999007782',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:40',
                'updated_at' => '2018-03-29 12:22:40',
            ),
            445 => 
            array (
                'id' => 482,
                'item_kode' => '8999999027032',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:40',
                'updated_at' => '2018-03-29 12:22:40',
            ),
            446 => 
            array (
                'id' => 483,
                'item_kode' => '8999999027032',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:41',
                'updated_at' => '2018-03-29 12:22:41',
            ),
            447 => 
            array (
                'id' => 484,
                'item_kode' => '8999999027056',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:41',
                'updated_at' => '2018-03-29 12:22:41',
            ),
            448 => 
            array (
                'id' => 485,
                'item_kode' => '8999999027056',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:41',
                'updated_at' => '2018-03-29 12:22:41',
            ),
            449 => 
            array (
                'id' => 486,
                'item_kode' => '8999999027599',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:41',
                'updated_at' => '2018-03-29 12:22:41',
            ),
            450 => 
            array (
                'id' => 487,
                'item_kode' => '8999999027599',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:41',
                'updated_at' => '2018-03-29 12:22:41',
            ),
            451 => 
            array (
                'id' => 488,
                'item_kode' => '8999999027605',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            452 => 
            array (
                'id' => 489,
                'item_kode' => '8999999027605',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            453 => 
            array (
                'id' => 490,
                'item_kode' => '8999999033125',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            454 => 
            array (
                'id' => 491,
                'item_kode' => '8999999033125',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            455 => 
            array (
                'id' => 492,
                'item_kode' => '8999999033163',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            456 => 
            array (
                'id' => 493,
                'item_kode' => '8999999033163',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:42',
                'updated_at' => '2018-03-29 12:22:42',
            ),
            457 => 
            array (
                'id' => 494,
                'item_kode' => '8999999033200',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            458 => 
            array (
                'id' => 495,
                'item_kode' => '8999999033200',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            459 => 
            array (
                'id' => 496,
                'item_kode' => '8999999036607',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            460 => 
            array (
                'id' => 497,
                'item_kode' => '8999999036607',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            461 => 
            array (
                'id' => 498,
                'item_kode' => '8999999036638',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            462 => 
            array (
                'id' => 499,
                'item_kode' => '8999999036638',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:22:43',
                'updated_at' => '2018-03-29 12:22:43',
            ),
            463 => 
            array (
                'id' => 500,
                'item_kode' => '8999999036898',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:44',
                'updated_at' => '2018-03-29 12:22:44',
            ),
            464 => 
            array (
                'id' => 501,
                'item_kode' => '8999999036904',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:44',
                'updated_at' => '2018-03-29 12:22:44',
            ),
            465 => 
            array (
                'id' => 506,
                'item_kode' => '8999999047221',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:22:45',
                'updated_at' => '2018-03-29 12:22:45',
            ),
            466 => 
            array (
                'id' => 507,
                'item_kode' => '8999999047221',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:45',
                'updated_at' => '2018-03-29 12:22:45',
            ),
            467 => 
            array (
                'id' => 508,
                'item_kode' => '8999999047245',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:22:45',
                'updated_at' => '2018-03-29 12:22:45',
            ),
            468 => 
            array (
                'id' => 509,
                'item_kode' => '8999999047245',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:45',
                'updated_at' => '2018-03-29 12:22:45',
            ),
            469 => 
            array (
                'id' => 510,
                'item_kode' => '8999999048174',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            470 => 
            array (
                'id' => 511,
                'item_kode' => '8999999048174',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            471 => 
            array (
                'id' => 512,
                'item_kode' => '8999999048259',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            472 => 
            array (
                'id' => 513,
                'item_kode' => '8999999048259',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            473 => 
            array (
                'id' => 514,
                'item_kode' => '8999999048488',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            474 => 
            array (
                'id' => 515,
                'item_kode' => '8999999048488',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:46',
                'updated_at' => '2018-03-29 12:22:46',
            ),
            475 => 
            array (
                'id' => 516,
                'item_kode' => '8999999048518',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:47',
                'updated_at' => '2018-03-29 12:22:47',
            ),
            476 => 
            array (
                'id' => 517,
                'item_kode' => '8999999048518',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:47',
                'updated_at' => '2018-03-29 12:22:47',
            ),
            477 => 
            array (
                'id' => 518,
                'item_kode' => '8999999049409',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:47',
                'updated_at' => '2018-03-29 12:22:47',
            ),
            478 => 
            array (
                'id' => 519,
                'item_kode' => '8999999049409',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:47',
                'updated_at' => '2018-03-29 12:22:47',
            ),
            479 => 
            array (
                'id' => 520,
                'item_kode' => '8999999049423',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:47',
                'updated_at' => '2018-03-29 12:22:47',
            ),
            480 => 
            array (
                'id' => 521,
                'item_kode' => '8999999049423',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:48',
                'updated_at' => '2018-03-29 12:22:48',
            ),
            481 => 
            array (
                'id' => 522,
                'item_kode' => '8999999050009',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:48',
                'updated_at' => '2018-03-29 12:22:48',
            ),
            482 => 
            array (
                'id' => 523,
                'item_kode' => '8999999050009',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:48',
                'updated_at' => '2018-03-29 12:22:48',
            ),
            483 => 
            array (
                'id' => 524,
                'item_kode' => '8999999052959',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:48',
                'updated_at' => '2018-03-29 12:22:48',
            ),
            484 => 
            array (
                'id' => 525,
                'item_kode' => '8999999052959',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:48',
                'updated_at' => '2018-03-29 12:22:48',
            ),
            485 => 
            array (
                'id' => 526,
                'item_kode' => '8999999052973',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:49',
                'updated_at' => '2018-03-29 12:22:49',
            ),
            486 => 
            array (
                'id' => 527,
                'item_kode' => '8999999052973',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:22:49',
                'updated_at' => '2018-03-29 12:22:49',
            ),
            487 => 
            array (
                'id' => 528,
                'item_kode' => '8999999055769',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:49',
                'updated_at' => '2018-03-29 12:22:49',
            ),
            488 => 
            array (
                'id' => 530,
                'item_kode' => '8999999056186',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:49',
                'updated_at' => '2018-03-29 17:29:34',
            ),
            489 => 
            array (
                'id' => 531,
                'item_kode' => '8999999056841',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:50',
                'updated_at' => '2018-03-29 12:22:50',
            ),
            490 => 
            array (
                'id' => 533,
                'item_kode' => '8999999057695',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:50',
                'updated_at' => '2018-03-29 12:22:50',
            ),
            491 => 
            array (
                'id' => 534,
                'item_kode' => '8999999057695',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:50',
                'updated_at' => '2018-03-29 17:30:18',
            ),
            492 => 
            array (
                'id' => 535,
                'item_kode' => '8999999059323',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:50',
                'updated_at' => '2018-03-29 12:22:50',
            ),
            493 => 
            array (
                'id' => 536,
                'item_kode' => '8999999059323',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:22:50',
                'updated_at' => '2018-03-29 12:22:50',
            ),
            494 => 
            array (
                'id' => 537,
                'item_kode' => '8999999059781',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:51',
                'updated_at' => '2018-03-29 12:22:51',
            ),
            495 => 
            array (
                'id' => 538,
                'item_kode' => '8999999059781',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:51',
                'updated_at' => '2018-03-29 12:22:51',
            ),
            496 => 
            array (
                'id' => 539,
                'item_kode' => '8999999195649',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:51',
                'updated_at' => '2018-03-29 17:03:21',
            ),
            497 => 
            array (
                'id' => 540,
                'item_kode' => '8999999195649',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:51',
                'updated_at' => '2018-03-29 12:22:51',
            ),
            498 => 
            array (
                'id' => 541,
                'item_kode' => '8999999390198',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:52',
                'updated_at' => '2018-03-29 12:22:52',
            ),
            499 => 
            array (
                'id' => 542,
                'item_kode' => '8999999390198',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:52',
                'updated_at' => '2018-03-29 12:22:52',
            ),
        ));
        \DB::table('relasi_satuans')->insert(array (
            0 => 
            array (
                'id' => 543,
                'item_kode' => '8999999406936',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:52',
                'updated_at' => '2018-03-29 12:22:52',
            ),
            1 => 
            array (
                'id' => 544,
                'item_kode' => '8999999406950',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:52',
                'updated_at' => '2018-03-29 12:22:52',
            ),
            2 => 
            array (
                'id' => 545,
                'item_kode' => '8999999407872',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:52',
                'updated_at' => '2018-03-29 12:22:52',
            ),
            3 => 
            array (
                'id' => 546,
                'item_kode' => '8999999504014',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 12:22:53',
            ),
            4 => 
            array (
                'id' => 547,
                'item_kode' => '8999999504014',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 12:22:53',
            ),
            5 => 
            array (
                'id' => 548,
                'item_kode' => '8999999504021',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 12:22:53',
            ),
            6 => 
            array (
                'id' => 549,
                'item_kode' => '8999999504021',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 15:00:30',
            ),
            7 => 
            array (
                'id' => 550,
                'item_kode' => '8999999504038',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 12:22:53',
            ),
            8 => 
            array (
                'id' => 551,
                'item_kode' => '8999999504038',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:53',
                'updated_at' => '2018-03-29 12:22:53',
            ),
            9 => 
            array (
                'id' => 552,
                'item_kode' => '8999999506544',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:54',
                'updated_at' => '2018-03-29 12:22:54',
            ),
            10 => 
            array (
                'id' => 553,
                'item_kode' => '8999999506544',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:54',
                'updated_at' => '2018-03-29 12:22:54',
            ),
            11 => 
            array (
                'id' => 554,
                'item_kode' => '8999999706081',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:54',
                'updated_at' => '2018-03-29 12:22:54',
            ),
            12 => 
            array (
                'id' => 555,
                'item_kode' => '8999999706081',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 12:22:54',
                'updated_at' => '2018-03-29 12:22:54',
            ),
            13 => 
            array (
                'id' => 556,
                'item_kode' => '8999999706081',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:54',
                'updated_at' => '2018-03-29 12:22:54',
            ),
            14 => 
            array (
                'id' => 557,
                'item_kode' => '8999999706173',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            15 => 
            array (
                'id' => 558,
                'item_kode' => '8999999706173',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            16 => 
            array (
                'id' => 559,
                'item_kode' => '8999999706173',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            17 => 
            array (
                'id' => 560,
                'item_kode' => '8999999706180',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            18 => 
            array (
                'id' => 561,
                'item_kode' => '8999999706180',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            19 => 
            array (
                'id' => 562,
                'item_kode' => '8999999706180',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            20 => 
            array (
                'id' => 563,
                'item_kode' => '8999999707774',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            21 => 
            array (
                'id' => 564,
                'item_kode' => '8999999707774',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:55',
                'updated_at' => '2018-03-29 12:22:55',
            ),
            22 => 
            array (
                'id' => 565,
                'item_kode' => '8999999707835',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            23 => 
            array (
                'id' => 566,
                'item_kode' => '8999999707835',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            24 => 
            array (
                'id' => 567,
                'item_kode' => '8999999707835',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            25 => 
            array (
                'id' => 568,
                'item_kode' => '8999999710880',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            26 => 
            array (
                'id' => 569,
                'item_kode' => '8999999710880',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            27 => 
            array (
                'id' => 570,
                'item_kode' => '8999999719395',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:56',
                'updated_at' => '2018-03-29 12:22:56',
            ),
            28 => 
            array (
                'id' => 572,
                'item_kode' => '8998866500951',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            29 => 
            array (
                'id' => 573,
                'item_kode' => '8998866500951',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            30 => 
            array (
                'id' => 574,
                'item_kode' => '8997007680057',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            31 => 
            array (
                'id' => 575,
                'item_kode' => '8997007680057',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            32 => 
            array (
                'id' => 576,
                'item_kode' => 'KMKOCELCEFUTK',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            33 => 
            array (
                'id' => 577,
                'item_kode' => 'KMKOCELCEFUTK',
                'satuan_id' => 6,
                'konversi' => 45,
                'created_at' => '2018-03-29 12:22:57',
                'updated_at' => '2018-03-29 12:22:57',
            ),
            34 => 
            array (
                'id' => 578,
                'item_kode' => '8993498210018',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            35 => 
            array (
                'id' => 579,
                'item_kode' => '8993498210018',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            36 => 
            array (
                'id' => 580,
                'item_kode' => '8993498210285',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            37 => 
            array (
                'id' => 581,
                'item_kode' => '8993498210285',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            38 => 
            array (
                'id' => 582,
                'item_kode' => '8998866607889',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            39 => 
            array (
                'id' => 583,
                'item_kode' => '8998866607889',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:22:58',
                'updated_at' => '2018-03-29 12:22:58',
            ),
            40 => 
            array (
                'id' => 584,
                'item_kode' => '8993051888869',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 12:22:59',
            ),
            41 => 
            array (
                'id' => 585,
                'item_kode' => '8993051888869',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 12:22:59',
            ),
            42 => 
            array (
                'id' => 586,
                'item_kode' => '8991002105584',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 12:22:59',
            ),
            43 => 
            array (
                'id' => 587,
                'item_kode' => '8991002105584',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 12:22:59',
            ),
            44 => 
            array (
                'id' => 588,
                'item_kode' => '8997207260011',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 12:22:59',
            ),
            45 => 
            array (
                'id' => 589,
                'item_kode' => '8997207260011',
                'satuan_id' => 13,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:22:59',
                'updated_at' => '2018-03-29 17:08:07',
            ),
            46 => 
            array (
                'id' => 590,
                'item_kode' => '8993058301200',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:00',
                'updated_at' => '2018-03-29 12:23:00',
            ),
            47 => 
            array (
                'id' => 592,
                'item_kode' => '8993058301200',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:23:00',
                'updated_at' => '2018-03-29 12:23:00',
            ),
            48 => 
            array (
                'id' => 593,
                'item_kode' => 'KMKVASUWTDQRP',
                'satuan_id' => 2,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:23:00',
                'updated_at' => '2018-03-29 13:23:50',
            ),
            49 => 
            array (
                'id' => 594,
                'item_kode' => 'KMKVASUWTDQRP',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:23:00',
                'updated_at' => '2018-03-29 13:23:20',
            ),
            50 => 
            array (
                'id' => 595,
                'item_kode' => 'KMKRZUKFEBPWL',
                'satuan_id' => 2,
                'konversi' => 60,
                'created_at' => '2018-03-29 12:23:00',
                'updated_at' => '2018-03-29 13:24:43',
            ),
            51 => 
            array (
                'id' => 596,
                'item_kode' => 'KMKRZUKFEBPWL',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 13:24:37',
            ),
            52 => 
            array (
                'id' => 597,
                'item_kode' => 'KMKUWVQFPGQYQ',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            53 => 
            array (
                'id' => 598,
                'item_kode' => 'KMKUWVQFPGQYQ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            54 => 
            array (
                'id' => 599,
                'item_kode' => 'KMKCYYVTZEAMW',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            55 => 
            array (
                'id' => 600,
                'item_kode' => 'KMKCYYVTZEAMW',
                'satuan_id' => 6,
                'konversi' => 1000,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            56 => 
            array (
                'id' => 601,
                'item_kode' => 'KMKCYYVTZEAMW',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            57 => 
            array (
                'id' => 602,
                'item_kode' => 'KMKGTSUBCSDPP',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:01',
                'updated_at' => '2018-03-29 12:23:01',
            ),
            58 => 
            array (
                'id' => 603,
                'item_kode' => 'KMKMITRNKOCBR',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:02',
                'updated_at' => '2018-03-29 12:23:02',
            ),
            59 => 
            array (
                'id' => 604,
                'item_kode' => 'KMKFCJWYXLAGM',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:02',
                'updated_at' => '2018-03-29 12:23:02',
            ),
            60 => 
            array (
                'id' => 605,
                'item_kode' => 'KMKXJUNCTYYVN',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:02',
                'updated_at' => '2018-03-29 12:23:02',
            ),
            61 => 
            array (
                'id' => 606,
                'item_kode' => 'KMKYPFAUGXDMB',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:02',
                'updated_at' => '2018-03-29 12:23:02',
            ),
            62 => 
            array (
                'id' => 607,
                'item_kode' => 'KMKXGGWDSBKIQ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:03',
                'updated_at' => '2018-03-29 12:23:03',
            ),
            63 => 
            array (
                'id' => 608,
                'item_kode' => 'KMKDUELKEEADC',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:03',
                'updated_at' => '2018-03-29 12:23:03',
            ),
            64 => 
            array (
                'id' => 609,
                'item_kode' => 'KMKVJWFUNAXAJ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:03',
                'updated_at' => '2018-03-29 12:23:03',
            ),
            65 => 
            array (
                'id' => 610,
                'item_kode' => 'KMKGBUFMCBTXK',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:03',
                'updated_at' => '2018-03-29 12:23:03',
            ),
            66 => 
            array (
                'id' => 611,
                'item_kode' => 'KMKMTQTVUTZXZ',
                'satuan_id' => 2,
                'konversi' => 25,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            67 => 
            array (
                'id' => 612,
                'item_kode' => 'KMKMTQTVUTZXZ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 13:29:08',
            ),
            68 => 
            array (
                'id' => 613,
                'item_kode' => 'KMKMTQTVUTZXZ',
                'satuan_id' => 6,
                'konversi' => 100,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            69 => 
            array (
                'id' => 614,
                'item_kode' => 'KMKWWBCBSYNCZ',
                'satuan_id' => 2,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            70 => 
            array (
                'id' => 615,
                'item_kode' => 'KMKWWBCBSYNCZ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 13:31:48',
            ),
            71 => 
            array (
                'id' => 616,
                'item_kode' => 'KMKWWBCBSYNCZ',
                'satuan_id' => 6,
                'konversi' => 120,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            72 => 
            array (
                'id' => 617,
                'item_kode' => '8999908204202',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            73 => 
            array (
                'id' => 618,
                'item_kode' => '8999908204202',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:23:04',
                'updated_at' => '2018-03-29 12:23:04',
            ),
            74 => 
            array (
                'id' => 619,
                'item_kode' => '8997208160044',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:05',
                'updated_at' => '2018-03-29 12:23:05',
            ),
            75 => 
            array (
                'id' => 621,
                'item_kode' => '8998685011003',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:23:05',
                'updated_at' => '2018-03-29 17:09:03',
            ),
            76 => 
            array (
                'id' => 623,
                'item_kode' => '8996001326404',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:23:05',
                'updated_at' => '2018-03-29 12:23:05',
            ),
            77 => 
            array (
                'id' => 624,
                'item_kode' => '8996001326404',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:05',
                'updated_at' => '2018-03-29 12:23:05',
            ),
            78 => 
            array (
                'id' => 625,
                'item_kode' => '8997013070019',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:06',
                'updated_at' => '2018-03-29 12:23:06',
            ),
            79 => 
            array (
                'id' => 626,
                'item_kode' => '8997013070019',
                'satuan_id' => 8,
                'konversi' => 50,
                'created_at' => '2018-03-29 12:23:06',
                'updated_at' => '2018-03-29 12:23:06',
            ),
            80 => 
            array (
                'id' => 627,
                'item_kode' => 'KMKVURMHALSTH',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:06',
                'updated_at' => '2018-03-29 15:18:00',
            ),
            81 => 
            array (
                'id' => 628,
                'item_kode' => 'KMKPJGPFRJZHK',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:06',
                'updated_at' => '2018-03-29 15:19:10',
            ),
            82 => 
            array (
                'id' => 629,
                'item_kode' => 'KMKWZUSAKKWOY',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:07',
                'updated_at' => '2018-03-29 15:19:35',
            ),
            83 => 
            array (
                'id' => 630,
                'item_kode' => 'KMKKVZGJDYQTM',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:07',
                'updated_at' => '2018-03-29 12:23:07',
            ),
            84 => 
            array (
                'id' => 631,
                'item_kode' => 'KMKKFWUEFHUZC',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:07',
                'updated_at' => '2018-03-29 12:23:07',
            ),
            85 => 
            array (
                'id' => 632,
                'item_kode' => 'KMKOVFGEUGXNK',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:07',
                'updated_at' => '2018-03-29 12:23:07',
            ),
            86 => 
            array (
                'id' => 633,
                'item_kode' => 'KMKETIMRVZMKZ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:08',
                'updated_at' => '2018-03-29 12:23:08',
            ),
            87 => 
            array (
                'id' => 634,
                'item_kode' => 'KMKASQOZCMGYS',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:08',
                'updated_at' => '2018-03-29 15:26:56',
            ),
            88 => 
            array (
                'id' => 635,
                'item_kode' => 'KMKANZZUNYVIL',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:08',
                'updated_at' => '2018-03-29 15:28:38',
            ),
            89 => 
            array (
                'id' => 636,
                'item_kode' => 'KMKCKKCWDHXGI',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:08',
                'updated_at' => '2018-03-29 15:29:03',
            ),
            90 => 
            array (
                'id' => 637,
                'item_kode' => 'KMKDIQSXKFPFC',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:09',
                'updated_at' => '2018-03-29 15:29:24',
            ),
            91 => 
            array (
                'id' => 638,
                'item_kode' => 'KMKQLGNTVRICW',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:09',
                'updated_at' => '2018-03-29 15:29:54',
            ),
            92 => 
            array (
                'id' => 639,
                'item_kode' => 'KMKZLQBDBEALB',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:09',
                'updated_at' => '2018-03-29 15:30:23',
            ),
            93 => 
            array (
                'id' => 640,
                'item_kode' => 'KMKARBRPVCGJC',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:10',
                'updated_at' => '2018-03-29 15:31:09',
            ),
            94 => 
            array (
                'id' => 641,
                'item_kode' => 'KMKHGWZLCFJYO',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:10',
                'updated_at' => '2018-03-29 12:23:10',
            ),
            95 => 
            array (
                'id' => 642,
                'item_kode' => '8998866888615',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:10',
                'updated_at' => '2018-03-29 12:23:10',
            ),
            96 => 
            array (
                'id' => 643,
                'item_kode' => '8998866888615',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:23:10',
                'updated_at' => '2018-03-29 12:23:10',
            ),
            97 => 
            array (
                'id' => 644,
                'item_kode' => '8999338136883',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:10',
                'updated_at' => '2018-03-29 12:23:10',
            ),
            98 => 
            array (
                'id' => 645,
                'item_kode' => '8999338136883',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 12:23:11',
                'updated_at' => '2018-03-29 12:23:11',
            ),
            99 => 
            array (
                'id' => 646,
                'item_kode' => '8998866609487',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:11',
                'updated_at' => '2018-03-29 12:23:11',
            ),
            100 => 
            array (
                'id' => 647,
                'item_kode' => '8998866609487',
                'satuan_id' => 6,
                'konversi' => 22,
                'created_at' => '2018-03-29 12:23:11',
                'updated_at' => '2018-03-29 12:23:11',
            ),
            101 => 
            array (
                'id' => 648,
                'item_kode' => '8992780030686',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 12:23:11',
                'updated_at' => '2018-03-29 12:23:11',
            ),
            102 => 
            array (
                'id' => 649,
                'item_kode' => '8992780030686',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:11',
                'updated_at' => '2018-03-29 12:23:11',
            ),
            103 => 
            array (
                'id' => 650,
                'item_kode' => '8997009510055',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:12',
                'updated_at' => '2018-03-29 12:23:12',
            ),
            104 => 
            array (
                'id' => 651,
                'item_kode' => '8997009510055',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:23:12',
                'updated_at' => '2018-03-29 12:23:12',
            ),
            105 => 
            array (
                'id' => 653,
                'item_kode' => '8992775406014',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 12:23:12',
                'updated_at' => '2018-03-29 12:23:12',
            ),
            106 => 
            array (
                'id' => 654,
                'item_kode' => '8992775406014',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:12',
                'updated_at' => '2018-03-29 12:23:12',
            ),
            107 => 
            array (
                'id' => 655,
                'item_kode' => '8998866607339',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:13',
                'updated_at' => '2018-03-29 12:23:13',
            ),
            108 => 
            array (
                'id' => 656,
                'item_kode' => '8998866607339',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:23:13',
                'updated_at' => '2018-03-29 12:23:13',
            ),
            109 => 
            array (
                'id' => 657,
                'item_kode' => '8998866803717',
                'satuan_id' => 6,
                'konversi' => 20,
                'created_at' => '2018-03-29 12:23:13',
                'updated_at' => '2018-03-29 12:23:13',
            ),
            110 => 
            array (
                'id' => 658,
                'item_kode' => '8998866803717',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:13',
                'updated_at' => '2018-03-29 12:23:13',
            ),
            111 => 
            array (
                'id' => 660,
                'item_kode' => '8998866609159',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:14',
                'updated_at' => '2018-03-29 17:34:17',
            ),
            112 => 
            array (
                'id' => 661,
                'item_kode' => '8998866609159',
                'satuan_id' => 6,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:14',
                'updated_at' => '2018-03-29 17:34:25',
            ),
            113 => 
            array (
                'id' => 663,
                'item_kode' => '8998866803618',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:14',
                'updated_at' => '2018-03-29 17:38:10',
            ),
            114 => 
            array (
                'id' => 664,
                'item_kode' => '8998866803618',
                'satuan_id' => 6,
                'konversi' => 5,
                'created_at' => '2018-03-29 12:23:14',
                'updated_at' => '2018-03-29 17:38:21',
            ),
            115 => 
            array (
                'id' => 665,
                'item_kode' => '8998866605816',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            116 => 
            array (
                'id' => 666,
                'item_kode' => '8998866605816',
                'satuan_id' => 6,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            117 => 
            array (
                'id' => 667,
                'item_kode' => '8998866805414',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            118 => 
            array (
                'id' => 668,
                'item_kode' => '8998866805414',
                'satuan_id' => 6,
                'konversi' => 6,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            119 => 
            array (
                'id' => 669,
                'item_kode' => '8998866606417',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            120 => 
            array (
                'id' => 670,
                'item_kode' => '8998866606417',
                'satuan_id' => 6,
                'konversi' => 72,
                'created_at' => '2018-03-29 12:23:15',
                'updated_at' => '2018-03-29 12:23:15',
            ),
            121 => 
            array (
                'id' => 671,
                'item_kode' => '8998866611121',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:16',
                'updated_at' => '2018-03-29 12:23:16',
            ),
            122 => 
            array (
                'id' => 672,
                'item_kode' => '8998866611121',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 12:23:16',
                'updated_at' => '2018-03-29 12:23:16',
            ),
            123 => 
            array (
                'id' => 673,
                'item_kode' => '8992759535457',
                'satuan_id' => 9,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:16',
                'updated_at' => '2018-03-29 12:23:16',
            ),
            124 => 
            array (
                'id' => 674,
                'item_kode' => '8992759535457',
                'satuan_id' => 6,
                'konversi' => 120,
                'created_at' => '2018-03-29 12:23:16',
                'updated_at' => '2018-03-29 12:23:16',
            ),
            125 => 
            array (
                'id' => 675,
                'item_kode' => 'KMKIOCZMYIGAU',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 12:23:22',
                'updated_at' => '2018-03-29 12:23:22',
            ),
            126 => 
            array (
                'id' => 678,
                'item_kode' => '8992765301008',
                'satuan_id' => 2,
                'konversi' => 24,
                'created_at' => '2018-03-29 13:08:24',
                'updated_at' => '2018-03-29 13:08:24',
            ),
            127 => 
            array (
                'id' => 679,
                'item_kode' => '8999999049508',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 13:18:20',
                'updated_at' => '2018-03-29 13:18:20',
            ),
            128 => 
            array (
                'id' => 680,
                'item_kode' => '8999999049508',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 13:18:31',
                'updated_at' => '2018-03-29 13:18:31',
            ),
            129 => 
            array (
                'id' => 681,
                'item_kode' => '8999999049485',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 13:18:54',
                'updated_at' => '2018-03-29 13:18:54',
            ),
            130 => 
            array (
                'id' => 682,
                'item_kode' => '8999999049485',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 13:19:04',
                'updated_at' => '2018-03-29 13:19:04',
            ),
            131 => 
            array (
                'id' => 683,
                'item_kode' => '1899721010',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 13:20:34',
                'updated_at' => '2018-03-29 13:20:34',
            ),
            132 => 
            array (
                'id' => 684,
                'item_kode' => 'KMKVASUWTDQRP',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 13:22:51',
                'updated_at' => '2018-03-29 13:22:51',
            ),
            133 => 
            array (
                'id' => 685,
                'item_kode' => 'KMKRZUKFEBPWL',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 13:24:55',
                'updated_at' => '2018-03-29 13:24:55',
            ),
            134 => 
            array (
                'id' => 686,
                'item_kode' => '8992858544817',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 13:42:10',
                'updated_at' => '2018-03-29 13:42:10',
            ),
            135 => 
            array (
                'id' => 687,
                'item_kode' => '8993058302900',
                'satuan_id' => 8,
                'konversi' => 30,
                'created_at' => '2018-03-29 13:52:42',
                'updated_at' => '2018-03-29 13:54:03',
            ),
            136 => 
            array (
                'id' => 688,
                'item_kode' => '8993058303105',
                'satuan_id' => 3,
                'konversi' => 10,
                'created_at' => '2018-03-29 13:54:43',
                'updated_at' => '2018-03-29 13:54:43',
            ),
            137 => 
            array (
                'id' => 689,
                'item_kode' => '8997208160044',
                'satuan_id' => 8,
                'konversi' => 24,
                'created_at' => '2018-03-29 14:51:55',
                'updated_at' => '2018-03-29 14:51:55',
            ),
            138 => 
            array (
                'id' => 690,
                'item_kode' => '8997013070019',
                'satuan_id' => 3,
                'konversi' => 10,
                'created_at' => '2018-03-29 14:53:02',
                'updated_at' => '2018-03-29 14:53:02',
            ),
            139 => 
            array (
                'id' => 691,
                'item_kode' => '8999777026370',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 14:55:22',
                'updated_at' => '2018-03-29 14:55:22',
            ),
            140 => 
            array (
                'id' => 692,
                'item_kode' => '8999999041854',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 14:57:56',
                'updated_at' => '2018-03-29 14:57:56',
            ),
            141 => 
            array (
                'id' => 693,
                'item_kode' => '8999999041892',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 14:58:20',
                'updated_at' => '2018-03-29 14:58:20',
            ),
            142 => 
            array (
                'id' => 694,
                'item_kode' => '8999999400958',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:01:03',
                'updated_at' => '2018-03-29 15:01:03',
            ),
            143 => 
            array (
                'id' => 695,
                'item_kode' => '8999999401023',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:01:15',
                'updated_at' => '2018-03-29 15:01:15',
            ),
            144 => 
            array (
                'id' => 696,
                'item_kode' => '8999999400903',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:02:01',
                'updated_at' => '2018-03-29 15:02:01',
            ),
            145 => 
            array (
                'id' => 697,
                'item_kode' => '8999999400934',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:02:15',
                'updated_at' => '2018-03-29 15:02:15',
            ),
            146 => 
            array (
                'id' => 698,
                'item_kode' => 'KMKGTSUBCSDPP',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:11:16',
                'updated_at' => '2018-03-29 15:11:16',
            ),
            147 => 
            array (
                'id' => 699,
                'item_kode' => 'KMKMITRNKOCBR',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:11:49',
                'updated_at' => '2018-03-29 15:11:49',
            ),
            148 => 
            array (
                'id' => 700,
                'item_kode' => 'KMKFCJWYXLAGM',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:12:39',
                'updated_at' => '2018-03-29 15:12:39',
            ),
            149 => 
            array (
                'id' => 701,
                'item_kode' => 'KMKXJUNCTYYVN',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:13:49',
                'updated_at' => '2018-03-29 15:13:49',
            ),
            150 => 
            array (
                'id' => 702,
                'item_kode' => 'KMKYPFAUGXDMB',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:14:38',
                'updated_at' => '2018-03-29 15:14:38',
            ),
            151 => 
            array (
                'id' => 703,
                'item_kode' => 'KMKXGGWDSBKIQ',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:14:56',
                'updated_at' => '2018-03-29 15:14:56',
            ),
            152 => 
            array (
                'id' => 704,
                'item_kode' => 'KMKDUELKEEADC',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:15:53',
                'updated_at' => '2018-03-29 15:15:53',
            ),
            153 => 
            array (
                'id' => 705,
                'item_kode' => 'KMKVJWFUNAXAJ',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:17:01',
                'updated_at' => '2018-03-29 15:17:01',
            ),
            154 => 
            array (
                'id' => 706,
                'item_kode' => 'KMKGBUFMCBTXK',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:17:38',
                'updated_at' => '2018-03-29 15:17:38',
            ),
            155 => 
            array (
                'id' => 707,
                'item_kode' => 'KMKVURMHALSTH',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:18:53',
                'updated_at' => '2018-03-29 15:18:53',
            ),
            156 => 
            array (
                'id' => 708,
                'item_kode' => 'KMKPJGPFRJZHK',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:19:20',
                'updated_at' => '2018-03-29 15:19:20',
            ),
            157 => 
            array (
                'id' => 709,
                'item_kode' => 'KMKWZUSAKKWOY',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:19:47',
                'updated_at' => '2018-03-29 15:19:47',
            ),
            158 => 
            array (
                'id' => 710,
                'item_kode' => 'KMKKFWUEFHUZC',
                'satuan_id' => 2,
                'konversi' => 4,
                'created_at' => '2018-03-29 15:24:10',
                'updated_at' => '2018-03-29 15:24:10',
            ),
            159 => 
            array (
                'id' => 711,
                'item_kode' => 'KMKOVFGEUGXNK',
                'satuan_id' => 2,
                'konversi' => 4,
                'created_at' => '2018-03-29 15:25:28',
                'updated_at' => '2018-03-29 15:25:28',
            ),
            160 => 
            array (
                'id' => 712,
                'item_kode' => 'KMKETIMRVZMKZ',
                'satuan_id' => 2,
                'konversi' => 2,
                'created_at' => '2018-03-29 15:26:27',
                'updated_at' => '2018-03-29 15:26:27',
            ),
            161 => 
            array (
                'id' => 713,
                'item_kode' => 'KMKASQOZCMGYS',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:27:05',
                'updated_at' => '2018-03-29 15:27:05',
            ),
            162 => 
            array (
                'id' => 714,
                'item_kode' => 'KMKKVZGJDYQTM',
                'satuan_id' => 2,
                'konversi' => 4,
                'created_at' => '2018-03-29 15:27:57',
                'updated_at' => '2018-03-29 15:27:57',
            ),
            163 => 
            array (
                'id' => 715,
                'item_kode' => 'KMKANZZUNYVIL',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:28:48',
                'updated_at' => '2018-03-29 15:28:48',
            ),
            164 => 
            array (
                'id' => 716,
                'item_kode' => 'KMKCKKCWDHXGI',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:29:11',
                'updated_at' => '2018-03-29 15:29:11',
            ),
            165 => 
            array (
                'id' => 717,
                'item_kode' => 'KMKDIQSXKFPFC',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:29:34',
                'updated_at' => '2018-03-29 15:29:34',
            ),
            166 => 
            array (
                'id' => 718,
                'item_kode' => 'KMKQLGNTVRICW',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:30:01',
                'updated_at' => '2018-03-29 15:30:01',
            ),
            167 => 
            array (
                'id' => 719,
                'item_kode' => 'KMKZLQBDBEALB',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:30:31',
                'updated_at' => '2018-03-29 15:30:31',
            ),
            168 => 
            array (
                'id' => 720,
                'item_kode' => 'KMKARBRPVCGJC',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:31:17',
                'updated_at' => '2018-03-29 15:31:17',
            ),
            169 => 
            array (
                'id' => 721,
                'item_kode' => 'KMKHGWZLCFJYO',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:31:43',
                'updated_at' => '2018-03-29 15:31:43',
            ),
            170 => 
            array (
                'id' => 722,
                'item_kode' => 'KMKQBXBTVCLAZ',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:32:38',
                'updated_at' => '2018-03-29 15:32:38',
            ),
            171 => 
            array (
                'id' => 723,
                'item_kode' => 'KMKQBXBTVCLAZ',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-03-29 15:32:56',
                'updated_at' => '2018-03-29 15:32:56',
            ),
            172 => 
            array (
                'id' => 724,
                'item_kode' => 'KMKBEGXFWXABE',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:33:30',
                'updated_at' => '2018-03-29 15:33:30',
            ),
            173 => 
            array (
                'id' => 725,
                'item_kode' => 'KMKBEGXFWXABE',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-03-29 15:33:38',
                'updated_at' => '2018-03-29 15:33:38',
            ),
            174 => 
            array (
                'id' => 726,
                'item_kode' => 'KMKCAHYJXGMYF',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:33:51',
                'updated_at' => '2018-03-29 15:33:51',
            ),
            175 => 
            array (
                'id' => 727,
                'item_kode' => 'KMKCAHYJXGMYF',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-03-29 15:33:57',
                'updated_at' => '2018-03-29 15:33:57',
            ),
            176 => 
            array (
                'id' => 728,
                'item_kode' => 'KMKKYROYGHFJJ',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:34:29',
                'updated_at' => '2018-03-29 15:34:29',
            ),
            177 => 
            array (
                'id' => 729,
                'item_kode' => 'KMKKYROYGHFJJ',
                'satuan_id' => 2,
                'konversi' => 30,
                'created_at' => '2018-03-29 15:34:41',
                'updated_at' => '2018-03-29 15:34:41',
            ),
            178 => 
            array (
                'id' => 730,
                'item_kode' => 'KMKXVGGDKPHHO',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:35:35',
                'updated_at' => '2018-03-29 15:35:35',
            ),
            179 => 
            array (
                'id' => 731,
                'item_kode' => 'KMKXVGGDKPHHO',
                'satuan_id' => 2,
                'konversi' => 25,
                'created_at' => '2018-03-29 15:36:04',
                'updated_at' => '2018-03-29 15:36:04',
            ),
            180 => 
            array (
                'id' => 732,
                'item_kode' => 'KMKEOTIMKUPRS',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:36:36',
                'updated_at' => '2018-03-29 15:36:36',
            ),
            181 => 
            array (
                'id' => 733,
                'item_kode' => 'KMKEOTIMKUPRS',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:36:47',
                'updated_at' => '2018-03-29 15:36:47',
            ),
            182 => 
            array (
                'id' => 734,
                'item_kode' => 'KMKCQJGFZDNJL',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:37:07',
                'updated_at' => '2018-03-29 15:37:07',
            ),
            183 => 
            array (
                'id' => 735,
                'item_kode' => 'KMKBMKKDQSTSL',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:38:00',
                'updated_at' => '2018-03-29 15:38:00',
            ),
            184 => 
            array (
                'id' => 736,
                'item_kode' => 'KMKBMKKDQSTSL',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:38:07',
                'updated_at' => '2018-03-29 15:38:07',
            ),
            185 => 
            array (
                'id' => 737,
                'item_kode' => 'KMKMWEQQJPOES',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:38:39',
                'updated_at' => '2018-03-29 15:38:39',
            ),
            186 => 
            array (
                'id' => 738,
                'item_kode' => 'KMKMWEQQJPOES',
                'satuan_id' => 2,
                'konversi' => 20,
                'created_at' => '2018-03-29 15:38:56',
                'updated_at' => '2018-03-29 15:38:56',
            ),
            187 => 
            array (
                'id' => 739,
                'item_kode' => 'KMKVODWSXWTTR',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:39:18',
                'updated_at' => '2018-03-29 15:39:18',
            ),
            188 => 
            array (
                'id' => 740,
                'item_kode' => 'KMKVODWSXWTTR',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:39:26',
                'updated_at' => '2018-03-29 15:39:26',
            ),
            189 => 
            array (
                'id' => 741,
                'item_kode' => 'KMKCCYEMNOZLJ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:39:41',
                'updated_at' => '2018-03-29 15:39:41',
            ),
            190 => 
            array (
                'id' => 742,
                'item_kode' => 'KMKCCYEMNOZLJ',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:39:47',
                'updated_at' => '2018-03-29 15:39:47',
            ),
            191 => 
            array (
                'id' => 743,
                'item_kode' => 'KMKZOZCGJBRQN',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:40:00',
                'updated_at' => '2018-03-29 15:40:00',
            ),
            192 => 
            array (
                'id' => 744,
                'item_kode' => 'KMKZOZCGJBRQN',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:40:07',
                'updated_at' => '2018-03-29 15:40:07',
            ),
            193 => 
            array (
                'id' => 745,
                'item_kode' => 'KMKJGQEGJHKYT',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:40:55',
                'updated_at' => '2018-03-29 15:40:55',
            ),
            194 => 
            array (
                'id' => 746,
                'item_kode' => 'KMKJGQEGJHKYT',
                'satuan_id' => 2,
                'konversi' => 10,
                'created_at' => '2018-03-29 15:41:14',
                'updated_at' => '2018-03-29 15:41:14',
            ),
            195 => 
            array (
                'id' => 747,
                'item_kode' => 'KMKJGQEGJHKYT',
                'satuan_id' => 12,
                'konversi' => 100,
                'created_at' => '2018-03-29 15:44:51',
                'updated_at' => '2018-03-29 15:44:51',
            ),
            196 => 
            array (
                'id' => 748,
                'item_kode' => 'KMKQIZKZCZHVP',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:45:26',
                'updated_at' => '2018-03-29 15:45:26',
            ),
            197 => 
            array (
                'id' => 749,
                'item_kode' => 'KMKQIZKZCZHVP',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 15:45:32',
                'updated_at' => '2018-03-29 15:45:32',
            ),
            198 => 
            array (
                'id' => 750,
                'item_kode' => 'KMKLXRZNURQOZ',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 15:49:15',
                'updated_at' => '2018-03-29 15:49:15',
            ),
            199 => 
            array (
                'id' => 751,
                'item_kode' => 'KMKLXRZNURQOZ',
                'satuan_id' => 2,
                'konversi' => 2,
                'created_at' => '2018-03-29 15:49:22',
                'updated_at' => '2018-03-29 15:49:22',
            ),
            200 => 
            array (
                'id' => 752,
                'item_kode' => '8991002105430',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 16:16:15',
                'updated_at' => '2018-03-29 16:16:15',
            ),
            201 => 
            array (
                'id' => 753,
                'item_kode' => '8992780010022',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 16:25:53',
                'updated_at' => '2018-03-29 16:25:53',
            ),
            202 => 
            array (
                'id' => 754,
                'item_kode' => '8992780030723',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 16:29:13',
                'updated_at' => '2018-03-29 16:29:13',
            ),
            203 => 
            array (
                'id' => 755,
                'item_kode' => '8992780030730',
                'satuan_id' => 8,
                'konversi' => 5,
                'created_at' => '2018-03-29 16:29:59',
                'updated_at' => '2018-03-29 16:29:59',
            ),
            204 => 
            array (
                'id' => 756,
                'item_kode' => '8992933320114',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 16:30:32',
                'updated_at' => '2018-03-29 16:30:32',
            ),
            205 => 
            array (
                'id' => 757,
                'item_kode' => '8998685011003',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:09:18',
                'updated_at' => '2018-03-29 17:09:18',
            ),
            206 => 
            array (
                'id' => 758,
                'item_kode' => '8999999057633',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:11:42',
                'updated_at' => '2018-03-29 17:11:42',
            ),
            207 => 
            array (
                'id' => 759,
                'item_kode' => '8999999057633',
                'satuan_id' => 6,
                'konversi' => 30,
                'created_at' => '2018-03-29 17:11:56',
                'updated_at' => '2018-03-29 17:11:56',
            ),
            208 => 
            array (
                'id' => 760,
                'item_kode' => '7640129890040',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:12:22',
                'updated_at' => '2018-03-29 17:12:22',
            ),
            209 => 
            array (
                'id' => 761,
                'item_kode' => '7640129890040',
                'satuan_id' => 8,
                'konversi' => 3,
                'created_at' => '2018-03-29 17:12:56',
                'updated_at' => '2018-03-29 17:12:56',
            ),
            210 => 
            array (
                'id' => 762,
                'item_kode' => '8998866608961',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:19:38',
                'updated_at' => '2018-03-29 17:19:38',
            ),
            211 => 
            array (
                'id' => 763,
                'item_kode' => '8999999001117',
                'satuan_id' => 6,
                'konversi' => 48,
                'created_at' => '2018-03-29 17:21:52',
                'updated_at' => '2018-03-29 17:21:52',
            ),
            212 => 
            array (
                'id' => 764,
                'item_kode' => '8999999056186',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:29:40',
                'updated_at' => '2018-03-29 17:29:40',
            ),
            213 => 
            array (
                'id' => 765,
                'item_kode' => '8999999001100',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:39:55',
                'updated_at' => '2018-03-29 17:39:55',
            ),
            214 => 
            array (
                'id' => 766,
                'item_kode' => '8999999406929',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:40:08',
                'updated_at' => '2018-03-29 17:40:08',
            ),
            215 => 
            array (
                'id' => 767,
                'item_kode' => '8999999406943',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:40:50',
                'updated_at' => '2018-03-29 17:40:50',
            ),
            216 => 
            array (
                'id' => 768,
                'item_kode' => '8999999001094',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:41:09',
                'updated_at' => '2018-03-29 17:41:09',
            ),
            217 => 
            array (
                'id' => 769,
                'item_kode' => '8999999027278',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:41:20',
                'updated_at' => '2018-03-29 17:41:20',
            ),
            218 => 
            array (
                'id' => 770,
                'item_kode' => '8999999005443',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:41:51',
                'updated_at' => '2018-03-29 17:41:51',
            ),
            219 => 
            array (
                'id' => 771,
                'item_kode' => '8999999407896',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:42:03',
                'updated_at' => '2018-03-29 17:42:03',
            ),
            220 => 
            array (
                'id' => 772,
                'item_kode' => '8999999036348',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:42:15',
                'updated_at' => '2018-03-29 17:42:15',
            ),
            221 => 
            array (
                'id' => 773,
                'item_kode' => '8999999500672',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:42:31',
                'updated_at' => '2018-03-29 17:42:31',
            ),
            222 => 
            array (
                'id' => 774,
                'item_kode' => '8999999500672',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 17:42:38',
                'updated_at' => '2018-03-29 17:42:38',
            ),
            223 => 
            array (
                'id' => 775,
                'item_kode' => '8992747180126',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:42:47',
                'updated_at' => '2018-03-29 17:42:47',
            ),
            224 => 
            array (
                'id' => 776,
                'item_kode' => '8992747180225',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:43:15',
                'updated_at' => '2018-03-29 17:43:15',
            ),
            225 => 
            array (
                'id' => 777,
                'item_kode' => '8999999041212',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:49:48',
                'updated_at' => '2018-03-29 17:49:48',
            ),
            226 => 
            array (
                'id' => 778,
                'item_kode' => '8999999039103',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:50:05',
                'updated_at' => '2018-03-29 17:50:05',
            ),
            227 => 
            array (
                'id' => 779,
                'item_kode' => '8999999043780',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:50:37',
                'updated_at' => '2018-03-29 17:50:37',
            ),
            228 => 
            array (
                'id' => 780,
                'item_kode' => '8999999043780',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 17:50:47',
                'updated_at' => '2018-03-29 17:50:47',
            ),
            229 => 
            array (
                'id' => 781,
                'item_kode' => '8999999043797',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:50:57',
                'updated_at' => '2018-03-29 17:50:57',
            ),
            230 => 
            array (
                'id' => 782,
                'item_kode' => '8999999043797',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 17:51:04',
                'updated_at' => '2018-03-29 17:51:04',
            ),
            231 => 
            array (
                'id' => 783,
                'item_kode' => '8992866110608',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 17:52:43',
                'updated_at' => '2018-03-29 17:52:43',
            ),
            232 => 
            array (
                'id' => 784,
                'item_kode' => '8999999030179',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 17:56:14',
                'updated_at' => '2018-03-29 17:56:14',
            ),
            233 => 
            array (
                'id' => 785,
                'item_kode' => '8999999030179',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 17:56:23',
                'updated_at' => '2018-03-29 17:56:23',
            ),
            234 => 
            array (
                'id' => 786,
                'item_kode' => 'KMKTZTKAROPAD',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 18:01:25',
                'updated_at' => '2018-03-29 18:01:25',
            ),
            235 => 
            array (
                'id' => 787,
                'item_kode' => 'KMKTZTKAROPAD',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 18:01:39',
                'updated_at' => '2018-03-29 18:01:39',
            ),
            236 => 
            array (
                'id' => 788,
                'item_kode' => 'KMKNFEGXOVUCZ',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 18:02:07',
                'updated_at' => '2018-03-29 18:02:07',
            ),
            237 => 
            array (
                'id' => 789,
                'item_kode' => 'KMKNFEGXOVUCZ',
                'satuan_id' => 6,
                'konversi' => 80,
                'created_at' => '2018-03-29 18:02:32',
                'updated_at' => '2018-03-29 18:02:32',
            ),
            238 => 
            array (
                'id' => 790,
                'item_kode' => 'KMKUYUZLPTMLA',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 18:03:06',
                'updated_at' => '2018-03-29 18:03:06',
            ),
            239 => 
            array (
                'id' => 791,
                'item_kode' => 'KMKUYUZLPTMLA',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 18:03:20',
                'updated_at' => '2018-03-29 18:03:20',
            ),
            240 => 
            array (
                'id' => 792,
                'item_kode' => 'KMKUYUZLPTMLA',
                'satuan_id' => 6,
                'konversi' => 240,
                'created_at' => '2018-03-29 18:03:36',
                'updated_at' => '2018-03-29 18:03:36',
            ),
            241 => 
            array (
                'id' => 793,
                'item_kode' => '8992765301008',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 18:08:34',
                'updated_at' => '2018-03-29 18:08:34',
            ),
            242 => 
            array (
                'id' => 794,
                'item_kode' => '8992765301008',
                'satuan_id' => 8,
                'konversi' => 6,
                'created_at' => '2018-03-29 18:08:46',
                'updated_at' => '2018-03-29 18:08:46',
            ),
            243 => 
            array (
                'id' => 795,
                'item_kode' => '711844150003',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 19:53:18',
                'updated_at' => '2018-03-29 19:53:18',
            ),
            244 => 
            array (
                'id' => 796,
                'item_kode' => '711844150003',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 19:53:36',
                'updated_at' => '2018-03-29 19:53:36',
            ),
            245 => 
            array (
                'id' => 797,
                'item_kode' => '8992828831527',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 19:54:27',
                'updated_at' => '2018-03-29 19:54:27',
            ),
            246 => 
            array (
                'id' => 798,
                'item_kode' => '8992828831527',
                'satuan_id' => 8,
                'konversi' => 4,
                'created_at' => '2018-03-29 19:54:43',
                'updated_at' => '2018-03-29 19:54:43',
            ),
            247 => 
            array (
                'id' => 799,
                'item_kode' => '8992828831527',
                'satuan_id' => 2,
                'konversi' => 48,
                'created_at' => '2018-03-29 19:55:58',
                'updated_at' => '2018-03-29 19:55:58',
            ),
            248 => 
            array (
                'id' => 800,
                'item_kode' => '8992003150245',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:00:03',
                'updated_at' => '2018-03-29 20:00:03',
            ),
            249 => 
            array (
                'id' => 801,
                'item_kode' => '8992003150245',
                'satuan_id' => 10,
                'konversi' => 10,
                'created_at' => '2018-03-29 20:00:26',
                'updated_at' => '2018-03-29 20:00:26',
            ),
            250 => 
            array (
                'id' => 802,
                'item_kode' => '8995757404404',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:02:17',
                'updated_at' => '2018-03-29 20:02:17',
            ),
            251 => 
            array (
                'id' => 803,
                'item_kode' => '8995757404404',
                'satuan_id' => 2,
                'konversi' => 5,
                'created_at' => '2018-03-29 20:05:01',
                'updated_at' => '2018-03-29 20:05:01',
            ),
            252 => 
            array (
                'id' => 804,
                'item_kode' => '8993571003810',
                'satuan_id' => 3,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:06:43',
                'updated_at' => '2018-03-29 20:06:43',
            ),
            253 => 
            array (
                'id' => 805,
                'item_kode' => '8993058000929',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:09:55',
                'updated_at' => '2018-03-29 20:09:55',
            ),
            254 => 
            array (
                'id' => 806,
                'item_kode' => '8993058000929',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 20:10:17',
                'updated_at' => '2018-03-29 20:10:17',
            ),
            255 => 
            array (
                'id' => 807,
                'item_kode' => '8992003150245',
                'satuan_id' => 8,
                'konversi' => 20,
                'created_at' => '2018-03-29 20:13:52',
                'updated_at' => '2018-03-29 20:13:52',
            ),
            256 => 
            array (
                'id' => 808,
                'item_kode' => '8993571003810',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 20:15:36',
                'updated_at' => '2018-03-29 20:15:36',
            ),
            257 => 
            array (
                'id' => 809,
                'item_kode' => '8993571003810',
                'satuan_id' => 14,
                'konversi' => 24,
                'created_at' => '2018-03-29 20:16:25',
                'updated_at' => '2018-03-29 20:16:25',
            ),
            258 => 
            array (
                'id' => 810,
                'item_kode' => '8995858899888',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:18:23',
                'updated_at' => '2018-03-29 20:18:23',
            ),
            259 => 
            array (
                'id' => 811,
                'item_kode' => '8995858899888',
                'satuan_id' => 8,
                'konversi' => 15,
                'created_at' => '2018-03-29 20:18:53',
                'updated_at' => '2018-03-29 20:18:53',
            ),
            260 => 
            array (
                'id' => 812,
                'item_kode' => '8993008125047',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:19:41',
                'updated_at' => '2018-03-29 20:19:41',
            ),
            261 => 
            array (
                'id' => 813,
                'item_kode' => '8993008125047',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-29 20:20:01',
                'updated_at' => '2018-03-29 20:20:01',
            ),
            262 => 
            array (
                'id' => 814,
                'item_kode' => '8993371100146',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:27:06',
                'updated_at' => '2018-03-29 20:27:06',
            ),
            263 => 
            array (
                'id' => 815,
                'item_kode' => '8993371100146',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-03-29 20:27:26',
                'updated_at' => '2018-03-29 20:27:26',
            ),
            264 => 
            array (
                'id' => 816,
                'item_kode' => '8992800215604',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:38:21',
                'updated_at' => '2018-03-29 20:38:21',
            ),
            265 => 
            array (
                'id' => 817,
                'item_kode' => '8992800215604',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-29 20:38:36',
                'updated_at' => '2018-03-29 20:38:36',
            ),
            266 => 
            array (
                'id' => 818,
                'item_kode' => '8992800215604',
                'satuan_id' => 6,
                'konversi' => 200,
                'created_at' => '2018-03-29 20:38:53',
                'updated_at' => '2018-03-29 20:38:53',
            ),
            267 => 
            array (
                'id' => 819,
                'item_kode' => '8998866610384',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:44:28',
                'updated_at' => '2018-03-29 20:44:28',
            ),
            268 => 
            array (
                'id' => 820,
                'item_kode' => '8998866610384',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-03-29 20:44:42',
                'updated_at' => '2018-03-29 20:44:42',
            ),
            269 => 
            array (
                'id' => 821,
                'item_kode' => '8991002105645',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:53:01',
                'updated_at' => '2018-03-29 20:53:01',
            ),
            270 => 
            array (
                'id' => 822,
                'item_kode' => '8991002105645',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 20:53:15',
                'updated_at' => '2018-03-29 20:53:15',
            ),
            271 => 
            array (
                'id' => 823,
                'item_kode' => '8991002101746',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-03-29 20:54:46',
                'updated_at' => '2018-03-29 20:54:46',
            ),
            272 => 
            array (
                'id' => 824,
                'item_kode' => '8991002101746',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-03-29 20:55:01',
                'updated_at' => '2018-03-29 20:55:01',
            ),
            273 => 
            array (
                'id' => 825,
                'item_kode' => 'KMKKJXPROTDPB',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:12:56',
                'updated_at' => '2018-03-29 21:12:56',
            ),
            274 => 
            array (
                'id' => 826,
                'item_kode' => 'KMKKJXPROTDPB',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-29 21:13:12',
                'updated_at' => '2018-03-29 21:13:12',
            ),
            275 => 
            array (
                'id' => 827,
                'item_kode' => 'KMKMKCHULDWRT',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:14:50',
                'updated_at' => '2018-03-29 21:14:50',
            ),
            276 => 
            array (
                'id' => 828,
                'item_kode' => 'KMKMKCHULDWRT',
                'satuan_id' => 13,
                'konversi' => 12,
                'created_at' => '2018-03-29 21:15:12',
                'updated_at' => '2018-03-29 21:15:12',
            ),
            277 => 
            array (
                'id' => 829,
                'item_kode' => 'KMKQSZITPAEME',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:16:13',
                'updated_at' => '2018-03-29 21:16:13',
            ),
            278 => 
            array (
                'id' => 830,
                'item_kode' => 'KMKDSOIGHIUBL',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:19:49',
                'updated_at' => '2018-03-29 21:19:49',
            ),
            279 => 
            array (
                'id' => 833,
                'item_kode' => 'KMKMRPHFUBYSH',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:28:48',
                'updated_at' => '2018-03-29 21:28:48',
            ),
            280 => 
            array (
                'id' => 834,
                'item_kode' => 'KMKMRPHFUBYSH',
                'satuan_id' => 14,
                'konversi' => 12,
                'created_at' => '2018-03-29 21:30:16',
                'updated_at' => '2018-03-29 21:30:16',
            ),
            281 => 
            array (
                'id' => 835,
                'item_kode' => 'KMKJRMIBPPXKL',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:32:32',
                'updated_at' => '2018-03-29 21:32:32',
            ),
            282 => 
            array (
                'id' => 836,
                'item_kode' => 'KMKJRMIBPPXKL',
                'satuan_id' => 14,
                'konversi' => 12,
                'created_at' => '2018-03-29 21:32:52',
                'updated_at' => '2018-03-29 21:32:52',
            ),
            283 => 
            array (
                'id' => 837,
                'item_kode' => 'KMKPOIMHYJNML',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:34:51',
                'updated_at' => '2018-03-29 21:34:51',
            ),
            284 => 
            array (
                'id' => 838,
                'item_kode' => 'KMKPOIMHYJNML',
                'satuan_id' => 14,
                'konversi' => 6,
                'created_at' => '2018-03-29 21:35:06',
                'updated_at' => '2018-03-29 21:35:06',
            ),
            285 => 
            array (
                'id' => 839,
                'item_kode' => 'KMKSUXKLLBTSO',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-29 21:36:47',
                'updated_at' => '2018-03-29 21:36:47',
            ),
            286 => 
            array (
                'id' => 840,
                'item_kode' => 'KMKSUXKLLBTSO',
                'satuan_id' => 14,
                'konversi' => 6,
                'created_at' => '2018-03-29 21:37:05',
                'updated_at' => '2018-03-29 21:37:05',
            ),
            287 => 
            array (
                'id' => 841,
                'item_kode' => 'KMKSUXKLLBTSO',
                'satuan_id' => 6,
                'konversi' => 144,
                'created_at' => '2018-03-29 21:39:56',
                'updated_at' => '2018-03-29 21:39:56',
            ),
            288 => 
            array (
                'id' => 842,
                'item_kode' => 'KMKPRMKJJMYVP',
                'satuan_id' => 8,
                'konversi' => 1,
                'created_at' => '2018-03-30 14:07:43',
                'updated_at' => '2018-03-30 14:07:43',
            ),
            289 => 
            array (
                'id' => 843,
                'item_kode' => 'KMKPRMKJJMYVP',
                'satuan_id' => 6,
                'konversi' => 25,
                'created_at' => '2018-03-30 14:08:00',
                'updated_at' => '2018-03-30 14:08:00',
            ),
            290 => 
            array (
                'id' => 844,
                'item_kode' => 'KMKUIKOPTZRNH',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-30 14:11:01',
                'updated_at' => '2018-03-30 14:11:01',
            ),
            291 => 
            array (
                'id' => 845,
                'item_kode' => 'KMKNHQQJKIVOA',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-30 14:12:50',
                'updated_at' => '2018-03-30 14:12:50',
            ),
            292 => 
            array (
                'id' => 846,
                'item_kode' => 'KMKGPUYQPMRZA',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-30 19:45:50',
                'updated_at' => '2018-03-30 19:45:50',
            ),
            293 => 
            array (
                'id' => 847,
                'item_kode' => 'KMKGPUYQPMRZA',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-30 19:46:14',
                'updated_at' => '2018-03-30 19:46:14',
            ),
            294 => 
            array (
                'id' => 848,
                'item_kode' => '8999908034205',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-31 11:49:32',
                'updated_at' => '2018-03-31 11:49:32',
            ),
            295 => 
            array (
                'id' => 849,
                'item_kode' => '8999908034205',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-31 11:49:48',
                'updated_at' => '2018-03-31 11:49:48',
            ),
            296 => 
            array (
                'id' => 850,
                'item_kode' => '8999908034205',
                'satuan_id' => 6,
                'konversi' => 50,
                'created_at' => '2018-03-31 11:50:04',
                'updated_at' => '2018-03-31 11:50:04',
            ),
            297 => 
            array (
                'id' => 851,
                'item_kode' => '8995152900068',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-31 18:04:11',
                'updated_at' => '2018-03-31 18:04:11',
            ),
            298 => 
            array (
                'id' => 852,
                'item_kode' => '8995152900068',
                'satuan_id' => 8,
                'konversi' => 10,
                'created_at' => '2018-03-31 18:04:27',
                'updated_at' => '2018-03-31 18:04:27',
            ),
            299 => 
            array (
                'id' => 853,
                'item_kode' => '8993099996472',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-31 18:10:06',
                'updated_at' => '2018-03-31 18:10:06',
            ),
            300 => 
            array (
                'id' => 854,
                'item_kode' => '8993099996472',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-31 18:10:23',
                'updated_at' => '2018-03-31 18:10:23',
            ),
            301 => 
            array (
                'id' => 855,
                'item_kode' => '8999908039101',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-31 19:09:47',
                'updated_at' => '2018-03-31 19:09:47',
            ),
            302 => 
            array (
                'id' => 856,
                'item_kode' => '8999908039101',
                'satuan_id' => 8,
                'konversi' => 25,
                'created_at' => '2018-03-31 19:09:59',
                'updated_at' => '2018-03-31 19:09:59',
            ),
            303 => 
            array (
                'id' => 857,
                'item_kode' => '8994472000021',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-03-31 19:32:32',
                'updated_at' => '2018-03-31 19:32:32',
            ),
            304 => 
            array (
                'id' => 858,
                'item_kode' => '8994472000021',
                'satuan_id' => 8,
                'konversi' => 12,
                'created_at' => '2018-03-31 19:32:44',
                'updated_at' => '2018-03-31 19:32:44',
            ),
            305 => 
            array (
                'id' => 859,
                'item_kode' => '8999099920509',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:11:18',
                'updated_at' => '2018-04-13 18:11:18',
            ),
            306 => 
            array (
                'id' => 860,
                'item_kode' => '8999099920585',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:12:45',
                'updated_at' => '2018-04-13 18:12:45',
            ),
            307 => 
            array (
                'id' => 861,
                'item_kode' => '8999099920981',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:14:35',
                'updated_at' => '2018-04-13 18:14:35',
            ),
            308 => 
            array (
                'id' => 862,
                'item_kode' => '8999099923739',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:15:50',
                'updated_at' => '2018-04-13 18:15:50',
            ),
            309 => 
            array (
                'id' => 863,
                'item_kode' => '8999099920462',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:27:46',
                'updated_at' => '2018-04-13 18:27:46',
            ),
            310 => 
            array (
                'id' => 864,
                'item_kode' => '8999099920547',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:30:38',
                'updated_at' => '2018-04-13 18:30:38',
            ),
            311 => 
            array (
                'id' => 865,
                'item_kode' => '8999099920684',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:32:05',
                'updated_at' => '2018-04-13 18:32:05',
            ),
            312 => 
            array (
                'id' => 868,
                'item_kode' => '8992628022156',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:37:11',
                'updated_at' => '2018-04-13 18:37:11',
            ),
            313 => 
            array (
                'id' => 869,
                'item_kode' => '8992628022156',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-04-13 18:37:29',
                'updated_at' => '2018-04-13 18:37:29',
            ),
            314 => 
            array (
                'id' => 870,
                'item_kode' => '8992628020152',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:38:48',
                'updated_at' => '2018-04-13 18:38:48',
            ),
            315 => 
            array (
                'id' => 871,
                'item_kode' => '8992628020152',
                'satuan_id' => 6,
                'konversi' => 6,
                'created_at' => '2018-04-13 18:39:00',
                'updated_at' => '2018-04-13 18:39:00',
            ),
            316 => 
            array (
                'id' => 872,
                'item_kode' => '8992717781025',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:40:20',
                'updated_at' => '2018-04-13 18:40:20',
            ),
            317 => 
            array (
                'id' => 873,
                'item_kode' => '8992717781025',
                'satuan_id' => 6,
                'konversi' => 36,
                'created_at' => '2018-04-13 18:40:56',
                'updated_at' => '2018-04-13 18:40:56',
            ),
            318 => 
            array (
                'id' => 874,
                'item_kode' => '8992696408746',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:42:20',
                'updated_at' => '2018-04-13 18:42:20',
            ),
            319 => 
            array (
                'id' => 875,
                'item_kode' => '8992696408722',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:43:53',
                'updated_at' => '2018-04-13 18:43:53',
            ),
            320 => 
            array (
                'id' => 876,
                'item_kode' => '8992696405417',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:47:53',
                'updated_at' => '2018-04-13 18:47:53',
            ),
            321 => 
            array (
                'id' => 877,
                'item_kode' => '8992696520103',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:50:21',
                'updated_at' => '2018-04-13 18:50:21',
            ),
            322 => 
            array (
                'id' => 878,
                'item_kode' => '8993175535878',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:53:30',
                'updated_at' => '2018-04-13 18:53:30',
            ),
            323 => 
            array (
                'id' => 879,
                'item_kode' => '8993175535878',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-04-13 18:53:45',
                'updated_at' => '2018-04-13 18:53:45',
            ),
            324 => 
            array (
                'id' => 880,
                'item_kode' => '8993175535885',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 18:59:28',
                'updated_at' => '2018-04-13 18:59:28',
            ),
            325 => 
            array (
                'id' => 881,
                'item_kode' => '8993175535885',
                'satuan_id' => 6,
                'konversi' => 60,
                'created_at' => '2018-04-13 18:59:43',
                'updated_at' => '2018-04-13 18:59:43',
            ),
            326 => 
            array (
                'id' => 882,
                'item_kode' => '4711036008200',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:01:00',
                'updated_at' => '2018-04-13 19:01:00',
            ),
            327 => 
            array (
                'id' => 883,
                'item_kode' => '4711036008200',
                'satuan_id' => 6,
                'konversi' => 36,
                'created_at' => '2018-04-13 19:01:14',
                'updated_at' => '2018-04-13 19:01:14',
            ),
            328 => 
            array (
                'id' => 884,
                'item_kode' => '8997025911058',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:02:39',
                'updated_at' => '2018-04-13 19:02:39',
            ),
            329 => 
            array (
                'id' => 888,
                'item_kode' => '8997025911058',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-04-13 19:04:20',
                'updated_at' => '2018-04-13 19:04:20',
            ),
            330 => 
            array (
                'id' => 889,
                'item_kode' => '8997025911058',
                'satuan_id' => 6,
                'konversi' => 36,
                'created_at' => '2018-04-13 19:04:37',
                'updated_at' => '2018-04-13 19:04:37',
            ),
            331 => 
            array (
                'id' => 890,
                'item_kode' => '089686018059',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:14:31',
                'updated_at' => '2018-04-13 19:14:31',
            ),
            332 => 
            array (
                'id' => 891,
                'item_kode' => '089686018059',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-04-13 19:14:42',
                'updated_at' => '2018-04-13 19:14:42',
            ),
            333 => 
            array (
                'id' => 892,
                'item_kode' => '4711036008200',
                'satuan_id' => 2,
                'konversi' => 6,
                'created_at' => '2018-04-13 19:17:14',
                'updated_at' => '2018-04-13 19:17:14',
            ),
            334 => 
            array (
                'id' => 893,
                'item_kode' => '089686017748',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:21:09',
                'updated_at' => '2018-04-13 19:21:09',
            ),
            335 => 
            array (
                'id' => 894,
                'item_kode' => '089686017748',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-04-13 19:21:24',
                'updated_at' => '2018-04-13 19:21:24',
            ),
            336 => 
            array (
                'id' => 895,
                'item_kode' => '089686010015',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:24:29',
                'updated_at' => '2018-04-13 19:24:29',
            ),
            337 => 
            array (
                'id' => 896,
                'item_kode' => '089686010015',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-04-13 19:24:52',
                'updated_at' => '2018-04-13 19:24:52',
            ),
            338 => 
            array (
                'id' => 897,
                'item_kode' => '089686010343',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:26:37',
                'updated_at' => '2018-04-13 19:26:37',
            ),
            339 => 
            array (
                'id' => 898,
                'item_kode' => '089686010343',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-04-13 19:26:52',
                'updated_at' => '2018-04-13 19:26:52',
            ),
            340 => 
            array (
                'id' => 899,
                'item_kode' => '089686010947',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:28:00',
                'updated_at' => '2018-04-13 19:28:00',
            ),
            341 => 
            array (
                'id' => 900,
                'item_kode' => '089686010947',
                'satuan_id' => 6,
                'konversi' => 40,
                'created_at' => '2018-04-13 19:28:14',
                'updated_at' => '2018-04-13 19:28:14',
            ),
            342 => 
            array (
                'id' => 901,
                'item_kode' => '089686060027',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:30:02',
                'updated_at' => '2018-04-13 19:30:02',
            ),
            343 => 
            array (
                'id' => 902,
                'item_kode' => '089686060027',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-04-13 19:30:20',
                'updated_at' => '2018-04-13 19:30:20',
            ),
            344 => 
            array (
                'id' => 903,
                'item_kode' => '089686060126',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:31:24',
                'updated_at' => '2018-04-13 19:31:24',
            ),
            345 => 
            array (
                'id' => 904,
                'item_kode' => '089686060126',
                'satuan_id' => 6,
                'konversi' => 24,
                'created_at' => '2018-04-13 19:31:35',
                'updated_at' => '2018-04-13 19:31:35',
            ),
            346 => 
            array (
                'id' => 905,
                'item_kode' => '8992772311014',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:33:53',
                'updated_at' => '2018-04-13 19:33:53',
            ),
            347 => 
            array (
                'id' => 906,
                'item_kode' => '8992772311014',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:34:44',
                'updated_at' => '2018-04-13 19:34:44',
            ),
            348 => 
            array (
                'id' => 907,
                'item_kode' => '8998899004105',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:36:23',
                'updated_at' => '2018-04-13 19:36:23',
            ),
            349 => 
            array (
                'id' => 908,
                'item_kode' => '8998899004105',
                'satuan_id' => 2,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:36:37',
                'updated_at' => '2018-04-13 19:36:37',
            ),
            350 => 
            array (
                'id' => 909,
                'item_kode' => '8993189271748',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:39:59',
                'updated_at' => '2018-04-13 19:39:59',
            ),
            351 => 
            array (
                'id' => 910,
                'item_kode' => '8993189271748',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:40:35',
                'updated_at' => '2018-04-13 19:40:35',
            ),
            352 => 
            array (
                'id' => 911,
                'item_kode' => '8993189270796',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:42:31',
                'updated_at' => '2018-04-13 19:42:31',
            ),
            353 => 
            array (
                'id' => 912,
                'item_kode' => '8993189270796',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:42:43',
                'updated_at' => '2018-04-13 19:42:43',
            ),
            354 => 
            array (
                'id' => 913,
                'item_kode' => '8993189270819',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:44:02',
                'updated_at' => '2018-04-13 19:44:02',
            ),
            355 => 
            array (
                'id' => 914,
                'item_kode' => '8993189270819',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:44:13',
                'updated_at' => '2018-04-13 19:44:13',
            ),
            356 => 
            array (
                'id' => 915,
                'item_kode' => '8993189270833',
                'satuan_id' => 10,
                'konversi' => 1,
                'created_at' => '2018-04-13 19:46:10',
                'updated_at' => '2018-04-13 19:46:10',
            ),
            357 => 
            array (
                'id' => 916,
                'item_kode' => '8993189270833',
                'satuan_id' => 6,
                'konversi' => 12,
                'created_at' => '2018-04-13 19:46:23',
                'updated_at' => '2018-04-13 19:46:23',
            ),
            358 => 
            array (
                'id' => 917,
                'item_kode' => '8999099920721',
                'satuan_id' => 1,
                'konversi' => 1,
                'created_at' => '2018-04-14 13:03:46',
                'updated_at' => '2018-04-14 13:03:46',
            ),
        ));
        
        
    }
}