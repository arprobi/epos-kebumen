<?php

use Illuminate\Database\Seeder;

class Item5sTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('__item5s')->delete();
        
        \DB::table('__item5s')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => '89',
                'nama_pendek' => '13',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => '90',
                'nama_pendek' => '13',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => '745',
                'nama_pendek' => '13',
            ),
            3 => 
            array (
                'id' => 4,
                'nama' => '778',
                'nama_pendek' => '13',
            ),
            4 => 
            array (
                'id' => 5,
                'nama' => '779',
                'nama_pendek' => '13',
            ),
            5 => 
            array (
                'id' => 6,
                'nama' => '58',
                'nama_pendek' => '11',
            ),
            6 => 
            array (
                'id' => 7,
                'nama' => '85',
                'nama_pendek' => '11',
            ),
            7 => 
            array (
                'id' => 8,
                'nama' => '161',
                'nama_pendek' => '11',
            ),
            8 => 
            array (
                'id' => 9,
                'nama' => '162',
                'nama_pendek' => '11',
            ),
            9 => 
            array (
                'id' => 10,
                'nama' => '337',
                'nama_pendek' => '11',
            ),
            10 => 
            array (
                'id' => 11,
                'nama' => '352',
                'nama_pendek' => '11',
            ),
            11 => 
            array (
                'id' => 12,
                'nama' => '515',
                'nama_pendek' => '11',
            ),
            12 => 
            array (
                'id' => 13,
                'nama' => '516',
                'nama_pendek' => '11',
            ),
            13 => 
            array (
                'id' => 14,
                'nama' => '706',
                'nama_pendek' => '11',
            ),
            14 => 
            array (
                'id' => 15,
                'nama' => '707',
                'nama_pendek' => '11',
            ),
            15 => 
            array (
                'id' => 16,
                'nama' => '1014',
                'nama_pendek' => '11',
            ),
            16 => 
            array (
                'id' => 17,
                'nama' => '87',
                'nama_pendek' => '9',
            ),
            17 => 
            array (
                'id' => 18,
                'nama' => '88',
                'nama_pendek' => '9',
            ),
            18 => 
            array (
                'id' => 19,
                'nama' => '257',
                'nama_pendek' => '9',
            ),
            19 => 
            array (
                'id' => 20,
                'nama' => '258',
                'nama_pendek' => '9',
            ),
            20 => 
            array (
                'id' => 21,
                'nama' => '259',
                'nama_pendek' => '9',
            ),
            21 => 
            array (
                'id' => 22,
                'nama' => '997',
                'nama_pendek' => '23',
            ),
            22 => 
            array (
                'id' => 23,
                'nama' => '998',
                'nama_pendek' => '23',
            ),
            23 => 
            array (
                'id' => 24,
                'nama' => '999',
                'nama_pendek' => '23',
            ),
            24 => 
            array (
                'id' => 25,
                'nama' => '1000',
                'nama_pendek' => '23',
            ),
            25 => 
            array (
                'id' => 26,
                'nama' => '1001',
                'nama_pendek' => '23',
            ),
            26 => 
            array (
                'id' => 27,
                'nama' => '25',
                'nama_pendek' => '25',
            ),
            27 => 
            array (
                'id' => 28,
                'nama' => '26',
                'nama_pendek' => '25',
            ),
            28 => 
            array (
                'id' => 29,
                'nama' => '166',
                'nama_pendek' => '25',
            ),
            29 => 
            array (
                'id' => 30,
                'nama' => '301',
                'nama_pendek' => '25',
            ),
            30 => 
            array (
                'id' => 31,
                'nama' => '401',
                'nama_pendek' => '25',
            ),
            31 => 
            array (
                'id' => 32,
                'nama' => '477',
                'nama_pendek' => '25',
            ),
            32 => 
            array (
                'id' => 33,
                'nama' => '478',
                'nama_pendek' => '25',
            ),
            33 => 
            array (
                'id' => 34,
                'nama' => '479',
                'nama_pendek' => '25',
            ),
            34 => 
            array (
                'id' => 35,
                'nama' => '480',
                'nama_pendek' => '25',
            ),
            35 => 
            array (
                'id' => 36,
                'nama' => '481',
                'nama_pendek' => '25',
            ),
            36 => 
            array (
                'id' => 37,
                'nama' => '670',
                'nama_pendek' => '25',
            ),
            37 => 
            array (
                'id' => 38,
                'nama' => '1005',
                'nama_pendek' => '25',
            ),
            38 => 
            array (
                'id' => 39,
                'nama' => '1006',
                'nama_pendek' => '25',
            ),
            39 => 
            array (
                'id' => 40,
                'nama' => '1007',
                'nama_pendek' => '25',
            ),
            40 => 
            array (
                'id' => 41,
                'nama' => '1008',
                'nama_pendek' => '25',
            ),
            41 => 
            array (
                'id' => 42,
                'nama' => '746',
                'nama_pendek' => '20',
            ),
            42 => 
            array (
                'id' => 43,
                'nama' => '740',
                'nama_pendek' => '20',
            ),
            43 => 
            array (
                'id' => 44,
                'nama' => '741',
                'nama_pendek' => '20',
            ),
            44 => 
            array (
                'id' => 45,
                'nama' => '742',
                'nama_pendek' => '20',
            ),
            45 => 
            array (
                'id' => 46,
                'nama' => '743',
                'nama_pendek' => '20',
            ),
            46 => 
            array (
                'id' => 47,
                'nama' => '744',
                'nama_pendek' => '20',
            ),
            47 => 
            array (
                'id' => 48,
                'nama' => '846',
                'nama_pendek' => '20',
            ),
            48 => 
            array (
                'id' => 49,
                'nama' => '128',
                'nama_pendek' => '26',
            ),
            49 => 
            array (
                'id' => 50,
                'nama' => '553',
                'nama_pendek' => '26',
            ),
            50 => 
            array (
                'id' => 51,
                'nama' => '554',
                'nama_pendek' => '26',
            ),
            51 => 
            array (
                'id' => 52,
                'nama' => '555',
                'nama_pendek' => '26',
            ),
            52 => 
            array (
                'id' => 53,
                'nama' => '556',
                'nama_pendek' => '26',
            ),
            53 => 
            array (
                'id' => 54,
                'nama' => '560',
                'nama_pendek' => '26',
            ),
            54 => 
            array (
                'id' => 55,
                'nama' => '561',
                'nama_pendek' => '26',
            ),
            55 => 
            array (
                'id' => 56,
                'nama' => '755',
                'nama_pendek' => '26',
            ),
            56 => 
            array (
                'id' => 57,
                'nama' => '830',
                'nama_pendek' => '26',
            ),
            57 => 
            array (
                'id' => 58,
                'nama' => '831',
                'nama_pendek' => '26',
            ),
            58 => 
            array (
                'id' => 59,
                'nama' => '832',
                'nama_pendek' => '26',
            ),
            59 => 
            array (
                'id' => 60,
                'nama' => '833',
                'nama_pendek' => '26',
            ),
            60 => 
            array (
                'id' => 61,
                'nama' => '834',
                'nama_pendek' => '26',
            ),
            61 => 
            array (
                'id' => 62,
                'nama' => '835',
                'nama_pendek' => '26',
            ),
            62 => 
            array (
                'id' => 63,
                'nama' => '836',
                'nama_pendek' => '26',
            ),
            63 => 
            array (
                'id' => 64,
                'nama' => '837',
                'nama_pendek' => '26',
            ),
            64 => 
            array (
                'id' => 65,
                'nama' => '838',
                'nama_pendek' => '26',
            ),
            65 => 
            array (
                'id' => 66,
                'nama' => '839',
                'nama_pendek' => '26',
            ),
            66 => 
            array (
                'id' => 67,
                'nama' => '840',
                'nama_pendek' => '26',
            ),
            67 => 
            array (
                'id' => 68,
                'nama' => '841',
                'nama_pendek' => '26',
            ),
            68 => 
            array (
                'id' => 69,
                'nama' => '842',
                'nama_pendek' => '26',
            ),
            69 => 
            array (
                'id' => 70,
                'nama' => '843',
                'nama_pendek' => '26',
            ),
            70 => 
            array (
                'id' => 71,
                'nama' => '844',
                'nama_pendek' => '26',
            ),
            71 => 
            array (
                'id' => 72,
                'nama' => '845',
                'nama_pendek' => '26',
            ),
            72 => 
            array (
                'id' => 73,
                'nama' => '1062',
                'nama_pendek' => '26',
            ),
            73 => 
            array (
                'id' => 74,
                'nama' => '1063',
                'nama_pendek' => '26',
            ),
            74 => 
            array (
                'id' => 75,
                'nama' => '1064',
                'nama_pendek' => '26',
            ),
            75 => 
            array (
                'id' => 76,
                'nama' => '1065',
                'nama_pendek' => '26',
            ),
            76 => 
            array (
                'id' => 77,
                'nama' => '1066',
                'nama_pendek' => '26',
            ),
            77 => 
            array (
                'id' => 78,
                'nama' => '1093',
                'nama_pendek' => '26',
            ),
            78 => 
            array (
                'id' => 79,
                'nama' => '517',
                'nama_pendek' => '24',
            ),
            79 => 
            array (
                'id' => 80,
                'nama' => '518',
                'nama_pendek' => '24',
            ),
            80 => 
            array (
                'id' => 81,
                'nama' => '19',
                'nama_pendek' => '24',
            ),
            81 => 
            array (
                'id' => 82,
                'nama' => '21',
                'nama_pendek' => '24',
            ),
            82 => 
            array (
                'id' => 83,
                'nama' => '175',
                'nama_pendek' => '24',
            ),
            83 => 
            array (
                'id' => 84,
                'nama' => '558',
                'nama_pendek' => '24',
            ),
            84 => 
            array (
                'id' => 85,
                'nama' => '559',
                'nama_pendek' => '24',
            ),
            85 => 
            array (
                'id' => 86,
                'nama' => '562',
                'nama_pendek' => '24',
            ),
            86 => 
            array (
                'id' => 87,
                'nama' => '564',
                'nama_pendek' => '24',
            ),
            87 => 
            array (
                'id' => 88,
                'nama' => '565',
                'nama_pendek' => '24',
            ),
            88 => 
            array (
                'id' => 89,
                'nama' => '566',
                'nama_pendek' => '24',
            ),
            89 => 
            array (
                'id' => 90,
                'nama' => '567',
                'nama_pendek' => '24',
            ),
            90 => 
            array (
                'id' => 91,
                'nama' => '747',
                'nama_pendek' => '24',
            ),
            91 => 
            array (
                'id' => 92,
                'nama' => '748',
                'nama_pendek' => '24',
            ),
            92 => 
            array (
                'id' => 93,
                'nama' => '749',
                'nama_pendek' => '24',
            ),
            93 => 
            array (
                'id' => 94,
                'nama' => '750',
                'nama_pendek' => '24',
            ),
            94 => 
            array (
                'id' => 95,
                'nama' => '751',
                'nama_pendek' => '24',
            ),
            95 => 
            array (
                'id' => 96,
                'nama' => '752',
                'nama_pendek' => '24',
            ),
            96 => 
            array (
                'id' => 97,
                'nama' => '753',
                'nama_pendek' => '24',
            ),
            97 => 
            array (
                'id' => 98,
                'nama' => '754',
                'nama_pendek' => '24',
            ),
            98 => 
            array (
                'id' => 99,
                'nama' => '756',
                'nama_pendek' => '24',
            ),
            99 => 
            array (
                'id' => 100,
                'nama' => '757',
                'nama_pendek' => '24',
            ),
            100 => 
            array (
                'id' => 101,
                'nama' => '758',
                'nama_pendek' => '24',
            ),
            101 => 
            array (
                'id' => 102,
                'nama' => '773',
                'nama_pendek' => '24',
            ),
            102 => 
            array (
                'id' => 103,
                'nama' => '774',
                'nama_pendek' => '24',
            ),
            103 => 
            array (
                'id' => 104,
                'nama' => '775',
                'nama_pendek' => '24',
            ),
            104 => 
            array (
                'id' => 105,
                'nama' => '776',
                'nama_pendek' => '24',
            ),
            105 => 
            array (
                'id' => 106,
                'nama' => '777',
                'nama_pendek' => '24',
            ),
            106 => 
            array (
                'id' => 107,
                'nama' => '1009',
                'nama_pendek' => '24',
            ),
            107 => 
            array (
                'id' => 108,
                'nama' => '1114',
                'nama_pendek' => '24',
            ),
            108 => 
            array (
                'id' => 109,
                'nama' => '1115',
                'nama_pendek' => '24',
            ),
            109 => 
            array (
                'id' => 110,
                'nama' => '1116',
                'nama_pendek' => '24',
            ),
            110 => 
            array (
                'id' => 111,
                'nama' => '1117',
                'nama_pendek' => '24',
            ),
            111 => 
            array (
                'id' => 112,
                'nama' => '934',
                'nama_pendek' => '17',
            ),
            112 => 
            array (
                'id' => 113,
                'nama' => '935',
                'nama_pendek' => '17',
            ),
            113 => 
            array (
                'id' => 114,
                'nama' => '1083',
                'nama_pendek' => '17',
            ),
            114 => 
            array (
                'id' => 115,
                'nama' => '1084',
                'nama_pendek' => '17',
            ),
            115 => 
            array (
                'id' => 116,
                'nama' => '785',
                'nama_pendek' => '21',
            ),
            116 => 
            array (
                'id' => 117,
                'nama' => '783',
                'nama_pendek' => '21',
            ),
            117 => 
            array (
                'id' => 118,
                'nama' => '784',
                'nama_pendek' => '21',
            ),
            118 => 
            array (
                'id' => 119,
                'nama' => '787',
                'nama_pendek' => '21',
            ),
            119 => 
            array (
                'id' => 120,
                'nama' => '349',
                'nama_pendek' => '12',
            ),
            120 => 
            array (
                'id' => 121,
                'nama' => '350',
                'nama_pendek' => '12',
            ),
            121 => 
            array (
                'id' => 122,
                'nama' => '490',
                'nama_pendek' => '12',
            ),
            122 => 
            array (
                'id' => 123,
                'nama' => '957',
                'nama_pendek' => '12',
            ),
            123 => 
            array (
                'id' => 124,
                'nama' => '86',
                'nama_pendek' => '16',
            ),
            124 => 
            array (
                'id' => 125,
                'nama' => '412',
                'nama_pendek' => '16',
            ),
            125 => 
            array (
                'id' => 126,
                'nama' => '413',
                'nama_pendek' => '16',
            ),
            126 => 
            array (
                'id' => 127,
                'nama' => '929',
                'nama_pendek' => '16',
            ),
            127 => 
            array (
                'id' => 128,
                'nama' => '930',
                'nama_pendek' => '16',
            ),
            128 => 
            array (
                'id' => 129,
                'nama' => '931',
                'nama_pendek' => '16',
            ),
            129 => 
            array (
                'id' => 130,
                'nama' => '932',
                'nama_pendek' => '16',
            ),
            130 => 
            array (
                'id' => 131,
                'nama' => '933',
                'nama_pendek' => '16',
            ),
            131 => 
            array (
                'id' => 132,
                'nama' => '200',
                'nama_pendek' => '10',
            ),
            132 => 
            array (
                'id' => 133,
                'nama' => '327',
                'nama_pendek' => '10',
            ),
            133 => 
            array (
                'id' => 134,
                'nama' => '328',
                'nama_pendek' => '10',
            ),
            134 => 
            array (
                'id' => 135,
                'nama' => '377',
                'nama_pendek' => '10',
            ),
            135 => 
            array (
                'id' => 136,
                'nama' => '378',
                'nama_pendek' => '10',
            ),
            136 => 
            array (
                'id' => 137,
                'nama' => '379',
                'nama_pendek' => '10',
            ),
            137 => 
            array (
                'id' => 138,
                'nama' => '380',
                'nama_pendek' => '10',
            ),
            138 => 
            array (
                'id' => 139,
                'nama' => '381',
                'nama_pendek' => '10',
            ),
            139 => 
            array (
                'id' => 140,
                'nama' => '864',
                'nama_pendek' => '10',
            ),
            140 => 
            array (
                'id' => 141,
                'nama' => '938',
                'nama_pendek' => '10',
            ),
            141 => 
            array (
                'id' => 142,
                'nama' => '209',
                'nama_pendek' => '18',
            ),
            142 => 
            array (
                'id' => 143,
                'nama' => '210',
                'nama_pendek' => '18',
            ),
            143 => 
            array (
                'id' => 144,
                'nama' => '211',
                'nama_pendek' => '18',
            ),
            144 => 
            array (
                'id' => 145,
                'nama' => '212',
                'nama_pendek' => '18',
            ),
            145 => 
            array (
                'id' => 146,
                'nama' => '213',
                'nama_pendek' => '18',
            ),
            146 => 
            array (
                'id' => 147,
                'nama' => '214',
                'nama_pendek' => '18',
            ),
            147 => 
            array (
                'id' => 148,
                'nama' => '215',
                'nama_pendek' => '18',
            ),
            148 => 
            array (
                'id' => 149,
                'nama' => '271',
                'nama_pendek' => '18',
            ),
            149 => 
            array (
                'id' => 150,
                'nama' => '376',
                'nama_pendek' => '18',
            ),
            150 => 
            array (
                'id' => 151,
                'nama' => '493',
                'nama_pendek' => '18',
            ),
            151 => 
            array (
                'id' => 152,
                'nama' => '494',
                'nama_pendek' => '18',
            ),
            152 => 
            array (
                'id' => 153,
                'nama' => '640',
                'nama_pendek' => '18',
            ),
            153 => 
            array (
                'id' => 154,
                'nama' => '641',
                'nama_pendek' => '18',
            ),
            154 => 
            array (
                'id' => 155,
                'nama' => '642',
                'nama_pendek' => '18',
            ),
            155 => 
            array (
                'id' => 156,
                'nama' => '858',
                'nama_pendek' => '18',
            ),
            156 => 
            array (
                'id' => 157,
                'nama' => '859',
                'nama_pendek' => '18',
            ),
            157 => 
            array (
                'id' => 158,
                'nama' => '1015',
                'nama_pendek' => '18',
            ),
            158 => 
            array (
                'id' => 159,
                'nama' => '1086',
                'nama_pendek' => '18',
            ),
            159 => 
            array (
                'id' => 160,
                'nama' => '1096',
                'nama_pendek' => '18',
            ),
            160 => 
            array (
                'id' => 161,
                'nama' => '22',
                'nama_pendek' => '19',
            ),
            161 => 
            array (
                'id' => 162,
                'nama' => '23',
                'nama_pendek' => '19',
            ),
            162 => 
            array (
                'id' => 163,
                'nama' => '167',
                'nama_pendek' => '19',
            ),
            163 => 
            array (
                'id' => 164,
                'nama' => '168',
                'nama_pendek' => '19',
            ),
            164 => 
            array (
                'id' => 165,
                'nama' => '169',
                'nama_pendek' => '19',
            ),
            165 => 
            array (
                'id' => 166,
                'nama' => '170',
                'nama_pendek' => '19',
            ),
            166 => 
            array (
                'id' => 167,
                'nama' => '171',
                'nama_pendek' => '19',
            ),
            167 => 
            array (
                'id' => 168,
                'nama' => '216',
                'nama_pendek' => '19',
            ),
            168 => 
            array (
                'id' => 169,
                'nama' => '269',
                'nama_pendek' => '19',
            ),
            169 => 
            array (
                'id' => 170,
                'nama' => '270',
                'nama_pendek' => '19',
            ),
            170 => 
            array (
                'id' => 171,
                'nama' => '293',
                'nama_pendek' => '19',
            ),
            171 => 
            array (
                'id' => 172,
                'nama' => '294',
                'nama_pendek' => '19',
            ),
            172 => 
            array (
                'id' => 173,
                'nama' => '303',
                'nama_pendek' => '19',
            ),
            173 => 
            array (
                'id' => 174,
                'nama' => '304',
                'nama_pendek' => '19',
            ),
            174 => 
            array (
                'id' => 175,
                'nama' => '764',
                'nama_pendek' => '19',
            ),
            175 => 
            array (
                'id' => 176,
                'nama' => '765',
                'nama_pendek' => '19',
            ),
            176 => 
            array (
                'id' => 177,
                'nama' => '768',
                'nama_pendek' => '19',
            ),
            177 => 
            array (
                'id' => 178,
                'nama' => '820',
                'nama_pendek' => '19',
            ),
            178 => 
            array (
                'id' => 179,
                'nama' => '821',
                'nama_pendek' => '19',
            ),
            179 => 
            array (
                'id' => 180,
                'nama' => '822',
                'nama_pendek' => '19',
            ),
            180 => 
            array (
                'id' => 181,
                'nama' => '823',
                'nama_pendek' => '19',
            ),
            181 => 
            array (
                'id' => 182,
                'nama' => '824',
                'nama_pendek' => '19',
            ),
            182 => 
            array (
                'id' => 183,
                'nama' => '825',
                'nama_pendek' => '19',
            ),
            183 => 
            array (
                'id' => 184,
                'nama' => '826',
                'nama_pendek' => '19',
            ),
            184 => 
            array (
                'id' => 185,
                'nama' => '827',
                'nama_pendek' => '19',
            ),
            185 => 
            array (
                'id' => 186,
                'nama' => '869',
                'nama_pendek' => '19',
            ),
            186 => 
            array (
                'id' => 187,
                'nama' => '903',
                'nama_pendek' => '15',
            ),
            187 => 
            array (
                'id' => 188,
                'nama' => '904',
                'nama_pendek' => '15',
            ),
            188 => 
            array (
                'id' => 189,
                'nama' => '905',
                'nama_pendek' => '15',
            ),
            189 => 
            array (
                'id' => 190,
                'nama' => '906',
                'nama_pendek' => '15',
            ),
            190 => 
            array (
                'id' => 191,
                'nama' => '907',
                'nama_pendek' => '15',
            ),
            191 => 
            array (
                'id' => 192,
                'nama' => '1087',
                'nama_pendek' => '15',
            ),
            192 => 
            array (
                'id' => 193,
                'nama' => '1088',
                'nama_pendek' => '15',
            ),
            193 => 
            array (
                'id' => 194,
                'nama' => '1089',
                'nama_pendek' => '15',
            ),
            194 => 
            array (
                'id' => 195,
                'nama' => '1090',
                'nama_pendek' => '15',
            ),
            195 => 
            array (
                'id' => 196,
                'nama' => '48',
                'nama_pendek' => '8',
            ),
            196 => 
            array (
                'id' => 197,
                'nama' => '49',
                'nama_pendek' => '8',
            ),
            197 => 
            array (
                'id' => 198,
                'nama' => '141',
                'nama_pendek' => '8',
            ),
            198 => 
            array (
                'id' => 199,
                'nama' => '142',
                'nama_pendek' => '8',
            ),
            199 => 
            array (
                'id' => 200,
                'nama' => '143',
                'nama_pendek' => '8',
            ),
            200 => 
            array (
                'id' => 201,
                'nama' => '144',
                'nama_pendek' => '8',
            ),
            201 => 
            array (
                'id' => 202,
                'nama' => '145',
                'nama_pendek' => '8',
            ),
            202 => 
            array (
                'id' => 203,
                'nama' => '146',
                'nama_pendek' => '8',
            ),
            203 => 
            array (
                'id' => 204,
                'nama' => '147',
                'nama_pendek' => '8',
            ),
            204 => 
            array (
                'id' => 205,
                'nama' => '148',
                'nama_pendek' => '8',
            ),
            205 => 
            array (
                'id' => 206,
                'nama' => '149',
                'nama_pendek' => '8',
            ),
            206 => 
            array (
                'id' => 207,
                'nama' => '150',
                'nama_pendek' => '8',
            ),
            207 => 
            array (
                'id' => 208,
                'nama' => '151',
                'nama_pendek' => '8',
            ),
            208 => 
            array (
                'id' => 209,
                'nama' => '152',
                'nama_pendek' => '8',
            ),
            209 => 
            array (
                'id' => 210,
                'nama' => '153',
                'nama_pendek' => '8',
            ),
            210 => 
            array (
                'id' => 211,
                'nama' => '154',
                'nama_pendek' => '8',
            ),
            211 => 
            array (
                'id' => 212,
                'nama' => '155',
                'nama_pendek' => '8',
            ),
            212 => 
            array (
                'id' => 213,
                'nama' => '156',
                'nama_pendek' => '8',
            ),
            213 => 
            array (
                'id' => 214,
                'nama' => '157',
                'nama_pendek' => '8',
            ),
            214 => 
            array (
                'id' => 215,
                'nama' => '158',
                'nama_pendek' => '8',
            ),
            215 => 
            array (
                'id' => 216,
                'nama' => '159',
                'nama_pendek' => '8',
            ),
            216 => 
            array (
                'id' => 217,
                'nama' => '160',
                'nama_pendek' => '8',
            ),
            217 => 
            array (
                'id' => 218,
                'nama' => '396',
                'nama_pendek' => '8',
            ),
            218 => 
            array (
                'id' => 219,
                'nama' => '397',
                'nama_pendek' => '8',
            ),
            219 => 
            array (
                'id' => 220,
                'nama' => '398',
                'nama_pendek' => '8',
            ),
            220 => 
            array (
                'id' => 221,
                'nama' => '399',
                'nama_pendek' => '8',
            ),
            221 => 
            array (
                'id' => 222,
                'nama' => '400',
                'nama_pendek' => '8',
            ),
            222 => 
            array (
                'id' => 223,
                'nama' => '406',
                'nama_pendek' => '8',
            ),
            223 => 
            array (
                'id' => 224,
                'nama' => '407',
                'nama_pendek' => '8',
            ),
            224 => 
            array (
                'id' => 225,
                'nama' => '408',
                'nama_pendek' => '8',
            ),
            225 => 
            array (
                'id' => 226,
                'nama' => '409',
                'nama_pendek' => '8',
            ),
            226 => 
            array (
                'id' => 227,
                'nama' => '423',
                'nama_pendek' => '8',
            ),
            227 => 
            array (
                'id' => 228,
                'nama' => '424',
                'nama_pendek' => '8',
            ),
            228 => 
            array (
                'id' => 229,
                'nama' => '425',
                'nama_pendek' => '8',
            ),
            229 => 
            array (
                'id' => 230,
                'nama' => '426',
                'nama_pendek' => '8',
            ),
            230 => 
            array (
                'id' => 231,
                'nama' => '427',
                'nama_pendek' => '8',
            ),
            231 => 
            array (
                'id' => 232,
                'nama' => '474',
                'nama_pendek' => '8',
            ),
            232 => 
            array (
                'id' => 233,
                'nama' => '519',
                'nama_pendek' => '8',
            ),
            233 => 
            array (
                'id' => 234,
                'nama' => '520',
                'nama_pendek' => '8',
            ),
            234 => 
            array (
                'id' => 235,
                'nama' => '521',
                'nama_pendek' => '8',
            ),
            235 => 
            array (
                'id' => 236,
                'nama' => '522',
                'nama_pendek' => '8',
            ),
            236 => 
            array (
                'id' => 237,
                'nama' => '523',
                'nama_pendek' => '8',
            ),
            237 => 
            array (
                'id' => 238,
                'nama' => '524',
                'nama_pendek' => '8',
            ),
            238 => 
            array (
                'id' => 239,
                'nama' => '539',
                'nama_pendek' => '8',
            ),
            239 => 
            array (
                'id' => 240,
                'nama' => '540',
                'nama_pendek' => '8',
            ),
            240 => 
            array (
                'id' => 241,
                'nama' => '645',
                'nama_pendek' => '8',
            ),
            241 => 
            array (
                'id' => 242,
                'nama' => '646',
                'nama_pendek' => '8',
            ),
            242 => 
            array (
                'id' => 243,
                'nama' => '647',
                'nama_pendek' => '8',
            ),
            243 => 
            array (
                'id' => 244,
                'nama' => '648',
                'nama_pendek' => '8',
            ),
            244 => 
            array (
                'id' => 245,
                'nama' => '649',
                'nama_pendek' => '8',
            ),
            245 => 
            array (
                'id' => 246,
                'nama' => '650',
                'nama_pendek' => '8',
            ),
            246 => 
            array (
                'id' => 247,
                'nama' => '651',
                'nama_pendek' => '8',
            ),
            247 => 
            array (
                'id' => 248,
                'nama' => '652',
                'nama_pendek' => '8',
            ),
            248 => 
            array (
                'id' => 249,
                'nama' => '653',
                'nama_pendek' => '8',
            ),
            249 => 
            array (
                'id' => 250,
                'nama' => '654',
                'nama_pendek' => '8',
            ),
            250 => 
            array (
                'id' => 251,
                'nama' => '655',
                'nama_pendek' => '8',
            ),
            251 => 
            array (
                'id' => 252,
                'nama' => '656',
                'nama_pendek' => '8',
            ),
            252 => 
            array (
                'id' => 253,
                'nama' => '657',
                'nama_pendek' => '8',
            ),
            253 => 
            array (
                'id' => 254,
                'nama' => '658',
                'nama_pendek' => '8',
            ),
            254 => 
            array (
                'id' => 255,
                'nama' => '659',
                'nama_pendek' => '8',
            ),
            255 => 
            array (
                'id' => 256,
                'nama' => '660',
                'nama_pendek' => '8',
            ),
            256 => 
            array (
                'id' => 257,
                'nama' => '712',
                'nama_pendek' => '8',
            ),
            257 => 
            array (
                'id' => 258,
                'nama' => '713',
                'nama_pendek' => '8',
            ),
            258 => 
            array (
                'id' => 259,
                'nama' => '714',
                'nama_pendek' => '8',
            ),
            259 => 
            array (
                'id' => 260,
                'nama' => '715',
                'nama_pendek' => '8',
            ),
            260 => 
            array (
                'id' => 261,
                'nama' => '716',
                'nama_pendek' => '8',
            ),
            261 => 
            array (
                'id' => 262,
                'nama' => '717',
                'nama_pendek' => '8',
            ),
            262 => 
            array (
                'id' => 263,
                'nama' => '718',
                'nama_pendek' => '8',
            ),
            263 => 
            array (
                'id' => 264,
                'nama' => '719',
                'nama_pendek' => '8',
            ),
            264 => 
            array (
                'id' => 265,
                'nama' => '720',
                'nama_pendek' => '8',
            ),
            265 => 
            array (
                'id' => 266,
                'nama' => '721',
                'nama_pendek' => '8',
            ),
            266 => 
            array (
                'id' => 267,
                'nama' => '722',
                'nama_pendek' => '8',
            ),
            267 => 
            array (
                'id' => 268,
                'nama' => '723',
                'nama_pendek' => '8',
            ),
            268 => 
            array (
                'id' => 269,
                'nama' => '724',
                'nama_pendek' => '8',
            ),
            269 => 
            array (
                'id' => 270,
                'nama' => '725',
                'nama_pendek' => '8',
            ),
            270 => 
            array (
                'id' => 271,
                'nama' => '860',
                'nama_pendek' => '8',
            ),
            271 => 
            array (
                'id' => 272,
                'nama' => '861',
                'nama_pendek' => '8',
            ),
            272 => 
            array (
                'id' => 273,
                'nama' => '866',
                'nama_pendek' => '8',
            ),
            273 => 
            array (
                'id' => 274,
                'nama' => '867',
                'nama_pendek' => '8',
            ),
            274 => 
            array (
                'id' => 275,
                'nama' => '878',
                'nama_pendek' => '8',
            ),
            275 => 
            array (
                'id' => 276,
                'nama' => '879',
                'nama_pendek' => '8',
            ),
            276 => 
            array (
                'id' => 277,
                'nama' => '880',
                'nama_pendek' => '8',
            ),
            277 => 
            array (
                'id' => 278,
                'nama' => '939',
                'nama_pendek' => '8',
            ),
            278 => 
            array (
                'id' => 279,
                'nama' => '940',
                'nama_pendek' => '8',
            ),
            279 => 
            array (
                'id' => 280,
                'nama' => '941',
                'nama_pendek' => '8',
            ),
            280 => 
            array (
                'id' => 281,
                'nama' => '958',
                'nama_pendek' => '8',
            ),
            281 => 
            array (
                'id' => 282,
                'nama' => '959',
                'nama_pendek' => '8',
            ),
            282 => 
            array (
                'id' => 283,
                'nama' => '960',
                'nama_pendek' => '8',
            ),
            283 => 
            array (
                'id' => 284,
                'nama' => '961',
                'nama_pendek' => '8',
            ),
            284 => 
            array (
                'id' => 285,
                'nama' => '962',
                'nama_pendek' => '8',
            ),
            285 => 
            array (
                'id' => 286,
                'nama' => '1040',
                'nama_pendek' => '8',
            ),
            286 => 
            array (
                'id' => 287,
                'nama' => '30',
                'nama_pendek' => '22',
            ),
            287 => 
            array (
                'id' => 288,
                'nama' => '174',
                'nama_pendek' => '22',
            ),
            288 => 
            array (
                'id' => 289,
                'nama' => '176',
                'nama_pendek' => '22',
            ),
            289 => 
            array (
                'id' => 290,
                'nama' => '201',
                'nama_pendek' => '22',
            ),
            290 => 
            array (
                'id' => 291,
                'nama' => '202',
                'nama_pendek' => '22',
            ),
            291 => 
            array (
                'id' => 292,
                'nama' => '203',
                'nama_pendek' => '22',
            ),
            292 => 
            array (
                'id' => 293,
                'nama' => '204',
                'nama_pendek' => '22',
            ),
            293 => 
            array (
                'id' => 294,
                'nama' => '205',
                'nama_pendek' => '22',
            ),
            294 => 
            array (
                'id' => 295,
                'nama' => '206',
                'nama_pendek' => '22',
            ),
            295 => 
            array (
                'id' => 296,
                'nama' => '207',
                'nama_pendek' => '22',
            ),
            296 => 
            array (
                'id' => 297,
                'nama' => '208',
                'nama_pendek' => '22',
            ),
            297 => 
            array (
                'id' => 298,
                'nama' => '290',
                'nama_pendek' => '22',
            ),
            298 => 
            array (
                'id' => 299,
                'nama' => '325',
                'nama_pendek' => '22',
            ),
            299 => 
            array (
                'id' => 300,
                'nama' => '326',
                'nama_pendek' => '22',
            ),
            300 => 
            array (
                'id' => 301,
                'nama' => '330',
                'nama_pendek' => '22',
            ),
            301 => 
            array (
                'id' => 302,
                'nama' => '392',
                'nama_pendek' => '22',
            ),
            302 => 
            array (
                'id' => 303,
                'nama' => '393',
                'nama_pendek' => '22',
            ),
            303 => 
            array (
                'id' => 304,
                'nama' => '394',
                'nama_pendek' => '22',
            ),
            304 => 
            array (
                'id' => 305,
                'nama' => '395',
                'nama_pendek' => '22',
            ),
            305 => 
            array (
                'id' => 306,
                'nama' => '514',
                'nama_pendek' => '22',
            ),
            306 => 
            array (
                'id' => 307,
                'nama' => '557',
                'nama_pendek' => '22',
            ),
            307 => 
            array (
                'id' => 308,
                'nama' => '563',
                'nama_pendek' => '22',
            ),
            308 => 
            array (
                'id' => 309,
                'nama' => '568',
                'nama_pendek' => '22',
            ),
            309 => 
            array (
                'id' => 310,
                'nama' => '588',
                'nama_pendek' => '22',
            ),
            310 => 
            array (
                'id' => 311,
                'nama' => '589',
                'nama_pendek' => '22',
            ),
            311 => 
            array (
                'id' => 312,
                'nama' => '590',
                'nama_pendek' => '22',
            ),
            312 => 
            array (
                'id' => 313,
                'nama' => '591',
                'nama_pendek' => '22',
            ),
            313 => 
            array (
                'id' => 314,
                'nama' => '592',
                'nama_pendek' => '22',
            ),
            314 => 
            array (
                'id' => 315,
                'nama' => '593',
                'nama_pendek' => '22',
            ),
            315 => 
            array (
                'id' => 316,
                'nama' => '594',
                'nama_pendek' => '22',
            ),
            316 => 
            array (
                'id' => 317,
                'nama' => '595',
                'nama_pendek' => '22',
            ),
            317 => 
            array (
                'id' => 318,
                'nama' => '596',
                'nama_pendek' => '22',
            ),
            318 => 
            array (
                'id' => 319,
                'nama' => '597',
                'nama_pendek' => '22',
            ),
            319 => 
            array (
                'id' => 320,
                'nama' => '598',
                'nama_pendek' => '22',
            ),
            320 => 
            array (
                'id' => 321,
                'nama' => '599',
                'nama_pendek' => '22',
            ),
            321 => 
            array (
                'id' => 322,
                'nama' => '600',
                'nama_pendek' => '22',
            ),
            322 => 
            array (
                'id' => 323,
                'nama' => '602',
                'nama_pendek' => '22',
            ),
            323 => 
            array (
                'id' => 324,
                'nama' => '603',
                'nama_pendek' => '22',
            ),
            324 => 
            array (
                'id' => 325,
                'nama' => '604',
                'nama_pendek' => '22',
            ),
            325 => 
            array (
                'id' => 326,
                'nama' => '605',
                'nama_pendek' => '22',
            ),
            326 => 
            array (
                'id' => 327,
                'nama' => '606',
                'nama_pendek' => '22',
            ),
            327 => 
            array (
                'id' => 328,
                'nama' => '607',
                'nama_pendek' => '22',
            ),
            328 => 
            array (
                'id' => 329,
                'nama' => '608',
                'nama_pendek' => '22',
            ),
            329 => 
            array (
                'id' => 330,
                'nama' => '609',
                'nama_pendek' => '22',
            ),
            330 => 
            array (
                'id' => 331,
                'nama' => '610',
                'nama_pendek' => '22',
            ),
            331 => 
            array (
                'id' => 332,
                'nama' => '612',
                'nama_pendek' => '22',
            ),
            332 => 
            array (
                'id' => 333,
                'nama' => '613',
                'nama_pendek' => '22',
            ),
            333 => 
            array (
                'id' => 334,
                'nama' => '614',
                'nama_pendek' => '22',
            ),
            334 => 
            array (
                'id' => 335,
                'nama' => '616',
                'nama_pendek' => '22',
            ),
            335 => 
            array (
                'id' => 336,
                'nama' => '618',
                'nama_pendek' => '22',
            ),
            336 => 
            array (
                'id' => 337,
                'nama' => '619',
                'nama_pendek' => '22',
            ),
            337 => 
            array (
                'id' => 338,
                'nama' => '620',
                'nama_pendek' => '22',
            ),
            338 => 
            array (
                'id' => 339,
                'nama' => '621',
                'nama_pendek' => '22',
            ),
            339 => 
            array (
                'id' => 340,
                'nama' => '623',
                'nama_pendek' => '22',
            ),
            340 => 
            array (
                'id' => 341,
                'nama' => '624',
                'nama_pendek' => '22',
            ),
            341 => 
            array (
                'id' => 342,
                'nama' => '625',
                'nama_pendek' => '22',
            ),
            342 => 
            array (
                'id' => 343,
                'nama' => '626',
                'nama_pendek' => '22',
            ),
            343 => 
            array (
                'id' => 344,
                'nama' => '627',
                'nama_pendek' => '22',
            ),
            344 => 
            array (
                'id' => 345,
                'nama' => '628',
                'nama_pendek' => '22',
            ),
            345 => 
            array (
                'id' => 346,
                'nama' => '696',
                'nama_pendek' => '22',
            ),
            346 => 
            array (
                'id' => 347,
                'nama' => '700',
                'nama_pendek' => '22',
            ),
            347 => 
            array (
                'id' => 348,
                'nama' => '703',
                'nama_pendek' => '22',
            ),
            348 => 
            array (
                'id' => 349,
                'nama' => '736',
                'nama_pendek' => '22',
            ),
            349 => 
            array (
                'id' => 350,
                'nama' => '737',
                'nama_pendek' => '22',
            ),
            350 => 
            array (
                'id' => 351,
                'nama' => '738',
                'nama_pendek' => '22',
            ),
            351 => 
            array (
                'id' => 352,
                'nama' => '739',
                'nama_pendek' => '22',
            ),
            352 => 
            array (
                'id' => 353,
                'nama' => '760',
                'nama_pendek' => '22',
            ),
            353 => 
            array (
                'id' => 354,
                'nama' => '761',
                'nama_pendek' => '22',
            ),
            354 => 
            array (
                'id' => 355,
                'nama' => '762',
                'nama_pendek' => '22',
            ),
            355 => 
            array (
                'id' => 356,
                'nama' => '763',
                'nama_pendek' => '22',
            ),
            356 => 
            array (
                'id' => 357,
                'nama' => '766',
                'nama_pendek' => '22',
            ),
            357 => 
            array (
                'id' => 358,
                'nama' => '767',
                'nama_pendek' => '22',
            ),
            358 => 
            array (
                'id' => 359,
                'nama' => '769',
                'nama_pendek' => '22',
            ),
            359 => 
            array (
                'id' => 360,
                'nama' => '770',
                'nama_pendek' => '22',
            ),
            360 => 
            array (
                'id' => 361,
                'nama' => '771',
                'nama_pendek' => '22',
            ),
            361 => 
            array (
                'id' => 362,
                'nama' => '772',
                'nama_pendek' => '22',
            ),
            362 => 
            array (
                'id' => 363,
                'nama' => '780',
                'nama_pendek' => '22',
            ),
            363 => 
            array (
                'id' => 364,
                'nama' => '781',
                'nama_pendek' => '22',
            ),
            364 => 
            array (
                'id' => 365,
                'nama' => '782',
                'nama_pendek' => '22',
            ),
            365 => 
            array (
                'id' => 366,
                'nama' => '786',
                'nama_pendek' => '22',
            ),
            366 => 
            array (
                'id' => 367,
                'nama' => '788',
                'nama_pendek' => '22',
            ),
            367 => 
            array (
                'id' => 368,
                'nama' => '789',
                'nama_pendek' => '22',
            ),
            368 => 
            array (
                'id' => 369,
                'nama' => '790',
                'nama_pendek' => '22',
            ),
            369 => 
            array (
                'id' => 370,
                'nama' => '791',
                'nama_pendek' => '22',
            ),
            370 => 
            array (
                'id' => 371,
                'nama' => '792',
                'nama_pendek' => '22',
            ),
            371 => 
            array (
                'id' => 372,
                'nama' => '793',
                'nama_pendek' => '22',
            ),
            372 => 
            array (
                'id' => 373,
                'nama' => '794',
                'nama_pendek' => '22',
            ),
            373 => 
            array (
                'id' => 374,
                'nama' => '795',
                'nama_pendek' => '22',
            ),
            374 => 
            array (
                'id' => 375,
                'nama' => '796',
                'nama_pendek' => '22',
            ),
            375 => 
            array (
                'id' => 376,
                'nama' => '797',
                'nama_pendek' => '22',
            ),
            376 => 
            array (
                'id' => 377,
                'nama' => '801',
                'nama_pendek' => '22',
            ),
            377 => 
            array (
                'id' => 378,
                'nama' => '802',
                'nama_pendek' => '22',
            ),
            378 => 
            array (
                'id' => 379,
                'nama' => '803',
                'nama_pendek' => '22',
            ),
            379 => 
            array (
                'id' => 380,
                'nama' => '804',
                'nama_pendek' => '22',
            ),
            380 => 
            array (
                'id' => 381,
                'nama' => '805',
                'nama_pendek' => '22',
            ),
            381 => 
            array (
                'id' => 382,
                'nama' => '806',
                'nama_pendek' => '22',
            ),
            382 => 
            array (
                'id' => 383,
                'nama' => '807',
                'nama_pendek' => '22',
            ),
            383 => 
            array (
                'id' => 384,
                'nama' => '808',
                'nama_pendek' => '22',
            ),
            384 => 
            array (
                'id' => 385,
                'nama' => '809',
                'nama_pendek' => '22',
            ),
            385 => 
            array (
                'id' => 386,
                'nama' => '810',
                'nama_pendek' => '22',
            ),
            386 => 
            array (
                'id' => 387,
                'nama' => '811',
                'nama_pendek' => '22',
            ),
            387 => 
            array (
                'id' => 388,
                'nama' => '812',
                'nama_pendek' => '22',
            ),
            388 => 
            array (
                'id' => 389,
                'nama' => '813',
                'nama_pendek' => '22',
            ),
            389 => 
            array (
                'id' => 390,
                'nama' => '814',
                'nama_pendek' => '22',
            ),
            390 => 
            array (
                'id' => 391,
                'nama' => '815',
                'nama_pendek' => '22',
            ),
            391 => 
            array (
                'id' => 392,
                'nama' => '819',
                'nama_pendek' => '22',
            ),
            392 => 
            array (
                'id' => 393,
                'nama' => '865',
                'nama_pendek' => '22',
            ),
            393 => 
            array (
                'id' => 394,
                'nama' => '870',
                'nama_pendek' => '22',
            ),
            394 => 
            array (
                'id' => 395,
                'nama' => '936',
                'nama_pendek' => '22',
            ),
            395 => 
            array (
                'id' => 396,
                'nama' => '986',
                'nama_pendek' => '22',
            ),
            396 => 
            array (
                'id' => 397,
                'nama' => '987',
                'nama_pendek' => '22',
            ),
            397 => 
            array (
                'id' => 398,
                'nama' => '988',
                'nama_pendek' => '22',
            ),
            398 => 
            array (
                'id' => 399,
                'nama' => '989',
                'nama_pendek' => '22',
            ),
            399 => 
            array (
                'id' => 400,
                'nama' => '990',
                'nama_pendek' => '22',
            ),
            400 => 
            array (
                'id' => 401,
                'nama' => '991',
                'nama_pendek' => '22',
            ),
            401 => 
            array (
                'id' => 402,
                'nama' => '992',
                'nama_pendek' => '22',
            ),
            402 => 
            array (
                'id' => 403,
                'nama' => '993',
                'nama_pendek' => '22',
            ),
            403 => 
            array (
                'id' => 404,
                'nama' => '994',
                'nama_pendek' => '22',
            ),
            404 => 
            array (
                'id' => 405,
                'nama' => '995',
                'nama_pendek' => '22',
            ),
            405 => 
            array (
                'id' => 406,
                'nama' => '1016',
                'nama_pendek' => '22',
            ),
            406 => 
            array (
                'id' => 407,
                'nama' => '1017',
                'nama_pendek' => '22',
            ),
            407 => 
            array (
                'id' => 408,
                'nama' => '1018',
                'nama_pendek' => '22',
            ),
            408 => 
            array (
                'id' => 409,
                'nama' => '1019',
                'nama_pendek' => '22',
            ),
            409 => 
            array (
                'id' => 410,
                'nama' => '1026',
                'nama_pendek' => '22',
            ),
            410 => 
            array (
                'id' => 411,
                'nama' => '1027',
                'nama_pendek' => '22',
            ),
            411 => 
            array (
                'id' => 412,
                'nama' => '1044',
                'nama_pendek' => '22',
            ),
            412 => 
            array (
                'id' => 413,
                'nama' => '1045',
                'nama_pendek' => '22',
            ),
            413 => 
            array (
                'id' => 414,
                'nama' => '1046',
                'nama_pendek' => '22',
            ),
            414 => 
            array (
                'id' => 415,
                'nama' => '1047',
                'nama_pendek' => '22',
            ),
            415 => 
            array (
                'id' => 416,
                'nama' => '1048',
                'nama_pendek' => '22',
            ),
            416 => 
            array (
                'id' => 417,
                'nama' => '1049',
                'nama_pendek' => '22',
            ),
            417 => 
            array (
                'id' => 418,
                'nama' => '1050',
                'nama_pendek' => '22',
            ),
            418 => 
            array (
                'id' => 419,
                'nama' => '1051',
                'nama_pendek' => '22',
            ),
            419 => 
            array (
                'id' => 420,
                'nama' => '1052',
                'nama_pendek' => '22',
            ),
            420 => 
            array (
                'id' => 421,
                'nama' => '1053',
                'nama_pendek' => '22',
            ),
            421 => 
            array (
                'id' => 422,
                'nama' => '1094',
                'nama_pendek' => '22',
            ),
            422 => 
            array (
                'id' => 423,
                'nama' => '1097',
                'nama_pendek' => '22',
            ),
            423 => 
            array (
                'id' => 424,
                'nama' => '1098',
                'nama_pendek' => '22',
            ),
            424 => 
            array (
                'id' => 425,
                'nama' => '133',
                'nama_pendek' => '6',
            ),
            425 => 
            array (
                'id' => 426,
                'nama' => '432',
                'nama_pendek' => '6',
            ),
            426 => 
            array (
                'id' => 427,
                'nama' => '433',
                'nama_pendek' => '6',
            ),
            427 => 
            array (
                'id' => 428,
                'nama' => '551',
                'nama_pendek' => '6',
            ),
            428 => 
            array (
                'id' => 429,
                'nama' => '218',
                'nama_pendek' => '28',
            ),
            429 => 
            array (
                'id' => 430,
                'nama' => '219',
                'nama_pendek' => '28',
            ),
            430 => 
            array (
                'id' => 431,
                'nama' => '220',
                'nama_pendek' => '28',
            ),
            431 => 
            array (
                'id' => 432,
                'nama' => '221',
                'nama_pendek' => '28',
            ),
            432 => 
            array (
                'id' => 433,
                'nama' => '1054',
                'nama_pendek' => '28',
            ),
            433 => 
            array (
                'id' => 434,
                'nama' => '1055',
                'nama_pendek' => '28',
            ),
            434 => 
            array (
                'id' => 435,
                'nama' => '1056',
                'nama_pendek' => '28',
            ),
            435 => 
            array (
                'id' => 436,
                'nama' => '1057',
                'nama_pendek' => '28',
            ),
            436 => 
            array (
                'id' => 437,
                'nama' => '20',
                'nama_pendek' => '3',
            ),
            437 => 
            array (
                'id' => 438,
                'nama' => '27',
                'nama_pendek' => '3',
            ),
            438 => 
            array (
                'id' => 439,
                'nama' => '28',
                'nama_pendek' => '3',
            ),
            439 => 
            array (
                'id' => 440,
                'nama' => '29',
                'nama_pendek' => '3',
            ),
            440 => 
            array (
                'id' => 441,
                'nama' => '50',
                'nama_pendek' => '3',
            ),
            441 => 
            array (
                'id' => 442,
                'nama' => '51',
                'nama_pendek' => '3',
            ),
            442 => 
            array (
                'id' => 443,
                'nama' => '69',
                'nama_pendek' => '3',
            ),
            443 => 
            array (
                'id' => 444,
                'nama' => '70',
                'nama_pendek' => '3',
            ),
            444 => 
            array (
                'id' => 445,
                'nama' => '132',
                'nama_pendek' => '3',
            ),
            445 => 
            array (
                'id' => 446,
                'nama' => '163',
                'nama_pendek' => '3',
            ),
            446 => 
            array (
                'id' => 447,
                'nama' => '164',
                'nama_pendek' => '3',
            ),
            447 => 
            array (
                'id' => 448,
                'nama' => '177',
                'nama_pendek' => '3',
            ),
            448 => 
            array (
                'id' => 449,
                'nama' => '178',
                'nama_pendek' => '3',
            ),
            449 => 
            array (
                'id' => 450,
                'nama' => '179',
                'nama_pendek' => '3',
            ),
            450 => 
            array (
                'id' => 451,
                'nama' => '180',
                'nama_pendek' => '3',
            ),
            451 => 
            array (
                'id' => 452,
                'nama' => '199',
                'nama_pendek' => '3',
            ),
            452 => 
            array (
                'id' => 453,
                'nama' => '266',
                'nama_pendek' => '3',
            ),
            453 => 
            array (
                'id' => 454,
                'nama' => '267',
                'nama_pendek' => '3',
            ),
            454 => 
            array (
                'id' => 455,
                'nama' => '268',
                'nama_pendek' => '3',
            ),
            455 => 
            array (
                'id' => 456,
                'nama' => '289',
                'nama_pendek' => '3',
            ),
            456 => 
            array (
                'id' => 457,
                'nama' => '291',
                'nama_pendek' => '3',
            ),
            457 => 
            array (
                'id' => 458,
                'nama' => '292',
                'nama_pendek' => '3',
            ),
            458 => 
            array (
                'id' => 459,
                'nama' => '295',
                'nama_pendek' => '3',
            ),
            459 => 
            array (
                'id' => 460,
                'nama' => '296',
                'nama_pendek' => '3',
            ),
            460 => 
            array (
                'id' => 461,
                'nama' => '297',
                'nama_pendek' => '3',
            ),
            461 => 
            array (
                'id' => 462,
                'nama' => '298',
                'nama_pendek' => '3',
            ),
            462 => 
            array (
                'id' => 463,
                'nama' => '299',
                'nama_pendek' => '3',
            ),
            463 => 
            array (
                'id' => 464,
                'nama' => '300',
                'nama_pendek' => '3',
            ),
            464 => 
            array (
                'id' => 465,
                'nama' => '302',
                'nama_pendek' => '3',
            ),
            465 => 
            array (
                'id' => 466,
                'nama' => '335',
                'nama_pendek' => '3',
            ),
            466 => 
            array (
                'id' => 467,
                'nama' => '336',
                'nama_pendek' => '3',
            ),
            467 => 
            array (
                'id' => 468,
                'nama' => '339',
                'nama_pendek' => '3',
            ),
            468 => 
            array (
                'id' => 469,
                'nama' => '342',
                'nama_pendek' => '3',
            ),
            469 => 
            array (
                'id' => 470,
                'nama' => '343',
                'nama_pendek' => '3',
            ),
            470 => 
            array (
                'id' => 471,
                'nama' => '344',
                'nama_pendek' => '3',
            ),
            471 => 
            array (
                'id' => 472,
                'nama' => '345',
                'nama_pendek' => '3',
            ),
            472 => 
            array (
                'id' => 473,
                'nama' => '346',
                'nama_pendek' => '3',
            ),
            473 => 
            array (
                'id' => 474,
                'nama' => '347',
                'nama_pendek' => '3',
            ),
            474 => 
            array (
                'id' => 475,
                'nama' => '348',
                'nama_pendek' => '3',
            ),
            475 => 
            array (
                'id' => 476,
                'nama' => '351',
                'nama_pendek' => '3',
            ),
            476 => 
            array (
                'id' => 477,
                'nama' => '359',
                'nama_pendek' => '3',
            ),
            477 => 
            array (
                'id' => 478,
                'nama' => '360',
                'nama_pendek' => '3',
            ),
            478 => 
            array (
                'id' => 479,
                'nama' => '361',
                'nama_pendek' => '3',
            ),
            479 => 
            array (
                'id' => 480,
                'nama' => '362',
                'nama_pendek' => '3',
            ),
            480 => 
            array (
                'id' => 481,
                'nama' => '363',
                'nama_pendek' => '3',
            ),
            481 => 
            array (
                'id' => 482,
                'nama' => '364',
                'nama_pendek' => '3',
            ),
            482 => 
            array (
                'id' => 483,
                'nama' => '365',
                'nama_pendek' => '3',
            ),
            483 => 
            array (
                'id' => 484,
                'nama' => '366',
                'nama_pendek' => '3',
            ),
            484 => 
            array (
                'id' => 485,
                'nama' => '367',
                'nama_pendek' => '3',
            ),
            485 => 
            array (
                'id' => 486,
                'nama' => '368',
                'nama_pendek' => '3',
            ),
            486 => 
            array (
                'id' => 487,
                'nama' => '369',
                'nama_pendek' => '3',
            ),
            487 => 
            array (
                'id' => 488,
                'nama' => '370',
                'nama_pendek' => '3',
            ),
            488 => 
            array (
                'id' => 489,
                'nama' => '371',
                'nama_pendek' => '3',
            ),
            489 => 
            array (
                'id' => 490,
                'nama' => '372',
                'nama_pendek' => '3',
            ),
            490 => 
            array (
                'id' => 491,
                'nama' => '373',
                'nama_pendek' => '3',
            ),
            491 => 
            array (
                'id' => 492,
                'nama' => '374',
                'nama_pendek' => '3',
            ),
            492 => 
            array (
                'id' => 493,
                'nama' => '375',
                'nama_pendek' => '3',
            ),
            493 => 
            array (
                'id' => 494,
                'nama' => '382',
                'nama_pendek' => '3',
            ),
            494 => 
            array (
                'id' => 495,
                'nama' => '383',
                'nama_pendek' => '3',
            ),
            495 => 
            array (
                'id' => 496,
                'nama' => '384',
                'nama_pendek' => '3',
            ),
            496 => 
            array (
                'id' => 497,
                'nama' => '385',
                'nama_pendek' => '3',
            ),
            497 => 
            array (
                'id' => 498,
                'nama' => '386',
                'nama_pendek' => '3',
            ),
            498 => 
            array (
                'id' => 499,
                'nama' => '387',
                'nama_pendek' => '3',
            ),
            499 => 
            array (
                'id' => 500,
                'nama' => '390',
                'nama_pendek' => '3',
            ),
        ));
        \DB::table('__item5s')->insert(array (
            0 => 
            array (
                'id' => 501,
                'nama' => '391',
                'nama_pendek' => '3',
            ),
            1 => 
            array (
                'id' => 502,
                'nama' => '414',
                'nama_pendek' => '3',
            ),
            2 => 
            array (
                'id' => 503,
                'nama' => '415',
                'nama_pendek' => '3',
            ),
            3 => 
            array (
                'id' => 504,
                'nama' => '416',
                'nama_pendek' => '3',
            ),
            4 => 
            array (
                'id' => 505,
                'nama' => '417',
                'nama_pendek' => '3',
            ),
            5 => 
            array (
                'id' => 506,
                'nama' => '418',
                'nama_pendek' => '3',
            ),
            6 => 
            array (
                'id' => 507,
                'nama' => '419',
                'nama_pendek' => '3',
            ),
            7 => 
            array (
                'id' => 508,
                'nama' => '420',
                'nama_pendek' => '3',
            ),
            8 => 
            array (
                'id' => 509,
                'nama' => '421',
                'nama_pendek' => '3',
            ),
            9 => 
            array (
                'id' => 510,
                'nama' => '422',
                'nama_pendek' => '3',
            ),
            10 => 
            array (
                'id' => 511,
                'nama' => '428',
                'nama_pendek' => '3',
            ),
            11 => 
            array (
                'id' => 512,
                'nama' => '429',
                'nama_pendek' => '3',
            ),
            12 => 
            array (
                'id' => 513,
                'nama' => '430',
                'nama_pendek' => '3',
            ),
            13 => 
            array (
                'id' => 514,
                'nama' => '434',
                'nama_pendek' => '3',
            ),
            14 => 
            array (
                'id' => 515,
                'nama' => '435',
                'nama_pendek' => '3',
            ),
            15 => 
            array (
                'id' => 516,
                'nama' => '436',
                'nama_pendek' => '3',
            ),
            16 => 
            array (
                'id' => 517,
                'nama' => '473',
                'nama_pendek' => '3',
            ),
            17 => 
            array (
                'id' => 518,
                'nama' => '475',
                'nama_pendek' => '3',
            ),
            18 => 
            array (
                'id' => 519,
                'nama' => '476',
                'nama_pendek' => '3',
            ),
            19 => 
            array (
                'id' => 520,
                'nama' => '482',
                'nama_pendek' => '3',
            ),
            20 => 
            array (
                'id' => 521,
                'nama' => '483',
                'nama_pendek' => '3',
            ),
            21 => 
            array (
                'id' => 522,
                'nama' => '484',
                'nama_pendek' => '3',
            ),
            22 => 
            array (
                'id' => 523,
                'nama' => '485',
                'nama_pendek' => '3',
            ),
            23 => 
            array (
                'id' => 524,
                'nama' => '486',
                'nama_pendek' => '3',
            ),
            24 => 
            array (
                'id' => 525,
                'nama' => '487',
                'nama_pendek' => '3',
            ),
            25 => 
            array (
                'id' => 526,
                'nama' => '488',
                'nama_pendek' => '3',
            ),
            26 => 
            array (
                'id' => 527,
                'nama' => '489',
                'nama_pendek' => '3',
            ),
            27 => 
            array (
                'id' => 528,
                'nama' => '525',
                'nama_pendek' => '3',
            ),
            28 => 
            array (
                'id' => 529,
                'nama' => '526',
                'nama_pendek' => '3',
            ),
            29 => 
            array (
                'id' => 530,
                'nama' => '527',
                'nama_pendek' => '3',
            ),
            30 => 
            array (
                'id' => 531,
                'nama' => '528',
                'nama_pendek' => '3',
            ),
            31 => 
            array (
                'id' => 532,
                'nama' => '529',
                'nama_pendek' => '3',
            ),
            32 => 
            array (
                'id' => 533,
                'nama' => '530',
                'nama_pendek' => '3',
            ),
            33 => 
            array (
                'id' => 534,
                'nama' => '531',
                'nama_pendek' => '3',
            ),
            34 => 
            array (
                'id' => 535,
                'nama' => '532',
                'nama_pendek' => '3',
            ),
            35 => 
            array (
                'id' => 536,
                'nama' => '533',
                'nama_pendek' => '3',
            ),
            36 => 
            array (
                'id' => 537,
                'nama' => '534',
                'nama_pendek' => '3',
            ),
            37 => 
            array (
                'id' => 538,
                'nama' => '535',
                'nama_pendek' => '3',
            ),
            38 => 
            array (
                'id' => 539,
                'nama' => '536',
                'nama_pendek' => '3',
            ),
            39 => 
            array (
                'id' => 540,
                'nama' => '537',
                'nama_pendek' => '3',
            ),
            40 => 
            array (
                'id' => 541,
                'nama' => '538',
                'nama_pendek' => '3',
            ),
            41 => 
            array (
                'id' => 542,
                'nama' => '543',
                'nama_pendek' => '3',
            ),
            42 => 
            array (
                'id' => 543,
                'nama' => '544',
                'nama_pendek' => '3',
            ),
            43 => 
            array (
                'id' => 544,
                'nama' => '545',
                'nama_pendek' => '3',
            ),
            44 => 
            array (
                'id' => 545,
                'nama' => '546',
                'nama_pendek' => '3',
            ),
            45 => 
            array (
                'id' => 546,
                'nama' => '547',
                'nama_pendek' => '3',
            ),
            46 => 
            array (
                'id' => 547,
                'nama' => '548',
                'nama_pendek' => '3',
            ),
            47 => 
            array (
                'id' => 548,
                'nama' => '549',
                'nama_pendek' => '3',
            ),
            48 => 
            array (
                'id' => 549,
                'nama' => '550',
                'nama_pendek' => '3',
            ),
            49 => 
            array (
                'id' => 550,
                'nama' => '630',
                'nama_pendek' => '3',
            ),
            50 => 
            array (
                'id' => 551,
                'nama' => '631',
                'nama_pendek' => '3',
            ),
            51 => 
            array (
                'id' => 552,
                'nama' => '632',
                'nama_pendek' => '3',
            ),
            52 => 
            array (
                'id' => 553,
                'nama' => '633',
                'nama_pendek' => '3',
            ),
            53 => 
            array (
                'id' => 554,
                'nama' => '634',
                'nama_pendek' => '3',
            ),
            54 => 
            array (
                'id' => 555,
                'nama' => '635',
                'nama_pendek' => '3',
            ),
            55 => 
            array (
                'id' => 556,
                'nama' => '636',
                'nama_pendek' => '3',
            ),
            56 => 
            array (
                'id' => 557,
                'nama' => '669',
                'nama_pendek' => '3',
            ),
            57 => 
            array (
                'id' => 558,
                'nama' => '689',
                'nama_pendek' => '3',
            ),
            58 => 
            array (
                'id' => 559,
                'nama' => '690',
                'nama_pendek' => '3',
            ),
            59 => 
            array (
                'id' => 560,
                'nama' => '691',
                'nama_pendek' => '3',
            ),
            60 => 
            array (
                'id' => 561,
                'nama' => '694',
                'nama_pendek' => '3',
            ),
            61 => 
            array (
                'id' => 562,
                'nama' => '695',
                'nama_pendek' => '3',
            ),
            62 => 
            array (
                'id' => 563,
                'nama' => '701',
                'nama_pendek' => '3',
            ),
            63 => 
            array (
                'id' => 564,
                'nama' => '702',
                'nama_pendek' => '3',
            ),
            64 => 
            array (
                'id' => 565,
                'nama' => '704',
                'nama_pendek' => '3',
            ),
            65 => 
            array (
                'id' => 566,
                'nama' => '705',
                'nama_pendek' => '3',
            ),
            66 => 
            array (
                'id' => 567,
                'nama' => '708',
                'nama_pendek' => '3',
            ),
            67 => 
            array (
                'id' => 568,
                'nama' => '709',
                'nama_pendek' => '3',
            ),
            68 => 
            array (
                'id' => 569,
                'nama' => '710',
                'nama_pendek' => '3',
            ),
            69 => 
            array (
                'id' => 570,
                'nama' => '711',
                'nama_pendek' => '3',
            ),
            70 => 
            array (
                'id' => 571,
                'nama' => '726',
                'nama_pendek' => '3',
            ),
            71 => 
            array (
                'id' => 572,
                'nama' => '727',
                'nama_pendek' => '3',
            ),
            72 => 
            array (
                'id' => 573,
                'nama' => '728',
                'nama_pendek' => '3',
            ),
            73 => 
            array (
                'id' => 574,
                'nama' => '729',
                'nama_pendek' => '3',
            ),
            74 => 
            array (
                'id' => 575,
                'nama' => '730',
                'nama_pendek' => '3',
            ),
            75 => 
            array (
                'id' => 576,
                'nama' => '731',
                'nama_pendek' => '3',
            ),
            76 => 
            array (
                'id' => 577,
                'nama' => '732',
                'nama_pendek' => '3',
            ),
            77 => 
            array (
                'id' => 578,
                'nama' => '733',
                'nama_pendek' => '3',
            ),
            78 => 
            array (
                'id' => 579,
                'nama' => '734',
                'nama_pendek' => '3',
            ),
            79 => 
            array (
                'id' => 580,
                'nama' => '735',
                'nama_pendek' => '3',
            ),
            80 => 
            array (
                'id' => 581,
                'nama' => '854',
                'nama_pendek' => '3',
            ),
            81 => 
            array (
                'id' => 582,
                'nama' => '855',
                'nama_pendek' => '3',
            ),
            82 => 
            array (
                'id' => 583,
                'nama' => '856',
                'nama_pendek' => '3',
            ),
            83 => 
            array (
                'id' => 584,
                'nama' => '882',
                'nama_pendek' => '3',
            ),
            84 => 
            array (
                'id' => 585,
                'nama' => '883',
                'nama_pendek' => '3',
            ),
            85 => 
            array (
                'id' => 586,
                'nama' => '887',
                'nama_pendek' => '3',
            ),
            86 => 
            array (
                'id' => 587,
                'nama' => '893',
                'nama_pendek' => '3',
            ),
            87 => 
            array (
                'id' => 588,
                'nama' => '896',
                'nama_pendek' => '3',
            ),
            88 => 
            array (
                'id' => 589,
                'nama' => '897',
                'nama_pendek' => '3',
            ),
            89 => 
            array (
                'id' => 590,
                'nama' => '898',
                'nama_pendek' => '3',
            ),
            90 => 
            array (
                'id' => 591,
                'nama' => '948',
                'nama_pendek' => '3',
            ),
            91 => 
            array (
                'id' => 592,
                'nama' => '952',
                'nama_pendek' => '3',
            ),
            92 => 
            array (
                'id' => 593,
                'nama' => '954',
                'nama_pendek' => '3',
            ),
            93 => 
            array (
                'id' => 594,
                'nama' => '955',
                'nama_pendek' => '3',
            ),
            94 => 
            array (
                'id' => 595,
                'nama' => '965',
                'nama_pendek' => '3',
            ),
            95 => 
            array (
                'id' => 596,
                'nama' => '1023',
                'nama_pendek' => '3',
            ),
            96 => 
            array (
                'id' => 597,
                'nama' => '1037',
                'nama_pendek' => '3',
            ),
            97 => 
            array (
                'id' => 598,
                'nama' => '1038',
                'nama_pendek' => '3',
            ),
            98 => 
            array (
                'id' => 599,
                'nama' => '1041',
                'nama_pendek' => '3',
            ),
            99 => 
            array (
                'id' => 600,
                'nama' => '1071',
                'nama_pendek' => '3',
            ),
            100 => 
            array (
                'id' => 601,
                'nama' => '1106',
                'nama_pendek' => '3',
            ),
            101 => 
            array (
                'id' => 602,
                'nama' => '64',
                'nama_pendek' => '27',
            ),
            102 => 
            array (
                'id' => 603,
                'nama' => '217',
                'nama_pendek' => '27',
            ),
            103 => 
            array (
                'id' => 604,
                'nama' => '643',
                'nama_pendek' => '27',
            ),
            104 => 
            array (
                'id' => 605,
                'nama' => '644',
                'nama_pendek' => '27',
            ),
            105 => 
            array (
                'id' => 606,
                'nama' => '863',
                'nama_pendek' => '27',
            ),
            106 => 
            array (
                'id' => 607,
                'nama' => '888',
                'nama_pendek' => '27',
            ),
            107 => 
            array (
                'id' => 608,
                'nama' => '891',
                'nama_pendek' => '27',
            ),
            108 => 
            array (
                'id' => 609,
                'nama' => '894',
                'nama_pendek' => '27',
            ),
            109 => 
            array (
                'id' => 610,
                'nama' => '895',
                'nama_pendek' => '27',
            ),
            110 => 
            array (
                'id' => 611,
                'nama' => '1010',
                'nama_pendek' => '27',
            ),
        ));
        
        
    }
}