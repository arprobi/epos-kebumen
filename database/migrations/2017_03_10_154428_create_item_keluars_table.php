<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_keluars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_masuk_id')->unsigned();
            $table->text('keterangan');
            $table->integer('jumlah');
            $table->timestamps();

            $table->foreign('item_masuk_id')->references('id')->on('item_masuks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_keluars');
    }
}
