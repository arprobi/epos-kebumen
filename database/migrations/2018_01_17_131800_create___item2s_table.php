<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItem2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('__item2s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('nama_pendek');
            $table->string('kode');
            $table->string('jenis');
            $table->string('satuan_1');
            $table->string('satuan_2');
            $table->string('satuan_3');
            $table->string('satuan_4');
            $table->string('satuan_5');
            $table->string('satuan_6');
            $table->string('suplier_1');
            $table->string('suplier_2');
            $table->string('suplier_3');
            $table->string('suplier_4');
            $table->string('suplier_5');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('__item2s');
    }
}
