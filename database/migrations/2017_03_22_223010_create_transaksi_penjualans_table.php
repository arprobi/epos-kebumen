<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiPenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_penjualans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->decimal('harga_total')->default(0);
            $table->decimal('nego_total')->default(0);
            $table->boolean('check_nego_total')->default(0);
            $table->decimal('potongan_penjualan')->default(0);
            $table->decimal('ongkos_kirim')->nullable()->default(0);
            $table->decimal('jumlah_bayar')->default(0);
            $table->enum('status', ['po_eceran', 'po_grosir', 'po_vip', 'eceran', 'grosir', 'vip']);

            $table->decimal('nominal_tunai')->default(0);

            $table->string('no_transfer')->nullable();
            $table->decimal('nominal_transfer')->default(0);
            $table->integer('bank_transfer')->unsigned()->nullable();

            $table->string('no_kartu')->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->default(0);
            $table->integer('bank_kartu')->unsigned()->nullable();

            $table->string('no_cek')->nullable();
            $table->decimal('nominal_cek')->default(0);
            $table->integer('bank_cek')->unsigned()->nullable();

            $table->string('no_bg')->nullable();
            $table->decimal('nominal_bg')->default(0);
            $table->integer('bank_bg')->unsigned()->nullable();

            $table->decimal('nominal_titipan')->default(0);

            $table->date('jatuh_tempo')->nullable();
            $table->integer('pelanggan_id')->unsigned()->nullable();
            $table->string('alamat')->nullable();
            $table->string('pengirim')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('grosir_id')->unsigned()->nullable();
            $table->integer('can_retur')->default(0);
            $table->timestamps();

            $table->foreign('bank_transfer')->references('id')->on('banks');
            $table->foreign('bank_cek')->references('id')->on('banks');
            $table->foreign('bank_bg')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
            $table->foreign('pelanggan_id')->references('id')->on('pelanggans');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('grosir_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_penjualans');
    }
}
