<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayarPiutangDagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayar_piutang_dagangs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->integer('piutang_dagang_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->decimal('jumlah_bayar');
            $table->decimal('nominal_tunai')->nullable();
            $table->string('no_transfer')->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();
            $table->string('no_kartu')->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();
            $table->string('no_cek')->nullable();
            $table->decimal('nominal_cek')->nullable();
            $table->string('no_bg')->nullable();
            $table->decimal('nominal_bg')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('piutang_dagang_id')->references('id')->on('piutang_dagangs');
            $table->foreign('bank_transfer')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayar_piutang_dagangs');
    }
}
