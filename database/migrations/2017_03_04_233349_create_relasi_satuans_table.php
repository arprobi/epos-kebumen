<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelasiSatuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relasi_satuans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_kode');
            $table->integer('satuan_id')->unsigned();
            $table->integer('konversi');
            $table->timestamps();

            $table->index('item_kode');

            $table->foreign('item_kode')->references('kode')->on('items');
            $table->foreign('satuan_id')->references('id')->on('satuans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relasi_satuans');
    }
}