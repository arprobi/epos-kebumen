<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHutangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hutangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaksi_pembelian_id')->unsigned();
            $table->integer('jumlah_bayar');
            $table->decimal('nominal_tunai')->nullable();
            $table->string('no_transfer')->nullable();
            $table->integer('bank_transfer')->unsigned()->nullable();
            $table->decimal('nominal_transfer')->nullable();
            $table->string('no_kartu')->nullable();
            $table->integer('bank_kartu')->unsigned()->nullable();
            $table->string('jenis_kartu')->nullable();
            $table->decimal('nominal_kartu')->nullable();
            $table->string('no_cek')->nullable();
            $table->integer('bank_cek')->unsigned()->nullable();
            $table->decimal('nominal_cek')->nullable();
            $table->integer('lunas_cek')->nullable();
            $table->string('no_bg')->nullable();
            $table->integer('bank_bg')->unsigned()->nullable();
            $table->decimal('nominal_bg')->nullable();
            $table->integer('lunas_bg')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians');
            $table->foreign('bank_transfer')->references('id')->on('banks');
            $table->foreign('bank_kartu')->references('id')->on('banks');
            $table->foreign('bank_cek')->references('id')->on('banks');
            $table->foreign('bank_bg')->references('id')->on('banks');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hutangs');
    }
}
