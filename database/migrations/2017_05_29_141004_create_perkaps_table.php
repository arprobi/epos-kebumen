<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerkapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perkaps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            // $table->string('label')->nullable();
            $table->string('nama');
            $table->decimal('harga');
            $table->decimal('nominal')->nullable();
            $table->decimal('residu')->nullable();
            $table->integer('umur');
            $table->integer('status');
            $table->string('no_transaksi')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perkaps');
    }
}
