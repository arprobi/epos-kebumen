<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogBebansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_bebans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modal_id')->unsigned();
            $table->integer('jumlah_bayar');
            $table->enum('metode_pembayaran', ['tunai', 'debit']);
            $table->string('no_transfer')->nullable();
            $table->timestamps();

            $table->foreign('modal_id')->references('id')->on('modals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_bebans');
    }
}
