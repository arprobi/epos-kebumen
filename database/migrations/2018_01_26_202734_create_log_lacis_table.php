<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogLacisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_lacis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengirim')->unsigned()->nullable();
            $table->integer('penerima')->unsigned()->nullable();
            $table->integer('approve')->nullable();
            $table->decimal('nominal');
            $table->string('keterangan')->nullable();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('pengirim')->references('id')->on('users');
            $table->foreign('penerima')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_lacis');
    }
}
